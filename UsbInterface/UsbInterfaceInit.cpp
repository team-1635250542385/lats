//!###############################################
//! Filename: UsbInterfaceInit.cpp
//! Description: Class that provides an interface
//!              to the Linux libusb library to get
//!              the device list and instantiate
//!              UsbInterface class per device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "UsbInterfaceInit.h"
#include "../SerialInterface/SerialInterface.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <usb.h>
#include <sstream>
#include <iomanip>
#include <bits/stdc++.h>

//!###############################################
//! Constructor: UsbInterfaceInit()
//!
//!###############################################
UsbInterfaceInit::UsbInterfaceInit(void)
{   

}

UsbInterfaceInit::~UsbInterfaceInit()
{

}

//!#########################################################
//! AddUsbInterfaceToArray()
//! Inputs: index - into the array; unique per device
//!         pointer to a UsbInterface class object
//! Description: Uses a lock for multithreading access
//!              hotplug and basic device access are
//!              on separate threads. Adds the UsbInterface
//!              pointer into the array at position index
//!#########################################################
void UsbInterfaceInit::AddUsbInterfaceToArray(uint8_t index, UsbInterface *usbInterfacePtr)
{
    usbInitCmdLock.lock();
    usbInterfaceArray[index] = usbInterfacePtr;
    usbInitCmdLock.unlock();

    iswDeviceCountLock.lock();
    iswDeviceCount++;
    iswDeviceCountLock.unlock();
}

//!#####################################################
//! RemoveUsbInterfaceFromArray()
//! Inputs: index - into the array; unique per device
//! Description: Uses a lock for multithreading access
//!              hotplug and basic device access are
//!              on separate threads. Sets entry to
//!              nullptr and decrements deviceCount
//!#####################################################
void UsbInterfaceInit::RemoveUsbInterfaceFromArray(uint8_t index)
{
    usbInitCmdLock.lock();
    usbInterfaceArray[index] = nullptr;
    usbInitCmdLock.unlock();

    iswDeviceCountLock.lock();
    if ( iswDeviceCount > 0 )
    {
        iswDeviceCount--;
    }
    else
    {
        iswDeviceCount = 0;
    }
    iswDeviceCountLock.unlock();
}

//!########################################################
//! CreateUsbInterface()
//! Inputs: dev - pointer to a libusb device handle
//!         desc - pointer to a libusb descriptor
//!         usbInterfacePtr - pointer to UsbInterface object
//!         index - index into the array for this device
//! Description:
//!########################################################
void UsbInterfaceInit::CreateUsbInterface(bool external, libusb_device *dev, libusb_device_descriptor *desc, UsbInterface **usbInterfacePtr, int index)
{
    std::string msgString;
    UsbInterface *usbInterface = nullptr;
    uint8_t moduleType         = UsbInterface::usbBoard; // default

    if ( external == true )
    {
        moduleType = UsbInterface::externalBoard;

        usbInterface = new UsbInterface(nullptr, theLogger, moduleType, nullptr);
        //! Add this usbInterface into the array at the place we found
        AddUsbInterfaceToArray(index, usbInterface);
        usbInterface->SetIndex(index);

        //! Pass back the pointer to the caller
        *usbInterfacePtr = usbInterface;

        return;
    }

    uint16_t vendorId = desc->idVendor;

    //! Check for board type. Vendor provides various
    //! test boards which have some different aspects
    //! that need to be addressed on when connecting.
    switch (vendorId)
    {
        case SERIAL_BOARD_VENDOR_ID:
        {
            if ( desc->idProduct == ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID )
            {
                moduleType                       = UsbInterface::serialBoard;
                SerialInterface *serialInterface = new SerialInterface(theLogger);
                usbInterface                     = new UsbInterface((long **)&dev, theLogger, moduleType, (void *)serialInterface);
                break;
            }
            else
            {
                moduleType = UsbInterface::usbBoard;

                //! Instantiate a UsbInterface for this device
                usbInterface = new UsbInterface((long **)&dev, theLogger, moduleType, nullptr);
            }
        }
        case ALEREON_BOARD_VENDOR_ID:
        default:
        {
            if ( desc->idProduct == ALEREON_PARALLEL_BOARD_PRODUCT_ID )
            {
                moduleType   = UsbInterface::parallelBoard;
                usbInterface = new UsbInterface((long **)&dev, theLogger, moduleType, nullptr);
            }
            else
            {
                moduleType = UsbInterface::usbBoard;

                //! Instantiate a UsbInterface for this device
                usbInterface = new UsbInterface((long **)&dev, theLogger, moduleType, nullptr);
            }
        }
    }

    int indexL = 0;
    uint8_t port[8];
    int numb = libusb_get_port_numbers(dev, port, sizeof(port));
    if (numb > 0 && desc->idVendor == SERIAL_BOARD_VENDOR_ID && desc->idProduct == ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID)
    {
        std::cout<<"Serial Device Found"<<std::endl;

        if (port[countSerial] >= port_tmp)
        {
            indexL = countSerial;
        }
        else
        {
            std::cout<<"Inserting serial device"<<std::endl;
            indexL = 0;//insert more than one?
        }
    }

    int status = -1;
    if ( usbInterface != nullptr )
    {
        //! Get the information from libusb Linux library and
        //! initialize the UsbInterface object.
        usbInterface->UpdateFromDevice(context, dev, desc, index);

        if ( moduleType == UsbInterface::serialBoard )
        {
            //! Open the device
            //! Uses libftdi to open and claim interface
            status = usbInterface->GetSerialInterface()->OpenDevice(index, countSerial, initCompleted, indexL);

            if ( status == 0 )
            {
                usbInterface->SetdDeviceOpen(true);
                port_tmp = port[countSerial];
                countSerial ++;
            }
            else
            {
                usbInterface->SetdDeviceOpen(false);
            }
        }
        else
        {
            //! Open the device
            //! Uses libusb to open and claim interface
            status = usbInterface->OpenDevice();
        }

        if (status == 0)
        {
            if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
            {
                msgString = "OpenDevice unsuccessful";
                theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Add this usbInterface into the array at the place we found
            AddUsbInterfaceToArray(index, usbInterface);
            usbInterface->SetIndex(index);

            if ( moduleType == UsbInterface::serialBoard )
            {
                usbInterface->GetSerialInterface()->SetIndex(index);
            }

            //! Pass back the pointer to the caller
            *usbInterfacePtr = usbInterface;

            if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
            {
                msgString = "OpenDevice successful";
                theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }
        else
        {
            if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
            {
                msgString = "OpenDevice unsuccessful";
                theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }
    }
    else
    {
        if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
        {
            msgString = "Create UsbInterface unsuccessful";
            theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
}

//!############################################################
//! InitUsbInterfaceArray()
//! Inputs: logger - pointer to the Logger object
//! Description: Initializes the usbInterfaceArray of all
//!              usb devices enumerated from the Linux libusb
//!              library that are ISW devices. We ignore other
//!              devices.
//!############################################################
std::array<UsbInterface *, MAX_DEVICES> *UsbInterfaceInit::InitUsbInterfaceArray(Logger *logger)
{
    int status = 0;

    //! Set the Logger
    theLogger = logger;
    std::string msgString;

    //! Initialize the array list that holds only ISW devices to nullptrs until we find some
    for (auto itr = usbInterfaceArray.begin(); itr != usbInterfaceArray.end(); itr++)
    {
        *itr = nullptr;
    }

    //! Initialize libusb library
    status = libusb_init(&context);

    if ( status == 0 )
    {
        if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
        {
            msgString = "libusb_init successful";
            theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
        }

        //! libusb deviceCount may be different than the iswDeviceCount
        //! which is what we care about. We are ignoring non-ISW devices
        deviceCount = libusb_get_device_list(context, &usbList);

        //! First go through all USB devices and get a list of just the ones we want
        //! This will be the usbInterfaceArray
        for (size_t idx = 0; idx < deviceCount; idx ++)
        {
            libusb_device *dev = usbList[idx];
            libusb_device_descriptor *desc = new libusb_device_descriptor;
            status = libusb_get_device_descriptor(dev, desc);

            if ( status == 0 )
            {
                //! See if this vendor id is in our list
                auto it = std::find(vendorIds.begin(), vendorIds.end(), desc->idVendor);

                if ( it != vendorIds.end() )
                {
                    UsbInterface *usbInterface = nullptr;
                    //! Instantiates usbInterface, open the device, and add to array[usbListIndex]
                    CreateUsbInterface(false, dev, desc, &usbInterface, usbListIndex);

                    //! We use locks because more than one thread could be trying
                    //! to do this - hotplug for USB devices is on a different thread
                    usbInitCmdLock.lock();
                    usbListIndex++;
                    usbInitCmdLock.unlock();
                }
            }
        }

        if ( iswDeviceCount <= 0 )
        {
            if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
            {
                std::string msgString = "UsbInterfaceInit did not find any ISW devices!";
                theLogger->LogMessage(msgString, 0xFF, std::chrono::system_clock::now());
            }
            status = -1;
        }

    }

    return ( &usbInterfaceArray );
}

//!############################################################
//! ExitUsbInterfaceArray()
//! Inputs: None
//! Description: Removes the entries from the usbInterfaceArray
//!############################################################
int UsbInterfaceInit::ExitUsbInterfaceArray(void)
{
    int status = 0;
    checkingUsbEvents = false;

    if ( iswDeviceCount > 0 )
    {

#ifndef TESTING  //!Don't run during unit tests
        while ( !handleUsbEventsThreadDone )
        {
            //! Wait for HandleUsbEvents thread to stop - it's on a timer
            sleep(2);
        }

        pthread_join(threadCheckUsbEvents, nullptr);
#endif

        for (auto itr = usbInterfaceArray.begin(); itr != usbInterfaceArray.end(); ++itr)
        {
            UsbInterface *usbInterface = *itr;
            if ( usbInterface != nullptr )
            {
                usbInterface->UsbInterfaceExit();
                delete(usbInterface);
                *itr = nullptr;
            }
        }
    }

    std::list<libusb_config_descriptor *> configs;
    configs.clear();

    //! Free reference in  libusb list
    libusb_free_device_list(usbList, deviceCount);
    //! Exit from libusb
    libusb_exit(context);

    return ( status );
}

//!######################################################################
//! HandleUsbEvents()
//! Inputs: arguments - pointer to a usbInterfaceInit object
//! Description: This method is started as a separate thread that
//!              is triggered by timeout and checks for USB hotplug events
//!              from the libusb library. libusb library was already setup to
//!              issue a callback after this check to
//!              IswInterfaceInit::HandleUsbHotplugEvent()
//!              The USB events are:
//!              - a device was removed from the list of Linux USB devices
//!              - a device was added to the list of LInux USB devices
//!######################################################################
void *UsbInterfaceInit::HandleUsbEvents(void *arguments)
{
    UsbInterfaceInit *usbInterfaceInit = (UsbInterfaceInit *)arguments;
    libusb_context *context = usbInterfaceInit->GetContext();

    //! Wait for checkingUsbEvents to be true
    //! checkingUsbEvents is true when SetupHotplugHandling() has
    //! linked libusb with the ISW library to handle hotplug events
    //! checkingUsbEvents is also false when shutting down
    int count = 0;
    while ( (!usbInterfaceInit->checkingUsbEvents) && (count++ < 90) )
    {
        sleep(1);
    }

    if ( usbInterfaceInit->checkingUsbEvents )
    {
        std::cout << "checkingUsbEvents true" << std::endl;
    }

    std::cout << "before while" << std::endl;
    //! Timeout every second and check for a USB hotplug events
    //! from libusb. The timeout is based on testing on a host Linux
    //! system.
    while ( usbInterfaceInit->checkingUsbEvents )
    {
        int completed = 0;
        struct timeval tv;

        libusb_handle_events_completed(context, &completed);
        tv.tv_sec = 0;
        tv.tv_usec = (1000);
        libusb_handle_events_timeout_completed(context,
                                               &tv,
                                               &completed);
    }

    std::cout << "about to set the thread handler to done" << std::endl;
    //! The ISW library is shutting down
    usbInterfaceInit->handleUsbEventsThreadDone = true;
    std::cout << "HandleUsbEvents ended" << std::endl;

    return (nullptr);
}

//!######################################################################
//! SetupHotplugHandling()
//! Inputs: callbackFunction - pointer to a callback function as defined
//!                            by the Linux libusb library
//!         thisPtr - a usbInterfacePtr since libusb callbacks come from
//!                   another thread
//! Description: This sets up libusb to notify the ISW Library on USB
//!              hotplug events.
//!              The USB events are:
//!              - a device was removed from the list of Linux USB devices
//!              - a device was added to the list of LInux USB devices
//!######################################################################
void UsbInterfaceInit::SetupHotplugHandling(libusb_hotplug_callback_fn callbackFunction, void *thisPtr)
{
    if ( libusb_has_capability(LIBUSB_CAP_HAS_HOTPLUG) )
    {
        hasHotplugCapability = true;
    }
    else
    {
        hasHotplugCapability = false;
    }

    if ( hasHotplugCapability )
    {
        //! Start the thread that will check libusb hotplug events
        if (pthread_create(&threadCheckUsbEvents, nullptr, &UsbInterfaceInit::HandleUsbEvents, (void *)this) != 0)
        {
            std::cout << "HandleUsbEvents didn't start!" << std::endl;
            return;
        }
        else
        {
            std::cout << "HandleUsbEvents started" << std::endl;
            checkingUsbEvents = true;
        }

        for (auto it = vendorIds.begin(); it != vendorIds.end(); ++it)
        {
            int status1 = LIBUSB_SUCCESS;
            int status2 = LIBUSB_SUCCESS;

            //! Register callback for hotplug event - a device has been added or has arrived in the Linux list of USB devices
            status1 = libusb_hotplug_register_callback(nullptr,
                                                      LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED,          //!events - device arrives
                                                      LIBUSB_HOTPLUG_ENUMERATE,                     //!flag
                                                      *it,                                          //!vendor Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!product Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!device class
                                                      callbackFunction,                             //!callback function
                                                      thisPtr,                                      //!user data
                                                      GetHotplugArriveHandle());                    //!hotplug handle for deregister

            if ( status1 != LIBUSB_SUCCESS )
            {
                if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
                {
                    std::string msgString = "libusb_hotplug_register_callback failed for LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED";
                    theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                }
            }

            //! Register callback for hotplug event - a device has been removed or has left the Linux list of USB devices
            status2 = libusb_hotplug_register_callback(nullptr,
                                                      LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT,             //!events - device arrives
                                                      LIBUSB_HOTPLUG_ENUMERATE,                     //!flag
                                                      *it,                                          //!vendor Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!product Id
                                                      LIBUSB_HOTPLUG_MATCH_ANY,                     //!device class
                                                      callbackFunction,                             //!callback function
                                                      thisPtr,                                      //!user data
                                                      GetHotplugLeaveHandle());                     //!hotplug handle for deregister

            if ( status2 != LIBUSB_SUCCESS )
            {
                if ( theLogger->DEBUG_UsbInterfaceInit == 1 )
                {
                    std::string msgString = "libusb_hotplug_register_callback failed for LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT";
                    theLogger->LogMessage(msgString, USB_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                }
            }
        }
    }
}
