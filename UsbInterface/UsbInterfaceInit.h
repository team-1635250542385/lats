//!###############################################
//! Filename: UsbInterface.h
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsafe I/O on
//!              a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef USBINTERFACE_H
#define USBINTERFACEINIT_H

#include "UsbInterface.h"

#ifdef TESTING
//! Use TESTING flag for unit tests
//! Unit testing doesnt access HW
#define ACCESS public
#else
#define ACCESS protected
#endif

class UsbInterfaceInit
{

public:
    UsbInterfaceInit(void);
    ~UsbInterfaceInit(void);

    //! Each entry in the usbInterfaceArray holds a pointer to
    //! one UsbInterface object with all information for accessing
    //! the device.  Only ISW devices are added from the Linux view of
    //! USB devices. The index in this array matches the index in
    //! the IswInterfaceInit class for ISW devices.
#define MAX_DEVICES 21
    std::array<UsbInterface *, MAX_DEVICES> *GetUsbInterfaceArray(void) { return (&usbInterfaceArray); }

    //! Initialize the array
    std::array<UsbInterface *, MAX_DEVICES> *InitUsbInterfaceArray(Logger *logger);

    //! Clean up the entries in the array before exiting
    int ExitUsbInterfaceArray(void);

    //! Count of ISW devices
    std::mutex iswDeviceCountLock;
    int iswDeviceCount = 0, countSerial = 0;
    uint8_t port_tmp   = 0;

    //! Get the libusb context
    libusb_context *GetContext(void) { return (context); }

    //! Create a UsbInterface object from information read in from libusb
    void CreateUsbInterface(bool external, libusb_device *dev, libusb_device_descriptor *desc, UsbInterface **usbInterfacePtr, int index);

    //! For Handling hotplug events from libusb
    //! Runs a separate thread that is triggered by timeout
    //! and checks for USB hotplug events from the libusb library.
    static void *HandleUsbEvents(void *arguments);

    //! For setting up hotplug handling within libusb
    int *GetHotplugArriveHandle(void)  { return (&hotplugArriveHandle); }
    int *GetHotplugLeaveHandle(void)  { return (&hotplugLeaveHandle); }
    void SetupHotplugHandling(libusb_hotplug_callback_fn callbackFunction, void *thisPtr);

    //! Add a new UsbInterface object to the array of devices
    void AddUsbInterfaceToArray(uint8_t index, UsbInterface *usbInterfacePtr);
    //! Remove a new UsbInterface object from the array of devices
    void RemoveUsbInterfaceFromArray(uint8_t index);

    //! Used to know when we are checking for USB hotplug events - could be shutting down
    bool GetCheckingUsbEvents(void) { return checkingUsbEvents; }
    void SetCheckingUsbEvents(bool checkUsbEvents) { checkingUsbEvents = checkUsbEvents; }

    //! ISW USB devices or cable
    //! from a terminal issue Linux command: lsusb -v
    static const uint16_t ALEREON_BOARD_VENDOR_ID             = 0x13DC;
    static const uint16_t SERIAL_BOARD_VENDOR_ID              = 0x0403;
    std::list<uint16_t> vendorIds                             = {ALEREON_BOARD_VENDOR_ID, SERIAL_BOARD_VENDOR_ID};
    static const uint16_t ALEREON_USB_BOARD_PRODUCT_ID        = 0xBA55;
    static const uint16_t ALEREON_PARALLEL_BOARD_PRODUCT_ID   = 0xCF01;
    static const uint16_t ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID = 0x6001;
    int initCompleted                                         = 0;

ACCESS:
    //! Keep libusb device list
    //! for freeing on exit
    libusb_device **usbList;
    //! libusb device count
    int deviceCount = 0;
    //! One context per instantiation of this class
    libusb_context *context;

    //! USB device list used by ISW
    //! the iswInterfaceArray uses the same index as the
    //! device uses in usbInterfaceArray
    std::array<UsbInterface *, MAX_DEVICES> usbInterfaceArray;

    //! Passed in by caller or per test
    Logger *theLogger;
    //! Used before any interfaces are initialized
    //! If there is a failure to log and there is
    //! no interface number, this is used in place
    //! of GetIndex()
#define USB_INTERFACE_LOG_INDEX 0x00FF

    //! Internal index for logging ISW devices
    //! matches the index in usbInterfaceArray
    uint8_t usbListIndex = 0;

    //! OS supports hotplug
    std::mutex usbInitCmdLock;
    bool hasHotplugCapability = true;
    libusb_hotplug_callback_handle hotplugArriveHandle;
    libusb_hotplug_callback_handle hotplugLeaveHandle;

    //! Used to check events while this module is running
    bool checkingUsbEvents         = false;

    //! Used to stop the hotplug handling thread
    bool handleUsbEventsThreadDone = false;

    //! Hotplug handling thread
    pthread_t threadCheckUsbEvents;
};

#endif //! UsbInterfaceInit
