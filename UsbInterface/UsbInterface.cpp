//!###############################################
//! Filename: UsbInterface.cpp
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsafe I/O on
//!              a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "UsbInterface.h"
#include <stddef.h>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <iomanip>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <string.h>
#include <new>
//#include "../iswTestTool/node.h"
//#include "../iswTestTool/edge.h"
#include <vector>
#include <array>
#include <sstream>
#include <chrono>

//!###############################################################
//! Constructor: UsbInterface()
//! Inputs: device - a pointer to the device in the libusb library
//!         logger - a pointer to the Logger object
//!         board_type - serial, usb, or parallel
//!         boardData - a pointer to a board specific object
//!                     like a SerialInterface class, etc.
//!###############################################################
UsbInterface::UsbInterface(long **device, Logger *logger, uint8_t board_type, void *boardData) : Interface(device, logger, board_type, boardData)
{
    if ( board_type != UsbInterface::externalBoard )
    {
        dev = (libusb_device *)*device;

    }
    else
    {
        dev = nullptr;
    }
    boardType = board_type;

    //! board types currently based on ISW vendor supplied
    //! test boards.  These all connect via USB to a host
    //! Linux system - they are not embedded devices for
    //! product development.
    //! Serial is usb to serial using an FTDI cable
    //! Parallel is usb to a test board that converts to an ISW
    //!          parallel board
    //! USB is the standard USB test board
    switch (boardType)
    {
        case serialBoard:
        {
            if ( boardData != nullptr )
            {
                serialInterface = (SerialInterface *)boardData;
            }
            break;
        }
        case parallelBoard:
        case usbBoard:
        case externalBoard:
        default:
            break;
    }
}

UsbInterface::~UsbInterface()
{

}

//!###############################################################
//! UsbInterfaceExit()
//! Inputs: None
//! Description: Shuts down this usbInterface object which is
//!              connected to one ISW device. It cleans up libusb
//!              objects and closes the device in Linux.
//!###############################################################
void UsbInterface::UsbInterfaceExit(void)
{
    switch (boardType)
    {
        case serialBoard:
        {
            if ( serialInterface != nullptr )
            {
                serialInterface->CloseDevice();
                delete serialInterface;
            }
            break;
        }
        case usbBoard:
        default:
            break;
    }

    //! delete the libusb descriptor
    if (desc != nullptr)
    {
        delete( desc);
    }

    //! Close the USB device - no more I/O
    if (devHandle != nullptr)
    {
        CloseDevice();
    }
}

//!###############################################################
//! UpdateFromDevice()
//! Inputs: ctx - pointer to a libusb context object
//!         device - pointer to a libusb device
//!         descriptor - point to a libusb descriptor
//!         index - index into the UsbInterface array of devices
//! Description: this method gets USB attributes that are stored
//!              in the libusb descriptor.
//!###############################################################
void UsbInterface::UpdateFromDevice(libusb_context *ctx, libusb_device *device, libusb_device_descriptor *descriptor, uint8_t index)
{
    std::string msgString = "UpdateFromDevice";
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        theLogger->LogMessage(msgString, index, std::chrono::system_clock::now());
    }

    usbCmdLock.lock();

    //! This index is for logging and should
    //! match the position in the array
    SetIndex(index);

    //! Initialize from libusb enumeration
    //! USB vendor id,
    SetVendorId(descriptor->idVendor);

    //! product id
    SetProductId(descriptor->idProduct);

    //! USB description of the device
    SetDeviceDescriptor(descriptor);

    //! libusb context
    SetContext(ctx);

    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        std::stringstream ss;
        ss << std::setfill('0');
        ss << std::endl;
        ss << "  idVendor  = " << std::hex << std::setw(2) << (int)(idVendor) << std::endl;
        ss << "  idProduct = " << std::hex << std::setw(2) << (int)(idProduct) << std::endl;
        ss << "  index     = " << std::to_string(index) << std::endl;
        theLogger->DebugMessage(ss.str(), GetIndex());
    }

    //! Fill the USB config descriptor list.
    //! It is needed later. Checks for more than one.
    std::list<libusb_config_descriptor *> *configs = GetConfigList();
    configs->clear();
    for (int i = 0; i < desc->bNumConfigurations; i++)
    {
        libusb_config_descriptor *config;
        int status = libusb_get_config_descriptor(dev, i, &config);

        if ( status == 0 )
        {
            configs->push_back(config);
        }
        else
        {
            if ( theLogger->DEBUG_UsbInterface == 1 )
            {
                msgString = "libusb_get_config_descriptor unsuccessful";
                theLogger->LogMessage(msgString, index, std::chrono::system_clock::now());
            }
        }
    }

    //! Get more USB attributes from the enumeration
    GetUsbAttributes(device);

    usbCmdLock.unlock();
}

//!###############################################################
//! GetUsbAttributes()
//! Inputs: device - pointer to a libusb device
//! Description: This method checks for other USB device attributes
//!              that are stored in the config list like the vendor
//!              USB Header type. It then gets the BULK_IN
//!              and BULK_OUT address for USB transport of data.
//!###############################################################
void UsbInterface::GetUsbAttributes(libusb_device *device)
{
    std::string msgString;

    std::list<libusb_config_descriptor *> *configList = GetConfigList();
    for (std::list<libusb_config_descriptor *>::const_iterator iterator = configList->begin(),
         end = configList->end(); iterator != end; ++iterator)
    {
        libusb_config_descriptor *config = *iterator;
        //! Alereon USB devices using the TRU interface have setting 0xFF
        if (config->interface->altsetting->bInterfaceClass == LIBUSB_VENDOR_SPECIFIC_DATA)
        {
            usbHeaderType = USB_HEADER_TRU;
        }
        else
        {
            std::cout << "Unsupported USB Header Type" << std::endl;
            exit(-1);
        }

        for (int i = 0; i < config->interface->altsetting->bNumEndpoints; i++)
        {
            struct libusb_endpoint_descriptor *endpoint =
                    (struct libusb_endpoint_descriptor *)&config->interface->altsetting->endpoint[i];

            std::stringstream ss;
            // Reset error bits
            ss.clear();
            // Clear string
            ss.str(std::string());

            int inAddr  = 0;
            int outAddr = 0;

            if (endpoint->bEndpointAddress & 0x80)
            {
                //! Set the IN address
                inAddr = LIBUSB_ENDPOINT_IN |
                        (config->interface->altsetting->endpoint->bEndpointAddress & 0xF);
                SetEndpointIN(inAddr);

                //! Set max packet size
                SetMaxPacketSizeIn(config->interface->altsetting->endpoint->wMaxPacketSize);

                if ( theLogger->DEBUG_UsbInterface == 1 )
                {
                    ss << std::setfill('0');
                    ss << std::endl;
                    ss << "  USB_ENDPOINT_IN = 0x" << std::hex << std::setw(2) << (int)(inAddr) << std::endl;
                    ss << "  USB_ENDPOINT_IN max packet size = " << std::to_string(GetMaxPacketSizeIn()) << std::endl;;
                     ss << std::endl;
                    theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                //! Set the OUT address
                outAddr = LIBUSB_ENDPOINT_OUT |
                        (endpoint->bEndpointAddress & 0xF);
                SetEndpointOUT(outAddr);

                //! Set max packet size
                SetMaxPacketSizeOut(config->interface->altsetting->endpoint->wMaxPacketSize);

                if ( theLogger->DEBUG_UsbInterface == 1 )
                {
                    ss << std::setfill('0');
                    ss << std::endl;
                    ss << "  USB_ENDPOINT_OUT = 0x" << std::hex << std::setw(2) << (int)(outAddr) << std::endl;
                    ss << "  USB_ENDPOINT_OUT max packet size = " << std::to_string(GetMaxPacketSizeOut()) << std::endl;
                    ss << std::endl;
                    theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                }
            }

            std::cout << ss.str();
        }
    }
}

//!###############################################################
//! OpenDevice()
//! Inputs: None
//! Description: Opens the ISW device as a Linux device
//!###############################################################
int UsbInterface::OpenDevice(void)
{
    int err   = 0;
    devHandle = nullptr;
    libusb_device_handle *dev_handle;

    err = libusb_open(dev, &dev_handle);
    if (err)
    {
        std::string errString = "Can not open USB device";
        LogErrorString(errString, err);
        deviceOpen = false;
    }
    else
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            std::string msgString = "libusb_open";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
        devHandle = dev_handle;

        //! Claim the interface from the kernel
        ClaimInterface();

        deviceOpen = true;
    }
    return err;
}

//!###############################################################
//! CloseDevice()
//! Inputs: None
//! Description: Closes the Linux device that is the ISW device
//!###############################################################
int UsbInterface::CloseDevice(void)
{
    int err = 0;

    //!Close the reference to the USB device
    //!I/O is no longer possible
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        std::string msgString = "libusb_close";
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }
    libusb_close(devHandle);
    devHandle = nullptr;
    deviceOpen = false;

    return err;
}

//!##################################################################
//! ClaimInterface()
//! Inputs: None
//! Description: Takes ownership of the Linux device from the kernel
//!              so that ISW Library can open/close/read/write to
//!              the device
//!##################################################################
int UsbInterface::ClaimInterface(void)
{
    //! Check whether a kernel driver is attached to
    //! any of the interfaces. If so, detach it.
    //! Kernel is re-attached in deconstructor
    int status = 0;
    std::string msgString;

    if (libusb_kernel_driver_active(devHandle, 0) == 1)
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString.append("libusb_kernel_driver_active");
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
        status = libusb_detach_kernel_driver(devHandle, 0);
        if (status == 0)
        {
            if ( theLogger->DEBUG_UsbInterface == 1 )
            {
                msgString = "libusb_detach_kernel_driver successful";
                theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            }
        }
        else
        {
            if ( theLogger->DEBUG_UsbInterface == 1 )
            {
                msgString= "libusb_detach_kernel_driver unsuccessful";
                theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
                LogError(status);
            }
        }       
    }

    //! Claim the interface
    status = libusb_claim_interface(devHandle, 0);
    if (status == 0)
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_claim_interface successful";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
    }
    else {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_claim_interface NOT successful";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            LogError(status);
        }
    }

    return (status);
}


//!#####################################################################
//! SendControlMessage()
//! Inputs: bmRequestType - USB Setup Packet request type
//!         bRequest - the actual request
//!         wValue - A word-size value that varies according to the request
//!         wIndex - A word-size value that varies according to the request
//!         data - Any data to be sent
//!         wLength - length of data to be transferred
//!         ctr_timeout
//! Description: Perform a USB control transfer. Returns the actual
//!              number of bytes transferred on success, in the range
//!              from and including zero up to and including wLength
//!              Not used in the Linux ISW Library
//!#####################################################################
int UsbInterface::SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                                     uint16_t wValue, uint16_t wIndex,
                                     std::string *data, uint16_t wLength,
                                     unsigned int ctr_timeout)
{
    int status = 0;

    std::string msgString;
    msgString.append("libusb_control_transfer");
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }
    status = libusb_control_transfer(devHandle, bmRequestType, bRequest,
                                    wValue, wIndex, (unsigned char *)data->data(), wLength,
                                    ctr_timeout);

    if (status == 0)
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_control_transfer successful";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
    }
    else {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_control_transfer NOT successful";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            LogError(status);
        }
    }

    return(status);
}


//!#####################################################################
//! AsyncWriteDevice()
//! Inputs: transferbuf - pointer to the data buffer
//!         endpoint - which USB endpoint to send to. (NOT ISW endpoint)
//!         length - length of transferBuf
//!         callback - pointer to callback function after transfer
//! Description: Perform a USB bulk asynchronous transfer. Returns the
//!              number of bytes transferred on success. Pass in a
//!              a callback function for a non-blocking call.
//!#####################################################################
int UsbInterface::AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback)
{
    int status = 0;
    libusb_transfer_cb_fn callBack = (libusb_transfer_cb_fn)callback;

    struct libusb_transfer *transfer = libusb_alloc_transfer(0);
    std::string msgString = "libusb_fill_bulk_transfer";
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }
    libusb_fill_bulk_transfer(transfer,
        devHandle,
        endpoint,
        (unsigned char *)transferBuf,
        length,
        callBack,
        nullptr/*callback data*/,
        timeout);

    status = libusb_submit_transfer(transfer);
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        msgString = "libusb_submit_transfer";
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }

    msgString.clear();
    if (status < 0)
    {
        //! Error
        libusb_free_transfer(transfer);
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString.append("libusb_fill_bulk_transfer failed");
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
        LogError(status);
    }
    else {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString.append("libusb_fill_bulk_transfer successful");
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
    }

    return (status);
}

//!#####################################################################
//! SyncWriteDevice()
//! Inputs: transferbuf - pointer to the data buffer
//!         endpoint - which USB endpoint to send to. (NOT ISW endpoint)
//!         length - length of transferBuf
//! Description: Perform a USB bulk synchronous transfer. Returns the
//!              number of bytes transferred on success. This is a
//!              blocking call
//!#####################################################################
int UsbInterface::SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length)
{
    //! ISW control commands and ISW SolNet Message Packets use
    //! this method.  They may come from different threads so get
    //! the lock
    usbCmdSendLock.lock();

    //! write data out USB interface
    int status = 0;
    int transferred = 0;

    std::string msgString = "libusb_bulk_transfer write";
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }

    status = libusb_bulk_transfer(devHandle, endpoint,
                               (unsigned char *)transferBuf,
                               length,
                               &transferred, timeout);

    if (status < 0)
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_bulk_transfer write failed";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            LogError(status);
            msgString.clear();
            msgString.append(" transferred: ");
            msgString += std::to_string(transferred);
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
    }
    else
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_bulk_transfer write successful";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
    }
    usbCmdSendLock.unlock();

    return (status);
}

//!#####################################################################
//! ReadDevice()
//! Inputs: receiveBuf - pointer to the data buffer
//!         endpoint - which USB endpoint to send to. (NOT ISW endpoint)
//!         length - length of transferBuf
//!         numRead - pointer to variable holding the number of bytes
//!                   read returned from the call
//! Description: Perform a USB read on the device. Returns the
//!              number of bytes read in on success. This is a
//!              blocking call
//!######################################################################
int UsbInterface::ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead)
{
    //! read data in from USB interface
    int status = 0;
    std::string msgString;

    usbCmdReceiveLock.lock();

    if (deviceOpen == true)
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_bulk_transfer read";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
        status = libusb_bulk_transfer(devHandle, endpoint,
                                   (unsigned char *)receiveBuf,
                                   length,
                                   numRead, timeout);

        msgString.clear();
        if (status != 0)
        {
            if ( theLogger->DEBUG_UsbInterface == 1 )
            {
                msgString.append("libusb_bulk_transfer read failed");
                theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
                LogError(status);
                msgString = " numRead: ";
                msgString.append(std::to_string(*numRead));
                theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            }
            status = -1;
        }
        else
        {
            if ( theLogger->DEBUG_UsbInterface == 1 )
            {
                msgString = "libusb_bulk_transfer read successful";
                theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
            }
        }
    }
    else
    {
        if ( theLogger->DEBUG_UsbInterface == 1 )
        {
            msgString = "libusb_bulk_transfer - device not open";
            theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
        }
        status = -1;
    }
    usbCmdReceiveLock.unlock();

    return(status);
}

//!#####################################################################
//! SetDebugLevel()
//! Inputs: context - libusb contex
//!         debug_level - integer representing a libusb debug level
//! Description: This is for libusb debugging
//!######################################################################
void UsbInterface::SetDebugLevel(int debug_level)
{
    libusb_set_debug(context, debug_level);
    debugLevel = debug_level;
}

//!#####################################################################
//! LogError()
//! Inputs: code - integer representing a libusb error level
//! Description: This is for printing libusb errors
//!######################################################################
void UsbInterface::LogError(int code)
{
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        std::string msgString;

        switch(code)
        {
            case LIBUSB_ERROR_IO:
            {
                msgString = "Error: LIBUSB_ERROR_IO\nInput/output error";
                break;
            }
            case LIBUSB_ERROR_INVALID_PARAM:
            {
                msgString = "Error: LIBUSB_ERROR_INVALID_PARAM\nInvalid parameter";
                break;
            }
            case LIBUSB_ERROR_ACCESS:
            {
                msgString = "Error: LIBUSB_ERROR_ACCESS\nAccess denied (insufficient permissions)";
                break;
            }
            case LIBUSB_ERROR_NO_DEVICE:
            {
                msgString = "Error: LIBUSB_ERROR_NO_DEVICE\nNo such device (it may have been disconnected)";
                break;
            }
            case LIBUSB_ERROR_NOT_FOUND:
            {
                msgString = "Error: LIBUSB_ERROR_NOT_FOUND\nEntity not found";
                break;
            }
            case LIBUSB_ERROR_BUSY:
            {
                msgString = "Error: LIBUSB_ERROR_BUSY\nResource busy";
                break;
            }
            case LIBUSB_ERROR_TIMEOUT:
            {
                msgString = "Error: LIBUSB_ERROR_TIMEOUT\nOperation timed out";
                break;
            }
            case LIBUSB_ERROR_OVERFLOW:
            {
                msgString = "Error: LIBUSB_ERROR_OVERFLOW\nOverflow";
                break;
            }
            case LIBUSB_ERROR_PIPE:
            {
                msgString = "Error: LIBUSB_ERROR_PIPE\nPipe error";
                break;
            }
            case LIBUSB_ERROR_INTERRUPTED:
            {
                msgString = "Error:LIBUSB_ERROR_INTERRUPTED\nSystem call interrupted (perhaps due to signal)";
                break;
            }
            case LIBUSB_ERROR_NO_MEM:
            {
                msgString = "Error: LIBUSB_ERROR_NO_MEM\nInsufficient memory";
                break;
            }
            case LIBUSB_ERROR_NOT_SUPPORTED:
            {
                msgString = "Error: LIBUSB_ERROR_NOT_SUPPORTED\nOperation not supported or unimplemented on this platform";
                break;
            }
            case LIBUSB_ERROR_OTHER:
            {
                msgString = "Error: LIBUSB_ERROR_OTHER\nOther error";
                break;
            }
            default:
            {
                msgString =  "Error: unkown error";
                break;
            }
        }
        theLogger->LogMessage(msgString, usbIswListIndex, std::chrono::system_clock::now());
    }
}

//!#####################################################################
//! LogErrorString()
//! Inputs: errString - Error description string
//!         err - integer representing a libusb error level
//! Description: This is for printing libusb errors to the ISW Library
//!              Logger and logFile
//!######################################################################
void UsbInterface::LogErrorString(std::string errString, int err)
{
    if ( theLogger->DEBUG_UsbInterface == 1 )
    {
        char buff[20];
        snprintf(buff, sizeof(buff), "%d", err);
        errString.append(" error = ");
        errString.append(buff);
        errString.append("\n");
        theLogger->LogMessage(errString, usbIswListIndex, std::chrono::system_clock::now());
    }
}
