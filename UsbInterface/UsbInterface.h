//!################################################
//! Filename: UsbInterface.h
//! Description: Class that provides an interface
//!              to the Linux libusb
//!              for threadsafe I/O on a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef USBINTERFACE_H
#define USBINTERFACE_H

#include "../SerialInterface/SerialInterface.h"
#include "../IswInterface/Interface.h"
#include "../Logger/Logger.h"
#include <stdint.h>
#include <iostream>
#include <string>
#include <list>
#include <cstdio>
#include <libusb-1.0/libusb.h>

//! Used for logging libusb errors
#define USB_LOGFILE_ERR     -2

//!Match libusb log levels
#define LOG_LEVEL_NONE      0   //! no messages ever printed by the library (default)
#define LOG_LEVEL_ERROR     1   //! error messages are printed to stderr
#define LOG_LEVEL_WARNING   2   //! warning and error messages are printed to stderr
#define LOG_LEVEL_INFO      3   //! informational messages are printed to stderr
#define LOG_LEVEL_DEBUG     4   //! debug and informational messages are printed to stderr

#ifdef TESTING
//! Use TESTING flag for unit tests
//! Unit testing doesnt acces HW
#define ACCESS public
#else
#define ACCESS protected
#endif

class UsbInterface : public Interface
{

public:
    UsbInterface(long **device, Logger *logger, uint8_t board_type, void *boardData);
    ~UsbInterface();

    //! gets USB attributes from libusb
    void UpdateFromDevice(libusb_context *ctx, libusb_device *device, libusb_device_descriptor *descriptor, uint8_t index);
    void GetUsbAttributes(libusb_device *device);

    //! Cleans up everything to exit
    void UsbInterfaceExit(void);

    //! Open as Linux device
    int OpenDevice(void);

    //! Close the Linux device
    int CloseDevice(void);

    //! Claim Linux device from the kernel
    int ClaimInterface(void);

    //! USB data transfer methods
    //! USB Setup and Control Message
    int SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                           uint16_t wValue, uint16_t wIndex,
                           std::string *data, uint16_t wLength,
                           unsigned int ctr_timeout);

    //! Asynchronous or non-blocking USB bulk transfer
    int AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback);

    //! Synchronous or blocking USB bulk transfer
    int SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length);

    //! Read from the USB device
    int ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead);

    //! gets USB attributes from libusb
    uint16_t GetVendorId(void) { return (idVendor); }
    uint16_t GetProductId(void) { return (idProduct); }
    libusb_device *GetDevice(void) { return (dev); }
    libusb_device_descriptor * GetDeviceDescriptor(void) { return (desc); }
    libusb_device_handle * GetDeviceHandle(void) { return (devHandle); }
    libusb_context *GetContext(void) { return (context); }
    std::list<libusb_config_descriptor *> *GetConfigList(void) { return (&configDescriptors); }

    //! Used to determine if the device is open
    bool IsDeviceOpen(void) { return (deviceOpen); }

    // Used with SerialInterface devices
    void SetdDeviceOpen(bool devOpen) { deviceOpen = devOpen; }

    //! USB headers - they are here due
    //! to being specific to the ISW vendor
    static const uint8_t USB_UNDEFINED_HEADER = 0;
    static const uint8_t USB_HEADER_TRU       = 1;
    static const uint8_t USB_HEADER_CDC       = 2;
    uint8_t GetUsbHeaderType(void) { return (usbHeaderType); }

    enum usbBoardTypes: uint8_t
    {
        usbBoard        = 0,
        serialBoard     = 1,
        parallelBoard   = 2,
        externalBoard   = 3
    };
    uint8_t GetBoardType(void) { return (boardType); }

    //! Get or set the index into the usbInterfaceArray of devices
    uint8_t GetIndex(void) { return (usbIswListIndex); }
    void SetIndex(uint8_t index) { usbIswListIndex = index; }

    //! Set class variables for attributes read in from libusb
    //! USB enumeration
    void SetDeviceDescriptor(libusb_device_descriptor *descriptor) { desc = descriptor; }
    void SetDeviceHandle(libusb_device_handle *deviceHandle) { devHandle = deviceHandle; }
    void SetContext(libusb_context *thisContext) { context = thisContext; }
    void SetVendorId(uint16_t vendorId) { idVendor = vendorId; }
    void SetProductId(uint16_t productId) { idProduct = productId; }

    //! Get/Set USB BULK transfer endpoint IN and OUT
    uint16_t GetEndpointIN(void) { return (USB_ENDPOINT_IN); }
    uint16_t GetEndpointOUT(void) { return (USB_ENDPOINT_OUT); }
    void SetEndpointIN(uint16_t endpoint) { USB_ENDPOINT_IN = endpoint; }
    void SetEndpointOUT(uint16_t endpoint) { USB_ENDPOINT_OUT = endpoint; }

    //! Get/Set max packet for BULK in and out as read from the libusb
    //! device attributes
    uint16_t GetMaxPacketSizeIn(void) { return (maxPacketSize_IN); }
    uint16_t GetMaxPacketSizeOut(void) { return (maxPacketSize_OUT); }
    void SetMaxPacketSizeIn(uint16_t maxPacketSize) { maxPacketSize_IN = maxPacketSize; }
    void SetMaxPacketSizeOut(uint16_t maxPacketSize) { maxPacketSize_OUT = maxPacketSize; }

    //! libusb can take a max data size 4064 regardless of device max packet
    //! size, so we use this for sending ISW SolNet data
    uint16_t GetMaxLibUsbPktSize(void) { return (maxLibUsbPktSize); }

    SerialInterface *GetSerialInterface(void) { return serialInterface; }

ACCESS:
    //! Used to log libusb error messages to ISW Library Logging
    void SetDebugLevel(int log_level);
    void LogError(int error);
    void LogErrorString(std::string errString, int err);

ACCESS:
    //! There are four or more threads running for every device
    //! so we use locks to prevent race conditions
    std::mutex usbCmdLock;
    std::mutex usbCmdSendLock;
    std::mutex usbCmdReceiveLock;

    uint8_t  usbHeaderType = USB_UNDEFINED_HEADER;

    //! libusb values
    libusb_device *dev              = nullptr;
    libusb_device_descriptor *desc  = nullptr;
    libusb_device_handle *devHandle = nullptr;
    libusb_context *context         = nullptr;
    std::list<libusb_config_descriptor *> configDescriptors;
    unsigned int timeout            = 3000;     //! Connection timeout (in ms) -- Default 3000

    //! Device attributes
    uint8_t boardType = usbBoard; //default
    uint16_t idVendor;
    uint16_t idProduct;

    SerialInterface *serialInterface = nullptr;

    //! Debugging
    int debugLevel                   = LOG_LEVEL_NONE;

    //! Used to determine if the device is open or closed
    bool deviceOpen                  = false;

    //! Internal index for logging
    //! Matches the array index
    uint8_t usbIswListIndex          = 0;

    //! Defaults - these get set when IswInterface reads the ISW hardware
    static const uint8_t LIBUSB_VENDOR_SPECIFIC_DATA = 255;

    uint16_t USB_ENDPOINT_IN   = (LIBUSB_ENDPOINT_IN  | LIBUSB_TRANSFER_TYPE_BULK);
    uint16_t USB_ENDPOINT_OUT  = (LIBUSB_ENDPOINT_OUT | LIBUSB_TRANSFER_TYPE_BULK);
    uint16_t maxPacketSize_IN  = 512;
    uint16_t maxPacketSize_OUT = 512;

    //! libusb default
    int maxLibUsbPktSize = 4064;
};

#endif //! USBINTERFACE_H
