var struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication =
[
    [ "condition", "struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication.html#a7b97edbb1f4baf214dceeb34cc14888c", null ],
    [ "dataflowId", "struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication.html#ac4308366ae43bf062c39e2ace4df1528", null ],
    [ "endpointNumber", "struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication.html#ac09b1cffdec407135c7d7fdf5984d544", null ],
    [ "reserved1", "struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication.html#a06b45cf6cbe9a35a40873f3670cd73b4", null ],
    [ "serviceSelector", "struct_isw_sol_net_1_1isw_sol_net_report_dataflow_condition_indication.html#aeb207f10168bbffe23991aea2bc5854c", null ]
];