var struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor =
[
    [ "direction", "struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor.html#aa6fedb7805a2e17ce582753ba3220ea9", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor.html#ab971ffa8665bc6e29d719edb56dda930", null ],
    [ "mcastIpAddress", "struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor.html#ae59e5499ca4f7f3e61beef0f192fab1b", null ],
    [ "reserved", "struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor.html#a7be40f1bac761325609998accf958322", null ],
    [ "udpPort", "struct_isw_sol_net_1_1isw_sol_net_property_udp_port_descriptor.html#a013ae9f9c9146a67d8027d8d99da8cab", null ]
];