var struct_isw_link_1_1_isw_get_background_scan_parameters_event =
[
    [ "fluxScanDuration", "struct_isw_link_1_1_isw_get_background_scan_parameters_event.html#ac0a17756f727bb9f3763dfd7780a3625", null ],
    [ "fluxScanFrequency", "struct_isw_link_1_1_isw_get_background_scan_parameters_event.html#af3029c78fb913540d0a582d4d75102ee", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_get_background_scan_parameters_event.html#aeddcb46543c8d9c049a5f10e2a764151", null ],
    [ "stableScanFrequency", "struct_isw_link_1_1_isw_get_background_scan_parameters_event.html#a8e31564af09f294b3432df1290f0d718", null ]
];