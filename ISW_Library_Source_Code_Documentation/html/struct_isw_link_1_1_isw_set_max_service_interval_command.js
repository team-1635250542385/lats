var struct_isw_link_1_1_isw_set_max_service_interval_command =
[
    [ "cmdContext", "struct_isw_link_1_1_isw_set_max_service_interval_command.html#aac8aeda5cef555887ebd6d38e49f8356", null ],
    [ "cmdType", "struct_isw_link_1_1_isw_set_max_service_interval_command.html#a98256b432bbc1b3f76773abef685176d", null ],
    [ "command", "struct_isw_link_1_1_isw_set_max_service_interval_command.html#a4b0319f91a2db2ec0357b7b45180a323", null ],
    [ "maxServiceInterval", "struct_isw_link_1_1_isw_set_max_service_interval_command.html#a2724b24a43c2f8d9a3fc0cd0179f57c3", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_set_max_service_interval_command.html#ac615debcb1d9f9da693a95b1bca318b7", null ]
];