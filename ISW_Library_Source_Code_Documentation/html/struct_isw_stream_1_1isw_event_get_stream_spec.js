var struct_isw_stream_1_1isw_event_get_stream_spec =
[
    [ "maxTxLatency", "struct_isw_stream_1_1isw_event_get_stream_spec.html#a7cf0d9b61b8022c77da6e927923dcb93", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_event_get_stream_spec.html#a44c784029e76a83f103029c39cb4e563", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_event_get_stream_spec.html#ae0ed5aa48e5bcddc764c37861b579159", null ],
    [ "subStreamTxDiscardable", "struct_isw_stream_1_1isw_event_get_stream_spec.html#a432150d4744a4df9a355424051778609", null ],
    [ "subStreamTxPriority", "struct_isw_stream_1_1isw_event_get_stream_spec.html#a27bff0b7e5f795c3d2195e39d33ac1a8", null ]
];