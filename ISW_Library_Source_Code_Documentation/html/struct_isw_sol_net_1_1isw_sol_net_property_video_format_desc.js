var struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc =
[
    [ "encodingFormat", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#a06d9090d2e439fa7d6eb55d92ce92114", null ],
    [ "formatOptions", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#ab5d801cbd5d7b63807fa83cfcd8baf75", null ],
    [ "frameRate", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#ae5fdb2cb06a0d0166a1ae2ab6f0b34ae", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#a35ba15eb7eb214a279409a5f0e646597", null ],
    [ "reserved", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#a04788e4597b0fe21000726c031b69f3d", null ],
    [ "videoFormatX", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#a7f5db5d27d7342f46deee119f4eefaba", null ],
    [ "videoFormatY", "struct_isw_sol_net_1_1isw_sol_net_property_video_format_desc.html#af8bee0cd23ce3e57ef030e1058ac0e34", null ]
];