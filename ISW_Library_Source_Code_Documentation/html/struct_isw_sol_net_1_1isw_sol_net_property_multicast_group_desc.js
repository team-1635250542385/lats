var struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc =
[
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#ae579e9e5af6b002f501aff551335464b", null ],
    [ "mcastAddress", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#a4ddc7c8df4a904323227b203a08090e0", null ],
    [ "mcastGroupId", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#af4b9fd062f1e0c126ba7393eea526da5", null ],
    [ "mcastMacAddress", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#ad78c5e16189daae3b6fde7769e6d9a1d", null ],
    [ "reserved1", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#adb43d07c3d33af54ac6d49ef4d4b292e", null ],
    [ "reserved2", "struct_isw_sol_net_1_1isw_sol_net_property_multicast_group_desc.html#a81f5cc9be1e8d42cd94d0e93cda4b872", null ]
];