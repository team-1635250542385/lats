var struct_isw_link_1_1_isw_get_antenna_configuration_command =
[
    [ "cmdContext", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#a39f50f872a1dfb4bd4b2a17b6fb2f7b7", null ],
    [ "cmdType", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#a38254945138f8ae08859a693e012cb13", null ],
    [ "command", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#af8e8ee299727240092b1bc9bc1891e79", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#ad63a468bb61210073eec157fee64baef", null ],
    [ "reserved2", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#ac27393cf49b51557697f897afc84fdda", null ],
    [ "switchThresholdAdder", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#a795cd8dc96cf2c53bec2aa7d8eff0d0b", null ],
    [ "triggerAlgorithm", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#ab8bf85de1a44534c1d8fba0364792b63", null ],
    [ "triggerPhyRateThreshold", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#a52850719e4cc40541e50467ee3184bdf", null ],
    [ "triggerRssiThreshold", "struct_isw_link_1_1_isw_get_antenna_configuration_command.html#aad1eb7be7cd0c9132e6270208b18419b", null ]
];