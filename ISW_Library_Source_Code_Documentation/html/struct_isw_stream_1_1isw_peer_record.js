var struct_isw_stream_1_1isw_peer_record =
[
    [ "connectionTimestampMicrosecs", "struct_isw_stream_1_1isw_peer_record.html#a87642473736a243fd64e8783f34b681b", null ],
    [ "inUse", "struct_isw_stream_1_1isw_peer_record.html#a4062de44929d6e578b90732411434815", null ],
    [ "isConnected", "struct_isw_stream_1_1isw_peer_record.html#a5512ae2112b842cdef153068dc316677", null ],
    [ "iswPeerRecord1", "struct_isw_stream_1_1isw_peer_record.html#adeb50278f6ea3c8b85bd857170482780", null ],
    [ "iswPeerRecord2", "struct_isw_stream_1_1isw_peer_record.html#afa51908d60c68f6a75727b61238d5266", null ],
    [ "iswPeerRecord3", "struct_isw_stream_1_1isw_peer_record.html#a212fd6a3f7991ce78a1d3ca63d03a740", null ],
    [ "iswPeerRecord4", "struct_isw_stream_1_1isw_peer_record.html#af46d0525b8b10bc4d380181e0ca033ad", null ],
    [ "metrics", "struct_isw_stream_1_1isw_peer_record.html#aefd10caa7d69a839ae672c11453bf646", null ],
    [ "recordVersion", "struct_isw_stream_1_1isw_peer_record.html#a4baaf28cc40f407d20d4e393f4de87e4", null ],
    [ "securityKey", "struct_isw_stream_1_1isw_peer_record.html#ae5fb809671b6eeec950ab5edf8b27df2", null ]
];