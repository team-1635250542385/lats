var struct_isw_stream_1_1isw_peer_record___version2 =
[
    [ "deviceType", "struct_isw_stream_1_1isw_peer_record___version2.html#ac91def1a76fde1f85a41aa223c5d24ab", null ],
    [ "linkQuality", "struct_isw_stream_1_1isw_peer_record___version2.html#a6bcedd405ca8128b73884d351ffececa", null ],
    [ "linkStatus", "struct_isw_stream_1_1isw_peer_record___version2.html#ac41399a29911470fba6dc1de8d19c9ee", null ],
    [ "macAddress", "struct_isw_stream_1_1isw_peer_record___version2.html#a1691387bcd24d5f800750e2a2c41b75a", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_peer_record___version2.html#a4ad1f6ed38e78c8765bd4b7a0d27616e", null ],
    [ "peerState", "struct_isw_stream_1_1isw_peer_record___version2.html#a5dbe14ebc3210f0a21092aa64c00c568", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_peer_record___version2.html#ab89ab3aa743f32ab3150f6788f6eb4fc", null ],
    [ "txQueueDepth", "struct_isw_stream_1_1isw_peer_record___version2.html#a17b336b825f23e236cf8289d7ffb2066", null ]
];