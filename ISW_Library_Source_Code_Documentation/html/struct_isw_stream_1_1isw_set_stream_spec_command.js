var struct_isw_stream_1_1isw_set_stream_spec_command =
[
    [ "cmdContext", "struct_isw_stream_1_1isw_set_stream_spec_command.html#af4b119c3cc1bb997a3869c591636283a", null ],
    [ "cmdType", "struct_isw_stream_1_1isw_set_stream_spec_command.html#a4d7200512d7c4a04854053c020d96aee", null ],
    [ "command", "struct_isw_stream_1_1isw_set_stream_spec_command.html#a9e0d19df43bb822d4d04becff267b5db", null ],
    [ "maxTxLatency", "struct_isw_stream_1_1isw_set_stream_spec_command.html#ad2818e32be31d6b790a085adc7dc0ef5", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_set_stream_spec_command.html#a8183118835ef415da36b7293f73e7789", null ],
    [ "reserved1", "struct_isw_stream_1_1isw_set_stream_spec_command.html#ac9e6bcd9377248922d445209195a22ed", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_set_stream_spec_command.html#a0f5da3bd14a63715f9c1d30d15bd2ca3", null ],
    [ "subStreamTxDiscardable", "struct_isw_stream_1_1isw_set_stream_spec_command.html#a86f26047e89b436798b82d681b542089", null ],
    [ "subStreamTxPriority", "struct_isw_stream_1_1isw_set_stream_spec_command.html#af47e836a47d515ef6c7b4dea93da66d2", null ]
];