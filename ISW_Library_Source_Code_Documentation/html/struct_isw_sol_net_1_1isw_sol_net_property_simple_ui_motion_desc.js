var struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc =
[
    [ "flags", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#a4387fe9f56c8fd86f21f584efad73bce", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#ae7808c3bb9aa86d753b47bc5d7a19ee8", null ],
    [ "terminalId", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#a091b09820791d29a035ce266a2ec0709", null ],
    [ "type", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#a8f348762075dbb279255ec3731c5d9a9", null ],
    [ "xMax", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#ad869dcfb01541bc2a9c4165e2155ea8a", null ],
    [ "xMin", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#ad85689b21bec0caddd2246e87dbbfcb8", null ],
    [ "yMax", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#afab1f55b42a97c54a74b686748aec4ab", null ],
    [ "yMin", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#a849fe6ba8fb47c268ac5a5f07cb6caae", null ],
    [ "zMax", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#a16d220a7feb5a1d2038ca2462a86980f", null ],
    [ "zMin", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_motion_desc.html#ab7829848aae09df34544373d9aa0bce8", null ]
];