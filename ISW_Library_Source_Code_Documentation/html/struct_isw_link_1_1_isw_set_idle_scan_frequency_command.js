var struct_isw_link_1_1_isw_set_idle_scan_frequency_command =
[
    [ "cmdContext", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#ab4dd534d968b29ad157501506b3055a8", null ],
    [ "cmdType", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#ab72a87317dbafd0d5fc06461809ffb11", null ],
    [ "command", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#abeeebe560381a6c8dbc10eaa399b137d", null ],
    [ "idleScanFrequency", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#a3bdf086ba3cd8187dd6778a6f43102ab", null ],
    [ "persist", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#a7b852ed84559eeb799a52b146aae435d", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_set_idle_scan_frequency_command.html#a2276e98210759f059e7542db35a540d0", null ]
];