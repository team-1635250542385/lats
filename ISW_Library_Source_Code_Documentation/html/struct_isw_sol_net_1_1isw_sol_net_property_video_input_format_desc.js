var struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc =
[
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc.html#a7bb6ad7dd6be84cd4822164cf7a10452", null ],
    [ "maximumRate", "struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc.html#a4e12ad8cf435d381c9346086daae83f0", null ],
    [ "minimumRate", "struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc.html#ae7d41eb9adb20ee58861ea6e1aca0ed7", null ],
    [ "supportedFormats", "struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc.html#a1a02d05953fd136346dd1762981186e3", null ],
    [ "supportedResolutions", "struct_isw_sol_net_1_1isw_sol_net_property_video_input_format_desc.html#af45a3a8db11b05fefb9df3a1467e18a9", null ]
];