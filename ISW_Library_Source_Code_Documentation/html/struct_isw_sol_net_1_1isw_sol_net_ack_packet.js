var struct_isw_sol_net_1_1isw_sol_net_ack_packet =
[
    [ "dataflowId", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#a1c13ac7e98d82aa8e0cf33f9a457caed", null ],
    [ "endpoint", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#ae7c9adbbf27961ec6680687b12c3b91a", null ],
    [ "protocolClass", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#a1c32e030f4def6d50039821ea8dc36a4", null ],
    [ "reserved", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#aebc62c7b32eec76bdace60e96702dec9", null ],
    [ "seqNumber", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#ad58666f2041f8980b97598b0dceef18a", null ],
    [ "status", "struct_isw_sol_net_1_1isw_sol_net_ack_packet.html#acffa04081a22e07130afd5cc2d45aee8", null ]
];