var struct_isw_stream_1_1isw_modify_multicast_group_command =
[
    [ "cmdContext", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a2077ebcc95c347cb9a6633c69485c303", null ],
    [ "cmdType", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a320c6be94835e4901e795252750b34b7", null ],
    [ "command", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a38a8583d1f719e22dfd490a216fe25cb", null ],
    [ "mcastPeerIndex", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a712dbbf4d39a25c0f9346c16aca54d48", null ],
    [ "mcastPhyRate", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a7f734416966d5b12ed6948d567db910e", null ],
    [ "reserved1", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#a76e9289bc037b26f06f81ae96edebc0d", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_modify_multicast_group_command.html#ad7f6564cf22896e988f7231bf8d89f2a", null ]
];