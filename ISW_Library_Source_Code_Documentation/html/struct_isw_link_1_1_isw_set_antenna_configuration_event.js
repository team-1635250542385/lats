var struct_isw_link_1_1_isw_set_antenna_configuration_event =
[
    [ "enable", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#abf10a6e07417a70370ef54b61ee4f73a", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#a23804f120929bb73ec996414fe9d740e", null ],
    [ "switchThresholdAdder", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#ac8674fbf22ddcd80ff4f60e6270b7921", null ],
    [ "triggerAlgorithm", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#a85f540375f9dca38b0f4051a3061c31a", null ],
    [ "triggerPhyRateThreshold", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#af3736d94985a9e3c7925647ace476c42", null ],
    [ "triggerRssiThreshold", "struct_isw_link_1_1_isw_set_antenna_configuration_event.html#a11cd7147959052330627d73b32a2068f", null ]
];