var struct_isw_identity_1_1isw_set_coordinator_command =
[
    [ "cmdContext", "struct_isw_identity_1_1isw_set_coordinator_command.html#adc90ddd78e91b2c2fdbb76a13d4ba15b", null ],
    [ "cmdType", "struct_isw_identity_1_1isw_set_coordinator_command.html#a1da8a36becc44632d995640eafcccd90", null ],
    [ "command", "struct_isw_identity_1_1isw_set_coordinator_command.html#a4dba81d457280b217024b314ae319db7", null ],
    [ "coordinatorCapable", "struct_isw_identity_1_1isw_set_coordinator_command.html#a75f2d7f58ca45f51b188d34faf53781e", null ],
    [ "coordinatorPrecedence", "struct_isw_identity_1_1isw_set_coordinator_command.html#a426886d6a043bb7d04fe0bfffcb936de", null ],
    [ "persist", "struct_isw_identity_1_1isw_set_coordinator_command.html#ae594d6fadc581b0a25db8f340d821e13", null ],
    [ "reserved1", "struct_isw_identity_1_1isw_set_coordinator_command.html#a6f3a63d3e63d369af4bee1b927f545f3", null ],
    [ "reserved2", "struct_isw_identity_1_1isw_set_coordinator_command.html#af8599e9b1fe96cc663b277a1ddef3032", null ]
];