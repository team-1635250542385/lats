var struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple =
[
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple.html#a306203d932b52a74d550bb9ae6c891de", null ],
    [ "quaternionI", "struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple.html#a8dd7dcf312c8a3b2d0b0f04815686f30", null ],
    [ "quaternionJ", "struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple.html#a071e6da7f68192ebd8a60194f9e077e7", null ],
    [ "quaternionK", "struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple.html#a5df869b80fcefef0dede6e2bbf191488", null ],
    [ "quaternionReal", "struct_isw_sol_net_1_1isw_sol_net_rta_imu_data_tuple.html#aa5943d9a98df843d8d78f79246ea7718", null ]
];