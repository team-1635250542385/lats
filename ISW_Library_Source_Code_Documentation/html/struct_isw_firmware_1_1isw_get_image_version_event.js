var struct_isw_firmware_1_1isw_get_image_version_event =
[
    [ "apiVersion", "struct_isw_firmware_1_1isw_get_image_version_event.html#abddbf323627d01f3536f120bd61280f5", null ],
    [ "reserved", "struct_isw_firmware_1_1isw_get_image_version_event.html#a6af5f5ec6b83452967869507c5011ebc", null ],
    [ "versionNumber", "struct_isw_firmware_1_1isw_get_image_version_event.html#af3173caec8ad3904da6c483cbb5b1507", null ],
    [ "versionString", "struct_isw_firmware_1_1isw_get_image_version_event.html#a9455f15d50d129cf36a97a8cadee9bff", null ]
];