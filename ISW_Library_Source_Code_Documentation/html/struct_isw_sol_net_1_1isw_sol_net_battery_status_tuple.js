var struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple =
[
    [ "batteryCharging", "struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple.html#a16434f3a469a74637fb398890c322b21", null ],
    [ "batteryId", "struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple.html#a353a4992793f6eb4cc710ec5825819d0", null ],
    [ "batteryLevel", "struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple.html#a44ff77b7d870b67fc1a34b81dafc6daf", null ],
    [ "batteryState", "struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple.html#a0202f9c2177800668d2664937b47ec6b", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_battery_status_tuple.html#a45a4c679277a2c8ef71f5829664d5de6", null ]
];