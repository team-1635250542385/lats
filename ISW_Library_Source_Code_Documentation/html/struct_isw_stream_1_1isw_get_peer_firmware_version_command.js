var struct_isw_stream_1_1isw_get_peer_firmware_version_command =
[
    [ "cmdContext", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#a25d57f94e2126175a4fe421a03f834dd", null ],
    [ "cmdType", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#a45f89e152579b7a1392163e108bec93a", null ],
    [ "command", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#a9841b329564ec0f885aa1c5d09e4feb5", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#a7fa6fd8d621260a4c64e68b0b18cc86a", null ],
    [ "reserved1", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#abacdf1bc4e64d7d0762165e7ef0d1c85", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_get_peer_firmware_version_command.html#a20c9f5f6cd88fc3efb9ceeb37459813d", null ]
];