var struct_isw_sol_net_1_1isw_sol_net_service_entry =
[
    [ "dataflowId", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a9f6d6a7dd027616ccd0f0a8354f5d392", null ],
    [ "dataPolicies", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#af018b08c5b84dc4c2b57f68378d76185", null ],
    [ "endpointDistribution", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a10ca696194bb5b95aca7f5524f673421", null ],
    [ "endpointId", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#af93788406b5fbe8e3bf86846af72a016", null ],
    [ "generationId", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a7b22cb4304d0979a54fbd6631439a83f", null ],
    [ "inUse", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a9d1a7e60891fd07a542215feb9ea8519", null ],
    [ "nextSendDataSeqNumber", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#affcf6393058e254b802129ebc0e8f4b7", null ],
    [ "pollDataCallbackFn", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#ac2f9a9cb5900dfd6f87737ca7ea108ee", null ],
    [ "recCallbackThisPtr", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a380a86a9ac35535c812951c049ef04f1", null ],
    [ "receiveCallbackFn", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#ae5a5825b50c5df420765ee871d9c7065", null ],
    [ "registeredPeers", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#afc7261c0ce3fd2f552cd33f3b9d91103", null ],
    [ "serviceControlDesc", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a7a4b2b278cc7cdde6347a54ef809cd8a", null ],
    [ "serviceDesc", "struct_isw_sol_net_1_1isw_sol_net_service_entry.html#a6170d3ae31adb097c5e9098f6cc69c8b", null ]
];