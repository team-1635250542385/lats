var struct_isw_product_1_1isw_set_hibernation_mode_command =
[
    [ "cmdContext", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a5799168a0ad0610504fca999f65a3f5b", null ],
    [ "cmdType", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a1b2a2db9b6c2d638f0b739c62987506e", null ],
    [ "command", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a2122416f9a30b331d6c08db2a0688a2b", null ],
    [ "hibernationMode", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a80b31012d933953b7d586135346be020", null ],
    [ "idleTime", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#ae3f6154120ac6757961fdf347b6da14b", null ],
    [ "persist", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#acc05c2a475e9f6586c03cc2f64a1e699", null ],
    [ "reserved1", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a4b4246c14d941612bd71276653df18ea", null ],
    [ "reserved2", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a772ba57b72df47be750cf88114c3d3b5", null ],
    [ "softHibernationTime", "struct_isw_product_1_1isw_set_hibernation_mode_command.html#a3ca6fb5d3943bd3af1cd62be7b00459a", null ]
];