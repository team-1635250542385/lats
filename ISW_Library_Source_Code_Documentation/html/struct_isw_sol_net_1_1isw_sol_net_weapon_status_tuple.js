var struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple =
[
    [ "activeReticleId", "struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple.html#a3aa9a8f753cb2cf2fae3c72c0b435160", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple.html#ae421d046ac73f40d84d56f0447116260", null ],
    [ "reserved", "struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple.html#a0577cf0f43bbfb6e8b2c76410831c695", null ],
    [ "symbologyState", "struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple.html#a61297b19afca51f7e8247924805fc2c3", null ],
    [ "weaponSightMode", "struct_isw_sol_net_1_1isw_sol_net_weapon_status_tuple.html#a69332eff24589722361c46fb6d340415", null ]
];