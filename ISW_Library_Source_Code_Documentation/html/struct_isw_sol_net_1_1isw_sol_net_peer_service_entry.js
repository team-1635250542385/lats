var struct_isw_sol_net_1_1isw_sol_net_peer_service_entry =
[
    [ "autonomy", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ab3bf08aa940d783cb7b24cf86918759a", null ],
    [ "dataflowId", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#a9a3722e00f736bdc090a38e6369c8707", null ],
    [ "dataPolicies", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#afe98a777a42b4de2b266aae74f7a1aa5", null ],
    [ "endpointDistribution", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ac24f3dd193150f481d235acdc8444c85", null ],
    [ "endpointId", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ad0cf368c73a3979a4a7dc617ffe48710", null ],
    [ "generationId", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ab39bffbfc3450c0fefe3b6474d5ba4c0", null ],
    [ "ImRegistered", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#a3684a952abf4b640375b6c971b83acb2", null ],
    [ "inUse", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#a72802adaa46bd19fdf4061e05248549b", null ],
    [ "nextSendDataSeqNumber", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#acc33a85e9c40d7d98cd22fc24ccc9469", null ],
    [ "recCallbackThisPtr", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ad21f21b72509de4c399a37d6bcb50690", null ],
    [ "receiveCallbackFn", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#a909dc1b566abf3e2697b6b6e08a1da49", null ],
    [ "serviceControlDesc", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#a6874a174e6a8b370019a4516de1297fd", null ],
    [ "serviceDesc", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#ac6154bff3e8ddccca22f997c23ca198c", null ],
    [ "status", "struct_isw_sol_net_1_1isw_sol_net_peer_service_entry.html#adbd57b5fca44afb680d0f42e289bc818", null ]
];