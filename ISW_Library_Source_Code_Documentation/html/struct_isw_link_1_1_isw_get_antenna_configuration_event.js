var struct_isw_link_1_1_isw_get_antenna_configuration_event =
[
    [ "enable", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#adb0a2d4aa4182baecd0cfc851734fea9", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#a04baf896b6e6e99dc47331af8da0ea89", null ],
    [ "switchThresholdAdder", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#adc72e1125f09ae6cff84329cf2241a09", null ],
    [ "triggerAlgorithm", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#aedfabbba4e6173144fe8224833289bae", null ],
    [ "triggerPhyRateThreshold", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#a2d38e62961d525c5f309827764547a75", null ],
    [ "triggerRssiThreshold", "struct_isw_link_1_1_isw_get_antenna_configuration_event.html#a54026dfb862053767872441502ef9d5e", null ]
];