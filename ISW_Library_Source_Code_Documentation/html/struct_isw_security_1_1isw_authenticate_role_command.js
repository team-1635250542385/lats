var struct_isw_security_1_1isw_authenticate_role_command =
[
    [ "cmdContext", "struct_isw_security_1_1isw_authenticate_role_command.html#a4ffd68344c4c6a720264853714c13f38", null ],
    [ "cmdType", "struct_isw_security_1_1isw_authenticate_role_command.html#a4807d271f38f53f3bee654974ce2af6d", null ],
    [ "command", "struct_isw_security_1_1isw_authenticate_role_command.html#a1817c949c13cb77987269e42e26cfce5", null ],
    [ "password", "struct_isw_security_1_1isw_authenticate_role_command.html#adeb4fee7e38d7c512bc23a8df09c0fd3", null ],
    [ "reserved1", "struct_isw_security_1_1isw_authenticate_role_command.html#a57ac6ddd4e01318448f97085cc837159", null ],
    [ "reserved2", "struct_isw_security_1_1isw_authenticate_role_command.html#a2617e929a7fa60b8e4d7417b48ca2049", null ],
    [ "role", "struct_isw_security_1_1isw_authenticate_role_command.html#af224a0e8655cce789c54ac5fd444aa0c", null ]
];