var struct_isw_stream_1_1isw_peer_record___version3 =
[
    [ "deviceType", "struct_isw_stream_1_1isw_peer_record___version3.html#ac2f108f90e6f546dadcb48aec595a62a", null ],
    [ "linkQuality", "struct_isw_stream_1_1isw_peer_record___version3.html#a1f7945a3515cf264a267b5bb5fffc101", null ],
    [ "linkStatus", "struct_isw_stream_1_1isw_peer_record___version3.html#af3e5fa89347e9359aa7fa5da0e1b03d6", null ],
    [ "macAddress", "struct_isw_stream_1_1isw_peer_record___version3.html#a08054f1ae8a7211e691540873680b31c", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_peer_record___version3.html#a61c0a95d7894d47a5e265ebf42ebce5e", null ],
    [ "peerState", "struct_isw_stream_1_1isw_peer_record___version3.html#af9257f83c65915fa0188c338ba51d2a7", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_peer_record___version3.html#a830836211bcb4100395acc610ccda526", null ],
    [ "running", "struct_isw_stream_1_1isw_peer_record___version3.html#a1ad73a6fb47852cbfb689dc4a4ada4cf", null ],
    [ "throughput", "struct_isw_stream_1_1isw_peer_record___version3.html#aee9435d37ac1d7f07e13c0490d3606e1", null ],
    [ "transferSize", "struct_isw_stream_1_1isw_peer_record___version3.html#a736baed1c0a5f060997cc97331122f82", null ],
    [ "txQueueDepth", "struct_isw_stream_1_1isw_peer_record___version3.html#a5af30d2ad59cba4a8114f1053014336d", null ]
];