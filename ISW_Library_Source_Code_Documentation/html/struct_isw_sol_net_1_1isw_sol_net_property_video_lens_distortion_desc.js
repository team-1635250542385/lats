var struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc =
[
    [ "distortionCoefficientK1", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#acb7d20bd46b45b8ba78b1f22908a2dfe", null ],
    [ "distortionCoefficientK2", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#a80575ccd2344af707799308553a88edd", null ],
    [ "distortionCoefficientK3", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#a1f60b57dfbbf6dd50c5a92709713a4df", null ],
    [ "distortionCoefficientP1", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#ae6948eeaf36f9d885ad357670c7862e1", null ],
    [ "distortionCoefficientP2", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#a7bdb6d7727a4e577a2ebde60523eff1a", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_video_lens_distortion_desc.html#af41a81cff7ffed8058a42c5963e3a5db", null ]
];