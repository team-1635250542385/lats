var struct_isw_link_1_1_isw_set_background_scan_parameters_command =
[
    [ "cmdContext", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#a64aa69f755f1cf6e314fe1f6186ac7d7", null ],
    [ "cmdType", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#acd7d3124dfc5542ba089c0882cd58e54", null ],
    [ "command", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#ad1d947cc4bd500f7532ab2350811ae0c", null ],
    [ "fluxScanDuration", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#a87f1f7610810ebb9fe8d654cd2b5fd53", null ],
    [ "fluxScanFrequency", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#a856c86372da02a83a71bfb316f8ef8e2", null ],
    [ "persist", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#acdb5eccaa905416dd8fcb62d60d2c995", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#ac1813bcdf0e68d8d7812d19fb9d7f79a", null ],
    [ "stableScanFrequency", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#aea7225b6a0018281606f45d8e491493b", null ],
    [ "version", "struct_isw_link_1_1_isw_set_background_scan_parameters_command.html#ad0d6579ebb055c01839057afdfc10a09", null ]
];