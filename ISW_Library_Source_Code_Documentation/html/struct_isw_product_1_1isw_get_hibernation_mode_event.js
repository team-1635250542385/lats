var struct_isw_product_1_1isw_get_hibernation_mode_event =
[
    [ "hibernationMode", "struct_isw_product_1_1isw_get_hibernation_mode_event.html#ab39044911853150b4a7a2d140b8507ca", null ],
    [ "idleTime", "struct_isw_product_1_1isw_get_hibernation_mode_event.html#aaa19b98f5f3ab0c268c677cc7e6eb05e", null ],
    [ "reserved1", "struct_isw_product_1_1isw_get_hibernation_mode_event.html#a3fdbccdcbfd4072f31d6290962dae5b7", null ],
    [ "reserved2", "struct_isw_product_1_1isw_get_hibernation_mode_event.html#a71a319249dd117d4d3281d7d1702d67b", null ],
    [ "softHibernationTime", "struct_isw_product_1_1isw_get_hibernation_mode_event.html#aabc7f72257a49409b9b83f485885574e", null ]
];