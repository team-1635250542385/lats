var struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update =
[
    [ "eventCount", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#adb88020b6f84d185318797bb32c9d842", null ],
    [ "eventId1", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#a49c6d48cfe294ef8d2fcaadf1a12a44f", null ],
    [ "eventId2", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#aea25977a094a0546cf63dbf798a81e58", null ],
    [ "eventId3", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#a82faac39223542c34cc2ad3524c98a34", null ],
    [ "reserved1", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#ad1a88b5162b2cd92e8f6daa57719359a", null ],
    [ "terminalId", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#a3e6fbfc5bfb6e966460c60daafff683f", null ],
    [ "updateType", "struct_isw_sol_net_1_1isw_sol_net_property_simple_ui_extended_event_map_update.html#ae65e722512c0cb74758dc042a009cf7d", null ]
];