var struct_isw_product_1_1isw_observer_mode_set_command =
[
    [ "cmdContext", "struct_isw_product_1_1isw_observer_mode_set_command.html#ab587834a37249d3b5ff290417240e2b3", null ],
    [ "cmdType", "struct_isw_product_1_1isw_observer_mode_set_command.html#a68f45427d61bb87b2d3b39c39f6dc4ed", null ],
    [ "command", "struct_isw_product_1_1isw_observer_mode_set_command.html#aa22158af4c23839a2a9eb105619b70a4", null ],
    [ "devType", "struct_isw_product_1_1isw_observer_mode_set_command.html#a7d07ee4d7d2ad772e40338febef56184", null ],
    [ "enable", "struct_isw_product_1_1isw_observer_mode_set_command.html#a0b3a1b8af60e4949282a8d28e40cc77b", null ],
    [ "phyRateCap", "struct_isw_product_1_1isw_observer_mode_set_command.html#a726f9d0f683e7230457c45bf49601966", null ],
    [ "reserved1", "struct_isw_product_1_1isw_observer_mode_set_command.html#a10e3643f75fe44a7f2318754bd905a0e", null ],
    [ "reserved2", "struct_isw_product_1_1isw_observer_mode_set_command.html#ad16247f85c2e5748c3f8be6185a227b9", null ],
    [ "rssiConnect", "struct_isw_product_1_1isw_observer_mode_set_command.html#aac9ed215f7bf4da6c98298171680c15e", null ],
    [ "rssiDisconnect", "struct_isw_product_1_1isw_observer_mode_set_command.html#a77729c7a4ed768385680d3119987ff89", null ]
];