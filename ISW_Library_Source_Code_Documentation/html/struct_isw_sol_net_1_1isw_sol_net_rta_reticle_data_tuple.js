var struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple =
[
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a2ae7376b23d47a9e78a200c72815be6f", null ],
    [ "rtaReticleColorBlue", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#afc52136e0894f02173ebd05ee294b05c", null ],
    [ "rtaReticleColorGreen", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a55b9085e0976573c4520a01c1ff9c9c7", null ],
    [ "rtaReticleColorRed", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a8e4e62e2d297d7891402bbdd6b717c71", null ],
    [ "rtaReticleType", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a13e2430d20884f7c088882904e234b88", null ],
    [ "rtaReticleXOffset", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a8ae192d21f7d19b6116a486284e14296", null ],
    [ "rtaReticleYOffset", "struct_isw_sol_net_1_1isw_sol_net_rta_reticle_data_tuple.html#a0282e06a4f7a9e64e4081040ec389461", null ]
];