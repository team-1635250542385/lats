var struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple =
[
    [ "frameColumnNumber", "struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple.html#a57eb53bdd3f532d75c7760d6a7377e01", null ],
    [ "frameNumber", "struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple.html#a12b574b3b604d1f847105d309a2a2135", null ],
    [ "frameRowNumber", "struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple.html#a4bbff39ef14ea56dc5d1befb5188c1e9", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple.html#a330e6f7afc086e272447f64b543fb2f9", null ],
    [ "packetCount", "struct_isw_sol_net_1_1isw_sol_net_video_frame_raw_metadata_tuple.html#a57cec8a4f499eea05223443e2acff3bf", null ]
];