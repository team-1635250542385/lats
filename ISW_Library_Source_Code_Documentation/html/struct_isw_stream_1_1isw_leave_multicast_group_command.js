var struct_isw_stream_1_1isw_leave_multicast_group_command =
[
    [ "cmdContext", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#acbad39e799b6b7258772616dc6ccfd95", null ],
    [ "cmdType", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#a3490158eb8e2daf6a7dea51680015961", null ],
    [ "command", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#ac45c59d615ded44bb8ad8b49693ed3a3", null ],
    [ "mcastPeerIndex", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#ac083b9f400050914d314c26b81b04419", null ],
    [ "reserved1", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#a7d15305903912c317367f0edc583569d", null ],
    [ "reserved2", "struct_isw_stream_1_1isw_leave_multicast_group_command.html#a36e059c96cc3b6d694bcec99a67893eb", null ]
];