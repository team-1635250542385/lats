var struct_isw_stream_1_1peer_metrics =
[
    [ "phyRate", "struct_isw_stream_1_1peer_metrics.html#a12e6dd88f16e4dfc84efc39556f4a4fd", null ],
    [ "receiveThroughput", "struct_isw_stream_1_1peer_metrics.html#a79f1b0fdb13bdd8fd0d3eef68bf97050", null ],
    [ "rssi", "struct_isw_stream_1_1peer_metrics.html#ab8c89c0c8db8c2f06494fcdace5399a4", null ],
    [ "transmitThroughput", "struct_isw_stream_1_1peer_metrics.html#a84300373144b32df910c3373b7e79945", null ]
];