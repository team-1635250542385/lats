var struct_isw_link_1_1_isw_set_antenna_configuration_command =
[
    [ "cmdContext", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#aa3488f0493af440c90cfe10a93b1727d", null ],
    [ "cmdType", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a8343970d89fe29df0215d691366817cd", null ],
    [ "command", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a0e98a28b74ad9abff87287ed655fdf7e", null ],
    [ "enable", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a81f84146466034cd6b85179b594e0378", null ],
    [ "reserved1", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a39aee4ef3773b2b1393aae693626e9d8", null ],
    [ "reserved2", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a29bb17c3ca6d3f35237e607f8180349b", null ],
    [ "switchThresholdAdder", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a0d8e18aca177c5c81dfbb6269c5d355d", null ],
    [ "triggerAlgorithm", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a00b84fa65bff9c07fd121f7a7b6da3d4", null ],
    [ "triggerPhyRateThreshold", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#a80a6d8f4632e21070f02076c060a9854", null ],
    [ "triggerRssiThreshold", "struct_isw_link_1_1_isw_set_antenna_configuration_command.html#aa5fdbec28492b9872e20a9bd1441f22b", null ]
];