var struct_isw_stream_1_1isw_multicast_group_record =
[
    [ "mcastAddress", "struct_isw_stream_1_1isw_multicast_group_record.html#aa25289204728bbd443c4483c38285be6", null ],
    [ "mcastPeerIndex", "struct_isw_stream_1_1isw_multicast_group_record.html#a10d99f88fbaaed4985871b37fd06d6df", null ],
    [ "mcastPhyRate", "struct_isw_stream_1_1isw_multicast_group_record.html#a842cfefd0a1f7f15bda5736dbcd19970", null ],
    [ "mcastStatus", "struct_isw_stream_1_1isw_multicast_group_record.html#a5cb888e2feb22e6345e43c45d77b9dfa", null ],
    [ "ownerPeerIndex", "struct_isw_stream_1_1isw_multicast_group_record.html#a50ca6bef59ed05d889a05ff7c5236ca0", null ],
    [ "txQueueDepth", "struct_isw_stream_1_1isw_multicast_group_record.html#a790f8cbc3e869c9146c8cb34e706d0a7", null ]
];