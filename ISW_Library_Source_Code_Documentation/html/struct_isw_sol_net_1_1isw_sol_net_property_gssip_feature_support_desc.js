var struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc =
[
    [ "count", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#a5daa17145ac181eabcc05b4132844cad", null ],
    [ "encodingFormat", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#a993222bd2fb1a71e41f5fe3975757e22", null ],
    [ "filter", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#ac584eb59986078a05451ba370603c462", null ],
    [ "gssipRevision", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#a750ee1692418ae9f32f96f6f57ed68a7", null ],
    [ "header", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#a903c66c3d7be5dd1a747117416b87064", null ],
    [ "messageId", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#a77cd919da76a1882434d9b3738c5b55d", null ],
    [ "reserved1", "struct_isw_sol_net_1_1isw_sol_net_property_gssip_feature_support_desc.html#aaa4b0fa557012a11ab926bf27654b5b6", null ]
];