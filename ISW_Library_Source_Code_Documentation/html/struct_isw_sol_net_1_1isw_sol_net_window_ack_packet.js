var struct_isw_sol_net_1_1isw_sol_net_window_ack_packet =
[
    [ "dataflowId", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#ad8b0fe8bd89d08f03897973b4e4bc741", null ],
    [ "endpoint", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#a9191c3faee40e4b24469126df8b723c6", null ],
    [ "protocolClass", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#a882c50c74aefcbcb987f464e9cd0293e", null ],
    [ "reserved", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#a97a7aa3507f7a137ad111739e5607a4c", null ],
    [ "seqNumber", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#a655f62f5999753e582e5c5b1537415ed", null ],
    [ "status", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#a1ea2050488e7b12a337f0679edbea271", null ],
    [ "windowBitmap", "struct_isw_sol_net_1_1isw_sol_net_window_ack_packet.html#ab4bea7a54774b3295a854216e3b7fc01", null ]
];