var struct_isw_metrics_1_1isw_peer_metrics =
[
    [ "peerIndex", "struct_isw_metrics_1_1isw_peer_metrics.html#a3d0bd755db4936e5dbcc925d9ec8609d", null ],
    [ "phyRate", "struct_isw_metrics_1_1isw_peer_metrics.html#ac8a4936649892eb86629dfa317403355", null ],
    [ "receiveThroughput", "struct_isw_metrics_1_1isw_peer_metrics.html#ae4a7871c52450609efaad4c9f637652f", null ],
    [ "reserved", "struct_isw_metrics_1_1isw_peer_metrics.html#ab4dcaf73ad3968912402fbf7cfdbe622", null ],
    [ "rssi", "struct_isw_metrics_1_1isw_peer_metrics.html#a87757004bce41f6bcc9c505ecfecf8cf", null ],
    [ "transmitThroughput", "struct_isw_metrics_1_1isw_peer_metrics.html#acfe9d140b39de1fcae8a524f6cc7b85e", null ]
];