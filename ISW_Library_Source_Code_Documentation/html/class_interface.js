var class_interface =
[
    [ "Interface", "class_interface.html#a6d055a7f130cf8ac6a6b4398ae121ff0", null ],
    [ "~Interface", "class_interface.html#a67eca71a4ef8d28dc959dd495e2b2b59", null ],
    [ "AsyncWriteDevice", "class_interface.html#a4119b10ac71a3b0bc681b8586f30b293", null ],
    [ "ClaimInterface", "class_interface.html#a4617695be5255572d9ae7e69fab3e94d", null ],
    [ "CloseDevice", "class_interface.html#a635a2b127c163dbc885e56f032a94645", null ],
    [ "OpenDevice", "class_interface.html#a5120b009c1c77ca184594d27a7759c83", null ],
    [ "ReadDevice", "class_interface.html#a217ff28076b5bbe84d0f89a5db915a53", null ],
    [ "SendControlMessage", "class_interface.html#aa0a8e4df15bc35badb6887d5c5ab873e", null ],
    [ "SyncWriteDevice", "class_interface.html#a7fd3a09ff6ff1ec1a1fe77583946c4a5", null ],
    [ "theLogger", "class_interface.html#a89a3e38dd1ae358ee090b42226c3630d", null ]
];