var struct_isw_stream_1_1isw_event_streams_connection_ind =
[
    [ "deviceType", "struct_isw_stream_1_1isw_event_streams_connection_ind.html#ad5855d476bf335a45e2425034bfb6649", null ],
    [ "linkStatus", "struct_isw_stream_1_1isw_event_streams_connection_ind.html#a1382bbb8b0a7725224f22989b8a44da5", null ],
    [ "macAddress", "struct_isw_stream_1_1isw_event_streams_connection_ind.html#a0ab1d47d08ea3d2da314f939dfce052a", null ],
    [ "peerIndex", "struct_isw_stream_1_1isw_event_streams_connection_ind.html#a24fd64fb765e864d31d028a52648884b", null ],
    [ "reserved", "struct_isw_stream_1_1isw_event_streams_connection_ind.html#a32b2227b7b26fe794e1bf2df0e6e9dd6", null ]
];