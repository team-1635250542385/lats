//!###############################################
//! Filename: powersupply.h
//! Description: Class that provides an
//!              interface to the Keysight Digital Multimeters
//!              measuring the voltage and current to calculate the Power
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#ifndef POWERSUPPLY_H
#define POWERSUPPLY_H
#include <QString>
#include "socketscpi.h"
#include "../Logger/Logger.h"

class PowerSupply
{
public:
    PowerSupply();
    PowerSupply(QString ipAddr1);  // Logger *logger
    ~PowerSupply();
    void connect();
    void disconnect();
    void init(double voltage, double current);
    void on(uint8_t channel);
    void off(uint8_t channel);
    double getPower();

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

private:
    QString device;
    QString ipAddr;
    int port;
    SocketScpi* scpi = nullptr;
};

#endif // POWERSUPPLY_H
