//!###############################################
//! Filename: dmm.cpp
//! Description: Class that provides an
//!              interface to the Keysight Digital Multimeters
//!              measuring the voltage and current to calculate the Power
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "dmm.h"
#include "socketscpi.h"

Dmm::Dmm()
{
//    device = "Keysight 34461A";
    device = "SIGLENT SDM3045X";
    ipAddr = "169.254.4.61";
    port   = 5025;
    scpi   = new SocketScpi(ipAddr, port);
}

Dmm::Dmm(QString ipAddr1)
{
//    device = "Keysight 34461A";
    device = "SIGLENT SDM3045X";
    ipAddr = ipAddr1;
    port   = 5025;
    scpi   = new SocketScpi(ipAddr, port);
    qDebug() << ipAddr1 << "Is Connecting";
}

Dmm::~Dmm()
{
    if ( scpi != nullptr )
        delete (scpi);
}

void Dmm::connect()
{
    scpi->connect();
}

void Dmm::disconnect()
{
    scpi->disconnect();
}

double Dmm::getVolt()
{
    scpi->send("MEAS:VOLT:DC? 10\n");
    return (scpi->recv());
}

double Dmm::getCurrent()
{
    scpi->send("MEAS:CURR:DC? 1\n");
    return (scpi->recv());
}

void Dmm::configureTemperature()
{
    scpi->send("CONF:TEMP THER,JITS90\n"); // "CONF:TEMP THER,KITS90\n"
}

double Dmm::getTemp()
{
    scpi->send("MEAS:TEMP? THER,JITS90\n"); // "MEAS:TEMP? THER,KITS90\n"
    return (scpi->recv());
}
