//!###############################################
//! Filename: node.h
//! Description: Class that creates an object within
//!              the network topology every time a physical device
//!              joins the network.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef NODE_H
#define NODE_H

#include <QGraphicsItem>
#include <QList>
#include <QGraphicsView>
#include <QPushButton>
#include <QtWidgets>
#include "NetTopologyGraphicsScene.h"

class Edge;

class QGraphicsSceneMouseEvent;

class Node : public QGraphicsItem
{
public:
    Node(QGraphicsView *graphWidget, NetTopologyGraphicsScene *netScene);
    ~Node();

    void setNodeColor(QColor);
    void setLabelWidget(QGraphicsProxyWidget *proxyWidget) { labelWidget = proxyWidget; }
    QGraphicsProxyWidget *getLabelWidget(void) { return labelWidget; }
    NetTopologyGraphicsScene *getScene(void) { return netScene; }

    void clearAllEdges(void);
    void addEdge(Edge *edge);
    Edge *findEdge(Node *source, Node *dest);
    void removeEdge(Edge *edge);
    QList<Edge *> edges() const;

    enum { Type = UserType + 1 };
    int type() const override { return Type; }

    void calculateForces();
    bool advancePosition();

    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QGraphicsView *netGraphicsView = nullptr;
    NetTopologyGraphicsScene *netScene = nullptr;
    QList<Edge *> edgeList;
    QPointF newPos;
    QColor nodeColor;
    QGraphicsProxyWidget *labelWidget;
    QMutex edgeListLock;
};

#endif // NODE_H
