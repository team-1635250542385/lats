//!###############################################
//! Filename: antennaid.h
//! Description: Class that provides an interface
//!              for the user to select
//!              an antenna on
//!              a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ANTENNAID_H
#define ANTENNAID_H

#include <QDialog>

namespace Ui {
class AntennaId;
}

class AntennaId : public QDialog
{
    Q_OBJECT

public:
    explicit AntennaId(QWidget *parent = nullptr);
    ~AntennaId();

    uint8_t getAntennaId();

private slots:
    void on_defaultAntennaRadioButton_clicked();
    void on_alternativeAntenna1RadioButton_clicked();
    void on_alternativeAntenna2RadioButton_clicked();

private:
    Ui::AntennaId *ui;
    uint8_t antennaId = 0x00;
};

#endif // ANTENNAID_H
