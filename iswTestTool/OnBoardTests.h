//!##################################################
//! Filename: onBoardTests.h
//! Description: Class that provides an interface
//!              to access the on board tests that
//!              run on some test boards from the
//!              vendor.  This class allows the user
//!              of iswTestTool to configure and start
//!              a test from one board to another.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef OnBoardTests_H
#define OnBoardTests_H

#include <QDialog>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QGridLayout>
#include <QTableWidget>
#include <pthread.h>
#include "iswtesttool.h"


namespace Ui {
class OnBoardTests;
}

class iswTestTool;

class OnBoardTests : public QDialog
{
    Q_OBJECT

public:
    explicit OnBoardTests(iswTestTool *testToolPtr, void *userData, QWidget *parent = nullptr);
    ~OnBoardTests(void);

    //! Used to determine the state of the on board test
    bool isTestRunning(void) { return onboardTestsRunning; }
    QButtonGroup *buttonGroup = nullptr;

public slots:
    void onGroupButtonClicked(int id);

    //! Connected to the user button to start the test
    void startDataTest();

    //! Connected to the user button to stop the test
    void stopDataTest();

private:
    //! Sets up the pop up window/table for user
    //! configuration and start/stop of the test
    void createDataTestTable(void);

    //! Pops up a window with a message for the user
    void displayMessage(QString string);

    void writeToCsv(qint64 timestamp, uint8_t rssi, uint8_t txMbps, uint8_t rxMbps);

private:
    Ui::OnBoardTests *ui;
    QPushButton *startTestPushButton = nullptr;
    QPushButton *stopTestPushButton = nullptr;

    iswTestTool *iswToolPtr = nullptr;
    bool onboardTestsRunning = false;

    CommonButtonParameters *cbp = nullptr;

    static const int START_BUTTON_ID = 1;
    static const int STOP_BUTTON_ID  = 2;

    QPushButton clearButton;
    QTableWidgetItem providerColumn;
    QTableWidgetItem subscriberColumn;
    QTableWidgetItem packetSizeColumn;
    QTableWidgetItem throughputColumn;
    QTableWidgetItem rssiColumn;
    QTableWidgetItem txMbpsColumn;
    QTableWidgetItem rxMbpsColumn;
    QTableWidgetItem PERColumn;

    QFuture<void> checkThroughputThread;
    static void getThroughput(OnBoardTests *thisPtr);

    //! IswInterface pointers to the source and destination
    //! inside the ISW Library
    IswInterface *providerIswIntrfcPtr   = nullptr;
    IswInterface *subscriberIswIntrfcPtr = nullptr;
    IswSolNet *providerIswSolNetPtr      = nullptr;
    OnBoard *onBoard                     = nullptr;
};

#endif // OnBoardTests_H
