//!##########################################################
//! Filename: setbackgroundscanparameters.h
//! Description: Class that provides an dialog
//!              that allows the user to configure
//!              the background scan parameters for
//!              the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef SETBACKGROUNDSCANPARAMETERSDIALOG_H
#define SETBACKGROUNDSCANPARAMETERSDIALOG_H

#include <QDialog>

namespace Ui {
class SetBackgroundScanParametersDialog;
}

class SetBackgroundScanParametersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetBackgroundScanParametersDialog(QWidget *parent = nullptr);
    ~SetBackgroundScanParametersDialog();
    uint16_t getStableScanFrequency() const;
    uint8_t getVersion() const;
    uint16_t getFluxScanDuration() const;
    uint16_t getFluxScanFrequency() const;

private slots:
    void on_versionComboBox_activated(int index);

private:
    Ui::SetBackgroundScanParametersDialog *ui;

    uint16_t stableScanFrequency = 60;  //! 60 - Default Value
    uint8_t version              = 0;   //! Default Value
    uint16_t fluxScanDuration    = 20;  //! 20 - Default Value
    uint16_t fluxScanFrequency   = 300; //! Default Value
};

#endif // SETBACKGROUNDSCANPARAMETERSDIALOG_H
