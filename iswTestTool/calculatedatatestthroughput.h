//!###############################################
//! Filename: calculatedatatestthroughput.h
//! Description: Class that provides an interface
//!              for the user to calculate the
//!              broadcast/multicast data throughput
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#ifndef CALCULATEDATATESTTHROUGHPUT_H
#define CALCULATEDATATESTTHROUGHPUT_H

//! Project Includes
#include "../Logger/Logger.h"

//! Qt Includes
#include <QMap>
#include <QString>

class CalculateDataTestThroughput
{

public:
    explicit CalculateDataTestThroughput(int testId, Logger* theLogger, Logger* throughputTestLogger, Logger* throughputRestultsLogger);
    void calculateThroughput(int testId);
    void markStartDataTestTimeInLog(int testId);
    void markEndDataTestTimeInLog(int testId);

protected:

    std::string loggerFilename            = "";
    std::string throughputTestFilename    = "";
    std::string throughputResultsFilename = "";
    Logger *loggerPtr                     = nullptr;
    Logger *throughputTestLoggerPtr       = nullptr;
    Logger *throughputResultsLoggerPtr    = nullptr;
    int testId                            = 0;
    long testDuration                     = 0;
    long totalPayload                     = 0;
    int throughput                        = 0;
    int deviceId                          = 0;
    QString dataResultStr                 = "";

    void outputThroughput(long startTime, long endTime, long payload);
    void fullyDequeue(Logger *loggerPtr);
};

#endif // CALCULATEDATATESTTHROUGHPUT_H
