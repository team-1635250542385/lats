//!#####################################################
//! Filename: nettopologygraphicsscene.h
//! Description: Custom QGraphicsScene class for
//!             NetTopologyGraphicsScene
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef NETTOPOLOGYGRAPHICSSCENE_H
#define NETTOPOLOGYGRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

class NetTopologyGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:
    NetTopologyGraphicsScene(QObject *parent = nullptr);
    ~NetTopologyGraphicsScene() {};

    void setClicksEnabled(int enabled);
    int getClicksEnabled(void) { return clicksEnabled; }

public slots:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

private:
    QGraphicsScene *m_scene;
    int clicksEnabled;
};

#endif // NETTOPOLOGYGRAPHICSSCENE_H
