//!###############################################
//! Filename: socketscpi.cpp
//! Description: Class that sends the SCPI commands
//!              to the KeySight Digital Multimeters.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "socketscpi.h"
#include <QDebug>
#include <QTcpSocket>
#include <QDataStream>
#include "iswtesttool.h"
#include <QtMath>

SocketScpi::SocketScpi()
{

}

SocketScpi::SocketScpi(QString ipAddr, int port)
{
    socketIpAddr = ipAddr;
    socketPort = port;
    socket = nullptr;
    connectedToHost = false;
}

SocketScpi::~SocketScpi()
{
    if ( socket != nullptr )
        delete (socket);
}

void SocketScpi::connect()
{
    socket = new QTcpSocket();
    if(!connectedToHost)
    {
        socket->connectToHost(socketIpAddr, (uint16_t)socketPort);
        socket->waitForConnected(10000);

        if(socket->state() == QAbstractSocket::ConnectedState)
        {
            qDebug() << "Socket Connected";
            connectedToHost = true;
        }
        else
        {
            qDebug() << "Socket Connecting";
        }
    }
    else
    {
        qDebug() <<"Socket Not Connected";
    }
}

void SocketScpi::disconnect()
{
    if(connectedToHost)
    {
       socket->disconnectFromHost();
       socket->close();
       connectedToHost = false;
       qDebug() << "Socket Disconnected";
    }
    else
    {
        qDebug() << "Socket Not Connected";
    }
}

void SocketScpi::send(QString command)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        QString cmd = "DISPlay:TEXT \"Hello World\"\n";

        socket->write(command.toUtf8().data());
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);

        qDebug() << "reading" << socket->bytesAvailable();
    }
    else
    {
        qDebug() << "Socket Not Connected -- Can't Send Command";
    }
}

double SocketScpi::recv()
{
    double value = 0.0;
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        QString reply = socket->readAll();
        reply.chop(1);

        qDebug() << reply;

        value = reply.toDouble();
    }
    else
    {
        qDebug() << "Socket Not Connected -- Can't Send Command";
    }

    return value;
}
