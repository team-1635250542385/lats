//!##########################################################
//! Filename: plotchart.cpp
//! Description: Class that plots the data throughput
//!              and power measurements generated
//!              from the radios.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "plotchart_interactive.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QScatterSeries>
#include <QtCore/QRandomGenerator>
#include <QtCore/QDebug>
#include "../Logger/Logger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../IswInterface/IswInterfaceInit.h"
#include "../IswInterface/IswInterface.h"
#include "../IswInterface/IswIdentity.h"
#include "../IswInterface/IswFirmware.h"
#include "../IswInterface/IswAssociation.h"
#include "../IswInterface/IswLink.h"
#include "../IswInterface/IswMetrics.h"
#include "../IswInterface/IswSecurity.h"
#include "../IswInterface/IswProduct.h"
#include "../IswInterface/IswSolNet.h"
#include "iswtesttool.h"
#include <QFile>
#include <QTextStream>

PlotChart_interactive::PlotChart_interactive(QGraphicsItem *parent, Qt::WindowFlags wFlags, int updateInterval, int xMax, int yMax):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(0),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_step(0),
    m_counter(0),
    m_xMax(xMax),
    m_yMax(yMax),
    y_out(0),
    m_x(0),
    m_y(0),
    m_updateInterval(updateInterval)
{
    setPreferredSize(850,500);

    m_series = new QScatterSeries(this);
    m_series->setMarkerSize(10);
    m_series->setMarkerShape(QScatterSeries::MarkerShapeCircle);

    m_series->append(m_x, m_y);

    addSeries(m_series);
    legend()->hide();

    addAxis(m_axisX,Qt::AlignBottom);
    addAxis(m_axisY,Qt::AlignLeft);
    m_series->attachAxis(m_axisX);
    m_series->attachAxis(m_axisY);

    m_axisX->setRange(0, xMax);
    m_axisY->setRange(0, yMax);
}

PlotChart_interactive::~PlotChart_interactive()
{
    if ( m_axisX != nullptr )
        delete (m_axisX);

    if ( m_axisY != nullptr )
        delete (m_axisY);

    if ( m_series != nullptr )
        delete (m_series);
}

void PlotChart_interactive::startPlot()
{
    m_timer.start();
    setPlotFlag = 0;
}

void PlotChart_interactive::stopPlot()
{
    m_timer.stop();
    setPlotFlag = 1;
}

void PlotChart_interactive::setYreal(double dataYPoint)
{
    m_y = dataYPoint;
    y_out = dataYPoint;
    if( dataYPoint > m_yMax)
    {
        m_yMax = dataYPoint + 100;
        m_axisY->setRange(0, m_yMax);
    }
}

void PlotChart_interactive::setXreal(double dataXPoint)
{
    m_x = dataXPoint;
    if( dataXPoint > m_xMax)
    {
        m_xMax = dataXPoint + 100;
        m_axisX->setRange(0, m_xMax);
    }
}

void PlotChart_interactive::appendPoint()
{
    m_series->append(m_x, m_y);

}
