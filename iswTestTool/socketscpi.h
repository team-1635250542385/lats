//!###############################################
//! Filename: socketscpi.h
//! Description: Class that sends the SCPI commands
//!              to the KeySight Digital Multimeters.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef SOCKETSCPI_H
#define SOCKETSCPI_H
#include <QTcpSocket>

class SocketScpi
{

public:
    SocketScpi();
    SocketScpi(QString ipAddr, int port);
    ~SocketScpi();
    void checkError();
    void connect();
    void disconnect();
    double recv();
    void send(QString command);

private:
    QTcpSocket* socket   = nullptr;
    QString socketIpAddr = "";
    int socketPort       = 0;
    bool connectedToHost;
};

#endif // SOCKETSCPI_H
