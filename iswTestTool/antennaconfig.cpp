//!###############################################
//! Filename: antennaconfig.cpp
//! Description: Class that provides an interface
//!              for the user to configure
//!              antenna parameters on
//!              a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "antennaconfig.h"
#include "ui_antennaconfig.h"

AntennaConfig::AntennaConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AntennaConfig)
{
    ui->setupUi(this);
}

AntennaConfig::~AntennaConfig()
{
    delete ui;
}

uint8_t AntennaConfig::getEnable()
{
    enable = ui->enableDisableAntennaDiversityComboBox->currentIndex();
    return (enable);
}

uint8_t AntennaConfig::getTriggerAlgorithm()
{
    triggerAlgorithm = ui->triggerAlgorithmComboBox->currentIndex();
    return (triggerAlgorithm);
}

uint8_t AntennaConfig::getTriggerRssiThreshold()
{
    triggerRssiThreshold = ui->triggerRssiThresholdLineEdit->text().toUInt();
    return (triggerRssiThreshold);
}

uint8_t AntennaConfig::getTriggerPhyRateThreshold()
{
    triggerPhyRateThreshold = ui->triggerPhyRateThresholdLineEdit->text().toUInt();
    return (triggerPhyRateThreshold);
}

uint8_t AntennaConfig::getSwitchThresholdAdder()
{
    switchThresholdAdder = ui->switchThresholdAdderLineEdit->text().toUInt();
    return (switchThresholdAdder);
}

