//!###############################################
//! Filename: CommonButtonParameter.h
//! Description: Class that provides an common
//!              button
//!              antenna parameters on
//!              a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef COMMONBUTTONPARAMETERS_H
#define COMMONBUTTONPARAMETERS_H

#include <QModelIndex>
#include <QString>
#include "../IswInterface/IswSolNet.h"

struct CommonButtonParameters
{
    QModelIndex cindex;
    QString provider                        = "DEFAULT_PROVIDER";
    QString subscriber                      = "DEFAULT_SUBSCRIBER";
    uint8_t providerIndexInDynArray         = 0xFF;
    uint8_t subscriberIndexInDynArray       = 0xFF;
    uint8_t subscriberIswPeerIndex          = 0xFF;
    uint8_t providerIswPeerIndex            = 0xFF;
    uint8_t dataflowId                      = 0xFF;
    uint8_t endpointDistribution            = 0xFF;
    bool destIsRegistered                   = false;
    IswSolNet::iswSolNetServiceEntry *entry = nullptr;
    uint8_t endpointId                      = 1;
};
typedef struct CommonButtonParameters CommonButtonParameters;


#endif // COMMONBUTTONPARAMETERS_H
