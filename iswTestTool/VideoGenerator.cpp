//!####################################################
//! Filename: VideoGenerator.cpp
//! Description: Class that allows the user to setup,
//!              start, and stop a video test from the
//!              iswTestTool GUI. The tests are run
//!              from the Linux host to the firmare and
//!              across the wireless to another node and
//!              received at the Linux host on the other
//!              node.
//!              The user selects the test video input
//!              which can be from the camera or one of
//!              25 video test patterns.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "VideoGenerator.h"
#include "ui_VideoGenerator.h"
#include <unistd.h>
#include <QButtonGroup>
#include <QMessageBox>

//!########################################################
//! Constructor: videoGenerator class
//! Inputs: - pointer to the iswTestTool object
//!         - userData pointer to a CommonButtonParameters
//!         struct of information needed to send/receive
//!         data
//!         - parent widget
//!########################################################
VideoGenerator::VideoGenerator(iswTestTool *testToolPtr, void *userData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoGenerator),
    iswToolPtr(testToolPtr)
{
    ui->setupUi(this);
    setWindowTitle("Video Tests");

    cbp = (CommonButtonParameters *)userData;
    providerIswIntrfcPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswInterface;
    subscriberIswIntrfcPtr = iswToolPtr->DynArray[cbp->subscriberIndexInDynArray].iswInterface;
    providerIswSolNetPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswSolNet;

    if ( (providerIswIntrfcPtr == nullptr) ||
         (subscriberIswIntrfcPtr == nullptr) ||
         (providerIswSolNetPtr == nullptr) )
    {
        displayMessage("Provider or Subscriber device is not available - Test ending!");
        return;
    }

    //! Setup the control buttons: start, stop, cancel
    startTestPushButton = new QPushButton(this);
    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
    startTestPushButton->setText("Start");
    startTestPushButton->setGeometry(QRect(60, 585, 122, 28));
    startTestPushButton->setCheckable(true);
    startTestPushButton->setAutoExclusive(true);
    stopTestPushButton = new QPushButton(this);
    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
    stopTestPushButton->setText("Stop");
    stopTestPushButton->setGeometry(QRect(220, 585, 122, 28));
    stopTestPushButton->setCheckable(true);
    stopTestPushButton->setAutoExclusive(true);

    startTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");
    stopTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(startTestPushButton);
    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);
    buttonGroup->addButton(stopTestPushButton);
    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

    // Setup the video test subwindows
    grid = new QGridLayout;
    ui->topWidget->setLayout(grid);
    grid->addWidget(createVideoTestGroup(), 0, 0);

    ui->topWidget->update();
    QApplication::processEvents();
}

//!########################################################
//! Constructor: videoGenerator class
//! Used for playbook execution
//! Inputs: - pointer to the iswTestTool object
//!         - CBP contains device info
//!         - playbookPacketSize
//!         - playbookPacketNum
//!         - playbookDelay
//!         - parent widget
//!########################################################
VideoGenerator::VideoGenerator(iswTestTool *testToolPtr, CommonButtonParameters *passedCbp, int playbookDelay, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoGenerator),
    iswToolPtr(testToolPtr)
{
    ui->setupUi(this);
    setWindowTitle("Video Tests");

    cbp = passedCbp;
    providerIswIntrfcPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswInterface;
    subscriberIswIntrfcPtr = iswToolPtr->DynArray[cbp->subscriberIndexInDynArray].iswInterface;
    providerIswSolNetPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswSolNet;

    microsecondSleep   = playbookDelay;

    qDebug() << "set video delay: " << microsecondSleep;
    playbookActive    = true;

    if ( (providerIswIntrfcPtr == nullptr) ||
         (subscriberIswIntrfcPtr == nullptr) ||
         (providerIswSolNetPtr == nullptr) )
    {
        displayMessage("Provider or Subscriber device is not available - Test ending!");
        return;
    }

//    //! Setup the control buttons: start, stop, cancel
//    startTestPushButton = new QPushButton(this);
//    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
//    startTestPushButton->setText("Start");
//    startTestPushButton->setGeometry(QRect(60, 585, 122, 28));
//    startTestPushButton->setCheckable(true);
//    startTestPushButton->setAutoExclusive(true);
//    stopTestPushButton = new QPushButton(this);
//    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
//    stopTestPushButton->setText("Stop");
//    stopTestPushButton->setGeometry(QRect(220, 585, 122, 28));
//    stopTestPushButton->setCheckable(true);
//    stopTestPushButton->setAutoExclusive(true);

//    startTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");
//    stopTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");

//    buttonGroup = new QButtonGroup(this);
//    buttonGroup->addButton(startTestPushButton);
//    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);
//    buttonGroup->addButton(stopTestPushButton);
//    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

//    // Setup the video test subwindows
//    grid = new QGridLayout;
//    ui->topWidget->setLayout(grid);
//    grid->addWidget(createVideoTestGroup(), 0, 0);
//    ui->topWidget->update();
    QApplication::processEvents();
}

VideoGenerator::~VideoGenerator()
{
    if ( cbp != nullptr )
        delete (cbp);

    if ( startTestPushButton != nullptr )
        delete (startTestPushButton);

    if ( stopTestPushButton != nullptr )
        delete (stopTestPushButton);

    if ( buttonGroup != nullptr )
        delete (buttonGroup);

    if ( videoTestsGroupBox != nullptr )
    {
        if ( videoTestsGroupBox->layout() != NULL )
        {
            QLayoutItem* item;
            while ( ( item = videoTestsGroupBox->layout()->takeAt( 0 ) ) != NULL )
            {
                delete item->widget();
                delete item;
            }
            delete videoTestsGroupBox->layout();
        }

        delete (videoTestsGroupBox);
    }

    if ( grid != nullptr )
        delete (grid);

    if ( ui != nullptr )
        delete (ui);
}

//!##################################################
//! createVideoTestGroup()
//! Sets up the pop up window/table for user
//! configuration and start/stop of the test
//! Shows all the video choices
//!##################################################
QGroupBox *VideoGenerator::createVideoTestGroup()
{
    //! Show the user the provider and subscriber
    ui->videoTestTable->setShowGrid(true);
    ui->videoTestTable->setRowCount(1);
    ui->videoTestTable->setColumnCount(3);

    QStringList tableHeader;
    tableHeader << "Provider" << "Subscriber" << " Delay (uSecs) " ;
    ui->videoTestTable->setHorizontalHeaderLabels(tableHeader);
    ui->videoTestTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->videoTestTable->horizontalHeader()->setResizeContentsPrecision(true);

    uint8_t row = 0;
    providerColumn.setText(cbp->provider);
    ui->videoTestTable->setItem(row, 0, &providerColumn);

    subscriberColumn.setText(cbp->subscriber);
    ui->videoTestTable->setItem(row, 1, &subscriberColumn);

    microsecondSleepColumn.setText("250");
    ui->videoTestTable->setItem(row, 2, &microsecondSleepColumn);

    //! Show the user what tests they can send
    videoTestsGroupBox = new QGroupBox(tr("Select a Video Pattern"));

    pattern0Button = new QRadioButton(tr("Pattern 0  - SMPTE 100% color bars"));
    pattern1Button = new QRadioButton(tr("Pattern 1  - Snow"));
    pattern2Button = new QRadioButton(tr("Pattern 2  - Black"));
    pattern3Button = new QRadioButton(tr("Pattern 3  - White"));
    pattern4Button = new QRadioButton(tr("Pattern 4  - Red"));
    pattern5Button = new QRadioButton(tr("Pattern 5  - Green"));
    pattern6Button = new QRadioButton(tr("Pattern 6  - Blue"));
    pattern7Button = new QRadioButton(tr("Pattern 7  - Checkers-1"));
    pattern8Button = new QRadioButton(tr("Pattern 8  - Checkers-2"));
    pattern9Button = new QRadioButton(tr("Pattern 9  - Checkers-4"));
    pattern10Button = new QRadioButton(tr("Pattern 10 - Checkers-8"));
    pattern11Button = new QRadioButton(tr("Pattern 11 - Circular"));
    pattern12Button = new QRadioButton(tr("Pattern 12 - Blink"));
    pattern13Button = new QRadioButton(tr("Pattern 13 - SMPTE 75% color bars"));
    pattern14Button = new QRadioButton(tr("Pattern 14 - Zone plate"));
    pattern15Button = new QRadioButton(tr("Pattern 15 - Gamut checkers"));
    pattern16Button = new QRadioButton(tr("Pattern 16 - Chroma zone plate"));
    pattern17Button = new QRadioButton(tr("Pattern 17 - Solid color"));
    pattern18Button = new QRadioButton(tr("Pattern 18 - Moving ball"));
    pattern19Button = new QRadioButton(tr("Pattern 19 - SMPTE 100% color bars"));
    pattern20Button = new QRadioButton(tr("Pattern 20 - Bar"));
    pattern21Button = new QRadioButton(tr("Pattern 21 - Pinwheel"));
    pattern22Button = new QRadioButton(tr("Pattern 22 - Spokes"));
    pattern23Button = new QRadioButton(tr("Pattern 23 - Gradient"));
    pattern24Button = new QRadioButton(tr("Pattern 24 - Colors"));
    cameraButton    = new QRadioButton(tr("Camera"));

    pattern0Button->setChecked(false);
    pattern1Button->setChecked(false);
    pattern2Button->setChecked(false);
    pattern3Button->setChecked(false);
    pattern4Button->setChecked(false);
    pattern5Button->setChecked(false);
    pattern6Button->setChecked(false);
    pattern7Button->setChecked(false);
    pattern8Button->setChecked(false);
    pattern9Button->setChecked(false);
    pattern10Button->setChecked(false);
    pattern11Button->setChecked(false);
    pattern12Button->setChecked(false);
    pattern13Button->setChecked(false);
    pattern14Button->setChecked(false);
    pattern15Button->setChecked(false);
    pattern16Button->setChecked(false);
    pattern17Button->setChecked(false);
    pattern18Button->setChecked(false);
    pattern19Button->setChecked(false);
    pattern20Button->setChecked(false);
    pattern21Button->setChecked(false);
    pattern22Button->setChecked(false);
    pattern23Button->setChecked(false);
    pattern24Button->setChecked(false);
    cameraButton->setChecked(false);

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(pattern0Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern1Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern2Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern3Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern4Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern5Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern6Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern7Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern8Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern9Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern10Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern11Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern12Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern13Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern14Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern15Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern16Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern17Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern18Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern19Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern20Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern21Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern22Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern23Button, 0, Qt::AlignTop);
    vbox->addWidget(pattern24Button, 0, Qt::AlignTop);

    vbox->addWidget(cameraButton);
    vbox->setAlignment(Qt::AlignTop);

    videoTestsGroupBox->setLayout(vbox);

    return (videoTestsGroupBox);
}

//!##################################################
//! onGroupButtonClicked()
//! Input: Button Id
//! Combines Start and Stop buttons to be mutually
//! exclusive
//!##################################################
void VideoGenerator::onGroupButtonClicked(int id)
{
    qDebug() << "button clicked id: " << id;
    switch(id)
    {
        case START_BUTTON_ID:
        {
            qDebug() << "got button click for start";
            startVideoTest();
            break;
        }
        case STOP_BUTTON_ID:
        {
            qDebug() <<"got button click for stop";
            stopVideoTest(true, true);
            break;
        }
        default:
            break;
    }
}

//!##################################################
//! startVideoTest()
//! Starts the test when the Start button is pushed
//!##################################################
void VideoGenerator::startVideoTest()
{
//    if ( isRunning )
//    {
//        displayMessage("Stop current test first.");
//        return;
//    }

    isRunning        = true;
    if(!playbookActive)
    {
        microsecondSleep = microsecondSleepColumn.text().toInt();
    }

    qDebug() << "starting video gen";

    providerIswIntrfcPtr->SetFlushSendQueue(cbp->entry->endpointId, cbp->dataflowId, false);
    qDebug() << "set send flush queue";

    subscriberIswIntrfcPtr->SetFlushReceiveQueue(cbp->entry->endpointId, cbp->dataflowId, false);
    qDebug() << "set receive flush queue";

    // Setup Video Sender and Player
    uint8_t videoTestType     = 0;
    uint16_t videoFormatType  = 0;
    uint16_t videoPixelFormat = 0;

    if(!playbookActive)
    {
        if ( cameraButton->isChecked() )
        {
            // Camera test
            videoTestType = 30;
        }
        else
        {
            // One of the pattern tests
            // There are 0 - 24
            int buttonCount = 0;
            foreach (QRadioButton *button, videoTestsGroupBox->findChildren<QRadioButton*>())
            {
                if (button->isChecked())
                {
                    videoTestType = buttonCount;
                    break;
                }
                buttonCount++;
            }
        }
    }

    // We only have RAW format right now
    // and YV12 from webcam. For other
    // formats use IswSolNet::iswSolNetRawVideoPixelFormat enums
    videoFormatType  = IswSolNet::VideoFormat;
    videoPixelFormat = GstSender::PIXEL_FORMAT_YV12;

    // Setup for the GstSender to stream video data to iswTestTool
    gstSender = new GstSender(videoTestType, videoFormatType, videoPixelFormat);

    qDebug() << "gst sender created";

    connect(gstSender, SIGNAL(videoFromSourceAvailable(const QByteArray&)),
            this, SLOT(sendVideoToRadio(const QByteArray&)), Qt::DirectConnection);

    // Setup for the sink that plays video
    gstPlayer = new GstPlayer(videoTestType, videoFormatType, videoPixelFormat);

    qDebug() << "gstPlayer creater";

    // Connect data from iswTestTool to GstPlayer
    connect(this, SIGNAL(videoFramesAvailable(const QByteArray&)),
            gstPlayer, SLOT(receiveVideoDataInPlayer(const QByteArray&)), Qt::DirectConnection);

    sleep(1);

    // Debug on signals / slots
    //QObject::dumpObjectInfo();

    gstPlayerThread = QtConcurrent::run(gstPlayer, &GstPlayer::startReceiving);

    qDebug() << "running gst player";

    while ( !gstPlayer->isPlayerRunning() )
    {
        usleep(100);
    }

    gstSenderThread = QtConcurrent::run(gstSender, &GstSender::startSending);

    qDebug() << "running gst sender";
}

//!##################################################
//! stopVideoTest()
//! Stops the test when the Stop button is pushed
//!##################################################
void VideoGenerator::stopVideoTest(bool flushProviderQueue, bool flushSubscriberQueue)
{
    if ( isRunning == false )
    {
        return;
    }
    isRunning = false;

    qDebug() << "called stop test";

    if ( gstSender != nullptr )
    {
        qDebug() << "attempting to stop gst sender";
        gstSender->stopSending();

        while ( gstSender->isSenderRunning() )
        {
            qDebug() << "still not stopped";
            usleep(100);
        }

        gstSenderThread.cancel();
        gstSenderThread.waitForFinished();

        delete gstSender;
        gstSender = nullptr;


        qDebug() << "stopped gstSender";
    }

    if ( gstPlayer != nullptr )
    {
        gstPlayer->stopReceiving();

        while ( gstPlayer->isPlayerRunning() )
        {
            usleep(100);
        }

        gstPlayerThread.cancel();
        gstPlayerThread.waitForFinished();

        delete gstPlayer;
        gstPlayer = nullptr;
        qDebug() << "stopped gstPlayer";
    }

    // Remove all packets on the outgoing queue for this flow
    if ( flushProviderQueue == true )
    {
        providerIswIntrfcPtr->SetFlushSendQueue(cbp->entry->endpointId, cbp->dataflowId, true);
        qDebug() << "flushed provider queue";
    }

    // Remove all packets on the receiver's incoming queue for this flow
    if ( flushSubscriberQueue == true )
    {
        subscriberIswIntrfcPtr->SetFlushReceiveQueue(cbp->entry->endpointId, cbp->dataflowId, true);
        qDebug() << "flushed receive queue";
    }

    if(!playbookActive)
    {
        startTestPushButton->setChecked(false);
        stopTestPushButton->setChecked(false);
    }

}

//!########################################################
//! sendVideoToRadio()
//! Input: QByteArray
//! videoGenerator calls this to deliver data to iswTestTool
//! running in another thread for transmission over the
//! wireless network
//!########################################################
void VideoGenerator::sendVideoToRadio(const QByteArray& sourceData)
{
    if ( (sourceData.size() > 0) &&
         (providerIswSolNetPtr != nullptr) &&
         (providerIswSolNetPtr->IsSolNetStopped() == false) &&
         (isRunning) )
    {
        uint16_t blockSize = IswSolNet::payloadDataBufferSize;
        uint8_t policies   = 0;

        //! Have to copy here because we are in a slot connected
        //! to incoming data from GStreamer pipeline and
        //! passing to the outgoing network
        int dataSize = sourceData.size();
        uint8_t buffer[dataSize];
        memset(buffer, 0, dataSize);
        memcpy(buffer, (uint8_t *)(sourceData.data()), dataSize);

        int numBlocks = 1;
        if ( dataSize > blockSize )
        {
            numBlocks = dataSize / blockSize;

            if ( (dataSize % blockSize) > 0 )
            {
                numBlocks++;
            }

            int offset = 0;
            int bytesLeft = dataSize;
            for (int i=0; i<numBlocks; i++)
            {
                //! Last block
                if ( bytesLeft < blockSize )
                {
                    providerIswSolNetPtr->SendSolNetDataPacket(cbp->subscriberIswPeerIndex,
                                                               cbp->entry->endpointId,
                                                               cbp->entry->dataflowId,
                                                               policies,
                                                               (uint8_t *)(buffer + offset),
                                                               bytesLeft);
                    break;
                }
                else
                {
                    providerIswSolNetPtr->SendSolNetDataPacket(cbp->subscriberIswPeerIndex,
                                                               cbp->entry->endpointId,
                                                               cbp->entry->dataflowId,
                                                               policies,
                                                               (uint8_t *)(buffer + offset),
                                                               blockSize);
                    offset += blockSize;
                    bytesLeft -= blockSize;
                }
                usleep(microsecondSleep);

            }
        }

    }

}

//!################################################################
//! receiveVideoDataCallback()
//! Input: data - pointer
//!        transferSize - size of data
//! Receives data as a callback from IswSolNet::DeliverSolNetData()
//! data coming in from the wireless network
//!################################################################
void VideoGenerator::receiveVideoDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr)
{
    if ( transferSize > 0 )
    {
        VideoGenerator *videoGenPtr = (VideoGenerator *)thisPtr;

        QByteArray byteArray = QByteArray((const char*)data, transferSize);
        emit videoGenPtr->videoFramesAvailable(byteArray);
    }
}

//!######################################################
//! displayMessage()
//! Pops up a window with a message for the user
//!######################################################
void VideoGenerator::displayMessage(QString string)
{
    QMessageBox msgBox;
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);

    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(2000);

    msgBox.exec();
}
