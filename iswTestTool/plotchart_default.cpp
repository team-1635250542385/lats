//!##########################################################
//! Filename: plotchart.cpp
//! Description: Class that plots the data throughput
//!              and power measurements generated
//!              from the radios.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "plotchart_default.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QRandomGenerator>
#include <QtCore/QDebug>
#include "../Logger/Logger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../IswInterface/IswInterfaceInit.h"
#include "../IswInterface/IswInterface.h"
#include "../IswInterface/IswIdentity.h"
#include "../IswInterface/IswFirmware.h"
#include "../IswInterface/IswAssociation.h"
#include "../IswInterface/IswLink.h"
#include "../IswInterface/IswMetrics.h"
#include "../IswInterface/IswSecurity.h"
#include "../IswInterface/IswProduct.h"
#include "../IswInterface/IswSolNet.h"
#include "iswtesttool.h"
#include <QFile>
#include <QTextStream>

PlotChart_default::PlotChart_default(QGraphicsItem *parent, Qt::WindowFlags wFlags, int updateInterval, int xMax, int yMax, QString fileName):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(0),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_step(0),
    m_counter(0),
    m_xMax(xMax),
    m_yMax(yMax),
    y_out(0),
    m_x(0),
    m_y(0),
    m_updateInterval(updateInterval)
{
    QObject::connect(&m_timer, &QTimer::timeout, this, &PlotChart_default::handleTimeout);
    m_timer.setInterval(updateInterval);
    setPreferredSize(850,500);

    m_series = new QLineSeries(this);
    QPen black(Qt::black);
    black.setWidth(3);
    m_series->setPen(black);
    m_series->append(m_x, m_y);

    file.setFileName(fileName);

    outStream.setDevice(&file);

    addSeries(m_series);
    legend()->hide();

    addAxis(m_axisX,Qt::AlignBottom);
    addAxis(m_axisY,Qt::AlignLeft);
    m_series->attachAxis(m_axisX);
    m_series->attachAxis(m_axisY);

    m_axisX->setRange(0, xMax);
    m_axisY->setRange(0, yMax);
}

PlotChart_default::~PlotChart_default()
{
    if ( m_axisX != nullptr )
        delete (m_axisX);

    if ( m_axisY != nullptr )
        delete (m_axisY);

    if ( m_series != nullptr )
        delete (m_series);
}

void PlotChart_default::startPlot()
{
    m_timer.start();
    setPlotFlag = 0;
    file.open(QIODevice::WriteOnly);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        qDebug() << "Output file did not open";
        return;
    }
}

void PlotChart_default::stopPlot()
{
    m_timer.stop();
    setPlotFlag = 1;
    file.flush();
    file.close();
}

void PlotChart_default::setYreal(double dataYPoint)
{
    m_y = dataYPoint;
    y_out = dataYPoint;
    m_series->append(m_x, m_y);
}

void PlotChart_default::handleTimeout()
{
    m_counter += 1;
    m_x = m_counter*m_updateInterval/1000;
    m_y = getYreal();
    m_series->append(m_x, m_y);
    outStream << (double)m_x << "," << (double)y_out << "\n";
    writeToCsv(y_out);

   // qDebug() << m_y << "y value in Handle";
    if (m_y > m_yMax)
    {
        m_axisY->setMax(ceil(m_y));
        m_yMax = ceil(m_y);
    }
    if (m_x > m_xMax)
    {
        m_xMax = m_xMax + 20;
        m_axisX->setMax(m_xMax);
    }
}

void PlotChart_default::writeToCsv(uint16_t value) //
{
    QString filename = "power_data.csv";
    QFile file(filename);

    // std::to_string(std::chrono::system_clock::now())

    if (file.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream <<(QDateTime::currentMSecsSinceEpoch())<<"\t"<<value<<"\n";
    }

    file.flush();
    file.close();
}
