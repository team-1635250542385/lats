//!##########################################################
//! Filename: plotchart.h
//! Description: Class that plots the data throughput
//!              and power measurements generated
//!              from the radios.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#ifndef PLOTCHART_INTERACTIVE_H
#define PLOTCHART_INTERACTIVE_H

#include <QtCharts/QChart>
#include <QScatterSeries>
#include <QtCore/QTimer>
#include "../Logger/Logger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../UsbInterface/UsbInterface.h"
#include "../IswInterface/IswInterfaceInit.h"
#include "../IswInterface/IswInterface.h"
#include "../IswInterface/IswIdentity.h"
#include "../IswInterface/IswFirmware.h"
#include "../IswInterface/IswAssociation.h"
#include "../IswInterface/IswLink.h"
#include "../IswInterface/IswMetrics.h"
#include "../IswInterface/IswSecurity.h"
#include "../IswInterface/IswProduct.h"
#include "../IswInterface/IswSolNet.h"
#include <QFile>
#include <QTextStream>
#include <QDir>

QT_CHARTS_BEGIN_NAMESPACE
class QLineSeries;
class QValueAxis;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class PlotChart_interactive: public QChart
{
public:
    PlotChart_interactive(QGraphicsItem *parent = 0, Qt::WindowFlags wFlags = 0, int updateInterval = 1, int xMax = 100, int yMax = 80);
    void setYreal(double dataYPoint);
    void setXreal(double dataXPoint);
    void appendPoint();
    double getYreal() {return m_y;}
    double getXreal() {return m_x;}
    void startPlot();
    void stopPlot();
    virtual ~PlotChart_interactive();

    bool setPlotFlag = 0;

public slots:
    void handleTimeout();

private:
    void writeToCsv(uint16_t value);
    QTimer m_timer;
    QScatterSeries *m_series;
    QStringList m_titles;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    qreal m_step;
    int m_counter;
    int m_xMax;
    int m_yMax;
    double y_out;
    int m_x;
    int m_y;
    int m_updateInterval;
    QFile file;
    QTextStream outStream;
};

#endif // PLOTCHART_INTERACTIVE_H
