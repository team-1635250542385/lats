#ifndef BANDGROUPBIASDIALOG_H
#define BANDGROUPBIASDIALOG_H

#include <QDialog>

namespace Ui {
class BandGroupBiasDialog;
}

class BandGroupBiasDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BandGroupBiasDialog(QWidget *parent = nullptr);
    ~BandGroupBiasDialog();

    uint8_t getBandgroupPrefer();
    uint8_t getBandgroupAvoid();
    uint8_t getPersist();

private:
    Ui::BandGroupBiasDialog *ui;

    uint8_t bgPrefer                = 0x00;
    uint8_t bgAvoid                 = 0x00;
    uint8_t persist                 = 0x00;
};

#endif // BANDGROUPBIASDIALOG_H
