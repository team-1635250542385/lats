#ifndef DIGITALTIMER_H
#define DIGITALTIMER_H

#include <QLCDNumber>

class DigitalTimer : public QLCDNumber
{
    Q_OBJECT
public:
    DigitalTimer();
    QString showTime();

protected:                                      // event handlers
    void timerEvent( QTimerEvent * );

private slots:                                  // internal slots
    void stopDate();

private:                                        // internal data
    void showDate();

    bool showingColon;
    int  normalTimer;
    int  showDateTimer;
};

#endif // DIGITALTIMER_H
