//!###############################################
//! Filename: debuglv.h
//! Description: Class that provides an
//!              Dialog Box for selecting ISW debug levels
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#ifndef DEBUGLV_H
#define DEBUGLV_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include "../Logger/Logger.h"

namespace Ui {
class debugLv;
}

class debugLv : public QDialog
{
    Q_OBJECT

public:
    explicit debugLv(QWidget *parent = nullptr);
    ~debugLv();

#define MAX_DEBUG_LEVELS 17

    void setDebugLevel(QStringList);
    uint16_t getDebugLevel();
    QStringList getDebugList();
    uint16_t* getDebugLevelArray();
    bool getLightMode();

private slots:
    void on_okPushButton_clicked();
    void on_checkAllPushButton_clicked();

    void on_clearAllPushButton_clicked();

private:
    Ui::debugLv *ui;
    QListWidget* listWidget;
    QDialogButtonBox* buttonBoxDebugLv;
    QGroupBox* viewBoxDebugLv;
    QPushButton* confirmButton;
    QPushButton* closeButton;
    QStringList strList;
    QListWidgetItem* item = nullptr;
    QStringList debugList;
    uint16_t dbL[MAX_DEBUG_LEVELS] = { 0 };
    uint16_t debugLevel = 0;
    bool light_mode = false;
};

#endif // DEBUGLV_H
