//!##########################################################
//! Filename: setbackgroundscanparameters.cpp
//! Description: Class that provides an dialog
//!              that allows the user to configure
//!              the background scan parameters for
//!              the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "setbackgroundscanparametersdialog.h"
#include "ui_setbackgroundscanparametersdialog.h"
#include <QDebug>

SetBackgroundScanParametersDialog::SetBackgroundScanParametersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetBackgroundScanParametersDialog)
{
    ui->setupUi(this);

    ui->fluxScanDurationLineEdit->setEnabled(false);
    ui->fluxScanFrequencyLineEdit->setEnabled(false);
}

SetBackgroundScanParametersDialog::~SetBackgroundScanParametersDialog()
{
    delete ui;
}

uint16_t SetBackgroundScanParametersDialog::getStableScanFrequency() const
{
    uint16_t value = ui->stableScanFrequencyLineEdit->text().toUInt();
    return value;
}

uint8_t SetBackgroundScanParametersDialog::getVersion() const
{
    return ui->versionComboBox->currentIndex();
}

uint16_t SetBackgroundScanParametersDialog::getFluxScanDuration() const
{
    return ui->fluxScanDurationLineEdit->text().toUInt();
}

uint16_t SetBackgroundScanParametersDialog::getFluxScanFrequency() const
{
    return ui->fluxScanFrequencyLineEdit->text().toUInt();
}

void SetBackgroundScanParametersDialog::on_versionComboBox_activated(int index)
{
//    qDebug() << "Current Index: " << index;

    switch(index)
    {
        case 0:
        {
            // Legacy
//            qDebug() << "Legacy";
            ui->fluxScanDurationLineEdit->setEnabled(false);
            ui->fluxScanFrequencyLineEdit->setEnabled(false);
            break;
        }
        case 1:
        {
            // Version 1
//            qDebug() << "Version 1";
            ui->fluxScanDurationLineEdit->setEnabled(true);
            ui->fluxScanFrequencyLineEdit->setEnabled(true);
            break;
        }
        default:
        {
            // Default -- Legacy
//            qDebug() << "Default: Legacy";
            ui->fluxScanDurationLineEdit->setEnabled(false);
            ui->fluxScanFrequencyLineEdit->setEnabled(false);
            break;
        }
    }
}
