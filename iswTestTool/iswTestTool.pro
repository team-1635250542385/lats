#-------------------------------------------------
#
# Project created by QtCreator 2019-01-15T08:28:21
#
#-------------------------------------------------

QT       += core gui sql charts multimedia multimediawidgets xml network opengl concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = iswTestTool
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += DEBUG

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH +=  /usr/local/include/ \
                /usr/include \
                /usr/include/gstreamer-1.0 \
                /usr/include/glib-2.0 \
                /usr/lib64/glib-2.0/include \
                /usr/lib/x86_64-linux-gnu/glib-2.0/include \
                /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include

SOURCES += \
    ../IswInterface/IswTest.cpp \
    ../Logger/DBLogRecord.cpp \
    ../Logger/DBLogger.cpp \
    ../Logger/Logger.cpp \
    ../UsbInterface/UsbInterface.cpp \
    ../SerialInterface/SerialInterface.cpp \
    ../OnBoard/OnBoard.cpp \
    ../UsbInterface/UsbInterfaceInit.cpp \
    ../IswInterface/IswFirmware.cpp \
    ../IswInterface/IswSystem.cpp \
    ../IswInterface/IswIdentity.cpp \
    ../IswInterface/IswSecurity.cpp \
    ../IswInterface/IswAssociation.cpp \
    ../IswInterface/IswStream.cpp \
    ../IswInterface/IswLink.cpp \
    ../IswInterface/IswMetrics.cpp \
    ../IswInterface/IswProduct.cpp \
    ../IswInterface/IswSolNet.cpp \
    ../IswInterface/IswInterface.cpp \
    ../IswInterface/IswInterfaceInit.cpp \
    NetTopologyGraphicsScene.cpp \
    antennaconfig.cpp \
    antennaid.cpp \
    bandgroupbiasdialog.cpp \
    calculatedatatestthroughput.cpp \
    debuglv.cpp \
    digitaltimer.cpp \
    dmm.cpp \
    edge.cpp \
    getradiostatusdialog.cpp \
    hibernationmodedialog.cpp \
    node.cpp \
    phyratesdialog.cpp \
    plotchart_default.cpp \
    plotchart_interactive.cpp \
    powersupply.cpp \
    rapcommanddialog.cpp \
    scandutycycledialog.cpp \
    servicechooser.cpp \
    setbackgroundscanparametersdialog.cpp \
    setstreamspecdialog.cpp \
    socketscpi.cpp \
    squadleadermodedialog.cpp \
    PacketGenerator.cpp \
    GstSender.cpp \
    GstPlayer.cpp \
    VideoGenerator.cpp \
    OnBoardTests.cpp \
    iswtesttool.cpp \
    main.cpp \
    verificationtestssummarydialog.cpp


HEADERS += \
    ../IswInterface/IswTest.h \
    ../Logger/DBLogRecord.h \
    ../Logger/DBLogger.h \
    ../Logger/Logger.h \
    ../IswInterface/Interface.h \
    ../UsbInterface/UsbInterface.h \
    ../SerialInterface/SerialInterface.h \
    ../OnBoard/OnBoard.h \
    ../UsbInterface/UsbInterfaceInit.h \
    ../IswInterface/IswFirmware.h \
    ../IswInterface/IswSystem.h \
    ../IswInterface/IswIdentity.h \
    ../IswInterface/IswSecurity.h \
    ../IswInterface/IswAssociation.h \
    ../IswInterface/IswStream.h \
    ../IswInterface/IswLink.h \
    ../IswInterface/IswMetrics.h \
    ../IswInterface/IswProduct.h \
    ../IswInterface/IswSolNet.h \
    ../IswInterface/IswInterface.h \
    ../IswInterface/IswInterfaceInit.h \
    CommonButtonParameters.h \
    NetTopologyGraphicsScene.h \
    antennaconfig.h \
    antennaid.h \
    bandgroupbiasdialog.h \
    calculatedatatestthroughput.h \
    debuglv.h \
    digitaltimer.h \
    dmm.h \
    edge.h \
    getradiostatusdialog.h \
    hibernationmodedialog.h \
    node.h \
    phyratesdialog.h \
    plotchart_default.h \
    plotchart_interactive.h \
    powersupply.h \
    powersupply.h \
    rapcommanddialog.h \
    scandutycycledialog.h \
    servicechooser.h \
    setbackgroundscanparametersdialog.h \
    setstreamspecdialog.h \
    socketscpi.h \
    squadleadermodedialog.h \
    PacketGenerator.h \
    GstSender.h \
    GstPlayer.h \
    VideoGenerator.h \
    OnBoardTests.h \
    iswtesttool.h \
    verificationtestssummarydialog.h

FORMS += \
        antennaconfig.ui \
        antennaid.ui \
        bandgroupbiasdialog.ui \
        debuglv.ui \
        getradiostatusdialog.ui \
        hibernationmodedialog.ui \
        iswtesttool.ui \
        phyratesdialog.ui \
        rapcommanddialog.ui \
        scandutycycledialog.ui \
        servicechooser.ui \
        setbackgroundscanparametersdialog.ui \
        setstreamspecdialog.ui \
        squadleadermodedialog.ui \
        PacketGenerator.ui \
        VideoGenerator.ui \
        OnBoardTests.ui \
        verificationtestssummarydialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#LIBS += -L/usr/lib64 -lusb-1.0 -lIswInterface -lz
LIBS += -L/usr/lib64 -lusb-1.0 -lz -lftdi1
LIBS += -L/usr/lib64/gstreamer-1.0 -lgstreamer-1.0 -lgstapp-1.0 -lgstvideo-1.0 -lgobject-2.0 -lglib-2.0 -lgio-2.0

DISTFILES += \
    DebugLevel.qml \
    DebugLevel.ui.qml
