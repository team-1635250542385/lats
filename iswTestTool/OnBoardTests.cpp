//!##################################################
//! Filename: OnBoardTests.cpp
//! Description: Class that provides an interface
//!              to access the on board tests that
//!              run on some test boards from the
//!              vendor.  This class allows the user
//!              of iswTestTool to configure and start
//!              a test from one board to another.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "OnBoardTests.h"
#include "ui_OnBoardTests.h"
#include <unistd.h>
#include <QButtonGroup>
#include <QMessageBox>

//!########################################################
//! Constructor: onBoardTests class
//! Inputs: - pointer to the iswTestTool object
//!         - userData pointer to a CommonButtonParameters
//!         struct of information needed to send/receive
//!         data
//!         - parent widget
//!########################################################
OnBoardTests::OnBoardTests(iswTestTool *testToolPtr, void *userData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OnBoardTests),
    iswToolPtr(testToolPtr)
{
    ui->setupUi(this);
    setWindowTitle("Onboard Data Traffic Generator");

    cbp = (CommonButtonParameters *)userData;
    providerIswIntrfcPtr = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswInterface;
    subscriberIswIntrfcPtr = iswToolPtr->DynArray[cbp->subscriberIndexInDynArray].iswInterface;
    providerIswSolNetPtr = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswSolNet;


    if ( (providerIswIntrfcPtr == nullptr) ||
         (subscriberIswIntrfcPtr == nullptr) ||
         (providerIswSolNetPtr == nullptr) )
    {
        displayMessage("Provider or Subscriber Device Is Not Available - Test Ending!");
        onBoard->theLogger->LogMessage("Provider or Subscriber Device Is Not Available - Test Ending!", ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
        return;
    }

    //! Setup the control buttons: start, stop, cancel
    startTestPushButton = new QPushButton(this);
    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
    startTestPushButton->setText("Start");
    startTestPushButton->setGeometry(QRect(100, 95, 122, 28));
    startTestPushButton->setCheckable(true);
    startTestPushButton->setAutoExclusive(true);
    stopTestPushButton = new QPushButton(this);
    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
    stopTestPushButton->setText("Stop");
    stopTestPushButton->setGeometry(QRect(270, 95, 122, 28));
    stopTestPushButton->setCheckable(true);
    stopTestPushButton->setAutoExclusive(true);

    startTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");
    stopTestPushButton->setStyleSheet("QPushButton {  background-color : lightgrey; color : darkblue; }");

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(startTestPushButton);
    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);
    buttonGroup->addButton(stopTestPushButton);
    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

    createDataTestTable();

    ui->topWidget->update();
    QApplication::processEvents();
}

OnBoardTests::~OnBoardTests()
{
    if ( startTestPushButton != nullptr )
        delete startTestPushButton;

    if ( stopTestPushButton != nullptr )
        delete stopTestPushButton;

    if ( buttonGroup != nullptr )
        delete buttonGroup;

    if (cbp != nullptr )
        delete (cbp);

    if ( ui != nullptr )
        delete (ui);
}

//!##################################################
//! createDataTestTable()
//! Sets up the pop up window/table for user
//! configuration and start/stop of the test
//!##################################################
void OnBoardTests::createDataTestTable()
{
    // Table used in the Data Traffic tab
    // Format Active Links Table
    ui->dataTestTable->setRowCount(1);
    ui->dataTestTable->setColumnCount(7);

    QStringList tableHeader;
    tableHeader << "Source" << "Destination" << "Size" << "Set Data Rate (Mbps)" << "Avg. RSSI" << "Tx Mbps" << "Rx Mbps";
    ui->dataTestTable->setHorizontalHeaderLabels(tableHeader);
    ui->dataTestTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->dataTestTable->horizontalHeader()->setResizeContentsPrecision(true);
    ui->dataTestTable->horizontalHeader()->setStyleSheet("QHeaderView { font-size: 8pt; }");

    uint8_t row = 0;
    providerColumn.setText(cbp->provider);
    ui->dataTestTable->setItem(row, 0, &providerColumn);

    subscriberColumn.setText(cbp->subscriber);
    ui->dataTestTable->setItem(row, 1, &subscriberColumn);

    packetSizeColumn.setText("65535");
    ui->dataTestTable->setItem(row, 2, &packetSizeColumn);

    throughputColumn.setText("100");
    ui->dataTestTable->setItem(row, 3, &throughputColumn);

    rssiColumn.setText("0");
    ui->dataTestTable->setItem(row, 4, &rssiColumn);

    txMbpsColumn.setText("0");
    ui->dataTestTable->setItem(row, 5, &txMbpsColumn);

    rxMbpsColumn.setText("0");
    ui->dataTestTable->setItem(row, 6, &rxMbpsColumn);
}

//!##################################################
//! onGroupButtonClicked()
//! Input: Button Id
//! Combines Start and Stop buttons to be mutually
//! exclusive
//!##################################################
void OnBoardTests::onGroupButtonClicked(int id)
{
    switch(id)
    {
        case START_BUTTON_ID:
        {
            startDataTest();
            break;
        }
        case STOP_BUTTON_ID:
        {
            stopDataTest();
            break;
        }
        default:
            break;
    }
}

//!##################################################
//! startDataTest()
//! Starts the test when the Start button is pushed
//!##################################################
void OnBoardTests::startDataTest()
{
    if ( onboardTestsRunning == true )
    {
        displayMessage("Stop Current Data Generator First");
        onBoard->theLogger->LogMessage("Stop Current Data Generator First", ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
        return;
    }
    else
    {
        // Hotplug causes this check to fail
        if ( (providerIswIntrfcPtr != nullptr) && providerIswIntrfcPtr->IsReady() )
        {
            onBoard = providerIswIntrfcPtr->GetIswOnBoard();

            if ( onBoard == nullptr )
            {
                displayMessage("OnBoardTest Object NULL");
                onBoard->theLogger->LogMessage("OnBoardTest Object NULL", ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                return;
            }

            uint16_t throughput = throughputColumn.text().toInt();
            uint16_t pktSize = packetSizeColumn.text().toInt();

            onBoard->SendSetTestModeCmd(cbp->subscriberIswPeerIndex, pktSize, throughput);
            usleep(10);

            int count = 5;
            while ( (onBoard->isTestModeSet() == false) && (count > 0) )
            {
                sleep(1);
                count--;
            }

            if ( onBoard->isTestModeSet() == true )
            {
                onBoard->SendStartTestCmd(cbp->subscriberIswPeerIndex);
                usleep(100);

                onBoard->SendGetTestModeCmd(cbp->subscriberIswPeerIndex);
                usleep(100);

                int count = 5;
                while ( (onBoard->isTestRunning() == false) && (count > 0) )
                {
                    sleep(1);
                    count--;
                }

                if ( onBoard->isTestRunning() == true )
                {
                    onboardTestsRunning = true;

                    checkThroughputThread = QtConcurrent::run(getThroughput, this);
                }
                else
                {
                    QString messageString = "Device " + QString::number(providerIswIntrfcPtr->GetIndex()) + " onBoard Data Traffic Generator Not Running";
                    displayMessage(messageString);
                    onBoard->theLogger->LogMessage(messageString.toStdString(), providerIswIntrfcPtr->GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                QString messageString = "Device " + QString::number(providerIswIntrfcPtr->GetIndex()) + " onBoard Data Traffic Generator Not Setup";
                displayMessage(messageString);
                onBoard->theLogger->LogMessage(messageString.toStdString(), providerIswIntrfcPtr->GetIndex(), std::chrono::system_clock::now());
            }
        }
    }
}

//!##################################################
//! stopDataTest()
//! Stops the test when the Stop button is pushed
//!##################################################
void OnBoardTests::stopDataTest()
{
    if ( onboardTestsRunning == false )
    {
        return;
    }

    onBoard->SendStopTestCmd(cbp->subscriberIswPeerIndex);
    sleep(1);

    checkThroughputThread.cancel();
    //checkThroughputThread.waitForFinished(); //This is hanging forever??

    onBoard->SendGetTestModeCmd(cbp->subscriberIswPeerIndex);
    sleep(1);

    if ( onBoard->isTestRunning() == true )
    {
        QString messageString = "Device " + QString::number(providerIswIntrfcPtr->GetIndex()) + " onBoard Data Traffic Generator Not Stopped";
        displayMessage(messageString);
        onBoard->theLogger->LogMessage(messageString.toStdString(), providerIswIntrfcPtr->GetIndex(), std::chrono::system_clock::now());
    }
    else
    {
        QString messageString = "Device " + QString::number(providerIswIntrfcPtr->GetIndex()) + " onBoard Data Traffic Generator Stopped";
        displayMessage(messageString);
        onBoard->theLogger->LogMessage(messageString.toStdString(), providerIswIntrfcPtr->GetIndex(), std::chrono::system_clock::now());
    }

    onboardTestsRunning = false;
}

//!######################################################
//! getThroughput()
//! Runs in a concurrent thread get rx and tx throughput
//! from the firmware during the test
//!######################################################
void OnBoardTests::getThroughput(OnBoardTests *thisPtr)
{
    IswMetrics *iswMetrics = thisPtr->providerIswIntrfcPtr->GetIswMetrics();
    IswStream *iswSrcStream = thisPtr->providerIswIntrfcPtr->GetIswStream();
    std::array<IswStream::iswPeerRecord, MAX_PEERS> &providersPeers = iswSrcStream->GetPeerRef();
    IswStream *iswDestStream = thisPtr->subscriberIswIntrfcPtr->GetIswStream();
    std::array<IswStream::iswPeerRecord, MAX_PEERS> &subscribersPeers = iswDestStream->GetPeerRef();

    while ( thisPtr->onboardTestsRunning == true )
    {
        int rssi = 0;
        rssi = iswMetrics->GetRssi();
        thisPtr->rssiColumn.setText(QString::number(iswMetrics->GetRssi()));

        uint8_t txMbps = providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.txThroughput;
        thisPtr->txMbpsColumn.setText(QString::number(providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.txThroughput));

        if (thisPtr->subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() != UsbInterface::externalBoard)
        {
            uint8_t rxMbps = subscribersPeers[thisPtr->cbp->providerIswPeerIndex].iswPeerRecord4.rxThroughput;
            thisPtr->rxMbpsColumn.setText(QString::number(rxMbps = subscribersPeers[thisPtr->cbp->providerIswPeerIndex].iswPeerRecord4.rxThroughput));

            qint64 qiTimestamp = QDateTime::currentMSecsSinceEpoch();

            thisPtr->writeToCsv(qiTimestamp, rssi, txMbps, rxMbps);
        }

        sleep(1);
    }
}

//!######################################################
//! displayMessage()
//! Pops up a window with a message for the user
//!######################################################
void OnBoardTests::displayMessage(QString string)
{
    QMessageBox msgBox;
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);

    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(3000);

    msgBox.exec();
}

void OnBoardTests::writeToCsv(qint64 timestamp, uint8_t rssi, uint8_t txMbps, uint8_t rxMbps) //
{
    float value1 = rssi;
    float value2 = txMbps;
    float value3 = rxMbps;

    QString filename = cbp->provider + "_" + cbp->subscriber + "_data.csv";
    QFile file(filename);

    if (file.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream <<timestamp<<"\t"<<value1<< "\t"<< value2 <<"\t"<<value3<<"\n";
    }
    file.flush();
    file.close();
}
