//!###############################################
//! Filename: powersupply.cpp
//! Description: Class that provides an
//!              interface to the Keysight Digital Multimeters
//!              measuring the voltage and current to calculate the Power
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "powersupply.h"
#include "socketscpi.h"
#include "../Logger/Logger.h"
#include "../IswInterface/IswInterfaceInit.h"
#include "../IswInterface/IswInterface.h"

PowerSupply::PowerSupply()
{
    device = "SIGLENT SPD3303X-E";
    ipAddr = "169.254.4.64";
    port   = 5025;
    scpi   = new SocketScpi(ipAddr, port);
}

PowerSupply::PowerSupply(QString ipAddr1) // Logger *logger
{
    device = "SIGLENT SPD3303X-E";
    ipAddr = ipAddr1;
    port   = 5025;
    scpi   = new SocketScpi(ipAddr, port);

    qDebug() << ipAddr1 << "is connecting";

//    theLogger = logger;
}

PowerSupply::~PowerSupply()
{
    if ( scpi != nullptr )
        delete (scpi);
}

void PowerSupply::connect()
{
    scpi->connect();
}

void PowerSupply::disconnect()
{
    scpi->disconnect();
}

void PowerSupply::on(uint8_t channel)
{
    switch (channel)
    {
        case 1:
        {
            scpi->send("OUTPut CH1,ON");
            break;
        }
        case 2:
        {
            scpi->send("OUTPut CH2,ON");
            break;
        }
        case 3:
        {
            scpi->send("OUTPut CH3,ON");
            break;
        }
        default:
        {
            qDebug() << "Invalid Power Supply Channel";
        }
    }

    return;
}

void PowerSupply::off(uint8_t channel)
{
    switch (channel)
    {
        case 1:
        {
            scpi->send("OUTPut CH1,OFF");
            break;
        }
        case 2:
        {
            scpi->send("OUTPut CH2,OFF");
            break;
        }
        case 3:
        {
            scpi->send("OUTPut CH3,OFF");
            break;
        }
        default:
        {
            qDebug() << "Invalid Power Supply Channel";
        }
    }
    return;
}

void PowerSupply::init(double voltage, double current)
{
    QString command;
    QString voltageString = QString::number(voltage);
    QString currentString = QString::number(current);

    command = "CH1:VOLTage " + voltageString;
    scpi->send(command);

    command = "CH2:VOLTage " + voltageString;
    scpi->send(command);

    command = "CH1:CURRent " + currentString;
    scpi->send(command);

    command = "CH2:CURRent " + currentString;
    scpi->send(command);
}

double PowerSupply::getPower()
{
    scpi->send("MEAS:POWEr? CH1\n");
    return (scpi->recv());
}
