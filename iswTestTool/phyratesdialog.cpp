//!##########################################################
//! Filename: phyratesdialog.cpp
//! Description: Class that provides an dialog
//!              that allows the user to configure
//!              the stream spec parameters for
//!              the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "phyratesdialog.h"
#include "ui_phyratesdialog.h"
#include "QDebug"

PhyRatesDialog::PhyRatesDialog(QWidget *parent, uint32_t phyRates) :
    QDialog(parent),
    ui(new Ui::PhyRatesDialog)
{
    ui->setupUi(this);

    ui->phyRatesLineEdit->setText(QString::number(phyRates));
}

PhyRatesDialog::~PhyRatesDialog()
{
    delete ui;
}

uint32_t PhyRatesDialog::getPhyRateBitmap()
{
    return phyRateBitmap;
}

uint8_t PhyRatesDialog::getPersist()
{
    return persist;
}

void PhyRatesDialog::setPhyRates(uint32_t value)
{
    phyRateBitmap = value;
    ui->phyRatesLineEdit->setText(QString::number(phyRateBitmap));
}

void PhyRatesDialog::setPersist(uint8_t value)
{
    persist = value;
}

bool PhyRatesDialog::verifyPhyRatesInput(QString text)
{
    bool validInput = false;

    int input = text.toInt();

    if(input >= 0 && input <= 255)
    {
        validInput = true;
    }

//    int bitmapSize = 8;

//    qDebug() << "input string size:" << text.size();
//    qDebug() << "input string::" << text;

//    if(text.size() == bitmapSize)
//    {
//        for(int i = 0; i < text.size(); i++)
//        {
//            QCharRef bit = text[i];

//            qDebug() << "bit: " << bit;

//            if(bit.isDigit())
//            {
//                if(i == bitmapSize - 1)
//                {
//                    qDebug() << "passed input validation";
//                    validInput = true;
//                    break;
//                }
//                else
//                {
//                    continue;
//                }

//            }
//            else
//            {
//                qDebug() << "failed input validation, breaking out";
//                break;
//            }
//        }
//    }


    return validInput;
}

void PhyRatesDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    uint8_t persistInput = 0;
    if(button->text() == "OK")
    {
        QString text = ui->phyRatesLineEdit->text().trimmed();
        bool validInput = verifyPhyRatesInput(text);

        if(validInput)
        {
            int intVal = text.toInt();
            setPhyRates(intVal);
            if(ui->persistRadioButton->isChecked())
            {
                persistInput = 1;
            }
            setPersist(persistInput);
            sendPhyRatesCommand = true;
        }
        else
        {
            qDebug() << "Phy Rates Input outside of valid range: 0 - 255";
        }
    }
    else
    {
        sendPhyRatesCommand = false;
    }
}

bool PhyRatesDialog::getSendPhyRatesCommand()
{
    return sendPhyRatesCommand;
}

