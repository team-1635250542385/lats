//!#####################################################
//! Filename: main.cpp
//! Description: Main Application
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "iswtesttool.h"
#include <QApplication>
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if(getuid())
    {
        qDebug() << "Not Logged As Root User";
        return 0;
    }
    else
    {
       iswTestTool w;
       w.show();
       return a.exec();
    }
}
