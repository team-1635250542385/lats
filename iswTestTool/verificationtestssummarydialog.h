#ifndef VERIFICATIONTESTSSUMMARYDIALOG_H
#define VERIFICATIONTESTSSUMMARYDIALOG_H

#include <QDialog>
#include <QTableWidgetItem>

namespace Ui {
class VerificationTestsSummaryDialog;
}

class VerificationTestsSummaryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VerificationTestsSummaryDialog(QWidget *parent = nullptr);
    ~VerificationTestsSummaryDialog();

    QString getNumberOfCommandsPass();
    QString getNumberOfCommandsFail();
    QString getPercentageOfRequiredCommandsPass();

    void setNumberOfCommandsPass(QString str);
    void setNumberOfCommandsFail(QString str);
    void setPercentageOfRequiredCommandsPass(QString str);
    void setCommand(QString str);
    void setResult(QString str);

private:
    Ui::VerificationTestsSummaryDialog *ui;

    int row     = 1;
    int column  = 1;

    QTableWidgetItem* verificationTableCell = nullptr;
};

#endif // VERIFICATIONTESTSSUMMARYDIALOG_H
