//!#####################################################
//! Filename: hibernationmodedialog.cpp
//! Description: Class that provides an dialog
//!              to configure the selected radio's
//!              hibernation mode parameters.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "hibernationmodedialog.h"
#include "ui_hibernationmodedialog.h"

HibernationModeDialog::HibernationModeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HibernationModeDialog)
{
    ui->setupUi(this);

    softHibernationTimeValidator = new QIntValidator(4, 16, this);
}

HibernationModeDialog::~HibernationModeDialog()
{
    delete ui;
}

void HibernationModeDialog::on_hibernationModeComboBox_activated(const QString &arg1)
{
    QString str = ui->softHibernationTimeLineEdit->text();
    int pos = 0;

    switch(ui->hibernationModeComboBox->currentIndex())
    {
        case 0: // Hibernation Disabled
        {
           ui->idleTimeLineEdit->setEnabled(false);
           ui->softHibernationTimeLineEdit->setEnabled(false);
           break;
        }
        case 1: // Soft Hibernation
        {
            ui->idleTimeLineEdit->setEnabled(true);
            ui->softHibernationTimeLineEdit->setEnabled(true);
            ui->softHibernationTimeLineEdit->setValidator(softHibernationTimeValidator);
            softHibernationTimeValidator->validate(str, pos);
            break;
        }
        case 2: // Hard Hibernation
        {
            ui->idleTimeLineEdit->setEnabled(true);
            ui->softHibernationTimeLineEdit->setEnabled(false);
            break;
        }
    }
}

uint8_t HibernationModeDialog::getSoftHibernationTime()
{
    softHibernationTime = ui->softHibernationTimeLineEdit->text().toUInt();
    return softHibernationTime;
}

uint16_t HibernationModeDialog::getIdleTime()
{
    idleTime = ui->idleTimeLineEdit->text().toUInt();
    return idleTime;
}

uint8_t HibernationModeDialog::getHibernationMode()
{
    hibernationMode = ui->hibernationModeComboBox->currentIndex();
    return hibernationMode;
}
