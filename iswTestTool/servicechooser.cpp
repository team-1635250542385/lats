//!##########################################################
//! Filename: servicechooser.cpp
//! Description: Class that provides an interface
//!              for the user to select the services
//!              to use for the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include <QDebug>
#include "../IswInterface/IswSolNet.h"
#include "../IswInterface/IswIdentity.h"
#include "servicechooser.h"
#include "ui_servicechooser.h"

//serviceChooser::serviceChooser(DBLogger *dbLogger, const QString &devStr, QListWidget *serviceDescListWidget, QWidget *parent) :
serviceChooser::serviceChooser(DBLogger *dbLogger, uint16_t devType, QListWidget *serviceDescListWidget, QWidget *parent) :
    QDialog(parent),
    dbLogger(dbLogger),
    ui(new Ui::serviceChooser),
    deviceType(devType),
    device(QString::fromStdString(IswIdentity::GetDeviceTypeString(devType)))
{
    ui->setupUi(this);
    setWindowTitle("Select a service to add");

    //! Define which services are available for each type of device
    if ( deviceType == IswIdentity::iswDeviceTypes::HTR )
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::RTA_SERVICE))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::Status))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PacketGenerator));
    }
    else if ( deviceType == IswIdentity::iswDeviceTypes::TWS )
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::VideoService))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::RTA_SERVICE))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::Status))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::SimpleUI))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::MotionSense))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PacketGenerator));
    }
    else if ( deviceType == IswIdentity::iswDeviceTypes::PHN )
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::VideoService))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PacketGenerator))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PositionSense))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::ArService));
    }
    else if ( deviceType == IswIdentity::iswDeviceTypes::LRF )
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::LrfService))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::LrfControlService))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::Status))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PacketGenerator));
    }
    else if ( deviceType == IswIdentity::iswDeviceTypes::HMD )
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::ArService));
    }
    else if( deviceType == IswIdentity::iswDeviceTypes::NET1 || deviceType == IswIdentity::iswDeviceTypes::NET2 || deviceType == IswIdentity::iswDeviceTypes::NET3 || deviceType == IswIdentity::iswDeviceTypes::NET4 || deviceType == IswIdentity::iswDeviceTypes::NET5
             || deviceType == IswIdentity::iswDeviceTypes::GNM || deviceType == IswIdentity::iswDeviceTypes::GNS)
    {
        strList << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::VideoService))
                << QString::fromStdString(IswSolNet::GetServiceIdString(IswSolNet::iswSolNetServiceIds::PacketGenerator));
    }

    ui->serviceListWidget->addItems(strList);

    // Remove services from the chooser that have previously been chosen
//    for (int i = 0; i < ui->serviceListWidget->count(); i++)
//    {
//        curItem = ui->serviceListWidget->item(i);
//        QString pat = "([0-9]+" + QRegExp::escape("-") + ")?" + QRegExp::escape(curItem->text()) + "(" + QRegExp::escape(UNSAVED_STR) + ")?";
//        auto duplicateServiceList = serviceDescListWidget->findItems(pat, Qt::MatchRegExp);
//        if (duplicateServiceList.length() == 0)
//        {
//            curItem->setFlags(curItem->flags() | Qt::ItemIsSelectable);
//            curItem->setSelected(false);
//        }
//        else
//        {
//            delete curItem;
//            i--;
//        }
//    }

//    curItem = nullptr;
}

serviceChooser::~serviceChooser()
{
    if ( ui != nullptr )
        delete ui;
}

std::string serviceChooser::getCurItemStr()
{
    if (curItem == nullptr) {
        return ("");
    } else {
        return (curItem->text().toStdString());
    }
}

void serviceChooser::on_serviceListWidget_itemClicked(QListWidgetItem *item)
{
    curItem = item;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("curItem->text()",getCurItemStr());
    dbLogger->dbLog(dblr);
}

void serviceChooser::on_serviceListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    curItem = item;

    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("curItem->text()",getCurItemStr());
    dbLogger->dbLog(dblr);

    emit chooseService(curItem->text());
    accept();
}

void serviceChooser::on_buttonBox_clicked(QAbstractButton *button)
{
    // DBLogger UI
    DBLogRecord dblr(tbUI, dbLogger->updateEpochTime(), dbLogger->updateUIIndex());
    dblr.addUIObservation("Q_FUNC_INFO", Q_FUNC_INFO);
    dblr.addUIObservation("button->text()",button->text().toStdString());
    dblr.addUIObservation("curItem->text()", getCurItemStr());
    dbLogger->dbLog(dblr);

    if (static_cast<QPushButton*>(button) == ui->buttonBox->button(QDialogButtonBox::Ok)) {
        if (curItem != nullptr) {
            emit chooseService(curItem->text());
        }
    }
}
