//!##########################################################
//! Filename: setstreamspecdialog.cpp
//! Description: Class that provides an dialog
//!              that allows the user to configure
//!              the stream spec parameters for
//!              the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "setstreamspecdialog.h"
#include "ui_setstreamspecdialog.h"
#include "QDebug"

SetStreamSpecDialog::SetStreamSpecDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetStreamSpecDialog)
{
    ui->setupUi(this);
}

SetStreamSpecDialog::~SetStreamSpecDialog()
{
    delete ui;
}

void SetStreamSpecDialog::on_endpointTxPriorityComboBox_activated(int index)
{
    switch(index)
    {
        case 0: // 1
        {
            // Endpoint 1 - Low Priority
            endpointTxPriority = 0x01;
            break;
        }
        case 1: // 2
        {
            // Endpoint 1 - High Priority
            endpointTxPriority = 0x03;
            break;
        }
        case 2: // 3
        {
            // Endpoint 2 - Low Priority
            endpointTxPriority = 0x01;
            break;
        }
        case 3:
        {
            // Endpoint 2 - High Priority
            endpointTxPriority = 0x05;
            break;
        }
        case 4:
        {
            // Endpoint 3 - Low Priority
            endpointTxPriority = 0x01;
            break;
        }
        case 5:
        {
            // Endpoint 3 - High Priority
            endpointTxPriority = 0x09;
            break;
        }
        default:
        {
            // Endpoint 1 - High Priority
            endpointTxPriority = 0x03;
            break;
        }
    }

//    qDebug() << "Endpoint Tx Priority = " << selectedStr;

//    switch(index)
//    {
////        case 0:
////        {
////            // Endpoint 0 - High Priority
////            endpointTxPriority = 0x01;
////            break;
////        }
//        case 0: // 1
//        {
//            // Endpoint 1 - High Priority
//            endpointTxPriority = 0x02;
//            break;
//        }
//        case 1: // 2
//        {
//            // Endpoint 2 - High Priority
//            endpointTxPriority = 0x04;
//            break;
//        }
//        case 2: // 3
//        {
//            // Endpoint 3 - High Priority
//            endpointTxPriority = 0x08;
//            break;
//        }
//        default:
//        {
//            // Endpoint 1 - High Priority
//            endpointTxPriority = 0x02;
//            break;
//        }
//    }
}

void SetStreamSpecDialog::on_endpointTxDiscardableComboBox_activated(int index)
{
    switch(index)
    {
        case 0: // 1
        {
            // Endpoint 1 - Discardable
            endpointTxDiscardable = 0x02;
            break;
        }
        case 1: // 2
        {
            // Endpoint 1 - Non-Discardable
            endpointTxDiscardable = 0x00;
            break;
        }
        case 2: // 3
        {
            // Endpoint 2 - Discardable
            endpointTxDiscardable = 0x04;
            break;
        }
        case 3:
        {
            // Endpoint 2 - Non-Discardable
            endpointTxDiscardable = 0x00;
            break;
        }
        case 4:
        {
            // Endpoint 3 - Discardable
            endpointTxDiscardable = 0x08;
            break;
        }
        case 5:
        {
            // Endpoint 3 - Non-Discardable
            endpointTxDiscardable = 0x00;
            break;
        }
        default:
        {
            // Endpoint 1 - Discardable
            endpointTxDiscardable = 0x02;
            break;
        }
    }
//    QString selectedStr = ui->endpointTxDiscardableComboBox->currentText();
//    qDebug() << "Endpoint Tx Discardable = " << selectedStr;
//    switch(index)
//    {
////        case 0:
////        {
////            // Endpoint 0 - High Priority
////            endpointTxDiscardable = 0x01;
////            break;
////        }
//        case 0: // 1
//        {
//            // Endpoint 1 - High Priority
//            endpointTxDiscardable = 0x02;
//            break;
//        }
//        case 1: // 2
//        {
//            // Endpoint 2 - High Priority
//            endpointTxDiscardable = 0x04;
//            break;
//        }
//        case 2: // 3
//        {
//            // Endpoint 3 - High Priority
//            endpointTxDiscardable = 0x08;
//            break;
//        }
//        default:
//        {
//            // Endpoint 1 - High Priority
//            endpointTxDiscardable = 0x02;
//            break;
//        }
//    }
}

uint8_t SetStreamSpecDialog::getPeerCount() const
{
    return peerCount;
}

void SetStreamSpecDialog::setPeerCount(const uint8_t &value)
{
    peerCount = value;

    for(int i = 0; i < peerCount; i++)
    {
        ui->peerIndexComboBox->insertItem(i,QString::number(i));
    }
}

uint32_t SetStreamSpecDialog::getMaxTxLatency()
{
    maxTxLatency = ui->maxTxLatencyLineEdit->text().toUInt();
    return maxTxLatency;
}

void SetStreamSpecDialog::setMaxTxLatency(const uint32_t &value)
{
    maxTxLatency = value;
}

uint8_t SetStreamSpecDialog::getEndpointTxDiscardable() const
{
    return endpointTxDiscardable;
}

void SetStreamSpecDialog::setEndpointTxDiscardable(const uint8_t &value)
{
    endpointTxDiscardable = value;
}

uint8_t SetStreamSpecDialog::getEndpointTxPriority() const
{
    return endpointTxPriority;
}

void SetStreamSpecDialog::setEndpointTxPriority(const uint8_t &value)
{
    endpointTxPriority = value;
}

void SetStreamSpecDialog::on_peerIndexComboBox_activated(int index)
{
    Q_UNUSED(index);
    peerIndex = ui->peerIndexComboBox->currentIndex();
}

uint8_t SetStreamSpecDialog::getPeerIndex() const
{
    return peerIndex;
}

