//!#####################################################
//! Filename: iswtesttool.h
//! Description: Class that provides the main GUI
//!              interface for connected and remote
//!              radios.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWTESTTOOL_H
#define ISWTESTTOOL_H

#include <stdio.h>
#include <QMainWindow>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QListWidgetItem>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>
#include <QThread>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QXmlSimpleReader>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QElapsedTimer>
#include <QBuffer>
#include "../IswInterface/IswInterfaceInit.h"
#include "../Logger/DBLogger.h"
#include "NetTopologyGraphicsScene.h"
#include "node.h"
#include "dmm.h"
#include "powersupply.h"
#include "plotchart_interactive.h"
#include "plotchart_default.h"
#include "CommonButtonParameters.h"
#include "PacketGenerator.h"
#include "VideoGenerator.h"
#include "OnBoardTests.h"
#include "debuglv.h"
#include "antennaconfig.h"
#include "antennaid.h"
#include "scandutycycledialog.h"
#include "setbackgroundscanparametersdialog.h"
#include "hibernationmodedialog.h"
#include "setstreamspecdialog.h"
#include "rapcommanddialog.h"
#include "phyratesdialog.h"
#include "getradiostatusdialog.h"
#include "digitaltimer.h"
#include "verificationtestssummarydialog.h"
#include "bandgroupbiasdialog.h"
#include <QHash>

//                          void(name) forces compiler to check if variable exists
#define GET_VAR_NAME(name) (void(name),varName(#name))
#define NAME_VAL_NUM(name) (void(name),nameValNum(#name, QString::number((name))))
#define NAME_VAL_TEXT(name) (void(name),nameValText(#name, QString(reinterpret_cast<char *>((name)))))

namespace Ui {
class iswTestTool;
}

class QPushButton;
class Node;

class iswTestTool : public QMainWindow
{
    Q_OBJECT

friend class VideoGenerator;

public:
    explicit iswTestTool(QWidget *parent = nullptr);
    ~iswTestTool();

    /*****************************************************
     * Macro Convenience: NAME_VAL_NUM and NAME_VAL_TEXT
     * **************************************************/
    struct NameVal {
        QString name;
        QString val;
    };
    QString varName(const char*);
    NameVal nameValNum(const char* c, QString v) { return (NameVal){varName(c), v}; };
    NameVal nameValText(const char* c, QString v) { return (NameVal){varName(c), v}; };

    int registerForService(uint8_t subscriberIndex, uint8_t providerIndex, uint8_t dataflowId, QString deviceAndService);
    void setPlaybookActive(bool);

    /*****************************************************
     * iswTestTool UI Input Validators
     * **************************************************/
    QRegExpValidator *textValidator   = nullptr;
    QIntValidator *uint8Validator     = nullptr;
    QIntValidator *uint16Validator    = nullptr;
    QDoubleValidator *uint32Validator = nullptr;
    QDoubleValidator *pfloatValidator = nullptr;

    /*****************************************************
     * iswTestTool DB Logging
     * **************************************************/
    void dbLogDevice(int selected);
    void dbLogDevices();
    void dbLogDevice(long long, int, int);
    void dbLogService(long long, int, int, IswSolNet::iswSolNetServiceEntry *);
    void dbLogService(int selected, IswSolNet::iswSolNetServiceEntry *serviceEntry);
    void dbLogDataStatistics(int selected);

    int getTestId();
    Logger* getLogger();
    Logger* getThroughputTestLogger();
    Logger* getThroughputResutlsLogger();

    void fullyDequeue();

    /*****************************************************
     * ISW API handling
     * **************************************************/
    friend class IswDevice;
    struct SolNetPeerListEntry {
        QString devName;
        QString macAddr;
        uint16_t devType;
        uint8_t peerIndex;
        bool isConnected = false;
        uint64_t connectionTimestampMicrosecs;
        uint16_t recvThroughput;
        uint16_t xmtThroughput; //! Transmit Rate
        uint8_t modePhyRate;    //! Only iswPeerRecord_Version4 has this
        uint8_t averageRSSI;    //! Only iswPeerRecord_Version4 has this
    }__attribute__((packed));

    struct MulticastGroupRecord {
        uint8_t mcastStatus;
        uint8_t mcastPeerIndex;
        uint16_t mcastAddress;
        uint8_t mcastPhyRate;
        uint16_t txQueueDepth;
    };

    struct DeviceMulticastGroups {
        MulticastGroupRecord startGroup;
        MulticastGroupRecord joinGroup;
    };

    //! ISW Device Attributes and Friend Classes
    struct IswAttr {
        QMutex attributesLock;
        bool isReady                   = false;
        QString devName                = "";
        QString macAddr                = "";
        QString firmwareVer            = "";
        uint32_t netId                 = 0;
        uint8_t audienceMode           = IswProduct::AUDIENCE_MODE_DISABLE;
        uint8_t observerMode           = IswProduct::OBSERVER_MODE_DISABLE;
        bool coordCapable              = false;
        uint8_t coordPrecedence        = 0;
        uint8_t role                   = 0;
        uint8_t channel                = 0;
        uint16_t devType               = 0;
        float phy_rate                 = 0.0;
        uint16_t txRate                = 0;
        uint16_t throughput            = 0;
        uint8_t linkQuality            = 0;
        IswInterface *iswInterface     = nullptr;
        IswIdentity *iswIdentity       = nullptr;
        IswFirmware *iswFirmware       = nullptr;
        IswStream *iswStream           = nullptr;
        IswAssociation *iswAssociation = nullptr;
        IswSystem *iswSystem           = nullptr;
        IswMetrics *iswMetrics         = nullptr;
        IswLink *iswLink               = nullptr;
        IswSecurity *iswSecurity       = nullptr;
        IswProduct *iswProduct         = nullptr;
        IswSolNet *iswSolNet           = nullptr;
        IswTest *iswTest               = nullptr;
        Node *node                     = nullptr;
        QVector<SolNetPeerListEntry *> peerList;
        DeviceMulticastGroups multicastGroup;
        QStringList mcastGroupList;
        bool hibernationModeEnabled    = false;
    }__attribute__((packed));

    //! Main data structure
    //! one entry for each device to
    //! hold all device attributes
    IswAttr *DynArray;

    QString getServiceRegistrationCellStatus(int subscriberIndex, int providerIndex);
    void setServiceRegistrationCellStatus(int subscriberIndex, int providerIndex);

#define MAX_CAPABILITIES_LEVELS 8

    enum iswSolNetLrfCapabilitiesValues: uint32_t
    {
        ENVIRONMENTAL_SENSOR        = 0x0040,
        BALLISTICS_CALCULATOR       = 0x0020,
        QUALITY_FACTOR              = 0x0010,
        ERROR_INDICATOR             = 0x0008,
        MULTIPLE_TARGETS            = 0x0004,
        DIRECTIONAL_SENSORS         = 0x0002,
        RANGE                       = 0x0001
    };

    enum iswSolNetLrfBatteryState: uint8_t
    {
        BATTERY_OK       = 0x00,
        BATTERY_LOW      = 0x01,
        BATTERY_CRITICAL = 0x02,
        BATTERY_CHARGING = 0x03
    };

    enum iswSolNetLrfBatteryCharging: uint8_t
    {
        BATTERY_NOT_CHARGING            = 0x00,
        BATTERY_IS_CHARGING             = 0x01,
        BATTERY_DOESNT_SUPPORT_CHARGING = 0x02
    };

    //! Data Policies String representations
    QString DATA_POLICIES_HIGH_DISCARDABLE =  "High Priority, Discardable";
    QString DATA_POLICIES_HIGH_NON_DISCARDABLE = "High Priority, Non-Discardable";
    QString DATA_POLICIES_LOW_DISCARDABLE = "Low Priority, Discardable";
    QString DATA_POLICIES_LOW_NON_DISCARDABLE = "Low Priority, Non-Discardable";



    /*****************************************************
     * Data Traffic Testing
     * **************************************************/
    QMutex commonButtonLock;

    uint16_t networkLoad();

    //bool light_mode = false;

signals:
    void KeepAliveMessage(int status);



protected:
    /*****************************************************
     * Logging
     * **************************************************/
    static void* dequeue_messages(void *arguments);
    static void* dequeue_dbmessages(void *arguments);

    /*****************************************************
     * iswTestTool startup/shutdown
     * **************************************************/
    void initTestTool();
    void stopAllPThreads(void);

    /*****************************************************
     * Network Configuration
     * **************************************************/
    void clearDevices();
    void clearDynArrayEntry(uint8_t selected, bool handleDynArrayAttributesLock);
    void clearDynArray(void);
    void clearSolNetPeerList(uint8_t selected);
    void clearDeviceListComboBox();
    void clearDeviceList();
    void clearPeerListWidget();

    void scanDevice(uint8_t selected, bool handleDynArrayAttributesLock);
    void scanDevices();

    void updateDynArrayWithExternalPeer(uint8_t selected, uint8_t peerIndex, uint16_t devType, std::string macAddress, uint8_t modePhyRate);
    void updatePeers(int selected, bool handleDynArrayAttributesLock);
    void updateIswAttr(int selected, bool handleDynArrayAttributesLock);
    void getMulticastGroup(int selected, bool handleDynArrayAttributesLock);
    void updateDeviceUiAudienceModeForSelected(int selected);
    void updateDeviceUiObserverModeForSelected(int selected);
    void updateDeviceUiForSelected(int selected, bool handleDynArrayAttributesLock);
    void updateDeviceComboBoxForSelected(uint8_t selected);
    void updateTestAutomationDeviceListForSelected(uint8_t selected);
    void updateRegisterServicesTableForSelected(uint8_t selected);
    void updateAvailableLinksTableForSelected(uint8_t selected);

    /*****************************************************
     * Network Topology Change
     * **************************************************/
    void clearEdgesForDevice(uint8_t selected, bool handleDynArrayAttributesLock);
    void clearEdges();
    void createNewNode(uint8_t selected);
    void removeNode(uint8_t selected);
    void updateNodeColor(uint8_t selected);
    void drawOrRemoveEdgesForDevice(uint8_t selected, bool handleDynArrayAttributesLock);
    void drawEdges();
    void startUserMouseEvents();
    void stopUserMouseEvents();

    QString getLinkQualityFromValue(uint8_t value);

    //! Initial Node Position
    int startPosX          = 0;
    int startPosY          = 0;

    bool sendForNewIswAttr = true;

    bool registrationTaskActive = false;

    //! When true we can add devices not
    //! connected over USB - after we have
    //! scanned and added all the local devices
    bool checkForExternalDevices = false;

    /******************************************************
     * iswTestTool SolNet Service Descriptor Configuration
     * ***************************************************/

    //! Group Service Descriptor setup pointers
    struct EndpointComboInputs
    {
        QComboBox *endpointIdComboBox           = nullptr;
        QLineEdit *endpointDataflowLineEdit     = nullptr;
        QComboBox *endpointDistributionComboBox = nullptr;
        QComboBox *endpointDataPoliciesComboBox = nullptr;
        QLineEdit *labelLineEdit  = nullptr;
    };

    void createServiceConfigurationTable();
    void addServiceDescriptorsUI();
    void addDeviceDescriptorUI();
    void addVideoDescriptorUI();
    void addRtaDescriptorUI();
    void addMotionSenseDescriptorUI();
    void addArServiceDescriptorUI();
    void addArType1DescriptorUI();
    void addArType2OverlayOutputDescriptorUI();
    void addPacketGeneratorDescriptorUI();
    void addStatusDescriptorUI();
    void addSimpleUiDescriptorUI();
    void addSimpleUiDataPayloadUI();
    void addLrfServiceDescriptorUI();
    void addLrfControlDescriptorUI();
    void addPositionDeviceDescriptorUI();
    void addPositionSenseDescriptorUI();
    QString getDeviceName();
    QString getDeviceSerialNumber();
    QString getDeviceManufacturer();
    QString getDeviceFriendlyName();
    QString getSoftwareFirmwareName();
    QString getSoftwareFirmwareVersion();
    int getEndpointId(QComboBox *endpointIdComboBox);
    int getEndpointDistribution(QComboBox *endpointDistributionComboBox, uint8_t selected, uint16_t *multicastAddress);
    uint8_t getEndpointDataPolicies(QComboBox *endpointDataPoliciesComboBox);
    uint32_t getEndpointQosDataRate(QLineEdit *endpointQosDataRateLineEdit);
    int getLabel(QLineEdit *labelLineEdit, uint8_t *labelBuf);
    uint32_t getRtaImuRate();
    uint32_t getMotionSenseRate();
    uint8_t getMotionSensorAxes();
    uint8_t getMotionSensorAccelAxes();
    uint8_t getMotionSensorGyroAxes();
    uint8_t getMotionSensorMagAxes();
    void setupEndpointDescriptors(IswSolNet::iswSolNetServiceEntry *serviceEntry, uint8_t currentIndex,
                                  EndpointComboInputs *comboInputs, IswSolNet::iswSolNetPropertyEndpointDesc *endpointDesc,
                                  uint8_t *endpointDistribution, uint8_t *multicastGroupId, uint8_t *deviceChildblock, uint32_t *offset);
    int getDeviceDescriptor(uint8_t currentIndex, int appIndex);
    int getVideoServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getRtaServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getMotionSenseServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getPacketGeneratorServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getStatusServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getPositionSenseServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getSimpleUiServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getArServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getArType1ServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getArType2ServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getLrfServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getLrfControlServiceDescriptor(uint8_t currentIndex, int appIndex);
    int getRegistrationStatus(int subscriberIndex, int providerIndex, int dataflowId);
    void getServiceIdFromText(QString columnHdrText, uint16_t *descriptorId);
    void getPropertyDescriptorIdFromText(QString descriptorText, uint16_t *descriptorId);
    uint16_t getVideoFormatResolutionXServiceDescriptor();
    uint16_t getVideoFormatResolutionYServiceDescriptor();
    float getVideoFormatFrameRateDescriptor();
    uint8_t getFormatOptionsDescriptor();
    uint32_t getVideoFormatDescriptorEncodingFormat(uint16_t *encodingFormat);
    int getVideoFormatDescriptorEncodingIndex(uint16_t encodingFormat);
    uint16_t getImageSensorTypeDescriptor();
    uint16_t getImageSensorWavebandDescriptor();
    uint16_t getVideoFormatBitDepthDescriptor();
    uint16_t getVideoFormatPixelFormatDescriptor();
    uint16_t getVideoProtocolRawDescriptor();
    //!############################################################################
    //! Name: getMaximumFrameRate
    //! Description: Parse the maximum frame rate from the user interface, set a
    //! pointer to the parsed value
    //!
    //! Input: *frameRate: A pointer to the maximum frame rate
    //!
    //! Output: Returns ISW_TEST_TOOL_SUCCESS upon successful parse of maximum
    //! frame rate or ISW_TEST_TOO_FAILURE if the frame rate is unavailable or if
    //! the function is unable to parse the frame rate
    //!############################################################################
    uint32_t getMaximumFrameSize(uint32_t *);
    float getVideoIfovDescriptor();
    uint16_t getVideoPrincipalPointXDescriptor();
    uint16_t getVideoPrincipalPointYDescriptor();
    float getVideoLensDistortionCoefficientsDescriptor();
//    uint16_t getVideoControlBitmapDescriptor();
    uint16_t getArType2MinWidthDescriptor();
    uint16_t getArType2MinHeightDescriptor();
    uint8_t getArType2RleVersionsDescriptor();
    uint8_t getButtonCount(QComboBox *simpleUiButtonCountComboBox);
    uint8_t getStateCount(QComboBox *simpleUiStateCountComboBox);

    // UI additions
    void loadControlDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadVideoDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadRtaDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadMotionSenseDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadArServiceDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadArType1Descriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadArType2Descriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadPacketGeneratorDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadStatusDescriptorUI(IswSolNet::iswSolNetServiceEntry *);
    void loadSimpleUiDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadLrfServiceDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadLrfControlDescriptor(IswSolNet::iswSolNetServiceEntry *);
    void loadPositionSenseDescriptor(IswSolNet::iswSolNetServiceEntry *);
    int getEndpointDataPoliciesIndex(uint8_t);
    int getEndpointDistributionIndex(uint8_t);
    int getVideoFormatPixelFormatDescriptorIndex(uint16_t);
    int getVideoProtocolRawDescriptorIndex(uint16_t);
    int getButtonCountIndex(uint8_t);
    int getStateCountIndex(uint8_t);
    void showDefaultServiceParameters(QString);
    void showSavedServiceParameters(QString, int, int);
    void exportSavedServiceParameters(QString descPlanId);
    //bool insertPropertyField(QString, QString, QString, int, QString, QString, NameVal); // OLD, no dataflowid, wont work with duplicate services
    bool insertPropertyField(QString, QString, QString, QString, int, QString, QString, NameVal);
    bool insertDeviceConfig(QString deviceType, uint32_t ord, QString macAddress, QString coordCapable, QString coordPrecedence, QString role, QString hibernationMode, QString firmwareVer,QString channel,QString netId);
    void writeNetworkMetricsToDb();
    void queryNetworkData(QStringList qryVariablesList);
    bool insertTestData(QString ts, QString providerType, QString linkName, int rssi, int dataRate, int dataThroughput, int linkQuality, QString testId);
    QString getPropertyType(uint16_t);
    int exportVideoDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportSimpleUiDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportDeviceDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportStatusDescriptorUI(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportRtaDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportMotionSenseDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportLrfServiceDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportLrfControlDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportArServiceDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportArType1Descriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportArType2Descriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportPacketGeneratorDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    int exportPositionSenseDescriptor(IswSolNet::iswSolNetServiceEntry *, QString, QString dataflowId);
    void importStoredServiceParameters(QListWidgetItem *);
    void importDeviceConfig();
    void exportDeviceConfig(int);
    QSqlQuery selectStoredServiceProperties(QString, QString, QString, QString);
    void importDeviceDescriptor(QString, QString, QString dataflowId);
    void importVideoDescriptor(QString, QString, QString dataflowId);
    void importRtaDescriptor(QString, QString, QString dataflowId);
    void importMotionSenseDescriptor(QString, QString, QString dataflowId);
    void importArServiceDescriptor(QString, QString, QString dataflowId);
    void importArType1Descriptor(QString, QString, QString dataflowId);
    void importArType2Descriptor(QString, QString, QString dataflowId);
    void importPacketGeneratorDescriptor(QString, QString, QString dataflowId);
    void importStatusDescriptorUI(QString, QString, QString dataflowId);
    void importSimpleUiDescriptor(QString, QString, QString dataflowId);
    void importLrfServiceDescriptor(QString, QString, QString dataflowId);
    void importLrfControlDescriptor(QString, QString, QString dataflowId);
    void importControlDescriptor(QString, QString, QString dataflowId);
    void importPositionSenseDescriptor(QString, QString, QString dataflowId);

    void CreateDeviceServiceDescriptor(IswSolNet *iswSolNet);
    void CreateVideoServiceDescriptor(IswSolNet *iswSolNet);
    void CreateRtaServiceDescriptor(IswSolNet *iswSolNet);
    void CreateMotionSenseServiceDescriptor(IswSolNet *iswSolNet);
    void CreateLrfServiceDescriptor(IswSolNet *iswSolNet);
    void CreateLrfControlServiceDescriptor(IswSolNet *iswSolNet);
    void CreateStatusServiceDescriptor(IswSolNet *iswSolNet);
    void CreatePositionDeviceServiceDescriptor(IswSolNet *iswSolNet);

    QString getNetworkChannelFromInt(int);
    bool isCleanDescPlanId(QString descPlanId);

    void printQryError(QSqlQuery);

    /*****************************************************
     * SolNet Registration Service Table
     * **************************************************/

    struct ExternalServiceEntry
    {
        uint8_t selected;
        uint8_t dynArrayIndex;
        uint8_t peerIndex;
        uint8_t dataflowId;
        uint8_t endpointDistribution;
        QString serviceName;
        uint8_t multicastGroupId = 0;
        uint8_t linkQuality      = 0;
    };

    QMutex registerServicesTableLock;
    void createRegisterServicesTable();
    void clearRegisterServicesTable();
    void addDeviceToRegisterServicesTable(uint8_t selected);
    void removeDeviceFromRegisterServicesTable(uint8_t selected, bool removeDeviceCompletely);
    void addExternalServiceToRegisterServicesTable(ExternalServiceEntry *entry);
    void addServiceToRegisterServicesTable(uint8_t providerIndex, QString serviceName, uint8_t endpointDistribution, uint8_t multicastGroupId);
    void removeServiceFromRegisterServicesTable(QString deviceServiceLabel, bool handleLock);
    //int registerForService(uint8_t subscriberIndex, uint8_t providerIndex, uint8_t dataflowId, QString deviceAndService);
    int deregisterFromService(uint8_t subscriberIndex, uint8_t providerIndex, uint8_t dataflowId);
    int revokeRegistrationOfService(uint8_t subscriberIndex, uint8_t providerIndex, uint8_t dataflowId);

    /*****************************************************
     * Data Traffic Testing
     * **************************************************/
    void stopDataTestsForSelected(uint8_t selected, uint8_t peer, bool deviceIsGone);

    enum testTypes: uint8_t
    {
        PacketGenTest   = 0,
        VideoTest       = 1,
        OnBoardTest     = 2
    };

    struct ActiveDataTest
    {
        CommonButtonParameters *cbp = nullptr;
        uint8_t testType            = 0;
        uint8_t testId              = 0;
        void *testPtr               = nullptr;
        uint8_t endpointId          = 1;
        QString descPlanId          = 0;
        QString playbookName        = "";
        QString serviceName         = "";
    };
    ActiveDataTest aDataTest;
    QList<ActiveDataTest *> activeDataTestsList;

    QMutex availableLinksTableLock;
    void createAvailableLinksTable();
    void clearAvailableLinksTable();
    void addAvailableLink(uint8_t sourceIndex, uint8_t destIndex);
    void removeAvailableLink(uint8_t sourceIndex, uint8_t destIndex);
    //void removeAvailableLinkByDataflowId(uint8_t sourceIndex, uint8_t destIndex, uint8_t dataflowId);
    void removeAvailableLinks(QString devName);
    void commonStartButtonProcess(uint16_t descriptorId, CommonButtonParameters *commonButtonPtr);

    QByteArray returnfile = 0;    
    QByteArray getReturnFile() {return returnfile;}
    void setReturnFile(QByteArray srf) {returnfile = srf;}

    uint32_t numPackets = 0;
    uint32_t getNumPac() {return numPackets;}
    void setNumPac(uint32_t np) {numPackets = np;}

    uint16_t getNextFrameNumber();
    uint16_t getNextPacketCount();
    uint8_t getTargetFlags();
    uint8_t getRangeEventId();
    uint8_t getTargetIndex();
    uint8_t getTotalNumberOfTarget();
    float_t getRange();
    float_t getRangeError();
    float_t getAzimuth();
    uint8_t getAzimuthNorthReference();
    float_t getAzimuthError();
    float_t getElevation();
    float_t getElevationError();
    float_t getBallisticElevationHold();
    float_t getBallisticWindageHold();
    float_t getBallisticWindageConfidence();
    float_t getTemperature();
    float_t getHumidity();
    float_t getPressure();
    float_t getQfactor();

    /******************************************************
     * ISW API Verification Testing -- TODO: Modify Code
     * ***************************************************/
    void verifyIswIdentity(int selected);
    void verifyIswSecurity(int selected);
    void verifyIswAssociation(int selected);
    void verifyIswStream(int selected);
    void verifyIswLink(int selected);
    void verifyIswMetrics(int selected);
    void verifyIswProduct(int selected);
    void verifyIswSystem(int selected);
    void verifyBrowseAdvertiseServices(int selected);
    void verifyRegisterRequestResponse(int selected);
    void verifyGetStatusRequestResponse(int selected);
    void verifyRevokeRegistrationRequestResponse(int selected);
    void verifyDeregisterRequestResponse(int selected);
    void verifyRevokeNetAssociationRequestResponse(int selected);
    int verifyPollDataRequestResponse(int selected);
    void verifyAutonomousDataStartStopRequestResponse(int selected);
    void verifyKeepAliveRequestResponse(int selected);
    void verifyAcknowledgement(int selected);

    /****************************************************
    * IswTestTool utilities
    * ***************************************************/
    QString findNameFromDeviceType(uint16_t devType);
    void displayMessage(QString string);
    void hoverEnter(QHoverEvent *event);
    void hoverLeave(QHoverEvent *event);
    void hoverMove(QHoverEvent *event);
    bool event(QEvent *event);
    int getIswArrayIndex(QString devName);
    int getDynArrayIndexFromMac(QString macAddrStr);
    iswTestTool::SolNetPeerListEntry *findPeerFromMacAddress(uint8_t selected, QString macAddrStr);

    QString getLrfEnvironmentalSensor();
    QString getLrfBallisticCalculator();
    QString getLrfQualityFactor();
    QString getLrfErrorIndicators();
    QString getLrfMultipleTargets();
    QString getLrfDirectionalSensors();
    QString getLrfRange();
    void setLrfCapabilities(QStringList level);

    QStringList phyRateStr    = { "53.3 Mbps", "80 Mbps", "106.7 Mbps", "160 Mbps", "200 Mbps", "320 Mbps" };
    int activeDataTestCnt     = 0;
    int subscriberIndexGlobal = 0;

    /*****************************************************
     * Playbook Utility
     * **************************************************/

    bool playbookActive = false;
    bool playlistActive = false;

    struct Playbook {
        QString name = "";
        int duration = 0; //milliseconds
        QStringList pairsList; // Stores device config plans for each device
    };

    struct ServiceItem
    {
        QString service   = "";
        QString provider  = "";
        QString subcriber = "";
        int dataflow_id = -1;
    };

    //! Send Playboook Checks Timer
    QTimer *playbookMonitorTimer = nullptr;
    int playbookCheckupInterval  = 5;   // Seconds between checks
    int playbookElapsedTime      = 0;   // Seconds
    int playbookDuration         = 0;   // Seconds
    bool restartInProgress       = false;

    // Must be changed if pgen burst settings change from 1 second ON time and 4 second OFF time.
    float burstOnPercentage = 0.20;

    void updatePlaybooksTab();
    bool insertPlaybook(QString, int, QStringList);
    bool hasSpecificServiceDescriptor(QString, QString);
    void deleteServiceSettings(QString descId);
    QList<ServiceItem> getServicesFromPlan(QString);
    bool executeServiceForPlaybook(QString service, int duration, QString deviceName, QString playbookName, QString descPlanId);
    bool insertServiceSettings(QString descId);
    bool insertPgenSettings(QString descriptor_id, int packet_size, int delay, int dataRate, bool burstMode);
    bool insertVideoSettings(QString descriptor_id, int delay);
    QStringList getServiceSettings(QString descriptor_id, QString serviceType);
    Playbook* getPlaybook(QString playbookName);
    void deployPlaybook(Playbook*);
    void deletePlaybook(QString);
    QStringList parsePairsList(QString);
    bool playbookHasDescriptorPlan(QString, QString);
    int findItemIndexInList(QString descPlanId, QListWidget*);
    void configureServiceSettingsInput(QString descId);
    void resetServiceSettingsInputs();
    void writeLineToPlaybookOutput(QString str);
    bool playbookPacketGenStart(int duration, int packetSize, int msgDelay, bool burstMode, int numOfPackets, QString playbookName, QString descPlanId);
    bool playbookVideoGenStart(int msgDelay, QString playbookName, QString descPlanId);
    bool descIdHasStoredServiceSettings(QString descId);
    void shutdownPlaybookServices();
    void checkServiceRegistration();
    void restartService(ActiveDataTest *testEntry);
    void browseForSingleDevice(QString deviceName);
    int getStatusSingleService(ActiveDataTest *testEntry);
    void reconfigureDevices(QStringList *deviceList);
    void deletePlaybooksUsingDescPlan(QString pair);
    bool playbooksDevicesMatch(Playbook *pb);
    bool isValidThroughput(QString textInput);
    int  calcDelayFromDataRate(int dataRate, int testDuration);
    bool convertToBool(QString text);

private slots:
    /****************************************************
    * Hotplug handling slots
    * ***************************************************/
    void checkHotplugEvents();

    /****************************************************
    * Network Configuration slots
    * ***************************************************/
    void on_scanButton_clicked();
    void on_actionExit_triggered();
    void on_netDeviceList_clicked(const QModelIndex &index);
    void showNetDeviceListContextMenu(const QPoint& pos);
    void on_netDeviceList_itemClicked(QListWidgetItem *item);
    void on_deviceComboBox_activated(const QString &arg1);
    void on_netDeviceList_customContextMenuRequested(const QPoint &pos);

    void updateFirmwareDialog();
    void on_actionUpdate_Firmware_triggered();
    void on_firmwareDialogToolButton_clicked();
    void on_updateFirmwareButton_clicked();
    void on_clearUpdateFirmwareTextPushButton_clicked();

    void clearAssociation();
    void clearAllAssociation();
    void inviteMode();
    void searchMode();
    void stopAssociation();

    void enableObserverMode();
    void disableObserverMode();

    void setCoordPrecedence();
    void setCoordPrecedence(int);
    void setDeviceType();
    void setDeviceType(QString);
    void idleRadio();
    void resetRadio();
    void setFactoryDefault();
    void getRadioStatus();

    void setAntennaConfiguration();
    void setAntennaId();
    void setBandgroupBias();
    void getBandgroupBias();

    void startMulticastGroup();
    void joinMulticastGroup();
    void leaveMulticastGroup();
    void modifyMulticastGroup();
    void streamSpecCommand();
    void setHibernationMode();
    void getHibernationMode();
    void setScanDutyCycle();
    void setBackgroundScanParameters();
    void getBackgroundScanParameters();
    void disableAudienceMode();
    void enableAudienceMode();
    void rapCommand();
    void phyRatesCommand();
    void togglePauseReceiveQueueStatus();

    void updateAntennaIds();

    /*****************************************************
     * Network Throughput Slots
     * **************************************************/
    void refreshNetworkData();

    /*****************************************************
     * Network Topology Change Slot
     * **************************************************/
    void handleNetworkTopologyChanges(void);

   /****************************************************
    * SolNet Service Descriptor Configuration slots
    * ***************************************************/
    void on_pushPrintServicesButton();
    void on_cancelPushButton_clicked();
    void on_serviceDescTreeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void on_clearServiceDescriptorTextPushButton_clicked();
    void on_saveServiceDescriptorTextPushButton_clicked();

    //! Common to all services added
    void saveCommonConfigButton_clicked(int appIndex);

    //! UI additions
    void on_pushServiceDescAddButton();
    void on_pushServiceDescRemoveButton();  
    void on_descPlansListWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_pushDescPlanImportButton();
    void on_pushDescPlanExportButton();
    void serviceChosen(const QString &);
    void on_serviceDescTreeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
    void on_serviceDescListWidget_itemDoubleClicked(QListWidgetItem *item);    
    void showDescriptorPlansListContextMenu(const QPoint& pos);
    void deleteDescPlan();
    void deleteDeviceConfig();
    void showServiceDescriptors();

    /****************************************************
    * SolNet Service Descriptor Registration Slots
    * ***************************************************/
    void disableChecksumButton_clicked();
    void browseAllServices_clicked();
    void registerBox_checked(bool checked = false);
    void deregisterBox_checked(bool checked = false);
    void revokeBox_checked(bool checked = false);
    void getRegistrationStatusBox_checked(bool checked = false);
    void getAllStatusButton_clicked();
    void registerAllButton_clicked();
    void deregisterAllButton_clicked();

    /*****************************************************
     * Data Traffic Testing Slots
     * **************************************************/
    void packetGenStart();
    void videoGenStart();
    void onBoardTestStart();

    /*******************************************
     * Power Measurment Slots
     ******************************************/
    void on_pwrMeasurementStartBtn_clicked();
    void refreshPwr();
    void on_pwrMeasurementStopBtn_clicked();
    //void on_performancePlotPushButton_clicked();
    void on_actionAttenuator_Control_triggered();
    void on_savePowerPlotPushButton_clicked();
    void generatePowerMeasurementsDataPlot();
    void writePowerToDB(QString ts, double power, QString nodeName, QString macAddr );

    /*******************************************
     * Temperature Measurment Slots
     ******************************************/
    void on_temperatureMeasurementStartBtn_clicked();
    void refreshTemperature();
    void on_temperatureMeasurementStopBtn_clicked();
    void on_saveTemperaturePlotPushButton_clicked();
    void generateTemperatureMeasurementsDataPlot();
    void writeTemperatureToDB(QString ts, double temp, QString nodeName, QString macAddr );

    /*******************************************
     * ISW Validation Slots
     ******************************************/
    void on_solnetValidationButton_clicked();
    void on_clearTestAutomationTextPushButton_clicked();
    void on_saveTestAutomationTextPushButton_clicked();
    void on_apiComplianceButton_clicked();

    /*******************************************
     * Debug Logging Level Slots
     ******************************************/
    void debugListItemChanged();
    void on_debugCancelPushButton_clicked();
    void on_debugUncheckAllPushButton_clicked();
    void on_debugCheckAllPushButton_clicked();
    void on_debugApplyPushButton_clicked();

    /*******************************************
     * Send Keep Alive Message Slot
     ******************************************/
    void emitKeepAliveMessage();

    void emitPollDataRequestMessage();
    void on_actionReconnectionTime_triggered();
    void on_actionCalculating_Re_Connection_Time_from_Logfile_triggered();
    void on_actionCalculating_Coordinator_Handover_Time_triggered();

    /*******************************************
     * Update Test Timer Slot
     ******************************************/
    void refreshTestTimer();

    void on_batteryInputButton_clicked();
    void on_PowerSupplyCH1ON_clicked();
    void on_PowerSupplyCH2ON_clicked();
    void on_PowerSupplyCH3ON_clicked();
    void on_PowerSupplyCH1OFF_clicked();
    void on_PowerSupplyCH2OFF_clicked();
    void on_PowerSupplyCH3OFF_clicked();
    void on_InitPowerSupply_clicked();
    void on_automateLinkDisruptionButton_clicked();
    void on_importDevConfigButton_clicked();
    void on_exportDevConfigButton_clicked();
    void on_pushButton_clicked();
    void on_DataPlotYAxisOptionsBox_currentIndexChanged(int index);
    void on_DataPlotXAxisOptionsBox_currentIndexChanged(int index);
    void on_DataSetBox_currentIndexChanged(int index);
    void on_SavePlotButton_clicked();

    void sendPollingMessage();

    /*******************************************
     * Latency Slots
     ******************************************/
    void on_ClearNetworkDataButton_clicked();
    void on_calcLatencyButton_clicked();

    /*******************************************
     * Playbook Slots
     ******************************************/

    void on_createPairButton_clicked();
    void on_deletePairButton_clicked();
    void on_savePlaybook_clicked();
    void on_playbooksDescriptorPlansList_itemClicked(QListWidgetItem *item);
    void on_playbooksDeviceList_itemClicked(QListWidgetItem *item);
    void on_DeployPlaybook_clicked();
    void on_playbooksDeleteButton_clicked();
    void on_playbookUpdatePairSettingsButton_clicked();
    void playbookMonitor();
    void on_addPlaybookToPlaylistButton_clicked();
    void on_deletePlaybookFromPlaylistButton_clicked();
    void on_deployPlaylistButton_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_subcaCertDialogToolButton_clicked();
    void on_signerCertDialogToolButton_clicked();
    void on_signatureDialogToolButton_clicked();

private:
    //! Main UI for iswTestTool
    Ui::iswTestTool *ui;
    AntennaConfig* antennaConfig                                        = nullptr;
    AntennaId* antennaIdDialog                                          = nullptr;
    ScanDutyCycleDialog* scanDutyCycleDialog                            = nullptr;
    SetBackgroundScanParametersDialog* setBackgroundScanParameterDialog = nullptr;
    HibernationModeDialog* hibernationModeDialog                        = nullptr;
    VerificationTestsSummaryDialog* verificationTestSummaryDialog       = nullptr;
    BandGroupBiasDialog* bgBiasDialog                                   = nullptr;

    bool antenaDiversityEnabled = false;

    QList<bool> moduleAntennaDiversityStatusList;

    //! Enums to track status of test tool
    enum iswTestToolStatus: uint32_t
    {
        ISW_TEST_TOOL_SUCCESS       = 0x0000,
        ISW_TEST_TOOL_FAILURE       = 0x0001
    };
    //! Video Format Descriptor constants
    const QString VIDEO_FORMAT_ENCODING_RAW  = "RAW";
    const QString VIDEO_FORMAT_ENCODING_JPEG = "Baseline JPEG";
    const QString VIDEO_FORMAT_ENCODING_RLE  = "RLE";

    //! Video Sample Format Descriptor constants
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_Y         = "PIXEL_FORMAT_Y";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_NV21      = "PIXEL_FORMAT_NV21";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_YUY2      = "PIXEL_FORMAT_YUY2";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_NV16      = "PIXEL_FORMAT_NV16";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_YUV422    = "PIXEL_FORMAT_YUV422";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_YUV420    = "PIXEL_FORMAT_YUV420";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGBA_4444 = "PIXEL_FORMAT_RGBA_4444";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGBA_8888 = "PIXEL_FORMAT_RGBA_8888";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGBA_5551 = "PIXEL_FORMAT_RGBA_5551";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGBX_8888 = "PIXEL_FORMAT_RGBX_8888";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGB_332   = "PIXEL_FORMAT_RGB_332";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGB_565   = "PIXEL_FORMAT_RGB_565";
    const QString SAMPLE_FORMAT_PIXEL_FORMAT_RGB_888   = "PIXEL_FORMAT_RGB_888";

    //!############################################################################
    //! Name: printDescriptor
    //! Description: Given an ISW SolNet descriptor common header, print a
    //! space delimited hexadecimal representation of the bytes that
    //! form an ISW SolNet descriptor.
    //!
    //! Input: commondHdr: iswSolNetDescCommonHeader pointer to the common
    //! header corresponding with an ISW SolNet descriptor that needs to be
    //! printed for debugging purposes.
    //!
    //! Output: A single space delimited hexadecimal representation of the
    //! bytes that form an ISW SolNet descriptor, printed to stdout.
    //!############################################################################
    void printDescriptor(IswSolNet::iswSolNetDescCommonHeader *commonHdr);

    //! Write Network Load and Timestamp to CSV File
    void networkLoadWriteToCsv(qint64 timestamp, uint16_t networkLoad);

    //! IswTestTool version
    QString kitVersion;

    //! dynArray is main struct
    //! lock on any changes
    QMutex dynArrayLock;

    //! Initializing the ISW Library
    IswInterfaceInit *iswInterfaceInit                = nullptr;
    std::array<IswInterface *, 21> *iswInterfaceArray = nullptr;

    //! Used to check for device count
    UsbInterfaceInit *usbInterfaceInit = nullptr;
    QString unknownDevTypeStr          = "Unknown Device Type"; // Unsupported

    //! Logger Debug Level set by user
    debugLv* debugLvl = nullptr;

    //! Set Stream Spec Dialog
    SetStreamSpecDialog * setStreamSpecDialog  = nullptr;

    //! Rap Command Dialog
    RapCommandDialog    * rapCommandDialog     = nullptr;

    //! Phy Rates Command Dialog
    PhyRatesDialog    * phyRatesDialog     = nullptr;

    //! Get Radio Status Dialog
    GetRadioStatusDialog* getRadioStatusDialog = nullptr;

    //! Digital Timer Object
    DigitalTimer *digitalTimer                 = nullptr;

    //! Test Timer
    QTimer *testTimer                          = nullptr;

    QTimer *updateAntennaIdTimer               = nullptr;

    //! Logging to text file
    //! Logger - beginning of filename with timestamp added
    std::string loggerFilename            = "iswTest";
    std::string throughputTestFilename    = "throughputTest";
    std::string throughputResultsFilename = "throughputResults";
    std::string latencyTestFilename       = "latencyTest";
    Logger* theLogger                     = nullptr;
    Logger* throughputTestLogger          = nullptr;
    Logger* throughputResultsLogger       = nullptr;
    Logger* latencyLogger                 = nullptr;

    //! Dequeue messages from Logger queue
    pthread_t threadLoggerDequeue = 0;

    //! Structure for Logger dequeue thread
    struct loggerArgStruct
    {
        Logger* logger;
        iswTestTool* iswToolPtr;
    };    

    //! DBLogger text file - formatted for Postgres COPY
    //! Logger - beginning of filename with timestamp added
    std::string dbLogFilename = "iswDBLog";
    DBLogger* dbLogger        = nullptr;

    //! Dequeue messages from DBLogger queue
    pthread_t threadDbLoggerdequeue = 0;

    // Structure for Thread -- DBLogger
    struct dbLoggerArgStruct
    {
        DBLogger* dbLogger;
        iswTestTool* iswToolPtr;
    };

    //! Database Handling
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    QSqlQuery qry;
    bool dbConnected;
    void openDatabase();
    void closeDatabase();

    //! Hotplug event handling
    bool checkingHotplug = false;
    QTimer *hotplugTimer = nullptr;

    //! Network Throughput Initialization
    void generateNetworkStatisticsDataPlot();

    //! Update Network Plots
    void updateNetworkDataPlot();

    void fillDataSetBox();

    //! Network Topology Change Handling
    QTimer *networkTopologyChangeTimer = nullptr;
    int networkTopologyChangeTimeout   = 2000; // 5000
    QMutex networkTopologyChangeLock;

    //! Send Keep Alive Message Timer
    QTimer *keepAliveMessageTimer = nullptr;
    int keepAlivePeriod           = 5;

    //! Send Poll Data Request Message Timer
    QTimer *pollDataMessageTimer = nullptr;

    //! UI setup
    NetTopologyGraphicsScene *netTopologyScene = nullptr;
    QMessageBox msgBox;
    QBrush brush;
    QPen outlinePen;
    QGraphicsTextItem *text;
    QLabel *label;
    QGraphicsProxyWidget *proxy;
    QColor mcastColor;
    QColor peerColor;
    QColor externalColor;
    QColor coordColor;
    QString indexDelim         = "-";
    QString deviceServiceDelim = ":";

    //! Network throughput
    PlotChart_interactive *dataPlotChart      =  nullptr;
    QChartView *dataPlotChartView             = nullptr;
    QGraphicsScene *dataPlotScene             =  nullptr;
    QTimer *data_timer                        = nullptr;
    QElapsedTimer *total_timer                = nullptr;
    uint8_t data_timer_lastIndex              = 0;
    int plotXdataType                         = 0;
    int plotYdataType                         = 0;
    QString dataPlotLinkChoice                = "ALL";

    //! Firmware Update
    int updateFirmwareTabWidgetLocation;
    QString file_label_type   = "";
    QString filename_firmware = "";
    QString filename_video    = "";
    QString filename_audio    = "";
    QString filename_scenario = "";

    QString filename_subca_cert  = "";
    QString filename_signer_cert = "";
    QString filename_signature   = "";


    //! SolNet descriptor handling
    QMutex nextFrameNumberMutex;
    QMutex nextPacketCountMutex;
    uint16_t nextFrameNumber = 0;
    uint16_t nextPacketCount = 0;
    QStringList unicastEndpointIds;
    QStringList broadcastEndpointId;
    QStringList multicastEndpointId;
    QStringList endpointDistribution;
    QStringList endpointDataPolicies;
    QStringList pixelFormatOptionsList;
    QStringList formatOptions;
    QStringList encodingFormatOptionsList;
    QStringList videoProtocol;
    QStringList imageSensorType;
    QStringList imageSensorWaveband;
    QStringList buttonCount;
    QStringList stateCount;
    QStringList videoPolarity;
    QStringList videoImageCalibration;
    QStringList rtaEnabled;
    QStringList rtaMode;
    QStringList rtaPolarity;
    QStringList rtaZoom;
    QStringList rtaBubble;
    QStringList rtaManualAlignment;
    QStringList rtaReticleInVideo;
    QStringList rtaReticleType;
    QStringList batteryStateStatus;
    QStringList batteryChargingStatus;
    QStringList deviceStateStatus;
    QStringList displayStateStatus;
    QStringList weaponSightModeStatus;
    QStringList weaponActiveReticleIDStatus;
    QStringList weaponSymbologyStateStatus;

    QComboBox *deviceEndpointIdComboBox                   = nullptr;
    QLineEdit *deviceEndpointDataflowLineEdit             = nullptr;
    QComboBox *deviceEndpointDistributionComboBox         = nullptr;
    QComboBox *deviceEndpointDataPoliciesComboBox         = nullptr;
    QLineEdit *deviceNameLineEdit                         = nullptr;
    QLineEdit *deviceSerialNumberLineEdit                 = nullptr;
    QLineEdit *deviceManufacturerLineEdit                 = nullptr;
    QLineEdit *deviceFriendlyNameLineEdit                 = nullptr;
    QLineEdit *softwareFirmwareNameLineEdit               = nullptr;
    QLineEdit *softwareFirmwareVersionLineEdit            = nullptr;
    QLineEdit *deviceTextLabelLineEdit                    = nullptr;
    QComboBox *videoEndpointIdComboBox                    = nullptr;
    QLineEdit *videoEndpointDataflowLineEdit              = nullptr;
    QComboBox *videoEndpointDistributionComboBox          = nullptr;
    QComboBox *videoEndpointDataPoliciesComboBox          = nullptr;
    QLineEdit *videoEndpointQosDataRateDescriptorLineEdit = nullptr;
    QLineEdit *videoFormatXServiceDescriptorLineEdit      = nullptr;
    QLineEdit *videoFormatYServiceDescriptorLineEdit      = nullptr;
    QLineEdit *videoFormatFrameRateDescriptorLineEdit     = nullptr;
    QLineEdit *videoFormatBitDepthDescriptorLineEdit      = nullptr;
    QLineEdit *videoMaximumFrameSizeDescriptorLineEdit    = nullptr;
    QComboBox *videoFormatPixelFormatDescriptorComboBox   = nullptr;
    QComboBox *videoFormatOptionsDescriptorComboBox       = nullptr;
    QComboBox *videoEncodingFormatDescriptorComboBox      = nullptr;
    QComboBox *videoProtocolRawDescriptorComboBox         = nullptr;
    QLineEdit *videoIfovDescriptorLineEdit                = nullptr;
    QLineEdit *videoPPointXLineEdit                       = nullptr;
    QLineEdit *videoPPointYLineEdit                       = nullptr;
    QComboBox *imageSensorTypeDescriptorComboBox          = nullptr;
    QComboBox *imageSensorWavebandDescriptorComboBox      = nullptr;
    QLineEdit *videoTextLabelLineEdit                     = nullptr;
    QLineEdit *videoLensDistortionCoefficientsLineEdit    = nullptr;
    QLineEdit *videoControlBitmapLineEdit                 = nullptr;
    QComboBox *videoPolarityComboBox                      = nullptr;
    QComboBox *videoImageCalibrationComboBox              = nullptr;

    QComboBox *rtaControlEndpointIdComboBox           = nullptr;
    QLineEdit *rtaControlEndpointDataflowLineEdit     = nullptr;
    QComboBox *rtaControlEndpointDistributionComboBox = nullptr;
    QComboBox *rtaControlEndpointDataPoliciesComboBox = nullptr;

    QComboBox *rtaDataEndpointIdComboBox           = nullptr;
    QLineEdit *rtaDataEndpointDataflowLineEdit     = nullptr;
    QComboBox *rtaDataEndpointDistributionComboBox = nullptr;
    QComboBox *rtaDataEndpointDataPoliciesComboBox = nullptr;

    QLineEdit *rtaImuRateLineEdit         = nullptr;
    QLineEdit *rtaTextLabelLineEdit       = nullptr;
    QComboBox *rtaEnabledComboBox         = nullptr;
    QComboBox *rtaModeComboBox            = nullptr;
    QComboBox *rtaPolarityComboBox        = nullptr;
    QComboBox *rtaZoomComboBox            = nullptr;
    QComboBox *rtaBubbleComboBox          = nullptr;
    QComboBox *rtaManualAlignmentComboBox = nullptr;
    QComboBox *rtaReticleInVideoComboBox  = nullptr;
    QComboBox *rtaReticleTypeComboBox     = nullptr;
    QLineEdit *rtaQuaternionReal          = nullptr;
    QLineEdit *rtaQuaternionI             = nullptr;
    QLineEdit *rtaQuaternionJ             = nullptr;
    QLineEdit *rtaQuaternionK             = nullptr;

    QComboBox *packetGenEndpointIdComboBox           = nullptr;
    QLineEdit *packetGenEndpointDataflowLineEdit     = nullptr;
    QComboBox *packetGenEndpointDistributionComboBox = nullptr;
    QComboBox *packetGenEndpointDataPoliciesComboBox = nullptr;

    QComboBox *statusUnicastEndpointIdComboBox             = nullptr;
    QLineEdit *statusUnicastEndpointDataflowLineEdit       = nullptr;
    QComboBox *statusUnicastEndpointDistributionComboBox   = nullptr;
    QComboBox *statusUnicastEndpointDataPoliciesComboBox   = nullptr;
    QComboBox *statusBroadcastEndpointIdComboBox           = nullptr;
    QLineEdit *statusBroadcastEndpointDataflowLineEdit     = nullptr;
    QComboBox *statusBroadcastEndpointDistributionComboBox = nullptr;
    QComboBox *statusBroadcastEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *statusTextLabelLineEdit                     = nullptr;
    QLineEdit *statusBatteryIdLineEdit                     = nullptr;
    QLineEdit *batteryLevelLineEdit                        = nullptr;
    QComboBox *batteryStateComboBox                        = nullptr;
    QComboBox *batteryChargingComboBox                     = nullptr;
    QComboBox *deviceStateComboBox                         = nullptr;
    QComboBox *displayStateComboBox                        = nullptr;
    QComboBox *weaponSightModeComboBox                     = nullptr;
    QComboBox *weaponActiveReticleIDComboBox               = nullptr;
    QComboBox *weaponSymbologyStateComboBox                = nullptr;

    QComboBox *simpleUiEndpointIdComboBox           = nullptr;
    QLineEdit *simpleUiEndpointDataflowLineEdit     = nullptr;
    QComboBox *simpleUiEndpointDistributionComboBox = nullptr;
    QComboBox *simpleUiEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *simpleUiTerminalIdLineEdit           = nullptr;
    QLineEdit *simpleUiCapabilitiesLineEdit         = nullptr;
    QComboBox *simpleUiButtonCountComboBox          = nullptr;
    QComboBox *simpleUiStateCountComboBox           = nullptr;
    QLineEdit *simpleUiTextLabelLineEdit            = nullptr;
    QLineEdit *packetGenTextLabelLineEdit           = nullptr;

    QComboBox *arEndpointIdComboBox                = nullptr;
    QLineEdit *arEndpointDataflowLineEdit          = nullptr;
    QComboBox *arEndpointDistributionComboBox      = nullptr;
    QComboBox *arEndpointDataPoliciesComboBox      = nullptr;
    QLineEdit *arServiceTextLabelLineEdit          = nullptr;

    QComboBox *arType1EndpointIdComboBox           = nullptr;
    QLineEdit *arType1EndpointDataflowLineEdit     = nullptr;
    QComboBox *arType1EndpointDistributionComboBox = nullptr;
    QComboBox *arType1EndpointDataPoliciesComboBox = nullptr;
    QLineEdit *arType1TextLabelLineEdit            = nullptr;

    QComboBox *arType2EndpointIdComboBox           = nullptr;
    QLineEdit *arType2EndpointDataflowLineEdit     = nullptr;
    QComboBox *arType2EndpointDistributionComboBox = nullptr;
    QComboBox *arType2EndpointDataPoliciesComboBox = nullptr;
    QLineEdit *arType2TextLabelLineEdit            = nullptr;
    QLineEdit *arType2MinWidthLineEdit             = nullptr;
    QLineEdit *arType2MinHeightLineEdit            = nullptr;
    QLineEdit *arType2RleVersionsLineEdit          = nullptr;

    QComboBox *lrfServiceEndpointIdComboBox           = nullptr;
    QLineEdit *lrfServiceEndpointDataflowLineEdit     = nullptr;
    QComboBox *lrfServiceEndpointDistributionComboBox = nullptr;
    QComboBox *lrfServiceEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *lrfServiceTextLabelLineEdit            = nullptr;
    QComboBox *lrfEnvironmentalSensorComboBox         = nullptr;
    QComboBox *lrfBallisticCalculatorComboBox         = nullptr;
    QComboBox *lrfQualityFactorComboBox               = nullptr;
    QComboBox *lrfErrorIndicatorComboBox              = nullptr;
    QComboBox *lrfMultipleTargetsComboBox             = nullptr;
    QComboBox *lrfDirectionalSensorsComboBox          = nullptr;
    QComboBox *lrfRangeComboBox                       = nullptr;
    QComboBox *lrfControlEndpointIdComboBox           = nullptr;
    QLineEdit *lrfControlEndpointDataflowLineEdit     = nullptr;
    QComboBox *lrfControlEndpointDistributionComboBox = nullptr;
    QComboBox *lrfControlEndpointDataPoliciesComboBox = nullptr;
    QLineEdit *lrfControlTextLabelLineEdit            = nullptr;

    QStringList strCapabilities;
    QComboBox *motionSenseEndpointIdComboBox                 = nullptr;
    QLineEdit *motionSenseEndpointDataflowLineEdit           = nullptr;
    QComboBox *motionSenseEndpointDistributionComboBox       = nullptr;
    QComboBox *motionSenseEndpointDataPoliciesComboBox       = nullptr;
    QLineEdit *motionSenseTextLabelLineEdit                  = nullptr;
    QLineEdit *motionSenseRateLineEdit                       = nullptr;
    QLineEdit *motionSenseAssociationServiceIdLineEdit       = nullptr;
    QLineEdit *motionSenseAssociationTypeLineEdit            = nullptr;
    QLineEdit *motionSenseAssociationServiceSelectorLineEdit = nullptr;

    QLineEdit *motionSenseAxesLineEdit      = nullptr;
    QLineEdit *motionSenseAccelAxesLineEdit = nullptr;
    QLineEdit *motionSenseGyroAxesLineEdit  = nullptr;
    QLineEdit *motionSenseMagAxesLineEdit   = nullptr;

    QLineEdit *positionSenseTextLabelLineEdit            = nullptr;
    QComboBox *positionSenseEndpointIdComboBox           = nullptr;
    QLineEdit *positionSenseEndpointDataflowLineEdit     = nullptr;
    QComboBox *positionSenseEndpointDistributionComboBox = nullptr;
    QComboBox *positionSenseEndpointDataPoliciesComboBox = nullptr;

    //! For availableLinksTable
    const int defaultAvailableLinkRowCount = 300;
    const int defaultAvailableLinkColCount = 4;

    QLineEdit *targetFlagLineEdit                 =  nullptr;
    QLineEdit *totalTargetsLineEdit               =  nullptr;
    QLineEdit *rangeLineEdit                      =  nullptr;
    QLineEdit *rangeErrorLineEdit                 =  nullptr;
    QLineEdit *azimuthLineEdit                    =  nullptr;
    QStringList azimuthNorthReference;
    QComboBox *azimuthNorthReferenceComboBox      =  nullptr;
    QLineEdit *azimuthErrorLineEdit               =  nullptr;
    QLineEdit *elevationLineEdit                  =  nullptr;
    QLineEdit *elevationErrorLineEdit             =  nullptr;
    QLineEdit *ballisticElevationHoldLineEdit     =  nullptr;
    QLineEdit *ballisticWindageHoldLineEdit       =  nullptr;
    QLineEdit *ballisticWindageConfidenceLineEdit =  nullptr;
    QLineEdit *temperatureLineEdit                =  nullptr;
    QLineEdit *humidityLineEdit                   =  nullptr;
    QLineEdit *pressureLineEdit                   =  nullptr;
    QLineEdit *qFactorLineEdit                    =  nullptr;
    uint8_t rangeEventId                          = 0;
    uint8_t targetIndex                           = 0;

    //! For registerServicesTable
    const int defaultRegSrvRowColCount = 1;
    QMutex registerMutex;
    QMutex deregisterMutex;
    QMutex revokeMutex;
    QMutex getStatusMutex;
    struct tableCell
    {
        int subscriberIndex;
        int providerIndex;
        int providerDataflowId;
        QLabel *statusLabel;
        QLabel *distributionLabel;
    };
    QMap<QCheckBox *, tableCell *> checkBoxMap;
    QPushButton *disableChecksumButton = nullptr;
    QPushButton *browseAllButton       = nullptr;
    QPushButton *regAllButton          = nullptr;
    QPushButton *getAllStatusButton    = nullptr;
    QPushButton *deregAllButton        = nullptr;
    bool verifyAdvertiseMessages       = false;

    QCheckBox *getStatusCheckBox       = nullptr;
    QCheckBox *registerCheckBox        = nullptr;
    QCheckBox *deregisterCheckBox      = nullptr;
    QCheckBox *revokeCheckBox          = nullptr;

    struct Timeouts
    {
        uint8_t autonomy;
        int requestTimeout;
        int keepAlivePeriod;
    };

    Timeouts getTimeoutParameters();

    struct KeepAlive
    {
        uint8_t providerIndex;
        uint8_t providerDataflowId;
        uint8_t subscriberIndex;
        uint8_t providerPeerIndex;
        uint32_t serviceSelector;
    };

    KeepAlive keepAlivePara;

    int SendKeepAliveMessage();

    //! Service List For SolNet registration
    QStringList serviceList;

    //! Elasped Timer
    QElapsedTimer elapsedTimer;

    //! Data Traffic testing
    bool dataTrafficButtonClicked;
    QBuffer transmitBuffer;
    QBuffer receiverBuffer;
    bool serialDevice = false;

    //! Power/temperature Measurements
    QGraphicsScene *powerMeasurementScene                = nullptr;
    PlotChart_default *pwrPlotChart                      = nullptr;
    PlotChart_default *powerMeasurementsPlotChart        = nullptr;
    QChartView *powerMeasurementsChartView               = nullptr;
    QGraphicsScene *temperatureMeasurementScene          = nullptr;
    PlotChart_default *temperaturePlotChart              = nullptr;
    PlotChart_default *temperatureMeasurementsPlotChart  = nullptr;
    QChartView *temperatureMeasurementsChartView         = nullptr;
    Dmm *dmmV                                            = nullptr;
    Dmm *dmmI                                            = nullptr;
    Dmm *dmmT                                            = nullptr;
    PowerSupply *powerSupplyP                            = nullptr;
    QString powerSupplyP_IP 			                 = "169.254.4.64";
    QString dmmV_IP 			                         = "169.254.4.62";
    QString dmmI_IP 			                         = "169.254.4.61";
    QString dmmT_IP 			                         = "169.254.4.63";
    QTimer *pwr_timer                                    = nullptr;
    QTimer *temperature_timer                            = nullptr;
    double Voltage                                       = 0;
    double Current                                       = 0;
    double Power                                         = 0;
    double Temperature                                   = 0;
    int powerCounter                                     = 0;
    int temperatureCounter                               = 0;
    double averageCurrent                                = 0;
    double batteryConsumed                               = 0;
    double batteryCapacity                               = 12000;

    //! Power Supply/RF Switch Control
    PowerSupply *RFSwitchController        = nullptr;
    QString RFSwitchController_IP          = "169.254.4.64";

    //! Checking coordinator role
    static const uint8_t ROLE_COORDINATOR    = 0x00;
    static const uint8_t ROLE_PEER           = 0x01;
    static const uint8_t COORDINATOR_CAPABLE = 1;
    static const uint8_t PEER_ONLY           = 0;

    uint16_t capabilitiesLvl[MAX_CAPABILITIES_LEVELS] = { 0 };
    uint16_t capabilitiesLevel                        = 0;
    QStringList iswSolNetCapabilitiesLevelList;

    //! Association Service Paramters for Motion Sense
    static const uint16_t MOTION_SENSE_ASSOCIATED_SERVICE_ID = 0x0001;
    static const uint8_t MOTION_SENSE_ASSOCIATION_TYPE       = 0x01;

    uint32_t generationId         = 0;
    uint32_t videoServiceSelector = 0;
    QVector<uint16_t> netLoad;
    QFile file;
    QTextStream descPlanId;
    int counter                   = 0;

    //! Network Load Data File
    QFile *networkLoadCsv = nullptr;

    //! Power Measurement String
    QString powerMeasurementStr = "";

    //! Temperature Measurement String
    QString temperatureMeasurementStr = "";

    QString inviteDevName = "";
    QString searchDevName = "";

    QStringList pollingAutonomyServiceList = {"TWS-Status", "LRF-Status"};
};

#endif // ISWTESTTOOL_H
