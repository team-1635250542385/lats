#include "digitaltimer.h"
#include <QDateTime>
#include <QTimerEvent>

DigitalTimer::DigitalTimer()
{

}

//! Handles timer events for the digital clock widget.
//! There are two different timers; one timer for updating the clock
//! and another one for switching back from date mode to time mode.
//!
void DigitalTimer::timerEvent( QTimerEvent *e )
{
    if ( e->timerId() == showDateTimer )        //! stop showing date
        stopDate();
    else {                                      //! normal timer
        if ( showDateTimer == -1 )              //! not showing date
            showTime();
    }
}

//! Shows the current date in the internal lcd widget.
//! Fires a timer to stop showing the date.
void DigitalTimer::showDate()
{
    if ( showDateTimer != -1 )                  //! already showing date
        return;
    QDate date = QDate::currentDate();
    QString s;
    s.sprintf( "%2d %2d", date.month(), date.day() );
    display( s );                               //! sets the LCD number/text
    showDateTimer = startTimer( 2000 );         //! keep this state for 2 secs
}

//! Stops showing the date.
void DigitalTimer::stopDate()
{
    killTimer( showDateTimer );
    showDateTimer = -1;
    showTime();
}

//! Shows the current time in the internal lcd widget.
QString DigitalTimer::showTime()
{
    QTime time = QTime::currentTime();
    QString text = time.toString("hh:mm:ss");
    if ((time.second() % 2) == 0)
    {
            text[2] = ' ';
    }

    return text;
}
