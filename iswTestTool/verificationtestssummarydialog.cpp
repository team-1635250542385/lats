#include "verificationtestssummarydialog.h"
#include "ui_verificationtestssummarydialog.h"

VerificationTestsSummaryDialog::VerificationTestsSummaryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VerificationTestsSummaryDialog)
{
    ui->setupUi(this);
}

VerificationTestsSummaryDialog::~VerificationTestsSummaryDialog()
{
    delete ui;
}

QString VerificationTestsSummaryDialog::getNumberOfCommandsPass()
{
    return ui->numOfCommandPassLineEdit->text();
}

QString VerificationTestsSummaryDialog::getNumberOfCommandsFail()
{
    return ui->numOfCommandsFailLineEdit->text();
}

QString VerificationTestsSummaryDialog::getPercentageOfRequiredCommandsPass()
{
    return ui->percentOfRequiredCommandsPassLineEdit->text();
}

void VerificationTestsSummaryDialog::setNumberOfCommandsFail(QString str)
{
    ui->numOfCommandsFailLineEdit->setText(str);
}

void VerificationTestsSummaryDialog::setNumberOfCommandsPass(QString str)
{
    ui->numOfCommandPassLineEdit->setText(str);
}

void VerificationTestsSummaryDialog::setPercentageOfRequiredCommandsPass(QString str)
{
    ui->percentOfRequiredCommandsPassLineEdit->setText(str + " %");
}

void VerificationTestsSummaryDialog::setCommand(QString str)
{
    verificationTableCell->setText(str);
    //! Increment Row
    row++;
}

void VerificationTestsSummaryDialog::setResult(QString str)
{
    verificationTableCell->setText(str);
    //! Increment Row
    row++;
}
