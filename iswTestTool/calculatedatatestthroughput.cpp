//!###############################################
//! Filename: calculatedatatestthroughput.cpp
//! Description: Class that provides an interface
//!              for the user to calculate the
//!              broadcast/multicast data throughput
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "calculatedatatestthroughput.h"

//! Project Includes
#include "../Logger/Logger.h"

//! C++ Includes
#include <map>
#include <string>

//! Qt includes
#include <QString>
#include <QDebug>
#include <QList>
#include <QMap>
#include <QMapIterator>
#include <QFile>
#include <QDateTime>
#include <QTime>

CalculateDataTestThroughput::CalculateDataTestThroughput(int activeTestId, Logger* theLogger, Logger* throughputTestLogger, Logger* throughputRestultsLogger)
{

    loggerFilename             = theLogger->getFilename();
    throughputTestFilename     = throughputTestLogger->getFilename();
    throughputResultsFilename  = throughputRestultsLogger->getFilename();
    loggerPtr                  = theLogger;
    throughputTestLoggerPtr    = throughputTestLogger;
    throughputResultsLoggerPtr = throughputRestultsLogger;

    testId             = activeTestId;
    testDuration       = 0;
    totalPayload       = 0;
    throughput         = 0;
    deviceId           = 0;
}

void CalculateDataTestThroughput::calculateThroughput(int testId)
{

    qDebug() << "IN CALCULATE DATA TEST THROUGHPUT";

    bool foundStartTestLine = false;
    bool foundEndTestLine   = false;
    long startTimestamp     = 0;
    long endTimestamp       = 0;
    long currentTimestamp   = 0;
    long nextSecTimestamp   = 0;
    long previousTimestamp  = 0;
    QString testStartStr    = "Starting Data Test " + QString::number(testId);
    QString testEndStr      = "Stopping Data Test " + QString::number(testId);
    QString dataLineStr     = "DeliverSolNetDataPacket";
    QString payloadCheck    = "Payload";
    QString testDurationStr = "";
    long secondPayload = 0;

    int sentMessageCnt = 0;

    std::cout << "Calculating throughput, loggerFilename: " << throughputTestFilename.c_str() << "\n";

    int lineCnt                       = 0;
    QString line                      = "";
    std::queue<std::string> dataLines = throughputTestLoggerPtr->getQueue();

    while(!foundEndTestLine && dataLines.size() > 0)
    {
        lineCnt++;

        line = dataLines.front().c_str();
        dataLines.pop();

        if (!line.isNull())
        {
            if (!line.isEmpty())
            {
                if(line.contains(testStartStr))
                {
                    qDebug() << "found test start string";
                    foundStartTestLine = true;
                    // Get start timestamp
                }

                if( line.contains(testEndStr))
                {
                    qDebug() << "found test end string";
                    // End of test window, exit file.
                    foundEndTestLine = true;
                }

                if(foundStartTestLine && !foundEndTestLine)
                {
                    // Get DeliverSolNetDataPacket line and extract data
                    if(line.contains(dataLineStr) && line.contains(payloadCheck))
                    {
                        sentMessageCnt++;

                        if(startTimestamp == 0)
                        {
                            startTimestamp    = line.split(" ")[4].toLong();
                            previousTimestamp = startTimestamp;
                            nextSecTimestamp  = startTimestamp + 1000000;
                        }

                        deviceId          = line.split(" ")[1].toInt();
                        int payloadLength = line.split(" ")[8].toLong();
                        currentTimestamp  = line.split(" ")[4].toLong();

                        totalPayload += payloadLength;
                        secondPayload += payloadLength;

                        if (currentTimestamp >= nextSecTimestamp)
                        {

                            outputThroughput(previousTimestamp, currentTimestamp, secondPayload);

                            previousTimestamp = currentTimestamp;
                            nextSecTimestamp  = currentTimestamp + 1000000;

                            // Remove payload data from this second range
                            secondPayload = 0;
                        }
                    }
                }
            }
        }
    }

    endTimestamp = currentTimestamp;

    throughputResultsLoggerPtr->LogSimpleMessage("Full Data Test Results Below For Test ID: " + QString::number(testId).toStdString());
    testDuration = endTimestamp - startTimestamp;

    testDurationStr = "\nTest Duration: " + (QString::number( testDuration / 1000000)) + " seconds";
    throughputResultsLoggerPtr->LogSimpleMessage(testDurationStr.toStdString());
    outputThroughput(startTimestamp, endTimestamp, totalPayload);

    // Clear any reamaining lines after test completion so they don't interfere with following tests.
    throughputTestLoggerPtr->clearQueue();

    totalPayload = 0;

    qDebug() << "throughput calc DONE";
}

void CalculateDataTestThroughput::outputThroughput(long startTime, long endTime, long payload)
{
    int elapsedTime = endTime - startTime;

    throughputResultsLoggerPtr->LogSimpleMessage("Time Range: " + QString::number(startTime).toStdString() + ", " + QString::number(endTime).toStdString());
    throughputResultsLoggerPtr->LogSimpleMessage("Duration: " + QString::number(elapsedTime).toStdString());
    throughputResultsLoggerPtr->LogSimpleMessage("Payload For Test " + QString::number(testId).toStdString() + ": " + QString::number(payload).toStdString());

    if (elapsedTime > 0)
    {
        throughput = (payload * 8) / elapsedTime;
    }

    dataResultStr = "Throughput For Test " + QString::number(testId) + " From Device " + QString::number(deviceId) + ": " + QString::number(throughput) + " Bits / s";
    throughputResultsLoggerPtr->LogSimpleMessage(dataResultStr.toStdString());

    // Writes all queued data to file. No thread for auto deQueueing
    fullyDequeue(throughputResultsLoggerPtr);
}

void CalculateDataTestThroughput::markStartDataTestTimeInLog(int testId)
{
    QString testIdStr = "Starting Data Test " + QString::number(testId);

    // Clear any logged messages before the test starts
    throughputTestLoggerPtr->clearQueue();
    throughputResultsLoggerPtr->clearQueue();

    throughputTestLoggerPtr->LogSimpleMessage(testIdStr.toStdString());
}

void CalculateDataTestThroughput::markEndDataTestTimeInLog(int testId)
{
    QString testIdStr = "Stopping Data Test " + QString::number(testId);

    throughputTestLoggerPtr->LogSimpleMessage(testIdStr.toStdString());
}

void CalculateDataTestThroughput::fullyDequeue(Logger *loggerPtr)
{
    int queueSize = loggerPtr->getLogQueueSize();

    while(queueSize > 0)
    {
        loggerPtr->deQueue();
        queueSize--;
    }
}
