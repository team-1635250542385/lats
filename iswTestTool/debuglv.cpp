//!###############################################
//! Filename: debuglv.cpp
//! Description: Class that provides an
//!              Dialog Box for selecting ISW debug levels
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "debuglv.h"
#include "ui_debuglv.h"
#include <QDebug>

debugLv::debugLv(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::debugLv)
{
    ui->setupUi(this);
    setWindowTitle("Set Debug Level");

    strList << "DEBUG_ISW_INTERFACE"        // 0
            << "DEBUG_ISW_SECURITY"         // 1
            << "DEBUG_ISW_FIRMWARE"         // 2
            << "DEBUG_ISW_IDENTITY"         // 3
            << "DEBUG_ISW_SYSTEM"           // 4
            << "DEBUG_ISW_ASSOCIATION"      // 5
            << "DEBUG_ISW_STREAM"           // 6
            << "DEBUG_ISW_SOLNET"           // 7
            << "DEBUG_ISW_METRICS"          // 8
            << "DEBUG_ISW_LINK"             // 9
            << "DEBUG_ISW_PRODUCT"          // 10
            << "DEBUG_USB_INTERFACE"        // 11
            << "DEBUG_ISW_ONBOARD"          // 12
            << "DEBUG_ISW_INTERFACEINIT"    // 13
            << "DEBUG_USB_INTERFACEINIT"    // 14
            << "DEBUG_SERIAL_INTERFACE"     // 15
            << "DEBUG_ISW_TEST";            // 16

    ui->debugLevelListWidget->addItems(strList);

    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        if(i == 3 || i == 4 || i == 5 || i == 6 || i == 9 || i == 10 || i == 2)
        {
            item = ui->debugLevelListWidget->item(i);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Checked);
        }
        else
        {
            item = ui->debugLevelListWidget->item(i);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Unchecked);
        }
    }
}

debugLv::~debugLv()
{
    if ( ui != nullptr )
        delete ui;
}

void debugLv::on_okPushButton_clicked()
{
    //! Get All Selected Items
    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        if(item->checkState() == Qt::Checked)
        {
            debugList << item->text();
        }
    }

    setDebugLevel(debugList);

    close();
}

void debugLv::setDebugLevel(QStringList dList)
{
    for (int i = 0; i < MAX_DEBUG_LEVELS; i++) {
        dbL[i] = 0;
    }

    if ( dList.contains("DEBUG_ISW_INTERFACE") ) {
        dbL[0] = Logger::DEBUG_ISW_INTERFACE;
    }
    if ( dList.contains("DEBUG_ISW_SECURITY") ) {
        dbL[1] = Logger::DEBUG_ISW_SECURITY;
    }
    if ( dList.contains("DEBUG_ISW_FIRMWARE") ) {
        dbL[2] = Logger::DEBUG_ISW_FIRMWARE;
    }
    if ( dList.contains("DEBUG_ISW_IDENTITY") ) {
        dbL[3] = Logger::DEBUG_ISW_IDENTITY;
    }
    if ( dList.contains("DEBUG_ISW_SYSTEM") ) {
        dbL[4] = Logger::DEBUG_ISW_SYSTEM;
    }
    if ( dList.contains("DEBUG_ISW_ASSOCIATION") ) {
        dbL[5] = Logger::DEBUG_ISW_ASSOCIATION;
    }
    if ( dList.contains("DEBUG_ISW_STREAM") ) {
        dbL[6] = Logger::DEBUG_ISW_STREAM;
    }
    if ( dList.contains("DEBUG_ISW_SOLNET") ) {
        dbL[7] = Logger::DEBUG_ISW_SOLNET;
    }
    if ( dList.contains("DEBUG_ISW_METRICS") ) {
        dbL[8] = Logger::DEBUG_ISW_METRICS;
    }
    if ( dList.contains("DEBUG_ISW_LINK") ) {
        dbL[9] = Logger::DEBUG_ISW_LINK;
    }
    if ( dList.contains("DEBUG_ISW_PRODUCT") ) {
        dbL[10] = Logger::DEBUG_ISW_PRODUCT;
    }
    if ( dList.contains("DEBUG_USB_INTERFACE") ) {
        dbL[11] = Logger::DEBUG_USB_INTERFACE;
    }
    if ( dList.contains("DEBUG_ISW_ONBOARD") ) {
        dbL[12] = Logger::DEBUG_ISW_ONBOARD;
    }
    if ( dList.contains("DEBUG_ISW_INTERFACEINIT") ) {
        dbL[13] = Logger::DEBUG_ISW_INTERFACEINIT;
    }
    if ( dList.contains("DEBUG_USB_INTERFACEINIT") ) {
        dbL[14] = Logger::DEBUG_USB_INTERFACEINIT;
    }
    if ( dList.contains("DEBUG_SERIAL_INTERFACE") ) {
        dbL[15] = Logger::DEBUG_SERIAL_INTERFACE;
    }
    if ( dList.contains("DEBUG_ISW_TEST") ) {
        dbL[16] = Logger::DEBUG_ISW_TEST;
    }

    if(ui->lightModeRadioButton->isChecked())
    {
        light_mode = true;
    }

    debugLevel = 0;
    for (uint16_t i: dbL) {
        debugLevel = debugLevel | i;
    }
}

uint16_t debugLv::getDebugLevel()
{
    return debugLevel;
}

QStringList debugLv::getDebugList()
{
    return strList;
}

uint16_t* debugLv::getDebugLevelArray()
{
    return dbL;
}

bool debugLv::getLightMode()
{
    return light_mode;
}


void debugLv::on_checkAllPushButton_clicked()
{
    //! Get All Selected Items and Enable Check
    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        item->setCheckState(Qt::Checked);
    }
}

void debugLv::on_clearAllPushButton_clicked()
{
    //! Get All Selected Items and Disable Check
    for(int i = 0; i < ui->debugLevelListWidget->count(); ++i)
    {
        item = ui->debugLevelListWidget->item(i);
        item->setCheckState(Qt::Unchecked);
    }
}
