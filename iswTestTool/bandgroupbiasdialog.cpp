#include "bandgroupbiasdialog.h"
#include "ui_bandgroupbiasdialog.h"
#include <QDebug>

BandGroupBiasDialog::BandGroupBiasDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BandGroupBiasDialog)
{
    ui->setupUi(this);
}

BandGroupBiasDialog::~BandGroupBiasDialog()
{
    delete ui;
}

uint8_t BandGroupBiasDialog::getBandgroupPrefer()
{
    switch(ui->bgPreferComboBox->currentIndex())
    {
        case 0:
        {
            bgPrefer = 0x02;
            qDebug() << "Bandgroup Prefer: " << ui->bgPreferComboBox->currentText();
            break;
        }
        case 1:
        {
            bgPrefer = 0x08;
            qDebug() << "Bandgroup Prefer: " << ui->bgPreferComboBox->currentText();
            break;
        }
        case 2:
        {
            bgPrefer = 0x40;
            qDebug() << "Bandgroup Prefer: " << ui->bgPreferComboBox->currentText();
            break;
        }
        default:
        {
            bgPrefer = 0x02;
            qDebug() << "Bandgroup Prefer: " << ui->bgPreferComboBox->currentText();
            break;
        }
    }
    return bgPrefer;
}
uint8_t BandGroupBiasDialog::getBandgroupAvoid()
{
    switch(ui->bgAvoidComboBox->currentIndex())
    {
        case 0:
        {
            bgAvoid = 0x02;
            qDebug() << "Bandgroup Avoid: " << ui->bgAvoidComboBox->currentText();
            break;
        }
        case 1:
        {
            bgAvoid = 0x08;
            qDebug() << "Bandgroup Avoid: " << ui->bgAvoidComboBox->currentText();
            break;
        }
        case 2:
        {
            bgAvoid = 0x40;
            qDebug() << "Bandgroup Avoid: " << ui->bgAvoidComboBox->currentText();
            break;
        }
        default:
        {
            bgAvoid = 0x02;
            qDebug() << "Bandgroup Avoid: " << ui->bgAvoidComboBox->currentText();
            break;
        }
    }
    return bgAvoid;
}
uint8_t BandGroupBiasDialog::getPersist()
{
    switch(ui->persistComboBox->currentIndex())
    {
        case 0: // Persist In NVRAM
        {
            persist = 0x01;
            qDebug() << "Persist: " << ui->persistComboBox->currentText();
            break;
        }
        case 1: // Do Not Persist In NVRAM
        default:
        {
            persist = 0x00;
            qDebug() << "Persist: " << ui->persistComboBox->currentText();
            break;
        }
    }

    return persist;
}
