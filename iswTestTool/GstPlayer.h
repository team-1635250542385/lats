//!#####################################################
//! Filename: GstPlayer.h
//! Description: Class that provides an interface
//!              to receive video data from iswTestTool
//!              object running in a different thread.
//!              This class uses Linux GStreamer library
//!              to pass data between the threads.
//!              Please see GStreamer documentation
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef GstPlayer_H
#define GstPlayer_H

#include <QThread>
#include <QByteArray>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <gst/gstclock.h>


class GstPlayer : public QObject
{
        Q_OBJECT
public:
    GstPlayer(uint8_t testType, uint16_t videoType, uint16_t pixelFormat);
    ~GstPlayer();

    //! Used to determine if the GstPlayer ojbect is running
    //! from another thread
    bool isPlayerRunning() { return isPlaying; }

    //! Tell GstPlayer to start receiving
    void startReceiving();
    //! Tell GstPlayer to stop receiving
    void stopReceiving();

public Q_SLOTS:
    //! VideoGenerator Class start both GstSender and GstPlayer
    //! VideoGenerator Class setus up the connections
    //! this slot is connected to the SIGNAL in iswTestTool
    //! so that video input from the camera or test pattern is
    //! sent to iswTestTool, then packaged into SolNet data and
    //! sent to this application
    void receiveVideoDataInPlayer(const QByteArray& bytes);

private:
    //! Needed to pass data in GstStreamer
    void initGStreamerPipeline();
    static void getBusMessage(GstBus *bus, GstMessage *message, void *thisPtr);

private:
    bool isPlaying = false; //! This is for our test app control
    bool is_live   = false;   //! This is a GStreamer setting

    static const uint8_t CAMERA_TEST_TYPE = 30;
    uint8_t videoTestType                 = 0; //! default is pattern test zero
    uint16_t videoFormatType              = 0;
    uint16_t videoPixelFormat             = 0;

    //! GStreamer flags
    typedef enum
    {
        GST_PLAY_FLAG_VIDEO         = (1 << 0), /* We want video output */
        GST_PLAY_FLAG_AUDIO         = (1 << 1), /* We want audio output */
        GST_PLAY_FLAG_TEXT          = (1 << 2)  /* We want subtitle output */
    } GstPlayFlags;

    //! These are a GStreamer variables
    GstElement *pipeline = nullptr;
    GstBus *bus          = nullptr;

    GstElement *source        = nullptr;
    GstElement *videoQueue2   = nullptr;
    GstElement *rawVideoParse = nullptr;
    GstElement *videoConvert  = nullptr;
    GstElement *sink          = nullptr;

    GstClockTime timestamp    = 0;

    //! Used to send data to iswTestTool
    int frameSize = 0; //! Used in video frame format
    QByteArray *frameData;
};

#endif
