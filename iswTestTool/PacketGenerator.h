//!##########################################################
//! Filename: packetGenerator.h
//! Description: Class that allows the user to setup,
//!              start, and stop a data test from the
//!              iswTestTool GUI. The tests are run
//!              from the Linux host to the firmare and
//!              across the wireless to another node and
//!              received at the Linux host on the other
//!              node. Currenty the data is a fixed pattern.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef PACKETGENERATOR_H
#define PACKETGENERATOR_H

#include <QDialog>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QGridLayout>
#include <QTableWidget>
#include <pthread.h>
#include "iswtesttool.h"

//! Project Includes
#include <calculatedatatestthroughput.h>

namespace Ui {
class PacketGenerator;
}

class iswTestTool;

class PacketGenerator : public QDialog
{
    Q_OBJECT

public:
    explicit PacketGenerator(iswTestTool *testToolPtr, void *userData, QWidget *parent = nullptr);
    explicit PacketGenerator(iswTestTool *testToolPtr, CommonButtonParameters *cbp, int duration, int playbookPacketSize, int playbookDelay, bool burstMode, int playbookPacketNum, QWidget *parent = nullptr);
    ~PacketGenerator(void);

    //! Used to determine the state of the test
    bool isTestRunning(void) { return isRunning; }
    QButtonGroup *buttonGroup = nullptr;
    Logger* loggerPtr         = nullptr;
    int testId                = 0;

public slots:
    void onGroupButtonClicked(int id);
    static void receiveDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr);
    void updateNumPacketsReceived();

    //! Connected to the user button to start the test
    void startDataTest();

    //! Connected to the user button to stop the test
    void stopDataTest(bool flushProviderQueue, bool flushSubscriberQueue);

    //! Clear the number of packets sent and received columns
    void clear(void);

    uint16_t getSerialThroughput() {return serialThroughput;};

    //! Reset Payload Size and Delay To Default Values
    void defaultParameters();



signals:
    void packetReceived();



private:
    //! Sets up the pop up window/table for user
    //! configuration and start/stop of the test
    //! Shows all the video choices
    void createDataTestGroup(void);
    void displayMessage(QString string);
    CalculateDataTestThroughput* cdtt;

    void writeToCsv(qint64 timestamp, uint8_t rssi, float PhyRate, double txMbps, double rxMbps, QString linkQuality, uint8_t activeAntennaID);

private:
    Ui::PacketGenerator *ui;
    QPushButton *startTestPushButton = nullptr;
    QPushButton *stopTestPushButton  = nullptr;
    QPushButton *defaultPushButton   = nullptr;
    iswTestTool *iswToolPtr          = nullptr;
    bool isRunning                   = false;
    CommonButtonParameters *cbp      = nullptr;

    bool playbookRunning = false;
    bool validInput = true;

    static const int START_BUTTON_ID   = 1;
    static const int STOP_BUTTON_ID    = 2;
    static const int DEFAULT_BUTTON_ID = 3;

    //! Shows which device is sending(provider)
    //! the video and which is receiving(subscriber)
    QTableWidgetItem providerColumn;
    QTableWidgetItem subscriberColumn;
    //QTableWidgetItem packetSizeColumn;
    QTableWidgetItem dataRateColumn;
    QTableWidgetItem burstModeColumn;
    QTableWidgetItem testDurationColumn;

    //! Number of packets to send; 0 = until stop; non-zero = send that amount
    //! User modifies
    QTableWidgetItem numPacketsToSendColumn;
    QTableWidgetItem numPacketsRecvdColumn;

    //! Delay between packets transmitted in microseconds
    //! User modifies
    //QTableWidgetItem microsecondSleepColumn;
    QTableWidgetItem throughputColumn;
    QTableWidgetItem rssiColumn;
    QTableWidgetItem txMbpsColumn;
    QTableWidgetItem rxMbpsColumn;
    QTableWidgetItem PERColumn;
    QPushButton clearButton;
    QRadioButton burstModeButton;

    bool burstModeActive = false;
    // Sends data burst every 4 seconds
    int burstDownTime = 4;
    // A single burst lasts 1 second
    int burstOnTime = 1;
    // Duration of a burst instance including the burst and down time.
    int totalBurstWindowTime = burstDownTime + burstOnTime;
    // Total Packets in one window, dead time and burst included.
    int packetsPerBurstWindow;
    // Packets sent in a burst
    int packetsPerBurst;
    // Keeps track of which packet we are on within a burst
    int packetInBurstWindowTracker = 0;
    // How many bursts are sent during the duration
    int totalBursts = 1;
    // Track how many bursts have been sent;
    int completedBurstCount = 0;

    bool updateBurstData();
    bool isValidThroughput(QString textInput);
    int getPacketsPerSecondFromDataRate(int dataRate);


    //! Number of SolNet Data Packets Sent
    float_t packetSentCtr = 0;

    //! Number of SolNet Data Packets we receive
    uint32_t numPacketsReceived = 0;
    uint32_t numPacketsSent     = 0;

    //! Number of SolNet Data Packets the user configures to send
    uint32_t numPacketsToSend   = 0;
    uint16_t packetSize_s       = 16;
    qint64 diff                 = 0;
    qint64 sTimestamp           = 0;
    uint16_t serialDataRate     = 0;
    uint16_t serialThroughput   = 0; // uint16_t

    QFuture<void> checkThroughputThread;
    static void getThroughput(PacketGenerator *thisPtr);
    QFuture<void> sendThread;
    static void sendData(PacketGenerator *thisPtr);

    //! Radio Buttons for the user to select
    //! the data test - one of several patterns
    QVBoxLayout *vbox            = nullptr;
    QGridLayout *grid            = nullptr;
    QGroupBox *dataTestsGroupBox = nullptr;
    QRadioButton *pattern0Button = nullptr;
    QRadioButton *pattern1Button = nullptr;
    QRadioButton *pattern2Button = nullptr;
    int dataTestPattern          = 0;
    int dataRate                 = 10;      // Mbps
    int testDuration             = 5;       // Minutes
    uint16_t packetSize          = 4064;    // Was 4040? been using 4064
    int microsecondSleep         = 250;
    QStringList tableHeader;

    //! IswInterface pointers to the source and destination
    //! inside the ISW Library
    IswInterface *providerIswIntrfcPtr   = nullptr;
    IswInterface *subscriberIswIntrfcPtr = nullptr;
    IswSolNet *providerIswSolNetPtr      = nullptr;
    IswSolNet *subscriberIswSolNetPtr    = nullptr;
    DBLogRecord *dbLogRecordPtr          = nullptr;

    //! Packet Generator Data File
    QFile *packetGeneratorCsv = nullptr;
};

#endif // PACKETGENERATOR_H
