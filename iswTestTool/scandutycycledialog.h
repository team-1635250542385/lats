//!##########################################################
//! Filename: scandutycycledialog.h
//! Description: Class that provides an dialog
//!              that allows the user to configure
//!              the scan duty cycle parameters for
//!              the selected radio.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef SCANDUTYCYCLEDIALOG_H
#define SCANDUTYCYCLEDIALOG_H

#include <QDialog>

namespace Ui {
class ScanDutyCycleDialog;
}

class ScanDutyCycleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ScanDutyCycleDialog(QWidget *parent = nullptr);
    ~ScanDutyCycleDialog();
    uint8_t getScanDutyCycle() const;
    uint16_t getStartupScanDuration() const;

private:
    Ui::ScanDutyCycleDialog *ui;

    uint8_t scanDutyCycle        = 16; // Default Value
    uint16_t startupScanDuration = 4; // Default Value
};

#endif // SCANDUTYCYCLEDIALOG_H
