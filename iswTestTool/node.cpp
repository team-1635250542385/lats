//!###############################################
//! Filename: node.cpp
//! Description: Class that creates an object within
//!              the network topology every time a physical device
//!              joins the network.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "edge.h"
#include "node.h"

#include <unistd.h>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOption>
#include <QMessageBox>


Node::Node(QGraphicsView *graphWidget, NetTopologyGraphicsScene *scene) :
    netGraphicsView(graphWidget),
    netScene(scene)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(-1);

    edgeList.clear();
}

Node::~Node()
{
    if ( labelWidget != nullptr )
    {
        QLabel* label = (QLabel *)labelWidget->widget();
        if ( label != nullptr )
        {
            delete label;
        }
    }

    clearAllEdges();
}

QList<Edge *> Node::edges() const
{
    return edgeList;
}

void Node::setNodeColor(QColor color)
{
    nodeColor = color;
}

void Node::clearAllEdges()
{
    edgeListLock.lock();

    if ( (edgeList.isEmpty() == false) &&
         (edgeList.size() > 0) )
    {
        for (int i = 0; i < edgeList.size(); ++i)
        {
            if ( edgeList.at(i) != nullptr )
            {
                if ( (netScene != nullptr) &&
                     (edgeList.at(i)->scene() == netScene) )
                {
                    netScene->removeItem(edgeList.at(i));
                    netScene->update();
                }
                edgeList.removeAt(i);
            }
        }
    }

    edgeList.clear();
    netScene->update();

    edgeListLock.unlock();
}

void Node::addEdge(Edge *edge)
{
    edgeListLock.lock();

    edgeList << edge;
    edge->adjust();

    QList<QGraphicsItem *> netTopologySceneItems = netScene->items();

    if ( netTopologySceneItems.contains(edge) == false )
    {
        netScene->addItem(edge);
        netScene->update();
    }

    edgeListLock.unlock();
}

void Node::removeEdge(Edge *edge)
{
    edgeListLock.lock();

    for (int i = 0; i < edgeList.size(); ++i)
    {
        if ( (edgeList.at(i) != nullptr) &&
             (edgeList.at(i) == edge) )
        {
            if ( netScene != nullptr )
            {
                // Remove for source and dest nodes
                netScene->removeItem(edge);
                netScene->update();
            }

            edgeList.removeAt(i);
        }
    }

    if ( edgeList.size() == 0 )
    {
        edgeList.clear();
    }

    edgeListLock.unlock();
}

Edge *Node::findEdge(Node *sourceNode, Node *destNode)
{
    Edge *edge = nullptr;

    edgeListLock.lock();

    for (int i = 0; i < edgeList.size(); ++i)
    {
        if ( ((edgeList.at(i)->sourceNode() == sourceNode) &&
              (edgeList.at(i)->destNode() == destNode)) ||
             ((edgeList.at(i)->sourceNode() == destNode) &&
              (edgeList.at(i)->destNode() == sourceNode)) )
        {
            // It's the same edge either way
            edge = edgeList.at(i);
            break;
        }

    }

    edgeListLock.unlock();

    return (edge);
}

void Node::calculateForces()
{
    if (!scene() || scene()->mouseGrabberItem() == this)
    {
        newPos = pos();
        return;
    }

    // Sum up all forces pushing this item away
    qreal xvel = 0;
    qreal yvel = 0;
    foreach (QGraphicsItem *item, scene()->items())
    {
        Node *node = qgraphicsitem_cast<Node *>(item);
        if (!node)
            continue;

        QPointF vec = mapToItem(node, 0, 0);
        qreal dx = vec.x();
        qreal dy = vec.y();
        double l = 2.0 * (dx * dx + dy * dy);
        if (l > 0)
        {
            xvel += (dx * 150.0) / l;
            yvel += (dy * 150.0) / l;
        }
    }

    // Now subtract all forces pulling items together
    double weight = (edgeList.size() + 1) * 10;
    foreach (Edge *edge, edgeList)
    {
        QPointF vec;
        if (edge->sourceNode() == this)
            vec = mapToItem(edge->destNode(), 0, 0);
        else
            vec = mapToItem(edge->sourceNode(), 0, 0);
        xvel -= vec.x() / weight;
        yvel -= vec.y() / weight;
    }

    if (qAbs(xvel) < 0.1 && qAbs(yvel) < 0.1)
        xvel = yvel = 0;

    QRectF sceneRect = scene()->sceneRect();
    newPos = pos() + QPointF(xvel, yvel);
    newPos.setX(qMin(qMax(newPos.x(), sceneRect.left() + 10), sceneRect.right() - 10));
    newPos.setY(qMin(qMax(newPos.y(), sceneRect.top() + 10), sceneRect.bottom() - 10));
}

bool Node::advancePosition()
{
    if (newPos == pos())
        return false;

    setPos(newPos);

    return true;
}

QRectF Node::boundingRect() const
{
    qreal adjust = 2;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20); //-10, -10, 20, 20
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    painter->setPen(Qt::NoPen);
    painter->setBrush(nodeColor);
    painter->drawEllipse(-10, -10, 20, 20); //-7, -7, 20, 20

    painter->setPen(QPen(nodeColor, 0));
    painter->drawEllipse(-10, -10, 20, 20); // -10, -10, 20, 20
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if ( value.isValid() &&
         (value.isNull() == false) )
    {
        switch (change)
        {
            case ItemPositionHasChanged:
            {
                edgeListLock.lock();

                // There is a case where we got into this function with
                // an edgeList that was garbage, so added the extra checks
                if ( (edgeList.isEmpty() == false) &&
                     (edgeList.size() > 0) )
                {
                    foreach (Edge *edge, edgeList)
                    {
                        if ( edge != nullptr )
                        {
                            edge->adjust();
                        }
                    }
                    QGraphicsItem::itemChange(change, value);
                }

                edgeListLock.unlock();
                break;
            }
            default:
                break;
        };
    }

    return (value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if ( netScene->getClicksEnabled() )
    {
        update();
        QGraphicsItem::mousePressEvent(event);
    }
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if ( netScene->getClicksEnabled() )
    {
        update();
        QGraphicsItem::mouseMoveEvent(event);
    }
}
