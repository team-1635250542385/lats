//!#####################################################
//! Filename: GstSender.h
//! Description: Class that provides an interface
//!              to send video data to the iswTestTool
//!              object running in a different thread.
//!              This class uses Linux GStreamer library
//!              to pass data between the threads.
//!              Please see GStreamer documentation
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef GstSender_H
#define GstSender_H

#include <QThread>
#include <QByteArray>
#include <gst/gst.h>
#include <gst/app/gstappsrc.h>


class GstSender : public QObject
{
        Q_OBJECT
public:
    GstSender(uint8_t testType, uint16_t videoType, uint16_t pixelFormat);
    ~GstSender();

    //! Used to determine if the GstPlayer ojbect is running
    //! from another thread
    bool isSenderRunning() { return isSending; }

    //! Tell GstSender to start sending
    void startSending();
    //! Tell GstSender to start sending
    void stopSending();

    //! constants for video frame configuration
    const static uint16_t PIXEL_FORMAT_YV12      = 0x0032;
    const static uint16_t PIXEL_FORMAT_Y         = 0x0001;
    const static uint16_t PIXEL_FORMAT_NV21      = 0x0002;
    const static uint16_t PIXEL_FORMAT_YUY2      = 0x0003;
    const static uint16_t PIXEL_FORMAT_NV16      = 0x0004;
    const static uint16_t PIXEL_FORMAT_RGBA_4444 = 0x0020;
    const static uint16_t PIXEL_FORMAT_RGBA_8888 = 0x0021;
    const static uint16_t PIXEL_FORMAT_RGBA_5551 = 0x0022;
    const static uint16_t PIXEL_FORMAT_RGBX_8888 = 0x0023;
    const static uint16_t PIXEL_FORMAT_RGB_332   = 0x0030;
    const static uint16_t PIXEL_FORMAT_RGB_565   = 0x0031;
    const static uint16_t PIXEL_FORMAT_RGB_888   = 0x0032;

Q_SIGNALS:
    //! VideoGenerator Class start both GstSender and GstPlayer
    //! VideoGenerator Class setus up the connections
    //! this SIGNAL is connected to the SLOT in iswTestTool
    //! so that video input from the camera or test pattern is
    //! sent to iswTestTool, then packaged into SolNet data
    void videoFromSourceAvailable(const QByteArray& sourceData);

private:
    //! Needed to pass data in GstStreamer
    void initGStreamerPipeline();
    static void getBusMessage(GstBus *bus, GstMessage *message, void *thisPtr);
    static GstPadProbeReturn probePipeline (GstPad *pad, GstPadProbeInfo *info, gpointer userData);

private:
    bool isSending = false; //! This is for our test app control
    static const uint8_t CAMERA_TEST_TYPE = 30;
    uint8_t videoTestType                 = 0; //! default is pattern test zero
    uint16_t videoFormatType              = 0;
    uint16_t videoPixelFormat             = 0;

    //! These are a GStreamer variables
    GstElement *pipeline     = nullptr;
    GstBus *bus              = nullptr;

    GstAppSrc *source        = nullptr;
    GstElement *videoQueue   = nullptr;
    GstElement *videoConvert = nullptr;
    GstElement *videoScale   = nullptr;
    GstElement *capsFilter2  = nullptr;
    GstElement *videoRate    = nullptr;
    GstElement *videoQueue2  = nullptr;
    GstElement *sink         = nullptr;
    GstPad *pad              = nullptr;
};

#endif
