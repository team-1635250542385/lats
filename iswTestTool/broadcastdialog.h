//!###############################################
//! Filename: broadcastdialog.h
//! Description: Class that provides an
//!              Dialog Box for broadcast data configuration
//!###############################################

#ifndef BROADCASTDIALOG_H
#define BROADCASTDIALOG_H

#include <QDialog>

namespace Ui {
class BroadcastDialog;
}

class BroadcastDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BroadcastDialog(QWidget *parent = nullptr);
    ~BroadcastDialog();

private:
    Ui::BroadcastDialog *ui;
};

#endif // BROADCASTDIALOG_H
