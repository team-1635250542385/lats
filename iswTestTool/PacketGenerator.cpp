//!##########################################################
//! Filename: PacketGenerator.cpp
//! Description: Class that allows the user to setup,
//!              start, and stop a data test from the
//!              iswTestTool GUI. The tests are run
//!              from the Linux host to the firmare and
//!              across the wireless to another node and
//!              received at the Linux host on the other
//!              node. Currenty the data is a fixed pattern.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "PacketGenerator.h"
#include "ui_PacketGenerator.h"
#include <unistd.h>
#include <QButtonGroup>
#include <QMessageBox>
#include <QFile>
#include <QDateTime>
#include "../Logger/Logger.h"
#include <QDebug>

//! Project Includes
#include <calculatedatatestthroughput.h>

//!########################################################
//! Constructor: packetGenerator class
//! Inputs: - pointer to the iswTestTool object
//!         - userData pointer to a CommonButtonParameters
//!         struct of information needed to send/receive
//!         data
//!         - parent widget
//!########################################################
PacketGenerator::PacketGenerator(iswTestTool *testToolPtr, void *userData, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PacketGenerator),
    iswToolPtr(testToolPtr)
{
    ui->setupUi(this);

    cbp                    = (CommonButtonParameters *)userData;
    providerIswIntrfcPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswInterface;
    subscriberIswIntrfcPtr = iswToolPtr->DynArray[cbp->subscriberIndexInDynArray].iswInterface;
    providerIswSolNetPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswSolNet;
    loggerPtr              = iswToolPtr->getLogger();
    testId                 = iswToolPtr->getTestId();

    //cdtt = new CalculateDataTestThroughput(testId, loggerPtr,iswToolPtr->getThroughputTestLogger(), iswToolPtr->getThroughputResutlsLogger());
    setWindowTitle("Packet Generator: " + cbp->provider + " to " + cbp->subscriber);

    if ( (providerIswIntrfcPtr == nullptr) ||
         (subscriberIswIntrfcPtr == nullptr) ||
         (providerIswSolNetPtr == nullptr) )
    {
        displayMessage("Provider or Subscriber Device Is Not Available - Test Ending!");
        return;
    }

    //! Setup the control buttons: start, stop, cancel
    startTestPushButton = new QPushButton(this);
    startTestPushButton->setObjectName(QString::fromUtf8("startTestPushButton"));
    startTestPushButton->setText("Start Data Traffic");
    startTestPushButton->setGeometry(QRect(120, 164, 122, 28));
    startTestPushButton->setCheckable(true);
    startTestPushButton->setAutoExclusive(true);

    stopTestPushButton = new QPushButton(this);
    stopTestPushButton->setObjectName(QString::fromUtf8("stopTestPushButton"));
    stopTestPushButton->setText("Stop Data Traffic");
    stopTestPushButton->setGeometry(QRect(360, 164, 122, 28));
    stopTestPushButton->setCheckable(true);
    stopTestPushButton->setAutoExclusive(true);

    defaultPushButton = new QPushButton(this);
    defaultPushButton->setObjectName(QString::fromUtf8("defaultPushButton"));
    defaultPushButton->setText("Set Default");
    defaultPushButton->setGeometry(QRect(600, 164, 122, 28));
    defaultPushButton->setCheckable(true);
    defaultPushButton->setAutoExclusive(true);

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(startTestPushButton);
    buttonGroup->setId(startTestPushButton, START_BUTTON_ID);

    buttonGroup->addButton(stopTestPushButton);
    buttonGroup->setId(stopTestPushButton, STOP_BUTTON_ID);

    buttonGroup->addButton(defaultPushButton);
    buttonGroup->setId(defaultPushButton, DEFAULT_BUTTON_ID);

    createDataTestGroup();

    ui->tableWidget->update();
    QApplication::processEvents();
}

//!########################################################
//! Constructor: packetGenerator class
//! Used for playbooks
//! Inputs: - pointer to the iswTestTool object
//!         - providerIndex: device sending data
//!         - subscriberIndex: device receiving data
//!         - parent widget
//!########################################################
PacketGenerator::PacketGenerator(iswTestTool *testToolPtr, CommonButtonParameters *passedCbp, int duration, int playbookPacketSize, int playbookDelay, bool burstMode, int playbookPacketNum, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PacketGenerator),
    iswToolPtr(testToolPtr)
{
    ui->setupUi(this);

    qDebug() << "passed delay into constructor: " << playbookDelay;
    cbp = passedCbp;
    providerIswIntrfcPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswInterface;
    subscriberIswIntrfcPtr = iswToolPtr->DynArray[cbp->subscriberIndexInDynArray].iswInterface;
    providerIswSolNetPtr   = iswToolPtr->DynArray[cbp->providerIndexInDynArray].iswSolNet;
    loggerPtr              = iswToolPtr->getLogger();
    testId                 = iswToolPtr->getTestId();

    packetSize         = playbookPacketSize;
    numPacketsToSend   = playbookPacketNum;
    microsecondSleep   = playbookDelay;
    playbookRunning    = true;
    burstModeActive    = burstMode;

    qDebug() << "delay in pgen constructor: " << microsecondSleep;

    //cdtt = new CalculateDataTestThroughput(testId, loggerPtr,iswToolPtr->getThroughputTestLogger(), iswToolPtr->getThroughputResutlsLogger());

    if ( (providerIswIntrfcPtr == nullptr) ||
         (subscriberIswIntrfcPtr == nullptr) ||
         (providerIswSolNetPtr == nullptr) )
    {
        displayMessage("Provider or Subscriber Device Is Not Available - Test Ending!");
        return;
    }

    ui->tableWidget->update();
    QApplication::processEvents();

    qDebug() << "through pgen constructor";
}

PacketGenerator::~PacketGenerator()
{
    if ( cbp != nullptr )
    {
        delete (cbp);
    }

    if ( startTestPushButton != nullptr )
    {
        delete (startTestPushButton);
    }

    if ( stopTestPushButton != nullptr )
    {
        delete (stopTestPushButton);
    }

    if ( buttonGroup != nullptr )
    {
        delete (buttonGroup);
    }

    if ( ui != nullptr )
    {
        delete ui;
    }
}

//!##################################################
//! createDataTestTable()
//! Sets up the pop up window/table for user
//! configuration and start/stop of the test
//!##################################################
void PacketGenerator::createDataTestGroup()
{
    //! Show the user the provider and subscriber
    ui->dataTestTable->setShowGrid(true);
    ui->dataTestTable->setRowCount(1);
    ui->dataTestTable->setColumnCount(12);

    if (providerIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard)
    {
        tableHeader << "Provider" << "Subscriber" << "Data Rate (Mbps)" << "Burst Mode" << "Time (Min)" << "# to Send"  << "# Rcvd" << "Avg. RSSI" << "Tx (kbps)" << "Rx (kbps)" << "PER (%)" << "Clear";
    }
    else if(subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard)
    {
        tableHeader << "Provider" << "Subscriber" << "Data Rate (Mbps)" << "Burst Mode" << "Time (Min)" << "# to Send" << "# Rcvd " << "Avg. RSSI" << "Tx (kbps)" << "Rx (kbps)" << "PER (%)" << "Clear";
    }
    else
    {
        tableHeader << "Provider" << "Subscriber" << "Rate (Mbps)" << "Burst Mode" << "Time (Min)" << "# to Send" << "# Rcvd" << "Avg. RSSI" << "Tx (Mbps)" << "Rx (Mbps)" << "PER (%)" << "Clear";
    }

    ui->dataTestTable->setHorizontalHeaderLabels(tableHeader);
    ui->dataTestTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->dataTestTable->horizontalHeader()->setResizeContentsPrecision(true);
    ui->dataTestTable->horizontalHeader()->setStyleSheet("QHeaderView { font-size: 8pt; }");

    uint8_t row = 0;

    //! Provider Column
    providerColumn.setText(cbp->provider);
    ui->dataTestTable->setItem(row, 0, &providerColumn);

    //! Subscriber Column
    subscriberColumn.setText(cbp->subscriber);
    ui->dataTestTable->setItem(row, 1, &subscriberColumn);

    ////! Packet Size -- Default: Max. Value
    //packetSizeColumn.setText("4064");
    //ui->dataTestTable->setItem(row, 2, &packetSizeColumn);

    //! Data Rate -- Default: 10 Mbps
    dataRateColumn.setText("10");
    ui->dataTestTable->setItem(row, 2, &dataRateColumn);

    //! Burst Mode -- Default: OFF
    burstModeButton.setChecked(false);

    //QString styleString = "text-align: center; margin-left:50%; margin-right:50%;";
    QString styleString = "text-align:center; margin-left:30%";
    burstModeButton.setStyleSheet(styleString);
    ui->dataTestTable->setCellWidget(row, 3, &burstModeButton);
    ui->dataTestTable->resizeColumnToContents(3);


    //! Test Duration -- Default: 5 Min
    testDurationColumn.setText("5");
    ui->dataTestTable->setItem(row, 4, &testDurationColumn);

    //! Number of Packets To Send
    numPacketsToSendColumn.setText("0");
    ui->dataTestTable->setItem(row, 5, &numPacketsToSendColumn);

    //! Delay In Microseconds
    //microsecondSleepColumn.setText("250");
    //ui->dataTestTable->setItem(row, 4, &microsecondSleepColumn);

    //! Number of Packets Received
    numPacketsRecvdColumn.setText("0");
    ui->dataTestTable->setItem(row, 6, &numPacketsRecvdColumn);

    //! Average RSSI
    rssiColumn.setText("0");
    ui->dataTestTable->setItem(row, 7, &rssiColumn);

    //! Data Rate in Mbps or Kbps
    txMbpsColumn.setText("0");
    ui->dataTestTable->setItem(row, 8, &txMbpsColumn);

    //! Data Throughput in Mbps or Kbps
    rxMbpsColumn.setText("0");
    ui->dataTestTable->setItem(row, 9, &rxMbpsColumn);

    //! Packet Error Rate In %
    PERColumn.setText("0%");
    ui->dataTestTable->setItem(row, 10, &PERColumn);

    //! Clear Packet Generator data transmition Button
    clearButton.setText("Clear");
    clearButton.setCheckable(true);
    clearButton.setChecked(false);

    ui->dataTestTable->setCellWidget(row, 11, &clearButton);
    connect(&clearButton, SIGNAL(toggled(bool)), this, SLOT(clear()));

    connect(this, SIGNAL(packetReceived()), this, SLOT(updateNumPacketsReceived()));

    ui->patternWidget->update();
    QApplication::processEvents();
}

//!##################################################
//! onGroupButtonClicked()
//! Input: Button Id
//! Combines Start and Stop buttons to be mutually
//! exclusive
//!##################################################
void PacketGenerator::onGroupButtonClicked(int id)
{
    switch(id)
    {
        case START_BUTTON_ID:
        {
            startDataTest();
            qDebug() << "completed start data test";
            break;
        }
        case STOP_BUTTON_ID:
        {
            stopDataTest(true, true);
            break;
        }
        case DEFAULT_BUTTON_ID:
        {
            defaultParameters();
            break;
        }
        default:
            break;
    }
}

//!##################################################
//! startDataTest()
//! Starts the test when the Start button is pushed
//!##################################################
void PacketGenerator::startDataTest()
{

    qDebug() << "start data test in pgen triggered";
    if ( isRunning )
    {
        displayMessage("Stop Current Data Traffic Generator First");
        return;
    }

    if (!playbookRunning)
    {
        packetSize         = 4064; // Set as default
        numPacketsToSend   = numPacketsToSendColumn.text().toInt();

        bool validThroughput = isValidThroughput(dataRateColumn.text());
        validInput           = validThroughput;
        if(validThroughput)
        {
            dataRate = dataRateColumn.text().toInt();
        }
        else
        {
            dataRate     = 10;
            displayMessage("Invalid Throughput, must be an integer between 1 and 100, setting to 10 Mbps.");
            dataRateColumn.setText("10");
        }

        if(validInput)
        {
            testDuration    = testDurationColumn.text().toInt();
            burstModeActive = burstModeButton.isChecked();

            int packetsPerSecond = getPacketsPerSecondFromDataRate(dataRate);

            qDebug() << "packets per second: " << packetsPerSecond;

            if(packetsPerSecond <= 0)
            {
                packetsPerSecond = 1;
            }

            uint32_t durationPacketsToSend = testDuration * 60 * packetsPerSecond;

            qDebug() << "Duration packets to send: " << durationPacketsToSend;

            // With burst mode, we have to account for the time between bursts when no data is being sent;
            // We will send less data over the duration, so we need to reduce the packets to send down to match the duration.
            if(burstModeActive)
            {
                totalBursts = (testDuration * 60) / totalBurstWindowTime;
                qDebug() << "total bursts:" << totalBursts;
                packetsPerBurst = packetsPerSecond * burstOnTime;
                packetsPerBurstWindow = packetsPerSecond * totalBurstWindowTime;

                float burstUpPercentage = (float)burstOnTime/(float)totalBurstWindowTime;
                qDebug() << "burst up percentage: " << burstUpPercentage;
                durationPacketsToSend = durationPacketsToSend * burstUpPercentage;
            }

            if( (durationPacketsToSend < numPacketsToSend) || (numPacketsToSend == 0) )
            {
                numPacketsToSend = durationPacketsToSend;
                numPacketsToSendColumn.setText(QString::number(numPacketsToSend));
            }

            float secondsSleep = ((float)1 / ((float) packetsPerSecond));

            qDebug() << "seconds sleep:" << secondsSleep;

            microsecondSleep = secondsSleep*1000000;

            qDebug() << "delay: " << microsecondSleep;

            bool ok            = false;

            dataTestPattern = ui->patternTextEdit->toPlainText().toInt(&ok, 16);
            if (!ok)
            {
                dataTestPattern = 0xF0;
            }
        }
    }

    if(validInput)
    {
        isRunning          = true;
        numPacketsReceived = 0;
        packetSize_s = 16;

        qDebug() << "Burst Mode status before sending data: " << burstModeActive;

        if(burstModeActive)
        {
            int packetsPerSecond = getPacketsPerSecondFromDataRate(dataRate);

            totalBursts = (testDuration * 60) / totalBurstWindowTime;

            qDebug() << "packets per second: " << packetsPerSecond;
            packetsPerBurst       = packetsPerSecond * burstOnTime;
            packetsPerBurstWindow = packetsPerSecond * totalBurstWindowTime;

            qDebug() << "packets per burst:" << packetsPerBurst;
            qDebug() << "packets per burst window:" << packetsPerBurstWindow;
         }

        providerIswIntrfcPtr->SetFlushSendQueue(cbp->entry->endpointId, cbp->dataflowId, false);

        subscriberIswIntrfcPtr->SetFlushReceiveQueue(cbp->entry->endpointId, cbp->dataflowId, false);

        usleep(500000);
        checkThroughputThread = QtConcurrent::run(getThroughput, this);
        sendThread            = QtConcurrent::run(sendData, this);
        sTimestamp            = QDateTime::currentMSecsSinceEpoch();

        qDebug() << "Pgen threads started, delay set to: " << microsecondSleep;

        loggerPtr->LogMessage("Pgen test started", cbp->dataflowId, std::chrono::system_clock::now());

        //cdtt->markStartDataTestTimeInLog(testId);
    }
    else
    {
        qDebug() << "resetting start button";

        buttonGroup->setExclusive(false);
        buttonGroup->checkedButton()->setChecked(false);
        buttonGroup->setExclusive(true);

        qDebug() << "checked: " << startTestPushButton->isChecked();
    }
}

//!##################################################
//! stopDataTest()
//! Stops the test when the Stop button is pushed
//!##################################################
void PacketGenerator::stopDataTest(bool flushProviderQueue, bool flushSubscriberQueue)
{
    if ( isRunning == false )
    {
        return;
    }

    qDebug() << "stop data test called";
    isRunning = false;
    usleep(100); // Let the other thread know we stopped

    checkThroughputThread.cancel();
    sendThread.cancel();

    sendThread.waitForFinished();
    checkThroughputThread.waitForFinished();

    qDebug() << "past threading calls";
    if ( flushProviderQueue )
    {
        //! Remove all packets on the outgoing queue for this flow
        providerIswIntrfcPtr->SetFlushSendQueue(cbp->entry->endpointId, cbp->dataflowId, true);
    }

    if ( flushSubscriberQueue )
    {
        //! Remove all packets on the receiver's incoming queue for this flow
        subscriberIswIntrfcPtr->SetFlushReceiveQueue(cbp->entry->endpointId, cbp->dataflowId, true);
    }

    qDebug() << "about to set checked";
    serialThroughput = 0 ;
    packetSize_s     = 0;

    if(!playbookRunning)
    {
        startTestPushButton->setChecked(false);
        stopTestPushButton->setChecked(false);
    }

    //clearLatencyValues();


//    if(numPacketsToSend != 0)
//    {

//        PERColumn.setText(QString::number((numPacketsToSend-numPacketsReceived)/numPacketsToSend));
//    }

    //cdtt->markEndDataTestTimeInLog(testId);

    //cdtt->calculateThroughput(testId);

    loggerPtr->LogMessage("Pgen test ended", cbp->dataflowId, std::chrono::system_clock::now());

    qDebug() << "end of stop pgen test";
}

//!###########################################################
//! updateNumPacketsReceived()
//! On signal it updates the user's view of packets received
//!###########################################################
void PacketGenerator::updateNumPacketsReceived()
{
    numPacketsRecvdColumn.setText(QString::number(numPacketsReceived));
    ui->dataTestTable->update();

    QCoreApplication::processEvents();
}

//!################################################################
//! receiveDataCallback()
//! Receives data as a callback from IswSolNet::DeliverSolNetData()
//! data coming in from the wireless network
//!################################################################
void PacketGenerator::receiveDataCallback(uint8_t *data, uint16_t transferSize, void *thisPtr)
{

    Q_UNUSED(data);
    if ( transferSize > 0 )
    {
        PacketGenerator *packetGen = (PacketGenerator *)thisPtr;

        //! Increment the number of SolNet data packets we get
        packetGen->numPacketsReceived++;
        emit packetGen->packetReceived();
    }
}

//!########################################################
//! clear()
//! Clears the number of packets sent and received columns
//!########################################################
void PacketGenerator::clear()
{
    if ( clearButton.isChecked() )
    {
        numPacketsReceived = 0;
        numPacketsRecvdColumn.setText(QString::number(numPacketsReceived));

        numPacketsToSend   = 0;
        numPacketsToSendColumn.setText(QString::number(numPacketsToSend));

        dataTestPattern    = 0xF0;
        ui->patternTextEdit->setText("F0");

        txMbpsColumn.setText(QString::number(0));
        rxMbpsColumn.setText(QString::number(0));
        PERColumn.setText(QString::number(0));

        clearButton.setChecked(false);
    }
}

//!######################################################
//! displayMessage()
//! Pops up a window with a message for the user
//!######################################################
void PacketGenerator::displayMessage(QString string)
{
    QMessageBox msgBox;
    msgBox.setText(string);
    msgBox.setIcon(QMessageBox::Information);

    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, &QTimer::timeout, [&]{ msgBox.close(); });
    timer.start(2000);

    msgBox.exec();
}

//!######################################################
//! getThroughput()
//! Runs in a concurrent thread get rx and tx throughput
//! from the firmware during the test
//!######################################################
void PacketGenerator::getThroughput(PacketGenerator *thisPtr)
{
    IswMetrics *iswMetrics                                            = thisPtr->providerIswIntrfcPtr->GetIswMetrics();
    IswStream *iswSrcStream                                           = thisPtr->providerIswIntrfcPtr->GetIswStream();
    std::array<IswStream::iswPeerRecord, MAX_PEERS> &providersPeers   = iswSrcStream->GetPeerRef();
    IswStream *iswDestStream                                          = thisPtr->subscriberIswIntrfcPtr->GetIswStream();
    std::array<IswStream::iswPeerRecord, MAX_PEERS> &subscribersPeers = iswDestStream->GetPeerRef();
    qint64 qiTimestamp                                                = 0;
    QString linkQualityStr                                            = "";

    IswLink *iswLink                                                  = thisPtr->providerIswIntrfcPtr->GetIswLink();
    uint8_t rssi                                                      = 0;
    float PhyRate                                                     = 0.0;
    uint8_t activeAntennaID                                           = 0;

    while ( thisPtr->isRunning == true )
    {
        rssi         = providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.averageRSSI;
        thisPtr->rssiColumn.setText(QString::number(rssi));

        PhyRate      = iswMetrics->GetPhyRate();
        activeAntennaID = iswLink->SendGetAntennaIdCmd();

        qiTimestamp           = QDateTime::currentMSecsSinceEpoch();
        thisPtr->diff         = qiTimestamp-thisPtr->sTimestamp;
        thisPtr->packetSize_s = 16;
        uint8_t txMbps        = 0;
        uint8_t rxMbps        = 0;

//        qDebug() << "Packet Loss (%) = " << ((thisPtr->packetSentCtr-thisPtr->numPacketsRecvdColumn.text().toDouble())/thisPtr->packetSentCtr)*100;

        switch(providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.linkQuality)
        {
            case 0:
            {
                linkQualityStr = "Poor";
                break;
            }
            case 1:
            {
                linkQualityStr = "Marginal";
                break;
            }
            case 2:
            {
                linkQualityStr = "Average";
                break;
            }
            case 3:
            {
                linkQualityStr = "Best";
                break;
            }
            case 255:
            default:
            {
                linkQualityStr = "Unknown";
                break;
            }
        }

        if ( thisPtr->subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() != UsbInterface::externalBoard )
        {
            if ( thisPtr->subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard ||
                 thisPtr->providerIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard )
            {
                uint8_t bits              = thisPtr->packetSize_s * 8;
                float seconds             = (float)thisPtr->microsecondSleep/1000000;
                thisPtr->serialDataRate   = (double)(bits/seconds)/1000;
                thisPtr->serialThroughput = ((8000*thisPtr->packetSize_s*thisPtr->numPacketsReceived))/(thisPtr->diff+1); // estimated
                thisPtr->txMbpsColumn.setText(QString::number(thisPtr->serialDataRate));
                thisPtr->rxMbpsColumn.setText(QString::number(thisPtr->serialThroughput));

                thisPtr->writeToCsv(qiTimestamp, rssi, PhyRate, thisPtr->serialDataRate, thisPtr->serialThroughput, linkQualityStr, activeAntennaID);
            }
            else
            {
                txMbps = providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.txThroughput;
                rxMbps = subscribersPeers[thisPtr->cbp->providerIswPeerIndex].iswPeerRecord4.rxThroughput;
                thisPtr->txMbpsColumn.setText(QString::number(txMbps));
                thisPtr->rxMbpsColumn.setText(QString::number(rxMbps));
            }
        }
        else
        {
            txMbps = providersPeers[thisPtr->cbp->subscriberIswPeerIndex].iswPeerRecord4.txThroughput;
            //rxMbps = providersPeers[thisPtr->cbp->providerIswPeerIndex].iswPeerRecord4.rxThroughput;

            thisPtr->txMbpsColumn.setText(QString::number(txMbps));
            thisPtr->rxMbpsColumn.setText("EXT");
        }

        thisPtr->writeToCsv(qiTimestamp, rssi, PhyRate, txMbps, rxMbps, linkQualityStr, activeAntennaID);

        sleep(1);
    }
}

//!########################################################
//! sendData()
//! Runs on a concurrent thread and sends a fixed pattern
//! of data to the other node in the test every ten ms.
//!########################################################
void PacketGenerator::sendData(PacketGenerator *thisPtr)
{

    qDebug() << "sendData call";

    uint32_t packetsSent     = 0;

    //! Fill a databuffer with alternating 10 0's and 10 1's

    if (thisPtr->packetSize > 4040)
    {
        thisPtr->packetSize = 4040;
    }

    uint8_t data[thisPtr->packetSize];
    memset(data, thisPtr->dataTestPattern, thisPtr->packetSize);

    uint8_t data_s[thisPtr->packetSize_s];
    memset(data_s, thisPtr->dataTestPattern, thisPtr->packetSize_s);

    // Temp for testing
    int loopCnt = 0;

    qDebug() << "send data loop starting...";

    while ( (thisPtr->providerIswSolNetPtr != nullptr) &&
            (thisPtr->providerIswSolNetPtr->IsSolNetStopped() == false) &&
            (thisPtr->isRunning) &&
            ((packetsSent < thisPtr->numPacketsToSend) ||
             (thisPtr->numPacketsToSend == 0))
            )
    {

        bool sendPacket = true;
        // If using burst mode, check if this packet is in the burst or not;
        if(thisPtr->burstModeActive)
        {
            sendPacket = thisPtr->updateBurstData();
        }

        int subscriberIndex = thisPtr->cbp->subscriberIndexInDynArray;

        std::array<IswSolNet::iswSolNetPeerServicesEntry, MAX_PEERS> *subscriberPeerServices
                = thisPtr->iswToolPtr->DynArray[subscriberIndex].iswSolNet->GetPeerServices();

        int providerDataflowId = thisPtr->cbp->dataflowId;
        int providerPeerIndex = thisPtr->cbp->providerIswPeerIndex;
        int providerIndex = thisPtr->cbp->providerIndexInDynArray;

        IswSolNet::iswSolNetPeerServicesEntry *subscriberPeerServicesEntry
                = &(*subscriberPeerServices)[providerPeerIndex];

        QString cleanRegistrationStr = "Available/Registered";
        QString deregisteredStr = "Available/Not Registered";

        //qDebug() << "dataflowId in pgen: " << providerDataflowId;

        QString registrationStatus = thisPtr->iswToolPtr->getServiceRegistrationCellStatus(subscriberIndex, providerIndex);

        if(thisPtr->subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() != UsbInterface::externalBoard)
        {
            if(!subscriberPeerServicesEntry->services[providerDataflowId].ImRegistered)
            {
                qDebug() << "service showing as not registered";
                if(registrationStatus == deregisteredStr)
                {
                    qDebug() << "Cell showing not registered";

                    thisPtr->iswToolPtr->setPlaybookActive(true); // set to avoid keepAlivePeriod input dialog on re-registrion
                    qDebug() << "Pgen no longer registered or service active, attempting to re-register";
                    QString deviceAndService =thisPtr->cbp->provider.split("-")[1] + "-Packet Generator";

                    qDebug() << "deviceAndService pgen send data: " << deviceAndService;

                    int status = thisPtr->iswToolPtr->registerForService(subscriberIndex, providerIndex, providerDataflowId, deviceAndService);
                    sleep(5);
                    qDebug() << "Registration attempt result: " << status;

                    if(status == 0)
                    {
                        thisPtr->iswToolPtr->setServiceRegistrationCellStatus(subscriberIndex, providerIndex);

                    }

                    thisPtr->iswToolPtr->setPlaybookActive(false); // set back to false after re-registration attempt
                }
            }
        }

        // Send this service data to peer
        uint8_t policies = 0x02; // Send Acknowledgement
        if (thisPtr->providerIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard)
        {
            if (thisPtr->packetSize > 16) {
                thisPtr->packetSize_s = 16; //the max packetSize for the camouflage
            }
            else {
                thisPtr->packetSize_s = thisPtr->packetSize;
            }

            if(sendPacket)
            {
                thisPtr->providerIswSolNetPtr->SendSolNetDataPacket(thisPtr->cbp->subscriberIswPeerIndex,
                                                                        thisPtr->cbp->entry->endpointId,
                                                                        thisPtr->cbp->dataflowId,
                                                                        policies,
                                                                        data_s,
                                                                        thisPtr->packetSize_s);
                packetsSent++;
            }


            //thisPtr->packetSizeColumn.setText("16");
            usleep(160000);
            if(!thisPtr->playbookRunning)
            {
                //column gone, replaced by dataRate, could set that instead.
                //thisPtr->microsecondSleepColumn.setText("160");
            }

        }
        else
        {
            //combat board sending, 2 types of receivers
            if ( thisPtr->subscriberIswIntrfcPtr->GetUsbInterface()->GetBoardType() == UsbInterface::serialBoard )
            {
                if (thisPtr->packetSize > 16) {
                    thisPtr->packetSize_s = 16;
                }
                else {
                    thisPtr->packetSize_s = thisPtr->packetSize;
                }

                if(sendPacket)
                {
                    thisPtr->providerIswSolNetPtr->SendSolNetDataPacket(thisPtr->cbp->subscriberIswPeerIndex,
                                                                            thisPtr->cbp->entry->endpointId,
                                                                            thisPtr->cbp->dataflowId,
                                                                            policies,
                                                                            data_s,
                                                                            thisPtr->packetSize_s);
                    packetsSent++;
                }

                //thisPtr->packetSizeColumn.setText("16");
                usleep(160000);
                if(!thisPtr->playbookRunning)
                {
                    //column gone, replaced by dataRate, could set that instead.
                    //thisPtr->microsecondSleepColumn.setText("160");
                }

            }
            else
            {

                if(sendPacket)
                {
                    thisPtr->providerIswSolNetPtr->SendSolNetDataPacket(thisPtr->cbp->subscriberIswPeerIndex,
                                                                            thisPtr->cbp->entry->endpointId,
                                                                            thisPtr->cbp->dataflowId,
                                                                            policies,
                                                                            data,
                                                                            thisPtr->packetSize);
                    packetsSent++;
                }

                usleep(thisPtr->microsecondSleep);
            }
        }

        loopCnt++;
    }

    // Stop data test or Close packet gen after done sending messages
    thisPtr->stopDataTest(true, true);
    //thisPtr->close();
}

void PacketGenerator::writeToCsv(qint64 timestamp, uint8_t rssi, float PhyRate, double txMbps, double rxMbps, QString linkQuality, uint8_t activeAntennaID) //
{
    QString filename = cbp->provider + "_" + cbp->subscriber + "_data.csv";

    QFile file(filename);   

    if (file.open(QIODevice::Append | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << timestamp <<"\t"<< rssi <<"\t" << (float)PhyRate << "\t" << (double)txMbps <<"\t"<< rxMbps <<"\t"<< linkQuality <<"\t" << activeAntennaID << "\n";
    }

    file.flush();
    file.close();
}

void PacketGenerator::defaultParameters()
{
    uint8_t row = 0;

    //! Data Rate -- Default: 10 Mbps
    dataRateColumn.setText("10");
   // ui->dataTestTable->setItem(row, 2, &dataRateColumn);

    //! Burst Mode -- Default: Unchecked
    burstModeButton.setChecked(false);
    //ui->dataTestTable->setItem(row, 3, &burstModeColumn);

    //! Test Duration -- Default: 5 Min
    testDurationColumn.setText("5");
    //ui->dataTestTable->setItem(row, 4, &testDurationColumn);

    //! Number of Packets To Send -- Default: 0 (No Max)
    numPacketsToSendColumn.setText("0");
    //ui->dataTestTable->setItem(row, 5, &numPacketsToSendColumn);
}

bool PacketGenerator::updateBurstData()
{
    bool sendPacket = false;
    packetInBurstWindowTracker++;

    if(completedBurstCount >= totalBursts)
    {
        qDebug() << "completed all bursts: " << completedBurstCount;
        sendPacket = false;
    }
    else
    {
        if(packetInBurstWindowTracker < packetsPerBurst)
        {
            sendPacket = true;
        }
        else if(packetInBurstWindowTracker == packetsPerBurst)
        {
            sendPacket = true;
            completedBurstCount++;
            qDebug() << "completed burst: " << completedBurstCount;
        }

        // After full burst window is completed, reset the paket tracker for the next burst.
        if(packetInBurstWindowTracker == packetsPerBurstWindow)
        {
            qDebug() << "completed full burst window: " << completedBurstCount;
            packetInBurstWindowTracker = 0;
            sendPacket = false;
        }
    }

    return sendPacket;
}

bool PacketGenerator::isValidThroughput(QString textInput)
{
    bool validThroughput = true;
    int throughput = textInput.toInt();

    if(throughput <= 0 || throughput > 100 )
    {
        validThroughput = false;
    }

    return validThroughput;
}

int PacketGenerator::getPacketsPerSecondFromDataRate(int dataRate)
{
    int packetsPerSecond = ((dataRate * 1000000) / (4064 * 8));

    return packetsPerSecond;
}
