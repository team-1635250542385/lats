//!#################################################
//! Filename: Logger.h
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues and prints to the log file
//!              in a background task.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <queue>
#include <mutex>

//                          void(name) forces compiler to check if variable exists
#define GET_VAR_NAME(name) (void(name),varName(#name))

class Logger
{

public:
    Logger(std::string filename, uint16_t debugLevel);
    Logger(std::string filename);
    ~Logger();

    void setImmediateShutdown(bool shutdowNow) { immediateShutdown = shutdowNow; }

    std::string varName(const char* strConst);
    uint64_t GetTimeStamp(void);
    void setDebugLevel(uint16_t);
    void LogMessage(std::string message, uint16_t index, std::chrono::system_clock::time_point timestamp);   //! Adds timestamp to string
//    void LogMessage(std::string message, std::string devName, std::chrono::system_clock::time_point timestamp);   //! Adds timestamp to string
    void LogSimpleMessage(std::string message); //! Writes Message to Log File
    void DebugMessage(std::string message, uint16_t index); //! No timestamp
    void deQueue(void);
    int getLogQueueSize();
    void clearQueue();
    std::queue<std::string> getQueue();
    std::string getFilename();

    //! Public Use these to set debugLevel
    //! - - - -|- - - -|- - - -|- - - -
    //!                               |- IswInterface class - 0x0001
    //!                             | - IswSecurity class - 0x0002
    //!                           | - IswFirmware class - 0x0004
    //!                         | - IswIdentity class - 0x0008
    //!                       | - IswSystem class - 0x0010
    //!                     | - IswAssociation class - 0x0020
    //!                   | - IswStream class - 0x0040
    //!                 | - IswSolNet class - 0x0080
    //!               | - IswMetrics class - 0x0100
    //!             | - IswLink class - 0x0200
    //!           | - IswProduct class - 0x0400
    //!         | - UsbInterface class - 0x0800
    //!       | - OnBoard class - 0x1000
    //!     | - IswInterfaceInit class - 0x2000
    //!   | - UsbInterfaceInit class - 0x4000
    //! | - SerialInterface class - 0x8000
    static const uint16_t DEBUG_ISW_INTERFACE       = 0x0001;
    static const uint16_t DEBUG_ISW_SECURITY        = 0x0002;
    static const uint16_t DEBUG_ISW_FIRMWARE        = 0x0004;
    static const uint16_t DEBUG_ISW_IDENTITY        = 0x0008;
    static const uint16_t DEBUG_ISW_SYSTEM          = 0x0010;
    static const uint16_t DEBUG_ISW_ASSOCIATION     = 0x0020;
    static const uint16_t DEBUG_ISW_STREAM          = 0x0040;
    static const uint16_t DEBUG_ISW_SOLNET          = 0x0080;
    static const uint16_t DEBUG_ISW_METRICS         = 0x0100;
    static const uint16_t DEBUG_ISW_LINK            = 0x0200;
    static const uint16_t DEBUG_ISW_PRODUCT         = 0x0400;
    static const uint16_t DEBUG_USB_INTERFACE       = 0x0800;
    static const uint16_t DEBUG_ISW_ONBOARD         = 0x1000;
    static const uint16_t DEBUG_ISW_INTERFACEINIT   = 0x2000;
    static const uint16_t DEBUG_USB_INTERFACEINIT   = 0x4000;
    static const uint16_t DEBUG_SERIAL_INTERFACE    = 0x7FFF; //ToDo Ben: Used to be 0x8000 before adding test
    static const uint16_t DEBUG_ISW_TEST            = 0x8000; //ToDo Ben: Change to uint32 and continue pattern

    //! Public Use these to check if debugging is on in source code
    uint8_t DEBUG_IswInterface     = 0;
    uint8_t DEBUG_IswSecurity      = 0;
    uint8_t DEBUG_IswFirmware      = 0;
    uint8_t DEBUG_IswIdentity      = 0;
    uint8_t DEBUG_IswSystem        = 0;
    uint8_t DEBUG_IswAssociation   = 0;
    uint8_t DEBUG_IswStream        = 0;
    uint8_t DEBUG_IswSolNet        = 0;
    uint8_t DEBUG_IswMetrics       = 0;
    uint8_t DEBUG_IswLink          = 0;
    uint8_t DEBUG_IswProduct       = 0;
    uint8_t DEBUG_UsbInterface     = 0;
    uint8_t DEBUG_OnBoard          = 0;
    uint8_t DEBUG_IswInterfaceInit = 0;
    uint8_t DEBUG_UsbInterfaceInit = 0;
    uint8_t DEBUG_SerialInterface  = 0;
    uint8_t DEBUG_IswTest          = 0;

private:
    void CreateLogFile(void);
    void EmptyQueue(void);

    std::mutex logMutex;
    std::queue<std::string> logQueue;
    std::string logFileName;
    FILE *logFile;
    bool immediateShutdown = false;
};

#endif //! LOGGER_H
