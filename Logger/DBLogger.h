//!#################################################
//! Filename: DBLogger.h
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues and prints to the log file
//!              in a background task.  The resulting
//!              CSV will be ingested by a database.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef DBLOGGER_H
#define DBLOGGER_H

#include <iostream>
#include <queue>
#include <mutex>
#include <QString>
#include <sstream>
#include "DBLogRecord.h"

class DBLogger
{

public:
    DBLogger(std::string filename, QString version);
    ~DBLogger();
    void dbLog(DBLogRecord &dbr);
    void deQueue();
    long long updateEpochTime() {return (et = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count());}
    long long getEpochTime() {return et;}
    int updateUIIndex() {return ++uiIndex;}
    int getUIIndex() {return uiIndex;}
    int resetUIIndex() {return (uiIndex = 0);}
    QString getDBLVersion() {return DBLVersion;}

private:
    void CreateLogFile();
    void InitLog(QString kitVersion);
    void FinalizeLog();
    void EmptyQueue();
    std::string GetUUID();
    std::mutex logMutex;
    std::queue<std::string> logQueue;
    std::string logFileName;
    QString DBLVersion;
    FILE *logFile;
    long long et;
    int uiIndex = 0;
};

#endif //! DBLOGGER_H
