//!#################################################
//! Filename: DBLogger.cpp
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while DBLogger
//!              dequeues and prints to the log file.
//!              Should be run in background task.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "DBLogger.h"
#include <time.h>
#include <sys/time.h>
#include <sstream>
#include <unistd.h>
#include <memory>
#include <algorithm>
#include <iomanip>
#include <iostream>

#define LOGNAME_FORMAT "_%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 20

DBLogger::DBLogger(std::string filename, QString kitVersion) :
    logFileName(filename)
{
    DBLVersion = "1.4";
    CreateLogFile();
    InitLog(kitVersion);
}

DBLogger::~DBLogger()
{
    EmptyQueue();
    FinalizeLog();
    fflush(logFile);
    fclose(logFile);
    logQueue.empty();
}


void DBLogger::dbLog(DBLogRecord &dbr)
{
    logMutex.lock();
    logQueue.push(dbr.getString());
    logMutex.unlock();
}


void DBLogger::deQueue()
{
    logMutex.lock();
    std::string tmpString;

    if (logQueue.empty() == false)
    {
        tmpString = logQueue.front();
        fwrite(tmpString.data(), sizeof(char), tmpString.size(), logFile);
        fflush(logFile);
        logQueue.pop();
    }

    logMutex.unlock();
}

void DBLogger::CreateLogFile()
{
    static char name[LOGNAME_SIZE];
    time_t now = time(nullptr);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));
    logFileName.append(name);
    logFileName.append(".csv");

    logFile = fopen(logFileName.c_str(), "w+");
    if (logFile == nullptr)
    {
        std::cout << "Can't open DB log file with name: " << logFileName << std::endl;
    }
}

void DBLogger::InitLog(QString kitVersion) // #WIP
{
    DBLogRecord dblr(tbStart, updateEpochTime(), resetUIIndex());
    dblr.addStartObservation("ISW Test Tool"); // product_name
    dblr.addStartObservation(kitVersion.toStdString()); // product_version
    dblr.addStartObservation(getDBLVersion().toStdString()); // logger_version
    dblr.addStartObservation(GetUUID()); // logger_uuid
    dbLog(dblr);
}

void DBLogger::FinalizeLog()
{
    //! Finalize the DB Log file to let us know that the program was terminated cleanly.
    DBLogRecord dblr(tbStop, updateEpochTime(), getUIIndex());
    dbLog(dblr);
    deQueue();
    usleep(10);
    fflush(logFile);
}

void DBLogger::EmptyQueue()
{
    //! Called from destructor on shutdown
    while (logQueue.empty() == false)
    {
        deQueue();
        usleep(10);
    }
    fflush(logFile);
}

std::string DBLogger::GetUUID()
{
    const char* cmd = "dmidecode --string system-uuid";
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    result.erase(std::remove(result.begin(), result.end(), '\n'), result.end()); // remove endline character
    return result;
}
