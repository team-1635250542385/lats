//!#################################################
//! Filename: Logger.cpp
//! Description: Class that provides threadsafe
//!              logging.  Clients push log
//!              messages to the queue while Logger
//!              dequeues an prints to the log file.
//!              Should be run in background task.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "Logger.h"
#include <time.h>
#include <sys/time.h>
#include <sstream>
#include <unistd.h>
#include <chrono>

#include <QDebug>

#define LOGNAME_FORMAT "_%Y%m%d_%H%M%S"
#define LOGNAME_SIZE 20

//!########################################################
//! Constructor: Logger()
//! Inputs: filename - full path name of the file that
//!                    log messages will be written to
//!         debugLevel - User set debug level based on their
//!                      selections when iswTestTool or
//!                      other test application is started
//!########################################################
Logger::Logger(std::string filename, uint16_t debugLevel) :
    logFileName(filename)
{
    CreateLogFile();
    setDebugLevel(debugLevel);
}

Logger::Logger(std::string filename) :
    logFileName(filename)
{
    CreateLogFile();
}

Logger::~Logger()
{
    EmptyQueue();
    fflush(logFile);
    fclose(logFile);
    logQueue.empty();
}

//!########################################################
//! varName()
//! Inputs: strConst - string constant
//! Description: If we have to refactor a variable, you
//! 		 don't have to go back and manually update 
//!              the textual representations of the refactored 
//!              variable name. Used in debug output
//!########################################################
std::string Logger::varName(const char* strConst)
{
    std::string name = strConst;
    return (name);
}

//!########################################################
//! setDebugLevel()
//! Inputs: debugLevel - User set debug level
//!########################################################
void Logger::setDebugLevel(uint16_t debugLevel)
{
    std::stringstream ss;
    ss << "Debug Level(s):" << std::endl;
    DEBUG_IswInterface     = 0;
    DEBUG_IswSecurity      = 0;
    DEBUG_IswFirmware      = 0;
    DEBUG_IswIdentity      = 0;
    DEBUG_IswSystem        = 0;
    DEBUG_IswAssociation   = 0;
    DEBUG_IswStream        = 0;
    DEBUG_IswSolNet        = 0;
    DEBUG_IswMetrics       = 0;
    DEBUG_IswLink          = 0;
    DEBUG_IswProduct       = 0;
    DEBUG_UsbInterface     = 0;
    DEBUG_OnBoard          = 0;
    DEBUG_IswInterfaceInit = 0;
    DEBUG_UsbInterfaceInit = 0;
    DEBUG_SerialInterface  = 0;
    DEBUG_IswTest          = 0;

    if ( debugLevel & DEBUG_ISW_INTERFACE )
    {
        DEBUG_IswInterface = 1;
        ss << " " << GET_VAR_NAME(DEBUG_IswInterface) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_SECURITY )
    {
        DEBUG_IswSecurity = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswSecurity) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_FIRMWARE )
    {
        DEBUG_IswFirmware = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswFirmware) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_IDENTITY )
    {
        DEBUG_IswIdentity = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswIdentity) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_SYSTEM )
    {
        DEBUG_IswSystem = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswSystem) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_ASSOCIATION )
    {
        DEBUG_IswAssociation = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswAssociation) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_STREAM )
    {
        DEBUG_IswStream = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswStream) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_SOLNET )
    {
        DEBUG_IswSolNet =  1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswSolNet) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_METRICS )
    {
        DEBUG_IswMetrics =  1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswMetrics) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_LINK )
    {
        DEBUG_IswLink =  1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswLink) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_PRODUCT )
    {
        DEBUG_IswProduct =  1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswProduct) << std::endl;
    }
    if ( debugLevel & DEBUG_USB_INTERFACE )
    {
        DEBUG_UsbInterface = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_UsbInterface) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_ONBOARD )
    {
        DEBUG_OnBoard = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_OnBoard) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_INTERFACEINIT )
    {
        DEBUG_IswInterfaceInit = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswInterfaceInit) << std::endl;
    }
    if ( debugLevel & DEBUG_USB_INTERFACEINIT )
    {
        DEBUG_UsbInterfaceInit = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_UsbInterfaceInit) << std::endl;
    }
    if ( debugLevel & DEBUG_SERIAL_INTERFACE )
    {
        DEBUG_SerialInterface = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_SerialInterface) << std::endl;
    }
    if ( debugLevel & DEBUG_ISW_TEST )
    {
        DEBUG_IswTest = 1;
        ss << " " <<  GET_VAR_NAME(DEBUG_IswTest) << std::endl;
    }

    if ( debugLevel == 0 )
    {
        ss << " NONE" << std::endl;
    }
    DebugMessage(ss.str(), 0);
}

//!##########################################################
//! LogMessage()
//! Inputs: message - string containing the message
//!         index - device index that the message applies to
//! Description: A timestamp is added to the message  which
//!              is then pushed onto the Logger queue.
//!              A background thread removes messages from
//!              this queue and writes them to the log file.
//!##########################################################
void Logger::LogMessage(std::string message, uint16_t index, std::chrono::system_clock::time_point timestamp)
{
    logMutex.lock();

    uint64_t now = std::chrono::duration_cast<std::chrono::microseconds>(timestamp.time_since_epoch()).count();

    std::stringstream ss;

    if(index == 65535)
    {
        ss << message.c_str() << " at " << std::to_string(now) << std::endl;
        std::string tmpString;
        tmpString.append(ss.str());
        logQueue.push(tmpString);
    }
    else
    {
        ss  << "Device " << std::to_string(index) << " " << message.c_str() << " at " << std::to_string(now) << std::endl;
        std::string tmpString;
        tmpString.append(ss.str());
        logQueue.push(tmpString);
    }

    logMutex.unlock();
}

//!##########################################################
//! LogMessage()
//! Inputs: message - string containing the message
//!         devName - device name
//! Description: A timestamp is added to the message  which
//!              is then pushed onto the Logger queue.
//!              A background thread removes messages from
//!              this queue and writes them to the log file.
//!##########################################################
//void Logger::LogMessage(std::string message, std::string devName, std::chrono::system_clock::time_point timestamp)
//{
//    logMutex.lock();

//    uint64_t now = std::chrono::duration_cast<std::chrono::microseconds>(timestamp.time_since_epoch()).count();
//    std::stringstream ss;

//    ss  << devName << " " << message.c_str() << " at " << std::to_string(now) << std::endl;
//    std::string tmpString;
//    tmpString.append(ss.str());
//    logQueue.push(tmpString);

//    logMutex.unlock();
//}

//!##########################################################
//! LogMessage()
//! Inputs: message - string containing the message
//!         devName - device name
//! Description: A timestamp is added to the message  which
//!              is then pushed onto the Logger queue.
//!              A background thread removes messages from
//!              this queue and writes them to the log file.
//!##########################################################
void Logger::LogSimpleMessage(std::string message)
{
    logMutex.lock();

    std::stringstream ss;

    ss  << message.c_str() << std::endl;
    std::string tmpString;
    tmpString.append(ss.str());
    logQueue.push(tmpString);

    logMutex.unlock();
}

//!##########################################################
//! DebugMessage()
//! Inputs: message - string containing the message
//!         index - device index that the message applies to
//! Description: The message is pushed onto the Logger queue
//!              Unlike LogMessage() this doesn't write the
//!              timestamp.  It is useful for printing out
//!              detailed information after the message header
//!              with timestamp.
//!##########################################################
void Logger::DebugMessage(std::string message, uint16_t index)
{
    //! No timestamp - useful for printing
    //! headers and databufs
    logMutex.lock();
    message.append("\n");
    logQueue.push(message);
    logMutex.unlock();
}

//!##########################################################
//! deQueue()
//! Inputs: None
//! Description: Removes a message from the queue and writes
//!              it to the logFile
//!##########################################################
void Logger::deQueue()
{
    logMutex.lock();
    std::string tmpString;

    if (logQueue.empty() == false)
    {
        tmpString = logQueue.front();
        fwrite(tmpString.data(), sizeof(char), tmpString.size(), logFile);
        fflush(logFile);
        logQueue.pop();
    }

    logMutex.unlock();
}

int Logger::getLogQueueSize()
{
    return logQueue.size();
}

void Logger::clearQueue()
{
    logQueue.empty();
}

std::queue<std::string> Logger::getQueue()
{
    return logQueue;
}

//!##########################################################
//! GetTimeStamp()
//! Inputs: None
//! Description: Gets the Linux timestamp
//!##########################################################
uint64_t Logger::GetTimeStamp()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return (tv.tv_sec*(uint64_t)1000000 + tv.tv_usec);
}

//!##########################################################
//! CreateLogFile()
//! Inputs: None
//! Description: Creates the logfile from logFileName passed
//!              into the constructor
//!##########################################################
void Logger::CreateLogFile()
{
    static char name[LOGNAME_SIZE];
    time_t now = time(0);
    strftime(name, sizeof(name), LOGNAME_FORMAT, localtime(&now));
    logFileName.append(name);
    logFileName.append(".txt");

    logFile = fopen(logFileName.c_str(), "w+");
    if (logFile == NULL)
    {
        std::cout << "Can't open log file with name: " << logFileName << std::endl;
    }
}

//!##########################################################
//! EmptyQueue()
//! Inputs: None
//! Description: Empties all messages in the queue and flushes
//!              the logFile
//!##########################################################
void Logger::EmptyQueue()
{
    if ( immediateShutdown == false )
    {
        //! Called from destructor on shutdown
        while (logQueue.empty() == false)
        {
            deQueue();
            usleep(10);
        }
    }
    fflush(logFile);
}

std::string Logger::getFilename()
{
    return logFileName;
}
