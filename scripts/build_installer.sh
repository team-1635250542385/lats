#!/bin/bash -e

###################################
# Script to build the ISW
# iV&V Kit Installer File
###################################

# Home directory -- Modify to the user's environment
HOME_DIR=/home/ctuser
BUILD_DIR=$HOME_DIR/Documents/qt_workspace/ISW
QT_BIN_DIR=/home/ctuser/Qt/5.12.10/gcc_64/bin
QT_INSTALLER_DIR=/home/ctuser/
QT_DEPLOYABLE_DIR=/home/ctuser/Downloads

# Check input arguments
if [ "$#" -ne 3 ]; then
    echo "Usage: ./build_installer.sh Branch Name Major Minor"
fi
#echo $#

# Build the GUI In Release Mode
cd $BUILD_DIR/$1/lats
mkdir -p build-$1-GUI
cd ./build-$1-GUI
$QT_BIN_DIR/qmake ../iswTestTool/iswTestTool.pro -spec linux-g++ CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Give time for copy to complete before creating deployable version
sleep 1
echo "Build - Release Completed"

sleep 1

# Create Deployable Version
cd ../scripts
./linuxdeployqt-7-x86_64.AppImage $BUILD_DIR/$1/lats/build-$1-GUI/iswTestTool -always-overwrite -bundle-non-qt-libs -qmake=$QT_BIN_DIR/qmake

sleep 1

# Compress Build Directory -- 7z
cd $BUILD_DIR/$1/lats
tar -zcvf build-$1-GUI.7z build-$1-GUI/

sleep 1

# Copy 7z File to Branch Release Directory
cp build-$1-GUI.7z /home/ctuser/Documents/qt_workspace/ISW/Releases/$1_Release/releases/Installer_Config/packages/com.c5isr.isw/data

sleep 1

# Create Installer File
/home/ctuser/Qt/Tools/QtInstallerFramework/4.0/bin/binarycreator -c $BUILD_DIR/Releases/$1_Release/releases/Installer_Config/config/config.xml -p $BUILD_DIR/Releases/$1_Release/releases/Installer_Config/packages/ $BUILD_DIR/Releases/$1_Release/releases/isw_v_$2_$3





