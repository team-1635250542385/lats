#!/bin/bash

###################################
#  Script to build all the LATS
#  ISW Library software.
###################################

# Home directory on DI2E site is /home/jenkins
HOME_DIR=/home/jenkins/
BUILD_DIR=$HOME_DIR/workspace/ISW/ISW_Build_LATS

# Qt directories
QTDIR=/opt/Qt5/5.13.1/gcc_64
QTBIN=/opt/Qt5/5.13.1/gcc_64/bin
QTINC=/opt/Qt5/5.13.1/gcc_64/include
QTLIB=/opt/Qt5/5.13.1/gcc_64/lib
QTSPECS=/opt/Qt5/5.13.1/gcc_64/mkspecs

# Check input arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: jenkins_build_lats.sh branch_name"
fi
echo $#

# Build UsbInterface Unit Tests
cd $BUILD_DIR/UnitTests/
mkdir -p build-Usb_UnitTests-$1
cd $BUILD_DIR/UnitTests/build-Usb_UnitTests-$1
$QTBIN/qmake ../UsbInterface/Usb_UnitTests.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
#./Usb_UnitTests

# Build IswInterface Unit Tests
cd $BUILD_DIR/UnitTests
mkdir -p build-Isw_UnitTests-$1
cd $BUILD_DIR/UnitTests/build-Isw_UnitTests-$1
$QTBIN/qmake ../IswInterface/Isw_UnitTest.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
#./Isw_UnitTest

# Build the Integration Tests:
# Logger
# UsbInterface
# IswInterface
# Hotplug
# SolNet

cd $BUILD_DIR/IntegrationTests
mkdir -p build-LoggerTest
mkdir -p build-UsbTest
mkdir -p build-IswTest
mkdir -p build-HotplugTest
cd $BUILD_DIR/IntegrationTests/SolNet
mkdir -p build-SolNetMessagePkts
mkdir -p build-SolNetDataPkts

cd $BUILD_DIR/IntegrationTests/build-LoggerTest
$QTBIN/qmake ../Logger/TestLogger.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd ../build-UsbTest
$QTBIN/qmake ../UsbInterface/TestUsbInterface.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd ../build-IswTest
$QTBIN/qmake ../IswInterface/TestIswInterface.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd ../build-HotplugTest
$QTBIN/qmake ../Hotplug/TestHotplug.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd $BUILD_DIR/IntegrationTests/SolNet/build-SolNetMessagePkts
$QTBIN/qmake ../TestMessagePkts/TestSolNetMessagePkts.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd ../build-SolNetDataPkts
$QTBIN/qmake ../TestDataPkts/TestSolNetDataPkts.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Make library
cd $BUILD_DIR/
mkdir -p build-$1-IswInteraceLib
cd build-$1-IswInteraceLib
$QTBIN/qmake ../IswInterface/IswInterface.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
cp ./libIswInterface.so.0.*.0 ../IswInterface/

# Give time for copy to complete before deleting directories
sleep 1

# Build the GUI:
cd $BUILD_DIR/
mkdir -p build-$1-GUI
cd ./build-$1-GUI
$QTBIN/qmake ../iswTestTool/iswTestTool.pro -spec $QTSPECS/linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Cleanup
cd $BUILD_DIR/
rm -rf */build-*
rm -rf */*/build-*
rm -rf ./build-*

