#!/bin/bash -e

###################################
#  Script to build all the LATS
#  ISW Library software and run
#  the unit tests.  This will work
#  locally with one ISW board on.
#  (Unit tests need on ISW board)
#  It's a good way to verify changes
#  when changing ISW library code
###################################

# Home directory on DI2E site is /home/jenkins
HOME_DIR=/home/ctuser
BUILD_DIR=$HOME_DIR/Documents/qt_workspace/ISW
QT_BIN_DIR=/home/ctuser/Qt/5.12.10/gcc_64/bin
#QT_BIN_DIR=$HOME_DIR/Qt5/5.13.2/gcc_64/bin

# Check input arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: build_lats.sh branch_name {y/n}"
fi
echo $#

cd $BUILD_DIR

if [ $2 == y ]
then
	if [ -d $BUILD_DIR/$1 ]
	then
	    echo "Directory exists - removing it"
	    rm -rf $1
	fi

	mkdir -p $1
	cd $BUILD_DIR/$1

	# Clone the software onto the test vm
	git clone -b $1 https://bitbucket.di2e.net/scm/isw/lats.git
fi

# Build UsbInterface Unit Tests
# UsbInterface
# IswInterface
# SolNet
cd $BUILD_DIR/$1/lats/UnitTests/
mkdir -p build-Usb_UnitTests-$1
cd $BUILD_DIR/$1/lats/UnitTests/build-Usb_UnitTests-$1
$QT_BIN_DIR/qmake ../UsbInterface/Usb_UnitTests.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
./Usb_UnitTests

# Build IswInterface Unit Tests
cd $BUILD_DIR/$1/lats/UnitTests
mkdir -p build-Isw_UnitTests-$1
cd $BUILD_DIR/$1/lats/UnitTests/build-Isw_UnitTests-$1
$QT_BIN_DIR/qmake ../IswInterface/Isw_UnitTest.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
./Isw_UnitTest

# Build SolNet Unit Tests
cd $BUILD_DIR/$1/lats/UnitTests
mkdir -p build-SolNet_UnitTests-$1
cd $BUILD_DIR/$1/lats/UnitTests/build-SolNet_UnitTests-$1
$QT_BIN_DIR/qmake ../SolNet/SolNet_UnitTest.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
./SolNet_UnitTest

# Build the Integration Tests:
# IswInterface
# SolNet

cd $BUILD_DIR/$1/lats/IntegrationTests
mkdir -p build-Integration_IswTest
cd $BUILD_DIR/$1/lats/IntegrationTests/SolNet
mkdir -p build-Integration_SolNetMessagePkts
mkdir -p build-Integration_SolNetDataPkts

cd ../build-Integration_IswTest
$QT_BIN_DIR/qmake ../IswInterface/TestIswInterface.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd $BUILD_DIR/$1/lats/IntegrationTests/SolNet/build-Integration_SolNetMessagePkts
$QT_BIN_DIR/qmake ../TestMessagePkts/TestSolNetMessagePkts.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

cd ../build-Integration_SolNetDataPkts
$QT_BIN_DIR/qmake ../TestDataPkts/TestSolNetDataPkts.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Make library
cd $BUILD_DIR/$1/lats/
mkdir -p build-$1-IswInterfaceLib
cd build-$1-IswInterfaceLib
$QT_BIN_DIR/qmake ../IswInterface/IswInterface.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1
cp ./libIswInterface.so.1.3.0 ../IswInterface/

# Give time for copy to complete before deleting directories
sleep 1

# Build the GUI:
cd $BUILD_DIR/$1/lats
mkdir -p build-$1-GUI
cd ./build-$1-GUI
$QT_BIN_DIR/qmake ../iswTestTool/iswTestTool.pro -spec linux-g++ CONFIG+=debug CONFIG+=qtquickcompiler
make clean -j1
make -j1

# Cleanup
cd $BUILD_DIR/$1/lats/
sudo rm -rf */build-*/
sudo rm -rf */*/build-*/
sudo rm -rf */*/*/build-*/
sudo rm -rf ./build-*/

