//!#################################################
//! Filename: SerialInterface.h
//! Description: Class that provides an serial interface
//!              to the Linux libusb
//!              for threadsafe I/O on a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <stddef.h>
#include <libftdi1/ftdi.h>

#include "../Logger/Logger.h"

class SerialInterface
{
public:
    SerialInterface(Logger *logger);
    ~SerialInterface();

    //! The index is used for logging
    //! It is the position in the USB and ISW array
    uint8_t GetIndex(void) { return (usbIndex); }
    void SetIndex(uint8_t index) { usbIndex = index; }

    int OpenDevice(int index, int countSerial, int initCompleted, int indexL);
    void CloseDevice(void);
    int SyncWriteDevice(uint8_t *sendBuf, uint32_t sendLength);
    int ReadDevice(uint8_t *receiveBuf, int recLength, int *numRead);

private:
    void ConvertBufferHexToAscii(uint8_t *sendBuf, uint32_t sendLength, uint8_t *asciiSendBuf, uint32_t asciiBufLength);
    void ConvertBufferAsciiToHex(char *asciiReceiveBuf, uint32_t asciiBufLength, uint8_t *receiveBuf, uint32_t *receiveBufLength);
    void SetSerialOptions();

    Logger *theLogger = nullptr;
    uint8_t usbIndex = 0;

    struct ftdi_context *ftdi = nullptr;
    struct ftdi_version_info version;
    struct ftdi_device_list *devlist = nullptr;
};

#endif // SERIALINTERFACE_H
