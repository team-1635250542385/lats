//!#################################################
//! Filename: SerialInterface.cpp
//! Description: Class that provides an serial interface
//!              to the Linux libusb
//!              for threadsafe I/O on a USB device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "SerialInterface.h"

//! C library headers
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <cstring>
#include <iomanip>

//! Linux headers
#include <stdio.h>
#include <fcntl.h>      // Contains file controls like O_RDWR
#include <errno.h>      // Error integer and strerror() function
#include <unistd.h>     // write(), read(), close()

//! Project headers
#include "../UsbInterface/UsbInterfaceInit.h"

SerialInterface::SerialInterface(Logger *logger) :
    theLogger(logger)
{
    if (theLogger == nullptr)
    {
        std::cout << "Logger pointer!" << std::endl;
        exit(-1);
    }

    //! Initialize Serial Device
    if ( (ftdi = ftdi_new()) == nullptr )
    {
        if ( theLogger->DEBUG_SerialInterface == 1 )
        {
            std::stringstream ss;
            ss << "ftdi_new failed" << strerror(errno) << std::endl;
            std::cout << ss.str();
            theLogger->DebugMessage(ss.str(), GetIndex());
        }
    }
    else
    {
        ftdi_init(ftdi);
    }
}

SerialInterface::~SerialInterface()
{
    if ( ftdi != nullptr )
    {
        int status = ftdi_usb_purge_buffers(ftdi);
        if ( status != 0 )
        {
            std::stringstream ss;
            ss << "ftdi_usb_purge_buffers failed" << std::endl;
            theLogger->DebugMessage(ss.str(), GetIndex());
        }

        CloseDevice();
        ftdi_free(ftdi);
    }
}

int SerialInterface::OpenDevice(int index, int countSerial, int initCompleted, int indexL)
{
    //! Open FTDI device based on FT232R vendor & product IDs
    //! Does ftdi_usb_open_desc:
    //!     - Does usb_init
    //!     - Looks for devices that match the vendor and product codes, and also match the description and serial number values unless they are null.
    //!     - To check the description and serial numbers, each device is opened using usb_open, checked, and then closed using usb_close.
    //!     - Ends by reopening the selected device (the first one matched) using ftdi_usb_open_dev and returns the file handle.
    //! ftdi_usb_open_dev:
    //!     - Does usb_open
    //!     - Does usb_claim_interface
    //!     - Does ftdi_usb_reset
    //!     - Does ftdi_set_baudrate
    //!     - Ascertains chip type, and for 2322C, sets ftdi.index to 1 if not already set.

    int openStatus = -1;
    if (initCompleted == 0)
    {
        openStatus = ftdi_usb_open_desc_index(ftdi, UsbInterfaceInit::SERIAL_BOARD_VENDOR_ID, UsbInterfaceInit::ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID, NULL, NULL, countSerial);
        std::cout<<"During Init, index   cntSerial   openStatus = "<<index<<"   "<<countSerial<<"   "<<openStatus<<std::endl;
    }
    else if (initCompleted > 0 && countSerial > 0)
    {
        openStatus = ftdi_usb_open_desc_index(ftdi, UsbInterfaceInit::SERIAL_BOARD_VENDOR_ID, UsbInterfaceInit::ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID, NULL, NULL, indexL);
        //openStatus = ftdi_usb_open(ftdi, UsbInterfaceInit::SERIAL_BOARD_VENDOR_ID, UsbInterfaceInit::ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID);
        std::cout<<"After Init, index and cntSerial and openStatus are "<<index<<" and "<<countSerial<<" and "<<openStatus<<std::endl;
    }
    else if (initCompleted > 0)
    {
        openStatus = ftdi_usb_open(ftdi, UsbInterfaceInit::SERIAL_BOARD_VENDOR_ID, UsbInterfaceInit::ALEREON_CAMOUFLAGE_BOARD_PRODUCT_ID);
        std::cout<<"Adding serialDev0 after Init"<<std::endl;
    }

    if ( openStatus < 0 )
    {
        if ( theLogger->DEBUG_SerialInterface == 1 )
        {
            std::stringstream ss;
            ss << "Can't open ftdi device" << std::endl;
            std::cout << ss.str();
            theLogger->DebugMessage(ss.str(), GetIndex());
        }
    }
    else
    {        
        //! Set Serial Parameters
        int status1 = ftdi_set_baudrate(ftdi, 9600);
        int status2 = ftdi_set_line_property(ftdi, BITS_8, STOP_BIT_1, NONE);
        int status3 = ftdi_setflowctrl(ftdi, SIO_XON_XOFF_HS);

        if ( (status1 == 0) && (status2 == 0) && (status3 == 0) )
        {
            status3 = ftdi_usb_purge_buffers(ftdi);
            if ( status3 < 0 )
            {
                if ( theLogger->DEBUG_SerialInterface == 1 )
                {
                    std::stringstream ss;
                    ss << "ftdi_usb_purge_buffers failed" << std::endl;
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
            }

            unsigned int chunksize = 4064;
            int status4 = ftdi_read_data_set_chunksize(ftdi, chunksize);

            if ( status4 < 0 )
            {
                if ( theLogger->DEBUG_SerialInterface == 1 )
                {
                    std::stringstream ss;
                    ss << "ftdi_read_data_set_chunksize failed" << std::endl;
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
            }
        }
        else
        {
            openStatus = -1;
            ftdi_usb_close(ftdi);
        }
    }

    return (openStatus);
}

void SerialInterface::CloseDevice()
{
    int status = ftdi_usb_close(ftdi);
    if ( status < 0 )
    {
        if ( theLogger->DEBUG_SerialInterface == 1 )
        {
            std::stringstream ss;
            ss << "Unable to close ftdi device" << ftdi_get_error_string(ftdi) << std::endl;
            theLogger->DebugMessage(ss.str(), GetIndex());
        }
    }
    else
    {
        ftdi_free(ftdi);
        ftdi = nullptr;
    }
}

void SerialInterface::ConvertBufferHexToAscii(uint8_t *sendBuf, uint32_t sendLength, uint8_t *asciiSendBuf, uint32_t asciiBufLength)
{
    //! Asciify the whole buffer
    uint8_t hexToAscii[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    uint32_t iPos = 0;

    for (uint32_t i = 0; i < sendLength; i++)
    {
        //! sendBuf contains some binary data
        const char cHex = sendBuf[i];
        asciiSendBuf[iPos++] = hexToAscii[(cHex >> 4) & 0x0F];
        asciiSendBuf[iPos++] = hexToAscii[cHex & 0x0F];
    }

    asciiSendBuf[iPos++] = '\r';
    asciiSendBuf[iPos]   = '\n';

    //! Show the converted buffer of ascii characters
    if ( theLogger->DEBUG_SerialInterface == 1 )
    {
        std::stringstream ss;
        ss << "ConvertBufferHexToAscii" << std::endl;
        ss << "sendLength     = " << std::to_string(sendLength) << std::endl;
        ss << "iPos           = " << std::to_string(iPos) << std::endl;
        ss << "asciiBufLength = " << std::to_string(asciiBufLength) << std::endl;
        ss << "asciiSendBuf   = " << std::endl;

        for (uint32_t i = 0; i < asciiBufLength; i++)
        {
            ss << " " << (char)asciiSendBuf[i];
        }

        ss << std::endl;
        theLogger->DebugMessage(ss.str(), GetIndex());

        std::cout << ss.str();
    }
}

int SerialInterface::SyncWriteDevice(uint8_t *sendBuf, uint32_t sendLength)
{
    //! Show the original buffer of hex data
    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        std::stringstream ss;
        ss << "sendBuf = ";
        ss << std::setfill('0');

        for (unsigned int i = 0; i < sendLength; i++)
        {
            ss << std::hex << std::setw(2) << (int)sendBuf[i] << " ";
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), GetIndex());

        std::cout << ss.str();
    }
    uint32_t asciiBufLength = (sendLength * 2) + 1;
    uint8_t asciiSendBuf[asciiBufLength];
    std::memset(asciiSendBuf, 0, asciiBufLength);
    int w_status = 0, status = -1;
    //! Convert the raw data buffer to ascii characters
    ConvertBufferHexToAscii(sendBuf, sendLength, asciiSendBuf, asciiBufLength);
    if (sendLength < 65) {
        w_status = ftdi_write_data(ftdi, asciiSendBuf, asciiBufLength);
        usleep(100000);
    }
    else
    {
        w_status = ftdi_write_data(ftdi, asciiSendBuf, asciiBufLength);
        usleep(100000);
    }
    if (w_status > 0) {
        status = 0;
    }
    return (status);
}

void SerialInterface::ConvertBufferAsciiToHex(char *asciiReceiveBuf, uint32_t asciiBufLength, uint8_t *receiveBuf, uint32_t *receiveBufLength)
{
    bool highOrder = true;
    char inputChar;
    uint8_t hexValue = 0x00;
    int numHexRead = 0;

    if ( (theLogger->DEBUG_SerialInterface == 1) &&
         (asciiBufLength > 0) )
    {
        std::stringstream ss;
        ss << "ConvertBufferAsciiToHex" << std::endl;
        ss << "asciiBufLength = " << std::to_string(asciiBufLength) << std::endl;
        ss << "asciiReceiveBuf = " << std::endl;

        for (uint32_t i = 0; i < asciiBufLength; i++)
        {
            ss << " " << (char)asciiReceiveBuf[i];
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), GetIndex());

        std::cout << ss.str();
    }

    for (uint32_t i = 0; i < asciiBufLength; i++)
    {
        //! Process the buffer - one character per byte
        inputChar = asciiReceiveBuf[i] & 0xFF;

        if ( ((inputChar >= '0') && (inputChar <= '9')) ||
             ((inputChar >= 'A') && (inputChar <= 'F')) ||
             ((inputChar >= 'a') && (inputChar <= 'f')) )
        {
            //! ASCII value of a hex nibble received
            if ( highOrder == true )
            {
                //! expecting high order nibble; convert from ASCII
                if ( inputChar > '9' )
                {
                    //! A - F
                    hexValue = (uint8_t)((inputChar - '7') << 4);
                }
                else
                {
                    //! 0 - 9
                    hexValue = (uint8_t)((inputChar - '0') << 4);
                }
                highOrder = false;
            }
            else
            {
                //! expecting low order nibble; convert from ASCII
                if (inputChar > '9')
                {
                    //! A - F
                    hexValue |= (uint8_t)(inputChar - '7');
                }
                else
                {
                    //! 0 - 9
                    hexValue |= (uint8_t)(inputChar - '0');
                }

                //! Add hexValue to the raw byte data buffer
                receiveBuf[numHexRead] = hexValue;
                numHexRead++;

                highOrder = true;
            }
        }
        else
        {
            //! line feed or return received; every received entry should be
            //! terminated by a return ('\r'); for compatibility with output,
            //! every entry may be terminated with a return and a line feed
            //! ('\r\n' or '\n\r'); the return will be processed, and the line
            //! feed will be ignored

            if ( (inputChar == '\r') || (inputChar == '\n') )
            {
                break;
            }
        }
    }

    *receiveBufLength = numHexRead;
}

int SerialInterface::ReadDevice(uint8_t *receiveBuf, int recLength, int *numRead)
{
    int status = -1;
    int asciiBytesRead = 0;
    int bytesRead = 1;
    //int recLength_s = 1600;

    //! Read in character buffer from serial device
    char asciiReceiveBuf[recLength];
    std::memset(asciiReceiveBuf, 0, recLength);
    uint32_t recBufLength = 0;

    if ( bytesRead > 0 )
    {
        bytesRead = ftdi_read_data(ftdi, (unsigned char*)asciiReceiveBuf, recLength);
        asciiBytesRead = bytesRead;
        if (bytesRead>1024) {
            //std::cout<<"bytesRead = "<<bytesRead<<std::endl;

        }
    }
    //usleep(100);

    if ( asciiBytesRead > 0 )
    {
        ConvertBufferAsciiToHex(asciiReceiveBuf, asciiBytesRead, receiveBuf, &recBufLength);
        *numRead = recBufLength;
        status = 0;
        //! Show the final buffer of hex data
        if ( theLogger->DEBUG_IswInterface == 1 )
        {
            std::stringstream ss;
            ss << "receiveBuf = ";
            ss << std::setfill('0');
            for (unsigned int i = 0; i < recBufLength; i++)
            {
                ss << std::hex << std::setw(2) << (int)receiveBuf[i] << " ";
            }
            ss << std::endl;
            theLogger->DebugMessage(ss.str(), GetIndex());

            std::cout << ss.str();
        }
    }
    else
    {
        *numRead = 0;
    }

    return (status);
}
