//###############################################
// Filename: Test_SolNet.cpp
// Description: Class that provides methods
//              to test IswInterface class
//###############################################
#include "Test_SolNet.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <zlib.h>
#include <assert.h>

TestSolNet::TestSolNet()
{

}

TestSolNet::~TestSolNet()
{

}

void TestSolNet::initTestCase()
{
    // This is called before other tests are run
    std::string testFilename = "unitTestIsw";
    uint16_t debugLevel      = 0x0000;
    theLogger                = new Logger(testFilename, debugLevel);
    assert(theLogger);

    dblogger = new DBLogger(testFilename, "1");
    assert(dblogger);

    iswInterfaceInit  = new IswInterfaceInit(IswInterface::USB, theLogger, dblogger);
    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();

    assert(*iswInterfaceArray->begin() != nullptr);

    // Just get the first one for this test
    iswInterface = *iswInterfaceArray->begin();

    usbInterface = iswInterface->GetUsbInterface();

    assert(usbInterface != nullptr);
    assert(usbInterface->IsDeviceOpen() == true);

    if (usbInterface->GetProductId() == UsbInterfaceInit::ALEREON_COMBAT_BOARD_PRODUCT_ID)
    {
        // Combat board
        assert(usbInterface->GetMaxPacketSizeIn() == 512);
        assert(usbInterface->GetMaxPacketSizeOut() == 512);
    }
    else if (usbInterface->GetProductId() == UsbInterfaceInit::ALEREON_PARALLEL_BOARD_PRODUCT_ID)
    {
        // Commander Board
        assert(usbInterface->GetMaxPacketSizeIn() == 1024);
        assert(usbInterface->GetMaxPacketSizeOut() == 1024);
    }

    // Get the friend classes
    iswFirmware    = iswInterface->GetIswFirmware();
    iswStream      = iswInterface->GetIswStream();
    iswSystem      = iswInterface->GetIswSystem();
    iswIdentity    = iswInterface->GetIswIdentity();
    iswSecurity    = iswInterface->GetIswSecurity();
    iswAssociation = iswInterface->GetIswAssociation();
    iswLink        = iswInterface->GetIswLink();
    iswMetrics     = iswInterface->GetIswMetrics();
    iswProduct     = iswInterface->GetIswProduct();
    iswSolNet      = iswInterface->GetIswSolNet();

    assert(iswFirmware != nullptr);
    assert(iswStream != nullptr);
    assert(iswSystem != nullptr);
    assert(iswIdentity != nullptr);
    assert(iswSecurity != nullptr);
    assert(iswAssociation != nullptr);
    assert(iswLink != nullptr);
    assert(iswMetrics != nullptr);
    assert(iswProduct != nullptr);
    assert(iswSolNet != nullptr);

    assert(iswInterface->theLogger == theLogger);
    assert(iswInterface->deviceInterface == usbInterface);
    assert(usbInterface->usbHeaderType == UsbInterface::USB_HEADER_TRU);
    assert(iswInterface->isLittleEndian == true);
    assert(iswInterface->GetIndex() == 0);  // First device has index 0

    //  Test QoS per endpoint - can be used for Hi/Low queues sending data
    iswInterface->SetQosOnEndpoint(IswInterface::HiNoDiscard, 0);
    assert(iswInterface->iswEndpointSendQueues[0].qOs == IswInterface::HiNoDiscard);
    iswInterface->SetQosOnEndpoint(IswInterface::LowDiscard, 1);
    assert(iswInterface->iswEndpointSendQueues[1].qOs == IswInterface::LowDiscard);
    iswInterface->SetQosOnEndpoint(IswInterface::HiDiscard, 2);
    assert(iswInterface->iswEndpointSendQueues[2].qOs == IswInterface::HiDiscard);
    iswInterface->SetQosOnEndpoint(IswInterface::LowNoDiscard, 3);
    assert(iswInterface->iswEndpointSendQueues[3].qOs == IswInterface::LowNoDiscard);

    // Test that send queues are empty at startup
    assert(iswInterface->iswEndpointSendQueues[0].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[1].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[2].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[3].sendQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[0].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[1].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[2].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[3].receiveQ.empty() == true);
}

void TestSolNet::init()
{

}

void TestSolNet::TestSolNetInit()
{
    iswSolNet->RemoveAllServices(true);

    for (uint8_t dataflowId = 0; dataflowId < IswSolNet::MAX_NUMBER_APPS; dataflowId++)
    {
        assert(iswSolNet->iswSolNetServices[dataflowId].inUse == false);
        assert(iswSolNet->iswSolNetServices[dataflowId].dataflowId == dataflowId);
        assert(iswSolNet->iswSolNetServices[dataflowId].endpointId == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].dataPolicies == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].endpointDistribution == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].nextSendDataSeqNumber == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].receiveCallbackFn == nullptr);
        assert(iswSolNet->iswSolNetServices[dataflowId].pollDataCallbackFn == nullptr);
        assert(iswSolNet->iswSolNetServices[dataflowId].serviceDesc.header.length == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].serviceDesc.header.descriptorId == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].serviceDesc.serviceSelector == 0);
        assert(iswSolNet->iswSolNetServices[dataflowId].serviceDesc.childBlock == nullptr);

        // Verify the registered peers initialization
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomy == 0);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].peerRegistered == false);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendRegRequestResponse == false);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].regRequestResponseSeqNo == 0);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendDeregistrationRequestResponse == false);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].deregRequestResponseSeqNo == 0);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendAutonomousStartStopResponse == false);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomousStartStopSeqNo == 0);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendGetStatusResponse == false);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].getStatusResponseSeqNo == 0);
           assert(iswSolNet->iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList == nullptr);
        }
    }

    // Verify the peer services initialization
    iswSolNet->RemoveAllPeerServices(true);

    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].inUse == false);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].sendBrowse == false);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].sendAdvertise == false);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse == false);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo == 0);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse == false);
        assert(iswSolNet->iswSolNetPeerServices[peerIndex].revokeNetworkAssociationResponseSeqNo == 0);

        for (uint8_t dataflowId = 0; dataflowId < IswSolNet::MAX_NUMBER_APPS; dataflowId++)
        {
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].inUse == false);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].dataflowId == dataflowId);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].endpointId == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].dataPolicies == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].endpointDistribution == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].autonomy == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].status == IswSolNet::RegStatusAvailableToRegister);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered == false);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].receiveCallbackFn == nullptr);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].nextSendDataSeqNumber == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.header.length == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.header.descriptorId == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == 0);
            assert(iswSolNet->iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.childBlock == nullptr);
        }
    }

    // Test verify settings in IswSolNet at start
    assert(iswSolNet->verifyAdvertiseMessages == false);
    assert(iswSolNet->verifyChecksum == true);

    // Change verify settings
    iswSolNet->setVerifyAdvertiseMsgs(true);
    assert(iswSolNet->verifyAdvertiseMessages == true);

    iswSolNet->setVerifyChecksum(false);
    assert(iswSolNet->verifyChecksum == false);

    // Set back to orig
    iswSolNet->setVerifyAdvertiseMsgs(false);
    assert(iswSolNet->verifyAdvertiseMessages == false);

    iswSolNet->setVerifyChecksum(true);
    assert(iswSolNet->verifyChecksum == true);
}

void TestSolNet::TestSetupSolNetMessagePacket()
{
    // Test setup a SolNet Message Packet
    IswSolNet::iswSolNetHeader iswSolNetHdr;

    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;
    uint8_t msgStatus          = IswSolNet::MsgSuccess;

    // Test Message Packet first
    uint8_t messageId      = 0;
    std::string testString = "This is a test";
    uint8_t payload[IswSolNet::payloadMsgBufferSize];
    uint16_t payloadLength = testString.size();
    int status             = 0;
    uint8_t policies       = 0;
    uint8_t seqNumber      = 0;

    // Test a message class is reserved
    uint8_t messageClass = 0;
    status               = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == -1);

    messageClass = 0x41;
    status       = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == -1);

    // Test Message Id bad
    messageClass = IswSolNet::Discovery;
    messageId    = 0;
    status       = 0;
    status       = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == -1);

    // Test Message payload length = zero
    messageClass  = IswSolNet::Discovery;
    messageId     = IswSolNet::BrowseServices;
    payloadLength = 0;
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == 0);

    for (int i = 0; i < IswSolNet::payloadMsgBufferSize; i++)
    {
        assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[i] == 0);
    }
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message policy = payload checksum
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Discovery;
    messageId     = IswSolNet::BrowseServices;
    payloadLength = testString.size();
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    memcpy(payload, (uint8_t *)testString.data(), testString.size());
    policies          = 1;
    uint16_t checksum = 0;
    checksum          = iswSolNet->IswChecksum(payloadLength, (uint16_t *)testString.data());

    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.reserved1 == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert (std::strcmp((char *)iswSolNetHdr.iswSolNetMessagePkt.messagePayload, testString.c_str()) == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == checksum);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test header checksum
    iswSolNetHdr.version = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass = IswSolNet::Discovery;
    messageId    = IswSolNet::BrowseServices;
    testString.clear();
    testString    = "New message for checksum test";
    payloadLength = testString.size();
    memcpy(payload, (uint8_t *)testString.data(), testString.size());
    policies                    = 0;
    iswSolNetHdr.protocolClass                     = IswSolNet::MessagePacket;
    iswSolNetHdr.version                           = 0;
    iswSolNetHdr.iswSolNetMessagePkt.messageClass  = messageClass;
    iswSolNetHdr.iswSolNetMessagePkt.messageId     = messageId;
    iswSolNetHdr.iswSolNetMessagePkt.seqNumber     = iswSolNet->GetNextMsgSeqNo();
    iswSolNetHdr.iswSolNetMessagePkt.status        = 0;
    iswSolNetHdr.iswSolNetMessagePkt.reserved1     = 0;
    iswSolNetHdr.iswSolNetMessagePkt.messageLength = payloadLength;

    // Calculate the header checksum upfront so we can compare
    iswSolNetHdr.iswSolNetMessagePkt.headerChecksum = 0;
    uint32_t hdrChecksum                            = 0;
    iswSolNet->CalculateHeaderChecksum(&iswSolNetHdr);
    hdrChecksum = iswSolNetHdr.iswSolNetMessagePkt.headerChecksum;
    iswSolNetHdr.iswSolNetMessagePkt.headerChecksum = 0;

    // Reset header and seqNumber
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));
    // GetNextMsgSeqNo() increments this, so decrement
    iswSolNet->iswSolNetMessageSeqNo--;

    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.reserved1 == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert (iswSolNetHdr.iswSolNetMessagePkt.headerChecksum == hdrChecksum);
    assert (std::strcmp((char *)iswSolNetHdr.iswSolNetMessagePkt.messagePayload, testString.c_str()) == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message is Discovery - Advertise
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass = IswSolNet::Discovery;
    messageId    = IswSolNet::ServiceAdvertiseAnnounce;
    testString.clear();
    testString    = "New message for Discovery Advertise test";
    payloadLength = testString.size();
    memcpy(payload, (uint8_t *)testString.data(), testString.size());
    policies = 0;
    status   = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.reserved1 == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert (std::strcmp((char *)iswSolNetHdr.iswSolNetMessagePkt.messagePayload, testString.c_str()) == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message is Discovery - Advertise policy
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass = IswSolNet::Discovery;
    messageId    = IswSolNet::ServiceAdvertiseAnnounce;
    testString.clear();
    testString    = "New message for Discovery Advertise test";
    payloadLength = testString.size();
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    memcpy(payload, (uint8_t *)testString.data(), testString.size());
    policies = 1;
    checksum = 0;
    checksum = iswSolNet->IswChecksum(payloadLength, (uint16_t *)testString.data());
    status   = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.reserved1 == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert (std::strcmp((char *)iswSolNetHdr.iswSolNetMessagePkt.messagePayload, testString.c_str()) == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == checksum);

    // Test Adding SWIM Header for Sol Net using the above setttings
    uint8_t peerIndex = 4;
    uint8_t endpoint  = 0;  // The endpoint is always zero for Message Packets
    uint8_t transferbuf[512];
    memset(transferbuf, 0, 512);
    IswInterface::iswCommand iswCmd;
    memcpy(&iswCmd.iswSolNetHdr, &iswSolNetHdr, sizeof(IswSolNet::iswSolNetHeader));
    uint32_t length = 0;
    //int AddSolNetSwimHeader(uint8_t *transferBuf, iswCommand *iswCmd, uint8_t peerIndex, uint8_t endpoint);
    length = iswInterface->AddSolNetSwimHeader(transferbuf, &iswCmd, peerIndex, endpoint);
    IswInterface::SWIM_Header *swimHeader = (IswInterface::SWIM_Header *)(transferbuf);
    assert(swimHeader->flags == 0);
    assert(swimHeader->streamNumber == 0);
    assert(swimHeader->peerIndex == peerIndex);
    uint32_t pktSize =IswSolNet::Size_SolNet_Header_Only + IswSolNet::Size_SolNet_MsgPktHdr + iswCmd.iswSolNetHdr.iswSolNetMessagePkt.messageLength;
    assert(length == (sizeof(IswInterface::SWIM_Header) + pktSize));
    assert(swimHeader->transferSize == pktSize);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message Advertise Change
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Discovery;
    messageId     = IswSolNet::ServiceAdvertiseChange;
    payloadLength = 0;
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == 0);
    for (int i = 0; i < IswSolNet::payloadMsgBufferSize; i++)
    {
        assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[i] == 0);
    }
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Flow Message RegisterRequest
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::RegisterRequest;
    seqNumber     = 0;
    policies      = 0;
    payloadLength = sizeof(IswSolNet::iswSolNetRegisterRequest);
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    status = 0;

    // Fill in the payload with the Register Request message
    uint32_t serviceSelector                             = iswSolNet->GetNextServiceNo();
    uint8_t autonomy                                     = 1;

    IswSolNet::iswSolNetRegisterRequest *registerRequest = (IswSolNet::iswSolNetRegisterRequest *)payload;
    registerRequest->serviceSelector                     = serviceSelector;
    registerRequest->autonomy                            = autonomy;
    //registerRequest->reserved is already set to zero by memset above

    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == sizeof(IswSolNet::iswSolNetRegisterRequest));
    assert ((uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] == serviceSelector);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[4] == autonomy);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    uint8_t responseSeqNumber = iswSolNetHdr.iswSolNetMessagePkt.seqNumber;

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message RegisterRequestResponse
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::RegisterResponse;
    payloadLength = 0;
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, responseSeqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == responseSeqNumber);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == 0);

    for (int i = 0; i < IswSolNet::payloadMsgBufferSize; i++)
    {
        assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[i] == 0);
    }
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Flow Message KeepAliveRequest
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::KeepAliveRequest;
    seqNumber     = 0;
    policies      = 0;
    payloadLength = sizeof(IswSolNet::iswSolNetKeepAliveRequest);
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    status = 0;

    // Fill in the payload with the Keep Alive Request message
    serviceSelector                                        = iswSolNet->GetNextServiceNo();
    uint8_t initiator                                      = 0x01; // Service Provider
    IswSolNet::iswSolNetKeepAliveRequest *keepAliveRequest = (IswSolNet::iswSolNetKeepAliveRequest *)payload;
    keepAliveRequest->serviceSelector                      = serviceSelector;
    keepAliveRequest->initiator                            = initiator;

    std::cout << "keepAliveRequest->serviceSelector = " << std::to_string(keepAliveRequest->serviceSelector) << std::endl;
    std::cout << "keepAliveRequest->initiator = " << std::to_string(keepAliveRequest->initiator) << std::endl;

    // Setup Keep Alive Request SolNet Message
    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    // Test Keep Alive Request SolNet Message
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert ((uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] == serviceSelector);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[4] == initiator); //IswSolNet::SERVICE_PROVIDER);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

//    responseSeqNumber = iswSolNetHdr.iswSolNetMessagePkt.seqNumber;

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize); // &iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0]
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message KeepAliveResponse
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::KeepAliveResponse;
    payloadLength = sizeof(IswSolNet::iswSolNetKeepAliveResponse);
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, responseSeqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == responseSeqNumber);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
    assert ((uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] == serviceSelector);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[4] == initiator);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Report Dataflow Condition Indication
//    iswSolNetHdr.version = 0;
//    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

//    messageClass = IswSolNet::Reporting;
//    messageId = IswSolNet::ReportDataflowConditionIndication;
//    seqNumber = 0;
//    policies = 0;
//    payloadLength = sizeof(IswSolNet::iswSolNetReportDataflowConditionIndication);
//    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
//    status = 0;

//    // Fill in the payload with the Report Dataflow Condition message
//    serviceSelector = iswSolNet->GetNextServiceNo();
//    std::cout << "iswSolNet->GetNextServiceNo() = " << iswSolNet->GetNextServiceNo() << std::endl;

//    uint8_t endpointNumber = 2;
//    uint8_t dataflowId = iswSolNet->GetNextDataflowId();
//    uint8_t condition = 0x02; // Data received on congested endpoint/dataflow ID combination
//    IswSolNet::iswSolNetReportDataflowConditionIndication *reportDataflowCondition = (IswSolNet::iswSolNetReportDataflowConditionIndication *)payload;
//    reportDataflowCondition->serviceSelector = serviceSelector;
//    reportDataflowCondition->endpointNumber = endpointNumber;
//    reportDataflowCondition->dataflowId = dataflowId;
//    reportDataflowCondition->condition = condition;

//    std::cout << "reportDataflowCondition->serviceSelector= " << std::to_string(reportDataflowCondition->serviceSelector) << std::endl;
//    std::cout << "reportDataflowCondition->endpointNumber= " << std::to_string(reportDataflowCondition->endpointNumber) << std::endl;
//    std::cout << "reportDataflowCondition->dataflowId= " << std::to_string(reportDataflowCondition->dataflowId) << std::endl;
//    std::cout << "reportDataflowCondition->condition= " << std::to_string(reportDataflowCondition->condition) << std::endl;

//    // Setup Report Dataflow Condition SolNet Message
//    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

//    std::cout << "(uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] = " << (uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] << std::endl;
//    std::cout << "serviceSelector = " << serviceSelector << std::endl;

//    // Test Report Dataflow Condition SolNet Message
//    assert (status == 0);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
//    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == payloadLength);
//    assert ((uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] == serviceSelector);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[1] == endpoint);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[2] == dataflowId);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[3] == condition);
//    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

//    responseSeqNumber = iswSolNetHdr.iswSolNetMessagePkt.seqNumber;

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize); // &iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0]
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Flow Message Autonomous Flow Request
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;
//    uint8_t payload[IswSolNet::payloadMsgBufferSize];

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::AutonomousStartStopRequest;
    seqNumber     = 0;
    policies      = 0;
    payloadLength = sizeof(IswSolNet::iswSolNetAutonomousStartStopRequest);
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    status = 0;

    // Fill in the payload with the Autonomous Flow Request message
    serviceSelector                                                       = iswSolNet->GetNextServiceNo();
    uint8_t requestedFlowState                                            = 0;
    IswSolNet::iswSolNetAutonomousStartStopRequest *autonomousFlowRequest = (IswSolNet::iswSolNetAutonomousStartStopRequest *)payload;
    autonomousFlowRequest->serviceSelector                                = serviceSelector;
    autonomousFlowRequest->flowState                                      = requestedFlowState;

//    memcpy(payload, &autonomousFlowRequest, sizeof(IswSolNet::iswSolNetAutonomousStartStopRequest));
    status = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == sizeof(IswSolNet::iswSolNetAutonomousStartStopRequest));
    assert ((uint32_t)iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0] == serviceSelector);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[1] == requestedFlowState);
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    responseSeqNumber = iswSolNetHdr.iswSolNetMessagePkt.seqNumber;

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message AutonomousFlowResponse
    iswSolNetHdr.version = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::AutonomousStartStopResponse;
    payloadLength = 0;
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, responseSeqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == responseSeqNumber);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == 0);
    for (int i = 0; i < IswSolNet::payloadMsgBufferSize; i++)
    {
        assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[i] == 0);
    }
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload, 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Flow Message Poll Data Request
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::PollDataRequest;
    seqNumber     = 0;
    policies      = 0;
    payloadLength = sizeof(IswSolNet::iswSolNetMessagePacket);
    memset(payload, 0, IswSolNet::payloadMsgBufferSize);
    status = 0;

    // Fill in the payload with the Autonomous Flow Request message
    serviceSelector = iswSolNet->GetNextServiceNo();
    status          = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, seqNumber,
                                                          msgStatus, messageClass, messageId, policies, payload, payloadLength);

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == (iswSolNet->GetNextMsgSeqNo()-1));
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == sizeof(IswSolNet::iswSolNetMessagePacket));
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);

    responseSeqNumber = iswSolNetHdr.iswSolNetMessagePkt.seqNumber;

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetMessagePkt.messagePayload, 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));

    // Test Message PollDataResponse
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::MessagePacket;

    messageClass  = IswSolNet::Flow;
    messageId     = IswSolNet::PollDataResponse;
    payloadLength = 0;
    policies      = 0;
    status        = iswSolNet->SetupSolNetMessagePacket(&iswSolNetHdr, responseSeqNumber, msgStatus, messageClass, messageId, policies, payload, payloadLength);
    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageClass == messageClass);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageId == messageId);
    assert (iswSolNetHdr.iswSolNetMessagePkt.seqNumber == responseSeqNumber);
    assert (iswSolNetHdr.iswSolNetMessagePkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetMessagePkt.messageLength == 0);
    for (int i = 0; i < IswSolNet::payloadMsgBufferSize; i++)
    {
        assert (iswSolNetHdr.iswSolNetMessagePkt.messagePayload[i] == 0);
    }
    assert (iswSolNetHdr.iswSolNetMessagePkt.payloadChecksum == 0);
}

void TestSolNet::TestSetupSolNetDataPacket()
{
    // Test setup a SolNet Data Packet
    IswSolNet::iswSolNetHeader iswSolNetHdr;
    IswSolNet::iswRetransmitItem *retransItem = nullptr;
    iswSolNetHdr.version       = 0;
    iswSolNetHdr.protocolClass = IswSolNet::DataPacket;

    std::string dataTestString = "This is a data test";
    uint8_t data[IswSolNet::payloadDataBufferSize];
    uint16_t dataLength = dataTestString.size();
    memset(data, 0, IswSolNet::payloadDataBufferSize);
    memcpy(data, (uint8_t *)dataTestString.data(), dataLength);

    uint8_t dataflowId = 1;
    int status         = 0;
    uint8_t policies   = 1;
    uint16_t checksum  = 0;
    checksum           = iswSolNet->IswChecksum(dataLength, (uint16_t *)dataTestString.data());
    status             = iswSolNet->SetupSolNetDataPacket(&iswSolNetHdr, dataflowId, policies, data, dataLength, &retransItem);

    uint8_t checkNextSeqNo = 0;
    for (uint16_t i = 0; i < IswSolNet::MAX_NUMBER_APPS; i++)
    {
        if ( (iswSolNet->iswSolNetServices[i].inUse) &&
           (iswSolNet->iswSolNetServices[i].dataflowId == dataflowId) )
        {
            checkNextSeqNo = iswSolNet->iswSolNetServices[i].nextSendDataSeqNumber - 1;
            break;
        }
    }

    assert (status == 0);
    assert (iswSolNetHdr.iswSolNetDataPkt.dataflowId == dataflowId);
    assert (iswSolNetHdr.iswSolNetDataPkt.seqNumber == checkNextSeqNo);
    assert (iswSolNetHdr.iswSolNetDataPkt.policies == policies);
    assert (iswSolNetHdr.iswSolNetDataPkt.reserved1 == 0);
    assert (iswSolNetHdr.iswSolNetDataPkt.reserved2 == 0);
    assert (iswSolNetHdr.iswSolNetDataPkt.dataLength == dataLength);
    assert (std::strcmp((char *)iswSolNetHdr.iswSolNetDataPkt.dataPayload, dataTestString.c_str()) == 0);
    assert (iswSolNetHdr.iswSolNetDataPkt.payloadChecksum == checksum);

    // Verify Ack Packet setup
    policies = IswSolNet::POLICY_ACK;
    status   = iswSolNet->SetupSolNetDataPacket(&iswSolNetHdr, dataflowId, policies, data, dataLength, &retransItem);

    assert (status == 0);
    assert (retransItem != nullptr);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.dataflowId == iswSolNetHdr.iswSolNetDataPkt.dataflowId);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.seqNumber == iswSolNetHdr.iswSolNetDataPkt.seqNumber);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.policies == iswSolNetHdr.iswSolNetDataPkt.policies);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.reserved1 == 0);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.reserved2 == 0);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.dataLength == iswSolNetHdr.iswSolNetDataPkt.dataLength);
    assert (std::strcmp((char *)retransItem->iswSolNetHdr.iswSolNetDataPkt.dataPayload, dataTestString.c_str()) == 0);
    assert (retransItem->iswSolNetHdr.iswSolNetDataPkt.payloadChecksum == iswSolNetHdr.iswSolNetDataPkt.payloadChecksum);

    // Reset buffers
    memset(&iswSolNetHdr.iswSolNetDataPkt.dataPayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&retransItem->iswSolNetHdr.iswSolNetDataPkt.dataPayload[0], 0, IswSolNet::payloadMsgBufferSize);
    memset(&iswSolNetHdr, 0, sizeof(IswSolNet::iswSolNetHeader));
}

void TestSolNet::Test_VerifySolNet_GeneralPropertyDescriptors()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    // *****************************************************
    // Status Service Descriptor
    // - Property Descriptors:
    //   - Label
    //   - Version
    //   - Endpoint - unicast
    //   - Flow Policy
    //   - Encoding
    //   - Device Name Descriptor
    //   - Device Serial Number Descriptor
    //   - Device Manufacturer Descriptor
    //   - Device Friendly Name Descriptor
    // *****************************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Status;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    // Label Property Descriptor
    std::string label = "Status Label";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    // Version Property Descriptor
    std::string version = "Version1";
    IswSolNet::iswSolNetPropertyVersionDesc versionDesc;
    memset(&versionDesc.versionText, 0, IswSolNet::SIZE_VERSION_TEXT);
    memcpy(&versionDesc.versionText, version.c_str(), version.size());
    versionDesc.header.length = version.size() + sizeof(versionDesc.solNetVersion) + sizeof(versionDesc.reserved);
    versionDesc.header.descriptorId = IswSolNet::Version;
    versionDesc.solNetVersion = 1;
    versionDesc.reserved[0] = 0;
    versionDesc.reserved[1] = 0;
    versionDesc.reserved[2] = 0;

    // Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0;

    // Flow Policy Property Descriptor
    // autonomy: Bit 0: Polling supported (0-no, 1-yes)
    //           Bit 1: Autonomous data supported(0-no, 1-yes)
    IswSolNet::iswSolNetPropertyFlowPolicyDesc flowPolicyDesc;
    flowPolicyDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyFlowPolicyDesc) - IswSolNet::commonHeaderSize;
    flowPolicyDesc.header.descriptorId = IswSolNet::FlowPolicy;
    flowPolicyDesc.autonomy = 3;
    flowPolicyDesc.reserved[0] = 0;
    flowPolicyDesc.reserved[1] = 0;
    flowPolicyDesc.reserved[2] = 0;

    IswSolNet::iswSolNetPropertyEncodingDesc encodingDesc;
    encodingDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEncodingDesc) - IswSolNet::commonHeaderSize;
    encodingDesc.header.descriptorId = IswSolNet::Encoding;
    encodingDesc.encoding = 0;
    encodingDesc.reserved[0] = 0;
    encodingDesc.reserved[1] = 0;
    encodingDesc.reserved[2] = 0;

    // This struct can be used for properties:
    // Device Name Descriptor
    // Device Serial Number Descriptor
    // Device Manufacturer Descriptor
    // Device Friendly Name Descriptor
    // They are all the same
    std::string deviceNameStr = "HMD01";
    IswSolNet::iswSolNetPropertyDeviceDesc deviceDesc;
    memset(&deviceDesc.devString, 0, 256);
    memcpy(&deviceDesc.devString, deviceNameStr.c_str(), deviceNameStr.size());
    deviceDesc.header.length = deviceNameStr.size();
    deviceDesc.header.descriptorId = IswSolNet::DeviceName;

    IswSolNet::iswSolNetPropertyAssociatedServiceDesc associatedDesc;
    associatedDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc) - IswSolNet::commonHeaderSize;
    associatedDesc.header.descriptorId = IswSolNet::AssociatedService;
    associatedDesc.associatedServiceId = IswSolNet::Video;
    associatedDesc.associationType = 1; //0x00=Dependent On; 0x01=Sibling; 0x02-0xFF=Reserved
    associatedDesc.reserved = 0;
    associatedDesc.associatedServiceSelector = iswSolNet->GetNextServiceNo();  // Testing only - in real world you would look up the service

    uint32_t childblockLength = labelDesc.header.length +
                                versionDesc.header.length +
                                endpointDesc.header.length +
                                flowPolicyDesc.header.length +
                                encodingDesc.header.length +
                                deviceDesc.header.length +
                                associatedDesc.header.length +
                                (IswSolNet::commonHeaderSize * 7);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    int offset = 0;
    memcpy(&childblock[offset], &labelDesc, (labelDesc.header.length + IswSolNet::commonHeaderSize));
    offset += labelDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &versionDesc, (versionDesc.header.length + IswSolNet::commonHeaderSize));
    offset += versionDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &endpointDesc, (endpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += endpointDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &flowPolicyDesc, (flowPolicyDesc.header.length + IswSolNet::commonHeaderSize));
    offset += flowPolicyDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &encodingDesc, (encodingDesc.header.length + IswSolNet::commonHeaderSize));
    offset += encodingDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &deviceDesc, (deviceDesc.header.length + IswSolNet::commonHeaderSize));
    offset += deviceDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &associatedDesc, (associatedDesc.header.length + IswSolNet::commonHeaderSize));
    offset += associatedDesc.header.length + IswSolNet::commonHeaderSize;

    // Finalize the serviceEntry
    serviceEntry.dataflowId                 = endpointDesc.dataflowId;
    serviceEntry.endpointId                 = endpointDesc.endpointId;
    serviceEntry.dataPolicies               = endpointDesc.dataPolicies;
    serviceEntry.endpointDistribution       = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock     = childblock;
    serviceEntry.serviceDesc.header.length += childblockLength;

    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
    assert(verifyService == true);

    // *****************************************
    // Cleanup
    iswSolNet->RemoveAllServices(true);
    // *****************************************

    // *****************************************
    // Reset pointers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    serviceDesc.childBlock = nullptr;
    verifyService = false;
    // *****************************************
}

void TestSolNet::Test_VerifySolNet_GeneralPropDesc_Endpoint_Unicast()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    // *****************************************************
    // Status Service Descriptor
    // - General Property Descriptors:
    //   - Endpoint - unicast
    // *****************************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Status;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    // Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0;

    uint32_t childblockLength = endpointDesc.header.length + IswSolNet::commonHeaderSize;
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    int offset = 0;

    memcpy(&childblock[offset], &endpointDesc, (endpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += endpointDesc.header.length + IswSolNet::commonHeaderSize;

    // Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = childblock;
    serviceEntry.serviceDesc.header.length += childblockLength;

    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
    assert(verifyService == true);

    // *****************************************
    // Cleanup
    iswSolNet->RemoveAllServices(true);
    // *****************************************

    // *****************************************
    // Reset pointers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    serviceDesc.childBlock = nullptr;
    verifyService = false;
    // *****************************************
}

void TestSolNet::Test_VerifySolNet_GeneralPropDesc_Endpoint_Broadcast()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    // *****************************************************
    // Status Service Descriptor
    // - General Property Descriptors:
    //   - Endpoint - broadcast
    // *****************************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Status;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    // Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = IswSolNet::Unicast;
    endpointDesc.dataPolicies = 0;

    // The user selected broadcast
    // Add and EndpointDescriptor with broadcast indicated
    IswSolNet::iswSolNetPropertyEndpointDesc broadcastEndpointDesc;
    broadcastEndpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    broadcastEndpointDesc.header.descriptorId = IswSolNet::iswSolNetPropertyDescIds::Endpoint;
    broadcastEndpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    broadcastEndpointDesc.endpointId = endpointDesc.endpointId;
    broadcastEndpointDesc.endpointDistribution = IswSolNet::Broadcast;
    broadcastEndpointDesc.dataPolicies = endpointDesc.dataPolicies;

    uint32_t childblockLength = endpointDesc.header.length + broadcastEndpointDesc.header.length + (IswSolNet::commonHeaderSize * 2);
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);
    int offset = 0;

    memcpy(&childblock[offset], &endpointDesc, (endpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += endpointDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &broadcastEndpointDesc, (broadcastEndpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += broadcastEndpointDesc.header.length + IswSolNet::commonHeaderSize;

    // Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = childblock;
    serviceEntry.serviceDesc.header.length += childblockLength;

    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
    assert(verifyService == true);

    // *****************************************
    // Cleanup
    iswSolNet->RemoveAllServices(true);
    // *****************************************

    // *****************************************
    // Reset pointers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    serviceDesc.childBlock = nullptr;
    verifyService = false;
    // *****************************************
}

void TestSolNet::Test_VerifySolNet_GeneralPropDesc_Endpoint_Multicast()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    // *****************************************************
    // Status Service Descriptor
    // - General Property Descriptors:
    //   - Endpoint - Multicast
    // *****************************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Status;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    // Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 1; // Multicast => requires a multicast property desc
    endpointDesc.dataPolicies = 0;

    IswSolNet::iswSolNetPropertyMulticastGroupDesc multicastDesc;
    multicastDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyMulticastGroupDesc) - IswSolNet::commonHeaderSize;
    multicastDesc.header.descriptorId = IswSolNet::MulticastGroup;
    multicastDesc.mcastAddress = 65287;
    multicastDesc.mcastGroupId = 64;

    // Get the MAC Address string - it can be the default for unit tests
    std::string macAddrStr = iswIdentity->GetMacAddressStr();
    int mcastMacAddress = std::stoi(macAddrStr);
    memcpy(multicastDesc.mcastMacAddress, &mcastMacAddress, 6);

    uint32_t childblockLength = endpointDesc.header.length + multicastDesc.header.length + (IswSolNet::commonHeaderSize * 2);
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    int offset = 0;

    memcpy(&childblock[offset], &endpointDesc, (endpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += endpointDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &multicastDesc, (multicastDesc.header.length + IswSolNet::commonHeaderSize));
    offset += multicastDesc.header.length + IswSolNet::commonHeaderSize;

    // Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = childblock;
    serviceEntry.serviceDesc.header.length += childblockLength;

    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
    assert(verifyService == true);

    // *****************************************
    // Cleanup
    iswSolNet->RemoveAllServices(true);
    // *****************************************

    // *****************************************
    // Reset pointers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    serviceDesc.childBlock = nullptr;
    verifyService = false;
    // *****************************************
}

//! ************************************************************
//! Video Service Descriptor - with child descriptors
//! ************************************************************
//! Endpoint Descriptor and possibly Multicast Group Descriptor
//! Label Descriptor
//! Video Format Descriptor
//! Video Sample Format Descriptor
//! Video Protocol Descriptor
//! Video IFOV Descriptor
//! Video Principal Point Descriptor
//! Image Sensor Type Descriptor
//! Image Sensor Waveband Descriptor
//! Video Lens Distortion Descriptor
//! Video Control Descriptor
//! ************************************************************
void TestSolNet::TestSolNet_VideoDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Video;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = 0x01;
    endpointDesc.endpointDistribution = 0x00; // Unicast
    endpointDesc.dataPolicies = 0x05;

    //! Text Label Property Descriptor
    std::string label = "Video";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Video Format Property Descriptor
    IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc videoFormatRawDesc;
    videoFormatRawDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc) - IswSolNet::commonHeaderSize;
    videoFormatRawDesc.header.descriptorId = IswSolNet::VideoOutputFormatRaw;
    videoFormatRawDesc.resX = 640;
    videoFormatRawDesc.resY = 480;
    videoFormatRawDesc.frameRate = 30;
    videoFormatRawDesc.bitDepth = 8;
    videoFormatRawDesc.pixelformat = IswSolNet::PIXEL_FORMAT_NV21;

    IswSolNet::iswSolNetPropertyVideoFormatDesc videoFormatDesc;
    videoFormatDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoFormatDesc) - IswSolNet::commonHeaderSize;
    videoFormatDesc.header.descriptorId = IswSolNet::VideoFormat;
    videoFormatDesc.videoFormatX = 640;
    videoFormatDesc.videoFormatY = 480;
    videoFormatDesc.frameRate = 30;
    videoFormatDesc.formatOptions = 0x00;
    videoFormatDesc.reserved1 = 0x00;
    videoFormatDesc.encodingFormat = 0x0000;

    //! Video Sample Format Descriptor
    IswSolNet::iswSolNetPropertyVideoSampleFormatDesc videoSampleDesc;
    videoSampleDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoSampleFormatDesc) - IswSolNet::commonHeaderSize;
    videoSampleDesc.header.descriptorId = IswSolNet::VideoSampleFormat;
    videoSampleDesc.bitDepth = 0x0008; // The bit depth is 8-bits Y
    videoSampleDesc.pixelFormat = IswSolNet::PIXEL_FORMAT_Y; // Luminance only (Y')(Grayscale)

    //! Video Protocol Property Descriptor
    IswSolNet::iswSolNetPropertyVideoProtocolDesc videoProtocolRawDesc;
    videoProtocolRawDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoProtocolDesc) - IswSolNet::commonHeaderSize;
    videoProtocolRawDesc.header.descriptorId = IswSolNet::Protocol;
    videoProtocolRawDesc.protocol = IswSolNet::FVTS;
    videoProtocolRawDesc.reserved = 0;

    //! IFOV Property Descriptor
    IswSolNet::iswSolNetPropertyVideoIfovDesc videoIFOVDesc;
    videoIFOVDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoIfovDesc) - IswSolNet::commonHeaderSize;
    videoIFOVDesc.header.descriptorId = IswSolNet::IFOV;
    videoIFOVDesc.IFoV = 0x1234;

    //! Video Principal Point descriptor
    IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc videoPrincipalPointDesc;
    memset(&videoPrincipalPointDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc));
    videoPrincipalPointDesc.principalPointX = 10;
    videoPrincipalPointDesc.principalPointY = 10;
    videoPrincipalPointDesc.header.descriptorId = IswSolNet::iswSolNetPropertyDescIds::PrincipalPoint;
    videoPrincipalPointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc) - IswSolNet::commonHeaderSize;

    //! Image Sensor Type Descriptor
    std::string imageSensorTypeDescription = "Video Image Sensor Type";
    IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc imageSensorTypeDesc;
    imageSensorTypeDesc.header.length = sizeof (IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc) - IswSolNet::commonHeaderSize;
    imageSensorTypeDesc.header.descriptorId = IswSolNet::ImageSensorType;
    imageSensorTypeDesc.type = IswSolNet::IMAGE_SENSOR_TYPE_COMPUTER;
    imageSensorTypeDesc.reserved1 = 0x0000;

    //! Image Sensor Waveband Descriptor
    std::string imageSensorWavebandDescription = "Video Image Sensor Waveband";
    IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc imageSensorWavebandDesc;

    imageSensorWavebandDesc.header.length = sizeof (IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc) - IswSolNet::commonHeaderSize;
    imageSensorWavebandDesc.header.descriptorId = IswSolNet::ImageSensorWaveband;
    imageSensorWavebandDesc.waveband = IswSolNet::IMAGE_SENSOR_WAVEBAND_XRAY;
    imageSensorWavebandDesc.reserved1 = 0x0000;

    //! Video Lens Distortion Descriptor
    IswSolNet::iswSolNetPropertyVideoLensDistortionDesc videoLensDistortionDesc;
    memset(&videoLensDistortionDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoLensDistortionDesc));
    videoLensDistortionDesc.header.descriptorId = IswSolNet::iswSolNetPropertyDescIds::VideoLensDistortion;
    videoLensDistortionDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc) - IswSolNet::commonHeaderSize;
//    for (int i=0; i<6; i++)
//    {
//        videoLensDistortionDesc.distortionCoefficients[i] = i;
//    }

    //! Video Control Descriptor
    IswSolNet::iswSolNetPropertyVideoControlDesc videoControlDesc;
    memset(&videoControlDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoControlDesc));
    videoControlDesc.header.descriptorId = IswSolNet::iswSolNetPropertyDescIds::VideoControl;
    videoControlDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoControlDesc) - IswSolNet::commonHeaderSize;
    videoControlDesc.controlBitmap = 0x01; // 1=Video-to-IMU sync timestamp appears at beginning of every frame;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength = endpointDesc.header.length
            + labelDesc.header.length
            + videoFormatRawDesc.header.length
            + videoFormatDesc.header.length
            + videoSampleDesc.header.length
            + videoProtocolRawDesc.header.length
            + videoIFOVDesc.header.length
            + videoPrincipalPointDesc.header.length
            + imageSensorTypeDesc.header.length
            + imageSensorWavebandDesc.header.length
            + videoLensDistortionDesc.header.length
            + videoControlDesc.header.length +
                                (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 11);
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;
    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    memcpy(&childblock[offset], &videoFormatRawDesc, (IswSolNet::commonHeaderSize + videoFormatRawDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoFormatRawDesc.header.length;

    memcpy(&childblock[offset], &videoFormatDesc, (IswSolNet::commonHeaderSize + videoFormatDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoFormatDesc.header.length;

    memcpy(&childblock[offset], &videoSampleDesc, (IswSolNet::commonHeaderSize + videoSampleDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoSampleDesc.header.length;

    memcpy(&childblock[offset], &videoProtocolRawDesc, (IswSolNet::commonHeaderSize + videoProtocolRawDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoProtocolRawDesc.header.length;

    memcpy(&childblock[offset], &videoIFOVDesc, (IswSolNet::commonHeaderSize + videoIFOVDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoIFOVDesc.header.length;

    memcpy(&childblock[offset], &videoPrincipalPointDesc, (IswSolNet::commonHeaderSize + videoPrincipalPointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoPrincipalPointDesc.header.length;

    memcpy(&childblock[offset], &imageSensorTypeDesc, (IswSolNet::commonHeaderSize + imageSensorTypeDesc.header.length));
    offset += IswSolNet::commonHeaderSize + imageSensorTypeDesc.header.length;

    memcpy(&childblock[offset], &imageSensorWavebandDesc, (IswSolNet::commonHeaderSize + imageSensorWavebandDesc.header.length));
    offset += IswSolNet::commonHeaderSize + imageSensorWavebandDesc.header.length;

    memcpy(&childblock[offset], &videoLensDistortionDesc, (IswSolNet::commonHeaderSize + videoLensDistortionDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoLensDistortionDesc.header.length;

    memcpy(&childblock[offset], &videoControlDesc, (IswSolNet::commonHeaderSize + videoControlDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoControlDesc.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);  // Need to update VerifySolNetServiceEntry Function
//    assert(verifyService == true);
}

//! ************************************************************
//! RTA Service Descriptor - with child descriptors
//! ************************************************************
//! Associated Service Descriptor
//! Text Label Descriptor
//! Control Endpoint Descriptor
//! Data Endpoint Descriptor
//! RTA IMU Rate Property Descriptor
//! ************************************************************
void TestSolNet::TestSolNet_RtaDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::RTA;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Associated Service Descriptor
    IswSolNet::iswSolNetPropertyAssociatedServiceDesc associatedServiceDesc;
    associatedServiceDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc) - IswSolNet::commonHeaderSize;
    associatedServiceDesc.header.descriptorId = IswSolNet::AssociatedService;
    associatedServiceDesc.associatedServiceId = 0x0001;
    associatedServiceDesc.associationType = 0x00;
    associatedServiceDesc.reserved = 0x00;
    associatedServiceDesc.associatedServiceSelector = iswSolNet->GetNextServiceNo(); // For Testing Purposes Only

    //! Text Label Property Descriptor
    std::string label = "RTA";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Control Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyRtaControlEndpointDesc controlEndpointDesc;
    controlEndpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyRtaControlEndpointDesc) - IswSolNet::commonHeaderSize;
    controlEndpointDesc.header.descriptorId = IswSolNet::Endpoint;
    controlEndpointDesc.endpointId = 1;
    controlEndpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    controlEndpointDesc.endpointDistribution = 0; // Unicast
    controlEndpointDesc.dataPolicies = 0x09;

    //! Data Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyRtaDataEndpointDesc dataEndpointDesc;
    dataEndpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyRtaDataEndpointDesc) - IswSolNet::commonHeaderSize;
    dataEndpointDesc.header.descriptorId = IswSolNet::Endpoint;
    dataEndpointDesc.endpointId = 1;
    dataEndpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    dataEndpointDesc.endpointDistribution = 0; // Unicast
    dataEndpointDesc.dataPolicies = 0x05;

    //! RTA IMU Rate Property Descriptor
    IswSolNet::iswSolNetPropertyImuOutputRateDesc rtaImuRateDesc;
    rtaImuRateDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyImuOutputRateDesc) - IswSolNet::commonHeaderSize;
    rtaImuRateDesc.header.descriptorId = IswSolNet::ImuOutputRate;
    rtaImuRateDesc.rate = 120;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength =
            associatedServiceDesc.header.length
            + labelDesc.header.length
            + controlEndpointDesc.header.length
            + dataEndpointDesc.header.length
            + rtaImuRateDesc.header.length
            + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 5);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;
    memcpy(&childblock[offset], &associatedServiceDesc, (IswSolNet::commonHeaderSize + associatedServiceDesc.header.length));
    offset += IswSolNet::commonHeaderSize + associatedServiceDesc.header.length;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;
//    memcpy(&childblock[offset], &lrfCapabilitiesDesc, (IswSolNet::commonHeaderSize + lrfCapabilitiesDesc.header.length));
//    offset += IswSolNet::commonHeaderSize + lrfCapabilitiesDesc.header.length;

    memcpy(&childblock[offset], &controlEndpointDesc, (IswSolNet::commonHeaderSize + controlEndpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + controlEndpointDesc.header.length;

    memcpy(&childblock[offset], &dataEndpointDesc, (IswSolNet::commonHeaderSize + dataEndpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + dataEndpointDesc.header.length;

    memcpy(&childblock[offset], &rtaImuRateDesc, (IswSolNet::commonHeaderSize + rtaImuRateDesc.header.length));
    offset += IswSolNet::commonHeaderSize + rtaImuRateDesc.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = controlEndpointDesc.dataflowId;
    serviceEntry.endpointId = controlEndpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = controlEndpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

//! ************************************************************
//! Motion Sense Service Descriptor - with child descriptors
//! ************************************************************
//! Text Label Descriptor
//! Endpoint Descriptor
//! Motion Sensor Rate Descriptor
//! Association Property Descriptor
//! Motion Sensor Accelerometer Property Descriptor
//! Motion Sensor Gyroscope Property Descriptor
//! Motion Sensor Magnetometer Property Descriptor
//! Motion Sensor Axes Property Descriptor
//! ************************************************************
void TestSolNet::TestSolNet_MotionSenseDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::MotionSense;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Text Label Property Descriptor
    std::string label = "Motion Sense";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Endpoint Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0x05;

    //! Motion Sensor Rate Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorRateDesc motionSensorRate;
    motionSensorRate.header.length = sizeof(IswSolNet::iswSolNetPropertyMotionSensorRateDesc) - IswSolNet::commonHeaderSize;
    motionSensorRate.header.descriptorId = IswSolNet::MotionSensorRate;
    motionSensorRate.rate = 120;

    //! Association Descriptor
    IswSolNet::iswSolNetPropertyAssociationDesc associationDesc;
    associationDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyAssociationDesc) - IswSolNet::commonHeaderSize;
    associationDesc.header.descriptorId = IswSolNet::AssociatedService;
    associationDesc.associatedServiceId = 0x0001;
    associationDesc.associationType = 0x01; // The association is a sibling to the Video Service
    associationDesc.reserved1 = 0x00;
    associationDesc.associatedServiceSelector = iswSolNet->GetNextServiceNo(); // For Testing Purposes Only

    //! Motion Sensor Axes Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorAxes motionSensorAxes;
    motionSensorAxes.header.length = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAxes) - IswSolNet::commonHeaderSize;
    motionSensorAxes.header.descriptorId = IswSolNet::MotionSensorAxes;
    motionSensorAxes.numberOfAxes = 3;
    motionSensorAxes.reserved[0] = 0x00000;
    motionSensorAxes.reserved[1] = 0x00000;
    motionSensorAxes.reserved[2] = 0x00000;

    //! Motion Sensor Accelerometer Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorAccel motionSensorAccel;
    motionSensorAccel.header.length = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAccel);
    motionSensorAccel.header.descriptorId = IswSolNet::MOTION_SENSE_ACCEL;

    //! Motion Sensor Gyroscope Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorGyro motionSensorGyro;
    motionSensorGyro.header.length = sizeof(IswSolNet::iswSolNetPropertyMotionSensorGyro);
    motionSensorGyro.header.descriptorId = IswSolNet::MOTION_SENSE_GYRO;

    //! Motion Sensor Magnetometer Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorMag motionSensorMag;
    motionSensorMag.header.length = sizeof(IswSolNet::iswSolNetPropertyMotionSensorGyro);
    motionSensorMag.header.descriptorId = IswSolNet::MOTION_SENSE_MAG;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength =
            associationDesc.header.length
            + labelDesc.header.length
            + endpointDesc.header.length
            + motionSensorRate.header.length
            + motionSensorAxes.header.length
            + motionSensorAccel.header.length
            + motionSensorGyro.header.length
            + motionSensorMag.header.length
            + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 8);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;
    memcpy(&childblock[offset], &associationDesc, (IswSolNet::commonHeaderSize + associationDesc.header.length));
    offset += IswSolNet::commonHeaderSize + associationDesc.header.length;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    memcpy(&childblock[offset], &motionSensorRate, (IswSolNet::commonHeaderSize + motionSensorRate.header.length));
    offset += IswSolNet::commonHeaderSize + motionSensorRate.header.length;

    memcpy(&childblock[offset], &motionSensorAxes, (IswSolNet::commonHeaderSize + motionSensorAxes.header.length));
    offset += IswSolNet::commonHeaderSize + motionSensorAxes.header.length;

    memcpy(&childblock[offset], &motionSensorAccel, (IswSolNet::commonHeaderSize + motionSensorAccel.header.length));
    offset += IswSolNet::commonHeaderSize + motionSensorAccel.header.length;

    memcpy(&childblock[offset], &motionSensorGyro, (IswSolNet::commonHeaderSize + motionSensorGyro.header.length));
    offset += IswSolNet::commonHeaderSize + motionSensorGyro.header.length;

    memcpy(&childblock[offset], &motionSensorMag, (IswSolNet::commonHeaderSize + motionSensorMag.header.length));
    offset += IswSolNet::commonHeaderSize + motionSensorMag.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

//! ************************************************************
//! Simple UI Service Descriptor - with child descriptors
//! ************************************************************
//! Endpoint Descriptor
//! Data Endpoint Descriptor
//! UI Motion Descriptor
//! UI Buttons Descriptor
//! ************************************************************
void TestSolNet::TestSolNet_SimpleUIDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::SimpleUI;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0;

    IswSolNet::iswSolNetPropertySimpleUiMotionDesc uiMotionDesc;
    uiMotionDesc.header.length = sizeof(IswSolNet::iswSolNetPropertySimpleUiMotionDesc) - IswSolNet::commonHeaderSize;
    uiMotionDesc.header.descriptorId = IswSolNet::UiMotion;
    uiMotionDesc.terminalId = 1;
    uiMotionDesc.type = 0; // 0 = motion; 1 = rotation
    uiMotionDesc.flags = 2; // Bit 0 = Updates; Bit 1 = Wrap at edges
    uiMotionDesc.xMax = 100;
    uiMotionDesc.xMin = 10;
    uiMotionDesc.yMax = 100;
    uiMotionDesc.yMin = 10;
    uiMotionDesc.zMax = 100;
    uiMotionDesc.zMin = 10;

    IswSolNet::iswSolNetPropertySimpleUiButtonInputDesc uiButtonInputDesc;
    uiButtonInputDesc.header.length = sizeof(IswSolNet::iswSolNetPropertySimpleUiButtonInputDesc) - IswSolNet::commonHeaderSize;
    uiButtonInputDesc.header.descriptorId = IswSolNet::UiButtonInput;
    uiButtonInputDesc.buttonCount = 2;
//    uiButtonInputDesc.capabilities = (IswSolNet::TERMINAL_SUPPORTS_STANDARD_FORMAT |
//                                      IswSolNet::TERMINAL_SUPPORTS_PRESS_EVENT |
//                                      IswSolNet::TERMINAL_SUPPORTS_RELEASE_EVENT |
//                                      IswSolNet::TERMINAL_SUPPORTS_HOLD_EVENT |
//                                      IswSolNet::TERMINAL_SUPPORTS_DOUBLE_PRESS);
    uiButtonInputDesc.stateCount = 2;
    uiButtonInputDesc.terminalId = 1;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength = endpointDesc.header.length +
                                uiMotionDesc.header.length +
                                uiButtonInputDesc.header.length +
                                (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 3);
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;

    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    memcpy(&childblock[offset], &uiMotionDesc, (IswSolNet::commonHeaderSize + uiMotionDesc.header.length));
    offset += IswSolNet::commonHeaderSize + uiMotionDesc.header.length;

    memcpy(&childblock[offset], &uiButtonInputDesc, (IswSolNet::commonHeaderSize + uiButtonInputDesc.header.length));
    offset += IswSolNet::commonHeaderSize + uiButtonInputDesc.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

//! ************************************************************
//! Status Service Descriptor - with child descriptors
//! ************************************************************
//! Text Label Descriptor
//! Broadcast Endpoint Descriptor
//! Unicast Endpoint Descriptor
//! ************************************************************
void TestSolNet::TestSolNet_StatusDescriptors()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Status;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Text Label Property Descriptor
    std::string label = "FWS-I Status";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Unicast Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = IswSolNet::Unicast;
    endpointDesc.dataPolicies = 0;

    //! Broadcast Endpoint Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc broadcastEndpointDesc;
    broadcastEndpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    broadcastEndpointDesc.header.descriptorId = IswSolNet::iswSolNetPropertyDescIds::Endpoint;
    broadcastEndpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    broadcastEndpointDesc.endpointId = endpointDesc.endpointId;
    broadcastEndpointDesc.endpointDistribution = IswSolNet::Broadcast;
    broadcastEndpointDesc.dataPolicies = endpointDesc.dataPolicies;

    uint32_t childblockLength = labelDesc.header.length + endpointDesc.header.length + broadcastEndpointDesc.header.length + (IswSolNet::commonHeaderSize * 2);
    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);
    int offset = 0;

    memcpy(&childblock[offset], &endpointDesc, (endpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += endpointDesc.header.length + IswSolNet::commonHeaderSize;

    memcpy(&childblock[offset], &broadcastEndpointDesc, (broadcastEndpointDesc.header.length + IswSolNet::commonHeaderSize));
    offset += broadcastEndpointDesc.header.length + IswSolNet::commonHeaderSize;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = childblock;
    serviceEntry.serviceDesc.header.length += childblockLength;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

void TestSolNet::TestSolNet_LrfDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::LrfService;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Text Label Property Descriptor
    std::string label = "LRF Service";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Endpoint Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0x09;

    //! LRF Capabilities Descriptor
    IswSolNet::iswSolNetLrfCapabilitiesDesc lrfCapabilitiesDesc;
    lrfCapabilitiesDesc.header.length = sizeof(IswSolNet::iswSolNetLrfCapabilitiesDesc) - IswSolNet::commonHeaderSize;
    lrfCapabilitiesDesc.header.descriptorId = IswSolNet::MotionSensorRate;
    lrfCapabilitiesDesc.capabilities = 0x07;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength =
            + labelDesc.header.length
            + endpointDesc.header.length
            + lrfCapabilitiesDesc.header.length
            + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 8);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    memcpy(&childblock[offset], &lrfCapabilitiesDesc, (IswSolNet::commonHeaderSize + lrfCapabilitiesDesc.header.length));
    offset += IswSolNet::commonHeaderSize + lrfCapabilitiesDesc.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

void TestSolNet::TestSolNet_LrfControlDescriptors()
{
    //! Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    bool verifyService = false;

    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::LrfControlService;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    //! Text Label Property Descriptor
    std::string label = "LRF Control";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    //! Endpoint Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0x09;

    //! Allocate a childblock for all the descriptors
    uint32_t childblockLength =
            + labelDesc.header.length
            + endpointDesc.header.length
            + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 8);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    //! Add all the property descriptors to the child block
    uint32_t offset = 0;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    //! Finalize the serviceEntry
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

//    verifyService = iswSolNet->VerifySolNetServiceEntry(&serviceEntry);
//    assert(verifyService == true);
}

void TestSolNet::TestSolNetDescriptors_OtherTests()
{
    // Test service descriptors
    IswSolNet::iswSolNetServiceDesc serviceDesc;
    IswSolNet::iswSolNetServiceEntry serviceEntry;

    // *****************************************
    // Video - no Property Descriptors
    // *****************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Video;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    serviceEntry.dataflowId = 1;
    serviceEntry.endpointId = 1;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = 0; // unicast

    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);

    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == true);

    IswSolNet::iswSolNetServiceEntry *testEntry = &iswSolNet->iswSolNetServices[serviceEntry.dataflowId];
    assert(testEntry->dataflowId == serviceEntry.dataflowId);
    assert(testEntry->endpointId == serviceEntry.endpointId);
    assert(testEntry->dataPolicies == serviceEntry.dataPolicies);
    assert(testEntry->endpointDistribution == serviceEntry.endpointDistribution);
    assert(testEntry->serviceDesc.header.length == serviceEntry.serviceDesc.header.length);
    assert(testEntry->serviceDesc.header.descriptorId == serviceEntry.serviceDesc.header.descriptorId);
    assert(testEntry->serviceDesc.childBlock == nullptr);

    // Defaults
    assert(testEntry->receiveCallbackFn == nullptr);
    assert(testEntry->pollDataCallbackFn == nullptr);

    // Initialize the registered peers
    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
        assert(testEntry->registeredPeers[peerIndex].autonomy == 0);
        assert(testEntry->registeredPeers[peerIndex].flowState == 0);
        assert(testEntry->registeredPeers[peerIndex].sendRegRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].regRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].deregRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse == false);
        assert(testEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendGetStatusResponse == false);
        assert(testEntry->registeredPeers[peerIndex].getStatusResponseSeqNo == 0);
    }

    // *****************************************
    // Cleanup
    iswSolNet->RemoveOneService(serviceEntry.dataflowId, true);
    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == false);
    // *****************************************

    // *****************************************
    // Reset buffers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    // *****************************************

    // *****************************************************
    // Video - with Property Descriptors
    // Test Adding Property Descriptors into the child block
    // *****************************************************

    // Video Class Descriptor - with Property Descriptors Label, Endpoint, IFOV, Format, Video Protocol
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);  // size of video class descriptor
    serviceDesc.header.descriptorId = IswSolNet::Video;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    // Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies = 0;

    // Label Property Descriptor
    std::string label = "Test Label 01";
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    memcpy(&labelDesc.textLabel, label.c_str(), label.size());
    labelDesc.header.length = label.size();
    labelDesc.header.descriptorId = IswSolNet::Label;

    // IFOV Property Descriptor
    IswSolNet::iswSolNetPropertyVideoIfovDesc videoIFOVDesc;
    videoIFOVDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoIfovDesc) - sizeof(IswSolNet::iswSolNetDescCommonHeader);
    videoIFOVDesc.header.descriptorId = IswSolNet::IFOV;
    videoIFOVDesc.IFoV = 0x1234;

    // Video Format Property Descriptor
    IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc videoFormatRawDesc;
    videoFormatRawDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc) - sizeof(IswSolNet::iswSolNetDescCommonHeader);
    videoFormatRawDesc.header.descriptorId = IswSolNet::VideoOutputFormatRaw;
    videoFormatRawDesc.resX = 640;
    videoFormatRawDesc.resY = 480;
    videoFormatRawDesc.frameRate = 30;
    videoFormatRawDesc.bitDepth = 8;
    videoFormatRawDesc.pixelformat = 0x12;

    // Video Protocol Property Descriptor
    IswSolNet::iswSolNetPropertyVideoProtocolDesc videoProtocolRawDesc;
    videoProtocolRawDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoProtocolDesc) - sizeof(IswSolNet::iswSolNetDescCommonHeader);
    videoProtocolRawDesc.header.descriptorId = IswSolNet::Protocol;
    videoProtocolRawDesc.protocol = IswSolNet::FVTS;
    videoProtocolRawDesc.reserved = 0;

    // Allocate a childblock for all the descriptors
    uint32_t childblockLength =
            labelDesc.header.length
            + endpointDesc.header.length
            + videoIFOVDesc.header.length
            + videoFormatRawDesc.header.length
            + videoProtocolRawDesc.header.length
            + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 5);

    uint8_t childblock[childblockLength];
    memset(childblock, 0, childblockLength);

    // Add all the property descriptors to the child block
    uint32_t offset = 0;
    memcpy(&childblock[offset], &endpointDesc, (IswSolNet::commonHeaderSize + endpointDesc.header.length));
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    memcpy(&childblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    memcpy(&childblock[offset], &videoIFOVDesc, (IswSolNet::commonHeaderSize + videoIFOVDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoIFOVDesc.header.length;

    memcpy(&childblock[offset], &videoFormatRawDesc, (IswSolNet::commonHeaderSize + videoFormatRawDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoFormatRawDesc.header.length;

    memcpy(&childblock[offset], &videoProtocolRawDesc, (IswSolNet::commonHeaderSize + videoProtocolRawDesc.header.length));
    offset += IswSolNet::commonHeaderSize + videoProtocolRawDesc.header.length;

    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.serviceDesc.childBlock = &childblock[0];
    serviceEntry.serviceDesc.header.length += offset;

    // Add this Video service descriptor with all the child property descriptors
    // to the IswSolNet->iswSolNetServices vector
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);

    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == true);

    testEntry = &iswSolNet->iswSolNetServices[serviceEntry.dataflowId];
    assert(testEntry->dataflowId == serviceEntry.dataflowId);
    assert(testEntry->endpointId == serviceEntry.endpointId);
    assert(testEntry->dataPolicies == serviceEntry.dataPolicies);
    assert(testEntry->endpointDistribution == serviceEntry.endpointDistribution);
    assert(testEntry->serviceDesc.header.length == serviceEntry.serviceDesc.header.length);
    assert(testEntry->serviceDesc.header.descriptorId == serviceEntry.serviceDesc.header.descriptorId);
    assert(testEntry->serviceDesc.childBlock != nullptr);

    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
    }

    offset = 0;
    // Verify Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc *checkEndpointDesc = (IswSolNet::iswSolNetPropertyEndpointDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkEndpointDesc->header.length == endpointDesc.header.length);
    assert(checkEndpointDesc->endpointId == endpointDesc.endpointId);
    assert(checkEndpointDesc->dataflowId == endpointDesc.dataflowId);
    assert(checkEndpointDesc->endpointDistribution == endpointDesc.endpointDistribution);
    assert(checkEndpointDesc->dataPolicies == endpointDesc.dataPolicies);
    offset += IswSolNet::commonHeaderSize + endpointDesc.header.length;

    // Verify the property descriptors in the childblock
    // Verify Label Property Descriptor
    IswSolNet::iswSolNetPropertyTextLabelDesc *checkLabelDesc = (IswSolNet::iswSolNetPropertyTextLabelDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkLabelDesc->header.length == labelDesc.header.length);
    assert(checkLabelDesc->header.descriptorId == labelDesc.header.descriptorId);
    for (int i=0; i<labelDesc.header.length; i++)
    {
        assert(checkLabelDesc->textLabel[i] == labelDesc.textLabel[i]);
    }
    offset += IswSolNet::commonHeaderSize + labelDesc.header.length;

    // Verify IFOV Property Descriptor
    IswSolNet::iswSolNetPropertyVideoIfovDesc *checkIfovDesc = (IswSolNet::iswSolNetPropertyVideoIfovDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkIfovDesc->header.length == videoIFOVDesc.header.length);
    assert(checkIfovDesc->header.descriptorId == videoIFOVDesc.header.descriptorId);
    assert(checkIfovDesc->IFoV == videoIFOVDesc.IFoV);
    offset += IswSolNet::commonHeaderSize + videoIFOVDesc.header.length;

    // Verify Video Format Property Descriptor
    IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc *checkFormatDesc = (IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkFormatDesc->header.length == videoFormatRawDesc.header.length);
    assert(checkFormatDesc->header.descriptorId == videoFormatRawDesc.header.descriptorId);
    assert(checkFormatDesc->resX == videoFormatRawDesc.resX);
    assert(checkFormatDesc->resY == videoFormatRawDesc.resY);
    assert(checkFormatDesc->frameRate == videoFormatRawDesc.frameRate);
    assert(checkFormatDesc->bitDepth == videoFormatRawDesc.bitDepth);
    assert(checkFormatDesc->pixelformat == videoFormatRawDesc.pixelformat);
    offset += IswSolNet::commonHeaderSize + videoFormatRawDesc.header.length;

    // Verify Video Protocol Property Descriptor
    IswSolNet::iswSolNetPropertyVideoProtocolDesc *checkProtocolDesc = (IswSolNet::iswSolNetPropertyVideoProtocolDesc *)&testEntry->serviceDesc.childBlock[offset];;
    assert(checkProtocolDesc->header.length == videoProtocolRawDesc.header.length);
    assert(checkProtocolDesc->header.descriptorId == videoProtocolRawDesc.header.descriptorId);
    assert(checkProtocolDesc->protocol == videoProtocolRawDesc.protocol);
    assert(checkProtocolDesc->reserved == 0);

    // Defaults
    assert(testEntry->receiveCallbackFn == nullptr);
    assert(testEntry->pollDataCallbackFn == nullptr);

    // Initialize the registered peers
    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
        assert(testEntry->registeredPeers[peerIndex].autonomy == 0);
        assert(testEntry->registeredPeers[peerIndex].flowState == 0);
        assert(testEntry->registeredPeers[peerIndex].sendRegRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].regRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].deregRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse == false);
        assert(testEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendGetStatusResponse == false);
        assert(testEntry->registeredPeers[peerIndex].getStatusResponseSeqNo == 0);
    }    

    // *****************************************
    // Cleanup
    iswSolNet->RemoveOneService(serviceEntry.dataflowId, true);
    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == false);
    // *****************************************

    // *****************************************
    // Reset buffers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    memset(childblock, 0, childblockLength);
    childblockLength = 0;
    // *****************************************

    // *****************************************
    // Video - with Device Property Descriptors
    // *****************************************
    // Video Class Descriptor - with Property Descriptors
    // Device Name, Serial Number, Manufacturer, and Friendly Name
    // *****************************************
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);  // size of video class descriptor
    serviceDesc.header.descriptorId = IswSolNet::Video;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    serviceEntry.dataflowId = 1;
    serviceEntry.endpointId = 1;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = 0; // unicast

    // Device Name Property Descriptor
    std::string deviceNameString = "Device01";
    IswSolNet::iswSolNetPropertyDeviceDesc devNameDesc;
    memset(&devNameDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
    devNameDesc.header.descriptorId = IswSolNet::DeviceName;
    memcpy(devNameDesc.devString, deviceNameString.c_str(), deviceNameString.size());
    // String must be multiple of 4 bytes
    devNameDesc.header.length = deviceNameString.size();
    while ( (devNameDesc.header.length % 4) > 0 )
    {
        devNameDesc.header.length++;
    }

    // Device Serial Number Property Descriptor
    std::string deviceSerialNoString = "0102030405";
    IswSolNet::iswSolNetPropertyDeviceDesc devSerialNoDesc;
    memset(&devSerialNoDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
    devSerialNoDesc.header.descriptorId = IswSolNet::DeviceSerialNumber;
    memcpy(devSerialNoDesc.devString, deviceSerialNoString.c_str(), deviceSerialNoString.size());
    // String must be multiple of 4 bytes
    devSerialNoDesc.header.length = deviceSerialNoString.size();
    while ( (devSerialNoDesc.header.length % 4) > 0 )
    {
        devSerialNoDesc.header.length++;
    }

    // Device Manufacturer Property Descriptor
    std::string deviceManufurerString = "MyTestDeviceCompany";
    IswSolNet::iswSolNetPropertyDeviceDesc devManufacturerDesc;
    memset(&devManufacturerDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
    devManufacturerDesc.header.descriptorId = IswSolNet::DeviceManufacturer;
    memcpy(devManufacturerDesc.devString, deviceManufurerString.c_str(), deviceManufurerString.size());
    // String must be multiple of 4 bytes
    devManufacturerDesc.header.length = deviceManufurerString.size();
    while ( (devManufacturerDesc.header.length % 4) > 0 )
    {
        devManufacturerDesc.header.length++;
    }

    // Device Friendly Name Descriptor
    std::string deviceFriendlyNameString = "VideoHelmet";
    IswSolNet::iswSolNetPropertyDeviceDesc devFriendlyNameDesc;
    memset(&devFriendlyNameDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
    devFriendlyNameDesc.header.descriptorId = IswSolNet::DeviceFriendlyName;
    memcpy(devFriendlyNameDesc.devString, deviceFriendlyNameString.c_str(), deviceFriendlyNameString.size());
    // String must be multiple of 4 bytes
    devFriendlyNameDesc.header.length = deviceFriendlyNameString.size();
    while ( (devFriendlyNameDesc.header.length % 4) > 0 )
    {
        devFriendlyNameDesc.header.length++;
    }

    // Allocate a childblock for all the descriptors
    childblockLength = devNameDesc.header.length + devSerialNoDesc.header.length + devManufacturerDesc.header.length + devFriendlyNameDesc.header.length + (sizeof(IswSolNet::iswSolNetDescCommonHeader) * 4);
    uint8_t deviceChildblock[childblockLength];
    memset(deviceChildblock, 0, childblockLength);

    // Add Device Name Property Descriptor
    offset = 0;
    memcpy(&deviceChildblock[offset], &devNameDesc, (IswSolNet::commonHeaderSize + devNameDesc.header.length));
    offset += IswSolNet::commonHeaderSize + devNameDesc.header.length;

    // Add Device Serial Number Property Descriptor
    memcpy(&deviceChildblock[offset], &devSerialNoDesc, (IswSolNet::commonHeaderSize + devSerialNoDesc.header.length));
    offset += IswSolNet::commonHeaderSize + devSerialNoDesc.header.length;

    // Add Device Manufacturer Property Descriptor
    memcpy(&deviceChildblock[offset], &devManufacturerDesc, (IswSolNet::commonHeaderSize + devManufacturerDesc.header.length));
    offset += IswSolNet::commonHeaderSize + devManufacturerDesc.header.length;

    // Add Device Friendly Name Property Descriptor
    memcpy(&deviceChildblock[offset], &devFriendlyNameDesc, (IswSolNet::commonHeaderSize + devFriendlyNameDesc.header.length));
    offset += IswSolNet::commonHeaderSize + devFriendlyNameDesc.header.length;

    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];
    serviceEntry.serviceDesc.header.length += offset;

    // Add this Video service descriptor with all the child property descriptors
    // to the IswSolNet->iswSolNetServices vector
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);

    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == true);

    testEntry = &iswSolNet->iswSolNetServices[serviceEntry.dataflowId];
    assert(testEntry->dataflowId == serviceEntry.dataflowId);
    assert(testEntry->endpointId == serviceEntry.endpointId);
    assert(testEntry->dataPolicies == serviceEntry.dataPolicies);
    assert(testEntry->endpointDistribution == serviceEntry.endpointDistribution);
    assert(testEntry->serviceDesc.header.length == serviceEntry.serviceDesc.header.length);
    assert(testEntry->serviceDesc.header.descriptorId == serviceEntry.serviceDesc.header.descriptorId);
    assert(testEntry->serviceDesc.childBlock != nullptr);
    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
    }

    offset = 0;
    // Verify the property descriptors in the childblock
    // Verify Device Name Property Descriptor
    IswSolNet::iswSolNetPropertyDeviceDesc *checkDevDesc = (IswSolNet::iswSolNetPropertyDeviceDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkDevDesc->header.length == devNameDesc.header.length);
    assert(checkDevDesc->header.descriptorId == devNameDesc.header.descriptorId);
    for (int i=0; i<devNameDesc.header.length; i++)
    {
        assert(checkDevDesc->devString[i] == devNameDesc.devString[i]);
    }
    offset += IswSolNet::commonHeaderSize + devNameDesc.header.length;

    // Verify Device Serial Number Property Descriptor
    checkDevDesc = (IswSolNet::iswSolNetPropertyDeviceDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkDevDesc->header.length == devSerialNoDesc.header.length);
    assert(checkDevDesc->header.descriptorId == devSerialNoDesc.header.descriptorId);
    for (int i=0; i<devSerialNoDesc.header.length; i++)
    {
        assert(checkDevDesc->devString[i] == devSerialNoDesc.devString[i]);
    }
    offset += IswSolNet::commonHeaderSize + devSerialNoDesc.header.length;

    // Verify Device Manufacturer Property Descriptor
    checkDevDesc = (IswSolNet::iswSolNetPropertyDeviceDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkDevDesc->header.length == devManufacturerDesc.header.length);
    assert(checkDevDesc->header.descriptorId == devManufacturerDesc.header.descriptorId);
    for (int i=0; i<devManufacturerDesc.header.length; i++)
    {
        assert(checkDevDesc->devString[i] == devManufacturerDesc.devString[i]);
    }
    offset += IswSolNet::commonHeaderSize + devManufacturerDesc.header.length;

    // Verify Device Friendly Name Property Descriptor
    checkDevDesc = (IswSolNet::iswSolNetPropertyDeviceDesc *)&testEntry->serviceDesc.childBlock[offset];
    assert(checkDevDesc->header.length == devFriendlyNameDesc.header.length);
    assert(checkDevDesc->header.descriptorId == devFriendlyNameDesc.header.descriptorId);
    for (int i=0; i<devFriendlyNameDesc.header.length; i++)
    {
        assert(checkDevDesc->devString[i] == devFriendlyNameDesc.devString[i]);
    }
    offset += IswSolNet::commonHeaderSize + devFriendlyNameDesc.header.length;

    // Defaults
    assert(testEntry->receiveCallbackFn == nullptr);
    assert(testEntry->pollDataCallbackFn == nullptr);
    // Initialize the registered peers
    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
        assert(testEntry->registeredPeers[peerIndex].autonomy == 0);
        assert(testEntry->registeredPeers[peerIndex].flowState == 0);
        assert(testEntry->registeredPeers[peerIndex].sendRegRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].regRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].deregRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse == false);
        assert(testEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendGetStatusResponse == false);
        assert(testEntry->registeredPeers[peerIndex].getStatusResponseSeqNo == 0);
    }   

    // *****************************************
    // Cleanup
    iswSolNet->RemoveOneService(serviceEntry.dataflowId, true);
    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == false);
    // *****************************************

    // *****************************************
    // Reset buffers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceDesc, 0, sizeof(IswSolNet::iswSolNetServiceDesc));
    memset(childblock, 0, childblockLength);
    childblockLength = 0;
    // *****************************************

    // *****************************************
    // Control Service - no Property Descriptors
    // *****************************************
    IswSolNet::iswSolNetServiceControlDesc serviceControlDesc;
    serviceControlDesc.header.length = IswSolNet::selectorSize + sizeof(serviceControlDesc.protocolId) + sizeof(serviceControlDesc.reserved);
    serviceControlDesc.header.descriptorId = IswSolNet::Control;
    serviceControlDesc.protocolId = 0x0000;
    serviceControlDesc.reserved = 0x0000;
    serviceControlDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceControlDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceControlDesc, &serviceControlDesc, sizeof(IswSolNet::iswSolNetServiceControlDesc));

    serviceEntry.dataflowId = 1;
    serviceEntry.endpointId = 1;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = 0; // unicast

    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);

    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == true);

    testEntry = &iswSolNet->iswSolNetServices[serviceEntry.dataflowId];
    assert(testEntry->dataflowId == serviceEntry.dataflowId);
    assert(testEntry->endpointId == serviceEntry.endpointId);
    assert(testEntry->dataPolicies == serviceEntry.dataPolicies);
    assert(testEntry->endpointDistribution == serviceEntry.endpointDistribution);
    assert(testEntry->serviceControlDesc.header.length == serviceEntry.serviceControlDesc.header.length);
    assert(testEntry->serviceControlDesc.header.descriptorId == serviceEntry.serviceControlDesc.header.descriptorId);
    assert(testEntry->serviceControlDesc.protocolId == serviceEntry.serviceControlDesc.protocolId);
    assert(testEntry->serviceControlDesc.reserved == 0x0000);
    assert(testEntry->serviceControlDesc.childBlock == nullptr);
    // Defaults
    assert(testEntry->receiveCallbackFn == nullptr);
    assert(testEntry->pollDataCallbackFn == nullptr);
    // Initialize the registered peers
    for (uint8_t peerIndex=0; peerIndex<MAX_PEERS; peerIndex++)
    {
        assert(testEntry->registeredPeers[peerIndex].peerRegistered == false);
        assert(testEntry->registeredPeers[peerIndex].status == IswSolNet::RegStatusAvailableToRegister);
        assert(testEntry->registeredPeers[peerIndex].autonomy == 0);
        assert(testEntry->registeredPeers[peerIndex].flowState == 0);
        assert(testEntry->registeredPeers[peerIndex].sendRegRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].regRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse == false);
        assert(testEntry->registeredPeers[peerIndex].deregRequestResponseSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse == false);
        assert(testEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo == 0);
        assert(testEntry->registeredPeers[peerIndex].sendGetStatusResponse == false);
        assert(testEntry->registeredPeers[peerIndex].getStatusResponseSeqNo == 0);
    }

    // *****************************************
    // Cleanup
    iswSolNet->RemoveOneService(serviceEntry.dataflowId, true);
    assert(iswSolNet->iswSolNetServices[serviceEntry.dataflowId].inUse == false);
    // *****************************************

    // *****************************************
    // Reset buffers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceControlDesc, 0, sizeof(IswSolNet::iswSolNetServiceControlDesc));
    memset(childblock, 0, childblockLength);
    childblockLength = 0;
    // *****************************************

    // *****************************************
    // Add to Peer Services
    // Video - no Property Descriptors
    // *****************************************
    uint8_t peerIndex = 1;
    serviceDesc.header.length = sizeof(serviceDesc.serviceSelector);
    serviceDesc.header.descriptorId = IswSolNet::Video;
    serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceDesc.childBlock = nullptr;
    memcpy(&serviceEntry.serviceDesc, &serviceDesc, sizeof(IswSolNet::iswSolNetServiceDesc));

    serviceEntry.dataflowId = 1;
    serviceEntry.endpointId = 1;
    serviceEntry.dataPolicies = 0; // no ack, no checksum
    serviceEntry.endpointDistribution = 0; // unicast
    iswSolNet->AddSolNetService(peerIndex, &serviceEntry);

    assert(iswSolNet->iswSolNetPeerServices[peerIndex].inUse == true);

    IswSolNet::iswSolNetPeerServicesEntry *testPeerEntry = &iswSolNet->iswSolNetPeerServices[peerIndex];
    assert(testPeerEntry->sendBrowse == false);
    assert(testPeerEntry->sendAdvertise == false);
    assert(testPeerEntry->sendRevokeRegistrationResponse == false);
    assert(testPeerEntry->revokeRegRequestResponseSeqNo == 0);
    assert(testPeerEntry->sendRevokeNetworkAssociationResponse == false);
    assert(testPeerEntry->revokeNetworkAssociationResponseSeqNo == 0);
    assert(testPeerEntry->services[serviceEntry.dataflowId].inUse = true);
    assert(testPeerEntry->services[serviceEntry.dataflowId].dataflowId == serviceEntry.dataflowId);
    assert(testPeerEntry->services[serviceEntry.dataflowId].endpointId == serviceEntry.endpointId);
    assert(testPeerEntry->services[serviceEntry.dataflowId].dataPolicies == serviceEntry.dataPolicies);
    assert(testPeerEntry->services[serviceEntry.dataflowId].endpointDistribution == serviceEntry.endpointDistribution);
    assert(testPeerEntry->services[serviceEntry.dataflowId].serviceDesc.header.length == serviceEntry.serviceDesc.header.length);
    assert(testPeerEntry->services[serviceEntry.dataflowId].serviceDesc.header.descriptorId == serviceEntry.serviceDesc.header.descriptorId);
    assert(testPeerEntry->services[serviceEntry.dataflowId].serviceDesc.childBlock == nullptr);

    // Defaults
    assert(testPeerEntry->services[serviceEntry.dataflowId].status == IswSolNet::RegStatusAvailableToRegister);
    assert(testPeerEntry->services[serviceEntry.dataflowId].ImRegistered == false);
    assert(testPeerEntry->services[serviceEntry.dataflowId].receiveCallbackFn == nullptr);

    // *****************************************
    // Cleanup
    iswSolNet->RemoveAllServicesForOnePeer(peerIndex, true);
    assert(iswSolNet->iswSolNetPeerServices[peerIndex].inUse == false);
    // *****************************************

    // *****************************************
    // Reset buffers
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));
    memset(&serviceControlDesc, 0, sizeof(IswSolNet::iswSolNetServiceControlDesc));
    memset(childblock, 0, childblockLength);
    childblockLength = 0;
    // *****************************************
}

void TestSolNet::cleanup()
{

}

void TestSolNet::cleanupTestCase()
{
    // Wait for all the tests to finish
    sleep(1);

    iswInterfaceInit->ExitIswInterfaceArray();

    // deletes iswInterface's too
    delete(iswInterfaceInit);

    // Calls deconstructor which closes log file
    delete(theLogger);
}

QTEST_APPLESS_MAIN(TestSolNet)
