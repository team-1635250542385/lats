//###############################################
// Filename: Test_SolNet.h
// Description: Class that provides methods
//              to test IswInterface class
//###############################################
#ifndef TEST_SOLNET_H
#define TEST_SOLNET_H

#include <QtTest/QTest>
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../IswInterface/IswInterfaceInit.h"
#include "../../IswInterface/IswInterface.h"
#include <stdio.h>
#include <array>

class TestSolNet : public QObject
{
    Q_OBJECT

public:
    TestSolNet();
    ~TestSolNet();

private slots:    
    // These are built into Qt:
    // called before the first test function
    void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    void cleanupTestCase();

    void TestSolNetInit();
    void TestSetupSolNetMessagePacket();
    void TestSetupSolNetDataPacket();
    void Test_VerifySolNet_GeneralPropertyDescriptors();
    void Test_VerifySolNet_GeneralPropDesc_Endpoint_Unicast();
    void Test_VerifySolNet_GeneralPropDesc_Endpoint_Broadcast();
    void Test_VerifySolNet_GeneralPropDesc_Endpoint_Multicast();
    void TestSolNet_VideoDescriptors();
    void TestSolNet_RtaDescriptors();
    void TestSolNet_MotionSenseDescriptors();
    void TestSolNet_SimpleUIDescriptors();
    void TestSolNet_StatusDescriptors();
    void TestSolNetDescriptors_OtherTests();
    void TestSolNet_LrfDescriptors();
    void TestSolNet_LrfControlDescriptors();
//    void TestSolNet_StatusDescriptors();

private:
    Logger *theLogger;
    DBLogger *dblogger;
    IswInterfaceInit *iswInterfaceInit;
    std::array<IswInterface *, 21> *iswInterfaceArray;
    IswInterface *iswInterface = nullptr;
    UsbInterface *usbInterface = nullptr;
    IswFirmware *iswFirmware = nullptr;
    IswStream *iswStream = nullptr;
    IswSystem *iswSystem = nullptr;
    IswIdentity *iswIdentity = nullptr;
    IswSecurity *iswSecurity = nullptr;
    IswAssociation *iswAssociation = nullptr;
    IswLink *iswLink = nullptr;
    IswMetrics *iswMetrics = nullptr;
    IswProduct *iswProduct = nullptr;
    IswSolNet *iswSolNet = nullptr;
};

#endif
