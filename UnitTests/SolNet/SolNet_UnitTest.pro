QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += TESTING

LIBS += -L/usr/lib64 -lusb-1.0 -lz -lftdi1

SOURCES +=  \
    ../../Logger/Logger.cpp \
    ../../Logger/DBLogger.cpp \
    ../../Logger/DBLogRecord.cpp \
    ../../UsbInterface/UsbInterface.cpp \
    ../../SerialInterface/SerialInterface.cpp \
    ../../UsbInterface/UsbInterfaceInit.cpp \
    ../../OnBoard/OnBoard.cpp \
    ../../IswInterface/IswFirmware.cpp \
    ../../IswInterface/IswSystem.cpp \
    ../../IswInterface/IswIdentity.cpp \
    ../../IswInterface/IswSecurity.cpp \
    ../../IswInterface/IswAssociation.cpp \
    ../../IswInterface/IswStream.cpp \
    ../../IswInterface/IswLink.cpp \
    ../../IswInterface/IswMetrics.cpp \
    ../../IswInterface/IswProduct.cpp \
    ../../IswInterface/IswSolNet.cpp \
    ../../IswInterface/IswInterface.cpp \
    ../../IswInterface/IswInterfaceInit.cpp \
    Test_SolNet.cpp

HEADERS += \
    ../../Logger/Logger.h \
    ../../Logger/DBLogger.h \
    ../../Logger/DBLogRecord.h \
    ../../IswInterface/Interface.h \
    ../../UsbInterface/UsbInterface.h \
    ../../SerialInterface/SerialInterface.h \
    ../../UsbInterface/UsbInterfaceInit.h \
    ../../OnBoard/OnBoard.h \
    ../../IswInterface/IswFirmware.h \
    ../../IswInterface/IswSystem.h \
    ../../IswInterface/IswIdentity.h \
    ../../IswInterface/IswSecurity.h \
    ../../IswInterface/IswAssociation.h \
    ../../IswInterface/IswStream.h \
    ../../IswInterface/IswLink.h \
    ../../IswInterface/IswMetrics.h \
    ../../IswInterface/IswProduct.h \
    ../../IswInterface/IswSolNet.h \
    ../../IswInterface/IswInterface.h \
    ../../IswInterface/IswInterfaceInit.h \
    Test_SolNet.h

