//###############################################
// Filename: Test_IswInterface.h
// Description: Class that provides methods
//              to test IswInterface class
//###############################################
#ifndef Test_IswInterface_H
#define Test_IswInterface_H

#include <QtTest/QTest>
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../IswInterface/IswInterfaceInit.h"
#include "../../IswInterface/IswInterface.h"
#include <stdio.h>
#include <array>

class TestIswInterface : public QObject
{
    Q_OBJECT

public:
    TestIswInterface();
    ~TestIswInterface();

private slots:    
    // These are built into Qt:
    // called before the first test function
    void initTestCase();

    // called before every test function
    void init();

    // called after every test function
    void cleanup();

    // called after the last test function
    void cleanupTestCase();

    void TestBigToLittleEndian();
    void TestAddSwimHeader();
    void TestRemoveSwimHeader();
    void TestFirmwareMsgs();
    void TestIdentity();
    void TestSecurity();
    void TestAssociation();
    void TestSystem();
    void TestMetrics();
    void TestLink();
    void TestLoadImage();
    void TestProduct();
    void TestStreams();

private:
    Logger *theLogger;
    DBLogger *dblogger;
    Logger *throughputTestLoggerPass;
    Logger *throughputResultsLoggerPass;
    Logger *latencyLogger;
    IswInterfaceInit *iswInterfaceInit;
    std::array<IswInterface *, 21> *iswInterfaceArray;
    IswInterface *iswInterface     = nullptr;
    UsbInterface *usbInterface     = nullptr;
    OnBoard *iswOnBoard            = nullptr;
    IswFirmware *iswFirmware       = nullptr;
    IswStream *iswStream           = nullptr;
    IswSystem *iswSystem           = nullptr;
    IswIdentity *iswIdentity       = nullptr;
    IswSecurity *iswSecurity       = nullptr;
    IswAssociation *iswAssociation = nullptr;
    IswLink *iswLink               = nullptr;
    IswMetrics *iswMetrics         = nullptr;
    IswProduct *iswProduct         = nullptr;
};

#endif
