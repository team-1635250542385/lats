//###############################################
// Filename: Test_IswInterface.cpp
// Description: Class that provides methods
//              to test IswInterface class
//###############################################
#include "Test_IswInterface.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <zlib.h>

//! Constructor
TestIswInterface::TestIswInterface()
{

}

//! Destructor
TestIswInterface::~TestIswInterface()
{

}

//! Initialize Test Case Function
void TestIswInterface::initTestCase()
{
    //! This is called before other tests are run
    std::string testFilename    = "unitTestIsw";
    uint16_t debugLevel         = 0x0000;
    theLogger                   = new Logger(testFilename, debugLevel);
    assert(theLogger);
    dblogger                    = new DBLogger(testFilename, "1");
    assert(dblogger);
    throughputTestLoggerPass    = new Logger(testFilename, debugLevel);
    assert(throughputTestLoggerPass);
    throughputResultsLoggerPass = new Logger(testFilename, debugLevel);
    assert(throughputResultsLoggerPass);
    latencyLogger               = new Logger(testFilename, debugLevel);
    assert(latencyLogger);

    iswInterfaceInit  = new IswInterfaceInit(IswInterface::USB, theLogger, throughputTestLoggerPass, throughputResultsLoggerPass, dblogger, latencyLogger);

    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();
    assert(*iswInterfaceArray->begin() != nullptr);

    //! Just get the first one for this test
    iswInterface = *iswInterfaceArray->begin();

    usbInterface = iswInterface->GetUsbInterface();
    assert(usbInterface != nullptr);
    assert(usbInterface->IsDeviceOpen() == true);

    if ( usbInterface->GetProductId() == UsbInterfaceInit::ALEREON_USB_BOARD_PRODUCT_ID )
    {
        //! Combat board
        assert(usbInterface->GetMaxPacketSizeIn() == 512);
        assert(usbInterface->GetMaxPacketSizeOut() == 512);
    }
    else if ( usbInterface->GetProductId() == UsbInterfaceInit::ALEREON_PARALLEL_BOARD_PRODUCT_ID )
    {
        //! Commander Board
        assert(usbInterface->GetMaxPacketSizeIn() == 1024);
        assert(usbInterface->GetMaxPacketSizeOut() == 1024);
    }

    //! Get the friend classes
    iswOnBoard     = iswInterface->GetIswOnBoard();
    iswFirmware    = iswInterface->GetIswFirmware();
    iswStream      = iswInterface->GetIswStream();
    iswSystem      = iswInterface->GetIswSystem();
    iswIdentity    = iswInterface->GetIswIdentity();
    iswSecurity    = iswInterface->GetIswSecurity();
    iswAssociation = iswInterface->GetIswAssociation();
    iswLink        = iswInterface->GetIswLink();
    iswMetrics     = iswInterface->GetIswMetrics();
    iswProduct     = iswInterface->GetIswProduct();

    assert(iswOnBoard != nullptr);
    assert(iswFirmware != nullptr);
    assert(iswStream != nullptr);
    assert(iswSystem != nullptr);
    assert(iswIdentity != nullptr);
    assert(iswSecurity != nullptr);
    assert(iswAssociation != nullptr);
    assert(iswLink != nullptr);
    assert(iswMetrics != nullptr);
    assert(iswProduct != nullptr);

    assert(iswInterface->theLogger == theLogger);
    assert(iswInterface->deviceInterface == usbInterface);
    assert(usbInterface->usbHeaderType == UsbInterface::USB_HEADER_TRU);
    assert(iswInterface->isLittleEndian == true);
    assert(iswInterface->GetIndex() == 0);  // First device has index 0

    //!  Test QoS per endpoint - can be used for Hi/Low queues sending data
    iswInterface->SetQosOnEndpoint(IswInterface::HiNoDiscard, 0);
    assert(iswInterface->iswEndpointSendQueues[0].qOs == IswInterface::HiNoDiscard);

    iswInterface->SetQosOnEndpoint(IswInterface::LowDiscard, 1);
    assert(iswInterface->iswEndpointSendQueues[1].qOs == IswInterface::LowDiscard);

    iswInterface->SetQosOnEndpoint(IswInterface::HiDiscard, 2);
    assert(iswInterface->iswEndpointSendQueues[2].qOs == IswInterface::HiDiscard);

    iswInterface->SetQosOnEndpoint(IswInterface::LowNoDiscard, 3);
    assert(iswInterface->iswEndpointSendQueues[3].qOs == IswInterface::LowNoDiscard);

    //! Test that send queues are empty at startup
    assert(iswInterface->iswEndpointSendQueues[0].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[1].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[2].sendQ.empty() == true);
    assert(iswInterface->iswEndpointSendQueues[3].sendQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[0].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[1].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[2].receiveQ.empty() == true);
    assert(iswInterface->iswEndpointReceiveQueues[3].receiveQ.empty() == true);
}

//! Init Function
void TestIswInterface::init()
{

}

//! Test Big to Little Endian Function
void TestIswInterface::TestBigToLittleEndian()
{
    uint64_t testInput[] = {0x1234123412341234};

    uint16_t type = 16;
    iswInterface->BigToLittleEndian(testInput, type);
    if (iswInterface->isLittleEndian == true)
    {
       assert((uint16_t)*testInput == 0x1234);
    }
    else
    {
       assert((uint16_t)*testInput == 0x4321);
    }

    type = 32;
    iswInterface->BigToLittleEndian(testInput, type);
    if (iswInterface->isLittleEndian == true)
    {
       assert((uint32_t)*testInput == 0x12341234);
    }
    else
    {
       assert((uint32_t)*testInput == 0x43214321);
    }

    type = 64;
    iswInterface->BigToLittleEndian(testInput, type);
    if (iswInterface->isLittleEndian == true)
    {
       assert((uint64_t)*testInput == 0x1234123412341234);
    }
    else
    {
       assert((uint64_t)*testInput == 0x4321432143214321);
    }
}

//! Test Add SWIM Header Function
void TestIswInterface::TestAddSwimHeader()
{
    //! Use IswFirmware::iswFirmwareCommand for this test
    int dataLength = sizeof(IswInterface::SWIM_Header) + sizeof(IswInterface::iswRoutingHdr)
                    + sizeof(IswFirmware::iswFirmwareCommand);
    uint8_t sendBuf[dataLength];

    //! Add the ISW Routing Header
    IswInterface::iswRoutingHdr *routingHdr = (IswInterface::iswRoutingHdr *)&sendBuf[sizeof(IswInterface::SWIM_Header)];
    routingHdr->srcAddr                     = 1;
    routingHdr->destAddr                    = 1;
    routingHdr->dataLength                  = sizeof(IswFirmware::iswFirmwareCommand);

    //! Add the ISW command
    IswFirmware::iswFirmwareCommand *iswFirmwareCmd = (IswFirmware::iswFirmwareCommand *)&sendBuf[sizeof(IswInterface::SWIM_Header) + sizeof(IswInterface::iswRoutingHdr)];

    iswFirmwareCmd->cmdType    = IswInterface::Firmware;
    iswFirmwareCmd->command    = IswFirmware::GetImageVersion;
    iswInterface->iswCmdLock.lock();
    iswInterface->iswCmdCount++;
    iswFirmwareCmd->cmdContext = iswInterface->iswCmdCount;
    iswInterface->iswCmdLock.unlock();
    iswFirmwareCmd->selector   = 1;

    //! Add the SWIM Header
    uint32_t length = 0;
    assert(usbInterface->usbHeaderType == UsbInterface::USB_HEADER_TRU);
    length = iswInterface->AddSwimHeader(sendBuf, routingHdr, (IswInterface::iswCommand *)iswFirmwareCmd);
    assert(length == (sizeof(IswInterface::SWIM_Header) +
                      sizeof(IswInterface::iswRoutingHdr) +
                      sizeof(IswFirmware::iswFirmwareCommand)));

    IswInterface::SWIM_Header *swimHeader = (IswInterface::SWIM_Header *)(sendBuf);
    assert(swimHeader->flags == 0);
    assert(swimHeader->streamNumber == 0);
    assert(swimHeader->peerIndex == 0x3F);
    assert(swimHeader->transferSize == (sizeof(IswInterface::iswRoutingHdr) + sizeof(IswFirmware::iswFirmwareCommand)));
}

//! Test Remove SWIM Header Function
void TestIswInterface::TestRemoveSwimHeader()
{
    int dataLength = sizeof(IswInterface::SWIM_Header) + sizeof(IswInterface::iswRoutingHdr) + sizeof(IswInterface::iswCommand);
    uint8_t receivBuf[dataLength];
    memset(receivBuf, 0, dataLength);

    //! Add the ISW Routing Header use IswFirmware::iswFirmwareCommand for this test
    IswInterface::iswRoutingHdr *routingHdr = (IswInterface::iswRoutingHdr *)&receivBuf[sizeof(IswInterface::SWIM_Header)];
    routingHdr->srcAddr                     = 1;
    routingHdr->destAddr                    = 1;
    routingHdr->dataLength                  = sizeof(IswFirmware::iswFirmwareCommand);

    //! Add the ISW command
    IswFirmware::iswFirmwareCommand *iswFirmwareCmd = (IswFirmware::iswFirmwareCommand *)&receivBuf[sizeof(IswInterface::SWIM_Header) + sizeof(IswInterface::iswRoutingHdr)];

    iswFirmwareCmd->cmdType    = IswInterface::Firmware;
    iswFirmwareCmd->command    = IswFirmware::GetImageVersion;
    iswInterface->iswCmdLock.lock();
    iswInterface->iswCmdCount++;
    iswInterface->iswCmdLock.unlock();
    iswFirmwareCmd->cmdContext = iswInterface->iswCmdCount;
    iswFirmwareCmd->selector   = 1;

    //! Add the TWIP Header
    uint32_t length = 0;
    assert(usbInterface->usbHeaderType == UsbInterface::USB_HEADER_TRU);

    length = iswInterface->AddSwimHeader(receivBuf, routingHdr, (IswInterface::iswCommand *)iswFirmwareCmd);
    assert(length == (sizeof(IswInterface::SWIM_Header) +
                      sizeof(IswInterface::iswRoutingHdr) +
                      sizeof(IswFirmware::iswFirmwareCommand)));

    IswInterface::SWIM_Header *swimHeader;
    IswInterface::iswEvent *event;
    IswInterface::iswRoutingHdr *routHdr;
    uint8_t *data = nullptr;
    iswInterface->RemoveSwimHeader(receivBuf, &swimHeader, &routHdr, &event, &data);
    assert(swimHeader->flags == 0);
    assert(swimHeader->streamNumber == 0);
    assert(swimHeader->peerIndex == 0x3F);
    assert(swimHeader->transferSize == (sizeof(IswInterface::iswRoutingHdr) + sizeof(IswFirmware::iswFirmwareCommand)));

    assert(routHdr->srcAddr == 1);
    assert(routHdr->destAddr == 1);
    assert(routHdr->dataLength == sizeof(IswFirmware::iswFirmwareCommand));

    assert(event->eventType == iswFirmwareCmd->cmdType);
    assert(event->event == iswFirmwareCmd->command);
    assert(event->resultCode == 0);
}

//! Test Firmware Messages Function
void TestIswInterface::TestFirmwareMsgs()
{
    std::string activeFirmwareVersion = "0";
    std::string standbyFirmwareVersion = "0";
    assert(activeFirmwareVersion == iswFirmware->GetActiveFirmwareVersion());
    assert(standbyFirmwareVersion == iswFirmware->GetStandbyFirmwareVersion());

    IswInterface::iswRoutingHdr routingHdr;
    IswFirmware::iswFirmwareCommand iswFirmwareCmd;

    //! Test Get Firmware Version command
    uint8_t selector = 1;
    iswFirmware->SetupGetFirmwareVersionCmd(&iswFirmwareCmd, selector, (void *)&routingHdr);
    assert(iswFirmwareCmd.cmdType == IswInterface::Firmware);
    assert(iswFirmwareCmd.command == IswFirmware::GetImageVersion);
    assert(iswFirmwareCmd.selector == selector);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswFirmware::iswFirmwareCommand));

    //! Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Run Image command
    selector = 0;
    iswFirmware->SetupRunImageCmd(&iswFirmwareCmd, (void *)&routingHdr);
    assert(iswFirmwareCmd.cmdType == IswInterface::Firmware);
    assert(iswFirmwareCmd.command == IswFirmware::RunImage);
    assert(iswFirmwareCmd.selector == selector);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswFirmware::iswFirmwareCommand));
}

//! Test ISW Identity Messages Function
void TestIswInterface::TestIdentity()
{
    //! Test Get MAC Address
    std::string macAddress = "12:34:56:78:9A:BC";
    IswIdentity::iswIdentityCommand iswIdentityCmd;
    IswInterface::iswRoutingHdr routingHdr;

    iswIdentity->SetupGetMacAddressCmd(&iswIdentityCmd, &routingHdr);
    assert(iswIdentityCmd.cmdType == IswInterface::Identity);
    assert(iswIdentityCmd.command == IswIdentity::GetMACAddress);
    assert(macAddress == iswIdentity->GetMacAddressStr());
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswIdentity::iswIdentityCommand));

    memset(&iswIdentityCmd, 0, sizeof(IswIdentity::iswIdentityCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Get Device Type
    uint16_t devType = 0;
    iswIdentity->SetupGetDeviceTypeCmd(&iswIdentityCmd, &routingHdr);
    assert(devType == iswIdentity->GetIswDeviceType());
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswIdentity::iswIdentityCommand));

    //! Clear structs
    memset(&iswIdentityCmd, 0, sizeof(IswIdentity::iswIdentityCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Set Coordinator command
    IswIdentity::iswSetCoordinatorCommand iswSetCoordCmd;
    uint8_t coordinatorCapable = 1;
    uint8_t coordinatorPrecedence = 0;
    uint8_t persist = 1;
    iswIdentity->SetupSetCoordinatorCmd(&iswSetCoordCmd, coordinatorCapable,
                                       coordinatorPrecedence, persist, &routingHdr);

    assert(iswIdentity->iswCoordinatorCapable == coordinatorCapable);
    assert(iswIdentity->iswCoordinatorPrecedence == coordinatorPrecedence);
    assert(iswIdentity->iswPersist == persist);
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswIdentity::iswSetCoordinatorCommand));

    memset(&iswIdentityCmd, 0, sizeof(IswIdentity::iswIdentityCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Get Coordinator command
    iswIdentity->SetupGetCoordinatorCmd(&iswIdentityCmd, &routingHdr);

    assert(iswIdentity->iswCoordinatorCapable == coordinatorCapable);
    assert(iswIdentity->iswCoordinatorPrecedence == coordinatorPrecedence);
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswIdentity::iswIdentityCommand));
}

//! Test ISW Security Messages Function
void TestIswInterface::TestSecurity()
{
    IswInterface::iswRoutingHdr routingHdr;

    //! Verify the password
    assert(iswSecurity->password[0] == 0x55);
    assert(iswSecurity->password[1] == 0x73);
    assert(iswSecurity->password[2] == 0x65);
    assert(iswSecurity->password[3] == 0x72);

    //! Test SetupAuthenticateRoleCmd
    IswSecurity::iswAuthenticateRoleCommand iswAuthenticateRoleCmd;

    iswSecurity->SetupAuthenticateRoleCmd(&iswAuthenticateRoleCmd, &routingHdr);
    assert(iswAuthenticateRoleCmd.cmdType == IswInterface::Security);
    assert(iswAuthenticateRoleCmd.command = IswSecurity::AuthenticateRole);
    assert(iswAuthenticateRoleCmd.role == 1);
    assert(iswAuthenticateRoleCmd.password[0] == iswSecurity->password[0]);
    assert(iswAuthenticateRoleCmd.password[1] == iswSecurity->password[1]);
    assert(iswAuthenticateRoleCmd.password[2] == iswSecurity->password[2]);
    assert(iswAuthenticateRoleCmd.password[3] == iswSecurity->password[3]);
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSecurity::iswAuthenticateRoleCommand));

    //! Clear routing header
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
            ;
    //! Test SetupCryptoSessionStartCmd
    IswSecurity::iswCryptoStartCommand iswCryptoStartCmd;
    iswSecurity->SetupCryptoSessionStartCmd(&iswCryptoStartCmd, &routingHdr);
    assert(iswCryptoStartCmd.cmdType == IswInterface::Security);
    assert(iswCryptoStartCmd.command = IswSecurity::CryptoSessionStart);
    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSecurity::iswCryptoStartCommand));
}

//! Test ISW Association Messages Function
void TestIswInterface::TestAssociation()
{
    IswInterface::iswRoutingHdr routingHdr;

    //! Test SetupStartAssociationCmd
    IswAssociation::iswStartAssociationCommand iswStartAssociationCmd;
    uint8_t type    = 1;
    uint8_t timeout = 30;

    iswAssociation->SetupStartAssociationCmd(&iswStartAssociationCmd, type, timeout, (void *)&routingHdr);
    assert(iswStartAssociationCmd.cmdType == IswInterface::Association);
    assert(iswStartAssociationCmd.command == IswAssociation::StartAssociation);
    assert(iswStartAssociationCmd.type == type);
    assert(iswStartAssociationCmd.timeout == timeout);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswAssociation::iswStartAssociationCommand));

    //! Clear routing header
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test SetupClearAssociationCmd
    IswAssociation::iswClearAssociationCommand iswClearAssociationCmd;

    iswAssociation->SetupClearAssociationCmd(&iswClearAssociationCmd, (void *)&routingHdr);
    assert(iswClearAssociationCmd.cmdType == IswInterface::Association);
    assert(iswClearAssociationCmd.command == IswAssociation::ClearAssociation);

}

//! Test ISW System Messages Function
void TestIswInterface::TestSystem()
{
    IswInterface::iswRoutingHdr routingHdr;
    IswSystem::iswSystemCommand iswSystemCmd;

    //! Test Reset Device command
    iswSystem->SetupResetDeviceCmd(&iswSystemCmd, (void *)&routingHdr);
    assert(iswSystemCmd.cmdType == IswInterface::System);
    assert(iswSystemCmd.command == IswSystem::ResetDevice);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSystemCommand));

    //! Clear structs
    memset(&iswSystemCmd, 0, sizeof(IswSystem::iswSystemCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));


    //! Test IdleReset command
    iswSystem->SetupIdleRadioCmd(&iswSystemCmd, (void *)&routingHdr);
    assert(iswSystemCmd.cmdType == IswInterface::System);
    assert(iswSystemCmd.command == IswSystem::IdleRadio);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSystemCommand));

    //! Clear structs
    memset(&iswSystemCmd, 0, sizeof(IswSystem::iswSystemCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test RadioReset command
    iswSystem->SetupResetRadioCmd(&iswSystemCmd, (void *)&routingHdr);
    assert(iswSystemCmd.cmdType == IswInterface::System);
    assert(iswSystemCmd.command == IswSystem::RadioReset);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSystemCommand));

    //! Clear structs
    memset(&iswSystemCmd, 0, sizeof(IswSystem::iswSystemCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Set Indications command
    IswSystem::iswSetIndicationsCommand iswSetIndicationsCmd;
    uint8_t wireInterfaceEnable    = 1;
    uint8_t associationIndEnable   = 1;
    uint8_t connectionIndEnable    = 1;
    uint8_t resetIndEnable         = 1;
    uint8_t streamsStatusIndEnable = 1;
    uint8_t multicastIndEnable     = 1;
    uint8_t hibernationIndEnable   = 1;
    iswSystem->SetupSetIndicationsCmd(&iswSetIndicationsCmd, wireInterfaceEnable, associationIndEnable,
                                      connectionIndEnable, resetIndEnable, multicastIndEnable,
                                      streamsStatusIndEnable, hibernationIndEnable, (void *)&routingHdr);

    assert(iswSetIndicationsCmd.cmdType == IswInterface::System);
    assert(iswSetIndicationsCmd.command == IswSystem::IndicationsSet);
    assert(iswSetIndicationsCmd.reserved1 == 0x00);
    assert(iswSetIndicationsCmd.reserved2 == 0x00);
    assert(iswSetIndicationsCmd.wireInterfaceEnable == wireInterfaceEnable);
    assert(iswSetIndicationsCmd.associationIndEnable == associationIndEnable);
    assert(iswSetIndicationsCmd.connectionIndEnable == connectionIndEnable);
    assert(iswSetIndicationsCmd.resetIndEnable == resetIndEnable);
    assert(iswSetIndicationsCmd.multicastIndEnable == multicastIndEnable);
    assert(iswSetIndicationsCmd.streamsStatusIndEnable == streamsStatusIndEnable);
    assert(iswSetIndicationsCmd.hibernationIndEnable == hibernationIndEnable);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSetIndicationsCommand));

    //! Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Factory Default command
    IswSystem::iswSystemCommand iswFactoryDefaultCmd;
    iswSystem->SetupSetFactoryDefaultCmd(&iswFactoryDefaultCmd, (void *)&routingHdr);
    assert(iswFactoryDefaultCmd.cmdType == IswInterface::System);
    assert(iswFactoryDefaultCmd.command == IswSystem::SetFactoryDefault);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSystemCommand));

    //! Clear structs
    memset(&iswSystemCmd, 0, sizeof(IswSystem::iswSystemCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Get Radio Status Command
    IswSystem::iswSystemCommand iswGetRadioStatusCmd;
    iswSystem->SetupGetRadioStatusCmd(&iswGetRadioStatusCmd, (void *)&routingHdr);
    assert(iswGetRadioStatusCmd.cmdType == IswInterface::System);
    assert(iswGetRadioStatusCmd.command == IswSystem::GetRadioStatus);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswSystem::iswSystemCommand));
}

//! Test ISW Metrics Messages Function
void TestIswInterface::TestMetrics()
{
    IswInterface::iswRoutingHdr routingHdr;
    IswMetrics::iswMetricsCommand iswMetricsCmd;

    //! Test Get Wireless Metrics command
    iswMetrics->SetupGetWirelessMetrics(&iswMetricsCmd, (void *)&routingHdr);
    assert(iswMetricsCmd.cmdType == IswInterface::Metrics);
    assert(iswMetricsCmd.command == IswMetrics::GetWirelessMetrics);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswMetrics::iswMetricsCommand));
}

//! Test ISW Link Messages Function
void TestIswInterface::TestLink()
{
    IswInterface::iswRoutingHdr routingHdr;

    //! Test Get Scan Duty Cycle command
    IswLink::IswLinkCommand iswLinkCmd;
    iswLink->SetupGetScanDutyCycleCmd(&iswLinkCmd, (void *)&routingHdr);
    assert(iswLinkCmd.cmdType == IswInterface::Link);
    assert(iswLinkCmd.command == IswLink::GetScanDutyCycle);
    assert(iswLinkCmd.reserved1 == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswLinkCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test Set Scan Duty Cycle command
    IswLink::IswSetScanDutyCycleCommand iswSetScanDutyCycleCmd;
    uint16_t startupScanDuration = 30;
    uint8_t scanDutyCycle        = 4;
    uint8_t persist              = 1;
    iswLink->SetupSetScanDutyCycleCmd(&iswSetScanDutyCycleCmd, startupScanDuration, scanDutyCycle, persist, (void *)&routingHdr);
    assert(iswSetScanDutyCycleCmd.cmdType == IswInterface::Link);
    assert(iswSetScanDutyCycleCmd.command == IswLink::SetScanDutyCycle);
    assert(iswSetScanDutyCycleCmd.reserved1 == 0x00);
    assert(iswSetScanDutyCycleCmd.startupScanDuration == startupScanDuration);
    assert(iswSetScanDutyCycleCmd.scanDutyCycle == scanDutyCycle);
    assert(iswSetScanDutyCycleCmd.persist == persist);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetScanDutyCycleCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Set Max Service Interval command
    IswLink::IswSetMaxServiceIntervalCommand iswSetMaxServiceIntervalCmd;
    uint32_t maxServiceInterval = 4096; // 4096 - 32000
    iswLink->SetupSetMaxServiceIntervalCmd(&iswSetMaxServiceIntervalCmd, maxServiceInterval, (void *)&routingHdr);
    assert(iswSetMaxServiceIntervalCmd.cmdType == IswInterface::Link);
    assert(iswSetMaxServiceIntervalCmd.command == IswLink::SetMaxServiceInterval);
    assert(iswSetMaxServiceIntervalCmd.reserved1 == 0x00);
    assert(iswSetMaxServiceIntervalCmd.maxServiceInterval == maxServiceInterval);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetScanDutyCycleCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswLinkCmd, 0, sizeof(IswLink::IswLinkCommand));

    // Test Reset Max Service Interval command
    iswLink->SetupResetMaxServiceIntervalCmd(&iswLinkCmd, (void *)&routingHdr);
    assert(iswLinkCmd.cmdType == IswInterface::Link);
    assert(iswLinkCmd.command == IswLink::ResetMaxServiceInterval);
    assert(iswLinkCmd.reserved1 == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswLinkCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswLinkCmd, 0, sizeof(IswLink::IswLinkCommand));

    // Test Get Idle Scan Frequency command
//    iswLink->SetupGetIdleScanFrequencyCmd(&iswLinkCmd, (void *)&routingHdr);
//    assert(iswLinkCmd.cmdType == IswInterface::Link);
//    assert(iswLinkCmd.command == IswLink::GetIdleScanFrequency);
//    assert(iswLinkCmd.reserved1 == 0x00);

//    assert(routingHdr.srcAddr == 1);
//    assert(routingHdr.destAddr == 1);
//    assert(routingHdr.dataLength == sizeof(IswLink::IswLinkCommand));

//    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
//    memset(&iswLinkCmd, 0, sizeof(IswLink::IswLinkCommand));

////    // Test Set Idle Scan Frequency command
//    IswLink::IswSetIdleScanFrequencyCommand setIdleScanFreqCmd;
//    uint16_t idleScanFrequency = 60;
//    persist =1;
//    iswLink->SetupSetIdleScanFrequencyCmd(&setIdleScanFreqCmd, idleScanFrequency, persist, (void *)&routingHdr);
//    assert(setIdleScanFreqCmd.cmdType == IswInterface::Link);
//    assert(setIdleScanFreqCmd.command == IswLink::SetIdleScanFrequency);
//    assert(setIdleScanFreqCmd.reserved1 == 0x00);
//    assert(setIdleScanFreqCmd.idleScanFrequency == idleScanFrequency);
//    assert(setIdleScanFreqCmd.persist == persist);

//    assert(routingHdr.srcAddr == 1);
//    assert(routingHdr.destAddr == 1);
//    assert(routingHdr.dataLength == sizeof(IswLink::IswSetIdleScanFrequencyCommand));

//    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
//    memset(&iswLinkCmd, 0, sizeof(IswLink::IswLinkCommand));

    //! Test Set Background Scan Parameters
    uint16_t stableScanFrequency = 60;
    persist                      = 1;
    uint8_t version              = 0x01;
    uint16_t fluxScanFrequency   = 20;
    uint16_t fluxScanDuration    = 300;

    IswLink::IswSetBackgroundScanParametersCommand iswSetBackgroundScanParametersCmd;
    iswLink->SetupSetBackgroundScanParametersCmd(&iswSetBackgroundScanParametersCmd, stableScanFrequency, persist, version, fluxScanFrequency, fluxScanDuration, (void *)&routingHdr);
    assert(iswSetBackgroundScanParametersCmd.cmdType == IswInterface::Link);
    assert(iswSetBackgroundScanParametersCmd.command == IswLink::SetBackgroundScanParameters);
    assert(iswSetBackgroundScanParametersCmd.reserved1 == 0x00);
    assert(iswSetBackgroundScanParametersCmd.stableScanFrequency == stableScanFrequency);
    assert(iswSetBackgroundScanParametersCmd.persist == persist);
    assert(iswSetBackgroundScanParametersCmd.version == version);
    assert(iswSetBackgroundScanParametersCmd.fluxScanFrequency == fluxScanFrequency);
    assert(iswSetBackgroundScanParametersCmd.fluxScanDuration == fluxScanDuration);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetBackgroundScanParametersCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswSetBackgroundScanParametersCmd, 0, sizeof(IswLink::IswSetBackgroundScanParametersCommand));

    //! Test Get Background Scan Parameters
    iswLink->SetupGetBackgroundScanParametersCmd(&iswLinkCmd, (void *)&routingHdr);
    assert(iswLinkCmd.cmdType == IswInterface::Link);
    assert(iswLinkCmd.command == IswLink::GetBackgroundScanParameters);
    assert(iswLinkCmd.reserved1 == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswLinkCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswLinkCmd, 0, sizeof(IswLink::IswLinkCommand));

    //! Antenna Diversity -- Set Antenna Configuration
    uint8_t enable                  = 0x01;
    uint8_t triggerAlgorithm        = 0x00;
    uint8_t triggerRssiThreshold    = 174;
    uint8_t triggerPhyRateThreshold = 0;
    uint8_t switchThresholdAdder    = 1;

    IswLink::IswSetAntennaConfigurationCommand iswSetAntennaConfigurationCmd;
    iswLink->SetupSetAntennaConfigurationCmd(&iswSetAntennaConfigurationCmd, enable, triggerAlgorithm, triggerRssiThreshold, triggerPhyRateThreshold, switchThresholdAdder, (void *)&routingHdr);
    assert(iswSetAntennaConfigurationCmd.cmdType == IswInterface::Link);
    assert(iswSetAntennaConfigurationCmd.command == IswLink::SetAntennaConfiguration);
    assert(iswSetAntennaConfigurationCmd.reserved1 == 0x00);
    assert(iswSetAntennaConfigurationCmd.enable == enable);
    assert(iswSetAntennaConfigurationCmd.triggerAlgorithm == triggerAlgorithm);
    assert(iswSetAntennaConfigurationCmd.triggerRssiThreshold == triggerRssiThreshold);
    assert(iswSetAntennaConfigurationCmd.triggerPhyRateThreshold == triggerPhyRateThreshold);
    assert(iswSetAntennaConfigurationCmd.switchThresholdAdder == switchThresholdAdder);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetAntennaConfigurationCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswSetAntennaConfigurationCmd, 0, sizeof(IswLink::IswSetAntennaConfigurationCommand));

    // Antenna Diversity -- Set Antenna Id

    uint8_t antennaId = 0x01;

    IswLink::IswSetAntennaIdCommand iswSetAntennaIdCmd;
    iswLink->SetupSetAntennaIdCmd(&iswSetAntennaIdCmd, antennaId, (void *)&routingHdr);
    assert(iswSetAntennaIdCmd.cmdType == IswInterface::Link);
    assert(iswSetAntennaIdCmd.command == IswLink::SetAntennaId);
    assert(iswSetAntennaIdCmd.reserved1 == 0x00);
    assert(iswSetAntennaIdCmd.antennaId == antennaId);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetAntennaIdCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswSetAntennaIdCmd, 0, sizeof(IswLink::IswSetAntennaIdCommand));

    // Test Scan Order Biasing Command
    uint8_t bgPrefer = 0x02; // Prefer Bandgroup 1
    uint8_t bgAvoid  = 0x08; // Avoid Bandgroup 3
    persist          = 0; // DO NOT Persist In NVRAM

    IswLink::IswSetBandgroupBiasCommand iswSetBandgroupBiasCmd;
    iswLink->SetupSetBandgroupBiasCmd(&iswSetBandgroupBiasCmd, bgPrefer, bgAvoid, persist, (void *)&routingHdr);
    assert(iswSetBandgroupBiasCmd.cmdType == IswInterface::Link);
    assert(iswSetBandgroupBiasCmd.command == IswLink::SetBandgroupBias);
    assert(iswSetBandgroupBiasCmd.reserved1 == 0x00);
    assert(iswSetBandgroupBiasCmd.bgPrefer == bgPrefer);
    assert(iswSetBandgroupBiasCmd.bgAvoid == bgAvoid);
    assert(iswSetBandgroupBiasCmd.persist == persist);
    assert(iswSetBandgroupBiasCmd.reserved2 == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswLink::IswSetBandgroupBiasCommand));

    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));
    memset(&iswSetBandgroupBiasCmd, 0, sizeof(IswLink::IswSetBandgroupBiasCommand));
}

//! Test ISW Load Firmware Image Function
void TestIswInterface::TestLoadImage(void)
{
    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    uint8_t lastBlock  = 0;
    uint16_t seqNumber = 0;
    uint16_t blockSize = 256;
    uint8_t data[256];

    for (int i = 0; i < 256; i++)
    {
        data[i] = i;
    }

    iswFirmware->SetupLoadImageCmd(&iswCmd.iswLoadImageCmd, lastBlock, seqNumber, blockSize, data, &routingHdr);

    assert(iswCmd.iswLoadImageCmd.cmdType == IswInterface::Firmware);
    assert(iswCmd.iswLoadImageCmd.command == IswFirmware::LoadImage);
    assert(iswCmd.iswLoadImageCmd.reserved1 == 0x00);
    assert(iswCmd.iswLoadImageCmd.selector == 0);
    assert(iswCmd.iswLoadImageCmd.lastBlock == lastBlock);
    assert(iswCmd.iswLoadImageCmd.blockSequence == seqNumber);
    assert(iswCmd.iswLoadImageCmd.reserved2 == 0x0000);
    assert(iswCmd.iswLoadImageCmd.blockSize == blockSize);

    for (int i = 0; i < 256; i++)
    {
        assert(iswCmd.iswLoadImageCmd.data[i] == data[i]);
    }
}

//! Test ISW Product Messages Function
void TestIswInterface::TestProduct()
{
    IswInterface::iswRoutingHdr routingHdr;
    IswProduct::iswProductCommand iswProductCmd;
    IswProduct::iswSetHibernationModeCommand iswSetHibernationModeCmd;
    IswProduct::iswGetHibernationModeEvent iswGetHibernationModeEvent;

    assert(iswProduct->GetIswNetworkId() == 0);

    //! Test Get Network ID command
    iswProduct->SetupGetNetworkIdCmd(&iswProductCmd, (void *)&routingHdr);
    assert(iswProductCmd.cmdType == IswInterface::Product);
    assert(iswProductCmd.command == IswProduct::GetNetworkId);
    assert(iswProductCmd.reserved1 == 0x0);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswProduct::iswProductCommand));

    memset(&iswProductCmd, 0, sizeof(IswProduct::iswProductCommand));
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    //! Test Set Hibernation Command -- Soft Hibernation Mode
    uint8_t hibernationMode     = IswProduct::SOFT_HIBERNATION; // Soft Hibernation Mode
    uint8_t persist             = 0; // Do Not Persist
    uint16_t idleTime           = 15; // Idle Time
    uint8_t softHibernationTime = 16; // Soft Hibernation Time


    iswProduct->SetupSetHibernationModeCmd(&iswSetHibernationModeCmd, hibernationMode, persist,
                                           idleTime, softHibernationTime, &routingHdr);
    assert(iswSetHibernationModeCmd.cmdType == IswInterface::Product);
    assert(iswSetHibernationModeCmd.command == IswProduct::SetHibernation);
    assert(iswSetHibernationModeCmd.reserved1 == 0x0);
    assert(iswSetHibernationModeCmd.hibernationMode == hibernationMode);
    assert(iswSetHibernationModeCmd.persist == persist);
    assert(iswSetHibernationModeCmd.idleTime == idleTime);
    assert(iswSetHibernationModeCmd.softHibernationTime = softHibernationTime);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswProduct::iswSetHibernationModeCommand));
}

void TestIswInterface::TestStreams()
{
    // Test Receiving StatusIndication
    IswInterface::iswEvent event;
    event.event      = IswStream::StreamsStatusIndication;
    event.eventType  = IswInterface::Stream;
    event.resultCode = 0;
    uint8_t *data    = nullptr;

    // Use some interesting numbers for the test
    IswStream::iswEventStreamsStatus streamsStatusInd;
    streamsStatusInd.role        = 1;
    streamsStatusInd.channel     = 13;
    streamsStatusInd.numPeers    = 14;
    streamsStatusInd.peerVersion = 1;
    data = (uint8_t *)&streamsStatusInd;

    iswStream->DeliverIswMessage(event.event, data);

    assert(iswStream->GetIswRole() == streamsStatusInd.role);
    assert(iswStream->GetIswChannel() == streamsStatusInd.channel);

    memset(&event, 0, sizeof(IswInterface::iswEvent));

    // Test Receiving ConnectionIndication
    event.event      = IswStream::ConnectionIndication;
    event.eventType  = IswInterface::Stream;
    event.resultCode = 0;
    data             = nullptr;

    // Use some interesting numbers for the test
    IswStream::iswEventStreamsConnectionInd connectionInd;
    connectionInd.peerIndex  = 1;
    connectionInd.deviceType = 1012;
    connectionInd.linkStatus = 2;
    for (int i = 0; i < 6; i++)
    {
        connectionInd.macAddress[i] = i;
    }
    std::array<IswStream::iswPeerRecord, MAX_PEERS>& iswPeers = iswStream->GetPeerRef();
    iswPeers[connectionInd.peerIndex].recordVersion = 2;

    iswStream->ParseConnectionIndication(&connectionInd);

    assert(iswPeers[connectionInd.peerIndex].iswPeerRecord2.linkStatus == connectionInd.linkStatus);
    assert(iswPeers[connectionInd.peerIndex].iswPeerRecord2.deviceType == connectionInd.deviceType);
    for  (int i = 0; i < 6; i++)
    {
        assert(iswPeers[connectionInd.peerIndex].iswPeerRecord2.macAddress[i] == connectionInd.macAddress[i]);
    }
    assert(iswPeers[connectionInd.peerIndex].inUse == true);
    assert(iswPeers[connectionInd.peerIndex].isConnected == true);

    // Clear structs
    memset(&event, 0, sizeof(IswInterface::iswEvent));

    // Test Receiving MulticastGroupStatusIndication
    event.event      = IswStream::MulticastGroupsStatusIndication;
    event.eventType  = IswInterface::Stream;
    event.resultCode = 0;
    data             = nullptr;

    // Use some interesting numbers for the test
    uint8_t buffer[512];
    IswStream::iswEventMulticastGroupStatus *mcastStatusInd = (IswStream::iswEventMulticastGroupStatus *)buffer;
    mcastStatusInd->numMulticastRecords                     = 1;
    IswStream::iswMulticastGroupRecord *mcastGroupRecord    = (IswStream::iswMulticastGroupRecord *)(buffer + sizeof(IswStream::iswEventMulticastGroupStatus));
    mcastGroupRecord->mcastPeerIndex                        = 0x12;
    mcastGroupRecord->mcastStatus                           = 2;
    mcastGroupRecord->mcastAddress                          = 0x1234;
    mcastGroupRecord->ownerPeerIndex                        = 0;
    mcastGroupRecord->mcastPhyRate                          = 0;
    mcastGroupRecord->txQueueDepth                          = 10;
    data                                                    = buffer;

    iswStream->DeliverIswMessage(event.event, data);

    std::array<IswStream::multicastGroupRecords, MAX_MCAST_RECORDS>& iswMcastGroupRecords = iswStream->GetMulticastGroupRecordsRef();

    bool addedMcastGroup = false;
    for (uint8_t i = 0; i < MAX_MCAST_RECORDS; i++)
    {
        if (( iswMcastGroupRecords[i].inUse ) &&
                (iswMcastGroupRecords[i].iswMcastRecord.mcastAddress == mcastGroupRecord->mcastAddress) )
        {
            assert(iswMcastGroupRecords[i].iswMcastRecord.mcastPeerIndex == mcastGroupRecord->mcastPeerIndex);
            assert(iswMcastGroupRecords[i].iswMcastRecord.mcastStatus == mcastGroupRecord->mcastStatus);
            assert(iswMcastGroupRecords[i].iswMcastRecord.ownerPeerIndex == mcastGroupRecord->ownerPeerIndex);
            assert(iswMcastGroupRecords[i].iswMcastRecord.mcastPhyRate == mcastGroupRecord->mcastPhyRate);
            assert(iswMcastGroupRecords[i].iswMcastRecord.txQueueDepth == mcastGroupRecord->txQueueDepth);
            addedMcastGroup = true;
            break;
        }
    }
    assert(addedMcastGroup == true);

    // Clear structs
    memset(&event, 0, sizeof(IswInterface::iswEvent));

    // Test Receiving MulticastActivityIndication
    event.event      = IswStream::MulticastActivityIndication;
    event.eventType  = IswInterface::Stream;
    event.resultCode = 0;
    data = nullptr;

    // Use some interesting numbers for the test
    IswStream::iswEventMulticastActivity mcastActivityInd;
    mcastActivityInd.mcastPeerIndex = 0x12;
    mcastActivityInd.activityType   = 3;
    mcastActivityInd.mcastAddress   = 0x1234;
    data                            = (uint8_t *)&mcastActivityInd;

    iswStream->DeliverIswMessage(event.event, data);

    bool updatedMcastGroup = false;
    for (uint8_t i = 0; i < MAX_MCAST_RECORDS; i++)
    {
        if ( (iswMcastGroupRecords[i].inUse) &&
             (iswMcastGroupRecords[i].iswMcastRecord.mcastAddress == mcastActivityInd.mcastAddress) )
        {
            assert(iswMcastGroupRecords[i].iswMcastRecord.mcastPeerIndex == mcastActivityInd.mcastPeerIndex);
            assert(iswMcastGroupRecords[i].iswMcastRecord.mcastStatus == mcastActivityInd.activityType);
            updatedMcastGroup = true;
            break;
        }
    }
    assert(updatedMcastGroup == true);

    // Test GetStreamsStatus
    IswStream::iswStreamCommand iswStreamCmd;
    IswInterface::iswRoutingHdr routingHdr;
    iswStream->SetupGetStreamsStatusCmd(&iswStreamCmd, &routingHdr);
    assert(iswStreamCmd.cmdType == IswInterface::Stream);
    assert(iswStreamCmd.command == IswStream::GetStreamsStatus);
    assert(iswStreamCmd.reserved1 == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswStreamCommand));

    // Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test GetExtendedStreamsStatus
    IswStream::iswExtendedStreamCommand iswExtendedStreamCmd;
    uint8_t version = 2;
    iswStream->SetupGetExtendedStreamsStatusCmd(&iswExtendedStreamCmd, version, &routingHdr);

    assert(iswExtendedStreamCmd.cmdType == IswInterface::Stream);
    assert(iswExtendedStreamCmd.command == IswStream::GetExtendedStreamsStatus);
    assert(iswExtendedStreamCmd.reserved1 == 0x00);
    assert(iswExtendedStreamCmd.version == version);
    assert(iswExtendedStreamCmd.reserved2[0] == 0x00);
    assert(iswExtendedStreamCmd.reserved2[1] == 0x00);
    assert(iswExtendedStreamCmd.reserved2[2] == 0x00);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswExtendedStreamCommand));

    // Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test SetStreamsSpec
    IswStream::iswSetStreamSpecCommand iswSetStreamsSpecCmd;
    uint8_t peerIndex = 0;

    // Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the sub-stream is High Priority
    uint8_t txPriority = 1;

    // Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the ISW-MAC
    // may automatically discard the sub-stream data in the event of network congestions
    uint8_t txDiscard = 2;

    // Max duration (in microseconds) between transmit opportunities
    uint32_t maxTxLatency = 10;
    iswStream->SetupSetStreamsSpecCmd(&iswSetStreamsSpecCmd, peerIndex, txPriority, txDiscard, maxTxLatency, &routingHdr);

    assert(iswSetStreamsSpecCmd.cmdType == IswInterface::Stream);
    assert(iswSetStreamsSpecCmd.command == IswStream::SetStreamsSpec);
    assert(iswSetStreamsSpecCmd.reserved1 == 0x00);
    assert(iswSetStreamsSpecCmd.peerIndex == peerIndex);
    assert(iswSetStreamsSpecCmd.subStreamTxPriority == txPriority);
    assert(iswSetStreamsSpecCmd.subStreamTxDiscardable == txDiscard);
    assert(iswSetStreamsSpecCmd.reserved2 == 0x00);
    assert(iswSetStreamsSpecCmd.maxTxLatency == maxTxLatency);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswSetStreamSpecCommand));

    // Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test GetStreamsSpec
    IswStream::iswGetStreamSpecCommand iswGetStreamsSpecCmd;
    peerIndex = 1;
    iswStream->SetupGetStreamsSpecCmd(&iswGetStreamsSpecCmd, peerIndex, &routingHdr);

    assert(iswGetStreamsSpecCmd.cmdType == IswInterface::Stream);
    assert(iswGetStreamsSpecCmd.command == IswStream::GetStreamsSpec);
    assert(iswGetStreamsSpecCmd.reserved1 == 0x00);
    assert(iswGetStreamsSpecCmd.peerIndex == peerIndex);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswGetStreamSpecCommand));

    // Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test GetPeerFirmwareVersion Cmd
    IswStream::iswGetPeerFirmwareVersionCommand getPeerFirmwareVersionCmd;
    peerIndex = 1;
    iswStream->SetupGetPeerFirmwareVersionCmd(&getPeerFirmwareVersionCmd, peerIndex, &routingHdr);

    assert(getPeerFirmwareVersionCmd.cmdType == IswInterface::Stream);
    assert(getPeerFirmwareVersionCmd.command == IswStream::GetPeerFirmwareVersion);
    assert(getPeerFirmwareVersionCmd.reserved1 == 0x00);
    assert(getPeerFirmwareVersionCmd.peerIndex == peerIndex);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswGetPeerFirmwareVersionCommand));

    // Clear structs
    memset(&routingHdr, 0, sizeof(IswInterface::iswRoutingHdr));

    // Test StartMulticastGroup Cmd
    IswStream::iswStartMulticastGroupCommand startMulticastGroup;
    uint8_t phyRate = 0;
    iswStream->SetupStartMulticastGroupCmd(&startMulticastGroup, phyRate, &routingHdr);

    assert(startMulticastGroup.cmdType == IswInterface::Stream);
    assert(startMulticastGroup.command == IswStream::StartMulticastGroup);
    assert(startMulticastGroup.reserved1 == 0x00);
    assert(startMulticastGroup.phyRate == phyRate);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswStartMulticastGroupCommand));

    // Test JoinMulticastGroup Cmd
    IswStream::iswJoinMulticastGroupCommand joinMulticastGroupCmd;
    uint16_t mcastAddress = 65534;  // 65535 is highest for uint16_t
    phyRate               = 1;
    iswStream->SetupJoinMulticastGroupCmd(&joinMulticastGroupCmd, mcastAddress, phyRate, &routingHdr);

    assert(joinMulticastGroupCmd.cmdType == IswInterface::Stream);
    assert(joinMulticastGroupCmd.command == IswStream::JoinMulticastGroup);
    assert(joinMulticastGroupCmd.reserved1 == 0x00);
    assert(joinMulticastGroupCmd.mcastAddress == mcastAddress);
    assert(joinMulticastGroupCmd.mcastPhyRate == phyRate);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswJoinMulticastGroupCommand));

    // Test LeaveMulticastGroup Cmd
    IswStream::iswLeaveMulticastGroupCommand leaveMulticastGroupCmd;
    uint16_t mcastPeerIndex = 64;
    iswStream->SetupLeaveMulticastGroupCmd(&leaveMulticastGroupCmd, mcastPeerIndex, &routingHdr);

    assert(leaveMulticastGroupCmd.cmdType == IswInterface::Stream);
    assert(leaveMulticastGroupCmd.command == IswStream::LeaveMulticastGroup);
    assert(leaveMulticastGroupCmd.reserved1 == 0x00);
    assert(leaveMulticastGroupCmd.mcastPeerIndex == mcastPeerIndex);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswLeaveMulticastGroupCommand));

    // Test ModifyMulticastGroup Cmd
    IswStream::iswModifyMulticastGroupCommand modifyMulticastGroupCmd;
    mcastPeerIndex = 64;
    phyRate        = 1;
    iswStream->SetupModifyMulticastGroupCmd(&modifyMulticastGroupCmd, mcastPeerIndex, phyRate, &routingHdr);

    assert(modifyMulticastGroupCmd.cmdType == IswInterface::Stream);
    assert(modifyMulticastGroupCmd.command == IswStream::ModifyMulticastGroup);
    assert(modifyMulticastGroupCmd.reserved1 == 0x00);
    assert(modifyMulticastGroupCmd.mcastPeerIndex == mcastPeerIndex);
    assert(modifyMulticastGroupCmd.mcastPhyRate == phyRate);
    assert(modifyMulticastGroupCmd.reserved2 == 0x0000);

    assert(routingHdr.srcAddr == 1);
    assert(routingHdr.destAddr == 1);
    assert(routingHdr.dataLength == sizeof(IswStream::iswModifyMulticastGroupCommand));

    // Test Hibernation Indication
//    event.event      = IswStream::HibernationIndication;
//    event.eventType  = IswInterface::Stream;
//    event.resultCode = 0;

//    // Use some interesting numbers for the test
//    IswStream::iswEventStreamsHibernationInd streamsHibernationInd;
//    streamsHibernationInd.peerIndex   = 0;
//    streamsHibernationInd.hibernating = 1;
//    streamsHibernationInd.duration    = 250;
//    data = (uint8_t *)&streamsStatusInd;

//    iswStream->DeliverIswMessage(event.event, data);

//    assert(iswStream->GetIswPeerIndex() == streamsHibernationInd.peerIndex);
//    assert(iswStream->GetIswHibernating() == streamsHibernationInd.hibernating);
//    assert(iswStream->GetIswDuration() == streamsHibernationInd.duration);

//    memset(&event, 0, sizeof(IswInterface::iswEvent));
}

void TestIswInterface::cleanup()
{

}

void TestIswInterface::cleanupTestCase()
{
    // Wait for all the tests to finish
    sleep(1);

    iswInterfaceInit->ExitIswInterfaceArray();

    // deletes iswInterface's too
    delete(iswInterfaceInit);

    // Calls deconstructor which closes log file
    theLogger->setImmediateShutdown(true);
    delete(theLogger);
}

QTEST_APPLESS_MAIN(TestIswInterface)
