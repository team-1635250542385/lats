QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += TESTING

LIBS += -L/usr/local/lib -lusb-1.0 -lftdi1

SOURCES +=  \
     ../../Logger/Logger.cpp \
    ../../UsbInterface/UsbInterface.cpp \
    ../../SerialInterface/SerialInterface.cpp \
    ../../UsbInterface/UsbInterfaceInit.cpp \
    Test_UsbInterface.cpp

HEADERS += \
    ../../Logger/Logger.h \
    ../../UsbInterface/UsbInterface.h \
    ../../SerialInterface/SerialInterface.h \
    ../../UsbInterface/UsbInterfaceInit.h \
    Test_UsbInterface.h

