//!###############################################
//! Filename: Test_UsbInterface.h
//! Description: Class that provides methods
//!              to test UsbInterface class
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef TEST_USBINTERFACE_H
#define TEST_USBINTERFACE_H

#include <QtTest/QTest>
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"

class TestUsbInterface : public QObject
{
    Q_OBJECT

public:
    TestUsbInterface();
    ~TestUsbInterface();

private slots:    
    // These are built into Qt:
    // called before the first test function
    void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    void cleanupTestCase();

    // Test UsbInterface
    void testOpenUsbDevice();
    void testCloseUsbDevice();

private:
    UsbInterfaceInit usbInterfaceInit;
    Logger *theLogger;
    std::array<UsbInterface *, MAX_DEVICES> *usbIntrfaceArray = nullptr;
};

#endif
