//!###############################################
//! Filename: Test_UsbInterface.cpp
//! Description: Class that provides methods
//!              to test UsbInterface class
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914

//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.

//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "Test_UsbInterface.h"
#include <stdio.h>
#include <stdlib.h>

#define USB_ENDPOINT_IN	    (LIBUSB_ENDPOINT_IN  | 1)   /* endpoint address */
#define USB_ENDPOINT_OUT	(LIBUSB_ENDPOINT_OUT | 2)   /* endpoint address */

TestUsbInterface::TestUsbInterface()
{

}

TestUsbInterface::~TestUsbInterface()
{

}

void TestUsbInterface::initTestCase()
{
    int status = 0;

    // This is called before other tests are run
    std::string testFilename = "unitTestUsb";
    uint16_t debugLevel = 0x0000;
    theLogger = new Logger(testFilename, debugLevel);
    assert(theLogger);

    usbIntrfaceArray = usbInterfaceInit.InitUsbInterfaceArray(theLogger);

    assert(status == 0);
    assert(usbInterfaceInit.theLogger == theLogger);
    assert(usbIntrfaceArray != nullptr);
    assert(usbInterfaceInit.GetUsbInterfaceArray() == usbIntrfaceArray);

    for (auto itr = usbIntrfaceArray->begin(); itr != usbIntrfaceArray->end(); ++itr)
    {
        UsbInterface *usbInterface = *itr;
        assert(usbInterface != nullptr);
        assert(usbInterface->desc != nullptr);        
        assert(usbInterface->GetDeviceDescriptor() == usbInterface->desc);
        assert(usbInterface->context != nullptr);
        assert(usbInterface->GetContext() == usbInterface->context);
        assert(usbInterface->theLogger == theLogger);
        assert(usbInterface->GetVendorId() == usbInterface->desc->idVendor);
        assert(usbInterface->GetIndex() == 0); // The device index is the array index

        // Check that this is TRU config not CDC
        std::list<libusb_config_descriptor *> *configs = usbInterface->GetConfigList();
        assert(configs->front() != nullptr);
        assert(configs->front()->interface->altsetting->bInterfaceClass == UsbInterface::LIBUSB_VENDOR_SPECIFIC_DATA);

        break; //Just test one
    }

}

void TestUsbInterface::init()
{

}

void TestUsbInterface::cleanup()
{

}

void TestUsbInterface::cleanupTestCase()
{
    int status = 0;

    status = usbInterfaceInit.ExitUsbInterfaceArray();
    assert(status == 0);

    for (auto itr = usbIntrfaceArray->begin(); itr != usbIntrfaceArray->end(); ++itr)
    {
        assert(*itr == nullptr);
    }

    // Calls deconstructor which closes log file
    delete(theLogger);
}

void TestUsbInterface::testOpenUsbDevice()
{
    UsbInterface *usbInterface = *usbIntrfaceArray->begin();
    assert(usbInterface != nullptr);
    assert(theLogger == usbInterface->theLogger);
    assert(usbInterface->deviceOpen == true);
}

void TestUsbInterface::testCloseUsbDevice()
{
    UsbInterface *usbInterface = *usbIntrfaceArray->begin();
    usbInterface->CloseDevice();
    assert(usbInterface->devHandle == NULL);
    assert(usbInterface->deviceOpen == false);
    usbInterface->~UsbInterface();
}

QTEST_APPLESS_MAIN(TestUsbInterface)
