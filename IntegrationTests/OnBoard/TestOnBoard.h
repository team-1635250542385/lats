#ifndef TESTONBOARD_H
#define TESTONBOARD_H

#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "../../Logger/Logger.h"
#include "../../Logger/DBLogger.h"
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../IswInterface/IswInterfaceInit.h"
#include "../../IswInterface/IswInterface.h"

class TestOnBoard
{

public:
    TestOnBoard();
    ~TestOnBoard();

    bool testParallelDone = false;
    void initIswTest();
    void *runIswTest();

private:
    static void *dequeue_messages(void *arguments);
    void exitIswTest(void);

    IswInterfaceInit *iswInterfaceInit = nullptr;
    std::array<IswInterface *, 21> *iswInterfaceArray = nullptr;

    Logger *theLogger = nullptr;
    DBLogger* dbLogger = nullptr;
    // Logger Thread
    pthread_t loggerThread;
    // Used for shutdown
    bool loggerDone = false;

    IswInterface *parallelIswInterface = nullptr;
    OnBoard *onBoard = nullptr;

    std::vector<uint8_t> testPeerList; // List of peers to send test data

    struct arg_struct
    {
        void *thisPtr;
        Logger* logger;
        DBLogger *dbLogger;
    };

};

#endif // TESTONBOARD_H
