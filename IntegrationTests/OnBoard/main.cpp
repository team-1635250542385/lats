//!######################################################
//! File: main.cpp
//! Description: Used for ISW interface integration tests
//!
//!######################################################

#include "TestOnBoard.h"


int main(int argc, char *argv[])
{
    if ( argc < 1 )
    {
        std::cout << "usage: ./testOnBoard" << std::endl;
        exit(0);
    }


    TestOnBoard *testOnBoard = new TestOnBoard();

    testOnBoard->initIswTest();

    testOnBoard->runIswTest();

    while ( !testOnBoard->testParallelDone )
    {
        sleep(1);
    }

    delete(testOnBoard);
    std::cout << "OnBoard Tests Integration Test completed" << std::endl;

    exit(0);
}
