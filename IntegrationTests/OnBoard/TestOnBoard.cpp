//!######################################################
//! File: TestOnBoard.cpp
//! Description: Used for Paralllel interface integration tests
//!              tests
//!
//!######################################################
#include "TestOnBoard.h"


/* There are four command/response pairs:
    - Configure a traffic generator for a given peer.
    - Get configuration of traffic generator for a given peer.
    - Start a configured traffic generator session.
    - Stop a configured traffic generator session.

    The use case is:

    1. A peer connects to the local node. Streams Status indicates a peer index to local application.
    2. Local application issues Set Indications command to select Stream Status version 4.
    3. Local application issues a Test Mode Set Command to configure a traffic generator for that peer.
    4. Local application waits for Test Mode Set Response (Success).
    5. Local application issues Test Start Command for that peer.
    6. Streams Status indication (version 4) shows Tx throughput of approximately configured
*/

TestOnBoard::TestOnBoard()
{

}

TestOnBoard::~TestOnBoard()
{
    delete(theLogger);
    delete(dbLogger);
}

void *TestOnBoard::dequeue_messages(void *arguments)
{
    struct arg_struct *args = (struct arg_struct *)arguments;

    TestOnBoard *thisTestPtr = (TestOnBoard *)args->thisPtr;
    Logger *theLogger = args->logger;
    DBLogger *dbLogger = args->dbLogger;
    std::cout << "Logger started" << std::endl;

    while ( !thisTestPtr->loggerDone )
    {
        // If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);
        dbLogger->deQueue();
        usleep(100);
    }

    std::cout << "Logger thread ending!" << std::endl;
}

void TestOnBoard::initIswTest()
{
    // Logger - beginning of filename with timestamp added
    std::string testFilename = "iswTest";
    std::string dbLogFilename = "iswDB";
    QString kitVersion = "1.0.0";

    uint16_t debugLevel = 0x1001;
    Logger* theLogger = new Logger(testFilename, debugLevel);

    // Create DBLogger
    dbLogFilename = "iswDBLog";
    dbLogger = new DBLogger(dbLogFilename, kitVersion);

    // Start the Logger dequeue thread
    struct arg_struct argsLogger;
    argsLogger.thisPtr = this;
    argsLogger.logger = theLogger;
    argsLogger.dbLogger = dbLogger;

    if (pthread_create(&loggerThread, NULL, &TestOnBoard::dequeue_messages, (void *)&argsLogger) != 0)
    {
        std::cout << "Logger thread didn't start!" << std::endl;
    }
    else
    {
        std::cout << "Logger thread started" << std::endl;
    }

    iswInterfaceInit = new IswInterfaceInit(IswInterface::USB, theLogger, dbLogger);

    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();
}

void TestOnBoard::exitIswTest(void)
{
    // End logger threads
    loggerDone = true;
    sleep(2);

    // Shutdown IswInterfaces
    if ( iswInterfaceInit != nullptr )
    {
        iswInterfaceInit->ExitIswInterfaceArray();
        delete(iswInterfaceInit);
    }

    // Make sure all the threads are gone
    pthread_join(loggerThread, nullptr);

    std::cout << "Test thread ending!" << std::endl;
    testParallelDone = true;
}

void *TestOnBoard::runIswTest()
{
    //! Check that we have devices
    int iswDeviceCount = 0;
    for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
    {
        IswInterface *iswInterface = *iterator;
        if ( (iswInterface != nullptr) && (iswInterface->IsReady()) )
        {
            iswDeviceCount++;

            if ( !iswInterface->GetUsbInterface()->IsDeviceOpen() )
            {
                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " is not open. Are you root?" << std::endl;
                iswDeviceCount--;
            }
        }
    }

    if ( iswDeviceCount <= 0 )
    {
        //! No devices
        std::cout << "No ISW devices present!" << std::endl;
        exitIswTest();
    }
    else
    {
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    UsbInterface *usbInterface = iswInterface->GetUsbInterface();

                    if ( (usbInterface == nullptr) ||
                            (usbInterface->IsDeviceOpen() == false) )
                    {
                        std::cout << "Usb Interface not Open" << std::endl;
                        continue;
                    }
                    else
                    {
                        IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                        IswFirmware *iswFirmware = iswInterface->GetIswFirmware();
                        IswProduct *iswProduct = iswInterface->GetIswProduct();
                        IswStream *iswStream = iswInterface->GetIswStream();

                        uint8_t wireInterfaceEnable = 1;  //! always 1
                        uint8_t associationIndEnable = 1;
                        uint8_t connectionIndEnable = 1;
                        uint8_t resetIndEnable = 1;
                        //! For onboard tests set to 4
                        uint8_t streamsStatusIndEnable = 4; //! For this test

                        int status = iswInterface->GetIswSystem()->SendSetIndicationsCmd(wireInterfaceEnable, associationIndEnable,
                                                                                         connectionIndEnable, resetIndEnable,
                                                                                         streamsStatusIndEnable);
                        sleep(1);

                        //! Test - Get identity of locally attached devices
                        //! The should be setup by the library after the IswInterfaces are initialized
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Active Firmware Version = " << iswFirmware->GetActiveFirmwareVersion() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " MAC Address = " << iswIdentity->GetMacAddressStr() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Device Type = " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Network ID = " << std::to_string(iswProduct->GetIswNetworkId()) << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Channel = " << std::to_string(iswStream->GetIswChannel()) << std::endl;
                    }
                }
            }
        }

        std::cout << "******************************************************" << std::endl;
        std::cout << "Starting On Board Tests" << std::endl;
        std::cout << "******************************************************" << std::endl;


        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;

                // Run the test from the first device that is ready
                if ( iswInterface->IsReady() )
                {
                    IswStream *iswStream = iswInterface->GetIswStream();
                    OnBoard *onBoard = iswInterface->GetIswOnBoard();

                    // Clear peer record and get them again
                    iswStream->InitIswPeerRecords();
                    iswStream->InitIswMulticastRecords();
                    iswStream->SendGetStreamsStatusCmd();

                    sleep(1);

                    // Read the latest peer records in IswLibrary
                    std::array<IswStream::iswPeerRecord, MAX_PEERS> &testPeers = iswStream->GetPeerRef();

                    for (uint8_t i=0; i<(int)testPeers.size(); i++)
                    {
                        if ( testPeers[i].inUse )
                        {
                            testPeerList.push_back(i);
                        }
                    }

                    if ( !testPeerList.empty() )
                    {
                        uint8_t peerIndex = testPeerList.front();
                        uint16_t throughput = 120;
                        uint16_t pktSize = 65535;

                        onBoard->SendSetTestModeCmd(peerIndex, pktSize, throughput);
                        usleep(10);

                        int count = 5;
                        while ( (onBoard->isTestModeSet() == false) && (count > 0) )
                        {
                            sleep(1);
                            count--;
                        }

                        if ( onBoard->isTestModeSet() == true )
                        {
                            onBoard->SendGetTestModeCmd(peerIndex);
                            usleep(10);

                            onBoard->SendStartTestCmd(peerIndex);
                            sleep(5);

                            onBoard->SendStopTestCmd(peerIndex);
                            usleep(10);
                        }
                        else
                        {
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " onBoard test not setup" << std::endl;
                        }
                        break; //Test done
                    }
                    else
                    {
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " has no peers" << std::endl;
                    }
                }
            }
        }

        sleep(5); // let the logging finish

    } //! end else we have devices

    //! Exit libusb
    exitIswTest();
}
