//!######################################################
//! File: TestHotplug.cpp
//! Description: Used for ISW interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//! Identity Command Tests:
//! SetGetDeviceType
//! SetGetCoordinator
//! GetMacAddress
//! GetPersist
//!
//! Firmware Command Tests:
//! GetFirmwareVersion
//!
//! Product:
//! GetNetworkId
//!
//! Metrics Command Tests:
//! GetWirelessThroughput
//! GetPhyRate
//! GetRssi
//!
//! Link Command Tests:
//! SetGetScanDutyCycle
//! ResetSetGetIdleScanFrequency
//!
//! Association Command Tests:
//! ClearStartAssociation
//!
//! Stream Command Tests:
//! SetGetStreamsSpec
//! GetChannel
//! Multicast
//!
//! System Command Tests:
//! SetIndications
//! SetFactoryDefault
//! ResetRadio
//! ResetDevice
//!
//!######################################################
#include "TestHotplug.h"


TestHotplug::TestHotplug(uint testNo):
    testsToRun(testNo)
{

}

TestHotplug::~TestHotplug()
{
    delete(theLogger);
    delete(dbLogger);
}

void *TestHotplug::dequeue_messages(void *arguments)
{
    struct arg_struct *args = (struct arg_struct *)arguments;

    TestHotplug *thisTestPtr = (TestHotplug *)args->thisPtr;
    Logger *theLogger = args->logger;
    DBLogger *dbLogger = args->dbLogger;
    std::cout << "Logger started" << std::endl;

    while ( !thisTestPtr->loggerDone )
    {
        // If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);
        dbLogger->deQueue();
        usleep(100);
    }

    std::cout << "Logger thread ending!" << std::endl;
}

void TestHotplug::initIswTest()
{
    // Logger - beginning of filename with timestamp added
    std::string testFilename = "iswTest";
    std::string dbLogFilename = "iswDB";
    QString kitVersion = "1.0.0";

    uint16_t debugLevel = 0xFFFF;
    Logger* theLogger = new Logger(testFilename, debugLevel);

    // Create DBLogger
    dbLogFilename = "iswDBLog";
    dbLogger = new DBLogger(dbLogFilename, kitVersion);

    // Start the Logger dequeue thread
    struct arg_struct argsLogger;
    argsLogger.thisPtr = this;
    argsLogger.logger = theLogger;
    argsLogger.dbLogger = dbLogger;

    if (pthread_create(&loggerThread, NULL, &TestHotplug::dequeue_messages, (void *)&argsLogger) != 0)
    {
        std::cout << "Logger thread didn't start!" << std::endl;
    }
    else
    {
        std::cout << "Logger thread started" << std::endl;
    }

    iswInterfaceInit = new IswInterfaceInit(IswInterface::USB, theLogger, dbLogger);

    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();
}

void TestHotplug::exitIswTest(void)
{
    // End logger threads
    loggerDone = true;
    sleep(2);

    // Shutdown IswInterfaces
    if ( iswInterfaceInit != nullptr )
    {
        iswInterfaceInit->ExitIswInterfaceArray();
        delete(iswInterfaceInit);
    }

    // Make sure all the threads are gone
    pthread_join(loggerThread, nullptr);

    std::cout << "Test thread ending!" << std::endl;
    testIswDone = true;
}

void TestHotplug::CheckIswCommands()
{
    for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
    {
        if ( *iterator != nullptr )
        {
            IswInterface *iswInterface = *iterator;
            if ( iswInterface->IsReady() )
            {
                UsbInterface *usbInterface = iswInterface->GetUsbInterface();

                if ( (usbInterface == nullptr) ||
                        (usbInterface->IsDeviceOpen() == false) )
                {
                 std::cout << "Usb Interface not Open" << std::endl;
                    continue;
                }
                else
                {
                    IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                    IswFirmware *iswFirmware = iswInterface->GetIswFirmware();
                    IswSystem *iswSystem = iswInterface->GetIswSystem();
                    IswLink *iswLink = iswInterface->GetIswLink();
                    IswProduct *iswProduct = iswInterface->GetIswProduct();
                    IswStream *iswStream = iswInterface->GetIswStream();

                    //! Test - Get identity of locally attached devices
                    //! The should be setup by the library after the IswInterfaces are initialized
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Active Firmware Version = " << iswFirmware->GetActiveFirmwareVersion() << std::endl;
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " MAC Address = " << iswIdentity->GetMacAddressStr() << std::endl;
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Device Type = " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Network ID = " << std::to_string(iswProduct->GetIswNetworkId()) << std::endl;

                    if ( testsToRun & GetChannel )
                    {
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Channel = " << std::to_string(iswStream->GetIswChannel()) << std::endl;
                    }

                    if ( testsToRun & SetGetCoordinator )
                    {
                        int status = iswIdentity->SendGetCoordinatorCmd();
                        usleep(10);
                    }

                    if ( testsToRun & GetWirelessThroughput )
                    {
                        ///! Test - Get Metrics
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Wireless Metrics" << std::endl;
                        IswMetrics *iswMetrics = iswInterface->GetIswMetrics();

                        //! Get Wireless Metrics
                        int status = iswMetrics->SendGetWirelessMetrics();
                        usleep(10);
                    }
                }
            }
        }
    }
}

void *TestHotplug::runIswTest()
{
    std::cout << "******************************************************" << std::endl;
    std::cout << "Starting Isw Interface Tests" << std::endl;
    std::cout << "testsToRun = " << std::hex << std::to_string(testsToRun) << std::endl;
    std::cout << "******************************************************" << std::endl;

    //! Check that we have devices
    int iswDeviceCount = 0;
    for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
    {
        IswInterface *iswInterface = *iterator;
        if ( (iswInterface != nullptr) && (iswInterface->IsReady()) )
        {
            iswDeviceCount++;

            if ( !iswInterface->GetUsbInterface()->IsDeviceOpen() )
            {
                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " is not open. Are you root?" << std::endl;
                iswDeviceCount--;
            }
        }
    }

    if ( iswDeviceCount <= 0 )
    {
        //! No devices
        std::cout << "No ISW devices present!" << std::endl;
        exitIswTest();
    }
    else
    {
        CheckIswCommands();

        for (int i=0; i<10; i++)
        {
            std::cout << "Add or remove a device now" << std::endl;
            sleep(10);

            iswInterfaceInit->CheckHotplugQueue();

            CheckIswCommands();
        }
    } //! end else we have devices

    //! Exit libusb
    exitIswTest();
}
