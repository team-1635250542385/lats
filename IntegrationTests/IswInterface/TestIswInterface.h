#ifndef TESTISWINTERFACE_H
#define TESTISWINTERFACE_H

#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include "../../Logger/Logger.h"
#include "../../Logger/DBLogger.h"
#include "../../UsbInterface/UsbInterfaceInit.h"
#include "../../UsbInterface/UsbInterface.h"
#include "../../IswInterface/IswInterfaceInit.h"
#include "../../IswInterface/IswInterface.h"

class TestIswInterface
{

public:
    TestIswInterface(uint testNo);
    ~TestIswInterface();

    bool testIswDone = false;
    void initIswTest();
    void *runIswTest();

private:
    static void *dequeue_messages(void *arguments);
    void exitIswTest(void);
    int testEndianess(IswInterface *iswInterface);

    IswInterfaceInit *iswInterfaceInit = nullptr;
    std::array<IswInterface *, 21> *iswInterfaceArray = nullptr;

    Logger *theLogger                   = nullptr;
    DBLogger* dbLogger                  = nullptr;
    Logger *throughputTestLoggerPass    = nullptr;
    Logger *throughputResultsLoggerPass = nullptr;
    Logger *latencyLogger               = nullptr;

    // Logger Thread
    pthread_t loggerThread;

    // Used for shutdown
    bool loggerDone = false;

    struct arg_struct {
        void *thisPtr;
        Logger* logger;
        DBLogger *dbLogger;
    };

    uint testsToRun = 0;

    //! Possible tests
    //!
    //! Identity Commands:
    static const uint SetGetDeviceType  = 0x01;
    static const uint SetGetCoordinator = 0x02;
    static const uint GetMacAddress     = 0x04;
    static const uint GetPersist        = 0x08;

    //! Firmware:
    static const uint GetFirmwareVersion = 0x10;

    //! Product:
    static const uint GetNetworkId = 0x20;

    //! Metrics Commands:
    static const uint GetWirelessThroughput = 0x40;
    static const uint GetPhyRate            = 0x80;
    static const uint GetRssi               = 0x100;

    //! Link Commands:
    static const uint SetGetScanDutyCycle            = 0x200;
    static const uint ResetSetMaxServiceInterval     = 0x400;
    static const uint ResetSetGetIdleScanFrequency   = 0x800;
    static const uint SetGetBackgroundScanParameters = 0x6000;
    static const uint SetGetAntennaConfiguration     = 0x600;
    static const uint SetGetAntennaId                = 0x700;
    static const uint SetBandgroupBias               = 0x6001;

    //! Association Commands:
    static const uint ClearStartAssociation = 0x1000;

    //! Stream Commands:
    static const uint SetGetStreamsSpec = 0x2000;
    static const uint GetChannel        = 0x4000;
    static const uint Multicast         = 0x8000;

    //! System Commands:
    static const uint SetIndications    = 0x10000;
    static const uint SetFactoryDefault = 0x20000;
    static const uint IdleRadio         = 0x40000;
    static const uint ResetRadio        = 0x80000;
    static const uint ResetDevice       = 0x100000;
    static const uint GetRadioStatus    = 0x200000;

};

#endif // TESTISWINTERFACE_H
