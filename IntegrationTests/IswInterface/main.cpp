//!######################################################
//! File: main.cpp
//! Description: Used for ISW interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//! Identity Command Tests:
//! SetGetDeviceType
//! SetGetCoordinator
//! GetMacAddress
//! GetPersist
//!
//! Firmware Command Tests:
//! GetFirmwareVersion
//!
//! Product:
//! GetNetworkId
//!
//! Metrics Command Tests:
//! GetWirelessThroughput
//! GetPhyRate
//! GetRssi
//!
//! Link Command Tests:
//! SetGetScanDutyCycle
//! ResetSetGetIdleScanFrequency
//!
//! Association Command Tests:
//! ClearStartAssociation
//!
//! Stream Command Tests:
//! SetGetStreamsSpec
//! GetChannel
//! Multicast
//!
//! System Command Tests:
//! SetIndications
//! SetFactoryDefault
//! ResetRadio
//! ResetDevice
//!
//!######################################################

#include "TestIswInterface.h"


int main(int argc, char *argv[])
{
    if ( argc < 2 )
    {
        std::cout << "usage: ./TestIswInterface testValue" << std::endl;
        std::cout << "See TestIswInterface.h file" << std::endl;
        std::cout << "0x7FFFF will run all but the reset commands" << std::endl;
        exit(0);
    }

    std::stringstream ss;
    ss << argv[1];
    uint testNo = 0;
    ss >> std::hex >> testNo;

    TestIswInterface *testIswInterface = new TestIswInterface(testNo);

    testIswInterface->initIswTest();
    testIswInterface->runIswTest();

    while ( !testIswInterface->testIswDone )
    {
        sleep(1);
    }

    delete(testIswInterface);
    std::cout << "ISW Integration Test completed" << std::endl;

    exit(0);
}
