//!######################################################
//! File: TestIswInterface.cpp
//! Description: Used for ISW interface integration tests
//! The following tests are available in this file and
//! can be set by command line argument:
//!
//! Identity Command Tests:
//! SetGetDeviceType
//! SetGetCoordinator
//! GetMacAddress
//! GetPersist
//!
//! Firmware Command Tests:
//! GetFirmwareVersion
//!
//! Product:
//! GetNetworkId
//!
//! Metrics Command Tests:
//! GetWirelessThroughput
//! GetPhyRate
//! GetRssi
//!
//! Link Command Tests:
//! SetGetScanDutyCycle
//! ResetSetGetIdleScanFrequency
//! SetBackgroundScanParameters
//! GetBackgroundScanParameters
//! SetAntennaConfiguration
//! GetAntennaConfiguration
//! SetAntennaId
//! GetAntennaId
//! SetBandgroupBias
//!
//! Association Command Tests:
//! ClearStartAssociation
//! StopAssociation
//!
//! Stream Command Tests:
//! SetGetStreamsSpec
//! GetChannel
//! Multicast
//!
//! System Command Tests:
//! SetIndications
//! SetFactoryDefault
//! ResetRadio
//! ResetDevice
//! GetRadioStatus
//!
//!######################################################
#include "TestIswInterface.h"
#include <iomanip>

TestIswInterface::TestIswInterface(uint testNo):
    testsToRun(testNo)
{

}

TestIswInterface::~TestIswInterface()
{
    delete(theLogger);
    delete(dbLogger);
}

void *TestIswInterface::dequeue_messages(void *arguments)
{
    struct arg_struct *args = (struct arg_struct *)arguments;

    TestIswInterface *thisTestPtr = (TestIswInterface *)args->thisPtr;
    Logger *theLogger             = args->logger;
    DBLogger *dbLogger            = args->dbLogger;
    std::cout << "Logger Started" << std::endl;

    while ( !thisTestPtr->loggerDone )
    {
        // If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);
        dbLogger->deQueue();
        usleep(100);
    }

    std::cout << "Logger Thread Ending!" << std::endl;
}

void TestIswInterface::initIswTest()
{
    //! Logger - beginning of filename with timestamp added
    std::string testFilename  = "iswTest";
    std::string dbLogFilename = "iswDB";
    QString kitVersion        = "1.7.0";

    uint16_t debugLevel         = 0xFFFF;
    Logger* theLogger           = new Logger(testFilename, debugLevel);
    throughputTestLoggerPass    = new Logger(testFilename, debugLevel);
    throughputResultsLoggerPass = new Logger(testFilename, debugLevel);
    latencyLogger               = new Logger(testFilename, debugLevel);

    //! Create DBLogger
    dbLogFilename = "iswDBLog";
    dbLogger      = new DBLogger(dbLogFilename, kitVersion);

    //! Start the Logger dequeue thread
    theLogger->setImmediateShutdown(true);
    struct arg_struct argsLogger;
    argsLogger.thisPtr  = this;
    argsLogger.logger   = theLogger;
    argsLogger.dbLogger = dbLogger;

    if (pthread_create(&loggerThread, NULL, &TestIswInterface::dequeue_messages, (void *)&argsLogger) != 0)
    {
        std::cout << "Logger Thread Didn't Start!" << std::endl;
    }
    else
    {
        std::cout << "Logger Thread Started" << std::endl;
    }

    iswInterfaceInit  = new IswInterfaceInit(IswInterface::USB, theLogger, throughputTestLoggerPass, throughputResultsLoggerPass, dbLogger, latencyLogger);
    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();
}

void TestIswInterface::exitIswTest(void)
{
    // End logger threads
    loggerDone = true;
    sleep(2);

    // Shutdown IswInterfaces
    if ( iswInterfaceInit != nullptr )
    {
        iswInterfaceInit->ExitIswInterfaceArray();
        delete(iswInterfaceInit);
    }

    // Make sure all the threads are gone
    pthread_join(loggerThread, nullptr);

    std::cout << "Test Thread Ending!" << std::endl;
    testIswDone = true;
}

int TestIswInterface::testEndianess(IswInterface *iswInterface)
{
    int status = 0;
    std::string msgString;

    std::cout << "Test Endianess" << std::endl;

    bool isLittleEndian = true;
    if ( htonl(47) == 47 )
    {
        //! Big Endian
        isLittleEndian = false;
    }
    else
    {
        //! Little Endian
        isLittleEndian = true;
    }

    if (iswInterface != nullptr)
    {
        if (iswInterface->IsLittleEndian() != isLittleEndian)
        {
           msgString = "iswInterface endianness != system";
           iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
           status = -1;
        }
        else
        {
           msgString = "iswInterface endianness OK";
           iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return (status);
}

void *TestIswInterface::runIswTest()
{
    std::cout << "******************************************************" << std::endl;
    std::cout << "Starting Isw Interface Tests" << std::endl;
    std::cout << "testsToRun = 0x" << std::hex << std::setw(2) << (int)(testsToRun) << std::endl;
    std::cout << "******************************************************" << std::endl;

    //! Check that we have devices
    int iswDeviceCount = 0;
    for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
    {
        IswInterface *iswInterface = *iterator;
        if ( (iswInterface != nullptr) && (iswInterface->IsReady()) )
        {
            iswDeviceCount++;

            if ( !iswInterface->GetUsbInterface()->IsDeviceOpen() )
            {
                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Is Not Open. Are You Root?" << std::endl;
                iswDeviceCount--;
            }
        }
    }

    if ( iswDeviceCount <= 0 )
    {
        //! No devices
        std::cout << "No ISW Devices Present!" << std::endl;
        exitIswTest();
    }
    else
    {
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    UsbInterface *usbInterface = iswInterface->GetUsbInterface();

                    if ( (usbInterface == nullptr) ||
                         (usbInterface->IsDeviceOpen() == false) )
                    {
                        std::cout << "Usb Interface Not Open" << std::endl;
                        continue;
                    }
                    else
                    {
                        IswSecurity *iswSecurity = iswInterface->GetIswSecurity();
                        if ( !iswSecurity->IsAuthenticated() )
                        {
                            std::cout << "Device " << iswInterface->GetIndex() << " Authentication failed"  << std::endl;
                            continue;
                        }

                        IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                        IswFirmware *iswFirmware = iswInterface->GetIswFirmware();
                        IswSystem *iswSystem     = iswInterface->GetIswSystem();
                        IswLink *iswLink         = iswInterface->GetIswLink();
                        IswProduct *iswProduct   = iswInterface->GetIswProduct();
                        IswStream *iswStream     = iswInterface->GetIswStream();
                        IswMetrics *iswMetrics   = iswInterface->GetIswMetrics();

                        if ( testsToRun & SetIndications )
                        {
                            //! Test Set Indications command
                            //! Set association | connection | reset | or streams status to zero
                            //! Look for the SetIndications message in the log file, then see if any
                            //! of the messages set to zero came after that.
                            uint8_t wireInterfaceEnable    = 1;  //! Disabling this disables the ability to monitor for connections/disconnections
                            uint8_t associationIndEnable   = 1;
                            uint8_t connectionIndEnable    = 1;
                            uint8_t resetIndEnable         = 1;
                            //! Set to 1, 2, 3, or 4 to enable indications
                            //! The meaning of each is unclear - not in docs
                            //! Except that the default is 2
                            uint8_t streamsStatusIndEnable = 2;
                            uint8_t multicastIndEnable     = 1;
                            uint8_t hibernationIndEnable   = 1;

                            iswSystem->SendSetIndicationsCmd(wireInterfaceEnable, associationIndEnable, connectionIndEnable,
                                                                          resetIndEnable, multicastIndEnable,
                                                                          streamsStatusIndEnable, hibernationIndEnable);
                            usleep(10);
                        }

                        // Ask the firmware for these things in case we didn't get them yet
                        iswIdentity->SendGetDeviceTypeCmd();
                        usleep(10);
                        iswFirmware->SendGetFirmwareVersionCmd(1);
                        usleep(10);
                        iswProduct->SendGetNetworkIdCmd();
                        usleep(10);
                        iswIdentity->SendGetMacAddressCmd();
                        usleep(10);
                        iswIdentity->SendGetCoordinatorCmd();
                        usleep(10);
                        iswStream->SendGetStreamsStatusCmd();
                        usleep(10);
                        iswMetrics->SendGetWirelessMetrics();
                        usleep(10);

                        //! Test - Get identity of locally attached devices
                        //! The should be setup by the library after the IswInterfaces are initialized
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Active Firmware Version = " << iswFirmware->GetActiveFirmwareVersion() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " MAC Address = " << iswIdentity->GetMacAddressStr() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Device Type = " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Network ID = " << std::to_string(iswProduct->GetIswNetworkId()) << std::endl;

                        if ( testsToRun & GetChannel )
                        {
                            uint8_t channel = iswStream->GetIswChannel();
                            iswStream->SendGetStreamsStatusCmd();
                            usleep(10);

                            // Channel and role come in the Streams Status message
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Channel = " << std::to_string(channel);

                            bool channelVerified = false;
                            switch (channel)
                            {
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 72:
                                case 73:
                                case 74:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 88:
                                case 89:
                                case 90:
                                case 49:
                                case 50:
                                case 51:
                                case 52:
                                case 54:
                                case 55:
                                case 112:
                                case 113:
                                case 114:
                                    std::cout << " GOOD Value!" << std::endl;
                                    break;
                                case 0:
                                default:
                                    std::cout << " NOT OK!" << std::endl;
                                    break;
                                }
                        }

                        if ( testsToRun & SetGetDeviceType )
                        {
                            if ( iswInterface == *iswInterfaceArray->begin() )
                            {
                                uint16_t deviceType = IswIdentity::TWS; // Thermal Weapons Sight - has coordinator capable 1
                                iswIdentity->SendSetDeviceTypeCmd(deviceType);
                                sleep(1);
                                iswIdentity->SendGetDeviceTypeCmd();
                                usleep(100);
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Changed Device Type To: " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                            }

                            sleep(1);
                        }

                        if ( testsToRun & SetGetScanDutyCycle )
                        {
                            //! Test - Set/Get ScanDutyCycleCmd for locally attached devices
                            uint16_t startupScanDuration = 2;
                            uint8_t scanDutyCycle        = 4;
                            uint8_t persist              = 1;
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Scan Duty Cycle" << std::endl;
                            iswLink->SendSetScanDutyCycleCmd(startupScanDuration, scanDutyCycle, persist);
                            usleep(100);
                            iswLink->SendGetScanDutyCycleCmd();
                            usleep(10);
                        }

                        if ( testsToRun & ResetSetMaxServiceInterval )
                        {
                            //! Test - SetMaxServiceInterval for locally attached devices
                            //!      - reset first to zero, check, then set
                            int status = iswLink->SendResetMaxServiceIntervalCmd();
                            usleep(100);
                            //! now set it to the default
                            uint32_t maxServiceInterval = 32000;
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Max Service Interval" << std::endl;
                            status = iswLink->SendSetMaxServiceIntervalCmd(maxServiceInterval);
                            usleep(100);
                        }

                        if ( testsToRun & ResetSetGetIdleScanFrequency )
                        {
                            //! Test - Set/GetIdleScanFrequency for locally attached devices
                            uint16_t idleScanFrequency = 60;
                            uint8_t persist            = 0;
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Idle Scan Frequency" << std::endl;
                            int status = iswLink->SendSetIdleScanFrequencyCmd(idleScanFrequency, persist);
                            usleep(100);

                            status = iswLink->SendGetIdleScanFrequencyCmd();
                            usleep(100);
                        }

                        if ( testsToRun & SetGetBackgroundScanParameters)
                        {
                            //! Test Set and Get Background Scan Parameters
                            uint16_t stableScanFrequency = 120;
                            uint8_t persist              = 0;
                            uint8_t version              = 0x01;
                            uint16_t fluxScanFrequency   = 20;
                            uint16_t fluxScanDuration    = 300;

                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Background Scan Parameters" << std::endl;
                            iswLink->SendSetBackgroundScanParametersCmd(stableScanFrequency, persist, version, fluxScanFrequency, fluxScanDuration);
                            usleep(100);

                            iswLink->SendGetBackgroundScanParametersCmd();
                            usleep(100);
                        }

                        if( testsToRun & SetGetAntennaConfiguration )
                        {
                            //! Test Set and Get Antenna Configuration
                            uint8_t enable                  = 0x01;
                            uint8_t triggerAlgorithm        = 0x00;
                            uint8_t triggerRssiThreshold    = 174;
                            uint8_t triggerPhyRateThreshold = 0;
                            uint8_t switchThresholdAdder    = 1;

                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set/Get Antenna Configuration" << std::endl;
                            iswLink->SendSetAntennaConfigurationCmd(enable, triggerAlgorithm, triggerRssiThreshold, triggerPhyRateThreshold, switchThresholdAdder);
                            sleep(1);

                            iswLink->SendGetAntennaConfigurationCmd();
                            sleep(1);
                        }

                        if( testsToRun & SetGetAntennaId )
                        {
                            //! Test Set and Get Antenna Id
                            uint8_t antennaId = 0x01;

                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set/Get Antenna Id" << std::endl;
                            iswLink->SendSetAntennaIdCmd(antennaId);
                            sleep(1);

                            iswLink->SendGetAntennaIdCmd();
                            sleep(1);
                        }

                        if ( testsToRun & SetGetCoordinator )
                        {
                            //! Test Set and Get Coordinator settings
                            //! Use the defaults according to the Embedment Guide
                            uint8_t coordinatorCapable    = 0;
                            uint8_t coordinatorPrecedence = 0;
                            uint8_t persists              = 0;
                            int status                    = 0;

                            if ( iswInterface->GetIndex() == 0 )
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set to Coordinator" << std::endl;
                                iswInterface->GetIswIdentity()->GetDeviceTypeDefaults(iswIdentity->GetIswDeviceType(), &coordinatorCapable, &coordinatorPrecedence);
                                status = iswIdentity->SendSetCoordinatorCmd(coordinatorCapable,coordinatorPrecedence, persists);
                            }
                            else
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set to Peer" << std::endl;
                                iswInterface->GetIswIdentity()->GetDeviceTypeDefaults(iswIdentity->GetIswDeviceType(), &coordinatorCapable, &coordinatorPrecedence);
                                status = iswIdentity->SendSetCoordinatorCmd(coordinatorCapable,coordinatorPrecedence, persists);
                            }
                            usleep(10);

                            status = iswIdentity->SendGetCoordinatorCmd();
                            usleep(10);
                        }


                        if ( testsToRun & GetWirelessThroughput )
                        {
                            ///! Test - Get Metrics
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Wireless Metrics" << std::endl;
                            IswMetrics *iswMetrics = iswInterface->GetIswMetrics();

                            //! Get Wireless Metrics
                            int status = iswMetrics->SendGetWirelessMetrics();
                            usleep(10);
                        }

                        if( testsToRun & SetBandgroupBias)
                        {
                            //! Test Set Bandgroup Bias Command
                            uint8_t bgPrefer = 0x02;
                            uint8_t bgAvoid  = 0x08;
                            uint8_t persist  = 0x00;

                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Bandgroup Bias" << std::endl;
                            iswLink->SendSetBandgroupBiasCmd(bgPrefer, bgAvoid, persist);
                            sleep(1);
                        }
                    }
                }
            }
        }

        if ( testsToRun & ClearStartAssociation )
        {
            //! Test Clear Association
            //! Reset the radio to force the radios to re-associate
            //! Send out the clear association commands to all devices
            for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
            {
               if ( *iterator != nullptr )
               {
                   IswInterface *iswInterface = *iterator;
                   if ( iswInterface->IsReady() )
                   {
                       IswAssociation *iswAssociation = iswInterface->GetIswAssociation();
                       IswSystem *iswSystem = iswInterface->GetIswSystem();
                       IswStream *iswStream = iswInterface->GetIswStream();
                       int status = 0;

                       //! The first device remembers it's network and will invite later
                       //! The other devices forget their network (ClearAssociation and ResetRadio)
                       //! and will search to join the first device's network
                       if ( iswInterface->GetIndex() > 0 )
                       {

                           std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Clear Association" << std::endl;
                           status = iswAssociation->SendClearAssociationCmd();
                           usleep(10);

                           //! Reset the radio to force disconnect
                           std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Reset Radio" << std::endl;
                           status = iswSystem->SendResetRadioCmd();
                           usleep(100);
                       }
                   }
                }
            }

            for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
            {
               if ( *iterator != nullptr )
               {
                   IswInterface *iswInterface = *iterator;
                   if ( iswInterface->IsReady() )
                   {
                       IswAssociation *iswAssociation = iswInterface->GetIswAssociation();
                       IswSystem *iswSystem           = iswInterface->GetIswSystem();
                       IswStream *iswStream           = iswInterface->GetIswStream();
                       int status                     = 0;

                       //! The first device remembers it's network and will invite later
                       //! The other devices forget their network (ClearAssociation and ResetRadio)
                       //! and will search to join the first device's network
                       if ( iswInterface->GetIndex() > 0 )
                       {
                            iswStream->InitIswPeerRecords();
                            usleep(100);
                            iswStream->InitIswMulticastRecords();
                            usleep(100);
                       }
                   }
               }
            }


            uint8_t timeout = 20;
            bool sendInvite = true;
            for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
            {
               if ( *iterator != nullptr )
               {
                   IswInterface *iswInterface = *iterator;
                   if ( iswInterface->IsReady() )
                   {
                       IswAssociation *iswAssociation = iswInterface->GetIswAssociation();
                       IswSystem *iswSystem           = iswInterface->GetIswSystem();
                       int status                     = 0;

                       if ( sendInvite )
                       {
                            //! Now try to start association
                            //! set the first one as "invite" and the next as "search"
                            uint8_t type = 1; //! invite
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Start Association - Invite" << std::endl;
                            status = iswAssociation->SendStartAssociationCmd(type, timeout);
                            usleep(10);
                            sendInvite = false;
                       }
                       else
                       {
                           uint8_t type = 2;   //! search
                           std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Start Association - Search" << std::endl;
                           status = iswAssociation->SendStartAssociationCmd(type, timeout);
                           // Wait for the two devices to connect
                           sleep(timeout);
                           sendInvite = true;
                           iterator--;
                       }
                   }
               }
            }
    }

    if ( testsToRun & SetGetStreamsSpec )
    {
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {

                    IswInterface *iswInterface = *iterator;
                    IswStream *iswStream       = iswInterface->GetIswStream();
                    int status                 = 0;

                    //! Just use the first device and run these test on one device
                    if ( iswInterface->GetIndex() == 0 )
                    {
                        //! Test SetStreamsSpec on one device
                        std::array<IswStream::iswPeerRecord, MAX_PEERS> &testPeers = iswStream->GetPeerRef();
                        uint8_t peerIndex = 0;
                        for (int i = 0; i < (int)testPeers.size(); i++)
                        {
                            if ( testPeers[i].inUse )
                            {
                                peerIndex = i;
                                break; //! out of for loop
                            }
                        }

                        //! Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the sub-stream is High Priority
                        uint8_t txPriority = 1;

                        //! Bitmap for unicast sub-streams (use bits 0-3) where a bit set to 1 indicates the ISW-MAC
                        //! may automatically discard the sub-stream data in the event of network congestions
                        uint8_t txDiscard = 3;

                        //! Max duration (in microseconds) between transmit opportunities
                        uint32_t maxTxLatency = 100;
                        status                = iswStream->SendSetStreamsSpecCmd(peerIndex, txPriority, txDiscard, maxTxLatency);
                        sleep(1);
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Stream Spec" << std::endl;

                        status = iswStream->SendGetStreamSpecCmd(peerIndex);
                        usleep(100);

                        //! Test IswStream GetStreamStatus command
                        //!status = iswStream->SendGetStreamsStatusCmd();
                        //!usleep(10);

                        uint8_t version = 2;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Extended Stream Spec" << std::endl;
                        status = iswStream->SendGetExtendedStreamsStatusCmd(version);
                        usleep(100);

                        //! Test GetPeerFirmwareVersion
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Peer Firmware Version" << std::endl;
                        status = iswStream->SendGetPeerFirmwareVersionCmd(peerIndex);
                        usleep(100);
                    }
                }
            }
        }
    }

    if ( testsToRun & Multicast )
    {
        int indexMCastStartDevice = 0;
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    IswStream *iswStream = iswInterface->GetIswStream();
                    int status = 0;

                    //! Just use the first device and run these test on one device
                    //! Test Multicast
                    uint8_t phyRate = 1;
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Start Multicast Group" << std::endl;
                    status = iswStream->SendStartMulticastGroupCmd(phyRate);
                    sleep(5);

                    status = iswStream->SendGetMulticastGroupsStatusCmd();
                    sleep(2);

                    indexMCastStartDevice = iswInterface->GetIndex();
                    break;
                }
            }
        }

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    IswStream *iswStream = iswInterface->GetIswStream();
                    int status = 0;

                    if ( iswInterface->GetIndex() == indexMCastStartDevice )
                    {
                        continue;
                    }
                    else
                    {
                        //! Others devices try to join the multicast group
                        //! Device 1 created.
                        status = iswStream->SendGetMulticastGroupsStatusCmd();
                        sleep(2);

                        std::array<IswStream::multicastGroupRecords, MAX_MCAST_RECORDS> multicastGroupRecords;
                        iswStream->GetMulticastGroupRecords(&multicastGroupRecords);

                        uint8_t testPhyRate = 1;
                        for (int i = 0; i < (int)multicastGroupRecords.size(); i++)
                        {
                            if ( multicastGroupRecords[i].inUse &&
                                 ((multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex >= 0x01) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex <= 0xFF)) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastStatus == IswStream::McastGroupAvailableToJoin) )
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Start Join Multicast Group" << std::endl;
                                status = iswStream->SendJoinMulticastGroupCmd(multicastGroupRecords[i].iswMcastRecord.mcastAddress, testPhyRate);
                            }
                        }

                        iswStream->GetMulticastGroupRecords(&multicastGroupRecords);
                        sleep(3);

                        //! Record that we joined the group
                        for (int i = 0; i < (int)multicastGroupRecords.size(); i++)
                        {
                            if ( multicastGroupRecords[i].inUse &&
                                 (multicastGroupRecords[i].iswMcastRecord.mcastStatus == IswStream::McastGroupJoined) &&
                                 ((multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex >= 0x01) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex <= 0xFE)) )
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Joined Multicast Group " << std::to_string(multicastGroupRecords[i].iswMcastRecord.mcastAddress) << std::endl;
                                std::cout.flush();
                            }
                        }

                        //! Modify the group that we joined
                        for (int i = 0; i <  (int)multicastGroupRecords.size(); i++)
                        {
                            if ( multicastGroupRecords[i].inUse &&
                                 (multicastGroupRecords[i].iswMcastRecord.mcastStatus == IswStream::McastGroupJoined) &&
                                 ((multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex >= 0x01) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex <= 0xFE)) )
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " sending Modify Multicast " << std::to_string(multicastGroupRecords[i].iswMcastRecord.mcastAddress) << std::endl;
                                testPhyRate = 2;
                                status = iswStream->SendModifyMulticastGroupCmd(multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex, testPhyRate);
                                break;
                            }
                        }

                        sleep(2);

                        //! Now leave the group that we joined
                        for (int i = 0; i < (int)multicastGroupRecords.size(); i++)
                        {
                            if ( multicastGroupRecords[i].inUse &&
                                 ((multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex >= 0x01) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex <= 0xFE)) &&
                                  (multicastGroupRecords[i].iswMcastRecord.mcastStatus >= IswStream::McastGroupOriginatedAndActive) )
                            {
                                status = iswStream->SendLeaveMulticastGroupCmd(multicastGroupRecords[i].iswMcastRecord.mcastPeerIndex);
                            }
                        }
                        sleep(3);

                        iswStream->GetMulticastGroupRecords(&multicastGroupRecords);
                        sleep(3);

                        //! Now leave the group that we joined
                        for (int i = 0; i < (int)multicastGroupRecords.size(); i++)
                        {
                            if ( multicastGroupRecords[i].inUse &&
                                 (multicastGroupRecords[i].iswMcastRecord.mcastStatus == IswStream::McastGroupJoined) )
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Still Joined to Multicast Group " << std::to_string(multicastGroupRecords[i].iswMcastRecord.mcastAddress) << std::endl;
                                std::cout.flush();
                            }
                            else
                            {
                                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Left Multicast Group " << std::to_string(multicastGroupRecords[i].iswMcastRecord.mcastAddress) << std::endl;
                            }
                        }

                    }
                }
            }
        }

        sleep(2);
    }

    if ( testsToRun & SetFactoryDefault )
    {
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                   IswSystem *iswSystem     = iswInterface->GetIswSystem();
                   IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                   IswFirmware *iswFirmware = iswInterface->GetIswFirmware();

                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Set Factory Defaults" << std::endl;
                   iswSystem->SendSetFactoryDefaultCmd();
                   sleep(5);

                   //! Test - Get identity of locally attached devices
                   //! The should be setup by the library after the IswInterfaces are initialized
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Active Firmware Version = " << iswFirmware->GetActiveFirmwareVersion() << std::endl;
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " MAC Address = " << iswIdentity->GetMacAddressStr() << std::endl;
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Device Type = " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                }
            }
        }
    }

    if ( testsToRun & ResetDevice )
    {
        //! Test Reset the whole device - radio and bus
        //! These tests sometimes cause hotplug events depending on your system
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if ( iswInterface->IsReady() )
            {
                IswSystem *iswSystem = iswInterface->GetIswSystem();
                int status = 0;

                if ( testsToRun & IdleRadio )
                {
                    //! Idle the radio to force disconnect
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Idle Radio" << std::endl;
                    int status = iswSystem->SendIdleRadioCmd();

                    sleep(1);

                    if ( iswSystem->GetIswRadioIdle() == false )
                    {
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Idle Radio Not Set" << std::endl;
                    }
                }

                if ( testsToRun & ResetRadio )
                {
                    //! Reset the radio to "un-idle"
                    std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Reset Radio" << std::endl;
                    int status = iswSystem->SendResetRadioCmd();
                    usleep(10);
                }

                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Reset Device - Radio and Bus - Goodbye!" << std::endl;
                sleep(1);  // for printout
                status = iswSystem->SendResetDeviceCmd();
            }
        }

        sleep(1);
    }

    if ( testsToRun & GetRadioStatus )
    {
        //! Test Get Radio Status Command
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                   IswSystem *iswSystem = iswInterface->GetIswSystem();

                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Get Radio Status" << std::endl;
                   iswSystem->SendGetRadioStatusCmd();
                   sleep(5);

                   std::string radioState              = "";
                   std::string radioChipVersion        = "";
                   std::string radioFirmwareUpdateType = "";

                   switch(iswSystem->GetIswRadioState())
                   {
                       case IswSystem::ACTIVE:
                       {
                           radioState = "Active";
                           break;
                       }
                       case IswSystem::WAITING_CRYPTO_SESSIONS_START:
                       {
                           radioState = "Waiting for start of crypto sessions";
                           break;
                       }
                       case IswSystem::IDLED:
                       {
                           radioState = "Idled";
                           break;
                       }
                       default:
                       {
                           radioState = "Invalid Radio State";
                           break;
                       }
                   }

                   switch(iswSystem->GetIswRadioChipVersion())
                   {
                       case IswSystem::AL5350:
                       {
                           radioChipVersion = "AL5350";
                           break;
                       }
                       case IswSystem::AL5350B:
                       {
                           radioChipVersion = "AL5350B";
                           break;
                       }
                       default:
                       {
                           radioChipVersion = "Invalid Chip Version";
                           break;
                       }
                   }

                   switch(iswSystem->GetRadioFirmwareUpdateType())
                   {
                       case IswSystem::LEGACY:
                       {
                           radioFirmwareUpdateType = "Legacy";
                           break;
                       }
                       case IswSystem::ECDSA_256:
                       {
                           radioFirmwareUpdateType = "ECDSA-256";
                           break;
                       }
                       default:
                       {
                           radioFirmwareUpdateType = "Invalid Firmware Update Type";
                           break;
                       }
                   }

                   //! Test - Get identity of locally attached devices
                   //! The should be setup by the library after the IswInterfaces are initialized
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Radio State = " << radioState << std::endl;
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Chip Version = " << radioChipVersion << std::endl;
                   std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Firmware Update Type = " << radioFirmwareUpdateType << std::endl;
                }
            }
        }
    }

    } //! end else we have devices

    //! Exit libusb
    exitIswTest();
}
