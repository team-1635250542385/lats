QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


HEADERS += \
    ../../../Logger/Logger.h \
    ../../../Logger/DBLogger.h \
    ../../../Logger/DBLogRecord.h \
    ../../../IswInterface/Interface.h \
    ../../../UsbInterface/UsbInterface.h \
    ../../../SerialInterface/SerialInterface.h \
    ../../../OnBoard/OnBoard.h \
    ../../../UsbInterface/UsbInterfaceInit.h \
    ../../../IswInterface/IswFirmware.h \
    ../../../IswInterface/IswSystem.h \
    ../../../IswInterface/IswIdentity.h \
    ../../../IswInterface/IswSecurity.h \
    ../../../IswInterface/IswAssociation.h \
    ../../../IswInterface/IswStream.h \
    ../../../IswInterface/IswLink.h \
    ../../../IswInterface/IswMetrics.h \
    ../../../IswInterface/IswProduct.h \
    ../../../IswInterface/IswSolNet.h \
    ../../../IswInterface/IswInterface.h \
    ../../../IswInterface/IswInterfaceInit.h

SOURCES += \
    ../../../Logger/Logger.cpp \
    ../../../Logger/DBLogger.cpp \
    ../../../Logger/DBLogRecord.cpp \
    ../../../UsbInterface/UsbInterface.cpp \
    ../../../SerialInterface/SerialInterface.cpp \
    ../../../OnBoard/OnBoard.cpp \
    ../../../UsbInterface/UsbInterfaceInit.cpp \
    ../../../IswInterface/IswFirmware.cpp \
    ../../../IswInterface/IswSystem.cpp \
    ../../../IswInterface/IswIdentity.cpp \
    ../../../IswInterface/IswSecurity.cpp \
    ../../../IswInterface/IswAssociation.cpp \
    ../../../IswInterface/IswStream.cpp \
    ../../../IswInterface/IswLink.cpp \
    ../../../IswInterface/IswMetrics.cpp \
    ../../../IswInterface/IswProduct.cpp \
    ../../../IswInterface/IswSolNet.cpp \
    ../../../IswInterface/IswInterface.cpp \
    ../../../IswInterface/IswInterfaceInit.cpp \
    ./main.cpp


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -L/usr/lib64 -lusb-1.0 -lz -lftdi1
