//!######################################################
//! File: main.cpp
//! Description: Used for ISW interface integration tests
//! - Runs through and tests ISW API commands including
//!   SolNet, loading new firmware.  See the other
//!   integration tests for those features.
//! - Some sections are commented out in order to have a
//!   quick association test.  Uncomment sections you want
//!   to test and rebuild.
//!######################################################

#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <string.h>
#include "../../../Logger/Logger.h"
#include "../../../Logger/DBLogger.h"
#include "../../../UsbInterface/UsbInterfaceInit.h"
#include "../../../UsbInterface/UsbInterface.h"
#include "../../../IswInterface/IswInterfaceInit.h"
#include "../../../IswInterface/IswInterface.h"


IswInterfaceInit *iswInterfaceInit                = nullptr;
std::array<IswInterface *, 21> *iswInterfaceArray = nullptr;
Logger *theLogger = nullptr;

pthread_t thread_isw;
pthread_t thread_dequeue;

struct arg_struct {
    Logger* logger;
    DBLogger *dblogger;
    int threadNo;
};

int stopIssued = 0;
pthread_mutex_t stopMutex;

uint32_t videoServiceSelector = 0;

int getStopIssued(void)
{
  int ret = 0;
  pthread_mutex_lock(&stopMutex);
  ret = stopIssued;
  pthread_mutex_unlock(&stopMutex);
  return ret;
}

void setStopIssued(int val)
{
  pthread_mutex_lock(&stopMutex);
  stopIssued = val;
  pthread_mutex_unlock(&stopMutex);
}

void *dequeue_messages(void *arguments)
{
    struct arg_struct *args = (struct arg_struct *)arguments;
    Logger *theLogger = args->logger;

    while ( getStopIssued() != 1 )
    {
        //! If items on queue - dequeue and print
        theLogger->deQueue();
        usleep(100);  //! 100 millisecs
    }
}

void initIswTest(Logger *logger, DBLogger *dblogger)
{
    iswInterfaceInit = new IswInterfaceInit(IswInterface::USB, logger, dblogger);
    iswInterfaceArray = iswInterfaceInit->InitIswInterfaceArray();
}

void exitIswTest(void)
{
    if (iswInterfaceInit != nullptr)
    {
        iswInterfaceInit->ExitIswInterfaceArray();
        delete(iswInterfaceInit);
    }
    setStopIssued(1);
}

void AddControlServiceDescriptor(IswSolNet *iswSolNet)
{
    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[IswSolNet::payloadMsgBufferSize];

    // Clear the buffer
    memset(deviceChildblock, 0, IswSolNet::payloadMsgBufferSize);

    // offset into childblock for copying data
    uint32_t offset = 0;

    // Create the service descriptors
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    serviceEntry.serviceControlDesc.header.length       = IswSolNet::selectorSize + sizeof(serviceEntry.serviceControlDesc.protocolId) + sizeof(serviceEntry.serviceControlDesc.reserved);
    serviceEntry.serviceControlDesc.header.descriptorId = IswSolNet::Control;

    // Set some defaults
    serviceEntry.serviceControlDesc.serviceSelector = 0;
    serviceEntry.serviceControlDesc.childBlock      = nullptr;

    // Endpoint Child Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    endpointDesc.dataflowId           = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointId           = 1;
    endpointDesc.endpointDistribution = 0;
    endpointDesc.dataPolicies         = IswSolNet::HiDiscard;
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(IswSolNet::iswSolNetPropertyEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId                         = endpointDesc.dataflowId;
    serviceEntry.endpointId                         = endpointDesc.endpointId;
    serviceEntry.endpointDistribution               = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies                       = endpointDesc.dataPolicies;
    serviceEntry.serviceControlDesc.serviceSelector = iswSolNet->GetNextServiceNo();
    serviceEntry.serviceControlDesc.protocolId      = 0;
    serviceEntry.serviceControlDesc.reserved        = 0;

    //! Device Name Child Descriptor
    std::string deviceNameString = "Device Number ";
    deviceNameString.append(std::to_string(iswSolNet->GetIndex()));
    if ( !deviceNameString.empty() )
    {
        IswSolNet::iswSolNetPropertyDeviceDesc devNameDesc;
        memset(&devNameDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
        devNameDesc.header.descriptorId = IswSolNet::DeviceName;
        memcpy(devNameDesc.devString, deviceNameString.c_str(), deviceNameString.size());

        //! String must be multiple of 4 bytes
        devNameDesc.header.length = deviceNameString.size();

        //! Length should be divisible by 4
        while ( (devNameDesc.header.length % 4) > 0 )
        {
            devNameDesc.header.length++;
        }

        //! Add Device Name Property Descriptor
        if ( offset < IswSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devNameDesc, (IswSolNet::commonHeaderSize + devNameDesc.header.length));
            offset += IswSolNet::commonHeaderSize + devNameDesc.header.length;
        }
    }

    //! Device Serial Number Child Descriptor
    std::string deviceSerialNoString = "123456";
    if ( !deviceSerialNoString.empty() )
    {
        IswSolNet::iswSolNetPropertyDeviceDesc devSerialNoDesc;
        memset(&devSerialNoDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
        memset(devSerialNoDesc.devString, 0, 256);
        devSerialNoDesc.header.descriptorId = IswSolNet::DeviceSerialNumber;
        memcpy(devSerialNoDesc.devString, deviceSerialNoString.c_str(), deviceSerialNoString.size());

        //! String must be multiple of 4 bytes
        devSerialNoDesc.header.length = deviceSerialNoString.size();
        while ( (devSerialNoDesc.header.length % 4) > 0 )
        {
            devSerialNoDesc.header.length++;
        }

        //! Add Device Serial Number Property Descriptor
        if ( offset < IswSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devSerialNoDesc, (IswSolNet::commonHeaderSize + devSerialNoDesc.header.length));
            offset += IswSolNet::commonHeaderSize + devSerialNoDesc.header.length;
        }
    }

    //! Device Manufacturer Child Descriptor
    std::string deviceManufacturerString = "Army101";
    if ( !deviceManufacturerString.empty() )
    {
        IswSolNet::iswSolNetPropertyDeviceDesc devManufacturerDesc;
        memset(&devManufacturerDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
        devManufacturerDesc.header.descriptorId = IswSolNet::DeviceManufacturer;
        memcpy(devManufacturerDesc.devString, deviceManufacturerString.c_str(), deviceManufacturerString.size());

        //! String must be multiple of 4 bytes
        devManufacturerDesc.header.length = deviceManufacturerString.size();
        while ( (devManufacturerDesc.header.length % 4) > 0 )
        {
            devManufacturerDesc.header.length++;
        }

        //! Add Device Manufacturer Property Descriptor
        if ( offset < IswSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devManufacturerDesc, (IswSolNet::commonHeaderSize + devManufacturerDesc.header.length));
            offset += IswSolNet::commonHeaderSize + devManufacturerDesc.header.length;
        }
    }

    //! Device Friendly Name Descriptor
    std::string deviceFriendlyNameString = "Dev";
    deviceFriendlyNameString.append(std::to_string(iswSolNet->GetIndex()));
    if ( !deviceFriendlyNameString.empty() )
    {
        IswSolNet::iswSolNetPropertyDeviceDesc devFriendlyNameDesc;
        memset(&devFriendlyNameDesc, 0, sizeof(IswSolNet::iswSolNetPropertyDeviceDesc));
        devFriendlyNameDesc.header.descriptorId = IswSolNet::DeviceFriendlyName;
        memcpy(devFriendlyNameDesc.devString, deviceFriendlyNameString.c_str(), deviceFriendlyNameString.size());

        //! String must be multiple of 4 bytes
        devFriendlyNameDesc.header.length = deviceFriendlyNameString.size();
        while ( (devFriendlyNameDesc.header.length % 4) > 0 )
        {
            devFriendlyNameDesc.header.length++;
        }

        //! Add Device Friendly Name Property Descriptor
        if ( offset < IswSolNet::payloadMsgBufferSize )
        {
            memcpy(&deviceChildblock[offset], &devFriendlyNameDesc, (IswSolNet::commonHeaderSize + devFriendlyNameDesc.header.length));
            offset += IswSolNet::commonHeaderSize + devFriendlyNameDesc.header.length;
        }
    }

    serviceEntry.serviceControlDesc.header.length += offset;
    serviceEntry.serviceControlDesc.childBlock = &deviceChildblock[0];

    // Add this Control service descriptor with all the child property descriptors
    // to the IswSolNet->iswSolNetServices vector
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);
}

void AddVideoDescriptor(IswSolNet *iswSolNet)
{
    // Create the service descriptors
    IswSolNet::iswSolNetServiceEntry serviceEntry;
    // Start with header size and add child descriptor lengths later
    // Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));

    // Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[IswSolNet::payloadMsgBufferSize];
    // Clear the buffer
    memset(deviceChildblock, 0, IswSolNet::payloadMsgBufferSize);
    // offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Video - with Device child descriptors
    // *****************************************
    // Video Class Descriptor - with child descriptors
    // Video Format Descriptor
    // Video Sample Descriptor
    // Video Protocol Raw Descriptor
    // Video IFOV Descriptor
    // Video Principal Point Descriptor
    // Image Sensor Type Descriptor
    // Image Sensor Waveband Descriptor
    // *****************************************

    // Add all the property descriptors to the child block
    serviceEntry.serviceDesc.header.length = IswSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = IswSolNet::Video;

    // Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    // Endpoint Child Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId = IswSolNet::Endpoint;
    endpointDesc.endpointId = 1;
    endpointDesc.dataflowId = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0;
    endpointDesc.dataPolicies = IswSolNet::HiNoDiscard;
    // so copy in this EndpointDesc behind the rest of the child descriptors
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(IswSolNet::iswSolNetPropertyEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyEndpointDesc);

    // Application sets these per device/app
    serviceEntry.dataflowId = endpointDesc.dataflowId;
    serviceEntry.endpointId = endpointDesc.endpointId;
    serviceEntry.endpointDistribution = endpointDesc.endpointDistribution;
    serviceEntry.dataPolicies = endpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();

    videoServiceSelector = serviceEntry.serviceDesc.serviceSelector;

    // Get the other Video Child descriptors

    // Video Label Descriptor
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    std::string labelText = "ISW Video";
    memcpy(&labelDesc.textLabel, labelText.c_str(), labelText.size());
    labelDesc.header.descriptorId = IswSolNet::Label;
    labelDesc.header.length = labelText.size();
    while ( (labelDesc.header.length % 4) > 0 )
    {
        labelDesc.header.length++;
    }

    // Add Label Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
        offset += IswSolNet::commonHeaderSize + labelDesc.header.length;
    }

    // Video Format Descriptor
    IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc videoOutputFormatRawDesc;
    memset(&videoOutputFormatRawDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc));
    videoOutputFormatRawDesc.resX                = 640;
    videoOutputFormatRawDesc.resY                = 480;
    videoOutputFormatRawDesc.frameRate           = 30.0;
    videoOutputFormatRawDesc.bitDepth            = 8;
    videoOutputFormatRawDesc.pixelformat         = 0x01;
    videoOutputFormatRawDesc.header.descriptorId = IswSolNet::VideoOutputFormatRaw;
    videoOutputFormatRawDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc)
            - IswSolNet::commonHeaderSize;

    //! Add the Video Format  Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoOutputFormatRawDesc, (IswSolNet::commonHeaderSize + videoOutputFormatRawDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoOutputFormatRawDesc.header.length;
    }

    //! Video Format Descriptor
    IswSolNet::iswSolNetPropertyVideoFormatDesc videoFormatDesc;
    memset(&videoFormatDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoFormatDesc));
    videoFormatDesc.videoFormatX        = 640;
    videoFormatDesc.videoFormatY        = 480;
    videoFormatDesc.frameRate           = 30.0;
    videoFormatDesc.formatOptions       = 0x00;
    videoFormatDesc.encodingFormat      = 0x0000;
    videoFormatDesc.header.descriptorId = IswSolNet::VideoFormat;
    videoFormatDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoFormatDesc) - IswSolNet::commonHeaderSize;

    //! Add the Video Format  Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoFormatDesc, (IswSolNet::commonHeaderSize + videoFormatDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoFormatDesc.header.length;
    }

    //! Video Sample Descriptor
    IswSolNet::iswSolNetPropertyVideoSampleFormatDesc videoSampleDesc;
    memset(&videoSampleDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoSampleFormatDesc));
    videoSampleDesc.bitDepth            = 0x0008;
    videoSampleDesc.pixelFormat         = 0x0001;
    videoSampleDesc.header.descriptorId = IswSolNet::VideoSampleFormat;
    videoSampleDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoSampleFormatDesc)
            - IswSolNet::commonHeaderSize;

    //! Add the Video Sample Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoSampleDesc, (IswSolNet::commonHeaderSize + videoSampleDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoSampleDesc.header.length;
    }

    // Video Protocol Raw Descriptor
    IswSolNet::iswSolNetPropertyVideoProtocolDesc videoProtocolDesc;
    memset(&videoProtocolDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoProtocolDesc));
    videoProtocolDesc.protocol            = 0x0001;
    videoProtocolDesc.header.descriptorId = IswSolNet::Protocol;
    videoProtocolDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoProtocolDesc)
            - IswSolNet::commonHeaderSize;

    //! Add the Video Protocol Raw Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoProtocolDesc, (IswSolNet::commonHeaderSize + videoProtocolDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoProtocolDesc.header.length;
    }

    //! Video IFOV Descriptor
    IswSolNet::iswSolNetPropertyVideoIfovDesc videoIfovDesc;
    memset(&videoIfovDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoIfovDesc));
    videoIfovDesc.IFoV                = (float)0.5;
    videoIfovDesc.header.descriptorId = IswSolNet::IFOV;
    videoIfovDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoIfovDesc) - IswSolNet::commonHeaderSize;

    //! Add the Video IFOV Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoIfovDesc, (IswSolNet::commonHeaderSize + videoIfovDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoIfovDesc.header.length;
    }

    //! Video Principal Point Descriptor
    IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc videoPrincipalPointDesc;
    memset(&videoPrincipalPointDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc));
    videoPrincipalPointDesc.principalPointX     = 320;
    videoPrincipalPointDesc.principalPointY     = 240;
    videoPrincipalPointDesc.header.descriptorId = IswSolNet::PrincipalPoint;
    videoPrincipalPointDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc)
            - IswSolNet::commonHeaderSize;

    //! Add the Video Principal Point Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &videoPrincipalPointDesc, (IswSolNet::commonHeaderSize + videoPrincipalPointDesc.header.length));
        offset += IswSolNet::commonHeaderSize + videoPrincipalPointDesc.header.length;
    }

    //! Video Image Sensor Type Descriptor
    IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc imageSensorTypeDesc;
    memset(&imageSensorTypeDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc));
    imageSensorTypeDesc.type = 0x0002;
    imageSensorTypeDesc.header.descriptorId = IswSolNet::ImageSensorType;
    imageSensorTypeDesc.header.length = sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc) - IswSolNet::commonHeaderSize;

    //! Add the Video Image Sensor Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &imageSensorTypeDesc, (IswSolNet::commonHeaderSize + imageSensorTypeDesc.header.length));
        offset += IswSolNet::commonHeaderSize + imageSensorTypeDesc.header.length;
    }

    //! Video Image Sensor Waveband Descriptor
    IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc imageSensorWavebandDesc;
    memset(&imageSensorWavebandDesc, 0, sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc));
    imageSensorWavebandDesc.waveband            = 0x0002;
    imageSensorWavebandDesc.header.descriptorId = IswSolNet::ImageSensorWaveband;
    imageSensorWavebandDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc)
            - IswSolNet::commonHeaderSize;

    //! Add the Image Sensor Waveband Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &imageSensorWavebandDesc, (IswSolNet::commonHeaderSize + imageSensorWavebandDesc.header.length));
        offset += IswSolNet::commonHeaderSize + imageSensorWavebandDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    //! Add this Video service descriptor with all the child property descriptors
    //! to the IswSolNet->iswSolNetServices vector
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);
}

void AddRtaDescriptor(IswSolNet *iswSolNet)
{
    //! Create the service descriptors
    IswSolNet::iswSolNetServiceEntry serviceEntry;

    //! Start with header size and add child descriptor lengths later
    //! Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));

    //! Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[IswSolNet::payloadMsgBufferSize];

    //! Clear buffer
    memset(deviceChildblock, 0, IswSolNet::payloadMsgBufferSize);

    //! offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // RTA - with
    // Associated Service Descriptor
    // Control Endpoint Descriptor
    // Data Endpoint Descriptor
    // Text Label
    // RTA IMU rate
    // *****************************************
    serviceEntry.serviceDesc.header.length       = IswSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = IswSolNet::RTA;

    //! Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock      = nullptr;

    //! Associated Service Descriptor
    IswSolNet::iswSolNetPropertyAssociatedServiceDesc associatedServiceDesc;
    associatedServiceDesc.header.length             = sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc) - IswSolNet::commonHeaderSize;
    associatedServiceDesc.header.descriptorId       = IswSolNet::AssociatedService;
    associatedServiceDesc.associatedServiceId       = 0x0001;
    associatedServiceDesc.associationType           = 0x00;
    associatedServiceDesc.reserved                  = 0x00;
    associatedServiceDesc.associatedServiceSelector = iswSolNet->GetNextServiceNo(); // For Testing Purposes Only
    // Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &associatedServiceDesc, sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc);

    //! Control Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyRtaControlEndpointDesc controlEndpointDesc;
    controlEndpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyRtaControlEndpointDesc) - IswSolNet::commonHeaderSize;
    controlEndpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    controlEndpointDesc.endpointId           = 1;
    controlEndpointDesc.dataflowId           = iswSolNet->GetNextDataflowId();
    controlEndpointDesc.endpointDistribution = 0; // Unicast
    controlEndpointDesc.dataPolicies         = 0x09;

    //! Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &controlEndpointDesc, sizeof(IswSolNet::iswSolNetPropertyRtaControlEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyRtaControlEndpointDesc);

    //! Data Endpoint Property Descriptor
    IswSolNet::iswSolNetPropertyRtaDataEndpointDesc dataEndpointDesc;
    dataEndpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyRtaDataEndpointDesc) - IswSolNet::commonHeaderSize;
    dataEndpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    dataEndpointDesc.endpointId           = 1;
    dataEndpointDesc.dataflowId           = iswSolNet->GetNextDataflowId();
    dataEndpointDesc.endpointDistribution = 0; // Unicast
    dataEndpointDesc.dataPolicies         = 0x05;

    //! Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &dataEndpointDesc, sizeof(IswSolNet::iswSolNetPropertyRtaDataEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyRtaDataEndpointDesc);

    //! RTA IMU Rate Property Descriptor
    IswSolNet::iswSolNetPropertyImuOutputRateDesc rtaImuRateDesc;
    rtaImuRateDesc.header.length       = sizeof(IswSolNet::iswSolNetPropertyImuOutputRateDesc) - IswSolNet::commonHeaderSize;
    rtaImuRateDesc.header.descriptorId = IswSolNet::ImuOutputRate;
    rtaImuRateDesc.rate                = 120;

    //! Add the Rta Imu Rate Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &rtaImuRateDesc, (IswSolNet::commonHeaderSize + rtaImuRateDesc.header.length));
        offset += IswSolNet::commonHeaderSize + rtaImuRateDesc.header.length;
    }

    //! Application sets these per device/app
    serviceEntry.dataflowId                  = controlEndpointDesc.dataflowId;
    serviceEntry.endpointId                  = controlEndpointDesc.endpointId;
    serviceEntry.endpointDistribution        = controlEndpointDesc.endpointDistribution;
    serviceEntry.dataPolicies                = controlEndpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();

    //! RTA Label Descriptor
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    std::string labelText = "ISW RTA";
    memcpy(&labelDesc.textLabel, labelText.c_str(), labelText.size());
    labelDesc.header.descriptorId = IswSolNet::Label;
    labelDesc.header.length       = labelText.size();
    while ( (labelDesc.header.length % 4) > 0 )
    {
        labelDesc.header.length++;
    }

    //! Add Label Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
        offset += IswSolNet::commonHeaderSize + labelDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    //! Add the RTA service to the services array for this device
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);
}

void AddMotionSenseDescriptor(IswSolNet *iswSolNet)
{
    //! Create the service descriptors
    IswSolNet::iswSolNetServiceEntry serviceEntry;

    //! Start with header size and add child descriptor lengths later
    //! Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));

    //! Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[IswSolNet::payloadMsgBufferSize];

    //! Clear the buffer
    memset(deviceChildblock, 0, IswSolNet::payloadMsgBufferSize);

    //!! offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Motion Sense - with Device child descriptors
    // *****************************************
    // Motion Sense Class Descriptor - with child descriptors
    // Text Label
    // Endpoint Descriptor
    // Motion Sensor Rate Descriptor
    // Association Property Descriptor
    // Motion Sensor Accelerometer Property Descriptor
    // Motion Sensor Gyroscope Property Descriptor
    // Motion Sensor Magenetometer Property Descriptor
    // Motion Sensor Axes Property Descriptor
    // *****************************************

    //! Add all the property descriptors to the child block
    serviceEntry.serviceDesc.header.length = IswSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = IswSolNet::MotionSense;

    //! Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock = nullptr;

    //! Motion Sense Label Descriptor
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    std::string labelText = "ISW Motion Sense";
    memcpy(&labelDesc.textLabel, labelText.c_str(), labelText.size());
    labelDesc.header.descriptorId = IswSolNet::Label;
    labelDesc.header.length = labelText.size();
    while ( (labelDesc.header.length % 4) > 0 )
    {
        labelDesc.header.length++;
    }

    //! Add Label Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
        offset += IswSolNet::commonHeaderSize + labelDesc.header.length;
    }

    //! Motion Sense Endpoint Descriptor
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    endpointDesc.endpointId           = 1;
    endpointDesc.dataflowId           = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0; // Unicast
    endpointDesc.dataPolicies         = 0x05;

    //! so copy in this EndpointDesc behind the rest of the child descriptors
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(IswSolNet::iswSolNetPropertyEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyEndpointDesc);

    //! Motion Sense Rate Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorRateDesc motionSensorRate;
    motionSensorRate.header.length       = sizeof(IswSolNet::iswSolNetPropertyMotionSensorRateDesc) - IswSolNet::commonHeaderSize;
    motionSensorRate.header.descriptorId = IswSolNet::MotionSensorRate;
    motionSensorRate.rate                = 120;
    memcpy(&deviceChildblock[offset], &motionSensorRate, sizeof(IswSolNet::iswSolNetPropertyMotionSensorRateDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyMotionSensorRateDesc);

    //! Association Descriptor
    IswSolNet::iswSolNetPropertyAssociationDesc associationDesc;
    associationDesc.header.length             = sizeof(IswSolNet::iswSolNetPropertyAssociationDesc) - IswSolNet::commonHeaderSize;
    associationDesc.header.descriptorId       = IswSolNet::AssociatedService;
    associationDesc.associatedServiceId       = 0x0001;
    associationDesc.associationType           = 0x01; // The association is a sibling to the Video Service
    associationDesc.reserved1                 = 0x00;
    associationDesc.associatedServiceSelector = videoServiceSelector;

    //! Add the Association Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &associationDesc, (IswSolNet::commonHeaderSize + associationDesc.header.length));
        offset += IswSolNet::commonHeaderSize + associationDesc.header.length;
    }

    //! Motion Sensor Axes Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorAxes motionSensorAxes;
    motionSensorAxes.header.length       = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAxes) - IswSolNet::commonHeaderSize;
    motionSensorAxes.header.descriptorId = IswSolNet::MotionSensorAxes;
    motionSensorAxes.numberOfAxes        = 3;
    motionSensorAxes.reserved[0]         = 0x00000;
    motionSensorAxes.reserved[1]         = 0x00000;
    motionSensorAxes.reserved[2]         = 0x00000;

    //! Motion Sensor Accelerometer Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorAccel motionSensorAccel;
    motionSensorAccel.header.length       = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAccel);
    motionSensorAccel.header.descriptorId = IswSolNet::MOTION_SENSE_ACCEL;
    memset(&motionSensorAccel.childBlock, 0, sizeof(motionSensorAccel.childBlock));
    memcpy(&motionSensorAccel.childBlock, &motionSensorAxes, sizeof(motionSensorAxes));

    //! Add the Motion Sensor Accelerometer Property Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &motionSensorAccel, (IswSolNet::commonHeaderSize + motionSensorAccel.header.length));
        offset += IswSolNet::commonHeaderSize + motionSensorAccel.header.length;
    }

    //! Motion Sensor Gyro Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorGyro motionSensorGyro;
    motionSensorGyro.header.length       = sizeof(IswSolNet::iswSolNetPropertyMotionSensorGyro);
    motionSensorGyro.header.descriptorId = IswSolNet::MOTION_SENSE_GYRO;
    memset(&motionSensorGyro.childBlock, 0, sizeof(motionSensorGyro.childBlock));
    memcpy(&motionSensorGyro.childBlock, &motionSensorAxes, sizeof(motionSensorAxes));

    //! Add the Motion Sensor Gyroscope Property Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &motionSensorGyro, (IswSolNet::commonHeaderSize + motionSensorGyro.header.length));
        offset += IswSolNet::commonHeaderSize + motionSensorGyro.header.length;
    }

    //! Motion Sensor Magnetometer Property Descriptor
    IswSolNet::iswSolNetPropertyMotionSensorMag motionSensorMag;
    motionSensorMag.header.length       = sizeof(IswSolNet::iswSolNetPropertyMotionSensorMag);
    motionSensorMag.header.descriptorId = IswSolNet::MOTION_SENSE_MAG;
    memset(&motionSensorMag.childBlock, 0, sizeof(motionSensorMag.childBlock));
    memcpy(&motionSensorMag.childBlock, &motionSensorAxes, sizeof(motionSensorAxes));

    //! Add the Motion Sensor Magnetometer Property Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &motionSensorMag, (IswSolNet::commonHeaderSize + motionSensorMag.header.length));
        offset += IswSolNet::commonHeaderSize + motionSensorMag.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    //! Add the Motion Sense service to the services array for this device
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);
}

void AddStatusDescriptor(IswSolNet *iswSolNet)
{
    //! Create the service descriptors
    IswSolNet::iswSolNetServiceEntry serviceEntry;

    //! Start with header size and add child descriptor lengths later
    //! Clear serviceEntry
    memset(&serviceEntry, 0, sizeof(IswSolNet::iswSolNetServiceEntry));

    //! Allocate a childblock for child or property descriptors
    uint8_t deviceChildblock[IswSolNet::payloadMsgBufferSize];

    //! Clear buffer
    memset(deviceChildblock, 0, IswSolNet::payloadMsgBufferSize);

    //! offset into childblock for copying data
    uint32_t offset = 0;

    // *****************************************
    // Status - with
    // Endpoint Descriptor
    // Text Label
    // *****************************************
    serviceEntry.serviceDesc.header.length       = IswSolNet::selectorSize;
    serviceEntry.serviceDesc.header.descriptorId = IswSolNet::Status;

    //! Set some defaults
    serviceEntry.serviceDesc.serviceSelector = 0;
    serviceEntry.serviceDesc.childBlock      = nullptr;

    //! Endpoint Child Descriptor for unicast
    IswSolNet::iswSolNetPropertyEndpointDesc endpointDesc;
    endpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    endpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    endpointDesc.endpointId           = 1;
    endpointDesc.dataflowId           = iswSolNet->GetNextDataflowId();
    endpointDesc.endpointDistribution = 0;
    endpointDesc.dataPolicies         = IswSolNet::HiNoDiscard;

    //! Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &endpointDesc, sizeof(IswSolNet::iswSolNetPropertyEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyEndpointDesc);

    //! Endpoint Child Descriptor for broadcast
    IswSolNet::iswSolNetPropertyEndpointDesc bcastEndpointDesc;
    bcastEndpointDesc.header.length        = sizeof(IswSolNet::iswSolNetPropertyEndpointDesc) - IswSolNet::commonHeaderSize;
    bcastEndpointDesc.header.descriptorId  = IswSolNet::Endpoint;
    bcastEndpointDesc.endpointId           = 0;
    bcastEndpointDesc.dataflowId           = endpointDesc.dataflowId;  // Use the same dataflowId
    bcastEndpointDesc.endpointDistribution = 0;
    bcastEndpointDesc.dataPolicies         = IswSolNet::HiNoDiscard;

    //! Total length of data in childblock area
    memcpy(&deviceChildblock[offset], &bcastEndpointDesc, sizeof(IswSolNet::iswSolNetPropertyEndpointDesc));
    offset += sizeof(IswSolNet::iswSolNetPropertyEndpointDesc);

    //! Set for broadcast. DataflowId is the same for unicast and broadcast and is the index into the service array
    serviceEntry.dataflowId                  = endpointDesc.dataflowId;
    serviceEntry.endpointId                  = bcastEndpointDesc.endpointId;
    serviceEntry.endpointDistribution        = bcastEndpointDesc.endpointDistribution;
    serviceEntry.dataPolicies                = bcastEndpointDesc.dataPolicies;
    serviceEntry.serviceDesc.serviceSelector = iswSolNet->GetNextServiceNo();

    //! Status Label Descriptor
    IswSolNet::iswSolNetPropertyTextLabelDesc labelDesc;
    memset(&labelDesc.textLabel, 0, IswSolNet::SIZE_TEXT_LABEL);
    std::string labelText = "ISW Status";
    memcpy(&labelDesc.textLabel, labelText.c_str(), labelText.size());
    labelDesc.header.descriptorId = IswSolNet::Label;
    labelDesc.header.length       = labelText.size();
    while ( (labelDesc.header.length % 4) > 0 )
    {
        labelDesc.header.length++;
    }

    //! Add Label Descriptor
    if ( offset < IswSolNet::payloadMsgBufferSize )
    {
        memcpy(&deviceChildblock[offset], &labelDesc, (IswSolNet::commonHeaderSize + labelDesc.header.length));
        offset += IswSolNet::commonHeaderSize + labelDesc.header.length;
    }

    serviceEntry.serviceDesc.header.length += offset;
    serviceEntry.serviceDesc.childBlock = &deviceChildblock[0];

    //! Add the Status service to the services array for this device
    iswSolNet->AddSolNetService(IswSolNet::defaultPeerIndex, &serviceEntry);
}

/********************************************************
 *
 *  Big Note: Uncomment sections below to test various
 *
 *******************************************************/
void *testSolNetMessages(void *arguments)
{
    /********************************************************
     *
     *  Init and enumeration for tests - has to be done
     *
     * ******************************************************/
    struct arg_struct *args = (struct arg_struct *)arguments;

    //! Init libusb and the UsbInterface class
    initIswTest(args->logger, args->dblogger);

    //! Give time for enumeration
    sleep(1); //!seconds

    /********************************************************
     *
     *  Check for devices and list attributes
     *
     * ******************************************************/

    //! Check that we have devices
    int iswDeviceCount = 0;
    for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
    {
        IswInterface *iswInterface = *iterator;
        if ( (iswInterface != nullptr) && (iswInterface->IsReady()) )
        {
            iswDeviceCount++;

            if ( !iswInterface->GetUsbInterface()->IsDeviceOpen() )
            {
                std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Is Not Open. Are You Root?" << std::endl;
                iswDeviceCount--;
            }
        }
    }

    if ( iswDeviceCount <= 0 )
    {
        //! No devices
        std::cout << "No ISW Devices Present!" << std::endl;
        setStopIssued(1);
    }
    else
    {
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            if ( *iterator != nullptr )
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    UsbInterface *usbInterface = iswInterface->GetUsbInterface();

                    if ( (usbInterface == nullptr) ||
                            (usbInterface->IsDeviceOpen() == false) )
                    {
                     std::cout << "Usb Interface Not Open" << std::endl;
                        continue;
                    }
                    else
                    {
                        IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                        IswFirmware *iswFirmware = iswInterface->GetIswFirmware();
                        IswProduct *iswProduct   = iswInterface->GetIswProduct();

                        //! Test - Get identity of locally attached devices
                        //! The should be setup by the library after the IswInterfaces are initialized
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Active Firmware Version = " << iswFirmware->GetActiveFirmwareVersion() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " MAC Address = " << iswIdentity->GetMacAddressStr() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Device Type = " << std::hex << (int)iswIdentity->GetIswDeviceType() << std::endl;
                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Network ID = " << std::to_string(iswProduct->GetIswNetworkId()) << std::endl;
                    }
                }
           }
        }

        /********************************************************
         *
         *  Sprint 6 - Verify that there are no services at the
         *             start of the test.
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();
                    iswSolNet->PrintServicesToLogfile();
                }
            }
        }

        /********************************************************
         *
         *  Sprint 6 - SolNet - Add some services to each node
         *
         * ******************************************************/
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++)
        {
            if (*iterator != nullptr)
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    //! Add Status Service Descriptor
                    AddStatusDescriptor(iswSolNet);

                    //! Add Video Service Descriptor
                    AddVideoDescriptor(iswSolNet);

                    //! Add RTA Service Descriptor
                    AddRtaDescriptor(iswSolNet);

                    //! Add Motion Sense Service Descriptor
                    AddMotionSenseDescriptor(iswSolNet);
                }
            }
            usleep(100);
        }

        sleep(3);

        /********************************************************
         *
         *  Sprint 6 - SolNet - Send Browse Message
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {

                    IswStream *iswStream = iswInterface->GetIswStream();
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    std::array<IswStream::iswPeerRecord, MAX_PEERS> peers;
                    iswStream->GetPeers(&peers);

                    uint8_t peerIndex = 0;
                    bool sendBrowse   = false;

                    for (uint8_t i = 0; i < peers.size(); i++)
                    {
                        peerIndex  = i;
                        sendBrowse = false;

                        if ( peers[i].inUse )
                        {
                            switch( peers[i].recordVersion )
                            {
                                case 1:
                                {
                                    if ( peers[i].iswPeerRecord1.linkStatus >= 1 )
                                    {
                                        sendBrowse = true;
                                    }
                                    break;
                                }
                                case 2:
                                {
                                    if ( peers[i].iswPeerRecord2.linkStatus >= 1 )
                                    {
                                        sendBrowse = true;
                                    }
                                    break;
                                }
                                case 3:
                                {
                                    if ( peers[i].iswPeerRecord3.linkStatus >= 1 )
                                    {
                                        sendBrowse = true;
                                    }
                                    break;
                                }
                                case 4:
                                {
                                    if ( peers[i].iswPeerRecord4.linkStatus >= 1 )
                                    {
                                        sendBrowse = true;
                                    }
                                    break;
                                }
                                default:
                                {
                                    break;
                                }
                            }

                            if ( sendBrowse )
                            {
                                iswSolNet->SetBrowse(peerIndex, true);
                            }
                            else
                            {
                                if ( iswInterface->theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    std::string msgString = "Peer Not Connected - Not Sending SolNet Browse Packet";
                                    iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                                }
                            }
                        }
                    }

                }
            }
        }

        /********************************************************
         *
         *  Sprint 6 - SolNet - Library sends Browse and Advertise
         *              when the flags are set to true for the peer.
         *              Wait a bit for all the devices to update
         *              their services.
         *
         ********************************************************/

        uint8_t count = 5;

        while ( --count > 0 )
        {
            std::cout << "Waiting For Peers To Browse and Get New Services..." << std::endl;
            sleep(5);
        }

#if 1
        /********************************************************
         *
         *  Sprint 6 - SolNet - SendGetStatusRequestMessage to
         *                      each peer from a device
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    std::array<IswSolNet::iswSolNetPeerServicesEntry, MAX_PEERS> *peerServices = iswSolNet->GetPeerServices();

                    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
                    {
                        IswSolNet::iswSolNetPeerServicesEntry *peerServiceEntry = &((*peerServices)[peerIndex]);

                        if ( peerServiceEntry->inUse )
                        {
                            for (uint8_t dataflowId = 0; dataflowId < IswSolNet::MAX_NUMBER_APPS; dataflowId++)
                            {
                                if ( peerServiceEntry->services[dataflowId].inUse )
                                {
                                    //! Set the status to RegStatusAvailableToRegister
                                    iswSolNet->SetRegistrationStatus(peerIndex,
                                                                     peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector,
                                                                     IswSolNet::RegStatusAvailableToRegister);

                                    sleep(1);

                                    iswSolNet->SendGetStatusRequestMessage(peerIndex,
                                                                           peerServiceEntry->services[dataflowId].serviceDesc.serviceSelector);
                                }
                            }
                        }
                    }
                }
            }
        }

        // Wait for GetStatusResponse messages
        sleep(5);
#endif

        sleep(3);

        /********************************************************
         *
         *  Sprint 6 - SolNet - Have all devices register
         *                      for the Video or Status service of
         *                      all peers
         *
         * ******************************************************/
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++)
        {
            if (*iterator != nullptr)
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    std::array<IswSolNet::iswSolNetPeerServicesEntry, MAX_PEERS> *peerServices = iswSolNet->GetPeerServices();

                    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
                    {
                        IswSolNet::iswSolNetPeerServicesEntry *entry = &(*peerServices)[peerIndex];

                        if ( !entry->inUse )
                        {
                            continue;
                        }

                        for (uint8_t dataflowId = 0; dataflowId < IswSolNet::MAX_NUMBER_APPS; dataflowId++)
                        {
                            if ( entry->services[dataflowId].inUse )
                            {
                                if ( ((entry->services[dataflowId].serviceDesc.header.descriptorId == IswSolNet::Video ) ||
                                      (entry->services[dataflowId].serviceDesc.header.descriptorId == IswSolNet::Status )) &&
                                      ((entry->services[dataflowId].status == IswSolNet::RegStatusAvailableToRegister) ||
                                       (entry->services[dataflowId].status == IswSolNet::RegStatusAvailableForYourUseOnly) ||
                                       (entry->services[dataflowId].status == IswSolNet::RegStatusAvailableForUseYouAndOthers)) )
                                {
                                    // Get the service we want to register for
                                    uint32_t serviceSelector = entry->services[dataflowId].serviceDesc.serviceSelector;

                                    // autonomy: 0 = wait to be polled; 1 = send data automatically to registered node(s)

                                    // Not Polled test
                                    uint8_t autonomy = 1;  // 0 = polled test; 1 = not polled test
                                    iswSolNet->SendRegisterRequestMessage(peerIndex, serviceSelector, autonomy);

                                    // Wait for the RegisterResponse to update ImRegistered flag
                                    // Verify that we are registered with this peer for this service
                                    int count = 0;
                                    while ( (!entry->services[dataflowId].ImRegistered) && (count < 100) )
                                    {
                                        sleep(1);
                                        count++;
                                    }

                                    if ( entry->services[dataflowId].ImRegistered )
                                    {
                                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Registration With Peer = " << std::to_string(peerIndex) << " Completed!" << std::endl;

                                        // Send Keep Alive Request To Subscribed Service
//                                        iswSolNet->SendKeepAliveRequestMessage(peerIndex, IswSolNet::SERVICE_PROVIDER, serviceSelector);
                                    }
                                    else
                                    {
                                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Registration Peer = " << std::to_string(peerIndex) << " Failed!" << std::endl;
                                    }

                                    uint8_t endpointNumber = entry->services[dataflowId].endpointId;
                                    uint8_t seqNumber      = entry->services[dataflowId].nextSendDataSeqNumber;
                                    uint8_t condition      = 0x02; // Data Received On Congested Endpoint/Dataflow Id

                                    iswSolNet->SendReportDataflowConditionIndication(peerIndex, serviceSelector, endpointNumber, dataflowId, seqNumber, condition);

//                                    int reportPeriod = 5;

//                                    while(entry->services[dataflowId].ImRegistered)
//                                    {
//                                        // Send Keep Alive Request To Subscribed Service
//                                        iswSolNet->SendKeepAliveRequestMessage(peerIndex, IswSolNet::SERVICE_PROVIDER, serviceSelector);
//                                        sleep(reportPeriod);
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Wait for the RegisterRequest response
        sleep(5);

        /********************************************************
         *
         *  Sprint 6 - SolNet - Debug Print out all our services
         *                      and all peer services
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();
                    iswSolNet->PrintServicesToLogfile();
                    sleep(1);
                }
            }
        }

#if 0
        /********************************************************
         *
         *  Sprint 6 - SolNet - Send Advertise Change message from
         *                      the first device to the other peers
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    // Get the services array
                    std::array<IswSolNet::iswSolNetServiceEntry, IswSolNet::MAX_NUMBER_APPS> *mySolNetServices = iswSolNet->GetServices();

                    for (uint8_t i = 0; i<IswSolNet::MAX_NUMBER_APPS; i++)
                    {
                        if ( (*mySolNetServices)[i].serviceDesc.header.descriptorId == IswSolNet::RTA )
                        {
                            iswSolNet->RemoveOneService(i);
                        }
                    }

                    std::array<IswSolNet::iswSolNetPeerServicesEntry, MAX_PEERS> *peerServices;
                    peerServices = iswSolNet->GetPeerServices();

                    for (uint8_t i=0; i<MAX_PEERS; i++)
                    {
                        if ( (*peerServices)[i].inUse )
                        {
                            // Send an Advertise Announcement to this peer
                            // This causes the peer to send a Browse to us again
                            iswSolNet->SendAdvertiseChangeMessage(i);
                            std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " SendAdvertiseChange to peer " << std::to_string(i) << std::endl;
                        }
                    }

                }
            }
            // Only do this on the first device
            break;
        }

        uint8_t count1 = 5;

        while ( --count1 > 0 )
        {
            std::cout << "Waiting for peers to Browse and get new services..." << std::endl;
            sleep(5);
        }


        /********************************************************
         *
         *  Sprint 6 - SolNet - Debug Print out services for devices
         *                      that received AdvertiseChangeMessage
         *
         * ******************************************************/

        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++ )
        {
            IswInterface *iswInterface = *iterator;
            if (*iterator != nullptr)
            {
                if ( iswInterface->IsReady() )
                {

                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();
                    iswSolNet->PrintServices();
                    sleep(4);
                }
            }
        }
#endif

        /********************************************************
         *
         *  Sprint 6 - SolNet - Have all devices deregister
         *                      for the Video service or Status
         *                      registered for with all peers
         *
         ********************************************************/
        for (auto iterator = iswInterfaceArray->begin(); iterator != iswInterfaceArray->end(); iterator++)
        {
            if (*iterator != nullptr)
            {
                IswInterface *iswInterface = *iterator;
                if ( iswInterface->IsReady() )
                {
                    IswSolNet *iswSolNet = iswInterface->GetIswSolNet();

                    std::array<IswSolNet::iswSolNetPeerServicesEntry, MAX_PEERS> *peerServices = iswSolNet->GetPeerServices();

                    for (uint8_t peerIndex = 0; peerIndex<MAX_PEERS; peerIndex++)
                    {
                        IswSolNet::iswSolNetPeerServicesEntry *entry = &(*peerServices)[peerIndex];

                        if ( !entry->inUse )
                        {
                            continue;
                        }

                        for (uint8_t dataflowId = 0; dataflowId < IswSolNet::MAX_NUMBER_APPS; dataflowId++)
                        {
                            if ( entry->services[dataflowId].inUse )
                            {
                                if ( ((entry->services[dataflowId].serviceDesc.header.descriptorId == IswSolNet::Video ) ||
                                      (entry->services[dataflowId].serviceDesc.header.descriptorId == IswSolNet::Status )) &&
                                     (entry->services[dataflowId].ImRegistered == true) )
                                {
                                    //! Get the service we registered for from the first peer
                                    uint32_t serviceSelector = entry->services[dataflowId].serviceDesc.serviceSelector;

                                    //! Polled test
                                    uint8_t flowState = 0; // Stop sending data!
                                    iswSolNet->SendAutonomousStartStopRequestMessage(peerIndex, serviceSelector, flowState);

                                    //! Wait for response
                                    sleep(1);

                                    //! Deregister for this service
                                    iswSolNet->SendDeregisterRequestMessage(peerIndex, serviceSelector);

                                    //! Wait for the DeregisterResponse to update ImRegistered flag
                                    //! Verify that we are registered with this peer for this service
                                    int count = 0;
                                    while ( (entry->services[dataflowId].ImRegistered) && (count < 100) )
                                    {
                                        sleep(1);
                                        count++;
                                    }

                                    if ( !entry->services[dataflowId].ImRegistered )
                                    {
                                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Deregistration With Peer = " << std::to_string(peerIndex) << " Completed!" << std::endl;
                                    }
                                    else
                                    {
                                        std::cout << "Device " << std::to_string(iswInterface->GetIndex()) << " Deregistration With Peer = " << std::to_string(peerIndex) << " Failed!" << std::endl;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        // Wait to see that peer clears association and resets
        sleep(10);

    } //! end else we have devices

    //! Exit libusb
    exitIswTest();
}

int main(int argc, char *argv[])
{
    std::string testFilename = "iswTest";

    //! Public Use these to set debugLevel
    //! - - - -|- - - -|- - - -|- - - -
    //!                               |- IswInterface class - 0x0001
    //!                             | - IswSecurity class - 0x0002
    //!                           | - IswFirmware class - 0x0004
    //!                         | - IswIdentity class - 0x0008
    //!                       | - IswSystem class - 0x0010
    //!                     | - IswAssociation class - 0x0020
    //!                   | - IswStream class - 0x0040
    //!                 | - IswSolNet class - 0x0080
    //!               | - IswMetrics class - 0x0100
    //!             | - IswLink class - 0x0200
    //!           | - IswProduct class - 0x0400
    //!         | - UsbInterface class - 0x0800
    uint16_t debugLevel = Logger::DEBUG_ISW_SOLNET | Logger::DEBUG_ISW_STREAM | Logger::DEBUG_ISW_ASSOCIATION | Logger::DEBUG_ISW_IDENTITY;
    //uint16_t debugLevel = Logger::DEBUG_ISW_SOLNET;
    Logger* theLogger = new Logger(testFilename, debugLevel);
    std::cout << "theLogger = " << theLogger << std::endl;

    QString version    = "1";
    DBLogger *dblogger = new DBLogger(testFilename, version);

    //! Start the dequeue thread
    struct arg_struct args1;
    args1.logger   = theLogger;
    args1.dblogger = dblogger;
    args1.threadNo = 1;

    if (pthread_create(&thread_dequeue, NULL, &dequeue_messages, (void *)&args1) != 0)
    {
        std::cout << "Dequeue Thread " << args1.threadNo << " Didn't Start!" << std::endl;
    }
    else
    {
        std::cout << "Dequeue Thread " << args1.threadNo << " Started" << std::endl;
    }

    //! Start a thread to test ISW and the USB interface
    struct arg_struct args2;
    args2.logger   = theLogger;
    args2.dblogger = dblogger;
    args2.threadNo = 2;

    if (pthread_create(&thread_isw, NULL, &testSolNetMessages, (void *)&args2) != 0)
    {
        std::cout << "Test SolNet Messages Thread " << args2.threadNo << " Didn't Start!" << std::endl;
    }
    else
    {
        std::cout << "Test SolNet Messages Thread " << args2.threadNo << " Started" << std::endl;
    }

    //! Wait for test to end
    while ( getStopIssued() != 1 )
    {
        sleep(2);
        std::cout << "Test In Progress..." << std::endl;
    }

    //! Make sure all the threads are gone
    pthread_join(thread_isw, nullptr);
    pthread_join(thread_dequeue, nullptr);

    theLogger->setImmediateShutdown(true);
    delete(theLogger);
    delete(dblogger);

    std::cout << "Thread testSolNet Ended" << std::endl;
    std::cout << "Thread dequeue Ended" << std::endl;

    std::cout << "ISW Integration Test completed" << std::endl;

    exit(0);
}
