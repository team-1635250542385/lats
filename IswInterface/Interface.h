//!###############################################
//! Filename: Interface.h
//! Description: Base class that provides an
//!              interface to a Linux device
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef INTERFACE_H
#define INTERFACE_H

#include "../Logger/Logger.h"

class Interface
{

public:
    Interface(long **device, Logger *logger, uint8_t board_type, void *boardData)
    {
        theLogger = logger;
    }
    virtual ~Interface() {};

    //! Linux USB device interface
    virtual int OpenDevice(void) = 0;
    virtual int CloseDevice(void) = 0;
    virtual int ClaimInterface(void) = 0;
    virtual int SendControlMessage(uint8_t bmRequestType, uint8_t bRequest,
                           uint16_t wValue, uint16_t wIndex,
                           std::string *data, uint16_t wLength,
                           unsigned int ctr_timeout) = 0;
    virtual int AsyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length, void *callback) = 0;
    virtual int SyncWriteDevice(uint8_t *transferBuf, uint8_t endpoint, int length) = 0;
    virtual int ReadDevice(uint8_t *receiveBuf, uint8_t endpoint, int length, int *numRead) = 0;

    Logger *theLogger;
};

#endif //! INTERFACE_H
