//!###############################################
//! Filename: IswProduct.cpp
//! Description: Class that provides an interface
//!              to ISW Product commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#include "IswProduct.h"
#include "IswInterface.h"
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <iomanip>

#include <QDebug>

//!###############################################################
//! Constructor: IswProduct()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswProduct::IswProduct(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswProduct::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswProduct::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;


    //qDebug() << "got product event: " << cmd;
    switch (cmd)
    {
        case GetNetworkId:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received GetNetworkId Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            IswProductCmdLock.lock();
            memcpy(&iswNetworkId, data, sizeof(iswNetworkId));
            IswProductCmdLock.unlock();

            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                std::stringstream ss;
                ss << "    Network ID = " << std::to_string((uint32_t)iswNetworkId) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case SetHibernation:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Set Hibernation Mode Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetHibernation:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Get Hibernation Mode Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswGetHibernationModeEvent *getHibernationModeData = (iswGetHibernationModeEvent *)data;

            IswProductCmdLock.lock();
            iswHibernationMode     = getHibernationModeData->hibernationMode;
            iswReserved1           = getHibernationModeData->reserved1;
            iswIdleTime            = getHibernationModeData->idleTime;
            iswSoftHibernationTime = getHibernationModeData->softHibernationTime;
            IswProductCmdLock.unlock();

            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                std::stringstream ss;
                ss << "    Hibernation Mode      = " << std::to_string(iswHibernationMode) << std::endl;
                ss << "    Idle Time             = " << std::to_string(iswIdleTime) << std::endl;
                ss << "    Soft Hibernation Time = " << std::to_string(iswSoftHibernationTime) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case SetObserverMode:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Set Observer Mode Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetObserverMode:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Get Observer Mode Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());

            }
            IswProductCmdLock.lock();
            iswGetObserverModeEvent *observerEvent = (iswGetObserverModeEvent *)data;
            iswObserverModeEnabled = observerEvent->iswEnable;
            //qDebug() << "Got observer mode: " << iswObserverModeEnabled;
            IswProductCmdLock.unlock();
            break;
        }
        case GetAudienceMode:
        {
            IswProductCmdLock.lock();
            memcpy(&iswAudienceModeEnabled, data, sizeof(iswAudienceModeEnabled));
            //qDebug() << "got audience mode in product:" << iswAudienceModeEnabled;
            IswProductCmdLock.unlock();
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Get Audience Mode Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case SetAudienceMode:
        {
            //! Update the stored Audience Mode Status by making a call to SendGetAudienceMode()
            SendGetAudienceModeCmd();
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Received Set Audience Mode Confirmation, updating Audience Mode Status";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswProduct == 1 )
            {
                msgString = "Bad Product event type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! SetupGetNetworkIdCmd()
//! Inputs: iswProductNetworkIdCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswProduct::SetupGetNetworkIdCmd(iswProductCommand *iswProductNetworkIdCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetupGetNetworkIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswProductNetworkIdCmd->cmdType    = IswInterface::Product;
    iswProductNetworkIdCmd->command    = GetNetworkId;
    iswProductNetworkIdCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswProductNetworkIdCmd->reservedForAlignment  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswProductCommand);

    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswProductNetworkIdCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswProductNetworkIdCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswProductNetworkIdCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswProductNetworkIdCmd->reservedForAlignment) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetNetworkIdCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswProduct::SendGetNetworkIdCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SendGetNetworkIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetNetworkIdCmd(&iswCmd.iswProductCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if (status < 0)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendGetNetworkIdCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return (status);
}

//!#####################################################################
//! SetupSetHibernationModeCmd()
//! Inputs: iswSetHibernationModeCmd - pointer to the command structure
//!         hibernationMode - 0 = disabled, 1 = soft hibernation, 2 = hard
//!                           hibernation
//!         persist - 0 = nothing, 1 = store values to NVRAM
//!         idleTime - in seconds
//!         softHibernationTime - time in seconds to hibernate before
//!                               waking up and resynchronizing with the
//!                               network
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!#####################################################################
void IswProduct::SetupSetHibernationModeCmd(iswSetHibernationModeCommand *iswSetHibernationModeCmd, uint8_t hibernationMode, uint8_t persist, uint16_t idleTime, uint8_t softHibernationTime, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetupSetHibernationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetHibernationModeCmd->cmdType             = IswInterface::Product;
    iswSetHibernationModeCmd->command             = SetHibernation;
    iswSetHibernationModeCmd->cmdContext          = iswInterface->GetNextCmdCount();
    iswSetHibernationModeCmd->reserved1           = 0x00;
    iswSetHibernationModeCmd->hibernationMode     = hibernationMode;
    iswSetHibernationModeCmd->persist             = persist;
    iswSetHibernationModeCmd->idleTime            = idleTime;
    iswSetHibernationModeCmd->softHibernationTime = softHibernationTime;
    iswSetHibernationModeCmd->reserved2[0]        = 0x00;
    iswSetHibernationModeCmd->reserved2[1]        = 0x00;
    iswSetHibernationModeCmd->reserved2[2]        = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(iswSetHibernationModeCommand);

    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType             = " << std::to_string(iswSetHibernationModeCmd->cmdType) << std::endl;
        ss << "    command             = " << std::to_string(iswSetHibernationModeCmd->command) << std::endl;
        ss << "    cmdContext          = " << std::to_string(iswSetHibernationModeCmd->cmdContext) << std::endl;
        ss << "    reserved1           = " << std::to_string(iswSetHibernationModeCmd->reserved1) << std::endl;
        ss << "    hibernationMode     = " << std::to_string(iswSetHibernationModeCmd->hibernationMode) << std::endl;
        ss << "    persist             = " << std::to_string(iswSetHibernationModeCmd->persist) << std::endl;
        ss << "    idleTime            = " << std::to_string(iswSetHibernationModeCmd->idleTime) << std::endl;
        ss << "    softHibernationTime = " << std::to_string(iswSetHibernationModeCmd->softHibernationTime) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!#######################################################################
//! SendSetHibernationModeCmd()
//! Inputs: hibernationMode - 0 = disabled, 1 = soft hibernation, 2 = hard
//!                           hibernation
//!         persist - 0 = nothing, 1 = store values to NVRAM
//!         idleTime - in seconds
//!         softHibernationTime - time in seconds to hibernate before
//!                               waking up and resynchronizing with the
//!                               network
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswProduct::SendSetHibernationModeCmd(uint8_t hibernationMode, uint8_t persist, uint16_t idleTime, uint8_t softHibernationTime)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetHibernationModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetHibernationModeCmd(&iswCmd.iswSetHibernationModeCmd, hibernationMode, persist, idleTime, softHibernationTime, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if (status < 0)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendSetHibernationModeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return (status);
}

//!#####################################################################
//! SetupGetHibernationModeCmd()
//! Inputs: iswProductCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!#####################################################################
void IswProduct::SetupGetHibernationModeCmd(iswProductCommand *iswProductCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetupGetHibernationModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswProductCmd->cmdType    = IswInterface::Product;
    iswProductCmd->command    = GetHibernation;
    iswProductCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswProductCmd->reservedForAlignment  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswProductCommand);

    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswProductCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswProductCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswProductCmd->cmdContext) << std::endl;
        ss << "    reservedForAlignment  = " << std::to_string(iswProductCmd->reservedForAlignment) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!############################################################################
//! SendGetAudienceModeCmd()
//!
//! Input: None
//!
//! Description: Send the ISW Get Audience Mode command to the firmware
//!############################################################################
int IswProduct::SendGetAudienceModeCmd()
{
    int status = IswInterface::ISW_CMD_SUCCESS;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;

    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SendGetAudienceModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    SetupGetAudienceModeCmd(&iswCmd.iswProductCmd, (void *)&routingHdr);

    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if(IswInterface::ISW_CMD_SUCCESS != status)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendGetAudienceModeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }    }

    return status;
}

//!############################################################################
//! SendSetAudienceModeCmd()
//!
//! Input: enable - 0x01: Enable Audience Mode, 0x00: Disable Audience Mode
//!
//! Description: Send the ISW Set Audience Mode command to the firmware
//!############################################################################
int IswProduct::SendSetAudienceModeCmd(uint8_t enable)
{
    int status = IswInterface::ISW_CMD_SUCCESS;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;

    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SendSetAudienceModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    SetupSetAudienceModeCmd(&iswCmd.iswSetAudienceModeCmd, (void *)&routingHdr, enable);

    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if(IswInterface::ISW_CMD_SUCCESS != status)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendSetAudienceModeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return status;
}

//!###########################################################
//! SendGetHibernationModeCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###########################################################
int IswProduct::SendGetHibernationModeCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "GetHibernationModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetHibernationModeCmd(&iswCmd.iswProductCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if (status < 0)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendGetHibernationModeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return (status);
}

//!############################################################################
//! prepareGetAudienceModeCmd()
//!
//! Input:  iswProductCmd - pointer to the command structure
//!         rountingHdr - pointer to the ISW Routing Header
//!
//! Description: Fills in the ISW Header for the send command to the firmware
//!############################################################################
void IswProduct::SetupGetAudienceModeCmd(iswProductCommand *cmd, void *routingHdr)
{
    std::string msgString;
    if(theLogger->DEBUG_IswProduct == 1)
    {
        msgString = "prepareGetAudienceModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    cmd->cmdType        = IswInterface::Product;
    cmd->command            = GetAudienceMode;
    cmd->cmdContext          = iswInterface->GetNextCmdCount();
    cmd->reservedForAlignment  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = ISW_ROUTING_HDR_DEFAULT_SRC_ADDR;
    routingHdrPtr->destAddr                    = ISW_ROUTING_HDR_DEFAULT_DST_ADDR;
    routingHdrPtr->dataLength                  = sizeof(iswProductCommand);

    if(theLogger->DEBUG_IswProduct == 1)
    {
        std::stringstream ss;
        ss << "cmdType = " << std::to_string(cmd->cmdType) << std::endl;
        ss << "command = " << std::to_string(cmd->command) << std::endl;
        ss << "cmdContext = " << std::to_string(cmd->cmdContext) << std::endl;
        ss << "reservedForAlignment = " << std::to_string(cmd->reservedForAlignment) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!############################################################################
//! prepareEnableAudienceModeCmd()
//!
//! Input:  iswProductCmd - pointer to the command structure
//!         rountingHdr - pointer to the ISW Routing Header
//!
//! Description: Fills in the ISW Header for the send command to the firmware
//!############################################################################
void IswProduct::SetupSetAudienceModeCmd(iswSetAudienceModeCommand *cmd, void *routingHdr, uint8_t audienceModeStatus)
{
    std::string msgString;
    if(theLogger->DEBUG_IswProduct == 1)
    {
        msgString = "prepareEnableAudienceModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    cmd->commandType        = IswInterface::Product;
    cmd->command            = SetAudienceMode;
    cmd->contextId          = iswInterface->GetNextCmdCount();
    cmd->reservedForAlignment  = ISW_PRODUCT_RESERVED_BYTE;
    cmd->enabled            = audienceModeStatus;
    memset(cmd->reserved, ISW_PRODUCT_RESERVED_BYTE, SET_AUDIENCE_MODE_NUM_RESERVED_BYTES);

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = ISW_ROUTING_HDR_DEFAULT_SRC_ADDR;
    routingHdrPtr->destAddr                    = ISW_ROUTING_HDR_DEFAULT_DST_ADDR;
    routingHdrPtr->dataLength                  = sizeof(iswSetAudienceModeCommand);

    if(theLogger->DEBUG_IswProduct == 1)
    {
        std::stringstream ss;
        int i = 0;
        ss << "commandType = " << std::to_string(cmd->commandType) << std::endl;
        ss << "command = " << std::to_string(cmd->command) << std::endl;
        ss << "contextId = " << std::to_string(cmd->contextId) << std::endl;
        ss << "reservedForAlignment = " << std::to_string(cmd->reservedForAlignment) << std::endl;
        ss << "enabled = " << std::to_string(cmd->enabled) << std::endl;
        ss << "reserved = [";
        for(i=0; i < SET_AUDIENCE_MODE_NUM_RESERVED_BYTES; i++)
        {
           ss << std::to_string(cmd->reserved[i]);
           if((i+1) < SET_AUDIENCE_MODE_NUM_RESERVED_BYTES)
           {
               ss << ",";
           }
        }
        ss << "]" << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

////!############################################################################
////! prepareDisableAudienceModeCmd()
////!
////! Input:  iswProductCmd - pointer to the command structure
////!         rountingHdr - pointer to the ISW Routing Header
////!
////! Description: Fills in the ISW Header for the send command to the firmware
////!############################################################################
//void IswProduct::SetupDisableAudienceModeCmd(iswSetAudienceModeCommand *cmd, void *routingHdr)
//{
//    std::string msgString;
//    if(theLogger->DEBUG_IswProduct == 1)
//    {
//        msgString = "prepareEnableAudienceModeCmd";
//        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
//    }

//    cmd->commandType        = IswInterface::Product;
//    cmd->command            = SetAudienceMode;
//    cmd->contextId          = iswInterface->GetNextCmdCount();
//    cmd->reservedForAlignment  = ISW_PRODUCT_RESERVED_BYTE;
//    cmd->enabled            = AUDIENCE_MODE_DISABLE;
//    memset(cmd->reserved, ISW_PRODUCT_RESERVED_BYTE, SET_AUDIENCE_MODE_NUM_RESERVED_BYTES);

//    //! Add the ISW Routing Header to the buffer
//    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
//    routingHdrPtr->srcAddr                     = ISW_ROUTING_HDR_DEFAULT_SRC_ADDR;
//    routingHdrPtr->destAddr                    = ISW_ROUTING_HDR_DEFAULT_DST_ADDR;
//    routingHdrPtr->dataLength                  = sizeof(iswSetAudienceModeCommand);

//    if(theLogger->DEBUG_IswProduct == 1)
//    {
//        std::stringstream ss;
//        int i = 0;
//        ss << "commandType = " << std::to_string(cmd->commandType) << std::endl;
//        ss << "command = " << std::to_string(cmd->command) << std::endl;
//        ss << "contextId = " << std::to_string(cmd->contextId) << std::endl;
//        ss << "reservedForAlignment = " << std::to_string(cmd->reservedForAlignment) << std::endl;
//        ss << "enabled = " << std::to_string(cmd->enabled) << std::endl;
//        ss << "reserved = [";
//        for(i=0; i < SET_AUDIENCE_MODE_NUM_RESERVED_BYTES; i++)
//        {
//           ss << std::to_string(cmd->reserved[i]);
//           if((i+1) < SET_AUDIENCE_MODE_NUM_RESERVED_BYTES)
//           {
//               ss << ",";
//           }
//        }
//        ss << "]" << std::endl;
//        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
//    }
//}

//!#####################################################################
//! SetupObserverModeSetCmd()
//! Inputs: iswObserverModeSetCmd - pointer to the command structure
//!         type
//!         rssiConnect
//!         rssiDisconnect
//!         phyRateCap
//!         devType
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!#####################################################################
void IswProduct::SetupSetObserverModeCmd(iswSetObserverModeCommand *iswSetObserverModeCmd, uint8_t type, uint8_t rssiConnect, uint8_t rssiDisconnect, uint8_t phyRateCap, uint16_t devType, void* routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetupSetObserverModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetObserverModeCmd->cmdType        = IswInterface::Product;
    iswSetObserverModeCmd->command        = SetObserverMode;
    iswSetObserverModeCmd->cmdContext     = iswInterface->GetNextCmdCount();
    iswSetObserverModeCmd->reserved1      = 0x00;
    iswSetObserverModeCmd->enable         = type;
    iswSetObserverModeCmd->rssiConnect    = rssiConnect;
    iswSetObserverModeCmd->rssiDisconnect = rssiDisconnect;
    iswSetObserverModeCmd->phyRateCap     = phyRateCap;
    iswSetObserverModeCmd->devType        = devType; // HMD -- Default
    iswSetObserverModeCmd->reserved2      = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSetObserverModeCommand);

    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType        = " << std::to_string(iswSetObserverModeCmd->cmdType) << std::endl;
        ss << "    command        = " << std::to_string(iswSetObserverModeCmd->command) << std::endl;
        ss << "    cmdContext     = " << std::to_string(iswSetObserverModeCmd->cmdContext) << std::endl;
        ss << "    reserved1      = " << std::to_string(iswSetObserverModeCmd->reserved1) << std::endl;
        ss << "    enable         = " << std::to_string(iswSetObserverModeCmd->enable) << std::endl;
        ss << "    rssiConnect    = " << std::to_string(iswSetObserverModeCmd->rssiConnect) << std::endl;
        ss << "    rssiDisconnect = " << std::to_string(iswSetObserverModeCmd->rssiDisconnect) << std::endl;
        ss << "    phyRateCap     = " << std::to_string(iswSetObserverModeCmd->phyRateCap) << std::endl;
        ss << "    devType        = " << std::to_string(iswSetObserverModeCmd->devType) << std::endl;
        ss << "    reserved2      = " << std::to_string(iswSetObserverModeCmd->reserved2) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###########################################################
//! SendObserverModeSetCmd()
//! Inputs: type
//!         rssiConnect
//!         rssiDisconnect
//!         phyRateCap
//!         devType
//! Description: Send the ISW command to the firmware
//!###########################################################
int IswProduct::SendSetObserverModeCmd(uint8_t type, uint8_t rssiConnect, uint8_t rssiDisconnect, uint8_t phyRateCap, uint16_t devType)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SendObserverModeSetCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetObserverModeCmd(&iswCmd.iswSetObserverModeCmd, type, rssiConnect, rssiDisconnect, phyRateCap, devType, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if (status < 0)
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendSetObserverModeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return (status);
}

//!#####################################################################
//! SetupObserverModeGetCmd()
//! Inputs: iswObserverModeGetCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!#####################################################################
void IswProduct::SetupGetObserverModeCmd(iswProductCommand *iswObserverModeGetCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SetupObserverModeGetCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswObserverModeGetCmd->cmdType    = IswInterface::Product;
    iswObserverModeGetCmd->command    = GetObserverMode;
    iswObserverModeGetCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswObserverModeGetCmd->reservedForAlignment  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswProductCommand);

    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswObserverModeGetCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswObserverModeGetCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswObserverModeGetCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswObserverModeGetCmd->reservedForAlignment) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###########################################################
//! SendObserverModeSetCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###########################################################
int IswProduct::SendGetObserverModeCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswProduct == 1 )
    {
        msgString = "SendObserverModeGetCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetObserverModeCmd(&iswCmd.iswProductCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswProduct == 1 )
        {
            msgString = "SendObserverModeGetCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}
