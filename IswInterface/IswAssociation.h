//!###############################################
//! Filename: IswAssociation.h
//! Description: Class that provides an interface
//!              to ISW Association commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWASSOCIATION_H
#define ISWASSOCIATION_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswAssociation
{

public:
    IswAssociation(IswInterface *isw_interface, Logger *logger);
    ~IswAssociation() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Association command structures
    struct iswStartAssociationCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t timeout;
        uint8_t type;
        uint16_t reserved2;
    }__attribute__((packed));

    struct iswClearAssociationCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct iswStopAssociationCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendStartAssociationCmd(uint8_t type, uint8_t timeout);
    int SendStopAssociationCmd(void);
    int SendClearAssociationCmd(void);

    uint64_t GetStartAssociationTime(void) { return (startAssociationTimestamp); }
    void ResetStartAssociationTime(void) { startAssociationTimestamp = 0; }

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex iswAssociationCmdLock;

    uint64_t startAssociationTimestamp = 0;

    //! ISW Association commands and events
    enum iswAssociationCmdEventTypes: uint8_t
    {
        ReservedAssociationCmd      = 0,
        StartAssociation            = 1,
        StopAssociation             = 2,
        //! Reserved
        ClearAssociation            = 4,
        //! Reserved
        AssociationIndication       = 129
    };

    //! ISW Association command structures
    struct iswEventAssociationInd
    {
        uint8_t macAddress[6];
        uint16_t deviceType;
        uint8_t securityKey[16];
        uint8_t peerIndex;
        uint8_t dissociation[24];
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupStartAssociationCmd(iswStartAssociationCommand *iswStartAssociateCmd, uint8_t type, uint8_t timeout, void *routingHdr);
    void SetupClearAssociationCmd(iswClearAssociationCommand *iswClearAssociateCmd, void *routingHdr);
    void SetupStopAssociationCmd(iswStopAssociationCommand *iswStopAssociateCmd, void *routingHdr);

    //! Method to parse incoming Association Indication message information
    void ParseAssociationIndication(iswEventAssociationInd *iswAssocInd);

};

#endif //! ISWASSOCIATION_H
