//!###################################################
//! Filename: IswInterfaceInit.cpp
//! Description: Class that initalizes each device
//!              Under test with an IswInterface
//!              and a UsbInterface class.  It returns
//!              a list of IswInterface pointers
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswInterfaceInit.h"
#include "IswInterface.h"
#include <unistd.h>
#include <signal.h>

//!###############################################
//! Constructor: IswInterfaceInit()
//! Inputs: devType - Currently only USB
//!         logger - pointer to Logger object
//!         dbLogger - pointer to DBLogger object
//!###############################################
IswInterfaceInit::IswInterfaceInit(uint8_t devType, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass, DBLogger *dblogger, Logger *latencyLoggerPass) :
    deviceType(devType),
    theLogger(logger),
    throughputTestLogger(throughputTestLoggerPass),
    throughputResultsLogger(throughputResultsLoggerPass),
    dbLogger(dblogger),
    latencyLogger(latencyLoggerPass)
{
    if ( theLogger == nullptr )
    {
        std::cout << "Bad Logger Pointer!" << std::endl;
        exit(-1);
    }
    if ( dbLogger == nullptr )
    {
        std::cout << "Bad DBLogger Pointer!" << std::endl;
        exit(-1);
    }
}

IswInterfaceInit::~IswInterfaceInit()
{

}

//!#########################################################
//! InitIswInterfaceArray()
//! Inputs: None
//! Description: Uses a lock for multithreading access
//!              hotplug and basic device access are
//!              on separate threads. Adds the IswInterface
//!              pointer into the array at position index
//!#########################################################
std::array<IswInterface *, MAX_ISW_NODES> *IswInterfaceInit::InitIswInterfaceArray(void)
{
    iswInitArrayCmdLock.lock();

    //! Initialize the ISW array to nullptrs until we get valid devices
    for (auto itr = iswInterfaceArray.begin(); itr != iswInterfaceArray.end(); ++itr)
    {
        *itr = nullptr;
    }

    iswInitArrayCmdLock.unlock();

    //! Internal calls have locks
    switch (deviceType)
    {
        case IswInterface::USB:
        {
            //! Enumerate the USB devices and get the list of just
            //! ISW USB devices from libusb with the next calls
            usbInterfaceInit                                     = new UsbInterfaceInit();
            std::array<UsbInterface *, MAX_DEVICES> *usbDevArray = usbInterfaceInit->InitUsbInterfaceArray(theLogger);

            if ( usbInterfaceInit->iswDeviceCount > 0 )
            {
                for (auto itr = usbDevArray->begin(); itr != usbDevArray->end(); ++itr )
                {
                    //! Add this UsbInterface to an IswInterface to go in the list
                    //! Each ISW USB device has one IswInterface which holds the
                    //! UsbInterface reference
                    UsbInterface *usbInterface = *itr;

                    if ( usbInterface != nullptr )
                    {
                        IswInterface *iswInterface = new IswInterface(usbInterface, IswInterface::USB, theLogger, throughputTestLogger, throughputResultsLogger, dbLogger, latencyLogger);

                        //! Set flag to false until the device has been authenticated
                        iswInterface->SetDeviceIsReady(false);

                        uint8_t index = usbInterface->GetIndex();

                        //! Set the index in the IswInterface array to be the same as the UsbInterface array index
                        iswInterface->SetIndex(index);

                        //! Insert into the iswInterfaceArray at same position as the USB array
                        AddIswInterfaceToArray(index, iswInterface);

                        //! Authenticate the device and startup all the threads for send/receive
                        int status = iswInterface->IswInit(index);

                        if ( status == 0 )
                        {
                            //! Set flag to true - apps can use this device now
                            iswInterface->SetDeviceIsReady(true);
                        }
                        else
                        {
                            std::cout << "  iswInterface init failed" << std::endl;
                            if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                            {
                                std::string msgString = "  usbInterface is nullptr";
                                theLogger->LogMessage(msgString, ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                            }
                        }
                    }
                    else
                    {
                        //! During initialization devices are added in order, so
                        //! leave for loop when no more are found
                        break;
                    }
                }

    #ifndef TESTING
                //! Setup Hotplug or hotswap handling and pass IswInterfaceInit
                //! instance so we can update the list from the the iswInterfaceList
                usbInterfaceInit->SetupHotplugHandling(HandleUsbHotplugEvent, (void *)this);
    #endif
            }
            else
            {
                std::cout << "No ISW Devices!" << std::endl;
                theLogger->DebugMessage("No ISW Devices!", ISW_INTERFACE_LOG_INDEX);
            }
            break;
        }
        default:
            std::cout << "Undefined Interface Type" << std::endl;
            break;
    }

    return ( &iswInterfaceArray );
}

//!############################################################
//! ExitIswInterfaceArray()
//! Inputs: None
//! Description: Removes the entries from the iswInterfaceArray
//!############################################################
void IswInterfaceInit::ExitIswInterfaceArray()
{
    //! Stop hotplug checking
    usbInterfaceInit->SetCheckingUsbEvents(false);

    EmptyHotplugQueue();
    hotplugQueue.empty();

    for (auto itr = iswInterfaceArray.begin(); itr != iswInterfaceArray.end(); ++itr )
    {
        IswInterface *iswInterface = *itr;
        if ( iswInterface != nullptr )
        {
            //! If device isReady then the IswInit() method was run
            //! So call IswExit()
            if ( iswInterface->IsReady() )
            {
                //! Stop SolNet first in case there are applications transmitting
                IswSolNet *iswSolNet = iswInterface->GetIswSolNet();
                if ( iswSolNet != nullptr )
                {
                    iswSolNet->StopSolNet();
                }

                //! Stop applications from trying to use this device
                iswInterface->SetDeviceIsReady(false);

                //! Shutdown IswInterface
                iswInterface->IswExit();
            }

            //! deletes all the friend ISW classes too
            delete(iswInterface);
        }
    }

    //! Shutdown and delete the UsbInterface list
    if ( usbInterfaceInit != nullptr )
    {
        usbInterfaceInit->ExitUsbInterfaceArray();
        usbInterfaceInit = nullptr;
    }
}

//!###############################################################
//! AddIswInterfaceToArray()
//! Inputs: index - into the array
//!          iswInterfacePtr - pointer to the IswInterface object
//!          to be added
//!###############################################################
void IswInterfaceInit::AddIswInterfaceToArray(uint8_t index, IswInterface *iswInterfacePtr)
{
    iswInitArrayCmdLock.lock();
    iswInterfaceArray[index] = iswInterfacePtr;
    iswInitArrayCmdLock.unlock();
}

//!###############################################################
//! RemoveIswInterfaceFromArray()
//! Inputs: index - position of the IswInterface object pointer to
//!                 be removed in the array
//!###############################################################
void IswInterfaceInit::RemoveIswInterfaceFromArray(uint8_t index)
{
    iswInitArrayCmdLock.lock();
    iswInterfaceArray[index] = nullptr;
    iswInitArrayCmdLock.unlock();
}

//!###############################################################
//! AddToHotplugQueue()
//! Inputs: hotplugEvent - libusb hotplug event
//! Description: Adds a libusb hotplug event to the queue for
//!              future processing
//!###############################################################
void IswInterfaceInit::AddToHotplugQueue(HotplugEvent *hotplugEvent)
{
    hotplugQueueMutex.lock();
    hotplugQueue.push(hotplugEvent);
    hotplugQueueMutex.unlock();
}

//!###############################################################
//! RemoveFromHotplugQueue()
//!###############################################################
IswInterfaceInit::HotplugEvent *IswInterfaceInit::RemoveFromHotplugQueue()
{
    HotplugEvent *hotplugEvent;

    if ( hotplugQueue.empty() == false )
    {
        hotplugQueueMutex.lock();
        hotplugEvent = hotplugQueue.front();
        hotplugQueue.pop();
        hotplugQueueMutex.unlock();
    }

    return (hotplugEvent);
}

//!###############################################################
//! CheckHotplugQueue()
//! Inputs: macAddr
//! Description: Checks the queue for an existing entry that
//!              hasn't been handled yet
//!###############################################################
bool IswInterfaceInit::CheckEntryExists(std::string macAddr)
{
    bool foundEntry = false;

    hotplugQueueMutex.lock();
    if ( hotplugQueue.empty() == false )
    {
        for (auto itr = hotplugQueue.front(); itr != hotplugQueue.back(); ++itr)
        {
            HotplugEvent *hotplugEvent = itr;
            if ( hotplugEvent->externalEntry.macAddr.compare(macAddr) == 0 )
            {
                foundEntry = true;
                break;
            }
        }
    }
    hotplugQueueMutex.unlock();

    return (foundEntry);
}

//!###############################################################
//! EmptyHotplugQueue()
//!###############################################################
void IswInterfaceInit::EmptyHotplugQueue()
{
    //! Called from destructor on shutdown
    while ( hotplugQueue.empty() == false )
    {
        HotplugEvent *hotplugEvent = RemoveFromHotplugQueue();
        delete(hotplugEvent);
        usleep(10);
    }
}

//!######################################################################
//! HandleUsbHotplugEvent()
//! Inputs: ctx - libusb context pointer
//!         dev - pointer to a libusb device
//!         event - a libusb event
//!         user_data - pointer to other data the callback may need
//! Description: This method is registered as a callback with the libusb
//!              library to be called after UsbInterfaceInit::HandleUsbEvents()
//!              periodic probe for USB events. Only called if there
//!              is an event.
//!              The USB events are:
//!              - a device was removed from the list of Linux USB devices
//!              - a device was added to the list of LInux USB devices
//!######################################################################
int IswInterfaceInit::HandleUsbHotplugEvent(struct libusb_context *ctx, struct libusb_device *dev,
                                            libusb_hotplug_event event, void *user_data)
{
    if ( user_data == nullptr )
    {
        std::cout << "HandleUsbHotplugEvent - user_data NULL" << std::endl;
        return (0);
    }

    switch ( event )
    {
        case LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT:
        {
            IswInterfaceInit *iswInterfaceInit = (IswInterfaceInit *)user_data;
            IswInterface *iswInterface         = nullptr;
            UsbInterface *usbInterface         = nullptr;
            bool foundDevice                   = false;

            for (auto iterator = iswInterfaceInit->iswInterfaceArray.begin(); iterator != iswInterfaceInit->iswInterfaceArray.end(); iterator++)
            {
                if ( *iterator != nullptr )
                {
                    iswInterface = *iterator;

                    if ( iswInterface->IsReady() )
                    {
                        usbInterface = iswInterface->GetUsbInterface();

                        if ( usbInterface->GetDevice() == dev )
                        {
                            // If removing a device - find it and set the iswInterface->isReady flag
                            // to false so apps can't send data
                            iswInterface->SetDeviceIsReady(false);

                            // Stop SolNet first in case there are applications transmitting
                            IswSolNet *iswSolNet = iswInterface->GetIswSolNet();
                            iswSolNet->StopSolNet();

                            foundDevice = true;
                            break;
                        }
                    }
                }
            }

            if ( !foundDevice )
            {
                std::cout << "LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT - Can't Find Device" << std::endl;
            }
            break;
        }
        case LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED:
        default:
            break;
    }

    IswInterfaceInit *iswInterfaceInit           = (IswInterfaceInit *)user_data;
    IswInterfaceInit::HotplugEvent *hotplugEvent = new IswInterfaceInit::HotplugEvent;
    hotplugEvent->external                       = false;
    hotplugEvent->ctx                            = ctx;
    hotplugEvent->dev                            = dev;
    hotplugEvent->event                          = event;
    iswInterfaceInit->AddToHotplugQueue(hotplugEvent);

    //! Return zero to rearm the callback
    return (0);
}

//!############################################################
//! AddHotplugDevice()
//! Inputs: hotplugEvent - hotplug event
//! Description: This method controls the addition of a new ISW
//!              device from a hotplug event so that the usb and
//!              isw arrays use the same index or slot for a
//!              device.
//!############################################################
uint8_t IswInterfaceInit::AddHotplugDevice(HotplugEvent *hotplugEvent)
{
    //! We add this device at the first nullptr
    std::array<UsbInterface *, MAX_DEVICES> *usbDevArray = usbInterfaceInit->GetUsbInterfaceArray();

    for (auto iterator = iswInterfaceArray.begin(); iterator != iswInterfaceArray.end(); iterator++)
    {
        if ( *iterator != nullptr )
        {
            IswInterface *iswInterface = *iterator;

            if ( hotplugEvent->external == false )
            {
                if ( iswInterface->IsReady() )
                {
                    UsbInterface *usbInterface = iswInterface->GetUsbInterface();

                    if ( usbInterface->GetDevice() == hotplugEvent->dev )
                    {
                        //! This case can happen during initialization
                        //! the device was added but hotplug still occurs
                        return (iswInterface->GetIndex());
                    }
                }
            }
        }
    }

    int index      = 0;
    bool foundSlot = false;
    for (auto itr = usbDevArray->begin(); itr != usbDevArray->end(); itr++ )
    {
        if ( *itr == nullptr )
        {
            //! Add device here
            foundSlot = true;
            break;
        }
        else
        {
            index++;
        }
    }

    if ( foundSlot )
    {
        libusb_device_descriptor *desc = nullptr;
        int status                     = 0;

        if ( hotplugEvent->external == false )
        {
            desc = new libusb_device_descriptor;
            status = libusb_get_device_descriptor(hotplugEvent->dev, desc);
        }

        if ( status == 0 )
        {
            UsbInterface *usbInterface = nullptr;
            usbInterfaceInit->CreateUsbInterface(hotplugEvent->external, hotplugEvent->dev, desc, &usbInterface, index);

            if ( usbInterface != nullptr )
            {
                IswInterface *iswInterface = new IswInterface(usbInterface, IswInterface::USB, theLogger, throughputTestLogger, throughputResultsLogger, dbLogger, latencyLogger);

                //! Set flag to false until the device has been authenticated
                iswInterface->SetDeviceIsReady(false);

                index = usbInterface->GetIndex();

                //! Set the index in the IswInterface array to be the same as the UsbInterface array index
                iswInterface->SetIndex(index);

                //! Insert into the iswInterfaceArray at same position as the USB array
                AddIswInterfaceToArray(index, iswInterface);

                //! Authenticate the device and startup all the threads for send/receive
                int status = iswInterface->IswInit(index);

                if ( status == 0 )
                {
                    if ( hotplugEvent->external == true )
                    {
                        IswIdentity *iswIdentity = iswInterface->GetIswIdentity();
                        iswIdentity->SetDeviceTypeForExternal(hotplugEvent->externalEntry.devType);
                        iswIdentity->SetMacAddrForExternal(hotplugEvent->externalEntry.macAddr);
                    }
                    //! Set flag to true - apps can use this device now
                    iswInterface->SetDeviceIsReady(true);
                }
                else
                {
                    std::cout << "  iswInterface init failed" << std::endl;
                    if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                    {
                        std::string msgString = "  usbInterface is nullptr";
                        theLogger->LogMessage(msgString, ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                    }
                }
            }
            else
            {
                std::cout << "  usbInterface is nullptr, OpenDevice error" << std::endl;
                if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                {
                    std::string msgString = "  usbInterface is nullptr";
                    theLogger->LogMessage(msgString, ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                }
            }
        }
        else
        {
            std::cout << "Device " << std::to_string(index) << " failed hotplug!" << std::endl;
        }
    }
    else
    {
        std::cout << "Couldn't get a slot to add hotplug device" << std::endl;
    }

    return (index);
}

//!############################################################
//! RemoveHotplugDevice()
//! Inputs: hotplugEvent - hotplug event
//! Description: Find the device in the USB and ISW arrays and
//!              remove it. Close the device, and clean up
//!              resources.
//!############################################################
uint8_t IswInterfaceInit::RemoveHotplugDevice(HotplugEvent *hotplugEvent)
{
    std::string msgString;
    uint8_t index = 0xFF;

    for (auto iterator = iswInterfaceArray.begin(); iterator != iswInterfaceArray.end(); iterator++)
    {
        if ( *iterator != nullptr )
        {
            IswInterface *iswInterface = *iterator;
            //! Don't check for iswInterface->isReady here because we set
            //! it to false in the interrupt handler HandleUsbHotplugEvent()

            UsbInterface *usbInterface = iswInterface->GetUsbInterface();

            if ( usbInterface->GetDevice() == hotplugEvent->dev )
            {
                //! Close the device
                if ( usbInterface->IsDeviceOpen() == true )
                {
                    int status = usbInterface->CloseDevice();

                    if ( status == 0 )
                    {
                        msgString = "  CloseDevice successful";
                    }
                    else
                    {
                        msgString = "  CloseDevice unsuccessful";
                    }

                    if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                    {
                        usbInterface->theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
                    }
                }

                index = usbInterface->GetIndex();

                //! Remove this device from the arrays
                usbInterfaceInit->RemoveUsbInterfaceFromArray(index);
                RemoveIswInterfaceFromArray(index);

                //! Shutdown usbInterface
                usbInterface->UsbInterfaceExit();

                //! Shutdown IswInterface
                iswInterface->IswExit();

                //! Free the resources
                delete(usbInterface);
                delete(iswInterface);

                break;
            }
        }
    }

    return(index);
}

//!############################################################
//! CheckHotplugQueue()
//! Inputs: None
//! Description: This gets called periodically to check the event
//!              hotplug handling queue
//!############################################################
uint8_t IswInterfaceInit::CheckHotplugQueue(void)
{
    std::string msgString;
    uint8_t index = 0xFF;

    if ( (usbInterfaceInit != nullptr) && (usbInterfaceInit->GetCheckingUsbEvents() == true) )
    {
        if ( hotplugQueue.empty() == false )
        {
            HotplugEvent *hotplugEvent = RemoveFromHotplugQueue();

            switch ( hotplugEvent->event )
            {
                case LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED:
                {
                    if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                    {
                        msgString = "  Event is LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED";
                        theLogger->LogMessage(msgString, ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                    }
                    std::cout << "    LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED" << std::endl;

                    index = AddHotplugDevice(hotplugEvent);
                    break;
                }
                case LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT:
                {
                    if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                    {
                        msgString = "  Event is LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT";
                        theLogger->LogMessage(msgString, ISW_INTERFACE_LOG_INDEX, std::chrono::system_clock::now());
                    }
                    std::cout << "    LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT" << std::endl;

                    index = RemoveHotplugDevice(hotplugEvent);
                    break;
                }
                default:
                {
                    if ( theLogger->DEBUG_IswInterfaceInit == 1 )
                    {
                        msgString = "  Event is not hotplug";
                        theLogger->LogMessage(msgString, 0xFF, std::chrono::system_clock::now());
                    }
                    break;
                }
            }

            delete (hotplugEvent);
        }
    }

    //! Return the index into the IswInterface array for the device just added or removed
    return (index);
}
