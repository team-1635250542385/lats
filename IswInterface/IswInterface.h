//!##########################################################
//! Filename: IswInterface.h
//! Description: Class that provides the ISW API for the user
//!              application.  Each device has one instance
//!              of an IswInterface object.  This is the main
//!              object that the user will use to send and
//!              receive ISW commands.  Each IswInterface object
//!              has links to:
//!              - An UsbInterface object
//!              - Each of the friends' classes objects:
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWINTERFACE_H
#define ISWINTERFACE_H

#include <mutex>
#include <pthread.h>
#include <array>
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"
#include "../UsbInterface/UsbInterface.h"
#include "../OnBoard/OnBoard.h"
#include "../SerialInterface/SerialInterface.h"
#include "IswFirmware.h"
#include "IswStream.h"
#include "IswSystem.h"
#include "IswIdentity.h"
#include "IswSecurity.h"
#include "IswAssociation.h"
#include "IswLink.h"
#include "IswMetrics.h"
#include "IswProduct.h"
#include "IswSolNet.h"
#include "IswTest.h"

#include "QMap"


#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface
{
//! Each of these classes implements one of
//! the types of ISW commands per the ISW
//! Embedment Guide.  The ISW Library is designed
//! so that they are all C++ friend classes of
//! iswInterface so they can access IswInterface
//! class in the same way.
friend class OnBoard;
friend class IswFirmware;
friend class IswStream;
friend class IswSystem;
friend class IswIdentity;
friend class IswSecurity;
friend class IswAssociation;
friend class IswLink;
friend class IswMetrics;
friend class IswProduct;
friend class IswSolNet;
friend class IswTest;

public:
    IswInterface(Interface *devInterface, uint8_t devType, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass, DBLogger *dblogger, Logger *latencyLoggerPass);
    ~IswInterface();

    //! Pointer to a Logger object
    Logger *theLogger               = nullptr;
    Logger *throughputTestLogger    = nullptr;
    Logger *throughputResultsLogger = nullptr;

    //! Pointer to a DBLogger object
    DBLogger *dbLogger = nullptr;
    Logger *latencyLogger = nullptr;

    //! Initialize an IswInterface object for one ISW device
    int IswInit(uint8_t index);

    //! Clean up and exit the running IswInterface object
    void IswExit(void);

    //! Check whether your system is Little Endian. The ISW
    //! firmware accepts commands in Lttle Endian format. This
    //! code has been tested on Little Endian systems
    bool IsLittleEndian(void) { return (isLittleEndian); }

    //! Return a pointer to a UsbInterface class linked to
    //! this object instance
    UsbInterface *GetUsbInterface(void) { return (usbInterface); }    

    //! The index is used for logging and hotplug
    //! It is the position in the array
    uint8_t GetIndex(void) { return (iswListIndex); }
    void SetIndex(uint8_t index) { iswListIndex = index; }

    //! Used by ISW firmware
    uint8_t GetNextCmdCount(void);

    bool GetPauseReceiveQueueProcessing(void) {return (pauseReceiveQueueProcessing);}
    void SetPauseReceiveQueueProcessing(bool receiveStatus) { pauseReceiveQueueProcessing = receiveStatus; }

    //! Each of these returns a pointer to the ISW object
    //! for that class
    OnBoard *GetIswOnBoard(void) { return (iswOnBoard); }
    IswFirmware *GetIswFirmware(void) { return (iswFirmware); }
    IswStream *GetIswStream(void) { return (iswStream); }
    IswSystem *GetIswSystem(void) { return (iswSystem); }
    IswIdentity *GetIswIdentity(void) { return (iswIdentity); }
    IswSecurity *GetIswSecurity(void) { return (iswSecurity); }
    IswAssociation *GetIswAssociation(void) { return (iswAssociation); }
    IswLink *GetIswLink(void) { return (iswLink); }
    IswMetrics *GetIswMetrics(void) { return (iswMetrics); }
    IswProduct *GetIswProduct(void) { return (iswProduct); }
    IswSolNet *GetIswSolNet(void) { return (iswSolNet); }
    IswTest *GetIswTest(void) {return (iswTest); }

    //! Used to indicate that this ISW device is ready for
    //! read/write from an application
    bool IsReady(void) { return (iswDeviceIsReady); }
    void SetDeviceIsReady(bool deviceReady);

    //! When stopping data traffic tests, we may
    //! want to flush the queues and not keep the
    //! send/receive and logging going - should
    //! only affect SolNet data queues and not ISW
    //! ISW API messages on endpoint 0
    void SetFlushReceiveQueue(u_int8_t endpointId, u_int8_t dataflowId, bool flush);
    void SetFlushSendQueue(u_int8_t endpointId, u_int8_t dataflowId, bool flush);

    void writeOutLatencyEvent(QString msgCntStr, QString macAddressStr, QString peerIndexStr, QString endpointStr, int seqNum, int headerChecksum, uint64_t timestamp, QString sendOrRec);

    bool sendingData = false;

    enum iswDeviceTypes: uint8_t
    {
        USB = 0
    };

    // Sequence number key, timestamp value
    QMap<int, uint64_t> sendTimeMap;

    //!

ACCESS:

    //! Double the libusb maximum packet size as the maximum ISW interface
    //! receive buffer size
    static const int MAX_RECEIVE_BUFFER_SIZE = 8128;

    uint8_t deviceType;  //! USB for now

    //! Not associated with ISW peer index
    //! Just internal for logging
    uint8_t iswListIndex       = 0;

    //! Generic Linux interface for USB
    Interface *deviceInterface     = nullptr;

    //! UsbInterface linked to this object
    UsbInterface *usbInterface     = nullptr;

    //! ISW Friend Class Pointers
    OnBoard *iswOnBoard            = nullptr;
    IswFirmware *iswFirmware       = nullptr;
    IswStream *iswStream           = nullptr;
    IswSystem *iswSystem           = nullptr;
    IswIdentity *iswIdentity       = nullptr;
    IswSecurity *iswSecurity       = nullptr;
    IswAssociation *iswAssociation = nullptr;
    IswLink *iswLink               = nullptr;
    IswMetrics *iswMetrics         = nullptr;
    IswProduct *iswProduct         = nullptr;
    IswSolNet *iswSolNet           = nullptr;
    IswTest   *iswTest             = nullptr;

    //! Little or Big Endian?
    bool isLittleEndian            = true;

    //! Coordinate starting the read thread
    bool enableReadIsw             = false;

    //! Coordinate starting/stopping the thread
    //! that processes the data send queues
    bool processDataSendQueues     = false;

    //! Coordinate starting/stopping the thread
    //! that processes the data receive queues
    bool processDataReceiveQueues  = false;

    //! Used to temporarily prevent a devices receive queues from being processed.
    bool pauseReceiveQueueProcessing = false;

    //! Done initializing the Isw host interface
    bool iswInterfaceInitComplete  = false;

    // BufCounter
    //uint8_t bufCounter = 0;
    //uint8_t sendBulkBuf[4096];

    std::mutex iswCmdLock;
    uint8_t iswCmdCount = 0;

    long msgSendCnt = 0;
    long msgRecCnt = 0;
    long msgPeerZeroCnt = 0;
    long msgPeerOneCnt = 0;

    bool light_mode = false;

    //! Thread that polls hardware for incoming ISW messages
    pthread_t iswReceiveThread  = 0;

    //! Thread that processes messages on the SolNet data send queues
    pthread_t iswSendQThread    = 0;

    //! Thread that processes messages on the SolNet data receive queues
    pthread_t iswReceiveQThread = 0;

    //! Hotplug: if set - this device was added by hotplug
    //! and needs to be initialized
    bool iswDeviceIsReady       = false;

    struct SWIM_Header
    {
        uint8_t peerIndex;
        uint8_t streamNumber;
        uint8_t flags;
        uint8_t reserved;
        uint32_t transferSize;
    }__attribute__((packed));

#define PEER_INDEX_PEER_MASK 0x0F
#define PEER_INDEX_CONTROL_MASK 0x10
#define PEER_INDEX_MULTICAST_MASK 0x20
#define PEER_INDEX_BROADCAST_MASK 0x40
#define ISW_SWIM_HDR_SIZE sizeof(SWIM_Header)

    //! Follows the SWIM Header on USB
    //! Before Cmds or Events
    struct iswRoutingHdr
    {
        uint8_t srcAddr;
        uint8_t destAddr;
        uint16_t dataLength;
    }__attribute__((packed));

//!************************************************************************
//! Ref: A3309771B
//!
//! The ISW Protocol Specification defines the default Interface Routing
//! Header source and destination addresses with the value 1
//!************************************************************************
#define ISW_ROUTING_HDR_DEFAULT_SRC_ADDR 1
#define ISW_ROUTING_HDR_DEFAULT_DST_ADDR 1

#define ISW_ROUTING_HDR_SIZE sizeof(iswRoutingHdr)

    //! This is the format of the generic ISW command
    struct iswGeneralCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    //! Union of all friend class ISW commands so that in
    //! each file the same send command format is used. Each
    //! friend class passes their own command as a iswGeneralCommand
    struct iswCommand
    {
        union
        {
            iswGeneralCommand iswCmd;  //! To get the command type
            OnBoard::iswSetTestModeCommand setOnBoardTestModeCmd;
            OnBoard::iswGetTestModeCommand getOnBoardTestModeCmd;
            OnBoard::iswStartTestCommand startTestCommand;
            OnBoard::iswStopTestCommand stopTestCommand;
            IswFirmware::iswFirmwareCommand iswFirmwareCmd;
            IswFirmware::iswLoadImageCommand iswLoadImageCmd;
            IswFirmware::iswLastLoadImageCommand iswLoadLastImageCmd;
            IswFirmware::iswLoadPublicKeyCertCommand iswLoadPublicKeyCertCmd;
            IswFirmware::iswLoadRootPublicKeyCommand iswLoadRootKeyCmd;
            IswIdentity::iswIdentityCommand iswIdentityCmd;
            IswIdentity::iswSetDeviceTypeCommand iswSetDevTypeCmd;
            IswIdentity::iswSetCoordinatorCommand iswSetCoordinatorCmd;
            IswSecurity::iswAuthenticateRoleCommand iswAuthenticateRoleCmd;
            IswSecurity::iswCryptoStartCommand iswCryptoStartCmd;
            IswSecurity::iswCryptoGetStatusCommand iswCryptoGetStatusCmd;
            IswAssociation::iswStartAssociationCommand iswStartAssociationCmd;
            IswAssociation::iswStopAssociationCommand iswStopAssociationCmd;
            IswAssociation::iswClearAssociationCommand iswClearAssociationCmd;
            IswStream::iswStreamCommand iswStreamCmd;
            IswStream::iswExtendedStreamCommand iswExtendedStreamCmd;
            IswStream::iswSetStreamSpecCommand iswSetStreamSpecCmd;
            IswStream::iswGetStreamSpecCommand iswGetStreamSpecCmd;
            IswStream::iswGetPeerFirmwareVersionCommand iswGetPeerFirmwareVersionCmd;
            IswStream::iswStartMulticastGroupCommand iswStartMulticastGroupCmd;
            IswStream::iswJoinMulticastGroupCommand iswJoinMulticastGroupCmd;
            IswStream::iswLeaveMulticastGroupCommand iswLeaveMulticastGroupCmd;
            IswStream::iswModifyMulticastGroupCommand iswModifyMulticastGroupCmd;
            IswLink::IswLinkCommand iswLinkCmd;
            IswLink::IswSetScanDutyCycleCommand iswSetScanDutyCycleCmd;
            IswLink::IswSetMaxServiceIntervalCommand iswSetMaxServiceIntervalCmd;
            IswLink::IswSetIdleScanFrequencyCommand iswSetIdleScanFrequencyCmd;
            IswLink::IswSetBackgroundScanParametersCommand iswSetBackgroundScanParametersCmd;
            IswLink::IswSetAntennaConfigurationCommand iswSetAntennaConfigurationCmd;
            IswLink::IswSetAntennaIdCommand iswSetAntennaIdCmd;
            IswLink::IswSetBandgroupBiasCommand iswSetBandgroupBiasCmd;
            IswLink::IswSetPhyRatesCommand iswSetPhyRatesCmd;
            IswMetrics::iswMetricsCommand iswMetricsCmd;
            IswProduct::iswProductCommand iswProductCmd;
            IswProduct::iswSetObserverModeCommand iswSetObserverModeCmd;
            IswProduct::iswSetHibernationModeCommand iswSetHibernationModeCmd;
            IswProduct::iswSetAudienceModeCommand iswSetAudienceModeCmd;
            IswSystem::iswSystemCommand iswSystemCmd;
            IswSystem::iswSetIndicationsCommand iswSetIndicationsCmd;
            IswSolNet::iswSolNetHeader iswSolNetHdr;
            IswTest::iswTestCommand iswTestCmd;
        };
    }__attribute__((packed));

#define ISW_CMD_SIZE sizeof(iswCommand)

    //! ISW Event as received from the
    //! firmware
    struct iswEvent
    {
        uint8_t eventType;
        uint8_t event;
        uint8_t eventContext;
        uint8_t resultCode;
    }__attribute__((packed));

#define ISW_EVENT_SIZE sizeof(iswEvent)

    //! Event types are the types of
    //! ISW commands per the spec
    //! Each of these has a friend class
    //! to process the commands
    enum iswCmdEventTypes: uint8_t
    {
        ResrvdCmdEvent0     = 0,
        Test                = 1,
        Firmware            = 2,
        ResrvdVendor        = 3,
        Identity            = 4,
        Security            = 5,
        Association         = 6,
        Stream              = 7,
        Link                = 8,
        Metrics             = 9,
        System              = 10, // 11 Reserved for vendor specification
        Product             = 12,
        OnBoardTests        = 255, // Likely not used anymore
    };

    //! ISW command status results
    enum iswCmdResultCodes: uint8_t
    {
        ISW_CMD_SUCCESS                                          = 0x00,
        ISW_CMD_FAILURE_GENERAL                                  = 0x01,
        ISW_CMD_FAILURE_NOT_FOUND                                = 0x02,
        ISW_CMD_FAILURE_ID_MISMATCH                              = 0x03,
        ISW_CMD_FAILURE_INSUFF_MEMORY                            = 0x04,
        ISW_CMD_FAILURE_INVALID_ACTION                           = 0x05,
        ISW_CMD_FAILURE_RESOURCE_UNAVAILABLE                     = 0x06,
        ISW_CMD_FAILURE_TOO_MANY_PENDING                         = 0x07,
        ISW_CMD_FAILURE_ACCESS_VIOLATION                         = 0x08,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_INVALID_BLOCK            = 0x09,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_INVALID_SEQUENCE_NUMBER  = 0x0A,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_DATA_OVERFLOW            = 0x0B,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_CRC_MISMATCH             = 0x0C,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_PUBLIC_KEY_INVALID       = 0x0D,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_CERTIFICATE_FORMAT_ERROR = 0x0E,
        ISW_CMD_FAILURE_FIRMWARE_UPDATE_SIGNATURE_INVALID        = 0x0F,
        ISW_CMD_FAILURE_AUTHENTICATION_NEEDED                    = 0x10,
        ISW_CMD_FAILURE_INVALID_SIZE                             = 0x11,
        //! 18-127 reserved for future definition
        ISW_CMD_FAILURE_OPERATION_PENDING                        = 0x80,
        //! 129-254 reserved for future definition
        ISW_CMD_FAILURE_TIMEOUT                                  = 0xFF
    };

    //! Quality of Service settings per the
    //! ISW Embedment Guide
    enum iswQoS: uint8_t
    {
        HiNoDiscard     = 0x01,
        HiDiscard       = 0x11,
        LowNoDiscard    = 0x00,
        LowDiscard      = 0x10
    };

    //! Endpoints 0 - 3 QoS setting
    //! Default on startup is HiNoDiscard for
    //! all endpoints.
    //! Right now - queue 0 is not being used -
    //! Messages on Endpoint zero go directly out
    static const uint8_t NumberOfIswEndpoints = 4;

    //! An item that gets put onto a send queue
    struct iswDataSendItem
    {
        uint8_t peerIndex;
        iswCommand *iswCmd;
    };

    //! There are four send queues in IswInterface class
    //! The queues are initialized with a quality of service
    //! setting.  Hi priority queues are handled first, then
    //! low priority queues.
    struct iswSendQueue
    {
        uint8_t qOs;
        std::array<bool, IswSolNet::MAX_NUMBER_APPS> flushArray;
        std::list<iswDataSendItem *> sendQ;
        std::mutex qLock;
    };
    std::array<iswSendQueue, NumberOfIswEndpoints> iswEndpointSendQueues;

    //! An item that gets put onto a receive queue
    struct iswDataReceiveItem
    {
        uint8_t peerIndex;
        uint8_t *data;
    };

    //! There are four receive queues in IswInterface class
    //! The queues are initialized with a quality of service
    //! setting.  Hi priority queues are handled first, then
    //! low priority queues.
    struct iswReceiveQueue
    {
        uint8_t qOs;
        std::array<bool, IswSolNet::MAX_NUMBER_APPS> flushArray;
        std::list<iswDataReceiveItem *> receiveQ;
        std::mutex qLock;
    };
    std::array<iswReceiveQueue, NumberOfIswEndpoints> iswEndpointReceiveQueues;

    //! Checks the endianess of your system
    void CheckSystemEndianness(void);

    //! Could be used to tranlate Big to Little Endian format
    //! ISW commands must be sent to the firmware in Little Endian
    //! This code was tested on a Little Endian system
    void BigToLittleEndian(uint64_t *input, uint16_t type);

    //! Method called during initialization of the ISW device to
    //! authenticate with the devices. See the ISW Embedment Guide
    //! on device authentication
    void LoginToIswNode(void);

    //! Start a thread to receive data from the ISW device
    void StartIswReceiveThread(void);
    //! Stop the thread receiveingdata from the ISW device
    void StopIswReceiveThread(void);

    //! IswInterface receive event handling
    //! Polls the firmware for events
    void HandleIswReceiveEvents(void);

    //! Needed for C++ compiler
    static void *ReceiveThreadHandler(void *This) {((IswInterface *)This)->HandleIswReceiveEvents(); return NULL;}

    //! Add the SWIM header as defined in the ISW Embedment Guide to
    //! the front of data being sent to the firmware
    uint32_t AddSwimHeader(uint8_t *transferBuf, iswRoutingHdr *routingHdr, iswCommand *iswCmd);

    //! Remove a SWIM header from a received buffer
    void RemoveSwimHeader(uint8_t *receiveBuf,
                          SWIM_Header **swimHeader,
                          iswRoutingHdr **routingHdr,
                          iswEvent **event,
                          uint8_t **data);

    //! When a successful read from the ISW firmware occurs, this method is
    //! called.  Every ISW friend class has the same DeliverIswMessage() so
    //! any of them can be called based on the event.
    void DeliverIswMessage(iswRoutingHdr *routingHdr, iswEvent *event, uint8_t *data);

    //! Send the ISW command to the firmware.  All ISW friend class call this
    //! method to send commands.  They don't send directly, but all come through
    //! this class.
    int SyncSendIswCmd(iswCommand *iswCmmnd, iswRoutingHdr *routingHdr);

    //! Used to give detailed description to ISW command headers for logging
    void DebugPrintHeaders(SWIM_Header *SwimHeader,
                           iswRoutingHdr *routingHdr, iswEvent *event);

    //! For SolNet
    //! Use iswQoS enum to set qOs
    //! Can be used for Hi/Low queues sending data
    void SetQosOnEndpoint(uint8_t qOs, uint8_t endpoint);

    //! Initialize the send and receive queues for this class
    void InitEndpointQueues(void);

    //! Empty all the queues on shutdown
    void EmptyEndpointQueues(void);

    //! IswInterface data send queue handling
    //! ISW API command messages are sent directly on endpoint zero
    //! and are not queued. These queues hold ISW SolNet data packets.

    //! Start the thread to process entriesdeviceType on the send queues
    void StartIswSendQThread(void);

    //! Stop the thread that is processing the send queues
    void StopIswSendQThread(void);

    //! The send Q thread runs this function to get entries off the
    //! queue and pass them to the firmware
    void HandleIswSendQueues(void);

    //! For C++ compiler
    static void *SendQThreadHandler(void *This) {((IswInterface *)This)->HandleIswSendQueues(); return NULL;}

    //! Add an entry onto send queue.  The endpoint number is the queue number.
    void AddToSendQueue(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint);

    //! Get an entry off a send queue.  The endpoint number is the queue number.
    iswDataSendItem *GetNextFromSendQueue(uint8_t endpoint);
    void RemoveSendQueueFront(uint8_t endpoint);

    //! Start the thread to process entries on the receive queues
    void StartIswReceiveQThread(void);

    //! Stop the thread that is processing the receive queues
    void StopIswReceiveQThread(void);

    //! The receive Q thread runs this function to get entries off the
    //! queue and pass them to the firmware
    void HandleIswReceiveQueues(void);

    //! For C++ compiler
    static void *ReceiveQThreadHandler(void *This) {((IswInterface *)This)->HandleIswReceiveQueues(); return NULL;}

    //! Add an entry onto receive queue.  The endpoint number is the queue number.
    void AddToReceiveQueue(uint8_t peerIndex, uint8_t endpoint, uint8_t *data);

    //! Get an entry off a receive queue.  The endpoint number is the queue number.
    iswDataReceiveItem *GetNextFromReceiveQueue(uint8_t endpoint);
    void RemoveReceiveQueueFront(uint8_t endpoint);

    //! Send SolNet Packet to HW
    //! Add the SWIM Header to the SolNet packet which does not have an
    //! ISW Routing Header like the other ISW API commands
    uint32_t AddSolNetSwimHeader(uint8_t *transferBuf, iswCommand *iswCmd, uint8_t peerIndex, uint8_t endpoint);

    //! Synchronized Send of a SolNet Message packet - non-blocking call
    int SyncSendSolNetMessagePacket(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint);

    //! Synchronized Send of a SolNet Data packet - non-blocking call
    int SyncSendSolNetDataPacket(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint);

    //! Helper method to print a buffer of bytes
    void printBytes(uint8_t *bytePtr, int nrBytes);
};

#endif //! ISWINTERFACE_H
