//!###############################################
//! Filename: IswFirmware.h
//! Description: Class that provides an interface
//!              to ISW Firmware commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWFIRMWARE_H
#define ISWFIRMWARE_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswFirmware
{

public:
    IswFirmware(IswInterface *isw_interface, Logger *logger);
    ~IswFirmware() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Firmware command structures
    struct iswFirmwareCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t selector;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswLoadImageCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t selector;
        uint8_t lastBlock;  //! 0 = not last, 1 = last, 2 = label
        uint16_t blockSequence;
        uint16_t reserved2;
        uint16_t blockSize;
        uint8_t data[256]; // Reduced from 512 to fit below max buff size
    }__attribute__((packed));

    struct iswLastLoadImageCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t selector;
        uint8_t lastBlock;  //! 0 = not last, 1 = last, 2 = label
        uint16_t blockSequence;
        uint16_t reserved2;
        uint16_t blockSize;
        uint8_t data[100]; // Three fields contained in data buffer directly instead: uint32_t crc, uint8_t label[32], uint8_t signature[64]
    }__attribute__((packed));

    struct iswLoadPublicKeyCertCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t certificateType;
        uint8_t lastBlock;
        uint16_t blockSequence;
        uint16_t reserved2;
        uint16_t blockSize;
        uint8_t data[256]; // Originally set to 512, over buf size with swim and routing hdr. block size is 256, use that.
    }__attribute__((packed));

    struct iswLoadRootPublicKeyCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    //! ISW Firmware commands and events
    enum iswFirmwareCmdEventTypes: uint8_t
    {
        Reserved1               = 0,
        GetImageVersion         = 1,  // Get the firmware version
        LoadImage               = 2,  // Downloads a block of firmware
        Reserved2               = 3,
        RunImage                = 4,  // Run the firmware image
        ResrvdFuture            = 5,
        LoadPublicKeyCert       = 8,  // Downloads a public key certificate block
        LoadRootPublicKey       = 9   // Causes module to check and install stored Root public key
    };

    enum iswFirmwareCertificateType: uint8_t
    {
        Sub_CA = 0x00,
        SIGNER = 0x01
    };

    enum iswFirmwareLastBlock: uint8_t
    {
        NOT_LAST = 0x00,
        LAST     = 0x01
    };

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    std::string GetActiveFirmwareVersion(void)  { return (iswActiveFirmwareVersion); }
    std::string GetStandbyFirmwareVersion(void)  { return (iswStandbyFirmwareVersion); }
    std::string GetRootPublicKey(void) {return (iswRootPublicKey);}
    std::string GetPublicKeyCertificate(void) {return (iswPublicKeyCertificate);}
    bool GetWaitingActiveSelector(void)  { return (waitingActiveSelector); }
    bool RunCommandConfirmed(void)  { return (runCmdConfirmed); }
    bool ImageLoaded(void)  { return (imageLoaded); }

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendGetFirmwareVersionCmd(uint8_t selector);
    int SendRunImageCmd(void);
    int SendLoadImageCmd(std::string firmwareFilename, std::string signatureFilename, std::string label, uint8_t update_type);
    int SendLastLoadImageCmd(uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr);
    int SendLoadPublicKeyCertificateCmd(std::string publicKeyCertificateFilename, uint8_t certificate, std::string label);
    int SendLoadRootPublicKeyCmd();

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex iswFwCmdLock;

    //! Class variables
    //!
    //! Which firmware we are accessing for version info
    std::string iswActiveFirmwareVersion = "0";
    std::string iswStandbyFirmwareVersion = "0";
    std::string iswRootPublicKey = "";
    std::string iswPublicKeyCertificate = "";
    bool waitingActiveSelector = true;   //! false = waiting standy
    bool runCmdConfirmed = false;
    bool rootCertLoaded = false;
    bool publicCertLoaded = false;

    uint8_t iswApiVersion = 0;
    uint32_t iswVersionNumber = 0;

    //! Used in LoadImage
    bool receivedImageLoadConfirmation = false;
    bool ImageLoadLastBlock = false;

    //! Has image finished loading
    bool imageLoaded = false;

    //! ISW Firmware command structures
    struct iswGetImageVersionEvent
    {
        uint8_t	versionString[32];
        uint8_t apiVersion;
        uint8_t reserved[3];
        uint32_t versionNumber;
    };

    struct iswImageCrcAndLabel
    {
        uint8_t crc[4];
        uint8_t label[32];
    }__attribute__((packed));

    struct iswImageCrcAndLabelAndSignature
    {
        uint8_t crc[4];
        uint8_t label[32];
        uint8_t signature[64];
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware

    int GetFirmwareFileToLoad(std::string firmwareFilename, char **memblock, unsigned long *crc);
    int GetRootPublicKeyToLoad(std::string publicRootKeyFilename, char **memblock, unsigned long *crc);
    int GetPublicKeyCertificateToLoad(std::string publicKeyCertificateFilename, char **memblock, unsigned long *crc);

    void SetupGetFirmwareVersionCmd(iswFirmwareCommand *iswFirmwareCmd, uint8_t selector, void *routingHdr);
    void SetupRunImageCmd(iswFirmwareCommand *iswFirmwareCmd, void *routingHdr);
    void SetupLoadImageCmd(iswLoadImageCommand *iswLoadImageCmd, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr);
    void SetupLastLoadImageCmd(iswLastLoadImageCommand *iswLastLoadImageCmd, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr);
    void SetupLoadPublicKeyCertificateCmd(iswLoadPublicKeyCertCommand *iswLoadPublicKeyCertCmd, uint8_t certificate, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr);
    void SetupLoadRootPublicKeyCmd(iswLoadRootPublicKeyCommand *iswLoadRootPublicKeyCmd, void *routingHdr);
};

#endif //! ISWFIRMWARE_H
