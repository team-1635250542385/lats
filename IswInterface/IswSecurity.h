//!###############################################
//! Filename: IswSecurity.h
//! Description: Class that provides an interface
//!              to ISW Security commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWSECURITY_H
#define ISWSECURITY_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswSecurity
{

public:
    IswSecurity(IswInterface *isw_interface, Logger *logger);
    ~IswSecurity() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Security command structures
    struct iswAuthenticateRoleCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t role;
        uint8_t reserved2[3];
        uint8_t password[4];
    }__attribute__((packed));


    struct iswCryptoStartCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint32_t reserved2;
    }__attribute__((packed));

    struct iswCryptoGetStatusCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    bool IsAuthenticated(void) { return (iswAuthenticated);}
    bool IsInterfaceReady(void) { return (iswInterfaceReady);}

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendAuthenticateRoleCmd(void);
    int SendCryptoSessionStartCmd(void);
    int SendCryptpSessionGetStatusCmd(void);

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex IswSecurityCmdLock;

    //! Class variables
    uint8_t password[4] = {0};

    //! Used to indicate the board is ready
    //! authenticate role and crypto start successful
    bool iswAuthenticated = false;
    bool iswInterfaceReady = false;    

    //! ISW Security commands and events
    enum iswSecurityCmdEventTypes: uint8_t
    {
        ReservedSecurityCmd     = 0x00,
        AuthenticateRole        = 0x01,
        CryptoSessionStart      = 0x02,
        CryptoSessionGetStatus  = 0x03
    };

    struct iswCryptoGetStatusEvent
    {
        uint8_t role;
        uint8_t cryptoStarted;
    };

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupAuthenticateRoleCmd(iswAuthenticateRoleCommand *iswSecurityCmd, void *routingHdr);
    void SetupCryptoSessionStartCmd(iswCryptoStartCommand *iswSecurityCmd, void *routingHdr);
    void SetupCryptoSessionGetStatusCmd(iswCryptoGetStatusCommand *iswSecurityCmd, void *routingHdr);
};

#endif //! ISWSECURITY_H
