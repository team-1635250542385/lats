//!###############################################
//! Filename: IswLink.h
//! Description: Class that provides an interface
//!              to ISW Link commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWLINK_H
#define ISWLINK_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswLink
{

public:
    IswLink(IswInterface *isw_interface, Logger *logger);
    ~IswLink() {};

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Link command structures
    struct IswLinkCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct IswSetScanDutyCycleCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t startupScanDuration;
        uint8_t scanDutyCycle;
        uint8_t persist;
    }__attribute__((packed));

    struct IswSetMaxServiceIntervalCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint32_t maxServiceInterval;
    }__attribute__((packed));

    struct IswSetIdleScanFrequencyCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t idleScanFrequency;
        uint8_t persist;
    }__attribute__((packed));
    
    struct IswSetBackgroundScanParametersCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t stableScanFrequency;
        uint8_t persist;
        uint8_t version;
        uint16_t fluxScanFrequency;
        uint16_t fluxScanDuration;
    }__attribute__((packed));

    struct IswSetAntennaConfigurationCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t enable;
        uint8_t triggerAlgorithm;
        uint8_t triggerRssiThreshold;
        uint8_t triggerPhyRateThreshold;
        uint8_t switchThresholdAdder;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct IswGetAntennaConfigurationCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t triggerAlgorithm;
        uint8_t triggerRssiThreshold;
        uint8_t triggerPhyRateThreshold;
        uint8_t switchThresholdAdder;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct IswSetAntennaIdCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t antennaId;
        uint8_t reserved2[3];
    }__attribute__((packed));;

    struct IswGetAntennaIdCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct IswSetBandgroupBiasCommand // FW 30810 or Later
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t bgPrefer;
        uint8_t bgAvoid;
        uint8_t persist;
        uint8_t reserved2;
    }__attribute__((packed));

    struct IswSetPhyRatesCommand
    {
        uint8_t  cmdType;
        uint8_t  command;
        uint8_t  cmdContext;
        uint8_t  reserved1;
        uint32_t phyRateBitmap;
        uint8_t  persist;
        uint8_t  reserved2[3];
    }__attribute__((packed));;

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    uint16_t GetIswStartupScanDuration(void)  { return (iswStartupScanDuration); }
    uint8_t GetIswScanDutyCycle(void)  { return (iswScanDutyCycle); }
    uint32_t GetIswMaxServiceInterval(void)  { return (iswMaxServiceInterval); }
    uint16_t GetIswBackgroundScanParameters(void)  { return (iswStableScanFrequency); }
    uint8_t GetIswEnabled(void) { return (iswEnabled); }
    uint8_t GetIswTriggerAlgorithm(void) { return (iswTriggerAlgorithm); }
    uint8_t GetIswTriggerRssiThreshold(void) { return (iswTriggerRssiThreshold); }
    uint8_t GetIswTriggerPhyRateThreshold(void) { return (iswTriggerPhyRateThreshold); }
    uint8_t GetIswSwitchThresholdAdder(void) { return (iswSwitchThresholdAdder); }
    uint8_t GetIswGetAntennaIdEnabled(void) { return (iswGetAntennaId);}
    uint16_t GetIswIdleScanFrequency(void)  { return (iswIdleScanFrequency); }

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendGetScanDutyCycleCmd(void);
    int SendSetScanDutyCycleCmd(uint16_t startupScanDuration, uint8_t scanDutyCycle, uint8_t persist);
    int SendSetMaxServiceIntervalCmd(uint32_t maxServiceInterval);
    int SendResetMaxServiceIntervalCmd(void);
    int SendGetIdleScanFrequencyCmd(void);
    int SendSetIdleScanFrequencyCmd(uint16_t idleScanFrequency, uint8_t persist);
    int SendGetBackgroundScanParametersCmd(void);
    int SendSetBackgroundScanParametersCmd(uint16_t stableScanFrequency, uint8_t persist, uint8_t version, uint16_t fluxScanFrequency, uint16_t fluxScanDuration);
    int SendSetAntennaConfigurationCmd(uint8_t enable, uint8_t triggerAlgorithm, uint8_t triggerRssiThreshold, uint8_t triggerPhyRateThreshold, uint8_t switchThresholdAdder);
    int SendGetAntennaConfigurationCmd(void);
    int SendSetAntennaIdCmd(uint8_t antennaId);
    int SendGetAntennaIdCmd(void);
    int SendSetBandgroupBiasCmd(uint8_t bgPrefer, uint8_t bgAvoid, uint8_t persist);
    int SendGetBandgroupBiasCmd(void);

    int SendSetPhyRatesCmd(uint32_t phyRatesBitmap, uint8_t persist);
    int SendGetPhyRatesCmd(void);

    enum iswLinkEnableDisableAntennaDiversity: uint8_t
    {
        DISABLE_ANTENNA_DIVERSITY = 0x00,
        ENABLE_ANTENNA_DIVERSITY  = 0x01
    };

    enum iswLinkTriggerAlgorithm: uint8_t
    {
        RSSI_BELOW_THRESHOLD_OR_PHY_RATE_BELOW_THRESHOLD  = 0x00,
        RSSI_BELOW_THRESHOLD_AND_PHY_RATE_BELOW_THRESHOLD = 0x01
    };

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex iswLinkCmdLock;

    //! Class variables
    uint8_t iswScanDutyCycle           = 0;
    uint16_t iswStartupScanDuration    = 0;
    uint32_t iswMaxServiceInterval     = 0;
    uint16_t iswStableScanFrequency    = 0;
    uint16_t iswFluxScanFrequency      = 0;
    uint16_t iswFluxScanDuration       = 0;
    uint8_t iswEnabled                 = 0;
    uint8_t iswTriggerAlgorithm        = 0;
    uint8_t iswTriggerRssiThreshold    = 0;
    uint8_t iswTriggerPhyRateThreshold = 0;
    uint8_t iswSwitchThresholdAdder    = 0;
    uint8_t iswGetAntennaId            = 0;
    uint16_t iswIdleScanFrequency      = 0;
    uint8_t iswBgPrefer                = 0;
    uint8_t iswBgAvoid                 = 0;

    uint32_t phyRateBitmap             = 0;
    uint8_t persist                    = 0;

    //! ISW Link commands and events
    enum IswLinkCmdEventTypes: uint8_t
    {
        ReservedLinkCmd                  = 0,
        //! Reserved
        SetScanDutyCycle                 = 10,
        GetScanDutyCycle                 = 11,

        //! Reserved 14 - 15
//        SetIdleScanFrequency             = 16,
//        GetIdleScanFrequency             = 17,
        SetBackgroundScanParameters      = 16,
        GetBackgroundScanParameters      = 17,
        SetPhyRatesCmd                   = 18,
        GetPhyRatesCmd                   = 19,
        //! Reserved
        SetMaxServiceInterval            = 21,
        ResetMaxServiceInterval          = 22,
        SetAntennaConfiguration          = 112,
        GetAntennaConfiguration          = 113,
        SetAntennaId                     = 114,
        GetAntennaId                     = 115,
        SetBandgroupBias                 = 23,
        GetBandgroupBias                 = 24,
        //! Reserved

        //SetPhyRatesEvent                 = 8,
        //GetPhyRatesEvent                 = 8,
    };

    struct IswScanDutyCycleEvent
    {
        uint16_t startupScanDuration;
        uint8_t scanDutyCycle;
        uint8_t reserved;
    };

    struct IswSetAntennaConfigurationEvent
    {
        uint8_t enable;
        uint8_t triggerAlgorithm;
        uint8_t triggerRssiThreshold;
        uint8_t triggerPhyRateThreshold;
        uint8_t switchThresholdAdder;
        uint8_t reserved1;
    };

    struct IswGetAntennaConfigurationEvent
    {
        uint8_t enable;
        uint8_t triggerAlgorithm;
        uint8_t triggerRssiThreshold;
        uint8_t triggerPhyRateThreshold;
        uint8_t switchThresholdAdder;
        uint8_t reserved1;
    };

    struct IswGetAntennaIdEvent
    {
        uint8_t enabled;
        uint8_t reserved;
    };

    struct IswGetBackgroundScanParametersEvent
    {
        uint16_t stableScanFrequency;
        uint16_t reserved1;
        uint16_t fluxScanFrequency;
        uint16_t fluxScanDuration;
    }__attribute__((packed));

    struct IswGetBandGroupBiasEvent
    {
        uint8_t bgPrefer;
        uint8_t bgAvoid;
    }__attribute__((packed));


    struct IswGetPhyRatesEvent
    {
        uint32_t phyRateBitmap;
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupGetScanDutyCycleCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupSetScanDutyCycleCmd(IswSetScanDutyCycleCommand *setScanDutyCycleCmd,
                                  uint16_t startupScanDuration, uint8_t scanDutyCycle, uint8_t persist,
                                  void *routingHdr);
    void SetupSetMaxServiceIntervalCmd(IswSetMaxServiceIntervalCommand *setMaxServiceIntervalCmd, uint32_t maxServiceInterval, void *routingHdr);
    void SetupResetMaxServiceIntervalCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupGetBackgroundScanParametersCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupSetBackgroundScanParametersCmd(IswSetBackgroundScanParametersCommand *iswSetBackgroundScanParametersCmd, uint16_t stableScanFrequency, uint8_t persist, uint8_t version, uint16_t fluxScanFrequency, uint16_t fluxScanDuration, void *routingHdr);
    void SetupSetAntennaConfigurationCmd(IswSetAntennaConfigurationCommand *setAntennaConfigurationCmd, uint8_t enable, uint8_t triggerAlgorithm, uint8_t triggerRssiThreshold, uint8_t triggerPhyRateThreshold, uint8_t switchThresholdAdder, void *routingHdr);
    void SetupGetAntennaConfigurationCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupSetAntennaIdCmd(IswSetAntennaIdCommand *iswSetAntennaIdCmd, uint8_t antennaId, void *routingHdr);
    void SetupGetAntennaIdCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupGetIdleScanFrequencyCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupSetIdleScanFrequencyCmd(IswSetIdleScanFrequencyCommand *iswSetIdleScanFreqCmd, uint16_t idleScanFrequency, uint8_t persist, void *routingHdr);
    void SetupSetBandgroupBiasCmd(IswSetBandgroupBiasCommand* iswSetBandgroupBiasCmd, uint8_t bgPrefer, uint8_t bgAvoid, uint8_t persist, void *routingHdr);
    void SetupGetBandgroupBiasCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
    void SetupSetPhyRatesCmd(IswSetPhyRatesCommand *iswSetPhyRatesCmd, void *routingHdr, uint32_t phyRateBitmap, uint8_t persist);
    void SetupGetPhyRatesCmd(IswLinkCommand *iswLinkCmd, void *routingHdr);
};

#endif //! ISWLINK_H
