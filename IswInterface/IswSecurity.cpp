//!###############################################
//! Filename: IswSecurity.cpp
//! Description: Class that provides an interface
//!              to ISW Security commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswSecurity.h"
#include "IswInterface.h"
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//!###############################################################
//! Constructor: IswSecurity()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswSecurity::IswSecurity(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;

        //! Set password
        //! passPhrase is 'User' and password should be 0x55736572;
        //! TBD the password should be read in as part of the test setup data
        password[0] = 0x55;
        password[1] = 0x73;
        password[2] = 0x65;
        password[3] = 0x72;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswSecurity::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswSecurity::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
        case AuthenticateRole:
        {
            if ( theLogger->DEBUG_IswSecurity == 1 )
            {
                msgString = "Received AuthenticateRole Confirm";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswAuthenticated = true;
            break;
        }
        case CryptoSessionStart:
        {
            if ( theLogger->DEBUG_IswSecurity == 1 )
            {
                msgString = "Received CryptoSessionStart Confirm";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswInterfaceReady = true;
            break;
        }
        case CryptoSessionGetStatus:
        {
            if ( theLogger->DEBUG_IswSecurity == 1 )
            {
                msgString = "Received CryptoSessionGetStatus Confirm";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswSecurity == 1 )
            {
                msgString = "Bad Security Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! SetupAuthenticateRoleCmd()
//! Inputs: iswSecurityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSecurity::SetupAuthenticateRoleCmd(iswAuthenticateRoleCommand *iswSecurityCmd, void *routingHdr)
{
    std::string msgString;

    iswSecurityCmd->cmdType    = IswInterface::Security;
    iswSecurityCmd->command    = AuthenticateRole;
    iswSecurityCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSecurityCmd->reserved1  = 0x00;
    iswSecurityCmd->role       = 1;   //!0=None; 1=User; 2=CryptoOfficer; 161=Debug;
    *iswSecurityCmd->reserved2 = {0};
    memcpy(iswSecurityCmd->password, password, 4);

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswAuthenticateRoleCommand);

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SetupAuthenticateRole";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSecurityCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSecurityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSecurityCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSecurityCmd->reserved1) << std::endl;
        ss << "    role       = " << std::to_string(iswSecurityCmd->role) << std::endl;
        ss << "    reserved2  = ";
        for (int i = 0; i < 3; i++)
        {
            ss << std::to_string(iswSecurityCmd->reserved2[i]);
        }
        ss << std::endl;
        ss << "    password = " ;
        for (int i = 0; i < 4; i++)
        {
            ss << std::to_string(iswSecurityCmd->password[i]);
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendAuthenticateRoleCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSecurity::SendAuthenticateRoleCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SendAuthenticateRole";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = -1;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupAuthenticateRoleCmd(&iswCmd.iswAuthenticateRoleCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSecurity == 1 )
        {
            msgString = "SendAuthenticateRole failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupCryptoSessionStartCmd()
//! Inputs: iswSecurityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSecurity::SetupCryptoSessionStartCmd(iswCryptoStartCommand *iswSecurityCmd, void *routingHdr)
{
    std::string msgString;

    iswSecurityCmd->cmdType    = IswInterface::Security;
    iswSecurityCmd->command    = CryptoSessionStart;
    iswSecurityCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSecurityCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswCryptoStartCommand);

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SetupCryptoSessionStart";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSecurityCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSecurityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSecurityCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSecurityCmd->reserved1) << std::endl;
    }
}

//!###################################################################
//! SendCryptoSessionStartCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSecurity::SendCryptoSessionStartCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SendCryptoSessionStart";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupCryptoSessionStartCmd(&iswCmd.iswCryptoStartCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSecurity == 1 )
        {
            msgString = "SendCryptoSessionStart failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupCryptoSessionGetCmd()
//! Inputs: iswSecurityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSecurity::SetupCryptoSessionGetStatusCmd(iswCryptoGetStatusCommand *iswCryptoGetStatusCmd, void *routingHdr)
{
    std::string msgString;

    iswCryptoGetStatusCmd->cmdType    = IswInterface::Security;
    iswCryptoGetStatusCmd->command    = CryptoSessionGetStatus;
    iswCryptoGetStatusCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswCryptoGetStatusCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswCryptoGetStatusCommand);

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SetupCryptoSessionGetStatus";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswCryptoGetStatusCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswCryptoGetStatusCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswCryptoGetStatusCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswCryptoGetStatusCmd->reserved1) << std::endl;
    }
}

//!###################################################################
//! SendCryptoSessionStartCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSecurity::SendCryptpSessionGetStatusCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSecurity == 1 )
    {
        msgString = "SendCryptoSessionGetStatus";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupCryptoSessionGetStatusCmd(&iswCmd.iswCryptoGetStatusCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSecurity == 1 )
        {
            msgString = "SendCryptoSessionGetStatus failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}
