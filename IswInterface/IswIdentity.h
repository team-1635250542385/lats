//!###############################################
//! Filename: IswIdentity.h
//! Description: Class that provides an interface
//!              to ISW Identity commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWIDENTITY_H
#define ISWIDENTITY_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswIdentity
{

public:
    IswIdentity(IswInterface *isw_interface, Logger *logger);
    ~IswIdentity() {}

    //! These are used with external devices
    void SetDeviceTypeForExternal(uint16_t deviceType) { iswDeviceType = deviceType; }
    void SetMacAddrForExternal(std::string macAddressStr) { iswMacAddressStr = macAddressStr; }

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Identity command structures
    struct iswIdentityCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct iswSetCoordinatorCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t coordinatorCapable;
        uint8_t coordinatorPrecedence;
        uint8_t reserved2;
        uint8_t  persist;
    }__attribute__((packed));

    struct iswSetDeviceTypeCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t deviceType;
        uint16_t reserved2;
    }__attribute__((packed));

    enum iswDeviceTypes: uint16_t
    {
        ReservedDeviceTypes             = 0,            //!  0-0xFFFF not to be assigned
        TWS                             = 0x1000,       //!  Thermal Weapon Sight
        HMD                             = 0x1001,       //!  Helmet Mounted Display
        PHN                             = 0x1002,       //!  Smartphone/computer
        LRF                             = 0x1003,       //!  Laser Range Finder
        GPS                             = 0x1004,       //!  GPS device
        RLH                             = 0x1005,       //!  ISW-Mac Relay, USB Host connected
        RLD                             = 0x1006,       //!  ISW-Mac Relay, USB Device connected
        DPT                             = 0x1007,       //!  Depot device
        NET                             = 0x1008,       //!  ISW-Mac Network device
        ENC                             = 0x1009,       //!  Video encoder (camera)
        KPD                             = 0x100A,       //!  Keypad
        NES                             = 0x100B,       //!  ISW-Mac Network device – slave only
        HTR                             = 0x100C,       //!  Handheld tactical radio
        CHD                             = 0x100D,       //!  Command Mode Head Mounted Device
        GNM                             = 0x100E,       //!  Generic Coordinator-Capable Device
        GNS                             = 0x100F,       //!  Generic Peer Device (Non-Coordinator)
        ReservedLegacy                  = 0x1010,       //!  for legacy compatibility
        NET1                            = 0x1011,       //!  General Network Node Priority 1
        NET2                            = 0x1012,       //!  General Network Node Priority 2
        NET3                            = 0x1013,       //!  General Network Node Priority 3
        NET4                            = 0x1014,       //!  General Network Node Priority 4
        NET5                            = 0x1015,       //!  General Network Node Priority 5
        Generic_Biometric_Sensor        = 0x1016,       //!  (e.g. heart rate)
        Generic_Environmental_Sensor    = 0x1017,       //!  (e.g. humidity)
        Generic_Positioning_Sensor      = 0x1018,       //!  (e.g. GPS, altitude)
        //! N	Reserved for future definition
    };

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    std::string GetMacAddressStr(void)  { return (iswMacAddressStr); }
    uint8_t *GetMacAddress(void) { return (iswMacAddress); };
    uint16_t GetIswDeviceType(void)  { return (iswDeviceType); }
    uint8_t GetCoordinatorCapable(void)  { return (iswCoordinatorCapable); }
    uint8_t GetCoordinatorPrecedence(void)  { return (iswCoordinatorPrecedence); }
    uint8_t GetPersist(void)  { return (iswPersist); }
    static std::string GetDeviceTypeString(uint16_t);
    void GetDeviceTypeDefaults(uint16_t devType, uint8_t *iswCoordinatorCapable, uint8_t *iswCoordinatorPrecedence);

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendGetMacAddressCmd(void);
    int SendSetDeviceTypeCmd(uint16_t deviceType);
    int SendGetDeviceTypeCmd(void);
    int SendSetCoordinatorCmd(uint8_t coordinatorCapable, uint8_t coordinatorPrecedence, uint8_t persist);
    int SendGetCoordinatorCmd(void);

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex IswIdentityCmdLock;

    //! Class variables
    uint8_t iswMacAddress[6] = {0};
    std::string iswMacAddressStr = "12:34:56:78:9A:BC";
    uint8_t iswDeviceRole = 0x00;  //! 0x00=Master, 0x01=Slave
    uint16_t iswDeviceType = 0x0000;
    uint8_t iswCoordinatorCapable = 0;
    uint8_t iswCoordinatorPrecedence = 0;
    uint8_t iswPersist = 0;

    //! ISW Identity commands and events
    enum iswIdentityCmdEventTypes: uint8_t
    {
        ReservedIdentityCmd     = 0,
        SetDeviceType           = 1,
        GetDeviceType           = 2,
        ResrvdFuture1           = 3,
        GetMACAddress           = 4,
        SetCoordinator          = 5,
        GetCoordinator          = 6,
        ResrvdFuture2           = 7
    };

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupGetMacAddressCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr);
    void SetupSetDeviceTypeCmd(iswSetDeviceTypeCommand *iswSetDevTypeCmd, uint16_t deviceType, void *routingHdr);
    void SetupGetDeviceTypeCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr);
    void SetupSetCoordinatorCmd(iswSetCoordinatorCommand *iswSetCoordCmd, uint8_t coordinatorCapable,
                                uint8_t coordinatorPrecedence, uint8_t persist, void *routingHdr);
    void SetupGetCoordinatorCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr);

};

#endif //! ISWIDENTITY_H
