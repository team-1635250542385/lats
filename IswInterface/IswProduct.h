//!###############################################
//! Filename: IswProduct.h
//! Description: Class that provides an interface
//!              to ISW Product commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWPRODUCT_H
#define ISWPRODUCT_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswProduct
{

public:
    IswProduct(IswInterface *isw_interface, Logger *logger);
    ~IswProduct() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Product command structures
    struct iswProductCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reservedForAlignment;
    }__attribute__((packed));

    struct iswSetHibernationModeCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t hibernationMode;
        uint8_t persist;
        uint16_t idleTime;
        uint8_t softHibernationTime;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswSetObserverModeCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t enable;
        uint8_t rssiConnect;
        uint8_t rssiDisconnect;
        uint8_t phyRateCap;
        uint16_t devType;
        uint16_t reserved2;
    }__attribute__((packed));

    //!************************************************************************
    //! Ref: A3309771B
    //!
    //! Command used to enable/disable Audience Mode
    //!
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                   Command Type               |          1
    //!                   Command                    |          1
    //!                   Context ID                 |          1
    //!                   Reserved (Alignment)       |          1
    //!                   Enabled                    |          1
    //!                   Reserved                   |          3
    //!
    //! Total: 8 bytes
    //!
    //! The ISW Protocol Specification calls for 3 bytes within the Set
    //! Audience Mode command packet to be reserved for future use
    //!************************************************************************
    const static int SET_AUDIENCE_MODE_NUM_RESERVED_BYTES = 3;

    const static int AUDIENCE_MODE_ENABLE = 0x01;
    const static int AUDIENCE_MODE_DISABLE = 0x00;
    const static int OBSERVER_MODE_ENABLE = 0x01;
    const static int OBSERVER_MODE_DISABLE = 0x00;

    struct iswSetAudienceModeCommand
    {
        uint8_t commandType;
        uint8_t command;
        uint8_t contextId;
        uint8_t reservedForAlignment;
        uint8_t enabled;
        uint8_t reserved[SET_AUDIENCE_MODE_NUM_RESERVED_BYTES];

    }__attribute__((packed));

    //! Default value used for reserved fields in ISW Product structures
    const static int  ISW_PRODUCT_RESERVED_BYTE = 0x00;

    enum iswObserverModeSetting: uint8_t
    {
        ENABLE  = 0x01,
        DISABLE = 0x00
    };

    enum iswHibernationMode: uint8_t
    {
        DISABLE_HIBERNATION = 0,
        SOFT_HIBERNATION    = 1,
        HARD_HIBERNATION    = 2
    };

    enum iswPhyRateCap: uint8_t
    {
        Mbps_53_3  = 0,
        Mbps_80    = 1,
        Mbps_106_7 = 2,
        Mbps_160   = 3,
        Mbps_200   = 4,
        Mbps_320   = 5,
        Mbps_400   = 6,
        Mbps_480   = 7
    };

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    uint32_t GetIswNetworkId(void)  { return (iswNetworkId); }
    uint8_t GetIswHibernationMode(void) { return (iswHibernationMode); }
    uint16_t GetIswIdleTime(void) { return (iswIdleTime); }
    uint8_t softHibernationTime(void) { return (iswSoftHibernationTime); }
    uint8_t getAudienceModeStatus(void) {return (iswAudienceModeEnabled);}
    uint8_t getObserverModeStatus(void) {return (iswObserverModeEnabled);}

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendGetNetworkIdCmd(void);
    int SendSetObserverModeCmd(uint8_t type, uint8_t rssiConnect, uint8_t rssiDisconnect, uint8_t phyRateCap, uint16_t devType);
    int SendGetObserverModeCmd(void);
    int SendSetHibernationModeCmd(uint8_t hibernationMode, uint8_t persist, uint16_t idleTime, uint8_t softHibernationTime);
    int SendGetHibernationModeCmd(void);
    int SendSetAudienceModeCmd(uint8_t enable);
    int SendGetAudienceModeCmd(void);


ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex IswProductCmdLock;

    //! Class variables
    uint32_t iswNetworkId          = 0x000000;
    uint8_t iswHibernationMode     = 0x00;
    uint16_t iswIdleTime           = 0x0000;
    uint8_t iswSoftHibernationTime = 0x00;
    uint8_t iswReserved1           = 0x00;
    uint8_t iswAudienceModeEnabled = AUDIENCE_MODE_DISABLE;
    uint8_t iswObserverModeEnabled = OBSERVER_MODE_DISABLE;

    //! ISW Product commands and events
    enum iswProductCmdEventTypes: uint8_t
    {
        Reserved01      = 0,
        GetNetworkId    = 2,
        Reserved02      = 3,
        SetHibernation  = 18,
        GetHibernation  = 19,
        Reserved03      = 20,
        SetObserverMode = 0x20,
        GetObserverMode = 0x21,
        SetAudienceMode = 0x30,
        GetAudienceMode = 0x31,
        Reserved04      = 34
    };

    struct iswGetHibernationModeEvent
    {
        uint8_t hibernationMode;
        uint8_t reserved1;
        uint16_t idleTime;
        uint8_t softHibernationTime;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswGetObserverModeEvent
    {
        uint8_t iswEnable;
        uint8_t iswRssiConnect;
        uint8_t iswRssiDisconnect;
        uint8_t iswPhyRateCap;
        uint16_t iswDevType;
        uint16_t reserved;
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupGetNetworkIdCmd(iswProductCommand *iswProductCmd, void *routingHdr);
    void SetupSetObserverModeCmd(iswSetObserverModeCommand *iswSetObserverModeCmd, uint8_t type, uint8_t rssiConnect, uint8_t rssiDisconnect, uint8_t phyRateCap, uint16_t devType, void *routingHdr);
    void SetupGetObserverModeCmd(iswProductCommand *iswProductCmd, void *routingHdr);
    void SetupSetHibernationModeCmd(iswSetHibernationModeCommand *iswSetHibernationModeCmd, uint8_t hibernationMode, uint8_t persist, uint16_t idleTime, uint8_t softHibernationTime, void *routingHdr);
    void SetupGetHibernationModeCmd(iswProductCommand *iswProductCmd, void *routingHdr);
    void SetupGetAudienceModeCmd(iswProductCommand *cmd, void *routingHdr);
    void SetupSetAudienceModeCmd(iswSetAudienceModeCommand *cmd, void *routingHdr, uint8_t audienceModeStatus);

};

#endif //! ISWPRODUCT_H
