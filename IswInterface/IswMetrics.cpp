//!###############################################
//! Filename: IswMetrics.cpp
//! Description: Class that provides an interface
//!              to ISW Metrics commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswMetrics.h"
#include "IswInterface.h"
#include <sstream>

//!###############################################################
//! Constructor: IswMetrics()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswMetrics::IswMetrics(IswInterface *isw_interface, Logger *logger, DBLogger *dblogger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
        dbLogger = dblogger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswMetrics::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswMetrics::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
        case GetWirelessMetrics:
        {
            if ( theLogger->DEBUG_IswMetrics == 1 )
            {
                msgString = "Received GetWirelessMetrics confirm";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswMetricsGetWirelessMetrics *wirelessMetrics = (iswMetricsGetWirelessMetrics *)data;
            iswWirelessThroughput                         = wirelessMetrics->throughput;
            iswPhyRate                                    = wirelessMetrics->phyRate;

            if ( theLogger->DEBUG_IswMetrics == 1 )
            {
                std::stringstream ss;
                msgString.clear();
                ss << "  wireless throughput = " << std::to_string(iswWirelessThroughput) << std::endl;
                ss << "  phy rate            = " << std::to_string(iswPhyRate);
                msgString.append(ss.str());
                theLogger->DebugMessage(msgString, iswInterface->GetIndex());
            }

            //! Parse Peer Data
            uint8_t numPeers         = wirelessMetrics->numPeers;
            iswPeerMetrics *nextPeer = (iswPeerMetrics *)(data + sizeof(iswMetricsGetWirelessMetrics));

            IswStream *iswStream                                      = iswInterface->GetIswStream();
            std::array<IswStream::iswPeerRecord, MAX_PEERS> &iswPeers = iswStream->GetPeerRef();

            iswStream->PeerRecordLock();
            for (uint8_t i = 0; i < numPeers; i++)
            {
                iswPeerMetrics *peerMetrics = (iswPeerMetrics *)nextPeer;

                if ( iswPeers[peerMetrics->peerIndex].inUse )
                {
                    iswPeers[peerMetrics->peerIndex].metrics.phyRate = peerMetrics->phyRate;
                    iswPeers[peerMetrics->peerIndex].metrics.rssi    = peerMetrics->rssi;
                    SetRssi(peerMetrics->rssi);

                    iswPeers[peerMetrics->peerIndex].metrics.transmitThroughput = peerMetrics->transmitThroughput;
                    iswPeers[peerMetrics->peerIndex].metrics.receiveThroughput  = peerMetrics->receiveThroughput;

                    if ( iswPeers[peerMetrics->peerIndex].recordVersion == 4 )
                    {
                        iswPeers[peerMetrics->peerIndex].iswPeerRecord4.modePhyRate  = peerMetrics->phyRate;
                        iswPeers[peerMetrics->peerIndex].iswPeerRecord4.averageRSSI  = peerMetrics->rssi;
                        iswPeers[peerMetrics->peerIndex].iswPeerRecord4.txThroughput = peerMetrics->transmitThroughput;
                        iswPeers[peerMetrics->peerIndex].iswPeerRecord4.rxThroughput = peerMetrics->receiveThroughput;
                    }

                    if ( theLogger->DEBUG_IswMetrics == 1 )
                    {
                        //! Print out what we saved
                        std::stringstream ss;
                        msgString.clear();
                        ss << "  Peer " << std::to_string(peerMetrics->peerIndex) << std::endl;
                        ss << "    phy rate            = " << std::to_string(iswPeers[peerMetrics->peerIndex].metrics.phyRate) << std::endl;
                        ss << "    rssi                = " << std::to_string(iswPeers[peerMetrics->peerIndex].metrics.rssi) << std::endl;
                        ss << "    transmit throughput = " << std::to_string(iswPeers[peerMetrics->peerIndex].metrics.transmitThroughput) << std::endl;
                        ss << "    receive throughput  = " << std::to_string(iswPeers[peerMetrics->peerIndex].metrics.receiveThroughput) << std::endl;
                        msgString.append(ss.str());
                        theLogger->DebugMessage(msgString, iswInterface->GetIndex());
                    }
                }
                else
                {
                    if ( theLogger->DEBUG_IswMetrics == 1 )
                    {
                        msgString = "Trying to update a peer that is not in use";
                        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }
                }
                //! Go to next peer
                nextPeer += sizeof(iswPeerMetrics);
            }
            iswStream->PeerRecordUnlock();
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswMetrics == 1 )
            {
                msgString = "Bad System Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!#############################################################
//! GetPhyRate()
//! Inputs: None
//! Description: Returns the phyRate for this device in Mbps
//! #############################################################
float IswMetrics::GetPhyRate(void)
{
    std::string msgString;
    float phyRate = 0.0;

    //! Returns Mbps according to the
    //! ISW Embedment Guide Table 14.3.1.2
    switch (iswPhyRate)
    {
        case 0:
        {
            phyRate = 53.3;
            break;
        }
        case 1:
        {
            phyRate = 80.0;
            break;
        }
        case 2:
        {
            phyRate = 106.7;
            break;
        }
        case 3:
        {
            phyRate = 160.0;
            break;
        }
        case 4:
        {
            phyRate = 200.0;
            break;
        }
        case 5:
        {
            phyRate = 320.0;
            break;
        }
        case 6:
        {
            phyRate = 400.0;
            break;
        }
        case 7:
        {
            phyRate = 480.0;
            break;
        }
        case 0xFF:
        default:
        {
            if ( theLogger->DEBUG_IswMetrics == 1 )
            {
                msgString = "Bad phyRate";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }

    return ( phyRate);
}

//!###################################################################
//! SetupGetWirelessMetrics()
//! Inputs: iswMetricsCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswMetrics::SetupGetWirelessMetrics(iswMetricsCommand *iswMetricsCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswMetrics == 1 )
    {
        msgString = "SetupGetWirelessMetrics";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswMetricsCmd->cmdType    = IswInterface::Metrics;
    iswMetricsCmd->command    = GetWirelessMetrics;
    iswMetricsCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswMetricsCmd->reserved   = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswMetricsCommand);


    if ( theLogger->DEBUG_IswMetrics == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswMetricsCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswMetricsCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswMetricsCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswMetricsCmd->reserved) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetWirelessMetrics()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswMetrics::SendGetWirelessMetrics(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswMetrics == 1 )
    {
        msgString = "SendGetWirelessMetrics";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetWirelessMetrics(&iswCmd.iswMetricsCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswMetrics == 1 )
        {
            msgString = "SendGetWirelessMetrics failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}
