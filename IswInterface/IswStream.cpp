﻿//!###############################################
//! Filename: IswStream.cpp
//! Description: Class that provides an interface
//!              to ISW stream commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswStream.h"
#include "IswInterface.h"
#include <sstream>
#include <iomanip>
#include <string.h>

//!###############################################################
//! Constructor: IswStream()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswStream::IswStream(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;

        InitIswPeerRecords();
        InitIswMulticastRecords();
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswStream::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswStream::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
        case SetStreamsSpec:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received SetStreamsSpec";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetStreamsSpec:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received GetStreamsSpec";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswEventGetStreamSpec *getStreamsSpec = (iswEventGetStreamSpec *)data;

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                ss << "    peerIndex              = " << std::to_string(getStreamsSpec->peerIndex) << std::endl;
                ss << "    subStreamTxPriority    = " << std::to_string(getStreamsSpec->subStreamTxPriority) << std::endl;
                ss << "    subStreamTxDiscardable = " << std::to_string(getStreamsSpec->subStreamTxDiscardable) << std::endl;
                ss << "    reserved2              = " << std::to_string(getStreamsSpec->reserved2) << std::endl;
                ss << "    maxTxLatency           = " << std::to_string(getStreamsSpec->maxTxLatency) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case GetStreamsStatus:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received GetStreamsStatus";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventStreamsStatus *streamsStatus = (iswEventStreamsStatus *)data;

                //! Update globals
                iswPeerRecordLock.lock();

                //! GetStreamsStatus doesn't send peer records
                iswChannel = streamsStatus->channel;
                iswRole    = streamsStatus->role;
                iswPeerRecordLock.unlock();

                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    std::stringstream ss;
                    ss << "    channel = " << std::to_string(iswChannel) << std::endl <<
                          "    role    = " << std::to_string(iswRole);
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received StreamsStatus with bad data pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case GetExtendedStreamsStatus:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received GetExtendedStreamsStatus";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                 //! Now get the new information
                iswEventStreamsStatus *streamsStatus = (iswEventStreamsStatus *)data;

                //!Update globals
                iswPeerRecordLock.lock();
                iswPeerRecordVersion = streamsStatus->peerVersion;
                iswChannel           = streamsStatus->channel;
                iswRole              = streamsStatus->role;
                iswPeerRecordLock.unlock();

                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    std::stringstream ss;
                    ss << "    numPeers    = " << std::to_string(streamsStatus->numPeers) << std::endl <<
                          "    peerVersion = " << std::to_string(iswPeerRecordVersion) << std::endl <<
                          "    channel     = " << std::to_string(iswChannel) << std::endl <<
                          "    role        = " << std::to_string(iswRole);
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }

                //! Get the Peer Records in the data
                uint8_t *dataPtr = data + sizeof(iswEventStreamsStatus);
                for (int i = 0; i < streamsStatus->numPeers; i++)
                {
                    //! dataPtr gets updated to the next peer record
                    //! when ParsePeerRecords has completed
                    ParsePeerRecords(streamsStatus->peerVersion, dataPtr);

                    //! Update the pointer to the next record
                    switch (streamsStatus->peerVersion)
                    {
                        case 1:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version1);
                            break;
                        }
                        case 2:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version2);
                            break;
                        }
                        case 3:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version3);
                            break;
                        }
                        case 4:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version4);
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received StreamsStatus With Bad Data Pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case StartMulticastGroup:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received StartMulticastGroup";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventStartMulticastGroup *startMcastGroup = (iswEventStartMulticastGroup *)data;
                iswMulticastGroupRecord mcastGroupRecord;

                mcastGroupRecord.mcastPeerIndex = startMcastGroup->mcastPeerIndex;
                mcastGroupRecord.mcastStatus    = McastGroupOriginatedAndPaused;
                mcastGroupRecord.mcastAddress   = startMcastGroup->mcastAddress;
                mcastGroupRecord.mcastPhyRate   = startMcastGroup->mcastPhyRate;
                mcastGroupRecord.ownerPeerIndex = MCAST_MY_OWNER_PEER_INDEX;

                //! The host never sets this.  It comes from the firmware to let
                //! the app know if the link is backing up.  It's for monitoring only.
                mcastGroupRecord.txQueueDepth = 0;

                ParseMulticastGroupRecord(&mcastGroupRecord);
            }
            break;
        }
        case JoinMulticastGroup:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received JoinMulticastGroup";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                 iswEventJoinMulticastGroup *joinMcastGroup = (iswEventJoinMulticastGroup *)data;

                 if ( theLogger->DEBUG_IswStream == 1 )
                 {
                     std::stringstream ss;
                     ss << "    mcastAddress   = " << std::to_string(joinMcastGroup->mcastAddress) << std::endl;
                     ss << "    mcastPeerIndex = " << std::to_string(joinMcastGroup->mcastPeerIndex) << std::endl;
                     ss << "    mcastPhyRate   = " << std::to_string(joinMcastGroup->mcastPhyRate) << std::endl;
                     theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                 }
            }
            break;
        }
        case LeaveMulticastGroup:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received LeaveMulticastGroup";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                 iswEventLeaveMulticastGroup *leaveMcastGroup = (iswEventLeaveMulticastGroup *)data;

                 if ( theLogger->DEBUG_IswStream == 1 )
                 {
                     std::stringstream ss;
                     ss << "    mcastPeerIndex = " << std::to_string(leaveMcastGroup->mcastPeerIndex) << std::endl;
                     theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                 }

                 for (int i = 0; i < MAX_MCAST_RECORDS; i++)
                 {
                     if ( (iswMulticastGroupRecords[i].inUse) &&
                          (iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex == leaveMcastGroup->mcastPeerIndex) )
                     {
                             iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex = 0;
                             iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress   = 0;
                             iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus    = 0;
                             iswMulticastGroupRecords[i].iswMcastRecord.ownerPeerIndex = 0;
                             iswMulticastGroupRecords[i].iswMcastRecord.mcastPhyRate   = 0;
                             iswMulticastGroupRecords[i].iswMcastRecord.txQueueDepth   = 0;
                             iswMulticastGroupRecords[i].inUse = false;
                             break;
                     }
                 }
            }
            break;
        }
        case ModifyMulticastGroup:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received ModifyMulticastGroup";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                 iswEventModifyMulticastGroup *modifyMcastGroup = (iswEventModifyMulticastGroup *)data;

                 if ( theLogger->DEBUG_IswStream == 1 )
                 {
                     std::stringstream ss;
                     ss << "    mcastPeerIndex = " << std::to_string(modifyMcastGroup->mcastPeerIndex) << std::endl;
                     ss << "    mcastPhyRate   = " << std::to_string(modifyMcastGroup->mcastPhyRate) << std::endl;
                     theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                 }
            }
            break;
        }
        case GetMulticastGroupStatus:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received GetMulticastGroupStatus";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventMulticastGroupStatus *mcastGroupStatus = (iswEventMulticastGroupStatus *)data;

                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    std::stringstream ss;
                    ss << "    numMcastRecords = " << std::to_string(mcastGroupStatus->numMulticastRecords);
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }

                //! Process the MulticastGroupRecords in the data
                uint8_t *dataPtr = data + sizeof(iswEventMulticastGroupStatus);

                for (uint8_t i = 0; i < mcastGroupStatus->numMulticastRecords; i++)
                {
                    //! dataPtr gets updated to the next peer record
                    //! when ParseMulticastGroupRecords has completed
                    ParseMulticastGroupRecord((iswMulticastGroupRecord *)dataPtr);
                    dataPtr += sizeof(iswMulticastGroupRecord);
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received MulticastStatus with bad data pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case GetPeerFirmwareVersion:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received GetPeerFirmwareVersion";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            uint32_t peerFirmwareVersion = (uint32_t)data[0];

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                ss << "    peerFirmwareVersion = " << std::to_string(peerFirmwareVersion);
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case ConnectionIndication:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received ConnectionIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventStreamsConnectionInd *streamsConnectionInd = (iswEventStreamsConnectionInd *)data;

                uint16_t peerDeviceType          = 0x00FF & data[3];
                peerDeviceType                   = peerDeviceType << 8;
                peerDeviceType                   += data[2];
                streamsConnectionInd->deviceType = peerDeviceType;

                ParseConnectionIndication(streamsConnectionInd);
            };
            break;
        }
        case StreamsStatusIndication:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received StreamsStatusIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                //! Now update with new information
                iswEventStreamsStatus *streamsStatus = (iswEventStreamsStatus *)data;

                //! Update globals
                iswPeerRecordLock.lock();
                iswPeerRecordVersion = streamsStatus->peerVersion;
                iswChannel           = streamsStatus->channel;
                iswRole              = streamsStatus->role;
                iswPeerRecordLock.unlock();

                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    std::stringstream ss;
                    ss << "    numPeers    = " << std::to_string(streamsStatus->numPeers) << std::endl <<
                          "    peerVersion = " << std::to_string(iswPeerRecordVersion) << std::endl <<
                          "    channel     = " << std::to_string(iswChannel) << std::endl <<
                          "    role        = " << std::to_string(iswRole);
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }

                //! Get the Peer Records in the data
                uint8_t *dataPtr = data + sizeof(iswEventStreamsStatus);
                for (int i = 0; i < streamsStatus->numPeers; i++)
                {
                    //! dataPtr gets updated to the next peer record
                    //! when ParsePeerRecords has completed
                    ParsePeerRecords(streamsStatus->peerVersion, dataPtr);

                    //! Update the pointer to the next record
                    switch (streamsStatus->peerVersion)
                    {
                        case 1:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version1);
                            break;
                        }
                        case 2:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version2);
                            break;
                        }
                        case 3:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version3);
                            break;
                        }
                        case 4:
                        {
                            dataPtr += sizeof(iswPeerRecord_Version4);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received StreamsStatus with bad data pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case MulticastActivityIndication:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received MulticastActivityIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventMulticastActivity *mcastActivity = (iswEventMulticastActivity *)data;

                //! Process the MulticastActivity in the data
                UpdateMulticastGroupRecord(mcastActivity);
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received MulticastActivity with bad data pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case MulticastGroupsStatusIndication:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received MulticastGroupsStatusIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventMulticastGroupStatus *mcastGroupStatus = (iswEventMulticastGroupStatus *)data;

                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    std::stringstream ss;
                    ss << "    numMcastRecords = " << std::to_string(mcastGroupStatus->numMulticastRecords);
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }

                //! Process the MulticastGroupRecords in the data
                uint8_t *dataPtr = data + sizeof(iswEventMulticastGroupStatus);

                for (uint8_t i = 0; i < mcastGroupStatus->numMulticastRecords; i++)
                {
                    //! dataPtr gets updated to the next peer record
                    //! when ParseMulticastGroupRecords has completed
                    ParseMulticastGroupRecord((iswMulticastGroupRecord *)dataPtr);
                    dataPtr += sizeof(iswMulticastGroupRecord);
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswStream == 1 )
                {
                    msgString = "Received MulticastStatus With Bad Data Pointer";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case HibernationIndication:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Received HibernationIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( data != nullptr )
            {
                iswEventStreamsHibernationInd *streamsHibernationInd = (iswEventStreamsHibernationInd *)data;
                ParseHibernationIndication(streamsHibernationInd);
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Bad Stream Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!####################################################################
//! InitIswPeerRecords()
//! Inputs: None
//! Description: Set the ISW peer records in this class to default
//!              values.  Called during init, reset, clear association
//!####################################################################
void IswStream::InitIswPeerRecords(void)
{
    //! Arrays get created with garbage in them on Linux
    //! So initialize the memory to zero before we start
    //! populating from incoming ISW messages
    //! The peerRecord is a union of all the record versions
    //! so use the largest one
    iswPeerRecordLock.lock();
    for (auto itr = iswPeers.begin(); itr != iswPeers.end(); itr++)
    {
        iswPeerRecord peerRecord = *itr;

        peerRecord.recordVersion                = 0;
        peerRecord.inUse                        = false;
        peerRecord.isConnected                  = false;
        peerRecord.connectionTimestampMicrosecs = 0;
        memset(peerRecord.securityKey, 0, 16);
        memset(&peerRecord.metrics, 0, sizeof(peerMetrics));

        switch ( peerRecord.recordVersion )
        {
            case 1:
            {
                peerRecord.iswPeerRecord1.peerIndex    = 0;
                peerRecord.iswPeerRecord1.peerState    = 0;
                peerRecord.iswPeerRecord1.deviceType   = 0;
                peerRecord.iswPeerRecord1.linkStatus   = 0;
                peerRecord.iswPeerRecord1.txQueueDepth = 0;
                break;
            }
            case 2:
            {
                peerRecord.iswPeerRecord2.peerIndex   = 0;
                peerRecord.iswPeerRecord2.peerState   = 0;
                peerRecord.iswPeerRecord2.deviceType  = 0;
                peerRecord.iswPeerRecord2.linkStatus  = 0;
                peerRecord.iswPeerRecord2.linkQuality = 0;
                for (int j = 0; j < 6; j++)
                {
                    peerRecord.iswPeerRecord2.macAddress[j] = 0x00;
                }
                peerRecord.iswPeerRecord2.reserved2 = 0;break;
            }
            case 3:
            {
                peerRecord.iswPeerRecord3.peerIndex  = 0;
                peerRecord.iswPeerRecord3.peerState  = 0;
                peerRecord.iswPeerRecord3.deviceType = 0;
                peerRecord.iswPeerRecord3.linkStatus = 0;
                for (int j = 0; j < 6; j++)
                {
                    peerRecord.iswPeerRecord3.macAddress[j] = 0x00;
                }
                peerRecord.iswPeerRecord3.linkQuality  = 0;
                peerRecord.iswPeerRecord3.throughput   = 0;
                peerRecord.iswPeerRecord3.running      = 0;
                peerRecord.iswPeerRecord3.txQueueDepth = 0;
                peerRecord.iswPeerRecord3.transferSize = 0;
                peerRecord.iswPeerRecord3.reserved2    = 0;break;
                break;
            }
            case 4:
            {
                peerRecord.iswPeerRecord4.peerIndex    = 0;
                peerRecord.iswPeerRecord4.peerState    = 0;
                peerRecord.iswPeerRecord4.deviceType   = 0;
                peerRecord.iswPeerRecord4.linkStatus   = 0;
                peerRecord.iswPeerRecord4.linkQuality  = 0;
                peerRecord.iswPeerRecord4.txQueueDepth = 0;
                for (int j = 0; j < 6; j++)
                {
                    peerRecord.iswPeerRecord4.macAddress[j] = 0x00;
                }
                peerRecord.iswPeerRecord4.rxThroughput = 0;
                peerRecord.iswPeerRecord4.txThroughput = 0;
                peerRecord.iswPeerRecord4.averageRSSI  = 0;
                peerRecord.iswPeerRecord4.modePhyRate  = 0;
                break;
            }
            default:
                break;
            }
    }
    iswPeerRecordLock.unlock();
}

//!####################################################################
//! InitIswMulticastRecords()
//! Inputs: None
//! Description: Set the ISW multicast records in this class to default
//!              values.  Called during init, reset, clear association
//!####################################################################
void IswStream::InitIswMulticastRecords(void)
{
    iswMcastRecordLock.lock();
    for (auto itr = iswMulticastGroupRecords.begin(); itr != iswMulticastGroupRecords.end(); itr++)
    {
        multicastGroupRecords multiRecord = *itr;
        multiRecord.inUse = false;
        memset(&multiRecord.iswMcastRecord, 0, sizeof(iswMulticastGroupRecord));
    }
    iswMcastRecordLock.unlock();
}

//!####################################################################
//! ParsePeerRecords()
//! Inputs: peerVersion - version of the peer record
//!         dataPtr - pointer to the peer record
//! Description: This method parses one peer record and is called
//!              from the DeliverIswMethod method that is parsing an
//!              incoming message with multiple records
//!####################################################################
void IswStream::ParsePeerRecords(uint8_t peerVersion, uint8_t *dataPtr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "ParsePeerRecords";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    uint8_t peerIndex   = 0;
    uint8_t linkStatus  = 0;
    uint8_t peerState   = 0;
    uint8_t hibernating = 0;
    uint8_t role        = 0;

    iswPeerRecordLock.lock();
    switch (peerVersion)
    {
        case 1:
        {
            iswPeerRecord_Version1 *iswPeerRecordPtr = (iswPeerRecord_Version1 *)dataPtr;

            //! Used for logging below
            peerIndex  = iswPeerRecordPtr->peerIndex;
            peerState  = iswPeerRecordPtr->peerState & PEER_STATE_MASK;
            linkStatus = iswPeerRecordPtr->linkStatus;

            iswPeers[peerIndex].recordVersion = 1;
            iswPeers[peerIndex].inUse         = true;
            memcpy(&iswPeers[peerIndex].iswPeerRecord1, iswPeerRecordPtr, sizeof(iswPeerRecord_Version1));
            iswPeers[peerIndex].iswPeerRecord1.peerIndex = peerIndex;
            break;
        }
        case 2:
        {
            iswPeerRecord_Version2 *iswPeerRecordPtr = (iswPeerRecord_Version2 *)dataPtr;

            //! Used for logging below
            peerIndex  = iswPeerRecordPtr->peerIndex;
            peerState  = iswPeerRecordPtr->peerState & PEER_STATE_MASK;
            linkStatus = iswPeerRecordPtr->linkStatus;

            iswPeers[peerIndex].recordVersion = 2;
            iswPeers[peerIndex].inUse         = true;
            memcpy(&iswPeers[peerIndex].iswPeerRecord2, iswPeerRecordPtr, sizeof(iswPeerRecord_Version2));
            iswPeers[peerIndex].iswPeerRecord2.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord2.linkStatus = linkStatus;
            iswPeers[peerIndex].iswPeerRecord2.peerState  = peerState;
            iswLinkQuality                                = iswPeers[peerIndex].iswPeerRecord2.linkQuality;

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                ss << "    recordVersion        = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].recordVersion) << std::endl <<
                      "    inUse                = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].inUse) << std::endl <<
                ss << "    record2.peerState    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.peerState) << std::endl <<
                ss << "    record2.peerIndex    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.peerIndex) << std::endl <<
                ss << "    record2.deviceType   = " << std::hex << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.deviceType << std::endl <<
                ss << "    record2.linkStatus   = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.linkStatus) << std::endl <<
                ss << "    record2.linkQuality  = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.linkQuality) << std::endl <<
                ss << "    record2.txQueueDepth = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.txQueueDepth) << std::endl <<
                ss << "    record2.macAddress   = ";

                std::stringstream macAddress;
                macAddress.str("");
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord2.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }

                ss << macAddress.str();
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case 3:
        {
            iswPeerRecord_Version3 *iswPeerRecordPtr = (iswPeerRecord_Version3 *)dataPtr;

            //! Used for logging below
            peerIndex  = iswPeerRecordPtr->peerIndex;
            peerState  = iswPeerRecordPtr->peerState & PEER_STATE_MASK;
            linkStatus = iswPeerRecordPtr->linkStatus;

            iswPeers[peerIndex].recordVersion = 3;
            iswPeers[peerIndex].inUse         = true;
            memcpy(&iswPeers[peerIndex].iswPeerRecord3, iswPeerRecordPtr, sizeof(iswPeerRecord_Version3));
            iswPeers[peerIndex].iswPeerRecord3.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord3.linkStatus = linkStatus;
            iswPeers[peerIndex].iswPeerRecord3.peerState  = peerState;
            iswLinkQuality                                = iswPeers[peerIndex].iswPeerRecord3.linkQuality;

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                ss << "    recordVersion        = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].recordVersion) << std::endl <<
                      "    inUse                = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].inUse) << std::endl <<
                ss << "    record3.peerIndex    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.peerIndex) << std::endl <<
                ss << "    record3.peerState    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.peerState) << std::endl <<
                ss << "    record3.deviceType   = " << std::hex << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.deviceType << std::endl <<
                ss << "    record3.linkStatus   = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.linkStatus) << std::endl <<
                ss << "    record3.linkQuality  = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.linkQuality) << std::endl <<
                ss << "    record3.txQueueDepth = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.txQueueDepth) << std::endl <<
                ss << "    record3.macAddress   = ";

                std::stringstream macAddress;
                macAddress.str("");
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord3.macAddress[i];
                    if ( i<5 )
                    {
                        macAddress << ":";
                    }
                }

                ss << macAddress.str();
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
         }
        case 4:
        {
            iswPeerRecord_Version4 *iswPeerRecordPtr = (iswPeerRecord_Version4 *)dataPtr;

            //! Used for logging below
            peerIndex  = iswPeerRecordPtr->peerIndex;
            peerState  = iswPeerRecordPtr->peerState & PEER_STATE_MASK;
            linkStatus = iswPeerRecordPtr->linkStatus;

            iswPeers[peerIndex].recordVersion = 4;
            iswPeers[peerIndex].inUse         = true;
            memcpy(&iswPeers[peerIndex].iswPeerRecord4, iswPeerRecordPtr, sizeof(iswPeerRecord_Version4));
            iswPeers[peerIndex].iswPeerRecord4.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord4.linkStatus = linkStatus;
            iswPeers[peerIndex].iswPeerRecord4.peerState  = peerState;
            iswLinkQuality                                = iswPeers[peerIndex].iswPeerRecord4.linkQuality;

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                ss << "    recordVersion        = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].recordVersion) << std::endl <<
                      "    inUse                = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].inUse) << std::endl <<
                ss << "    record4.peerIndex    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.peerIndex) << std::endl <<
                ss << "    record4.peerState    = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.peerState) << std::endl <<
                ss << "    record4.deviceType   = " << std::hex << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.deviceType << std::endl <<
                ss << "    record4.linkStatus   = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.linkStatus) << std::endl <<
                ss << "    record4.linkQuality  = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.linkQuality) << std::endl <<
                ss << "    record4.txQueueDepth = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.txQueueDepth) << std::endl <<
                ss << "    record4.txThroughput = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.txThroughput) << std::endl <<
                ss << "    record4.rxThroughput = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.rxThroughput) << std::endl <<
                ss << "    record4.modePhyRate  = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.modePhyRate) << std::endl <<
                ss << "    record4.averageRSSI  = " << std::to_string(iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.averageRSSI) << std::endl <<
                ss << "    record4.macAddress   = ";

                std::stringstream macAddress;
                macAddress.str("");
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[iswPeerRecordPtr->peerIndex].iswPeerRecord4.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }

                ss << macAddress.str();
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Bad Peer Record Version";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }

    msgString.clear();
    msgString = "    peer ";
    msgString.append(std::to_string(peerIndex));
    if ( (linkStatus & CONNECTED) == CONNECTED )
    {
        iswPeers[peerIndex].isConnected = true;
        msgString.append(" connected");
    }
    else if ( (linkStatus & DISCONNECTED) == DISCONNECTED )
    {
        iswPeers[peerIndex].isConnected = false;
        msgString.append(" disconnected");
    }
    else
    {
        // Future development - bits not defined yet
        iswPeers[peerIndex].isConnected = false;
        msgString.append(" disconnected");
    }
    iswPeerRecordLock.unlock();

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!####################################################################
//! ParseConnectionIndication()
//! Inputs: streamsConnectionInd - pointer to ISW command structure
//! Description: This method parses the ISW Connection Indication
//!              message and updates the iswPeers data
//!####################################################################
void IswStream::ParseConnectionIndication(iswEventStreamsConnectionInd *streamsConnectionInd)
{
    //! Update the connection time with this peer
    uint64_t currentTimeMicrosecs = theLogger->GetTimeStamp();

    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "ParseConnectionIndication";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    uint8_t peerIndex = streamsConnectionInd->peerIndex;
    //! Update the peer record
    iswPeerRecordLock.lock();
    iswPeers[peerIndex].inUse = true;

    if ( streamsConnectionInd->linkStatus >= 1 )
    {
        IswAssociation *iswAssociation         = iswInterface->GetIswAssociation();
        uint64_t startAssociationTimeMicrosecs = iswAssociation->GetStartAssociationTime();
        if ( (startAssociationTimeMicrosecs != 0) &&
             (currentTimeMicrosecs > startAssociationTimeMicrosecs) )
        {
           iswPeers[peerIndex].connectionTimestampMicrosecs = currentTimeMicrosecs;
        }

        // Set isConnected
        iswPeers[peerIndex].isConnected = true;
    }
    else
    {
        iswPeers[peerIndex].isConnected                  = false;
        iswPeers[peerIndex].connectionTimestampMicrosecs = 0;
    }

    uint8_t coordinatorCapable    = 0;
    uint8_t coordinatorPrecedence = 0;

    switch ( iswPeers[peerIndex].recordVersion )
    {
        case 1:
        {
            iswPeers[peerIndex].iswPeerRecord1.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord1.linkStatus = streamsConnectionInd->linkStatus;
            iswPeers[peerIndex].iswPeerRecord1.deviceType = streamsConnectionInd->deviceType;
            break;
        }
        case 2:
        {
            iswPeers[peerIndex].iswPeerRecord2.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord2.linkStatus = streamsConnectionInd->linkStatus;
            iswPeers[peerIndex].iswPeerRecord2.deviceType = streamsConnectionInd->deviceType;
            memcpy(&iswPeers[peerIndex].iswPeerRecord2.macAddress, &streamsConnectionInd->macAddress, sizeof(iswPeers[peerIndex].iswPeerRecord2.macAddress));

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                macAddress.str(""); //!Clear string

                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord2.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record3.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord2.deviceType << std::endl;
                ss << "    Link status        = " << std::to_string(iswPeers[peerIndex].iswPeerRecord2.linkStatus) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());

                //! Use the next call to print out the device type name in the logfile
                iswInterface->GetIswIdentity()->GetDeviceTypeDefaults(iswPeers[peerIndex].iswPeerRecord2.deviceType, &coordinatorCapable, &coordinatorPrecedence);
            }
            break;
        }
        case 3:
        {
            iswPeers[peerIndex].iswPeerRecord3.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord3.linkStatus = streamsConnectionInd->linkStatus;
            iswPeers[peerIndex].iswPeerRecord3.deviceType = streamsConnectionInd->deviceType;
            memcpy(&iswPeers[peerIndex].iswPeerRecord3.macAddress, &streamsConnectionInd->macAddress, sizeof(iswPeers[peerIndex].iswPeerRecord3.macAddress));

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                macAddress.str(""); //!Clear string

                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord3.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record3.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord3.deviceType << std::endl;
                ss << "    Link status        = " << std::to_string(iswPeers[peerIndex].iswPeerRecord3.linkStatus) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());

                //! Use the next call to print out the device type name in the logfile
                iswInterface->GetIswIdentity()->GetDeviceTypeDefaults(iswPeers[peerIndex].iswPeerRecord3.deviceType, &coordinatorCapable, &coordinatorPrecedence);
            }
            break;
        }
        case 4:
        {
            iswPeers[peerIndex].iswPeerRecord4.peerIndex  = peerIndex;
            iswPeers[peerIndex].iswPeerRecord4.linkStatus = streamsConnectionInd->linkStatus;
            iswPeers[peerIndex].iswPeerRecord4.deviceType = streamsConnectionInd->deviceType;
            memcpy(&iswPeers[peerIndex].iswPeerRecord4.macAddress, &streamsConnectionInd->macAddress, sizeof(iswPeers[peerIndex].iswPeerRecord4.macAddress));

            if ( theLogger->DEBUG_IswStream == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                macAddress.str(""); //!Clear string

                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord4.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record4.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord4.deviceType << std::endl;
                ss << "    Link status        = " << std::to_string(iswPeers[peerIndex].iswPeerRecord4.linkStatus) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                //! Use the next call to print out the device type name in the logfile
                iswInterface->GetIswIdentity()->GetDeviceTypeDefaults(iswPeers[peerIndex].iswPeerRecord4.deviceType, &coordinatorCapable, &coordinatorPrecedence);
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswStream == 1 )
            {
                msgString = "Bad Peer Record Version";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "    Peer index = ";
        msgString.append(std::to_string(peerIndex));
        msgString.append("\n");
        msgString.append("    inUse = ");
        msgString.append(std::to_string(iswPeers[peerIndex].inUse));
        msgString.append("\n");
        msgString.append("    isConnected = ");
        msgString.append(std::to_string(iswPeers[peerIndex].isConnected));
        msgString.append("\n");
        msgString.append("    connectionTimestampMicrosecs = ");
        msgString.append(std::to_string(iswPeers[peerIndex].connectionTimestampMicrosecs));
        msgString.append("\n");
        msgString.append("    Record Version = ");
        msgString.append(std::to_string(iswPeers[peerIndex].recordVersion));
        msgString.append("\n");
        theLogger->DebugMessage(msgString, iswInterface->GetIndex());
    }

    msgString.clear();
    msgString = "    peer ";
    msgString.append(std::to_string(peerIndex));
    if ( (streamsConnectionInd->linkStatus & CONNECTED) == CONNECTED )
    {
        msgString.append(" connected");

        //! Get the NetworkId if the status is connected
        iswInterface->GetIswProduct()->SendGetNetworkIdCmd();

    }
    else if ( (streamsConnectionInd->linkStatus & DISCONNECTED) == DISCONNECTED )
    {
        msgString.append(" disconnected");
    }
    else
    {
        // Future - bits not defined yet
        msgString.append(" disconnected");
    }

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswPeerRecordLock.unlock();
}

//!####################################################################
//! ParseHibernationIndication()
//! Inputs: iswEventStreamsHibernationInd - pointer to ISW command structure
//! Description: This method parses the ISW Connection Indication
//!              message and updates the iswPeers data
//!####################################################################
void IswStream::ParseHibernationIndication(iswEventStreamsHibernationInd *streamsHibernationInd)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "ParseHibernationIndication";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswPeerIndex   = streamsHibernationInd->peerIndex;
    iswHibernating = streamsHibernationInd->hibernating;
    iswDuration    = streamsHibernationInd->duration;

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    peerIndex   = " << std::to_string(iswPeerIndex) << std::endl <<
        ss << "    hibernating = " <<  std::to_string(iswHibernating) << std::endl;
        ss << "    duration    = " << std::to_string(iswDuration) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!####################################################################
//! ParseMulticastGroupRecord()
//! Inputs: dataPtr - pointer to ISW iswMulticastGroupRecord
//! Description: This method parses the ISW Multicast Group Record
//!              message and updates stored multicast records
//!####################################################################
void IswStream::ParseMulticastGroupRecord(iswMulticastGroupRecord *dataPtr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "ParseMulticastGroupRecord";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Get address of data coming in
    iswMulticastGroupRecord *mcastGroupRecord = (iswMulticastGroupRecord *)dataPtr;

    iswMcastRecordLock.lock();
    bool addedMcastGroup = false;
    int index            = 0;

    //! Check if we already have a record for this peerIndex
    for (int i = 0; i < MAX_MCAST_RECORDS; i++)
    {
        if ( (iswMulticastGroupRecords[i].inUse) &&
             (iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex == mcastGroupRecord->mcastPeerIndex) )
        {
            iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress   = mcastGroupRecord->mcastAddress;
            iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus    = mcastGroupRecord->mcastStatus;
            iswMulticastGroupRecords[i].iswMcastRecord.ownerPeerIndex = mcastGroupRecord->ownerPeerIndex;
            iswMulticastGroupRecords[i].iswMcastRecord.mcastPhyRate   = mcastGroupRecord->mcastPhyRate;
            iswMulticastGroupRecords[i].iswMcastRecord.txQueueDepth   = mcastGroupRecord->txQueueDepth;
            addedMcastGroup                                           = true;
            index                                                     = i;
            break;
        }
    }

    if ( !addedMcastGroup )
    {
        for (int i = 0; i < MAX_MCAST_RECORDS; i++)
        {
            if ( !iswMulticastGroupRecords[i].inUse )
            {
                iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex = mcastGroupRecord->mcastPeerIndex;
                iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus    = mcastGroupRecord->mcastStatus;
                iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress   = mcastGroupRecord->mcastAddress;
                iswMulticastGroupRecords[i].iswMcastRecord.ownerPeerIndex = mcastGroupRecord->ownerPeerIndex;
                iswMulticastGroupRecords[i].iswMcastRecord.mcastPhyRate   = mcastGroupRecord->mcastPhyRate;
                iswMulticastGroupRecords[i].iswMcastRecord.txQueueDepth   = mcastGroupRecord->txQueueDepth;
                iswMulticastGroupRecords[i].inUse                         = true;
                addedMcastGroup                                           = true;
                index                                                     = i;
                break;
            }
        }
    }
    iswMcastRecordLock.unlock();

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        std::string indexString = std::to_string(index);

        if ( addedMcastGroup )
        {
            ss << "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastPeerIndex = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastPeerIndex) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastStatus    = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastStatus) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastAddress   = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastAddress) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].ownerPeerIndex = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.ownerPeerIndex) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastPhyRate   = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastPhyRate) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].txQueueDepth   = " << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.txQueueDepth);
        }
        else
        {
            ss << "    Unable to add MulticastGroupRecord for " << std::to_string(mcastGroupRecord->mcastPeerIndex);
        }
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!####################################################################
//! UpdateMulticastGroupRecord()
//! Inputs: mcastActivity - pointer to ISW Multicast Activity structure
//! Description: This method parses updates stored multicast records
//!####################################################################
void IswStream::UpdateMulticastGroupRecord(iswEventMulticastActivity *mcastActivity)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "UpdateMulticastGroupRecord";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Update Mcast Group Records array
    //! We already have info on this mcast group so update it
    bool updatedMcastRecord = false;
    uint8_t index           = 0;

    iswMcastRecordLock.lock();
    for (uint8_t i = 0; i < MAX_MCAST_RECORDS; i++)
    {
        if ( (iswMulticastGroupRecords[i].inUse) &&
             (iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex == mcastActivity->mcastPeerIndex) )
        {
            iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress = mcastActivity->mcastAddress;
            iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus  = mcastActivity->activityType;
            updatedMcastRecord                                      = true;
            index                                                   = i;
            break;
        }
    }

    if ( !updatedMcastRecord )
    {
        for (uint8_t i = 0; i < MAX_MCAST_RECORDS; i++)
        {
            if ( !iswMulticastGroupRecords[i].inUse )
            {
                iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex = mcastActivity->mcastPeerIndex;
                iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress   = mcastActivity->mcastAddress;
                iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus    = mcastActivity->activityType;
                iswMulticastGroupRecords[i].iswMcastRecord.ownerPeerIndex = 0;
                iswMulticastGroupRecords[i].iswMcastRecord.txQueueDepth   = 1;
                updatedMcastRecord                                        = true;
                index                                                     = i;
                break;
            }
        }
    }
    iswMcastRecordLock.unlock();

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        std::string indexString = std::to_string(index);

        if ( updatedMcastRecord )
        {
            ss << "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastPeerIndex=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastPeerIndex) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastStatus=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastStatus) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastAddress=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastAddress) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].ownerPeerIndex=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.ownerPeerIndex) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].mcastPhyRate=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.mcastPhyRate) << std::endl <<
                  "    iswMulticastGroupRecords[" << indexString.c_str() << "].txQueueDepth=" << std::to_string(iswMulticastGroupRecords[index].iswMcastRecord.txQueueDepth);
        }
        else
        {
            ss << "    Unable to update MulticastGroupRecord for " << std::to_string(mcastActivity->mcastPeerIndex);
        }
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SetupGetStreamsStatusCmd()
//! Inputs: iswStreamCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupGetStreamsStatusCmd(iswStreamCommand *iswStreamCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupGetStreamsStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswStreamCmd->cmdType    = IswInterface::Stream;
    iswStreamCmd->command    = GetStreamsStatus;
    iswStreamCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswStreamCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStreamCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswStreamCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswStreamCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswStreamCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswStreamCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetStreamsStatusCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendGetStreamsStatusCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendGetStreamsStatus";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetStreamsStatusCmd(&iswCmd.iswStreamCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendGetStreamsStatus failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetExtendedStreamsStatusCmd()
//! Inputs: iswExtendedStreamCmd - pointer to the command structure
//!         version - version of streams status message
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupGetExtendedStreamsStatusCmd(iswExtendedStreamCommand *iswExtendedStreamCmd, uint8_t version, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupGetExtendedStreamsStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswExtendedStreamCmd->cmdType      = IswInterface::Stream;
    iswExtendedStreamCmd->command      = GetExtendedStreamsStatus;
    iswExtendedStreamCmd->cmdContext   = iswInterface->GetNextCmdCount();
    iswExtendedStreamCmd->reserved1    = 0x00;
    iswExtendedStreamCmd->version      = version;
    iswExtendedStreamCmd->reserved2[0] = 0;
    iswExtendedStreamCmd->reserved2[1] = 0;
    iswExtendedStreamCmd->reserved2[2] = 0;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswExtendedStreamCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswExtendedStreamCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswExtendedStreamCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswExtendedStreamCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswExtendedStreamCmd->reserved1) << std::endl;
        ss << "    version    = " << std::to_string(iswExtendedStreamCmd->version) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetExtendedStreamsStatusCmd()
//! Inputs: version - version of the Streams Status record
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendGetExtendedStreamsStatusCmd(uint8_t version)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendGetExtendedStreamsStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetExtendedStreamsStatusCmd(&iswCmd.iswExtendedStreamCmd, version, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendGetExtendedStreamsStatusCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetStreamsSpecCmd()
//! Inputs: iswSetStreamSpecCmd - pointer to the command structure
//!         peerIndex    - ISW representation of the peer to this node
//!         txPriority   - substream transmit priority bitmap
//!                        (use bits 0-3) where a bit set to
//!                        1 = High Priority
//!         txDiscard    - substream transmit priority bitmap
//!                        (use bits 0-3) where a bit set to
//!                        1 = firmware may automatically discard data
//!                        in the case of network congestion
//!         maxTxLatency - Max duration in microseconds between
//!                        transmit opportunities
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupSetStreamsSpecCmd(iswSetStreamSpecCommand *iswSetStreamSpecCmd, uint8_t peerIndex, uint8_t txPriority,
                            uint8_t txDiscard, uint32_t maxTxLatency, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupSetStreamsSpecCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetStreamSpecCmd->cmdType                = IswInterface::Stream;
    iswSetStreamSpecCmd->command                = SetStreamsSpec;
    iswSetStreamSpecCmd->cmdContext             = iswInterface->GetNextCmdCount();
    iswSetStreamSpecCmd->reserved1              = 0x00;
    iswSetStreamSpecCmd->peerIndex              = peerIndex;
    iswSetStreamSpecCmd->subStreamTxPriority    = txPriority;
    iswSetStreamSpecCmd->subStreamTxDiscardable = txDiscard;
    iswSetStreamSpecCmd->reserved2              = 0x00;
    iswSetStreamSpecCmd->maxTxLatency           = maxTxLatency;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSetStreamSpecCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType                = " << std::to_string(iswSetStreamSpecCmd->cmdType) << std::endl;
        ss << "    command                = " << std::to_string(iswSetStreamSpecCmd->command) << std::endl;
        ss << "    cmdContext             = " << std::to_string(iswSetStreamSpecCmd->cmdContext) << std::endl;
        ss << "    reserved1              = " << std::to_string(iswSetStreamSpecCmd->reserved1) << std::endl;
        ss << "    peerIndex              = " << std::to_string(iswSetStreamSpecCmd->peerIndex) << std::endl;
        ss << "    subStreamTxPriority    = " << std::to_string(iswSetStreamSpecCmd->subStreamTxPriority) << std::endl;
        ss << "    subStreamTxDiscardable = " << std::to_string(iswSetStreamSpecCmd->subStreamTxDiscardable) << std::endl;
        ss << "    reserved2              = " << std::to_string(iswSetStreamSpecCmd->reserved2) << std::endl;
        ss << "    maxTxLatency           = " << std::to_string(iswSetStreamSpecCmd->maxTxLatency) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetStreamsSpecCmd()
//! Inputs: peerIndex    - ISW representation of the peer to this node
//!         txPriority   - substream transmit priority bitmap
//!                        (use bits 0-3) where a bit set to
//!                        1 = High Priority
//!         txDiscard    - substream transmit priority bitmap
//!                        (use bits 0-3) where a bit set to
//!                        1 = firmware may automatically discard data
//!                        in the case of network congestion
//!         maxTxLatency - Max duration in microseconds between
//!                        transmit opportunities
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendSetStreamsSpecCmd(uint8_t peerIndex, uint8_t txPriority, uint8_t txDiscard, uint32_t maxTxLatency)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendSetStreamsSpecCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetStreamsSpecCmd(&iswCmd.iswSetStreamSpecCmd, peerIndex, txPriority, txDiscard, maxTxLatency, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendSetStreamsSpecCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetStreamsSpecCmd()
//! Inputs: iswGetStreamSpecCmd - pointer to the command structure
//!         peerIndex - ISW representation of the peer to this node
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupGetStreamsSpecCmd(iswGetStreamSpecCommand *iswGetStreamSpecCmd, uint8_t peerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupGetStreamsSpecCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswGetStreamSpecCmd->cmdType      = IswInterface::Stream;
    iswGetStreamSpecCmd->command      = GetStreamsSpec;
    iswGetStreamSpecCmd->cmdContext   = iswInterface->GetNextCmdCount();
    iswGetStreamSpecCmd->reserved1    = 0x00;
    iswGetStreamSpecCmd->peerIndex    = peerIndex;
    iswGetStreamSpecCmd->reserved2[0] = 0x00;
    iswGetStreamSpecCmd->reserved2[1] = 0x00;
    iswGetStreamSpecCmd->reserved2[2] = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswGetStreamSpecCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswGetStreamSpecCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswGetStreamSpecCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswGetStreamSpecCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswGetStreamSpecCmd->reserved1) << std::endl;
        ss << "    peerIndex  = " << std::to_string(iswGetStreamSpecCmd->peerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetStreamSpecCmd()
//! Inputs: peerIndex - ISW representation of the peer to this node
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendGetStreamSpecCmd(uint8_t peerIndex)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendGetStreamsSpecCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetStreamsSpecCmd(&iswCmd.iswGetStreamSpecCmd, peerIndex, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendGetStreamsSpecCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetMulticastGroupsStatusCmd()
//! Inputs: iswStreamCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupGetMulticastGroupsStatusCmd(iswStreamCommand *iswStreamCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupGetMulticastGroupsStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswStreamCmd->cmdType    = IswInterface::Stream;
    iswStreamCmd->command    = GetMulticastGroupStatus;
    iswStreamCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswStreamCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStreamCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswStreamCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswStreamCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswStreamCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswStreamCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetMulticastGroupsStatusCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendGetMulticastGroupsStatusCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendGetMulticastGroupStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetMulticastGroupsStatusCmd(&iswCmd.iswStreamCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendGetMulticastGroupStatusCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetPeerFirmwareVersionCmd()
//! Inputs: getPeerFirmwareVersionCmd - pointer to the command structure
//!         peerIndex - ISW representation of the peer to this node
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupGetPeerFirmwareVersionCmd(iswGetPeerFirmwareVersionCommand *getPeerFirmwareVersionCmd, uint8_t peerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupGetPeerVersionCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    getPeerFirmwareVersionCmd->cmdType      = IswInterface::Stream;
    getPeerFirmwareVersionCmd->command      = GetPeerFirmwareVersion;
    getPeerFirmwareVersionCmd->cmdContext   = iswInterface->GetNextCmdCount();
    getPeerFirmwareVersionCmd->reserved1    = 0x00;
    getPeerFirmwareVersionCmd->peerIndex    = peerIndex;
    getPeerFirmwareVersionCmd->reserved2[0] = 0x00;
    getPeerFirmwareVersionCmd->reserved2[1] = 0x00;
    getPeerFirmwareVersionCmd->reserved2[2] = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswGetPeerFirmwareVersionCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(getPeerFirmwareVersionCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(getPeerFirmwareVersionCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(getPeerFirmwareVersionCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(getPeerFirmwareVersionCmd->reserved1) << std::endl;
        ss << "    peerIndex  = " << std::to_string(getPeerFirmwareVersionCmd->peerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetPeerFirmwareVersionCmd()
//! Inputs: peerIndex - ISW representation of the peer to this node
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendGetPeerFirmwareVersionCmd(uint8_t peerIndex)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendGetPeerFirmwareVersionCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetPeerFirmwareVersionCmd(&iswCmd.iswGetPeerFirmwareVersionCmd, peerIndex, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendGetPeerFirmwareVersionCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupStartMulticastGroupCmd()
//! Inputs: startMulticastGroupCmd - pointer to the command structure
//!         phyRate - PHY rate to use for Tx multicasting (0 - 7)
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupStartMulticastGroupCmd(iswStartMulticastGroupCommand *startMulticastGroupCmd, uint8_t phyRate, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SetupStartMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    startMulticastGroupCmd->cmdType      = IswInterface::Stream;
    startMulticastGroupCmd->command      = StartMulticastGroup;
    startMulticastGroupCmd->cmdContext   = iswInterface->GetNextCmdCount();
    startMulticastGroupCmd->reserved1    = 0x00;
    startMulticastGroupCmd->phyRate      = phyRate;
    startMulticastGroupCmd->reserved2[0] = 0x00;
    startMulticastGroupCmd->reserved2[1] = 0x00;
    startMulticastGroupCmd->reserved2[2] = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStartMulticastGroupCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(startMulticastGroupCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(startMulticastGroupCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(startMulticastGroupCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(startMulticastGroupCmd->reserved1) << std::endl;
        ss << "    phyRate    = " << std::to_string(startMulticastGroupCmd->phyRate) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendStartMulticastGroupCmd()
//! Inputs: phyRate - PHY rate to use for Tx multicasting (0 - 7)
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendStartMulticastGroupCmd(uint8_t phyRate)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendStartMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupStartMulticastGroupCmd(&iswCmd.iswStartMulticastGroupCmd, phyRate, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendStartMulticastGroupCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupJoinMulticastGroupCmd()
//! Inputs: joinMulticastGroupCmd - pointer to the command structure
//!         mcastAddress - ECMA-368 multicast address of the
//!                        multicast group to join
//!         phyRate - PHY rate to use for Tx multicasting (0 - 7)
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupJoinMulticastGroupCmd(iswJoinMulticastGroupCommand *joinMulticastGroupCmd, uint16_t mcastAddress, uint8_t phyRate, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SetupJoinMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    joinMulticastGroupCmd->cmdType      = IswInterface::Stream;
    joinMulticastGroupCmd->command      = JoinMulticastGroup;
    joinMulticastGroupCmd->cmdContext   = iswInterface->GetNextCmdCount();
    joinMulticastGroupCmd->reserved1    = 0x00;
    joinMulticastGroupCmd->mcastAddress = mcastAddress;
    joinMulticastGroupCmd->mcastPhyRate = phyRate;
    joinMulticastGroupCmd->reserved2    = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswJoinMulticastGroupCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType      = " << std::to_string(joinMulticastGroupCmd->cmdType) << std::endl;
        ss << "    command      = " << std::to_string(joinMulticastGroupCmd->command) << std::endl;
        ss << "    cmdContext   = " << std::to_string(joinMulticastGroupCmd->cmdContext) << std::endl;
        ss << "    reserved1    = " << std::to_string(joinMulticastGroupCmd->reserved1) << std::endl;
        ss << "    mcastAddress = " << std::to_string(joinMulticastGroupCmd->mcastAddress) << std::endl;
        ss << "    phyRate      = " << std::to_string(joinMulticastGroupCmd->mcastPhyRate) << std::endl;
        ss << "    reserved2    = " << std::to_string(joinMulticastGroupCmd->reserved2) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendJoinMulticastGroupCmd()
//! Inputs: mcastAddress - ECMA-368 multicast address of the
//!                        multicast group to join
//!         phyRate - PHY rate to use for Tx multicasting (0 - 7)
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendJoinMulticastGroupCmd(uint16_t mcastAddress, uint8_t phyRate)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendJoinMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupJoinMulticastGroupCmd(&iswCmd.iswJoinMulticastGroupCmd, mcastAddress, phyRate, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendJoinMulticastGroupCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupLeaveMulticastGroupCmd()
//! Inputs: leaveMulticastGroupCmd - pointer to the command structure
//!         mcastPeerIndex - multicast peer index of the group to
//!                          leave - not the same as peerIndex
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupLeaveMulticastGroupCmd(iswLeaveMulticastGroupCommand *leaveMulticastGroupCmd, uint8_t mcastPeerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupLeaveMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    leaveMulticastGroupCmd->cmdType        = IswInterface::Stream;
    leaveMulticastGroupCmd->command        = LeaveMulticastGroup;
    leaveMulticastGroupCmd->cmdContext     = iswInterface->GetNextCmdCount();
    leaveMulticastGroupCmd->reserved1      = 0x00;
    leaveMulticastGroupCmd->mcastPeerIndex = mcastPeerIndex;
    leaveMulticastGroupCmd->reserved2[0]   = 0x00;
    leaveMulticastGroupCmd->reserved2[1]   = 0x00;
    leaveMulticastGroupCmd->reserved2[2]   = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswLeaveMulticastGroupCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType        = " << std::to_string(leaveMulticastGroupCmd->cmdType) << std::endl;
        ss << "    command        = " << std::to_string(leaveMulticastGroupCmd->command) << std::endl;
        ss << "    cmdContext     = " << std::to_string(leaveMulticastGroupCmd->cmdContext) << std::endl;
        ss << "    reserved1      = " << std::to_string(leaveMulticastGroupCmd->reserved1) << std::endl;
        ss << "    mcastPeerIndex = " << std::to_string(leaveMulticastGroupCmd->mcastPeerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendLeaveMulticastGroupCmd()
//! Inputs: mcastPeerIndex - multicast peer index of the group to
//!                          leave - not the same as peerIndex
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendLeaveMulticastGroupCmd(uint8_t mcastPeerIndex)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendLeaveMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupLeaveMulticastGroupCmd(&iswCmd.iswLeaveMulticastGroupCmd, mcastPeerIndex, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendLeaveMulticastGroupCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupModifyMulticastGroupCmd()
//! Inputs: modifyMulticastGroupCmd - pointer to the command structure
//!         mcastPeerIndex - multicast peer index of the group to
//!                          leave - not the same as peerIndex
//!         phyRate - PHY rate to use for Tx multicasting (0 - 7)
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswStream::SetupModifyMulticastGroupCmd(iswModifyMulticastGroupCommand *modifyMulticastGroupCmd, uint8_t mcastPeerIndex, uint8_t phyRate, void *routingHdr)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "SetupModifyMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    modifyMulticastGroupCmd->cmdType        = IswInterface::Stream;
    modifyMulticastGroupCmd->command        = ModifyMulticastGroup;
    modifyMulticastGroupCmd->cmdContext     = iswInterface->GetNextCmdCount();
    modifyMulticastGroupCmd->reserved1      = 0x00;
    modifyMulticastGroupCmd->mcastPeerIndex = mcastPeerIndex;
    modifyMulticastGroupCmd->mcastPhyRate   = phyRate;
    modifyMulticastGroupCmd->reserved2      = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswModifyMulticastGroupCommand);

    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType        = " << std::to_string(modifyMulticastGroupCmd->cmdType) << std::endl;
        ss << "    command        = " << std::to_string(modifyMulticastGroupCmd->command) << std::endl;
        ss << "    cmdContext     = " << std::to_string(modifyMulticastGroupCmd->cmdContext) << std::endl;
        ss << "    reserved1      = " << std::to_string(modifyMulticastGroupCmd->reserved1) << std::endl;
        ss << "    mcastPeerIndex = " << std::to_string(modifyMulticastGroupCmd->mcastPeerIndex) << std::endl;
        ss << "    mcastPhyRate   = " << std::to_string(modifyMulticastGroupCmd->mcastPhyRate) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendModifyMulticastGroupCmd()
//! Inputs: mcastPeerIndex - multicast peer index of the group to
//!                          leave - not the same as peerIndex
//!         phyRate - PHY rate to use for Tx multicasting (0 - 7)
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswStream::SendModifyMulticastGroupCmd(uint8_t mcastPeerIndex, uint8_t phyRate)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "SendModifyMulticastGroupCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupModifyMulticastGroupCmd(&iswCmd.iswModifyMulticastGroupCmd, mcastPeerIndex, phyRate, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswStream == 1 )
        {
            msgString = "SendModifyMulticastGroupCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! GetPeers()
//! Inputs: peers - pointer to std::array<iswPeerRecord, MAX_PEERS>
//! Description: Copy the peer records into the user's space
//!###################################################################
void IswStream::GetPeers(std::array<iswPeerRecord, MAX_PEERS> *peers)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        msgString = "GetPeers";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswPeerRecordLock.lock();
    for (uint8_t i = 0; i < iswPeers.size(); i++)
    {
        if ( iswPeers[i].inUse )
        {
            (*peers)[i].inUse = true;
            (*peers)[i].isConnected = iswPeers[i].isConnected;
            (*peers)[i].connectionTimestampMicrosecs = iswPeers[i].connectionTimestampMicrosecs;
            (*peers)[i].recordVersion = iswPeers[i].recordVersion;

            switch ( iswPeers[i].recordVersion )
            {
                case 1:
                {
                    (*peers)[i].iswPeerRecord1.peerIndex    = iswPeers[i].iswPeerRecord1.peerIndex;
                    (*peers)[i].iswPeerRecord1.peerState    = 0x00;
                    (*peers)[i].iswPeerRecord1.deviceType   = iswPeers[i].iswPeerRecord1.deviceType;
                    (*peers)[i].iswPeerRecord1.linkStatus   = iswPeers[i].iswPeerRecord1.linkStatus;
                    (*peers)[i].iswPeerRecord1.linkQuality  = iswPeers[i].iswPeerRecord1.linkQuality;
                    (*peers)[i].iswPeerRecord1.txQueueDepth = iswPeers[i].iswPeerRecord1.txQueueDepth;
                    break;
                }
                case 2:
                {
                    (*peers)[i].iswPeerRecord2.peerState    = 0x00;
                    (*peers)[i].iswPeerRecord2.deviceType   = iswPeers[i].iswPeerRecord2.deviceType;
                    (*peers)[i].iswPeerRecord2.linkStatus   = iswPeers[i].iswPeerRecord2.linkStatus;
                    (*peers)[i].iswPeerRecord2.linkQuality  = iswPeers[i].iswPeerRecord2.linkQuality;
                    (*peers)[i].iswPeerRecord2.txQueueDepth = iswPeers[i].iswPeerRecord2.txQueueDepth;

                    for (int j = 0; j < 6; j++)
                    {
                        (*peers)[i].iswPeerRecord2.macAddress[j] = iswPeers[i].iswPeerRecord2.macAddress[j];

                    }

                    (*peers)[i].iswPeerRecord2.reserved2 = 0x0000;
                    break;
                }
                case 3:
                {
                    (*peers)[i].iswPeerRecord3.peerState    = 0x00;
                    (*peers)[i].iswPeerRecord3.deviceType   = iswPeers[i].iswPeerRecord3.deviceType;
                    (*peers)[i].iswPeerRecord3.linkStatus   = iswPeers[i].iswPeerRecord3.linkStatus;
                    (*peers)[i].iswPeerRecord3.linkQuality  = iswPeers[i].iswPeerRecord3.linkQuality;
                    (*peers)[i].iswPeerRecord3.txQueueDepth = iswPeers[i].iswPeerRecord3.txQueueDepth;

                    for (int j = 0; j < 6; j++)
                    {
                        (*peers)[i].iswPeerRecord3.macAddress[j] = iswPeers[i].iswPeerRecord3.macAddress[j];

                    }

                    (*peers)[i].iswPeerRecord3.throughput   = iswPeers[i].iswPeerRecord3.throughput;
                    (*peers)[i].iswPeerRecord3.transferSize = iswPeers[i].iswPeerRecord3.transferSize;
                    (*peers)[i].iswPeerRecord3.running      = iswPeers[i].iswPeerRecord3.running;
                    (*peers)[i].iswPeerRecord3.reserved2    = 0x0000;
                    break;
                }
                case 4:
                {
                    (*peers)[i].iswPeerRecord4.deviceType   = iswPeers[i].iswPeerRecord4.deviceType;
                    (*peers)[i].iswPeerRecord4.peerState    = 0x00;
                    (*peers)[i].iswPeerRecord4.linkStatus   = iswPeers[i].iswPeerRecord4.linkStatus;
                    (*peers)[i].iswPeerRecord4.linkQuality  = iswPeers[i].iswPeerRecord4.linkQuality;
                    (*peers)[i].iswPeerRecord4.txQueueDepth = iswPeers[i].iswPeerRecord4.txQueueDepth;

                    for (int j = 0; j < 6; j++)
                    {
                        (*peers)[i].iswPeerRecord4.macAddress[j] = iswPeers[i].iswPeerRecord4.macAddress[j];

                    }

                    (*peers)[i].iswPeerRecord4.txThroughput = iswPeers[i].iswPeerRecord4.txThroughput;
                    (*peers)[i].iswPeerRecord4.rxThroughput = iswPeers[i].iswPeerRecord4.rxThroughput;
                    (*peers)[i].iswPeerRecord4.modePhyRate  = iswPeers[i].iswPeerRecord4.modePhyRate;
                    (*peers)[i].iswPeerRecord4.averageRSSI  = iswPeers[i].iswPeerRecord4.averageRSSI;
                    break;
                }
                default:
                {
                    if ( theLogger->DEBUG_IswStream == 1 )
                    {
                        msgString = "Bad Peer Record Version";
                        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }
                    break;
                }
            }
        }
    }
    iswPeerRecordLock.unlock();

}

//!##################################################################################
//! GetMulticastGroupRecords()
//! Inputs: peers - pointer to std::array<multicastGroupRecords, MAX_MCAST_RECORDS>
//! Description: Copy the multicast records into the user's space
//!##################################################################################
void IswStream::GetMulticastGroupRecords(std::array<multicastGroupRecords, MAX_MCAST_RECORDS> *mcastGroupRecords)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        std::string msgString;
        msgString = "GetMulticastGroupRecords";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswMcastRecordLock.lock();
    int index = 0;

    //! Check if we already have a record for this peerIndex
    for (int i = 0; i < MAX_MCAST_RECORDS; i++)
    {
        if ( iswMulticastGroupRecords[i].inUse )
        {
            (*mcastGroupRecords)[i].inUse                         = iswMulticastGroupRecords[i].inUse;
            (*mcastGroupRecords)[i].iswMcastRecord.mcastPeerIndex = iswMulticastGroupRecords[i].iswMcastRecord.mcastPeerIndex;
            (*mcastGroupRecords)[i].iswMcastRecord.mcastStatus    = iswMulticastGroupRecords[i].iswMcastRecord.mcastStatus;
            (*mcastGroupRecords)[i].iswMcastRecord.mcastAddress   = iswMulticastGroupRecords[i].iswMcastRecord.mcastAddress;
            (*mcastGroupRecords)[i].iswMcastRecord.ownerPeerIndex = iswMulticastGroupRecords[i].iswMcastRecord.ownerPeerIndex;
            (*mcastGroupRecords)[i].iswMcastRecord.mcastPhyRate   = iswMulticastGroupRecords[i].iswMcastRecord.mcastPhyRate;
            (*mcastGroupRecords)[i].iswMcastRecord.txQueueDepth   =  iswMulticastGroupRecords[i].iswMcastRecord.txQueueDepth;
        }
    }

    iswMcastRecordLock.unlock();
}

//!##################################################################################
//! PrintPeerRecords()
//! Inputs: None
//! Description: Debug method to print out all the peer records to the log file
//!##################################################################################
void IswStream::PrintPeerRecords(void)
{
    if ( theLogger->DEBUG_IswStream == 1 )
    {
        iswPeerRecordLock.lock();
        for (uint8_t i = 0; i < iswPeers.size(); i++)
        {
            std::stringstream ss;
            ss << "iswPeers[" << std::to_string(i) << "]:" << std::endl;
            ss << "            inUse = " << std::to_string(iswPeers[i].inUse) << std::endl;
            ss << "            recordVersion = " << std::to_string(iswPeers[i].recordVersion) << std::endl;

            switch ( iswPeers[i].recordVersion )
            {
                case 1:
                {
                    ss << "                    .peerIndex    = " << std::to_string(iswPeers[i].iswPeerRecord1.peerIndex) << std::endl;
                    ss << "                    .peerState    = " << std::to_string(iswPeers[i].iswPeerRecord1.peerState) << std::endl;
                    ss << "                    .deviceType   = " << std::to_string(iswPeers[i].iswPeerRecord1.deviceType) << std::endl;
                    ss << "                    .linkStatus   = " << std::to_string(iswPeers[i].iswPeerRecord1.linkStatus) << std::endl;
                    ss << "                    .linkQuality  = " << std::to_string(iswPeers[i].iswPeerRecord1.linkQuality) << std::endl;
                    ss << "                    .txQueueDepth = " << std::to_string(iswPeers[i].iswPeerRecord1.txQueueDepth) << std::endl;
                    break;
                }
                case 2:
                {
                    ss << "                    .peerIndex    = " << std::to_string(iswPeers[i].iswPeerRecord2.peerIndex) << std::endl;
                    ss << "                    .peerState    = " << std::to_string(iswPeers[i].iswPeerRecord2.peerState) << std::endl;
                    ss << "                    .deviceType   = " << std::to_string(iswPeers[i].iswPeerRecord2.deviceType) << std::endl;
                    ss << "                    .linkStatus   = " << std::to_string(iswPeers[i].iswPeerRecord2.linkStatus) << std::endl;
                    ss << "                    .linkQuality  = " << std::to_string(iswPeers[i].iswPeerRecord2.linkQuality) << std::endl;
                    ss << "                    .txQueueDepth = " << std::to_string(iswPeers[i].iswPeerRecord2.txQueueDepth) << std::endl;
                    ss << "                    .macAddress   = ";
                    for (int j = 0; j < 6; j++)
                    {
                        if ( j == 5 )
                        {
                           ss << std::to_string(iswPeers[i].iswPeerRecord2.macAddress[j]) << std::endl;
                        }
                        else
                        {
                            ss << std::to_string(iswPeers[i].iswPeerRecord2.macAddress[j]) << ":";
                        }
                    }
                    ss << "                    .reserved2 = " << std::to_string(iswPeers[i].iswPeerRecord2.reserved2) << std::endl;
                    break;
                }
                case 3:
                {
                    ss << "                    .peerIndex    = " << std::to_string(iswPeers[i].iswPeerRecord3.peerIndex) << std::endl;
                    ss << "                    .peerState    = " << std::to_string(iswPeers[i].iswPeerRecord3.peerState) << std::endl;
                    ss << "                    .deviceType   = " << std::to_string(iswPeers[i].iswPeerRecord3.deviceType) << std::endl;
                    ss << "                    .linkStatus   = " << std::to_string(iswPeers[i].iswPeerRecord3.linkStatus) << std::endl;
                    ss << "                    .linkQuality  = " << std::to_string(iswPeers[i].iswPeerRecord3.linkQuality) << std::endl;
                    ss << "                    .txQueueDepth = " << std::to_string(iswPeers[i].iswPeerRecord3.txQueueDepth) << std::endl;
                    ss << "                    .macAddress   = ";
                    for (int j = 0; j < 6; j++)
                    {
                        if ( j == 5 )
                        {
                           ss << std::to_string(iswPeers[i].iswPeerRecord3.macAddress[j]) << std::endl;
                        }
                        else
                        {
                            ss << std::to_string(iswPeers[i].iswPeerRecord3.macAddress[j]) << ":";
                        }
                    }
                    ss << "                    .throughput   = " << std::to_string(iswPeers[i].iswPeerRecord3.throughput) << std::endl;
                    ss << "                    .transferSize = " << std::to_string(iswPeers[i].iswPeerRecord3.transferSize) << std::endl;
                    ss << "                    .running      = " << std::to_string(iswPeers[i].iswPeerRecord3.running) << std::endl;
                    ss << "                    .reserved2    = " << std::to_string(iswPeers[i].iswPeerRecord3.reserved2) << std::endl;
                    break;
                }
                case 4:
                {
                    ss << "                    .peerIndex    = " << std::to_string(iswPeers[i].iswPeerRecord4.peerIndex) << std::endl;
                    ss << "                    .peerState    = " << std::to_string(iswPeers[i].iswPeerRecord4.peerState) << std::endl;
                    ss << "                    .deviceType   = " << std::to_string(iswPeers[i].iswPeerRecord4.deviceType) << std::endl;
                    ss << "                    .linkStatus   = " << std::to_string(iswPeers[i].iswPeerRecord4.linkStatus) << std::endl;
                    ss << "                    .linkQuality  = " << std::to_string(iswPeers[i].iswPeerRecord4.linkQuality) << std::endl;
                    ss << "                    .txQueueDepth = " << std::to_string(iswPeers[i].iswPeerRecord4.txQueueDepth) << std::endl;
                    ss << "                    .macAddress   = ";
                    for (int j = 0; j < 6; j++)
                    {
                        if ( j == 5 )
                        {
                           ss << std::to_string(iswPeers[i].iswPeerRecord4.macAddress[j]) << std::endl;
                        }
                        else
                        {
                            ss << std::to_string(iswPeers[i].iswPeerRecord4.macAddress[j]) << ":";
                        }
                    }
                    ss << "                    .txThroughput = " << std::to_string(iswPeers[i].iswPeerRecord4.txThroughput) << std::endl;
                    ss << "                    .rxThroughput = " << std::to_string(iswPeers[i].iswPeerRecord4.rxThroughput) << std::endl;
                    ss << "                    .modePhyRate  = " << std::to_string(iswPeers[i].iswPeerRecord4.modePhyRate) << std::endl;
                    ss << "                    .averageRSSI  = " << std::to_string(iswPeers[i].iswPeerRecord4.averageRSSI) << std::endl;
                    break;
                }
                default:
                {
                    break;
                }
            }
            theLogger->LogMessage(ss.str(), GetIndex(), std::chrono::system_clock::now());
        }
        iswPeerRecordLock.unlock();
    }
}
