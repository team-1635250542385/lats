//!##########################################################
//! Filename: IswInterface.cpp
//! Description: Class that provides the ISW API for the user
//!              application.  Each device has one instance
//!              of an IswInterface object.  This is the main
//!              object that the user will use to send and
//!              receive ISW commands.  Each IswInterface object
//!              has links to:
//!              - An UsbInterface object
//!              - Each of the friends' classes objects:
//!                 - OnBoard - special tests that run on the device
//!                 - IswAssociation
//!                 - IswFirmware
//!                 - IswIdentity
//!                 - IswLink
//!                 - IswMetrics
//!                 - IswProduct
//!                 - IswSecurity
//!                 - IswStream
//!                 - IswSystem
//!                 - IswSolNet
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswInterface.h"
#include <arpa/inet.h>
#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <byteswap.h>
#include <chrono>
#include <thread>
#include <string>
#include <cstring>
#include <sstream>
#include <unistd.h>
#include <iomanip>

#include <QDebug>

//!###############################################################
//! Constructor: IswInterface()
//! Inputs: devInterface - a pointer to the generic Interface object
//!         devType - USB for now
//!         logger - a pointer to the Logger object
//!         dbLogger - a pointer to the DBLogger object
//!###############################################################
IswInterface::IswInterface(Interface *devInterface, uint8_t devType, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass, DBLogger *dblogger, Logger *latencyLoggerPass) :
    deviceInterface(devInterface),
    deviceType(devType),
    theLogger(logger),
    throughputTestLogger(throughputTestLoggerPass),
    throughputResultsLogger(throughputResultsLoggerPass),
    dbLogger(dblogger),
    latencyLogger(latencyLoggerPass)
{
    if (theLogger == nullptr)
    {
        std::cout << "Logger Pointer!" << std::endl;
        exit(-1);
    }
    if (dbLogger == nullptr)
    {
        std::cout << "DBLogger Pointer!" << std::endl;
        exit(-1);
    }
}

IswInterface::~IswInterface()
{

}

//!###################################################
//! LoginToIswNode()
//! Inputs: None
//! Description: Method called during initialization
//!              of the ISW device to authenticate with
//!              the devices. See the ISW Embedment Guide
//!              on device authentication.
//!###################################################
void IswInterface::LoginToIswNode()
{
    std::string msgString;
    int status                       = 0;
    int lpCount                      = 0;
    SerialInterface *serialInterface = usbInterface->GetSerialInterface();

    //! Wait for success
    while ( !iswSecurity->IsAuthenticated() )
    {
        //! Authenticate role to hw
        status = iswSecurity->SendAuthenticateRoleCmd();

        if (serialInterface != nullptr && lpCount > 8 && lpCount < 12)
        {
            std::cout<<"Camouflage Module Unresponsive, Please Restart the Device"<<std::endl;
        }

        if ( status == 0 )
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Waiting for Authenticate Role";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            sleep(1);  //! Let read thread run
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Authenticate Role Failed";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            return;
        }
        lpCount ++;
    }

    //! If authentication was successful, send the crypto
    //! session start command.  Wait for success
    if ( (status == 0) && iswSecurity->IsAuthenticated() )
    {
        if ( theLogger->DEBUG_IswInterface == 1 )
        {
            msgString = "Authenticate Role Successful";
            theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
        }

        while ( !iswSecurity->IsInterfaceReady() )
        {
            //! Send Crypto Session start
            status = iswSecurity->SendCryptoSessionStartCmd();

            if ( status == 0 )
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "Waiting for Crypto Session Start";
                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                }
                sleep(1);
            }
            else
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "Crypto Session Start Failed";
                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                }
                return;
            }
        }

        //! If the authentication steps were successful, get
        //! the device attributes
        if ( (status == 0) && iswSecurity->IsInterfaceReady() )
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Crypto Start Successful";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }

            //! Send commands to firmware
            uint8_t selector = 1;  //! 0 = standby, 1 = active
            iswFirmware->SendGetFirmwareVersionCmd(selector);

            while ( iswFirmware->GetWaitingActiveSelector() )
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "Waiting to Get Active Firmware Version";
                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                }
                sleep(1);
            }

            //! The following are sending commands to the firmware
            //! For each command, the firmware sends back a response
            //! which is processed in the receiving DeliverIswMessage()
            //! We don't wait here for the response - different threads

            //! Send the get standby firmware version command
            selector = 0;
            status   = iswFirmware->SendGetFirmwareVersionCmd(selector);

            if ( serialInterface != nullptr )
            {
                std::cout<<"Camouflage Module Detected"<<std::endl;
                usleep(330000);
            }
            usleep(10);

            //! Send the get Mac address command
            status = iswIdentity->SendGetMacAddressCmd();
            if ( serialInterface != nullptr )
            {
                usleep(330000);
            }
            usleep(10);

            status = iswIdentity->SendGetDeviceTypeCmd();
            if ( serialInterface != nullptr )
            {
                usleep(360000);
            }
            usleep(10);

            status = iswLink->SendGetScanDutyCycleCmd();
            if ( serialInterface != nullptr )
            {
                usleep(330000);
            }
            usleep(10);

            status = iswProduct->SendGetNetworkIdCmd();
            if ( serialInterface != nullptr )
            {
                usleep(330000);
            }
            usleep(10);

            //! Set Indications command - it's different for parallel board
            uint8_t wireInterfaceEnable    = 1;  // Disabling this disables the ability to monitor for connections/disconnections
            uint8_t associationIndEnable   = 1;
            uint8_t connectionIndEnable    = 1;
            uint8_t resetIndEnable         = 1;
            uint8_t multicastIndEnable     = 1;
            uint8_t streamsStatusIndEnable = 4; // Use 4 for onBoard tests
            uint8_t hibernationIndEnable   = 1; // FW 30810 or Later
            iswSystem->SendSetIndicationsCmd(wireInterfaceEnable, associationIndEnable,
                                             connectionIndEnable, resetIndEnable, multicastIndEnable, streamsStatusIndEnable, hibernationIndEnable);

            usleep(10);
        }
    }
}

//!##################################################################
//! IswInit()
//! Inputs: index - position in the iswInterfaceArray for this device
//! Description: Initialize an IswInterface object for one ISW device
//!##################################################################
int IswInterface::IswInit(uint8_t index)
{
    std::string msgString;
    int status = 0;

    //! Internal device index for logging
    iswListIndex = index;

    //! Clear queues to start
    EmptyEndpointQueues();

    switch (deviceType)
    {
        case USB:
        {
            usbInterface = (UsbInterface *)deviceInterface;
            break;
        }
        default:
        {
            std::cout << "Undefined Interface Type" << std::endl;
            exit(-1);
            break;
        }
    }

    CheckSystemEndianness();

    if (usbInterface->GetBoardType() != UsbInterface::externalBoard)
    {
        InitEndpointQueues();

        //! Create all the ISW friend classes that
        //! handle types of ISW messages
        iswOnBoard     = new OnBoard(this, theLogger);
        iswFirmware    = new IswFirmware(this, theLogger);
        iswStream      = new IswStream(this, theLogger);
        iswSystem      = new IswSystem(this, theLogger);
        iswIdentity    = new IswIdentity(this, theLogger);
        iswSecurity    = new IswSecurity(this, theLogger);
        iswAssociation = new IswAssociation(this, theLogger);
        iswLink        = new IswLink(this, theLogger);
        iswMetrics     = new IswMetrics(this, theLogger, dbLogger);
        iswProduct     = new IswProduct(this, theLogger);
        iswSolNet      = new IswSolNet(this, theLogger, throughputTestLogger, throughputResultsLogger);
        iswTest        = new IswTest(this, theLogger, dbLogger);

        if ( (iswOnBoard == nullptr) ||
             (iswFirmware == nullptr) ||
             (iswStream == nullptr) ||
             (iswSystem == nullptr) ||
             (iswIdentity == nullptr) ||
             (iswSecurity == nullptr) ||
             (iswAssociation == nullptr) ||
             (iswLink == nullptr) ||
             (iswMetrics == nullptr) ||
             (iswProduct == nullptr) ||
             (iswSolNet == nullptr) ||
             (iswTest == nullptr) )
        {
            return (status = -1);
        }
    }
    else
    {
       iswIdentity = new IswIdentity(this, theLogger);
       iswMetrics  = new IswMetrics(this, theLogger, dbLogger);
    }

#ifndef TESTING  //! Don't run during unit tests
    StopIswReceiveThread();
        
    if ( usbInterface->GetBoardType() != UsbInterface::externalBoard and !light_mode)
    {
        if ( usbInterface->IsDeviceOpen() )
        {
            //! Start the thread to handle receive events from the hardware
            //! The iswReceiveThread is a child thread to IswInterface so it has
            //! access to public and protected elements and a reference to this
            //! instance of IswInterface.
            if (pthread_create(&iswReceiveThread, nullptr, &IswInterface::ReceiveThreadHandler, this) != 0)
            {
                std::cout << "ISW Receive Thread Didn't Start!" << std::endl;
            }

            StartIswReceiveThread();

            while ( !iswInterfaceInitComplete )
            {
                usleep(10); //!10 milliseconds
            }

            msgString.clear();
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "IswInterface Init Completed";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }

            //! Two more threads to start
            //! 1) Thread to deliver data packets on data receive queues
            StopIswReceiveQThread();

            if (pthread_create(&iswReceiveQThread, nullptr, &IswInterface::ReceiveQThreadHandler, this) != 0)
            {
                std::cout << "ISW Receive SolNet Data Thread Didn't Start!" << std::endl;
            }

            StartIswReceiveQThread();

            //! 2) Thread to send data packets on data send queues
            StopIswSendQThread();

            if (pthread_create(&iswSendQThread, nullptr, &IswInterface::SendQThreadHandler, this) != 0)
            {
                std::cout << "ISW Send Thread Didn't Start!" << std::endl;
            }

            StartIswSendQThread();

            //! Authenticate and Crypto Start for ISW comms
            LoginToIswNode();
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "IswInterface Init Not Completed";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            status = -1;
        }
    }
    else
    {
        //! External device
        status = 0;
    }
#else
    if ( usbInterface->GetBoardType() != UsbInterface::externalBoard )
    {
        if ( usbInterface->IsDeviceOpen() )
        {
            StartIswReceiveQThread();
            StartIswSendQThread();
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "IswInterface Init not completed";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            status = -1;
        }
    }
    else
    {
        //! External device
        status = 0;
    }
#endif
    return (status);
}

//!#############################################################
//! CheckSystemEndianness()
//! Inputs: None
//! Description: Checks the endianess of the system. All ISW
//!              commands must be sent in Little Endian format
//!#############################################################
void IswInterface::CheckSystemEndianness()
{
    std::string msgString = "Endianess = ";
    //! Check the system Endianess. use the built in network byte conversion
    //! operations (since network byte order is always Big Endian)
    if ( htonl(47) == 47 )
    {
        //! Big Endian
        isLittleEndian = false;
        msgString.append("Big");
    }
    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        msgString.append("Little");
        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
    }
}

//!#############################################################
//! SetQosOnEndpoint()
//! Inputs: qOs - number representing one of four quality of
//!               service values
//!         endpoint - 0 - 3
//! Description: Set the quality of service value for the queue
//!              servicing the given endpoint
//!#############################################################
void IswInterface::SetQosOnEndpoint(uint8_t qOs, uint8_t endpoint)
{
    // Applies to send queues only!
    iswEndpointSendQueues[endpoint].qOs = qOs;
}

//!#############################################################
//! InitEndpointQueues()
//! Inputs: None
//! Description: Initialize all the send and receive queues
//!#############################################################
void IswInterface::InitEndpointQueues()
{
    for (uint8_t i = 0; i < NumberOfIswEndpoints; i++)
    {
        iswEndpointReceiveQueues[i].qOs = HiNoDiscard;
        memset(&iswEndpointReceiveQueues[i].flushArray[0], 0, IswSolNet::MAX_NUMBER_APPS);
    }

    // Per ISW spec
    // SolNet can use enpoint 0 for data
    iswEndpointSendQueues[0].qOs = HiNoDiscard;
    iswEndpointSendQueues[1].qOs = HiNoDiscard;
    iswEndpointSendQueues[2].qOs = HiNoDiscard;
    iswEndpointSendQueues[3].qOs = LowNoDiscard;
}

//!#############################################################
//! SetFlushReceiveQueue()
//! Inputs: endpointId - which receive queue number
//!         dataflowId - within an endpoint's queue, each dataflowId
//!                      represents a stream of data that can be
//!                      flushed individually from other streams.
//!                      Useful if the stream is shutting down.
//!         flush - boolean true/false
//! Description: Initialize all the send and receive queues
//!#############################################################
void IswInterface::SetFlushReceiveQueue(uint8_t endpointId, uint8_t dataflowId, bool flush)
{
    iswEndpointReceiveQueues[endpointId].flushArray[dataflowId] = flush;
}

//!#############################################################
//! SetFlushSendQueue()
//! Inputs: endpointId - which send queue number
//!         dataflowId - within an endpoint's queue, each dataflowId
//!                      represents a stream of data that can be
//!                      flushed individually from other streams.
//!                      Useful if the stream is shutting down.
//!         flush - boolean true/false
//! Description: Initialize all the send and receive queues
//!#############################################################
void IswInterface::SetFlushSendQueue(uint8_t endpointId, uint8_t dataflowId, bool flush)
{
    iswEndpointSendQueues[endpointId].flushArray[dataflowId] = flush;
}

//!#############################################################
//! EmptyEndpointQueues()
//! Inputs: None
//! Description: Empty all the send and receive queues
//!#############################################################
void IswInterface::EmptyEndpointQueues()
{
    for (uint8_t i = 0; i < NumberOfIswEndpoints; i++)
    {
        //! Free the SendQueues items
        //! Called on shutdown - no sleep
        while ( iswEndpointSendQueues[i].sendQ.empty() == false )
        {
            RemoveSendQueueFront(i);
        }
        memset(&iswEndpointSendQueues[i].flushArray[0], 0, IswSolNet::MAX_NUMBER_APPS);

        //! Free the ReceiveQueues items
        while ( iswEndpointReceiveQueues[i].receiveQ.empty() == false )
        {
            RemoveReceiveQueueFront(i);
        }
        memset(&iswEndpointReceiveQueues[i].flushArray[0], 0, IswSolNet::MAX_NUMBER_APPS);
    }
}

//!#############################################################
//! SetDeviceIsReady()
//! Inputs: deviceReady - boolean true/false
//! Description: Set whether the device is ready to send/receive
//!              data based on the input.
//!#############################################################
void IswInterface::SetDeviceIsReady(bool deviceReady)
{
    iswCmdLock.lock();
    iswDeviceIsReady = deviceReady;
    iswCmdLock.unlock();
}

//!###############################################################
//! IswExit()
//! Inputs: None
//! Description: Clean up and exit the running IswInterface object
//!###############################################################
void IswInterface::IswExit()
{
    if ( iswReceiveThread != 0 )
    {
        //! Stop the receive thread and then
        //! call PThread join() essentially closing it
        StopIswReceiveThread();
#ifndef TESTING  //!Don't run during unit tests
        pthread_join(iswReceiveThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleIswReceiveEvents Ended" << std::endl;
#endif
    }

    if ( iswReceiveQThread != 0 )
    {
        //! Stop the receive queue thread and then
        //! call PThread join() essentially closing it
        StopIswReceiveQThread();
#ifndef TESTING  //! Don't run during unit tests
        pthread_join(iswReceiveQThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleIswReceiveQueues Ended" << std::endl;
#endif
    }

    if ( iswSendQThread != 0 )
    {
        //! Stop the send queue thread and then
        //! call PThread join() essentially closing it
        StopIswSendQThread();
#ifndef TESTING  //! Don't run during unit tests
        pthread_join(iswSendQThread, nullptr);
        std::cout << "Device " << std::to_string(GetIndex()) << " Thread HandleIswSendQueues Ended" << std::endl;
#endif
    }

    //! Empty all send and receive the Endpoint queues
    EmptyEndpointQueues();

    //! Delete all the ISW friend classes that
    //! handle types of ISW messages
    if ( iswStream != nullptr )
    {
        // Clear the peer lists so applications don't
        // use old lists. Can happen during a hotplug event
        iswStream->InitIswPeerRecords();
        iswStream->InitIswMulticastRecords();

        delete iswStream;
        iswStream = nullptr;
    }
    if ( iswOnBoard != nullptr )
    {
        delete iswOnBoard;
    }
    if ( iswSolNet != nullptr )
    {
        delete iswSolNet;
        iswSolNet = nullptr;
    }
    if ( iswFirmware != nullptr )
    {
        delete iswFirmware;
        iswFirmware = nullptr;
    }
    if ( iswSystem != nullptr )
    {
        delete iswSystem;
        iswSystem = nullptr;
    }
    if ( iswIdentity != nullptr )
    {
        delete iswIdentity;
        iswIdentity = nullptr;
    }
    if ( iswSecurity != nullptr )
    {
        delete iswSecurity;
        iswSecurity = nullptr;
    }
    if ( iswAssociation != nullptr )
    {
        delete iswAssociation;
        iswAssociation = nullptr;
    }
    if ( iswLink != nullptr )
    {
        delete iswLink;
        iswLink = nullptr;
    }
    if ( iswMetrics != nullptr )
    {
        delete iswMetrics;
        iswMetrics = nullptr;
    }
    if ( iswProduct != nullptr )
    {
        delete iswProduct;
        iswProduct = nullptr;
    }
    if ( iswTest != nullptr )
    {
        delete iswTest;
        iswTest = nullptr;
    }
}

//!###############################################################
//! BigToLittleEndian()
//! Inputs: input - point to a long
//!         type - 16, 32, 64 depending on the size of input
//! Description: Swap Big Endian to Little Endian for the input
//!              value. The ISW Library was tested on a Little Endian
//!              system, so this method is not currently used.
//!              It's provided as an idea that you may have to deal
//!              with this.  All ISW commands are passed the firmware
//!              in Little Endian format.
//!###############################################################
void IswInterface::BigToLittleEndian(uint64_t *input, uint16_t type)
{
    //! This method uses GNU host to Little Endian macros
    //! If you are on a Little Endian host, it does nothing
    //! They only swap if the host is identified as Big Endian
    switch (type)
    {
        case 16:
        {
            htole16(uint16_t (*input));
            break;
        }
        case 32:
        {
            htole32(uint32_t (*input));
            break;
        }
        case 64:
        {
            htole64(uint64_t (*input));
            break;
        }
    }
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: routingHdr - pointer to an iswRoutingHdr in a receive
//!                      buffer
//!         event - pointer an iswEvent in a receive buffer
//!         data - pointer to the data in a receive buffer
//! Description: This method checks the event type from the iswEvent
//!              and calls the appropriate friend ISW class to
//!              process the incoming message
//!###############################################################
void IswInterface::DeliverIswMessage(iswRoutingHdr *routingHdr, iswEvent *event, uint8_t *data)
{
    std::string msgString;

    switch (event->eventType)
    {
        case OnBoardTests:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received OnBoard Test Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }

            iswOnBoard->DeliverIswMessage(event->event, data);
            break;
        }
        case Firmware:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Firmware Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswFirmware->DeliverIswMessage(event->event, data);
            break;
        }
        case Identity:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Identity Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswIdentity->DeliverIswMessage(event->event, data);
            break;
        }
        case Security:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Security Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswSecurity->DeliverIswMessage(event->event, data);
            break;
        }
        case Association:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Association Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswAssociation->DeliverIswMessage(event->event, data);
            break;
        }
        case Stream:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Stream Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswStream->DeliverIswMessage(event->event, data);
            break;
        }
        case Link:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Link Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswLink->DeliverIswMessage(event->event, data);
            break;
        }
        case Metrics:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Metric Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswMetrics->DeliverIswMessage(event->event, data);
            break;

        }
        case System:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received System Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswSystem->DeliverIswMessage(event->event, data);
            break;
        }
        case Product:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Product Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswProduct->DeliverIswMessage(event->event, data);
            break;
        }
        case Test:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received IswTest Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            iswTest->DeliverIswMessage(event->event, data);
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "Received Another Event";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###############################################################
//! StopIswReceiveThread()
//! Inputs: None
//! Description: Sets enableReadIsw boolean to false.  The receive
//!              thread will see this and stop receiving data from
//!              the firmware.
//!###############################################################
void IswInterface::StopIswReceiveThread()
{
    iswCmdLock.lock();
    enableReadIsw = false;
    iswCmdLock.unlock();
}

//!###############################################################
//! StartIswReceiveThread()
//! Inputs: None
//! Description: Sets enableReadIsw boolean to true.  The receive
//!              thread will see this and start receiving data from
//!              the firmware.
//!###############################################################
void IswInterface::StartIswReceiveThread()
{
    iswCmdLock.lock();
    enableReadIsw = true;
    iswCmdLock.unlock();
}

//!###############################################################
//! StopIswReceiveQThread()
//! Inputs: None
//! Description: Sets processDataReceiveQueues boolean to false.
//!              The receive queue processing thread will see this
//!              and stop
//!###############################################################
void IswInterface::StopIswReceiveQThread()
{
    iswCmdLock.lock();
    processDataReceiveQueues = false;
    iswCmdLock.unlock();
}

//!###############################################################
//! StartIswReceiveQThread()
//! Inputs: None
//! Description: Sets processDataReceiveQueues boolean to true.
//!              The receive queue processing thread will see this
//!              and start
//!###############################################################
void IswInterface::StartIswReceiveQThread()
{
    iswCmdLock.lock();
    processDataReceiveQueues = true;
    iswCmdLock.unlock();
}

//!###############################################################
//! HandleIswReceiveQueues()
//! Inputs: None
//! Description: Started in its own thread to run every 10ms and
//!              take entries off the receive queue and call
//!              DeliverSolNetPacket()
//!###############################################################
void IswInterface::HandleIswReceiveQueues(void)
{
    std::string msgString;

#ifndef TESTING  //!Don't run during unit tests
    //! Wait for main thread to be ready
    while ( !processDataReceiveQueues )
    {
        usleep(10);
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        msgString = "HandleIswReceiveQueues Thread Started";
        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
    }

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleIswReceiveQueues Started" << std::endl;

    while ( processDataReceiveQueues )
    {
        if (!pauseReceiveQueueProcessing)
        {
            //! Process in this order:
            //! HiNoDiscard until empty
            //! HiDiscard for three - five messages
            //! If HiNoDiscard queues are empty - process LowPriority queues:
            //! LowNoDiscard then LowDiscard
            //! By default - all queues are HiNoDiscard, so cycle through them
            //! SolNet control messages use endpoint zero and queue zero
            //! SolNet data queues are 1 - 3 as they use endpoints 1 - 3
            for (uint8_t endpoint = 0; endpoint < NumberOfIswEndpoints; endpoint++)
            {
                //! Just send one of these - can change this later
                if ( iswEndpointReceiveQueues[endpoint].receiveQ.empty() == false )
                {
                    //! Get oldest item on front of queue
                    iswDataReceiveItem *receiveItem = GetNextFromReceiveQueue(endpoint);


                    if ( receiveItem != nullptr )
                    {
                        IswSolNet::iswSolNetHeader *dataPkt = (IswSolNet::iswSolNetHeader *)receiveItem->data;

                        int seqNum = dataPkt->iswSolNetDataPkt.seqNumber;

                        //! Check if we are to flush the queue for this dataflowId
                        //! which happens when we shutdown a data traffic stream from the application
                        if ( iswEndpointReceiveQueues[endpoint].flushArray[dataPkt->iswSolNetDataPkt.dataflowId & IswSolNet::iswSolNetProviderConsumerMask] == false )
                        {

                            //! Deliver the packet to the application
                            iswSolNet->DeliverSolNetPacket(receiveItem->peerIndex, endpoint, receiveItem->data);

                        }

                        //! Cleanup and delete resources for item just sent
                        RemoveReceiveQueueFront(endpoint);
                    }
                    usleep(10);
                }
            }
            usleep(10);

        }
    }


#endif
}

//!#################################################################
//! AddToReceiveQueue()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         endpoint - ISW endpoint
//!         data - pointer to the data buffer
//! Description: Called from the ISW receive thread to put data
//!              on the queue for the endpoint.
//!#################################################################
void IswInterface::AddToReceiveQueue(uint8_t peerIndex, uint8_t endpoint, uint8_t *data)
{

    iswEndpointReceiveQueues[endpoint].qLock.lock();

    iswDataReceiveItem *receiveItem = new iswDataReceiveItem;
    receiveItem->peerIndex          = peerIndex;
    receiveItem->data               = data;
    iswEndpointReceiveQueues[endpoint].receiveQ.push_back(receiveItem);

    iswEndpointReceiveQueues[endpoint].qLock.unlock();
}

//!###############################################################
//! GetNextFromReceiveQueue()
//! Inputs: endpoint - ISW endpoint
//! Description: Gets the oldest item on the endpoint receive queue
//!###############################################################
IswInterface::iswDataReceiveItem *IswInterface::GetNextFromReceiveQueue(uint8_t endpoint)
{
    iswEndpointReceiveQueues[endpoint].qLock.lock();
    iswDataReceiveItem *receiveItem = iswEndpointReceiveQueues[endpoint].receiveQ.front();

    iswEndpointReceiveQueues[endpoint].qLock.unlock();

    IswSolNet::iswSolNetHeader *dataPkt = (IswSolNet::iswSolNetHeader *)receiveItem->data;

    int dataPolicies = dataPkt->iswSolNetDataPkt.policies;

    //qDebug() << "data policies: " << dataPolicies;

    int seqNum = dataPkt->iswSolNetDataPkt.seqNumber;

    int headerChecksum = dataPkt->iswSolNetDataPkt.headerChecksum;

    // could be used to filter out non data messages, data messages sent have checksums in this range.
    //control messages use a different range and we want to ignore them.
    //Edit: Messes with Pgen with different packet sizes, but is needed for video
    int headerChecksumMin = 60000;
    int headerChecksumMax = 63000;

    uint64_t now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    QString macAddressStr = QString::fromStdString(iswIdentity->GetMacAddressStr());
    QString peerIndexStr = QString::number(receiveItem->peerIndex);
    QString endpointStr = QString::number(endpoint);

    //qDebug() << "got rec side header checksum" << headerChecksum;

    //qDebug() << "rec headerChecksum and endpoint: " << QString::number(headerChecksum) + ", " + QString::number(endpoint);

    // ToDo Ben: find a way to check for this automatically instead of manually setting it for tests
    // dataPolicies == 2 for pgen
    bool sendingVideo = false;

    if(dataPolicies == 0)
    {
        sendingVideo = true;
    }

    //qDebug() << "is video? " << sendingVideo;

    // used for pgen
    if(!sendingVideo && endpoint > 0)
    {
        //qDebug() << "sending pgen";
        msgRecCnt++;
        QString msgRecCntStr = QString::number(msgRecCnt);
        writeOutLatencyEvent(msgRecCntStr, macAddressStr, peerIndexStr, endpointStr, seqNum, headerChecksum, now, "RECEIVE");
    }
    else if(sendingVideo && endpoint > 0 && headerChecksum > headerChecksumMin && headerChecksum < headerChecksumMax)
    {
        msgRecCnt++;

        //qDebug() << "sending video";
        QString msgRecCntStr = QString::number(msgRecCnt);

        // used for multi thread video test, currently supports two video streams.
        if(receiveItem->peerIndex == 0)
        {
            msgPeerZeroCnt++;
            msgRecCntStr = QString::number(msgPeerZeroCnt);
        }
        else if(receiveItem->peerIndex == 1)
        {
            msgPeerOneCnt++;
            msgRecCntStr = QString::number(msgPeerOneCnt);
        }


       writeOutLatencyEvent(msgRecCntStr, macAddressStr, peerIndexStr, endpointStr, seqNum, headerChecksum, now, "RECEIVE");
    }

    return (receiveItem);
}

//!###############################################################
//! RemoveReceiveQueueFront()
//! Inputs: endpoint - ISW endpoint
//! Description: Removes and deletes the oldest item on the queue
//!              This is called after GetNextFromReceiveQueue() and
//!              the application has processed the data.
//!###############################################################
void IswInterface::RemoveReceiveQueueFront(uint8_t endpoint)
{

    iswEndpointReceiveQueues[endpoint].qLock.lock();

    // Remove list element and delete
    std::list<iswDataReceiveItem *>::iterator it = iswEndpointReceiveQueues[endpoint].receiveQ.begin();
    iswDataReceiveItem *receiveItem              = *it;


    if ( receiveItem->data != nullptr )
    {
        delete (receiveItem->data);
    }

    delete (receiveItem);
    it = iswEndpointReceiveQueues[endpoint].receiveQ.erase(it);

    iswEndpointReceiveQueues[endpoint].qLock.unlock();
}

//!###############################################################
//! HandleIswReceiveEvents()
//! Inputs: None
//! Description: Runs in its own thread, calls a blocking read on
//!              the USB device association with the ISB device.
//!              Determines if the data is good and then delivers
//!              it to the correct friend ISW class or puts it on
//!              a receive queue if it is SolNet data.
//!              This method doesn't worry about whether we have
//!              authenticated with the firmware.  We need to send
//!              and receive in order to authenticate.  If the
//!              authentication fails, then none of the other commands
//!              will work, this will just loop.
//!###############################################################
void IswInterface::HandleIswReceiveEvents(void)
{
    int status            = 0;
    std::string msgString = "";
    int numRead           = 0;

#ifndef TESTING  //!Don't run during unit tests
    //! Wait for main thread to be ready
    //! When true, we can read/write to the USB device
    while ( !enableReadIsw )
    {
        usleep(10);
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        msgString = "HandleIswReceiveEvents Thread Started";
        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
    }

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleIswReceiveEvents Started" << std::endl;

    if ( usbInterface->IsDeviceOpen() )
    {
        //! Added for Hotplug events during initialization
        iswInterfaceInitComplete = true;
    }

    while ( usbInterface->IsDeviceOpen() && enableReadIsw )
    {
        static int recLength = MAX_RECEIVE_BUFFER_SIZE;
        uint8_t receiveBuf[recLength];

        std::memset(receiveBuf, 0, recLength);
        numRead = 0;

        //! Check if the read thread has been stopped
        //! Could happen when test is shutting down
        if ( enableReadIsw )
        {
            switch (deviceType)
            {
                case USB:
                {
                    //! Check again in case device was removed - hotplug
                    if ( usbInterface->IsDeviceOpen() )
                    {
                        uint8_t usbEndpoint = usbInterface->GetEndpointIN();

                        // Serial devices
                        if ( usbInterface->GetBoardType() == UsbInterface::serialBoard )
                        {
                            status = usbInterface->GetSerialInterface()->ReadDevice(receiveBuf, recLength, &numRead);

                            if ( status < 0 )
                            {
                                usleep(1000);
                                continue;
                            }
                        }
                        else
                        {
                            // USB only
                            status = deviceInterface->ReadDevice(receiveBuf, usbEndpoint, recLength, &numRead);
                        }
                    }
                    else
                    {
                        if ( theLogger->DEBUG_IswInterface == 1 )
                        {
                            msgString = "HandleIswReceiveEvents Failed - Device Not Open";
                            theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
                        }
                        status = -1;
                    }
                    break;
                }
                default:
                {
                    status = -1;
                    break;
                }
            }
        }
        else
        {
            status = -1;
        }

        //! If the read was successful and the number of bytes
        //! read in is greater than zero - proceed
        if ( (status == 0) && (numRead > 0) )
        {
            SWIM_Header *swimHeader = nullptr;

            //! Check for SolNet packets
            swimHeader = (SWIM_Header *)(receiveBuf);

            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                std::stringstream ss;
                ss << "Isw Receiving " << std::to_string(numRead) << std::endl;
                ss << "receiveBuf = ";
                ss << std::setfill('0');

                for (int i = 0; i < numRead; i++)
                {
                    ss << std::hex << std::setw(2) << std::to_string(receiveBuf[i]) << " ";
                }

                ss << std::endl;
                ss << " transferSize = " << std::to_string(swimHeader->transferSize) << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());
            }

            //! We are checking for SolNet packets first.  We don't receive SolNet packets if
            //! we have not authenticated with the device yet.
            if ( (iswSecurity->IsInterfaceReady() == true) &&
                 ((swimHeader->peerIndex < 0x3F) || (swimHeader->peerIndex == 0xFF)
                  || (swimHeader->peerIndex >= 0x40 && swimHeader->peerIndex <= 0x5E)) )
            {
                //! SolNet packet - doesn't have the routing header
                if ( numRead > (int)(ISW_SWIM_HDR_SIZE + IswSolNet::Size_SolNet_Header_Only) )
                {
                    uint8_t peerIndex = swimHeader->peerIndex;
                    uint8_t endpoint  = swimHeader->streamNumber;

                    //! SolNet Packet
                    uint8_t *solNetPtr                       = (uint8_t *)(receiveBuf + ISW_SWIM_HDR_SIZE);
                    IswSolNet::iswSolNetHeader *iswSolNetHdr = (IswSolNet::iswSolNetHeader *)solNetPtr;
                    uint8_t *data                            = nullptr;

                    if ( solNetPtr == nullptr )
                    {
                        continue;
                    }

                    if ( iswSolNetHdr->protocolClass == IswSolNet::MessagePacket )
                    {
                        //! Buffer that will hold the complete SolNet Packet
                        data = new uint8_t[swimHeader->transferSize];

                        //! Copy in what we received
                        memcpy(data, solNetPtr, swimHeader->transferSize);
                    }
                    else
                    {
                        //! SolNet Data Packet - check for multiple USB buffers

                        //! Is there more data following this first SolNet packet with header?
                        uint32_t totalSolNetPacketSize = IswSolNet::Size_SolNet_Header_Only + IswSolNet::Size_SolNet_DataPktHdr + iswSolNetHdr->iswSolNetDataPkt.dataLength;

                        //! Buffer that will hold the complete SolNet Packet
                        data = new uint8_t[totalSolNetPacketSize];

                        //! Copy in what we already received
                        memcpy(data, solNetPtr, swimHeader->transferSize);

                        //! If there is more data to get, it will be coming next from USB read
                        if ( totalSolNetPacketSize > (swimHeader->transferSize + IswSolNet::Size_SolNet_Header_Only) )
                        {
                            std::stringstream dbg3;
                            dbg3 << Q_FUNC_INFO << " More data to read." << std::endl;
                            std::cout << dbg3.str();

                            uint32_t bytesToGet = totalSolNetPacketSize - swimHeader->transferSize - IswSolNet::Size_SolNet_Header_Only;
                            int bufLength       = bytesToGet;
                            uint8_t bytesLeftBuf[bufLength];

                            while ( bytesToGet > 0 )
                            {
                                int numberReadIn = 0;
                                memset(bytesLeftBuf, 0, bufLength);

                                //! The next incoming USB data will be the rest of the SolNet packet
                                switch (deviceType)
                                {
                                    case USB:
                                    {
                                        //! Check again in case device was removed - hotplug
                                        if ( usbInterface->IsDeviceOpen() )
                                        {
                                            //serial receiving with packetGen
                                            if ( usbInterface->GetBoardType() == UsbInterface::serialBoard )
                                            {
                                                status = usbInterface->GetSerialInterface()->ReadDevice(receiveBuf, recLength, &numRead);
                                                if ( status < 0 )
                                                {
                                                    usleep(1000);
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                uint8_t usbEndpoint = usbInterface->GetEndpointIN();
                                                status              = deviceInterface->ReadDevice(bytesLeftBuf, usbEndpoint, bytesToGet, &numberReadIn);
                                            }
                                        }
                                        else
                                        {
                                            if ( theLogger->DEBUG_IswInterface == 1 )
                                            {
                                                msgString = "HandleIswReceiveEvents Failed - Device Not Open";
                                                theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
                                            }
                                            status = -1;
                                        }
                                        break;
                                    }
                                    default:
                                    {
                                        if ( theLogger->DEBUG_IswInterface == 1 )
                                        {
                                            msgString = "HandleIswReceiveEvents Failed - Bad Device";
                                            theLogger->LogMessage(msgString, usbInterface->GetIndex(), std::chrono::system_clock::now());
                                        }
                                        break;
                                    }
                                }

                                if ( numberReadIn <= 0 )
                                {
                                    break;
                                }
                                else
                                {
                                    //! Move pointer into the receive buffer after data we already received
                                    uint8_t *bytesToGetPtr = (uint8_t *)(bytesLeftBuf + ISW_SWIM_HDR_SIZE);

                                    //! Copy in what we just received
                                    memcpy(data, bytesToGetPtr, numberReadIn);
                                    bytesToGet -= numberReadIn;
                                }
                            }
                        }
                    }

                    // Add buffer with complete SolNet data packet
                    AddToReceiveQueue(peerIndex, endpoint, data);
                }
                else
                {
                    if ( theLogger->DEBUG_IswInterface == 1 )
                    {
                        msgString = "ReadDevice numRead on SolNet Is Less Than Header Sizes";
                        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                    }
                }
            }
            else
            {
                //! ISW Control Message
                //! Firmware controls what comes through until we authenticate
                //! These ISW API commands have a SWIM header and a routing header
                iswRoutingHdr *routingHdr = nullptr;
                iswEvent *event           = nullptr;
                uint8_t *data             = nullptr;

                if ( numRead >= (int)(ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE + ISW_EVENT_SIZE) )
                {
                    RemoveSwimHeader(receiveBuf, &swimHeader, &routingHdr, &event, &data);

                    if ( event != nullptr )
                    {
                        //! Check for any error from the firmware
                        switch (event->resultCode)
                        {
                            case ISW_CMD_SUCCESS:
                            {
                                //! On success - deliver it!
                                DeliverIswMessage(routingHdr, event, data);
                                break;
                            }
                            case ISW_CMD_FAILURE_OPERATION_PENDING:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Operation Pending";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_GENERAL:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice General Failure";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_NOT_FOUND:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - Not Found";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_ID_MISMATCH:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - ID Mismatch";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_INSUFF_MEMORY:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - Insufficient Memory";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_INVALID_ACTION:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - Invalid Action";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_TIMEOUT:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - Timeout";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            case ISW_CMD_FAILURE_INVALID_SIZE:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Failure - Invalid Size";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                break;
                            }
                            default:
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    msgString = "ReadDevice Bad Event Result Code";
                                    theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                                }
                                qDebug() << "bad event result code. eventType, event, result code: " << QString::number(event->eventType) << ", " << QString::number(event->event) << ", " << event->resultCode;
                                break;

                            }
                        }
                    }
                }
                else
                {
                    if ( theLogger->DEBUG_IswInterface == 1 )
                    {
                        msgString = "ReadDevice numRead Less Than Header Sizes";
                        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
                    }
                }
            }
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "ReadDevice Bad Status or No Data";
                theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
            }

            //qDebug() << "Bad status or no data. Status: " << QString::number(status) << " numRead: " << QString::number(numRead);
        }

        usleep(10); //! Sleep 10 microseconds
    }
#endif
}

//!###############################################################
//! StartIswSendQThread()
//! Inputs: None
//! Description:
//!###############################################################
void IswInterface::StartIswSendQThread()
{
    iswCmdLock.lock();
    processDataSendQueues = true;
    iswCmdLock.unlock();
}

void IswInterface::StopIswSendQThread()
{

    iswCmdLock.lock();
    processDataSendQueues = false;
    iswCmdLock.unlock();
}

void IswInterface::HandleIswSendQueues(void)
{
    int status = 0;
    std::string msgString;

#ifndef TESTING  //! Don't run during unit tests
    //! Wait for main thread to be ready
    while ( !processDataSendQueues )
    {
        usleep(10);
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        msgString = "HandleIswSendQueues Thread Started";
        theLogger->LogMessage(msgString, iswListIndex, std::chrono::system_clock::now());
    }

    std::cout << "Device " << std::to_string(GetIndex()) << " HandleIswSendQueues Started" << std::endl;

    while ( processDataSendQueues )
    {
        // Process in this order:
        // HiNoDiscard until empty
        // HiDiscard for three - five messages
        // If HiNoDiscard queues are empty - process LowPriority queues:
        // LowNoDiscard then LowDiscard
        // By default - all queues are HiNoDiscard, so cycle through them
        // Queue Zero is used for broadcast or multicast SolNet data packets
        // Queues 1 - 3 are used for unicast SolNet data packets
        for (uint8_t endpoint = 0; endpoint < NumberOfIswEndpoints; endpoint++)
        {
            if ( iswEndpointSendQueues[endpoint].qOs == HiNoDiscard )
            {
                while ( iswEndpointSendQueues[endpoint].sendQ.empty() == false )
                {
                    // Get oldest item on front of queue
                    iswDataSendItem *sendItem           = GetNextFromSendQueue(endpoint);
                    IswSolNet::iswSolNetHeader *dataPkt = (IswSolNet::iswSolNetHeader *)sendItem->iswCmd;                 

                    // Check if we are to flush the queue for this dataflowId
                    // which happens when we shutdown a data traffic stream from the application
                    // Mask the top bit that determines consumer vs. producer - we only use the dataflowId as index withou it
                    if ( iswEndpointSendQueues[endpoint].flushArray[dataPkt->iswSolNetDataPkt.dataflowId & IswSolNet::iswSolNetProviderConsumerMask] == false )
                    {

                        // Deliver the packet to the radio
                        status = SyncSendSolNetDataPacket(sendItem->iswCmd, sendItem->peerIndex, endpoint);
                    }


                    // Cleanup and delete resources for item just sent
                    RemoveSendQueueFront(endpoint);
                    usleep(100);
                }
            }
        }

        // Process low priority queues
        for (uint8_t endpoint = 1; endpoint < NumberOfIswEndpoints; endpoint++)
        {
            // Just send one of these - can change this later
            if ( (iswEndpointSendQueues[endpoint].sendQ.empty() == false) &&
                 (iswEndpointSendQueues[endpoint].qOs != HiNoDiscard) )
            {
                // Get oldest item on front of queue
                iswDataSendItem *sendItem = GetNextFromSendQueue(endpoint);

                status = SyncSendSolNetDataPacket(sendItem->iswCmd, sendItem->peerIndex, endpoint);

                // Cleanup and delete resources for item just sent
                RemoveSendQueueFront(endpoint);
                usleep(100);
            }
        }
    }

#endif
}

uint32_t IswInterface::AddSwimHeader(uint8_t *transferBuf, iswRoutingHdr *routingHdr, iswCommand *iswCmd)
{
    uint32_t length = 0;

    //! See ISW Embedment Guide Section 14.2
    SWIM_Header *swimHeader      = nullptr;
    iswRoutingHdr *routingHdrPtr = nullptr;
    iswCommand *iswCmdPtr        = nullptr;

    if (usbInterface->GetUsbHeaderType() == UsbInterface::USB_HEADER_TRU)
    {
        swimHeader    = (SWIM_Header *)transferBuf;
        routingHdrPtr = (iswRoutingHdr *)&transferBuf[ISW_SWIM_HDR_SIZE];
        iswCmdPtr     = (iswCommand *)&(transferBuf[ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE]);

        switch (iswCmd->iswCmd.cmdType)
        {
            case OnBoardTests:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0x01;
                swimHeader->reserved     = 0;
                break;
            }
            case Firmware:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Identity:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Security:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Association:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Stream:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Link:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Metrics:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case System:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Product:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case Test:
            {
                swimHeader->peerIndex    = 0x3F;
                swimHeader->streamNumber = 0;
                swimHeader->flags        = 0;
                swimHeader->reserved     = ~(swimHeader->peerIndex); // One's Complement
                break;
            }
            case ResrvdCmdEvent0:
            case ResrvdVendor:
            default:
                //! Not implemented or unknown
                break;
        }
    }
    else
    {
        routingHdrPtr = (iswRoutingHdr *)transferBuf;
        iswCmdPtr     = (iswCommand *)&(transferBuf[ISW_ROUTING_HDR_SIZE]);
    }

    //! If it's a Control ISW message it can't be larger than the interface
    //! max packet size
    if ( (swimHeader->peerIndex == 0x3F) &&
         ( (ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE + routingHdr->dataLength) >
           usbInterface->GetMaxPacketSizeOut() ) )
    {
        qDebug() << "swim size: " << ISW_SWIM_HDR_SIZE;
        qDebug() << "routing hdr size: " << ISW_ROUTING_HDR_SIZE;
        qDebug() << "routing hdr len: " << routingHdr->dataLength;
        qDebug() << "got control message instance, reduce send length";
        length = -1;
    }
    else
    {
        //! Copy routing header
        memcpy(routingHdrPtr, routingHdr, ISW_ROUTING_HDR_SIZE);

        //! xfer_size does not include sizeof(SWIM_Header)
        swimHeader->transferSize = ISW_ROUTING_HDR_SIZE + routingHdr->dataLength;

        //! Copy Isw Cmd to buffer
        memcpy(iswCmdPtr, iswCmd, routingHdr->dataLength);

        length = ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE + routingHdr->dataLength;
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        iswEvent *event = nullptr;
        DebugPrintHeaders(swimHeader, routingHdrPtr, event);
    }

    return (length);
}

void IswInterface::RemoveSwimHeader(uint8_t *receiveBuf, SWIM_Header **swimHeader,
                                    iswRoutingHdr **routingHdr, iswEvent **event, uint8_t **data)
{
    *swimHeader = (SWIM_Header *)(receiveBuf);
    *routingHdr = (iswRoutingHdr *)&receiveBuf[ISW_SWIM_HDR_SIZE];
    *event      = (iswEvent *)&receiveBuf[ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE];

    // Check to see if there is no data with this response
    if ( ((SWIM_Header *)(*swimHeader))->transferSize > (ISW_ROUTING_HDR_SIZE + ISW_EVENT_SIZE) )
    {
        //! Some events don't have data.  The individual event types will check this
        *data = &receiveBuf[ISW_SWIM_HDR_SIZE + ISW_ROUTING_HDR_SIZE + ISW_EVENT_SIZE];
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        DebugPrintHeaders(*swimHeader, *routingHdr, *event);
    }
}

void IswInterface::DebugPrintHeaders(SWIM_Header *swimHeader,
                                     iswRoutingHdr *routingHdr, iswEvent *event)
{
    //!Debugging

    //! Log SWIM Header
    std::string msgString;
    std::ostringstream ss; //stringstream

    theLogger->DebugMessage("Start Debug Print Headers Block\n", iswListIndex);

    if ( event != nullptr )
    {
        msgString = "Received Message From Hardware:";

    }
    else
    {
        msgString = "Sending Message To Hardware:";
    }
    theLogger->DebugMessage(msgString, iswListIndex);
    msgString = "SWIM Header:";
    theLogger->DebugMessage(msgString, iswListIndex);
    msgString.clear();

    msgString = " flags = ";
    msgString.append(std::to_string(swimHeader->flags));
    theLogger->DebugMessage(msgString, iswListIndex);
    msgString.clear();

    msgString = " streamNumber = ";
    msgString.append(std::to_string(swimHeader->streamNumber));
    theLogger->DebugMessage(msgString, iswListIndex);
    msgString.clear();

    ss.str( std:: string() );
    ss.clear();

    msgString = " peerIndex = 0x";
    ss << std::hex << (int)swimHeader->peerIndex;
    msgString.append(ss.str());
    theLogger->DebugMessage(msgString, iswListIndex);
    msgString.clear();

    ss.str( std:: string() );
    ss.clear();
    msgString = " transferSize = ";
    msgString.append(std::to_string(swimHeader->transferSize));
    theLogger->DebugMessage(msgString, iswListIndex);

    //! Log Routing Header
    if ( routingHdr != nullptr )
    {
        msgString.clear();
        msgString = "Routing Header:";
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        msgString = " srcAddr = ";
        msgString.append(std::to_string(routingHdr->srcAddr));
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        msgString = " destAddr = ";
        msgString.append(std::to_string(routingHdr->destAddr));
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        msgString = " dataLength = ";
        msgString.append(std::to_string(routingHdr->dataLength));
        theLogger->DebugMessage(msgString, iswListIndex);
    }

    //! When debugging sending side, event will be NULL
    if ( event != nullptr )
    {
        //! Log Event Header
        msgString.clear();
        msgString = "Event Header:";
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        ss.str( std:: string() );
        ss.clear();

        msgString = " event = 0x";
        ss << std::hex << (int)event->event;
        msgString.append(ss.str());
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        ss.str( std:: string() );
        ss.clear();

        msgString = " eventType = ";
        msgString.append(std::to_string(event->eventType));
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        msgString = " resultCode = ";
        msgString.append(std::to_string(event->resultCode));
        theLogger->DebugMessage(msgString, iswListIndex);
        msgString.clear();

        msgString = " eventContext = ";
        msgString.append(std::to_string(event->eventContext));
        theLogger->DebugMessage(msgString, iswListIndex);
    }

    theLogger->DebugMessage("End Debug Print Headers Block\n", iswListIndex);
}

uint8_t IswInterface::GetNextCmdCount()
{
    uint8_t nextCmdCount = 0;

    iswCmdLock.lock();
    iswCmdCount++;
    nextCmdCount = iswCmdCount;
    iswCmdLock.unlock();
    return (nextCmdCount);
}

int IswInterface::SyncSendIswCmd(iswCommand *iswCmmnd, iswRoutingHdr *routingHdr)
{
    std::string msgString;
    int status = 0;

    //! Don't try to send if the device is not open or
    //! or the read thread hasn't started yet
    if ( usbInterface->IsDeviceOpen() && enableReadIsw )
    {
        uint32_t bufLength  = 0;
        uint32_t sendLength = 0;

        bufLength = usbInterface->GetMaxPacketSizeOut();
        uint8_t sendBuf[bufLength];
        std::memset(sendBuf, 0, bufLength);

        //! Add the SWIM Header and data to the sendBuf
        //! Send length is total amount of ISW info in buffer
        sendLength = AddSwimHeader(sendBuf, routingHdr, iswCmmnd);

        if ( sendLength > 0 )
        {

            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                std::ostringstream ss; //stringstream
                ss << "sendBuf = ";
                ss << std::setfill('0');

                for (unsigned int i = 0; i < sendLength; i++)
                {
                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";

                }

                ss << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());

            }

            switch (deviceType)
            {
                case USB:
                {
                    if ( usbInterface->IsDeviceOpen() )
                    {
                        uint16_t usbEndpoint = usbInterface->GetEndpointOUT();

                        // USB/Serial devices
                        if ( usbInterface->GetBoardType() == UsbInterface::serialBoard )
                        {
                            // Send the converted ascii characters to the serial device
                            SerialInterface *serialInterface = usbInterface->GetSerialInterface();
                            if ( serialInterface != nullptr )
                            {
                                status = serialInterface->SyncWriteDevice(sendBuf, sendLength);
                                usleep(500);
                            }
                            else
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    std::stringstream ss;
                                    ss << "serialInterface Pointer NULL!" << std::endl;
                                    std::cout << ss.str();
                                    theLogger->DebugMessage(ss.str(), GetIndex());
                                }
                            }
                        }
                        else
                        {
                            // USB only
                            status = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);

                        }
                        usleep(10);
                    }
                    else
                    {
                        if ( theLogger->DEBUG_IswInterface == 1 )
                        {
                            msgString = "SyncSendIswCmd Failed - Device Not Open";
                            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                        }
                        status = -1;
                    }
                    break;
                }
                default:
                {
                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                    status               = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                    break;
                }
            }

            if ( status < 0 )
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "SyncSendIswCmd Failed - Device Returned Status != 0";
                    theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                }
                status = -1;
            }
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "SyncSendIswCmd: No Data To Send?";
                theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
            }
            status = -1;
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswInterface == 1 )
        {
            msgString = "SyncSendIswCmd Failed - Device Not Open";
            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
        }
        status = -1;
    }

    if(iswCmmnd->iswCmd.command == 19)
    {
        qDebug() << "sleeping on sync send for get phy rates";
        usleep(10000);
    }

    return (status);
}

uint32_t IswInterface::AddSolNetSwimHeader(uint8_t *transferBuf, iswCommand *iswCmd, uint8_t peerIndex, uint8_t endpoint)
{
    uint32_t length = 0;

    //! See ISW Embedment Guide Section 14.2
    SWIM_Header *swimHeader = nullptr;
    iswCommand *iswCmdPtr   = nullptr;

    if (usbInterface->GetUsbHeaderType() == UsbInterface::USB_HEADER_TRU)
    {
        swimHeader = (SWIM_Header *)transferBuf;
        iswCmdPtr  = (iswCommand *)&(transferBuf[ISW_SWIM_HDR_SIZE]);

        uint32_t pktSize = 0;
        switch ( iswCmd->iswSolNetHdr.protocolClass )
        {
            case IswSolNet::DataPacket:
            {
                //! Case Data Packet
                swimHeader->peerIndex    = peerIndex;
                swimHeader->streamNumber = endpoint;
                //!############################################################
                //! The following line accounts for a feature of the FPGA host
                //! board (i.e. CFP Board) for the Commander 256 module, in
                //! which the CFP board checks the Reserved field of the SWIM
                //! Header to be the ones complement of the Peer Index field
                //! of the SWIM Header. In the current implementation, the CFP
                //! board will drop any ISW message whose SWIM Header Reserved
                //! field does not equal the ones complement of the Peer Index
                //! field.
                //! ###########################################################
                swimHeader->reserved = ~(swimHeader->peerIndex);

                //! Copy Isw Cmd to buffer
                pktSize = IswSolNet::Size_SolNet_Header_Only + IswSolNet::Size_SolNet_DataPktHdr + iswCmd->iswSolNetHdr.iswSolNetDataPkt.dataLength;
                memcpy(iswCmdPtr, iswCmd, pktSize);

                //! xfer_size does not include sizeof(SWIM_Header)
                swimHeader->transferSize = pktSize;

                //! Length to return to caller
                length = ISW_SWIM_HDR_SIZE + pktSize;
                break;
            }
            case IswSolNet::MessagePacket:
            {
                //! Case Message Packet
                swimHeader->peerIndex    = peerIndex;
                swimHeader->streamNumber = 0;
                //!############################################################
                //! The following line accounts for a feature of the FPGA host
                //! board (i.e. CFP Board) for the Commander 256 module, in
                //! which the CFP board checks the Reserved field of the SWIM
                //! Header to be the ones complement of the Peer Index field
                //! of the SWIM Header. In the current implementation, the CFP
                //! board will drop any ISW message whose SWIM Header Reserved
                //! field does not equal the ones complement of the Peer Index
                //! field.
                //! ###########################################################
                swimHeader->reserved = ~(swimHeader->peerIndex);

                //! Copy Isw Cmd to buffer
                pktSize = IswSolNet::Size_SolNet_Header_Only + IswSolNet::Size_SolNet_MsgPktHdr + iswCmd->iswSolNetHdr.iswSolNetMessagePkt.messageLength;
                memcpy(iswCmdPtr, iswCmd, pktSize);

                //! xfer_size does not include sizeof(SWIM_Header)
                swimHeader->transferSize = pktSize;

                //! Length to return to caller
                length = ISW_SWIM_HDR_SIZE + pktSize;
                break;
            }
            default:
            {
                //! Do nothing - length will be zero
                break;
            }
        }
    }
    else
    {
        // Not USB case
        iswCmdPtr = (iswCommand *)transferBuf;
    }

    if ( theLogger->DEBUG_IswInterface == 1 )
    {
        iswEvent *event = nullptr;
        DebugPrintHeaders(swimHeader, nullptr, event);
    }

    return (length);
}

void IswInterface::AddToSendQueue(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint)
{
    iswEndpointSendQueues[endpoint].qLock.lock();

    iswDataSendItem *sendItem = new iswDataSendItem;
    sendItem->peerIndex       = peerIndex;
    sendItem->iswCmd          = iswCmmnd;
    iswEndpointSendQueues[endpoint].sendQ.push_back(sendItem);

    iswEndpointSendQueues[endpoint].qLock.unlock();
}

IswInterface::iswDataSendItem *IswInterface::GetNextFromSendQueue(uint8_t endpoint)
{
    iswEndpointSendQueues[endpoint].qLock.lock();
    iswDataSendItem *sendItem = iswEndpointSendQueues[endpoint].sendQ.front();
    iswEndpointSendQueues[endpoint].qLock.unlock();

    IswSolNet::iswSolNetHeader *dataPkt = (IswSolNet::iswSolNetHeader *)sendItem->iswCmd;

    int dataPolicies = dataPkt->iswSolNetDataPkt.policies;

    //qDebug() << "data policies: " << dataPolicies;

    int seqNum         = dataPkt->iswSolNetDataPkt.seqNumber;
    int headerChecksum = dataPkt->iswSolNetDataPkt.headerChecksum;
    uint64_t now       = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();

    sendingData = true;

    QString macAddressStr = QString::fromStdString(iswIdentity->GetMacAddressStr());
    QString peerIndexStr = QString::number(sendItem->peerIndex);
    QString endpointStr = QString::number(endpoint);

    // could be used to filter out non data messages, data messages sent have checksums in this range.
    //control messages use a different range and we want to ignore them.
    // Edit: Messes with Pgen with different packet sizes, but is needed for video
    int headerChecksumMin = 60000;
    int headerChecksumMax = 63000;

    //qDebug() << "send headerChecksum and endpoint: " << QString::number(headerChecksum) + ", " + QString::number(endpoint);

    // dataPolicies == 2 for pgen
    bool sendingVideo = false;

    if(dataPolicies == 0)
    {
        sendingVideo = true;
    }


    // Used for Pgen
   if(!sendingVideo && endpoint > 0)
    {
        msgSendCnt++;
        QString msgCntStr = QString::number(msgSendCnt);
        writeOutLatencyEvent(msgCntStr, macAddressStr, peerIndexStr, endpointStr, seqNum, headerChecksum, now, "SEND");
    }
    else if(sendingVideo && endpoint > 0 && headerChecksum > headerChecksumMin && headerChecksum < headerChecksumMax)
    {
        msgSendCnt++;
        QString msgCntStr = QString::number(msgSendCnt);
        writeOutLatencyEvent(msgCntStr, macAddressStr, peerIndexStr, endpointStr, seqNum, headerChecksum, now, "SEND");
    }

    return (sendItem);

}

void IswInterface::RemoveSendQueueFront(uint8_t endpoint)
{
    iswEndpointSendQueues[endpoint].qLock.lock();

    // Remove list element and delete
    std::list<iswDataSendItem *>::iterator it = iswEndpointSendQueues[endpoint].sendQ.begin();
    iswDataSendItem *sendItem                 = *it;
    delete (sendItem->iswCmd);

    delete (sendItem);
    it = iswEndpointSendQueues[endpoint].sendQ.erase(it);

    iswEndpointSendQueues[endpoint].qLock.unlock();
}

int IswInterface::SyncSendSolNetMessagePacket(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint)
{
    std::string msgString;
    int status = 0;

    iswSolNet->iswSolNetCmdLock.lock();

    //! Don't try to send if the device is not open or
    //! or the read thread hasn't started yet
    if ( usbInterface->IsDeviceOpen() && enableReadIsw )
    {
        static int bufLength = 0;
        uint32_t sendLength  = 0;

        bufLength = usbInterface->GetMaxPacketSizeOut();
        uint8_t sendBuf[bufLength];
        std::memset(sendBuf, 0, bufLength);

        //! Add the SWIM Header and data to the sendBuf
        //! Send length is total amount of ISW info in buffer
        sendLength = AddSolNetSwimHeader(sendBuf, iswCmmnd, peerIndex, endpoint);

        if ( sendLength > 0 )
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                std::ostringstream ss; //stringstream
                ss << "sendBuf = ";
                ss << std::setfill('0');
                for (unsigned int i = 0; i<sendLength; i++)
                {
                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";
                }
                ss << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());
            }

            switch (deviceType)
            {
                case USB:
                {
                    if ( usbInterface->IsDeviceOpen() )
                    {
                        //serial
                        if ( usbInterface->GetBoardType() == UsbInterface::serialBoard )
                        {
                            // Send the converted ascii characters to the serial device
                            SerialInterface *serialInterface = usbInterface->GetSerialInterface();
                            if ( serialInterface != nullptr )
                            {
                                status = serialInterface->SyncWriteDevice(sendBuf, sendLength);
                            }
                            else
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    std::stringstream ss;
                                }
                            }
                        }
                        else
                        {
                            uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                            status               = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                            usleep(10);
                        }
                    }
                    else
                    {
                        if ( theLogger->DEBUG_IswInterface == 1 )
                        {
                            msgString = "SyncSendIswCmd Failed - Device Not Open";
                            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                        }
                        status = -1;
                    }
                    break;
                }
                default:
                {
                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                    status               = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                    break;
                }
            }

            if ( status < 0 )
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "SyncSendIswCmd failed - Device Returned Status != 0";
                    theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                }
            }
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "SyncSendIswCmd: No Data To Send?";
                theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
            }
            status = -1;
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswInterface == 1 )
        {
            msgString = "SyncSendIswCmd Failed - Device Not Open";
            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
        }
        status = -1;
    }
    iswSolNet->iswSolNetCmdLock.unlock();

    return (status);
}

int IswInterface::SyncSendSolNetDataPacket(iswCommand *iswCmmnd, uint8_t peerIndex, uint8_t endpoint)
{
    std::string msgString;
    int status = 0;

    iswSolNet->iswSolNetCmdLock.lock();

    //! Don't try to send if the device is not open or
    //! or the read thread hasn't started yet
    if ( usbInterface->IsDeviceOpen() && enableReadIsw )
    {
        static int bufLength = 0;
        uint32_t sendLength  = 0;

        bufLength = usbInterface->GetMaxLibUsbPktSize();
        uint8_t sendBuf[bufLength];
        std::memset(sendBuf, 0, bufLength);

        //! Add the SWIM Header and data to the sendBuf
        //! Send length is total amount of ISW info in buffer
        sendLength = AddSolNetSwimHeader(sendBuf, iswCmmnd, peerIndex, endpoint);

        if ( sendLength > 0 )
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                std::ostringstream ss; //stringstream
                ss << "sendBuf = ";
                ss << std::setfill('0');
                for (unsigned int i = 0; i < sendLength; i++)
                {
                    ss <<  std::hex << std::setw(2) << (int)sendBuf[i] << " ";
                }
                ss << std::endl;
                theLogger->DebugMessage(ss.str(), GetIndex());
            }

            switch (deviceType)
            {
                case USB:
                {
                    if ( usbInterface->IsDeviceOpen() )
                    {
                        //serial
                        if ( usbInterface->GetBoardType() == UsbInterface::serialBoard )
                        {
                            // Send the converted ascii characters to the serial device
                            SerialInterface *serialInterface = usbInterface->GetSerialInterface();
                            if ( serialInterface != nullptr )
                            {
                                status = serialInterface->SyncWriteDevice(sendBuf, sendLength);
                            }
                            else
                            {
                                if ( theLogger->DEBUG_IswInterface == 1 )
                                {
                                    std::stringstream ss;
                                }
                            }
                        }
                        else
                        {
                            uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                            status               = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                            usleep(10);
                        }
                    }
                    else
                    {
                        if ( theLogger->DEBUG_IswInterface == 1 )
                        {
                            msgString = "SyncSendIswCmd Failed - Device Not Open";
                            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                        }
                        status = -1;
                    }

                    break;
                }
                default:
                {
                    uint16_t usbEndpoint = usbInterface->GetEndpointOUT();
                    status               = usbInterface->SyncWriteDevice(sendBuf, usbEndpoint, sendLength);
                    break;
                }
            }

            if ( status < 0 )
            {
                if ( theLogger->DEBUG_IswInterface == 1 )
                {
                    msgString = "SyncSendIswCmd failed - Device Returned Status != 0";
                    theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
                }
            }
        }
        else
        {
            if ( theLogger->DEBUG_IswInterface == 1 )
            {
                msgString = "SyncSendIswCmd: No Data To Send?";
                theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
            }
            status = -1;
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswInterface == 1 )
        {
            msgString = "SyncSendIswCmd Failed - Device Not Open";
            theLogger->LogMessage(msgString, GetIndex(), std::chrono::system_clock::now());
        }
        status = -1;
    }
    iswSolNet->iswSolNetCmdLock.unlock();

    return (status);
}

void IswInterface::writeOutLatencyEvent(QString msgCntStr, QString macAddressStr, QString peerIndexStr, QString endpointStr, int seqNum, int headerChecksum, uint64_t timestamp, QString sendOrRec)
{

//    long msgCnt = 0;
//    if(sendOrRec.compare("SEND") == 0 )
//    {
//        //qDebug() << "got send" << msgSendCnt;
//        msgCnt = msgSendCnt;
//        //qDebug() << "got send after set" << msgCnt;
//    }
//    else if(sendOrRec.compare("RECEIVE") == 0)
//    {
//        //qDebug() << "got rec" << msgRecCnt;
//        msgCnt = msgRecCnt;
//        //qDebug() << "got rec after set" << msgCnt;
//    }

    QString msgQStr = msgCntStr + ", " + macAddressStr + ", " + peerIndexStr + ", " + endpointStr + ", " + QString::number(seqNum) + "," + QString::number(headerChecksum) + "," + QString::number(timestamp) + "," + sendOrRec;

    qDebug() << msgQStr;

    latencyLogger->LogSimpleMessage(msgQStr.toStdString());
}

void IswInterface::printBytes(uint8_t *bytePtr, int nrBytes)
{
    std::stringstream ss;
    char hexFormat[3];
    int i = 0;

    ss << Q_FUNC_INFO << "Bytes: ";
    for(i=0;i<nrBytes;i++)
    {
        sprintf(hexFormat, "%02x", *(bytePtr+i));
        ss << hexFormat << " ";
    }
    ss << std::endl;
    std::cout << ss.str();
}
