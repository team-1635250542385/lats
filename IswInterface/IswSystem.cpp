//!###############################################
//! Filename: IswSystem.cpp
//! Description: Class that provides an interface
//!              to ISW System commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswSystem.h"
#include "IswInterface.h"
#include <sstream>

//!###############################################################
//! Constructor: IswSystem()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswSystem::IswSystem(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswSystem::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswSystem::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
        case ResetDevice:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "ResetDevice";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case SetFactoryDefault:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "SetFactoryDefault";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case IndicationsSet:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "IndicationsSet";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case IndicationsGet:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "IndicationsGet";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetRadioStatus:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "Received GetRadioStatus ";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            IswGetRadioStatusEvent *getRadioStatusData = (IswGetRadioStatusEvent *)data;
            iswRadioState                              = getRadioStatusData->state;
            iswRadioChipVersion                        = getRadioStatusData->chipVersion;
            iswRadioFirmwareUpdateType                 = getRadioStatusData->firmwareUpdateType;

            std::string radioState              = " ";
            std::string radioChipVersion        = " ";
            std::string radioFirmwareUpdateType = " ";

            switch(iswRadioState)
            {
                case ACTIVE:
                {
                    radioState = "Active";
                    break;
                }
                case WAITING_CRYPTO_SESSIONS_START:
                {
                    radioState = "Waiting for Start of Crypto Sessions";
                    break;
                }
                case IDLED:
                {
                    radioState = "Idled";
                    break;
                }
                default:
                {
                    radioState = "Invalid Radio State";
                    break;
                }
            }

            switch(iswRadioChipVersion)
            {
                case AL5350:
                {
                    radioChipVersion = "AL5350";
                    break;
                }
                case AL5350B:
                {
                    radioChipVersion = "AL5350B";
                    break;
                }
                default:
                {
                    radioChipVersion = "Invalid Chip Version";
                    break;
                }
            }

            switch(iswRadioFirmwareUpdateType)
            {
                case LEGACY:
                {
                    radioFirmwareUpdateType = "Legacy";
                    break;
                }
                case ECDSA_256:
                {
                    radioFirmwareUpdateType = "ECDSA-256";
                    break;
                }
                default:
                {
                    radioFirmwareUpdateType = "Invalid Firmware Update Type";
                    break;
                }
            }

            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                std::stringstream ss;
                ss << "  radioState              = " << radioState << std::endl;
                ss << "  radioChipVersion        = " << radioChipVersion << std::endl;
                ss << "  radioFirmwareUpdateType = " << radioFirmwareUpdateType << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case RadioReset:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "RadioReset";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case IdleRadio:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "IdleRadio";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            isRadioIdle = true;
            break;
        }
        case ResetIndication:
        {
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "Received Successful Reset Indication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            receivedResetIndication = true;
            break;

        }
        default:
            if ( theLogger->DEBUG_IswSystem == 1 )
            {
                msgString = "Bad System Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
    }
}

//!###################################################################
//! SetupResetDeviceCmd()
//! Inputs: iswSystemCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSystem::SetupResetDeviceCmd(iswSystemCommand *iswSystemCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupResetDeviceCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSystemCmd->cmdType    = IswInterface::System;
    iswSystemCmd->command    = ResetDevice;
    iswSystemCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSystemCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSystemCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSystemCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSystemCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSystemCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSystemCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendResetDeviceCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendResetDeviceCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendResetDeviceCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupResetDeviceCmd(&iswCmd.iswSystemCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendResetDeviceCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupIdleRadioCmd()
//! Inputs: iswSystemCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSystem::SetupIdleRadioCmd(iswSystemCommand *iswSystemCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupIdleRadioCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSystemCmd->cmdType    = IswInterface::System;
    iswSystemCmd->command    = IdleRadio;
    iswSystemCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSystemCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSystemCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSystemCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSystemCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSystemCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSystemCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendIdleRadioCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendIdleRadioCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendIdleRadioCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupIdleRadioCmd(&iswCmd.iswSystemCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendIdleRadioCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupResetRadioCmd()
//! Inputs: iswSystemCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSystem::SetupResetRadioCmd(iswSystemCommand *iswSystemCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupResetRadioCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSystemCmd->cmdType    = IswInterface::System;
    iswSystemCmd->command    = RadioReset;
    iswSystemCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSystemCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSystemCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSystemCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSystemCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSystemCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSystemCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendResetRadioCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendResetRadioCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendResetRadioCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupResetRadioCmd(&iswCmd.iswSystemCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendResetRadioCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!#############################################################################
//! SetupSetIndicationsCmd()
//! Inputs: iswSetIndicationsCmd   - pointer to the command structure
//!         wireInterfaceEnable    - 0 = disable, 1 = enable output to wire
//!                                  interface, default = 1 (leave this 1 if
//!                                  you want to send/receive over the wireless)
//!         associationIndEnable   - 0 = disable, 1 = enable association and
//!                                  disassociation indications
//!         connectionIndEnable    - 0 = disable, 1 = enable connection and
//!                                  disconnection indications
//!         resetIndEnable         - 0 = disable, 1 = enable reset indiciation
//!         streamsStatusIndEnable - 0 = disable, 1,2,3,4 = enable streams
//!                                  status indications in the verion
//!                                  used 1,2,3, or 4. Non-zero enables
//!                                  multicast group status messages as well
//!         hibernationIndEnable   - Set to 1 to enable hibernation indications (0 by default)
//!         routingHdr             - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware.  Enable or disable at startup. If enabled
//!              the indications come in autonomously and the parse methods in
//!              the Streams and Association classes are used to store
//!              the incoming data.
//!#############################################################################
void IswSystem::SetupSetIndicationsCmd(iswSetIndicationsCommand *iswSetIndicationsCmd, uint8_t wireInterfaceEnable, uint8_t associationIndEnable,
                                       uint8_t connectionIndEnable, uint8_t resetIndEnable, uint8_t multicastIndEnable, uint8_t streamsStatusIndEnable, uint8_t hibernationIndEnable, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupSetIndicationsCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetIndicationsCmd->cmdType                = IswInterface::System;
    iswSetIndicationsCmd->command                = IndicationsSet;
    iswSetIndicationsCmd->cmdContext             = iswInterface->GetNextCmdCount();
    iswSetIndicationsCmd->reserved1              = 0x00;
    iswSetIndicationsCmd->reserved2              = 0x00;

    //! For the following, 1 = enable; 0 = disable
    iswSetIndicationsCmd->wireInterfaceEnable    = wireInterfaceEnable;
    iswSetIndicationsCmd->associationIndEnable   = associationIndEnable;
    iswSetIndicationsCmd->connectionIndEnable    = connectionIndEnable;
    iswSetIndicationsCmd->resetIndEnable         = resetIndEnable;
    iswSetIndicationsCmd->multicastIndEnable     = multicastIndEnable;
    iswSetIndicationsCmd->streamsStatusIndEnable = streamsStatusIndEnable;
    iswSetIndicationsCmd->hibernationIndEnable   = hibernationIndEnable;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSetIndicationsCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType                = " << std::to_string(iswSetIndicationsCmd->cmdType) << std::endl;
        ss << "    command                = " << std::to_string(iswSetIndicationsCmd->command) << std::endl;
        ss << "    cmdContext             = " << std::to_string(iswSetIndicationsCmd->cmdContext) << std::endl;
        ss << "    reserved1              = " << std::to_string(iswSetIndicationsCmd->reserved1) << std::endl;
        ss << "    reserved2              = " << std::to_string(iswSetIndicationsCmd->reserved2) << std::endl;
        ss << "    wireInterfaceEnable    = " << std::to_string(iswSetIndicationsCmd->wireInterfaceEnable) << std::endl;
        ss << "    associationIndEnable   = " << std::to_string(iswSetIndicationsCmd->associationIndEnable) << std::endl;
        ss << "    connectionIndEnable    = " << std::to_string(iswSetIndicationsCmd->connectionIndEnable) << std::endl;
        ss << "    resetIndEnable         = " << std::to_string(iswSetIndicationsCmd->resetIndEnable) << std::endl;
        ss << "    multicastIndEnable     = " << std::to_string(iswSetIndicationsCmd->multicastIndEnable) << std::endl;
        ss << "    streamsStatusIndEnable = " << std::to_string(iswSetIndicationsCmd->streamsStatusIndEnable) << std::endl;
        ss << "    hibernationIndEnable   = " << std::to_string(iswSetIndicationsCmd->hibernationIndEnable) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetIndicationsCmd()
//! Inputs: iswSetIndicationsCmd   - pointer to the command structure
//!         wireInterfaceEnable    - 0 = disable, 1 = enable output to wire
//!                                  interface, default = 1 (leave this 1 if
//!                                  you want to send/receive over the wireless)
//!         associationIndEnable   - 0 = disable, 1 = enable association and
//!                                  disassociation indications
//!         connectionIndEnable    - 0 = disable, 1 = enable connection and
//!                                  disconnection indications
//!         resetIndEnable         - 0 = disable, 1 = enable reset indiciation
//!         multicastIndEnable     - Set to 1 to enable multicast indication (1 by default)
//!         streamsStatusIndEnable - 0 = disable, 1,2,3,4 = enable streams
//!                                  status indications in the verion
//!                                  used 1,2,3, or 4. Non-zero enables
//!                                  multicast group status messages as well
//!         hibernationIndEnable   - 0 = disable, 1 = enable, 0 by default
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendSetIndicationsCmd(uint8_t wireInterfaceEnable, uint8_t associationIndEnable, uint8_t connectionIndEnable,
                                      uint8_t resetIndEnable, uint8_t multicastIndEnable, uint8_t streamsStatusIndEnable, uint8_t hibernationIndEnable)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendSetIndicationsCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetIndicationsCmd(&iswCmd.iswSetIndicationsCmd, wireInterfaceEnable, associationIndEnable, connectionIndEnable,
                           resetIndEnable, multicastIndEnable, streamsStatusIndEnable, hibernationIndEnable,(void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendSetIndicationsCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetFactoryDefaultCmd()
//! Inputs: iswSystemCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSystem::SetupSetFactoryDefaultCmd(iswSystemCommand *iswSystemCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupFactoryDefaultCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSystemCmd->cmdType    = IswInterface::System;
    iswSystemCmd->command    = SetFactoryDefault;
    iswSystemCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSystemCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSystemCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSystemCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSystemCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSystemCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSystemCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetFactoryDefaultCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendSetFactoryDefaultCmd(void)
{
    std::string msgString;    
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendFactoryDefaultCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetFactoryDefaultCmd(&iswCmd.iswSystemCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendFactoryDefaultCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetRadioStatusCmd()
//! Inputs: iswSystemCmd - pointer to the command structure
//!         routingHdr   - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswSystem::SetupGetRadioStatusCmd(iswSystemCommand *iswSystemCmd, void *routingHdr)
{
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::string msgString;
        msgString = "SetupGetRadioStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSystemCmd->cmdType    = IswInterface::System;
    iswSystemCmd->command    = GetRadioStatus;
    iswSystemCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSystemCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSystemCommand);

    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSystemCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSystemCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSystemCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSystemCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetRadioStatus()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSystem::SendGetRadioStatusCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSystem == 1 )
    {
        msgString = "SendGetRadioStatusCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetRadioStatusCmd(&iswCmd.iswSystemCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSystem == 1 )
        {
            msgString = "SendGetRadioStatusCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}
