#-------------------------------------------------
#
# Project created by QtCreator 2018-12-19T15:56:31
# Project name: IswInterface part of ISW Linux lib
#
#-------------------------------------------------

QT       += xml dbus

QT       -= gui

TARGET = IswInterface
TEMPLATE = lib
VERSION = 2.0.0

DEFINES += ISWINTERFACE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ../Logger/Logger.cpp \
    ../UsbInterface/UsbInterface.cpp \
    ../SerialInterface/SerialInterface.cpp \
    ../UsbInterface/UsbInterfaceInit.cpp \
    ../OnBoard/OnBoard.cpp \
    ./IswFirmware.cpp \
    ./IswSystem.cpp \
    ./IswIdentity.cpp \
    ./IswSecurity.cpp \
    ./IswAssociation.cpp \
    ./IswStream.cpp \
    ./IswLink.cpp \
    ./IswMetrics.cpp \
    ./IswProduct.cpp \
    ./IswSolNet.cpp \
    ./IswInterface.cpp \
    ./IswInterfaceInit.cpp

HEADERS += \
    ../Logger/Logger.h \
    ./Interface.h \
    ../UsbInterface/UsbInterface.h \
    ../SerialInterface/SerialInterface.h \
    ../UsbInterface/UsbInterfaceInit.h \
    ../OnBoard/OnBoard.h \
    ./IswFirmware.h \
    ./IswSystem.h \
    ./IswIdentity.h \
    ./IswSecurity.h \
    ./IswAssociation.h \
    ./IswStream.h \
    ./IswLink.h \
    ./IswMetrics.h \
    ./IswProduct.h \
    ./IswSolNet.h \
    ./IswInterface.h \
    ./IswInterfaceInit.h

LIBS += -L/usr/lib64 -lusb-1.0 -lftdi1

unix {
    target.path = /usr/lib64
    INSTALLS += target
}
