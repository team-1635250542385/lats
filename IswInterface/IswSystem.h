//!###############################################
//! Filename: IswSystem.h
//! Description: Class that provides an interface
//!              to ISW System commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWSYSTEM_H
#define ISWSYSTEM_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswSystem
{

public:
    IswSystem(IswInterface *isw_interface, Logger *logger);
    ~IswSystem() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW System command structures
    struct iswSystemCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct iswSetIndicationsCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t reserved2;
        uint8_t wireInterfaceEnable;
        uint8_t associationIndEnable;
        uint8_t connectionIndEnable;
        uint8_t resetIndEnable;
        uint8_t multicastIndEnable;
        uint8_t streamsStatusIndEnable;
        uint8_t hibernationIndEnable; // FW 30810 or later
    }__attribute__((packed));

    struct iswGetIndicationsCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t reserved2;
        uint8_t wireInterfaceEnable;
        uint8_t associationIndEnable;
        uint8_t connectionIndEnable;
        uint8_t resetIndEnable;
        uint8_t multicastIndEnable;
        uint8_t streamsStatusIndEnable;
        uint8_t hibernationIndEnable; // FW 30810 or later
    }__attribute__((packed));

    enum iswSystemRadioState: uint8_t
    {
        ACTIVE                         = 0x00,
        WAITING_CRYPTO_SESSIONS_START  = 0x01,
        IDLED                          = 0x02
    };

    enum iswSystemChipVersion: uint8_t
    {
        AL5350  = 0x00,
        AL5350B = 0x01
    };

    enum iswSystemFirmwareUpdateType: uint8_t
    {
        LEGACY    = 0x00,
        ECDSA_256 = 0x01
    };

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    bool GetIswRadioIdle(void) { return (isRadioIdle); }
    bool ResetIndicationReceived(void) { return (receivedResetIndication); }
    uint8_t GetIswRadioState(void) { return (iswRadioState); }
    uint8_t GetIswRadioChipVersion(void) { return (iswRadioChipVersion); }
    uint8_t GetRadioFirmwareUpdateType(void) { return (iswRadioFirmwareUpdateType); }

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendResetDeviceCmd(void);
    int SendIdleRadioCmd(void);
    int SendResetRadioCmd(void);
    int SendSetIndicationsCmd(uint8_t wireInterfaceEnable, uint8_t associationIndEnable, uint8_t connectionIndEnable,
                               uint8_t resetIndEnable, uint8_t multicastIndEnable, uint8_t streamsStatusIndEnable, uint8_t hibernationIndEnable);
    int SendSetFactoryDefaultCmd(void);
    int SendGetRadioStatusCmd(void);

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex IswSystemCmdLock;

    //! Class variables
    bool receivedResetIndication       = false;
    bool isRadioIdle                   = false;
    uint8_t iswRadioState              = 0x00;
    uint8_t iswRadioChipVersion        = 0x00;
    uint8_t iswRadioFirmwareUpdateType = 0x00;

    struct IswGetRadioStatusEvent
    {
        uint8_t state;
        uint8_t chipVersion;
        uint8_t firmwareUpdateType;
        uint8_t reserved1[3];
    }__attribute__((packed));


    //! ISW System commands and events
    enum iswSystemCmdEventTypes: uint8_t
    {
        SysReserved             = 0x00,
        //! 0x01 - N
        ResetDevice             = 0x04, //! perform a chip reset
        SetFactoryDefault       = 0x05,
        IndicationsSet          = 0x06,
        IndicationsGet          = 0x07,
        GetRadioStatus          = 0x09,
        //! Reserved gap
        RadioReset              = 0x10, //! Reset radio only - not bus
        IdleRadio               = 0x11,
        //! Reserved gap
        ResetIndication         = 0x81
    };

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupResetDeviceCmd(iswSystemCommand *iswSystemCmd, void *routingHdr);
    void SetupIdleRadioCmd(iswSystemCommand *iswSystemCmd, void *routingHdr);
    void SetupResetRadioCmd(iswSystemCommand *iswSystemCmd, void *routingHdr);
    void SetupSetIndicationsCmd(iswSetIndicationsCommand *iswSetIndicationsCmd, uint8_t wireInterfaceEnable, uint8_t associationIndEnable,
                                uint8_t connectionIndEnable, uint8_t resetIndEnable, uint8_t multicastIndEnable,  uint8_t streamsStatusIndEnable, uint8_t hibernationIndEnable, void *routingHdr);
    void SetupSetFactoryDefaultCmd(iswSystemCommand *iswSystemCmd, void *routingHdr);
    void SetupGetRadioStatusCmd(iswSystemCommand *iswSystemCmd, void *routingHdr);

};

#endif //! ISWSYSTEM_H
