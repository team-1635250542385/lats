//!###############################################
//! Filename: IswLink.cpp
//! Description: Class that provides an interface
//!              to ISW Link commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswLink.h"
#include "IswInterface.h"
#include <sstream>

#include <QDebug>

//!###############################################################
//! Constructor: IswLink()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswLink::IswLink(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswLink::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswLink::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    //qDebug() << "ISW Link Deliver Called with cmd: " << cmd;

    switch (cmd)
    {
        case SetScanDutyCycle:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetScanDutyCycle confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetScanDutyCycle:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetScanDutyCycle=";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswLinkCmdLock.lock();

            IswScanDutyCycleEvent *scanDutyCycleData = (IswScanDutyCycleEvent *)data;
            iswStartupScanDuration                   = scanDutyCycleData->startupScanDuration;
            iswScanDutyCycle                         = scanDutyCycleData->scanDutyCycle;  //! scanDutyCycle is really 1/value, so a 4 means 1/4

            iswLinkCmdLock.unlock();

            if ( theLogger->DEBUG_IswLink == 1 )
            {
                std::stringstream ss;
                ss << "  startupScanDuration = " << std::to_string(iswStartupScanDuration) << std::endl;
                ss << "  scanDutyCycle       = " << std::to_string(iswScanDutyCycle) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
    //    case SetIdleScanFrequency:
    //    {
    //        if ( theLogger->DEBUG_IswLink == 1 )
    //        {
    //            msgString = "Received SetIdleScanFrequency confirmation";
    //            theLogger->LogMessage(msgString, iswInterface->GetIndex());
    //        }
    //        break;
    //    }
    //    case GetIdleScanFrequency:
    //    {
    //        if ( theLogger->DEBUG_IswLink == 1 )
    //        {
    //            msgString = "Received GetIdleScanFrequency=";
    //            theLogger->LogMessage(msgString, iswInterface->GetIndex());
    //        }

    //        uint16_t idleScanFrequency = 0x00FF & data[1];
    //        idleScanFrequency = idleScanFrequency << 8;
    //        idleScanFrequency += data[0];
    //        iswLinkCmdLock.lock();
    //        iswIdleScanFrequency = idleScanFrequency;
    //        iswLinkCmdLock.unlock();

    //        if ( theLogger->DEBUG_IswLink == 1 )
    //        {
    //            std::stringstream ss;
    //            ss << "    idleScanFrequency = " << std::to_string(iswIdleScanFrequency) << std::endl;
    //            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    //        }

    //        break;
    //    }
        case SetBackgroundScanParameters:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetBackgroundScanParameters confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetBackgroundScanParameters:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetBackgroundScanParameters=";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswLinkCmdLock.lock();

            IswGetBackgroundScanParametersEvent * iswGetBackgroundScanParametersData = (IswGetBackgroundScanParametersEvent*)data;
            iswStableScanFrequency                                                   = iswGetBackgroundScanParametersData->stableScanFrequency;
            uint16_t reserved                                                        = iswGetBackgroundScanParametersData->reserved1;
            iswFluxScanFrequency                                                     = iswGetBackgroundScanParametersData->fluxScanFrequency;
            iswFluxScanDuration                                                      = iswGetBackgroundScanParametersData->fluxScanDuration;

            iswLinkCmdLock.unlock();

            if ( theLogger->DEBUG_IswLink == 1 )
            {
                std::stringstream ss;
                ss << "    stableScanFrequency = " << std::to_string(iswStableScanFrequency) << std::endl;
                ss << "    reserved            = " << std::to_string(reserved) << std::endl;
                ss << "    fluxScanFrequency   = " << std::to_string(iswFluxScanFrequency) << std::endl;
                ss << "    fluxScanDuration    = " << std::to_string(iswFluxScanDuration) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }

            break;
        }
        case SetMaxServiceInterval:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetMaxServiceInterval confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case ResetMaxServiceInterval:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetMaxServiceInterval=";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case SetAntennaConfiguration:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetAntennaConfiguration confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetAntennaConfiguration:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetAntennaConfiguration=";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswLinkCmdLock.lock();

            IswGetAntennaConfigurationEvent *getAntennaConfigurationData = (IswGetAntennaConfigurationEvent *)data;
            iswEnabled                                                   = getAntennaConfigurationData->enable;
            iswTriggerAlgorithm                                          = getAntennaConfigurationData->triggerAlgorithm;
            iswTriggerRssiThreshold                                      = getAntennaConfigurationData->triggerRssiThreshold;
            iswTriggerPhyRateThreshold                                   = getAntennaConfigurationData->triggerPhyRateThreshold;
            iswSwitchThresholdAdder                                      = getAntennaConfigurationData->switchThresholdAdder;

            iswLinkCmdLock.unlock();

            if ( theLogger->DEBUG_IswLink == 1 )
            {
                std::stringstream ss;
                ss << "  enable                  = " << std::to_string(iswEnabled) << std::endl;
                ss << "  triggerAlgorithm        = " << std::to_string(iswTriggerAlgorithm) << std::endl;
                ss << "  triggerRssiThreshold    = " << std::to_string(iswTriggerRssiThreshold) << std::endl;
                ss << "  triggerPhyRateThreshold = " << std::to_string(iswTriggerPhyRateThreshold) << std::endl;
                ss << "  switchThresholdAdder    = " << std::to_string(iswSwitchThresholdAdder) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());

            }


            break;
        }
        case SetAntennaId:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetAntennaId confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetAntennaId:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetAntennaId = ";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswLinkCmdLock.lock();

            IswGetAntennaIdEvent *getAntennaIdData = (IswGetAntennaIdEvent *)data;
            iswGetAntennaId                        = getAntennaIdData->enabled;

            iswLinkCmdLock.unlock();

            if ( theLogger->DEBUG_IswLink == 1 )
            {
                std::stringstream ss;
                ss << "  antennaId = " << std::to_string(iswGetAntennaId) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case SetBandgroupBias:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received SetBandgroupBias Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetBandgroupBias:
        {
            if ( theLogger->DEBUG_IswLink == 1 )
            {
                msgString = "Received GetBandgroupBias = ";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswLinkCmdLock.lock();

            IswGetBandGroupBiasEvent *getBandGroupBiasData = (IswGetBandGroupBiasEvent *)data;
            iswBgPrefer                                    = getBandGroupBiasData->bgPrefer;
            iswBgAvoid                                     = getBandGroupBiasData->bgAvoid;

            iswLinkCmdLock.unlock();

            if ( theLogger->DEBUG_IswLink == 1 )
            {
                std::stringstream ss;
                ss << "  BG Prefer = " << std::to_string(iswBgPrefer) << std::endl;
                ss << "  BG Avoid  = " << std::to_string(iswBgAvoid) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
    case SetPhyRatesCmd:
    {
        //qDebug() << "DeliverIswMessage case SetPhyRate";
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "Received SetPhyRates Confirmation";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        break;
    }
    case GetPhyRatesCmd:
    {
        //qDebug() << "got GetPhyRate data";
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "Received GetPhyRates = ";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        iswLinkCmdLock.lock();

//        for(uint8_t i = 0; i < sizeof(data); i++)
//        {
//            qDebug() << "bit in bitmap: " << data[i];
//        }


        IswGetPhyRatesEvent *getPhyRatesData           = (IswGetPhyRatesEvent *)data;
        phyRateBitmap                                  = getPhyRatesData->phyRateBitmap;

        //qDebug() << "got phy rates bitmap: " << phyRateBitmap;

        iswLinkCmdLock.unlock();

        if ( theLogger->DEBUG_IswLink == 1 )
        {
            std::stringstream ss;
            ss << "  Phy Rate Bitmap = " << std::to_string(phyRateBitmap) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
        break;
    }
    default:
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "Bad Link Event Type";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        break;
    }
    }
}


//!###################################################################
//! SetupGetScanDutyCycleCmd()
//! Inputs: iswLinkCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupGetScanDutyCycleCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetScanDutyCycleCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetScanDutyCycle;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);


    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetScanDutyCycleCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetScanDutyCycleCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetScanDutyCycleCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetScanDutyCycleCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetScanDutyCycleCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetScanDutyCycleCmd()
//! Inputs: setScanDutyCycleCmd - pointer to the command structure
//!         startupScanDuration - time to scan in seconds after startup
//!                               beforethe scan duty cycle is started
//!         scanDutyCycle - scan duty cycle will be (1/this number)
//!         persist - 0 = nothing, 1 = store values to NVRAM
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupSetScanDutyCycleCmd(IswSetScanDutyCycleCommand *setScanDutyCycleCmd,
                                       uint16_t startupScanDuration, uint8_t scanDutyCycle, uint8_t persist,
                                       void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetScanDutyCycleCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    setScanDutyCycleCmd->cmdType             = IswInterface::Link;
    setScanDutyCycleCmd->command             = SetScanDutyCycle;
    setScanDutyCycleCmd->cmdContext          = iswInterface->GetNextCmdCount();
    setScanDutyCycleCmd->reserved1           = 0x00;
    setScanDutyCycleCmd->startupScanDuration = startupScanDuration;
    setScanDutyCycleCmd->scanDutyCycle       = scanDutyCycle;
    setScanDutyCycleCmd->persist             = persist;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetScanDutyCycleCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType             = " << std::to_string(setScanDutyCycleCmd->cmdType) << std::endl;
        ss << "    command             = " << std::to_string(setScanDutyCycleCmd->command) << std::endl;
        ss << "    cmdContext          = " << std::to_string(setScanDutyCycleCmd->cmdContext) << std::endl;
        ss << "    reserved1           = " << std::to_string(setScanDutyCycleCmd->reserved1) << std::endl;
        ss << "    startupScanDuration = " << std::to_string(setScanDutyCycleCmd->startupScanDuration) << std::endl;
        ss << "    scanDutyCycle       = " << std::to_string(setScanDutyCycleCmd->scanDutyCycle) << std::endl;
        ss << "    persist             = " << std::to_string(setScanDutyCycleCmd->persist) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetScanDutyCycleCmd()
//! Inputs: startupScanDuration - time to scan in seconds after startup
//!                               beforethe scan duty cycle is started
//!         scanDutyCycle - scan duty cycle will be (1/this number)
//!         persist - 0=nothing, 1=store values to NVRAM
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetScanDutyCycleCmd(uint16_t startupScanDuration, uint8_t scanDutyCycle, uint8_t persist)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetScanDutyCycleCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetScanDutyCycleCmd(&iswCmd.iswSetScanDutyCycleCmd, startupScanDuration, scanDutyCycle, persist, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetScanDutyCycleCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetMaxServiceIntervalCmd()
//! Inputs: setMaxServiceIntervalCmd - pointer to the command structure
//!         maxServiceInterval - service interval in microseconds
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupSetMaxServiceIntervalCmd(IswSetMaxServiceIntervalCommand *setMaxServiceIntervalCmd, uint32_t maxServiceInterval, void *routingHdr)
{
    std::string msgString;

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetMaxServiceIntervalCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    setMaxServiceIntervalCmd->cmdType            = IswInterface::Link;
    setMaxServiceIntervalCmd->command            = SetMaxServiceInterval;
    setMaxServiceIntervalCmd->cmdContext         = iswInterface->GetNextCmdCount();
    setMaxServiceIntervalCmd->reserved1          = 0x00;
    setMaxServiceIntervalCmd->maxServiceInterval = maxServiceInterval;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetMaxServiceIntervalCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType            = " << std::to_string(setMaxServiceIntervalCmd->cmdType) << std::endl;
        ss << "    command            = " << std::to_string(setMaxServiceIntervalCmd->command) << std::endl;
        ss << "    cmdContext         = " << std::to_string(setMaxServiceIntervalCmd->cmdContext) << std::endl;
        ss << "    reserved1          = " << std::to_string(setMaxServiceIntervalCmd->reserved1) << std::endl;
        ss << "    maxServiceInterval = " << std::to_string(setMaxServiceIntervalCmd->maxServiceInterval) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetMaxServiceIntervalCmd()
//! Inputs: maxServiceInterval - service interval in microseconds
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetMaxServiceIntervalCmd(uint32_t maxServiceInterval)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetMaxServiceIntervalCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetMaxServiceIntervalCmd(&iswCmd.iswSetMaxServiceIntervalCmd, maxServiceInterval, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetMaxServiceIntervalCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    else
    {
        //! Set local value to return to GUI
        iswLinkCmdLock.lock();
        iswMaxServiceInterval = maxServiceInterval;
        iswLinkCmdLock.unlock();
    }

    return ( status );
}

//!###################################################################
//! SetupResetMaxServiceIntervalCmd()
//! Inputs: iswLinkCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupResetMaxServiceIntervalCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupResetMaxServiceIntervalCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = ResetMaxServiceInterval;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);


    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SetupSetBandgroupBiasCmd()
//! Inputs: iswSetBandgroupBiasCmd - pointer to the command structure
//!         bgPrefer - Bitmap of preferred bandgroups, numbering from bandgroup 0.
//!         bgAvoid - Bitmap of avoided bandgroups, numbering from bandgroup 0.
//!         persist - Set to '1' to persist the command in NVRAM.
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupSetBandgroupBiasCmd(IswSetBandgroupBiasCommand* iswSetBandgroupBiasCmd, uint8_t bgPrefer, uint8_t bgAvoid, uint8_t persist, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetBandgroupBiasCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetBandgroupBiasCmd->cmdType    = IswInterface::Link;
    iswSetBandgroupBiasCmd->command    = SetBandgroupBias;
    iswSetBandgroupBiasCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSetBandgroupBiasCmd->reserved1  = 0x00;
    iswSetBandgroupBiasCmd->bgPrefer   = bgPrefer;
    iswSetBandgroupBiasCmd->bgAvoid    = bgAvoid;
    iswSetBandgroupBiasCmd->persist    = persist;
    iswSetBandgroupBiasCmd->reserved2  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetBandgroupBiasCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSetBandgroupBiasCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSetBandgroupBiasCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSetBandgroupBiasCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSetBandgroupBiasCmd->reserved1) << std::endl;
        ss << "    bgPrefer   = " << std::to_string(iswSetBandgroupBiasCmd->bgPrefer) << std::endl;
        ss << "    bgAvoid    = " << std::to_string(iswSetBandgroupBiasCmd->bgAvoid) << std::endl;
        ss << "    persist    = " << std::to_string(iswSetBandgroupBiasCmd->persist) << std::endl;
        ss << "    reserved2  = " << std::to_string(iswSetBandgroupBiasCmd->reserved2) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetBandgroupBiasCmd()
//! Inputs: bgPrefer - Bitmap of preferred bandgroups, numbering from bandgroup 0.
//!         bgAvoid - Bitmap of avoided bandgroups, numbering from bandgroup 0.
//!         persist - Set to '1' to persist the command in NVRAM.
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetBandgroupBiasCmd(uint8_t bgPrefer, uint8_t bgAvoid, uint8_t persist)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetBandgroupBiasCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetBandgroupBiasCmd(&iswCmd.iswSetBandgroupBiasCmd, bgPrefer, bgAvoid, persist, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetBandgroupBiasCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetBandgroupBiasCmd()
//! Inputs: iswLinkCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupGetBandgroupBiasCmd(IswLinkCommand* iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetBandgroupBiasCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetBandgroupBias;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;

        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetBandgroupBiasCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetBandgroupBiasCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetBandgroupBiasCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetBandgroupBiasCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetBandgroupBiasCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SendResetMaxServiceIntervalCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendResetMaxServiceIntervalCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendResetMaxServiceIntervalCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupResetMaxServiceIntervalCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendResetMaxServiceIntervalCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetIdleScanFrequencyCmd()
//! Inputs: iswLinkCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupGetIdleScanFrequencyCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetIdleScanFrequencyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
//    iswLinkCmd->command = GetIdleScanFrequency;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetIdleScanFrequencyCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetIdleScanFrequencyCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetIdleScanFrequencyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetIdleScanFrequencyCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetIdleScanFrequencyCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetIdleScanFrequencyCmd()
//! Inputs: iswSetIdleScanFreqCmd - pointer to the command structure
//!         idleScanFrequency - set idle scan frequence in seconds
//!         persist - 0=do nothing, 1=store values to NVRAM
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupSetIdleScanFrequencyCmd(IswSetIdleScanFrequencyCommand *iswSetIdleScanFreqCmd, uint16_t idleScanFrequency, uint8_t persist, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetIdleScanFrequencyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetIdleScanFreqCmd->cmdType = IswInterface::Link;
//    iswSetIdleScanFreqCmd->command = SetIdleScanFrequency;
    iswSetIdleScanFreqCmd->cmdContext        = iswInterface->GetNextCmdCount();
    iswSetIdleScanFreqCmd->reserved1         = 0x00;
    iswSetIdleScanFreqCmd->idleScanFrequency = idleScanFrequency;
    iswSetIdleScanFreqCmd->persist           = persist;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetIdleScanFrequencyCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType           = " << std::to_string(iswSetIdleScanFreqCmd->cmdType) << std::endl;
        ss << "    command           = " << std::to_string(iswSetIdleScanFreqCmd->command) << std::endl;
        ss << "    cmdContext        = " << std::to_string(iswSetIdleScanFreqCmd->cmdContext) << std::endl;
        ss << "    reserved1         = " << std::to_string(iswSetIdleScanFreqCmd->reserved1) << std::endl;
        ss << "    idleScanFrequency = " << std::to_string(iswSetIdleScanFreqCmd->idleScanFrequency) << std::endl;
        ss << "    persist           = " << std::to_string(iswSetIdleScanFreqCmd->persist) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetIdleScanFrequencyCmd()
//! Inputs: idleScanFrequency - set idle scan frequence in seconds
//!         persist - 0=do nothing, 1=store values to NVRAM
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetIdleScanFrequencyCmd(uint16_t idleScanFrequency, uint8_t persist)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetIdleScanFrequencyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetIdleScanFrequencyCmd(&iswCmd.iswSetIdleScanFrequencyCmd, idleScanFrequency, persist, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetIdleScanFrequencyCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetBackgroundScanParametersCmd()
//! Inputs: iswLinkCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupGetBackgroundScanParametersCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetBackgroundScanParametersCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetBackgroundScanParameters;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);


    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetBackgroundScanParametersCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetBackgroundScanParametersCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetIdleScanFrequencyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetBackgroundScanParametersCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetIdleScanFrequencyCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetBackgroundScanParametersCmd()
//! Inputs: iswSetIdleScanFreqCmd - pointer to the command structure
//!         idleScanFrequency - set idle scan frequence in seconds
//!         persist - 0 = do nothing, 1 = store values to NVRAM
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswLink::SetupSetBackgroundScanParametersCmd(IswSetBackgroundScanParametersCommand *iswSetBackgroundScanParametersCmd, uint16_t stableScanFrequency, uint8_t persist, uint8_t version, uint16_t fluxScanFrequency, uint16_t fluxScanDuration, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetBackgroundScanParametersCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetBackgroundScanParametersCmd->cmdType             = IswInterface::Link;
    iswSetBackgroundScanParametersCmd->command             = SetBackgroundScanParameters;
    iswSetBackgroundScanParametersCmd->cmdContext          = iswInterface->GetNextCmdCount();
    iswSetBackgroundScanParametersCmd->reserved1           = 0x00;
    iswSetBackgroundScanParametersCmd->stableScanFrequency = stableScanFrequency;
    iswSetBackgroundScanParametersCmd->persist             = persist;
    iswSetBackgroundScanParametersCmd->version             = version;
    iswSetBackgroundScanParametersCmd->fluxScanFrequency   = fluxScanFrequency;
    iswSetBackgroundScanParametersCmd->fluxScanDuration    = fluxScanDuration;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetBackgroundScanParametersCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType             = " << std::to_string(iswSetBackgroundScanParametersCmd->cmdType) << std::endl;
        ss << "    command             = " << std::to_string(iswSetBackgroundScanParametersCmd->command) << std::endl;
        ss << "    cmdContext          = " << std::to_string(iswSetBackgroundScanParametersCmd->cmdContext) << std::endl;
        ss << "    reserved1           = " << std::to_string(iswSetBackgroundScanParametersCmd->reserved1) << std::endl;
        ss << "    stableScanFrequency = " << std::to_string(iswSetBackgroundScanParametersCmd->stableScanFrequency) << std::endl;
        ss << "    persist             = " << std::to_string(iswSetBackgroundScanParametersCmd->persist) << std::endl;
        ss << "    version             = " << std::to_string(iswSetBackgroundScanParametersCmd->version) << std::endl;
        ss << "    fluxScanFrequency   = " << std::to_string(iswSetBackgroundScanParametersCmd->fluxScanFrequency) << std::endl;
        ss << "    fluxScanDuration    = " << std::to_string(iswSetBackgroundScanParametersCmd->fluxScanDuration) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetBackgroundScanParametersCmd()
//! Inputs: idleScanFrequency - set idle scan frequence in seconds
//!         persist - 0 = do nothing, 1 = store values to NVRAM
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetBackgroundScanParametersCmd(uint16_t stableScanFrequency, uint8_t persist, uint8_t version, uint16_t fluxScanFrequency, uint16_t fluxScanDuration)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetBackgroundScanParametersCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetBackgroundScanParametersCmd(&iswCmd.iswSetBackgroundScanParametersCmd, stableScanFrequency, persist, version, fluxScanFrequency, fluxScanDuration, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetBackgroundScanParametersCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

void IswLink::SetupSetAntennaConfigurationCmd(IswSetAntennaConfigurationCommand *iswSetAntennaConfigurationCmd, uint8_t enable, uint8_t triggerAlgorithm, uint8_t triggerRssiThreshold, uint8_t triggerPhyRateThreshold, uint8_t switchThresholdAdder, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetAntennaConfigurationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetAntennaConfigurationCmd->cmdType                 = IswInterface::Link;
    iswSetAntennaConfigurationCmd->command                 = SetAntennaConfiguration;
    iswSetAntennaConfigurationCmd->cmdContext              = iswInterface->GetNextCmdCount();
    iswSetAntennaConfigurationCmd->reserved1               = 0x00;
    iswSetAntennaConfigurationCmd->enable                  = enable;
    iswSetAntennaConfigurationCmd->triggerAlgorithm        = triggerAlgorithm;
    iswSetAntennaConfigurationCmd->triggerRssiThreshold    = triggerRssiThreshold;
    iswSetAntennaConfigurationCmd->triggerPhyRateThreshold = triggerPhyRateThreshold;
    iswSetAntennaConfigurationCmd->switchThresholdAdder    = switchThresholdAdder;


    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetAntennaConfigurationCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType                 = " << std::to_string(iswSetAntennaConfigurationCmd->cmdType) << std::endl;
        ss << "    command                 = " << std::to_string(iswSetAntennaConfigurationCmd->command) << std::endl;
        ss << "    cmdContext              = " << std::to_string(iswSetAntennaConfigurationCmd->cmdContext) << std::endl;
        ss << "    reserved1               = " << std::to_string(iswSetAntennaConfigurationCmd->reserved1) << std::endl;
        ss << "    enable                  = " << std::to_string(iswSetAntennaConfigurationCmd->enable) << std::endl;
        ss << "    triggerAlgorithm        = " << std::to_string(iswSetAntennaConfigurationCmd->triggerAlgorithm) << std::endl;
        ss << "    triggerRssiThreshold    = " << std::to_string(iswSetAntennaConfigurationCmd->triggerRssiThreshold) << std::endl;
        ss << "    triggerPhyRateThreshold = " << std::to_string(iswSetAntennaConfigurationCmd->triggerPhyRateThreshold) << std::endl;
        ss << "    switchThresholdAdder    = " << std::to_string(iswSetAntennaConfigurationCmd->switchThresholdAdder) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetAntennaConfigurationCmd()
//! Inputs: idleScanFrequency - set idle scan frequence in seconds
//!         persist - 0=do nothing, 1=store values to NVRAM
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetAntennaConfigurationCmd(uint8_t enable, uint8_t triggerAlgorithm, uint8_t triggerRssiThreshold, uint8_t triggerPhyRateThreshold, uint8_t switchThresholdAdder)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetAntennaConfigurationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetAntennaConfigurationCmd(&iswCmd.iswSetAntennaConfigurationCmd, enable, triggerAlgorithm, triggerRssiThreshold, triggerPhyRateThreshold, switchThresholdAdder, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetAntennaConfigurationCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

void IswLink::SetupGetAntennaConfigurationCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetAntennaConfigurationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetAntennaConfiguration;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetAntennaConfigurationCmd()
//! Inputs:
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetAntennaConfigurationCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetAntennaConfigurationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetAntennaConfigurationCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetAntennaConfigurationCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

void IswLink::SetupSetAntennaIdCmd(IswSetAntennaIdCommand *iswSetAntennaIdCmd, uint8_t antennaId, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetAntennaIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetAntennaIdCmd->cmdType    = IswInterface::Link;
    iswSetAntennaIdCmd->command    = SetAntennaId;
    iswSetAntennaIdCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswSetAntennaIdCmd->reserved1  = 0x00;
    iswSetAntennaIdCmd->antennaId  = antennaId;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetAntennaIdCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSetAntennaIdCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSetAntennaIdCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSetAntennaIdCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSetAntennaIdCmd->reserved1) << std::endl;
        ss << "    antennaId  = " << std::to_string(iswSetAntennaIdCmd->antennaId) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetAntennaIdCmd()
//! Inputs:
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetAntennaIdCmd(uint8_t antennaId)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetAntennaIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetAntennaIdCmd(&iswCmd.iswSetAntennaIdCmd, antennaId, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetAntennaIdCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

void IswLink::SetupGetAntennaIdCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupSetAntennaIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetAntennaId;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLinkCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetAntennaIdCmd()
//! Inputs:
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetAntennaIdCmd()
{

    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetAntennaIdCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetAntennaIdCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetAntennaIdCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    return ( status );
}

void IswLink::SetupSetPhyRatesCmd(IswSetPhyRatesCommand *iswSetPhyRatesCmd, void *routingHdr, uint32_t phyRateBitmap, uint8_t persist)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupPhyRatesCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetPhyRatesCmd->cmdType     = IswInterface::Link;
    iswSetPhyRatesCmd->command     = SetPhyRatesCmd;
    iswSetPhyRatesCmd->cmdContext  = iswInterface->GetNextCmdCount();
    iswSetPhyRatesCmd->reserved1   = 0x00;
    iswSetPhyRatesCmd->phyRateBitmap = phyRateBitmap;
    iswSetPhyRatesCmd->persist     = persist;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswSetPhyRatesCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSetPhyRatesCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSetPhyRatesCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSetPhyRatesCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSetPhyRatesCmd->reserved1) << std::endl;
        ss << "    PHY Rate Bitmap  = " << std::to_string(iswSetPhyRatesCmd->phyRateBitmap) << std::endl;
        ss << "    Persist  = " << std::to_string(iswSetPhyRatesCmd->persist) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetPhyRatesCmd()
//! Inputs:
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendSetPhyRatesCmd(uint32_t phyRatesBitmap, uint8_t persist)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendSetPhyRatesCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetPhyRatesCmd(&iswCmd.iswSetPhyRatesCmd,(void *)&routingHdr, phyRatesBitmap, persist);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    //qDebug() << "completed sync send phy rate with status: " << status;

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendSetAntennaIdCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

void IswLink::SetupGetPhyRatesCmd(IswLinkCommand *iswLinkCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SetupGetPhyRatesCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLinkCmd->cmdType    = IswInterface::Link;
    iswLinkCmd->command    = GetPhyRatesCmd;
    iswLinkCmd->cmdContext = iswInterface->GetNextCmdCount();

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(IswLinkCommand);

    if ( theLogger->DEBUG_IswLink == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLinkCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLinkCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLinkCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLinkCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());

        qDebug() << ss;
    }
}

//!###################################################################
//! SendGetPhyRatesCmd()
//! Inputs:
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswLink::SendGetPhyRatesCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswLink == 1 )
    {
        msgString = "SendGetPhyRatesCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetPhyRatesCmd(&iswCmd.iswLinkCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

   // qDebug() << "completed sync send for get phy rate: " <<  status;

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswLink == 1 )
        {
            msgString = "SendGetPhyRatesCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    std::cout<<"debugging PHY Rates = " << phyRateBitmap << std::endl;
    return ( phyRateBitmap );
}

