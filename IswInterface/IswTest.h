//!###############################################
//! Filename: IswTest.h
//! Description: Class that provides an interface
//!              to ISW Metrics commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWTEST_H
#define ISWTEST_H

#include <mutex>
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswTest
{
public:
    //! IswTest Public Constructor
    IswTest(IswInterface *isw_interface, Logger *logger, DBLogger *dblogger);
    ~IswTest() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;
    //! DBLogger object pointer passed into constructor
    DBLogger *dbLogger = nullptr;

    struct uint24_t {
        uint32_t value:24;
    };

    struct iswTestCommand //!Command Enables or Disables the RAP indication
    {
        uint8_t  cmdType;        // Test
        uint8_t  command;        // Radio Activity Profile (RAP)
        uint8_t  contextId;      // Incrementing count
        uint8_t  reserved;       // Reserved for alignment with event structure
        uint8_t  rate;           // Frequency of indication in seconds. Set to 0 to disable indication.
        uint8_t  reserved2[3];      // Reserved for future specification: NOTE - spec says 24 bits
    }__attribute__((packed));

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    uint8_t GetIswSleepPercentage(void)  { return (iswSleepPercentage); }
    uint8_t GetIswScanPercentage(void)  { return (iswScanPercentage); }

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    ///! Send ISW commands to the firmware
    int SendRapCommand(uint8_t rate);


ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex iswTestCmdLock;

    //! Class variables
    bool rapEnabled = false;
    uint8_t iswSleepPercentage = 0;
    uint8_t iswScanPercentage = 0;

    //! ISW Test commands and events
    enum iswTestCmdEventTypes: uint16_t // could be uint8_t normally
    {
        RapCommand    = 0x10,
        //RapIndication = 0x01, // defined in spec
        RapIndication = 0x90,   // What is being returned after a non-zero set

    };

//    struct iswRapResponse //!Command Confirms the RAP Enable/Disable
//    {
//        uint8_t eventType   = 0x01; // Test
//        uint8_t event       = 0x10; // Radio Activity Profile (RAP)
//        uint8_t contextId   = 0;    // Incrementing count
//        uint8_t resultCode;         // Result of request
//    }__attribute__((packed));

    struct iswTestRapInd //! Command shows the sleep precentage over the specified period;
    {
        uint8_t sleepPercentage;        // Percentage of time sleeping from 0 to 99
        uint8_t scanPercentage;         // Percentage of time scanning from 0 to 99. Field is reserved and shows 0 for 30821 build.
        uint16_t Reserved;              // Reserved for future specification.
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupRapCommand(iswTestCommand *iswTestCmd, void *routingHdr, uint8_t rate);

    //! Method to parse incoming Rap Indication message information
    void ParseRapIndication(iswTestRapInd *iswRapInd);

};

#endif //! ISWTEST_H
