//!###############################################
//! Filename: IswSolNet.cpp
//! Description: Class that provides an interface
//!              to ISW SolNet commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswSolNet.h"
#include "IswInterface.h"
#include <sstream>
#include <iomanip>
#include <string.h>
#include <zlib.h>
#include <float.h>
#include <QDebug>

//!###############################################################
//! Constructor: IswSolNet()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswSolNet::IswSolNet(IswInterface *isw_interface, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface            = isw_interface;
        theLogger               = logger;
        throughputTestLogger    = throughputTestLoggerPass;
        throughputResultsLogger = throughputResultsLoggerPass;
    }
#ifndef TESTING  //!Don't run during unit tests
    //! Create the ack lists for registered peers
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList = new std::list<iswSolNetAckPacket *>;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList->clear();
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList = new std::list<iswRetransmitItem *>;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList->clear();
        }
    }
#endif
    //! for this device and for the peers
    RemoveAllServices(true);

#ifndef TESTING  //! Don't run during unit tests
    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        iswSolNetPeerServices[peerIndex].acksToSendList = new std::list<iswSolNetAckPacket *>;
        iswSolNetPeerServices[peerIndex].acksToSendList->clear();
        iswSolNetPeerServices[peerIndex].retransmitList = new std::list<iswRetransmitItem *>;
        iswSolNetPeerServices[peerIndex].retransmitList->clear();
    }
#endif
    //! Remove or clear all services from other peers
    RemoveAllPeerServices(true);

    //! Set the timeout for checking packets to be retransmitted
    iswSolNetRetransTimeout = 1000000; //! Linux timestamp 1000000 = 1 second
    iswSolNetNoOfRetrans    = 5;

#ifndef TESTING  //! Don't run during unit tests
    if(isw_interface->usbInterface->IsDeviceOpen())
    {
        int status = StartSolNetSendThread();
        if ( status < 0 )
        {
            std::cout << "SolNet Read event thread didn't start!" << std::endl;
            exit(-1);
        }
    }
#endif
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswSolNet::GetIndex()
{
    return (iswInterface->GetIndex());
}

//!###################################################################
//! DeliverSolNetPacket()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         endPoint  - ISW endpoints are 1 - 3 for SolNet data,
//!                     endpoint 0 for SolNet messages
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###################################################################
void IswSolNet::DeliverSolNetPacket(uint8_t peerIndex, uint8_t endPoint, uint8_t *data)
{
    //! Logger
    std::string msgString = "DeliverSolNetPacket";
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! SolNet Packet
    iswSolNetHeader *iswSolNetHdr = (iswSolNetHeader *)data;

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SolNet protocol version = " << std::to_string(iswSolNetHdr->version) << std::endl;
        ss << "SolNet protocol class   = " << std::to_string(iswSolNetHdr->protocolClass) << std::endl;
        ss << "    peerIndex = " << std::to_string(peerIndex) << std::endl;
        ss << "    endPoint  = " << std::to_string(endPoint) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }

    //! ISW SolNet Protocol Class
    switch ( iswSolNetHdr->protocolClass )
    {
        case DataPacket:
        {
            // If Verify Checksum Is Enabled
            if ( verifyChecksum )
            {
                uint16_t packetHeaderChecksum                 = iswSolNetHdr->iswSolNetDataPkt.headerChecksum;
                iswSolNetHdr->iswSolNetDataPkt.headerChecksum = 0;

                // The checksum includes the common header before the SolNet header
                uint16_t *pktPtr            = (uint16_t *)data;
                uint32_t count              = dataPacketHdrSize + 2;  // SolNet Data Header + Protocol Version + Protocol Class
                uint16_t calculatedChecksum = IswChecksum(count, pktPtr);

                if ( (calculatedChecksum > 0) &&
                    (packetHeaderChecksum == calculatedChecksum) )
                {
                    DeliverSolNetDataPacket(peerIndex, endPoint, &iswSolNetHdr->iswSolNetDataPkt);
                }
                else
                {

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "    packet header checksum bad = " << std::to_string(packetHeaderChecksum) << std::endl;
                        ss << "    calculated checksum        = " << std::to_string(calculatedChecksum) << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
            }
            else
            {
                DeliverSolNetDataPacket(peerIndex, endPoint, &iswSolNetHdr->iswSolNetDataPkt);
            }
            break;
        }
        case MessagePacket:
        {
            if ( verifyChecksum )
            {
                uint16_t packetHeaderChecksum                    = iswSolNetHdr->iswSolNetMessagePkt.headerChecksum;
                iswSolNetHdr->iswSolNetMessagePkt.headerChecksum = 0;

                // The checksum includes the common header before the SolNet header
                uint16_t *pktPtr = (uint16_t *)data;

                uint32_t count              = messagePacketHdrSize + 2;  // SolNet Message Header + Protocol Version + Protocol Class
                uint16_t calculatedChecksum = IswChecksum(count, pktPtr);

                if ( (calculatedChecksum > 0) &&
                    (packetHeaderChecksum == calculatedChecksum) )
                {
                    DeliverSolNetMessagePacket(peerIndex, endPoint, &iswSolNetHdr->iswSolNetMessagePkt);
                }
                else
                {

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "    packet header checksum bad = " << std::to_string(packetHeaderChecksum) << std::endl;
                        ss << "    calculated checksum        = " << std::to_string(calculatedChecksum) << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
            }
            else
            {
                DeliverSolNetMessagePacket(peerIndex, endPoint, &iswSolNetHdr->iswSolNetMessagePkt);
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                msgString = "Unknown SolNet protocol class = ";
                msgString += std::to_string(iswSolNetHdr->protocolClass);
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! VerifySolNetDataPayload()
//! Inputs: serviceEntry -
//!         iswSolNetDataPkt  -
//! Description: This method verifies the incoming ISW SolNet data payload from the ISW-Enabled HW and/or Emulator
//!              Current ICD Implementations: FWS-I RevA and LRF RevA
//!              Updated For LRF RevB
//!###################################################################
void IswSolNet::VerifySolNetDataPayload(IswSolNet::iswSolNetPeerServiceEntry *serviceEntry, iswSolNetDataPacket *iswSolNetDataPkt)
{
    //! Processes the SolNet queues so use a lock for reentrancy issues
    if (verifyDataPayloadLock.try_lock() == false)
    {
        //! Don't wait for lock - we don't have to verify
        //! every data packet
        return;
    }

    uint16_t offsetIntoPacket = 0;
    int dataPktLen            = iswSolNetDataPkt->dataLength;

    while (offsetIntoPacket < dataPktLen)
    {
        //! Common Data Tuple Header (Length/TupleId)
        iswSolNetDataTupleCommonHeader *tuplePtr = (iswSolNetDataTupleCommonHeader *)&iswSolNetDataPkt->dataPayload[offsetIntoPacket];
        bool tupleVerified                       = true;
        int tuplePtrSize                         = tuplePtr->length + commonDataHeaderSize;

        // Calculates the number of set child values.
        // 16 is the size of the commonDataHeader (4) and the required range value.
        // Each set optional child value is 8 bits.
        // We add back in 1 to the count to account for validating the range.
        int childTupleSetValuesCount = ((tuplePtrSize - 16) / 8) + 1;
        int offset                   = 0;

        //! Parse Data Payload Based On DescriptorId and TupleId
        switch(serviceEntry->serviceDesc.header.descriptorId)
        {
            case VideoService: //! Video Data Tuples
            {
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Verifying Video Service Payload..." << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str() << std::endl;

                switch(tuplePtr->tupleId)
                {
                    case VIDEO_FRAME_RAW_METADATA:
                    {
                        //! Video Frame RAW MetaData Tuple
                        iswSolNetVideoFrameRawMetadataTuple *videoFrameRawMetadata = (iswSolNetVideoFrameRawMetadataTuple *)tuplePtr;
                        videoFrameLength                                           = videoFrameRawMetadata->header.length;

                        if (videoFrameLength != (sizeof(iswSolNetVideoFrameRawMetadataTuple) - commonDataHeaderSize))
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Video Frame: Incorrect Header Length = " << std::to_string(videoFrameRawMetadata->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Video Frame: Correct Header Length = " << std::to_string(videoFrameRawMetadata->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        videoFrameDataTupleId = videoFrameRawMetadata->header.tupleId;
                        if (videoFrameDataTupleId != VIDEO_FRAME_RAW_METADATA)
                        {
                            tupleVerified = false;

                            std::stringstream ss2;
                            ss2 << "Video Frame: Incorrect Tuple Id = " << std::to_string(videoFrameRawMetadata->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Frame: Correct Tuple Id = " << std::to_string(videoFrameRawMetadata->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        frameNumber = videoFrameRawMetadata->frameNumber;
                        if (frameNumber < 0x00)
                        {
                            std::stringstream ss2;
                            ss2 << "Video Frame: Incorrect Frame Number = " << std::to_string(frameNumber) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Frame: Correct Frame Number = " << std::to_string(frameNumber) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        packetCount = videoFrameRawMetadata->packetCount;
                        if (packetCount < 0x00)
                        {
                            std::stringstream ss3;
                            ss3 << "Video Frame: Incorrect Packet Count = " << std::to_string(packetCount) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }
                        else
                        {
                            std::stringstream ss3;
                            ss3 << "Video Frame: Correct Packet Count = " << std::to_string(packetCount) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }

                        rowNumber = videoFrameRawMetadata->frameRowNumber;
                        if (rowNumber < 0x00)
                        {
                            std::stringstream ss4;
                            ss4 << "Video Frame: Incorrect Row Number = " << std::to_string(rowNumber) << std::endl;
                            if ( theLogger->DEBUG_IswSolNet == 1 )
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                        else
                        {
                            std::stringstream ss4;
                            ss4 << "Video Frame: Correct Row Number = " << std::to_string(rowNumber) << std::endl;
                            if ( theLogger->DEBUG_IswSolNet == 1 )
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }

                        colNumber = videoFrameRawMetadata->frameColumnNumber;
                        if (colNumber > 0x00)
                        {
                            std::stringstream ss4;
                            ss4 << "Video Frame: Incorrect Column Number = " << std::to_string(colNumber) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                        else
                        {
                            std::stringstream ss4;
                            ss4 << "Video Frame: Correct Column Number = " << std::to_string(colNumber) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                        break;
                    }
                    case VIDEO_DATA:
                    {
                        //! Video Data Tuple
                        iswSolNetVideoDataTuple *videoDataTuple = (iswSolNetVideoDataTuple *)tuplePtr;
                        videoDataLength                         = videoDataTuple->header.length;

                        if (videoDataLength < 1)
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Video Data: Incorrect Header Length = " << std::to_string(videoDataTuple->header.length) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Video Data: Correct Header Length = " << std::to_string(videoDataTuple->header.length) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        videoDataTupleId = videoDataTuple->header.tupleId;
                        if (videoDataTupleId != VIDEO_DATA)
                        {
                            tupleVerified = false;

                            std::stringstream ss2;
                            ss2 << "Video Data: Incorrect Tuple Id = " << std::to_string(videoDataTuple->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Data: Correct Tuple Id = " << std::to_string(videoDataTuple->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        // Detect Presence of Video Data
                        if((sizeof(videoDataTuple) - commonDataHeaderSize) > 0)
                        {
                            std::stringstream ss2;
                            ss2 << "Video Data Present..." << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Data Not Present..." << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        offsetIntoPacket += commonDataHeaderSize + tuplePtr->length;
                        break;
                    }
                    case VIDEO_FRAME_JPEG_METADATA:
                    {
                        //! Video Frame JPEG Medata Tuple
                        iswSolNetVideoFrameJpegMetadataTuple *jpegMetadataTuple = (iswSolNetVideoFrameJpegMetadataTuple *)tuplePtr;

                        if(VIDEO_FRAME_JPEG_METADATA_TUPLE_LENGTH != jpegMetadataTuple->header.length)
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Video Frame JPEG Metadata Tuple: Invalid Header Length: " << std::to_string(jpegMetadataTuple->header.length) << std::endl;

                            if(theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Video Frame JPEG Metadata Tuple: Valid Header Length: " << std::to_string(jpegMetadataTuple->header.length) << std::endl;
                            if(theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        if(VIDEO_FRAME_JPEG_METADATA_TUPLE_JPEG_PROPERTIES_MASK & jpegMetadataTuple->jpegProperties)
                        {
                            tupleVerified = false;
                            std::stringstream ss2;
                            ss2 << "Video Frame JPEG Metadata Tuple: Invalid JPEG Properties : " << std::to_string(jpegMetadataTuple->jpegProperties) << std::endl;
                            if(theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Frame JPEG Metadata Tuple: Valid JPEG Properties : " << std::to_string(jpegMetadataTuple->jpegProperties) << std::endl;
                            if(theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        //offsetIntoPacket += commonDataHeaderSize + tuplePtr->length;
                        break;
                    }
                    case VIDEO_PROPERTIES:
                    {
                        //! Video Properties Data Tuple
                        iswSolNetVideoPropertiesDataTuple *videoProperties = (iswSolNetVideoPropertiesDataTuple *)tuplePtr;
                        videoPropertiesLength                              = videoProperties->header.length;

                        if (videoPropertiesLength < 0x0000)
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Video Properties: Incorrect Header Length = " << std::to_string(videoProperties->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Video Properties: Correct Header Length = " << std::to_string(videoProperties->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        videoPropertiesTupleId = videoProperties->header.tupleId;
                        if (videoPropertiesTupleId != VIDEO_PROPERTIES)
                        {
                            tupleVerified = false;

                            std::stringstream ss2;
                            ss2 << "Video Properties: Incorrect Tuple Id = " << std::to_string(videoProperties->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Video Properties: Correct Tuple Id = " << std::to_string(videoProperties->header.tupleId) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        //! Iterate over nested property tuples
                        uint8_t *bytePtr = (uint8_t *)tuplePtr;
                        iswSolNetDataTupleCommonHeader *nestedTuplePtr = nullptr;
                        int nestedTupleOffset = commonDataHeaderSize;

                        while(nestedTupleOffset < videoProperties->header.length)
                        {
                            nestedTuplePtr = (iswSolNetDataTupleCommonHeader *)(bytePtr + nestedTupleOffset);
                            switch(nestedTuplePtr->tupleId)
                            {
                                case VIDEO_AGC:
                                {
                                    //! Video AGC Tuple
                                    iswSolNetVideoAGCTuple *agcTuple = (iswSolNetVideoAGCTuple *)nestedTuplePtr;

                                    std::stringstream ss1;
                                    if(agcTuple->header.length != VIDEO_AGC_TUPLE_LENGTH)
                                    {
                                        tupleVerified = false;
                                        ss1 << "Video AGC Tuple: Invalid header length: " << std::to_string(agcTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        ss1 << "Video AGC Tuple: Valid header length: " << std::to_string(agcTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    break;
                                }
                                case VIDEO_POLARITY:
                                {
                                    //! Video IR Polarity Tuple
                                    iswSolNetVideoPolarityTuple *polarityTuple = (iswSolNetVideoPolarityTuple *)nestedTuplePtr;

                                    std::stringstream ss1;
                                    if(polarityTuple->header.length != VIDEO_POLARITY_TUPLE_LENGTH)
                                    {
                                        tupleVerified = false;
                                        ss1 << "Video IR Polarity Tuple: Invalid header length: " << std::to_string(polarityTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        ss1 << "Video IR Polarity Tuple: Valid header length: " << std::to_string(polarityTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    break;
                                }
                                case VIDEO_RESOLUTION:
                                {
                                    //! Video Resolution Tuple
                                    iswSolNetVideoResolutionTuple *resolutionTuple = (iswSolNetVideoResolutionTuple *)nestedTuplePtr;

                                    std::stringstream ss1;
                                    if(resolutionTuple->header.length != VIDEO_POLARITY_TUPLE_LENGTH)
                                    {
                                        tupleVerified = false;
                                        ss1 << "Video Resolution Tuple: Invalid header length: " << std::to_string(resolutionTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        ss1 << "Video Resolution Tuple: Valid header length: " << std::to_string(resolutionTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    break;
                                }
                                case VIDEO_WAVEBAND:
                                {
                                    //! Video Waveband Tuple
                                    iswSolNetVideoWavebandTuple *wavebandTuple = (iswSolNetVideoWavebandTuple *)nestedTuplePtr;

                                    std::stringstream ss1;
                                    if(wavebandTuple->header.length != VIDEO_POLARITY_TUPLE_LENGTH)
                                    {
                                        tupleVerified = false;
                                        ss1 << "Video Waveband Tuple: Invalid header length: " << std::to_string(wavebandTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        ss1 << "Video Waveband Tuple: Valid header length: " << std::to_string(wavebandTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    break;
                                }
                                case VIDEO_ZOOM:
                                {
                                    //! Video Zoom Tuple
                                    iswSolNetVideoZoomTuple *zoomTuple = (iswSolNetVideoZoomTuple *)nestedTuplePtr;

                                    std::stringstream ss1;
                                    if(zoomTuple->header.length != VIDEO_POLARITY_TUPLE_LENGTH)
                                    {
                                        tupleVerified = false;
                                        ss1 << "Video Zoom Tuple: Invalid header length: " << std::to_string(zoomTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        ss1 << "Video Zoom Tuple: Valid header length: " << std::to_string(zoomTuple->header.length) << std::endl;
                                        if(theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }

                                    break;
                                }
                                case VIDEO_CALIBRATION:
                                {
                                    //! Video Image Calibration Data Tuple
                                    iswSolNetVideoImageCalibrationDataTuple *videoImageCalibrationDataTuple = (iswSolNetVideoImageCalibrationDataTuple *)nestedTuplePtr;
                                    videoImageCalibrationLength                                             = videoImageCalibrationDataTuple->header.length;

                                    if (videoImageCalibrationLength != (sizeof(iswSolNetVideoImageCalibrationDataTuple) - commonDataHeaderSize))
                                    {
                                        tupleVerified = false;

                                        std::stringstream ss1;
                                        ss1 << "Video Image Calibration: Incorrect Header Length = " << std::to_string(videoImageCalibrationDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss1;
                                        ss1 << "Video Image Calibration: Correct Header Length = " << std::to_string(videoImageCalibrationDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }

                                    videoImageCalibrationTupleId = videoImageCalibrationDataTuple->header.tupleId;
                                    if (videoImageCalibrationTupleId != VIDEO_CALIBRATION)
                                    {
                                        tupleVerified = false;

                                        std::stringstream ss2;
                                        ss2 << "Video Image Calibration: Incorrect Tuple Id = " << std::to_string(videoImageCalibrationDataTuple->header.tupleId) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss2.str(), GetIndex());
                                        }
                                        std::cout << ss2.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss2;
                                        ss2 << "Video Image Calibration: Correct Tuple Id = " << std::to_string(videoImageCalibrationDataTuple->header.tupleId) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss2.str(), GetIndex());
                                        }
                                        std::cout << ss2.str();
                                    }

                                    videoImageCalibration = videoImageCalibrationDataTuple->imageCalibration;
                                    if (videoImageCalibration > 0x0003)
                                    {
                                        std::stringstream ss6;
                                        ss6 << "Video Image Calibration: Incorrect Image Calibration = " << std::to_string(videoImageCalibration) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss6.str(), GetIndex());
                                        }
                                        std::cout << ss6.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss6;
                                        ss6 << "Video Image Calibration: Correct Image Calibration = " << std::to_string(videoImageCalibration) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss6.str(), GetIndex());
                                        }
                                        std::cout << ss6.str();
                                    }

                                    break;
                                }
                                default:
                                {
                                    std::stringstream ss1;
                                    ss1 << "Video: Unknown Nested Tuple Id" << std::endl;
                                    ss1 << "Tuple Id     = " << std::to_string(tuplePtr->tupleId) << std::endl;
                                    ss1 << "Tuple Length = " << std::to_string(tuplePtr->length) << std::endl;

                                    if (theLogger->DEBUG_IswSolNet == 1)
                                    {
                                        theLogger->DebugMessage(ss1.str(), GetIndex());
                                    }
                                    std::cout << ss1.str();
                                    break;
                                }
                            }
                            nestedTupleOffset += commonDataHeaderSize + nestedTuplePtr->length;
                        }


                        // Iterate through set optional child tuple values
                        uint8_t numChildTuplesInc = 0;

                        /*
                        while (numChildTuplesInc < 2) // childTupleSetValuesCount
                        {
                            int nextChildLocInPayload = offsetIntoPacket + (sizeof(iswSolNetVideoPropertiesDataTuple)-1) + 8*numChildTuplesInc;

                            iswSolNetDataTupleCommonHeader *commonDataHdr = (iswSolNetDataTupleCommonHeader *)&iswSolNetDataPkt->dataPayload[nextChildLocInPayload];

                            switch(commonDataHdr->tupleId)
                            {
                                case VIDEO_POLARITY:
                                {
                                    //! Video Polarity Data Tuple
                                    iswSolNetVideoPolarityDataTuple *videoPolarityDataTuple = (iswSolNetVideoPolarityDataTuple *)commonDataHdr;
                                    videoPolarityLength                                     = videoPolarityDataTuple->header.length;

                                    if (videoPolarityLength != (sizeof(iswSolNetVideoPolarityDataTuple) - commonDataHeaderSize))
                                    {
                                        tupleVerified = false;

                                        std::stringstream ss1;
                                        ss1 << "Video Polarity: Incorrect Header Length = " << std::to_string(videoPolarityDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss1;
                                        ss1 << "Video Polarity: Correct Header Length = " << std::to_string(videoPolarityDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }

                                    videoPolarity = videoPolarityDataTuple->videoPolarity;
                                    if (videoPolarity > 0x03)
                                    {
                                        std::stringstream ss5;
                                        ss5 << "Video Polarity: Incorrect Video Polarity = " << std::to_string(videoPolarity) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss5.str(), GetIndex());
                                        }
                                        std::cout << ss5.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss5;
                                        ss5 << "Video Polarity: Correct Video Polarity = " << std::to_string(videoPolarity) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss5.str(), GetIndex());
                                        }
                                        std::cout << ss5.str();
                                    }

                                    //! Offset to another data tuple
                                    offset += commonDataHdr->length + videoPolarityDataTuple->header.length;
                                    break;
                                }
                                case VIDEO_CALIBRATION:
                                {
                                    //! Video Image Calibration Data Tuple
                                    iswSolNetVideoImageCalibrationDataTuple *videoImageCalibrationDataTuple = (iswSolNetVideoImageCalibrationDataTuple *)commonDataHdr;
                                    videoImageCalibrationLength                                             = videoImageCalibrationDataTuple->header.length;

                                    if (videoImageCalibrationLength != (sizeof(iswSolNetVideoImageCalibrationDataTuple) - commonDataHeaderSize))
                                    {
                                        tupleVerified = false;

                                        std::stringstream ss1;
                                        ss1 << "Video Image Calibration: Incorrect Header Length = " << std::to_string(videoImageCalibrationDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss1;
                                        ss1 << "Video Image Calibration: Correct Header Length = " << std::to_string(videoImageCalibrationDataTuple->header.length) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss1.str(), GetIndex());
                                        }
                                        std::cout << ss1.str();
                                    }

                                    videoImageCalibrationTupleId = videoImageCalibrationDataTuple->header.tupleId;
                                    if (videoImageCalibrationTupleId != VIDEO_CALIBRATION)
                                    {
                                        tupleVerified = false;

                                        std::stringstream ss2;
                                        ss2 << "Video Image Calibration: Incorrect Tuple Id = " << std::to_string(videoImageCalibrationDataTuple->header.tupleId) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss2.str(), GetIndex());
                                        }
                                        std::cout << ss2.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss2;
                                        ss2 << "Video Image Calibration: Correct Tuple Id = " << std::to_string(videoImageCalibrationDataTuple->header.tupleId) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss2.str(), GetIndex());
                                        }
                                        std::cout << ss2.str();
                                    }

                                    videoImageCalibration = videoImageCalibrationDataTuple->imageCalibration;
                                    if (videoImageCalibration > 0x0003)
                                    {
                                        std::stringstream ss6;
                                        ss6 << "Video Image Calibration: Incorrect Image Calibration = " << std::to_string(videoImageCalibration) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss6.str(), GetIndex());
                                        }
                                        std::cout << ss6.str();
                                    }
                                    else
                                    {
                                        std::stringstream ss6;
                                        ss6 << "Video Image Calibration: Correct Image Calibration = " << std::to_string(videoImageCalibration) << std::endl;
                                        if (theLogger->DEBUG_IswSolNet == 1)
                                        {
                                            theLogger->DebugMessage(ss6.str(), GetIndex());
                                        }
                                        std::cout << ss6.str();
                                    }

                                    offset += commonDataHdr->length + videoImageCalibrationDataTuple->header.length;
                                    break;
                                }
                                default:
                                {
                                    std::stringstream ss1;
                                    ss1 << "Video Properties: Unknown Child Tuple Id " << std::endl;
                                    ss1 << "Tuple Id     = " << std::to_string(commonDataHdr->tupleId) << std::endl;
                                    ss1 << "Tuple Length = " << std::to_string(commonDataHdr->length) << std::endl;

                                    if (theLogger->DEBUG_IswSolNet == 1)
                                    {
                                        theLogger->DebugMessage(ss1.str(), GetIndex());
                                    }
                                    std::cout << ss1.str();

                                    tupleVerified = false;
                                }
                            }

                            if (tupleVerified)
                            {

                            }
                            else
                            {
                                tupleVerified = false;
                                break;
                            }

                            numChildTuplesInc++;
                        } //! end while
                        */
                        break;
                    }                    
                    default:
                    {
                        std::stringstream ss1;
                        ss1 << "Video: Unknown Tuple Id" << std::endl;
                        ss1 << "Tuple Id     = " << std::to_string(tuplePtr->tupleId) << std::endl;
                        ss1 << "Tuple Length = " << std::to_string(tuplePtr->length) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss1.str(), GetIndex());
                        }
                        std::cout << ss1.str();
                        break;
                    }
                }

                if (tupleVerified == true)
                {
                    std::stringstream ss_end;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        ss_end << "Verified Video Data Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            case RTA_SERVICE: //! RTA Data Tuples
            {
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Verifying RTA Service Payload..." << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str();

                //! RTA Status Data Tuple
                iswSolNetRtaStatusDataTuple *rtaStatusData = (iswSolNetRtaStatusDataTuple *)tuplePtr;
                rtaStatusDataLength                        = rtaStatusData->header.length;

                if (rtaStatusDataLength != (sizeof(iswSolNetRtaStatusDataTuple) - commonDataHeaderSize))
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "RTA Status: Incorrect Header Length = " << std::to_string(rtaStatusData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "RTA Status: Correct Header Length = " << std::to_string(rtaStatusData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                rtaStatusDataTupleId = rtaStatusData->header.tupleId;
                if (rtaStatusDataTupleId != RTA_STATUS)
                {
                    tupleVerified = false;

                    std::stringstream ss2;
                    ss2 << "RTA Status: Incorrect Tuple Id = " << std::to_string(rtaStatusData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "RTA Status: Correct Tuple Id = " << std::to_string(rtaStatusData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                rtaEnabled = rtaStatusData->rtaEnabled;
                if (rtaEnabled > 0x01)
                {
                    std::stringstream ss7;
                    ss7 << "RTA Status: Incorrect RTA Enabled = " << std::to_string(rtaEnabled) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss7.str(), GetIndex());
                    }
                    std::cout << ss7.str();
                }
                else
                {
                    std::stringstream ss7;
                    ss7 << "RTA Status: Correct RTA Enabled = " << std::to_string(rtaEnabled) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss7.str(), GetIndex());
                    }
                    std::cout << ss7.str();
                }

                rtaMode = rtaStatusData->rtaMode;
                if (rtaMode > 0x04)
                {
                    std::stringstream ss8;
                    ss8 << "RTA Status: Incorrect RTA Mode = " << std::to_string(rtaMode) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss8.str(), GetIndex());
                    }
                    std::cout << ss8.str();
                }
                else
                {
                    std::stringstream ss8;
                    ss8 << "RTA Status: Correct RTA Mode = " << std::to_string(rtaMode) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss8.str(), GetIndex());
                    }
                    std::cout << ss8.str();
                }

                rtaPolarity = rtaStatusData->rtaPolarity;
                if (rtaPolarity > 0x01)
                {
                    std::stringstream ss9;
                    ss9 << "RTA Status: Incorrect RTA Polarity = " << std::to_string(rtaPolarity) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss9.str(), GetIndex());
                    }
                    std::cout << ss9.str();
                }
                else
                {
                    std::stringstream ss9;
                    ss9 << "RTA Status: Correct RTA Polarity = " << std::to_string(rtaPolarity) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss9.str(), GetIndex());
                    }
                    std::cout << ss9.str();
                }

                rtaZoom = rtaStatusData->rtaZoom;
                if (rtaZoom > 0x01)
                {
                    std::stringstream ss10;
                    ss10 << "RTA Status: Incorrect RTA Zoom = " << std::to_string(rtaZoom) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss10.str(), GetIndex());
                    }
                    std::cout << ss10.str();
                }
                else
                {
                    std::stringstream ss10;
                    ss10 << "RTA Status: Correct RTA Zoom = " << std::to_string(rtaZoom) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss10.str(), GetIndex());
                    }
                    std::cout << ss10.str();
                }

                rtaBubble = rtaStatusData->rtaBubble;
                if (rtaBubble > 0x01)
                {
                    std::stringstream ss11;
                    ss11 << "RTA Status: Incorrect RTA Bubble = " << std::to_string(rtaBubble) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss11.str(), GetIndex());
                    }
                    std::cout << ss11.str();
                }
                else
                {
                    std::stringstream ss11;
                    ss11 << "RTA Status: Correct RTA Bubble = " << std::to_string(rtaBubble) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss11.str(), GetIndex());
                    }
                    std::cout << ss11.str();
                }

                rtaManualAlignment = rtaStatusData->rtaManualAlignment;
                if (rtaManualAlignment > 0x01)
                {
                    std::stringstream ss12;
                    ss12 << "RTA Status: Incorrect RTA Manual Alignment = " << std::to_string(rtaManualAlignment) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss12.str(), GetIndex());
                    }
                    std::cout << ss12.str();
                }
                else
                {
                    std::stringstream ss12;
                    ss12 << "RTA Status: Correct RTA Manual Alignment = " << std::to_string(rtaManualAlignment) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss12.str(), GetIndex());
                    }
                    std::cout << ss12.str();
                }

                reticleInVideo = rtaStatusData->reticleInVideo;
                if (reticleInVideo > 0x01)
                {
                    std::stringstream ss13;
                    ss13 << "RTA Status: Incorrect RTA Reticle In Video = " << std::to_string(reticleInVideo) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss13.str(), GetIndex());
                    }
                    std::cout << ss13.str();
                }
                else
                {
                    std::stringstream ss13;
                    ss13 << "RTA Status: Correct RTA Reticle In Video = " << std::to_string(reticleInVideo) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss13.str(), GetIndex());
                    }
                    std::cout << ss13.str();
                }

                //! RTA Reticle Data Tuple
                iswSolNetRtaReticleDataTuple *rtaReticleData = (iswSolNetRtaReticleDataTuple *)tuplePtr;
                rtaReticleDataLength                         = rtaReticleData->header.length;

                if (rtaReticleDataLength != (sizeof(iswSolNetRtaReticleDataTuple) - commonDataHeaderSize))
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "RTA Reticle: Incorrect Header Length = " << std::to_string(rtaReticleData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "RTA Reticle: Correct Header Length = " << std::to_string(rtaReticleData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                rtaReticleDataTupleId = rtaReticleData->header.tupleId;
                if (rtaReticleDataTupleId != RTA_RETICLE)
                {
                    tupleVerified = false;

                    std::stringstream ss2;
                    ss2 << "RTA Reticle: Incorrect Tuple Id = " << std::to_string(rtaReticleData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "RTA Reticle: Correct Tuple Id = " << std::to_string(rtaReticleData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                rtaReticleType = rtaReticleData->rtaReticleType;
                if (rtaReticleType > 0x01)
                {
                    std::stringstream ss14;
                    ss14 << "RTA Reticle: Incorrect RTA Reticle Type = " << std::to_string(rtaReticleType) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss14.str(), GetIndex());
                    }
                    std::cout << ss14.str();
                }
                else
                {
                    std::stringstream ss14;
                    ss14 << "RTA Reticle: Correct RTA Reticle Type = " << std::to_string(rtaReticleType) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss14.str(), GetIndex());
                    }
                    std::cout << ss14.str();
                }

                rtaReticleColorRed = rtaReticleData->rtaReticleColorRed;
                if(rtaReticleColorRed != 0xFF)
                {
                    std::stringstream ss15;
                    ss15 << "RTA Reticle: Incorrect RTA Reticle Color Red = " << std::to_string(rtaReticleColorRed) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss15.str(), GetIndex());
                    }
                    std::cout << ss15.str();
                }
                else
                {
                    std::stringstream ss15;
                    ss15 << "RTA Reticle: Correct RTA Reticle Color Red = " << std::to_string(rtaReticleColorRed) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss15.str(), GetIndex());
                    }
                    std::cout << ss15.str();
                }

                rtaReticleColorGreen = rtaReticleData->rtaReticleColorGreen;
                if(rtaReticleColorGreen != 0x00)
                {
                    std::stringstream ss16;
                    ss16 << "RTA Reticle: Incorrect RTA Reticle Color Green = " << std::to_string(rtaReticleColorGreen) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss16.str(), GetIndex());
                    }
                    std::cout << ss16.str();
                }
                else
                {
                    std::stringstream ss16;
                    ss16 << "RTA Reticle: Correct RTA Reticle Color Green = " << std::to_string(rtaReticleColorGreen) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss16.str(), GetIndex());
                    }
                    std::cout << ss16.str();
                }

                rtaReticleColorBlue = rtaReticleData->rtaReticleColorBlue;
                if(rtaReticleColorBlue != 0x00)
                {
                    std::stringstream ss17;
                    ss17 << "RTA Reticle: Incorrect RTA Reticle Color Blue = " << std::to_string(rtaReticleColorBlue) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss17.str(), GetIndex());
                    }
                    std::cout << ss17.str();
                }
                else
                {
                    std::stringstream ss17;
                    ss17 << "RTA Reticle: Correct RTA Reticle Color Blue = " << std::to_string(rtaReticleColorBlue) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss17.str(), GetIndex());
                    }
                    std::cout << ss17.str();
                }

                // Nothing to verify
                rtaReticleXOffset = rtaReticleData->rtaReticleXOffset;

                // Nothing to verify
                rtaReticleYOffset = rtaReticleData->rtaReticleYOffset;

                std::stringstream ss18;
                ss18 << "RTA Reticle: Reticle X Offset = " << std::to_string(rtaReticleXOffset) << std::endl;
                ss18 << "RTA Reticle: Reticle Y Offset = " << std::to_string(rtaReticleYOffset) << std::endl;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss18.str(), GetIndex());
                }
                std::cout << ss18.str();

                //! IMU Data Tuple
                iswSolNetRtaImuDataTuple *rtaImuData = (iswSolNetRtaImuDataTuple *)tuplePtr;
                rtaImuDataLength                     = rtaImuData->header.length;

                if (rtaImuDataLength != (sizeof(iswSolNetRtaImuDataTuple) - commonDataHeaderSize))
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "RTA IMU Data: Incorrect Header Length = " << std::to_string(rtaImuData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "RTA IMU Data: Correct Header Length = " << std::to_string(rtaImuData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                rtaImuDataTupleId = rtaImuData->header.tupleId;
                if (rtaImuDataTupleId != RTA_IMU_DATA)
                {
                    tupleVerified = false;

                    std::stringstream ss2;
                    ss2 << "RTA IMU Data: Incorrect Tuple Id = " << std::to_string(rtaImuData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "RTA IMU Data: Correct Tuple Id = " << std::to_string(rtaImuData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                // Nothing to verify
                quaternionReal = rtaImuData->quaternionReal;

                // Nothing to verify
                quaternionI = rtaImuData->quaternionI;

                // Nothing to verify
                quaternionJ = rtaImuData->quaternionJ;

                // Nothing to verify
                quaternionK = rtaImuData->quaternionK;

                if (tupleVerified == true)
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "RTA IMU Data: Quaternion Real = " << quaternionReal << std::endl;
                        ss_end << "RTA IMU Data: Quaternion I    = " << quaternionI << std::endl;
                        ss_end << "RTA IMU Data: Quaternion J    = " << quaternionJ << std::endl;
                        ss_end << "RTA IMU Data: Quaternion K    = " << quaternionK << std::endl;
                        ss_end << "Verified RTA Data Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            case MotionSense: //! Motion Sense Data Tuples
            {
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Verifying Motion Sense Data Payload..." << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str();

                //! Sample Data Tuple
                iswSolNetSampleDataTuple *sampleData = (iswSolNetSampleDataTuple *)tuplePtr;
                sampleDataLength                     = sampleData->header.length;

                if (sampleDataLength < sizeof(iswSolNetSampleDataTuple))
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "Sample Data: Incorrect Header Length = " << std::to_string(sampleData->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "Sample Data: Correct Header Length = " << std::to_string(sampleData->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                sampleDataTupleId = sampleData->header.tupleId;
                if (sampleDataTupleId != MOTION_SENSE_SAMPLE)
                {
                    tupleVerified = false;

                    std::stringstream ss2;
                    ss2 << "Sample: Incorrect Tuple Id = " << std::to_string(sampleData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "Sample: Correct Tuple Id = " << std::to_string(sampleData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                // Nothing to verify
                sampleTimestamp = sampleData->sampleTimestamp;

                // Iterate through set optional child tuple values
                uint8_t numChildTuplesInc = 0;

                while (numChildTuplesInc < 3) // childTupleSetValuesCount
                {
                    int nextChildLocInPayload                     = offsetIntoPacket
                            + (sizeof(iswSolNetSampleDataTuple)-1) + 16*numChildTuplesInc;

                    iswSolNetDataTupleCommonHeader *commonDataHdr =
                            (iswSolNetDataTupleCommonHeader *)&iswSolNetDataPkt->dataPayload[nextChildLocInPayload];

                    switch(commonDataHdr->tupleId)
                    {
                        case MOTION_SENSE_ACCEL:
                        {
                            //! Motion Sense Accel Data Tuple
                            iswSolNetAccelerometerDataTuple *accelerometerData = (iswSolNetAccelerometerDataTuple *)commonDataHdr;
                            accelerationDataLength                             = accelerometerData->header.length;

                            if (accelerationDataLength < sizeof(iswSolNetAccelerometerDataTuple))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "Accelerometer Data: Incorrect Header Length = " << std::to_string(accelerometerData->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "Accelerometer Data: Correct Header Length = " << std::to_string(accelerometerData->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            accelerationDataTupleId = accelerometerData->header.tupleId;
                            if (accelerationDataTupleId != MOTION_SENSE_ACCEL)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "Accelerometer Data: Incorrect Tuple Id = " << std::to_string(accelerometerData->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "Accelerometer Data: Correct Tuple Id = " << std::to_string(accelerometerData->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            for(int i = 0; i < 3; i++)
                            {
                                std::stringstream ss2;
                                ss2 << "Accelerometer Data: Acceleration Data[" << std::to_string(i) << "] = " << std::to_string(accelerometerData->accelerationData[i]) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHdr->length + accelerometerData->header.length;
                            break;
                        }
                        case MOTION_SENSE_GYRO:
                        {
                            //! Motion Sense Gyro Data Tuple
                            iswSolNetGyroDataTuple *gyroDataTuple = (iswSolNetGyroDataTuple *)commonDataHdr;
                            gyroDataLength                        = gyroDataTuple->header.length;

                            if (gyroDataLength < sizeof(iswSolNetGyroDataTuple))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "Gyroscope Data: Incorrect Header Length = " << std::to_string(gyroDataTuple->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "Gyroscope Data: Correct Header Length = " << std::to_string(gyroDataTuple->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            gyroDataTupleId = gyroDataTuple->header.tupleId;
                            if (gyroDataTupleId != MOTION_SENSE_GYRO)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "Gyroscope Data: Incorrect Tuple Id = " << std::to_string(gyroDataTuple->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "Gyroscope Data: Correct Tuple Id = " << std::to_string(gyroDataTuple->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            for(int i = 0; i < 3; i++)
                            {
                                std::stringstream ss2;
                                ss2 << "Gyroscope Data: Gyro Data[" << std::to_string(i) << "] = " << std::to_string(gyroDataTuple->gyroData[i]) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHdr->length + gyroDataTuple->header.length;
                            break;
                        }
                        case MOTION_SENSE_MAG:
                        {
                            //! Motion Sense Mag Data Tuple
                            iswSolNetMagnetometerDataTuple *magDataTuple = (iswSolNetMagnetometerDataTuple *)commonDataHdr;
                            magDataLength                                = magDataTuple->header.length;

                            if (magDataLength < sizeof(iswSolNetMagnetometerDataTuple))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "Magnetometer Data: Incorrect Header Length = " << std::to_string(magDataTuple->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "Magnetometer Data: Correct Header Length = " << std::to_string(magDataTuple->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            magDataTupleId = magDataTuple->header.tupleId;
                            if (magDataTupleId != MOTION_SENSE_MAG)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "Magnetometer Data: Incorrect Tuple Id = " << std::to_string(magDataTuple->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "Magnetometer Data: Correct Tuple Id = " << std::to_string(magDataTuple->header.tupleId) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            for(int i = 0; i < 3; i++)
                            {
                                std::stringstream ss2;
                                ss2 << "Magnetometer Data: Mag Data[" << std::to_string(i) << "] = " << std::to_string(magDataTuple->magnetometerData[i]) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHdr->length + magDataTuple->header.length;
                            break;
                        }
                        default:
                        {
                            std::stringstream ss1;
                            ss1 << "Motion Sense: Unknown Child Tuple Id" << std::endl;
                            ss1 << "Tuple Id     = " << std::to_string(commonDataHdr->tupleId) << std::endl;
                            ss1 << "Tuple Length = " << std::to_string(commonDataHdr->length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();

                            offset += commonDataHeaderSize + commonDataHdr->length;
                            break;
                        }
                    }

                    if (tupleVerified)
                    {

                    }
                    else
                    {
                        tupleVerified = false;
                        break;
                    }

                    numChildTuplesInc++;
                } //! end while

                if (tupleVerified == true)
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "Sample: Sample Timestamp = " << sampleTimestamp << std::endl;
                        ss_end << "Verified Motion Sense Data Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            case LrfService: //! LRF Service Data Tuples
            {
                //! The first tuple is iswSolNetLrfTargetTuple so process it
                //! then other tuples
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Verifying LRF Service Payload..." << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str() <<std::endl;

                //! Main Target Tuple -- Stores Child Tuples
                iswSolNetLrfTargetTuple *targetData = (iswSolNetLrfTargetTuple *)tuplePtr;
                totalNumberOfTargets                = targetData->totalNumberOfTargets;

                //! Target Data Header Length
                targetDataLength = targetData->header.length;

                //! Verify Target Data Header Length
                if (targetData->header.length < sizeof(iswSolNetLrfTargetTuple) - commonDataHeaderSize)
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "LRF Target Tuple: Incorrect Header Length = " << std::to_string(targetData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "LRF Target Tuple: Correct Header Length = " << std::to_string(targetData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                targetDataTupleId = targetData->header.tupleId;
                if (targetData->header.tupleId != LRF_TARGET)
                {
                    tupleVerified = false;

                    std::stringstream ss2;
                    ss2 << "LRF Target Tuple: Incorrect Tuple Id = " << std::to_string(targetData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "LRF Target Tuple: Correct Tuple Id = " << std::to_string(targetData->header.tupleId) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                targetFlags = targetData->targetFlags;
                if ((targetData->targetFlags & 0xFC) > 0x00)
                {
                    tupleVerified = false;

                    std::stringstream ss3;
                    ss3 << "LRF Target Tuple: Incorrect Flags = " << std::to_string(targetData->targetFlags) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss3.str(), GetIndex());
                    }
                    std::cout << ss3.str();
                }
                else
                {
                    std::stringstream ss3;
                    ss3 << "LRF Target Tuple: Correct Flags = " << std::to_string(targetData->targetFlags) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss3.str(), GetIndex());
                    }
                    std::cout << ss3.str();
                }

                rangeEventId = targetData->rangeEventId;
                // Nothing to check here - range can be 0 - 255, then rolls over

                targetIndex = targetData->targetIndex;
                // Nothing to check here - range can be 0 - 255, then rolls over

                std::stringstream ss3;
                ss3 << "Total Number of Targets          = " << std::to_string(totalNumberOfTargets) << std::endl;
                ss3 << "LRF Target Tuple: Range Event Id = " << std::to_string(targetData->rangeEventId) << std::endl;
                ss3 << "LRF Target Tuple: Target Index   = " << std::to_string(targetData->targetIndex) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();

                // Iterate through set optional child tuple values
                uint8_t numChildTuplesInc = 0;

                // Will track total targets and which one you are on, but not used for anything anymore.
                totalNumberOfTargets--;

                bool azimuthSet = false;

                while (numChildTuplesInc < childTupleSetValuesCount)
                {
                    int nextChildLocInPayload = offsetIntoPacket + (sizeof(iswSolNetLrfTargetTuple)-1) + 8*numChildTuplesInc;

                    if(azimuthSet)
                    {
                        nextChildLocInPayload +=4;
                    }

                    iswSolNetDataTupleCommonHeader *commonDataHdr = (iswSolNetDataTupleCommonHeader *)&iswSolNetDataPkt->dataPayload[nextChildLocInPayload];

                    switch(commonDataHdr->tupleId)
                    {
                        case LRF_TARGET_RANGE:
                        {
                            iswSolNetLrfTargetRangeTuple* targetRange = (iswSolNetLrfTargetRangeTuple *)commonDataHdr;

                            //! Verify Header Length
                            targetRangeLength = targetRange->header.length;
                            if (targetRange->header.length != (sizeof(iswSolNetLrfTargetRangeTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Range: Incorrect Header Length = " << std::to_string(targetRange->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Range: Correct Header Length = " << std::to_string(targetRange->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetRangeTupleId = targetRange->header.tupleId;
                            targetRangeData    = targetRange->range;

                            if (targetRange->range < 0)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Range: Incorrect Range = " << std::to_string(targetRange->range) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Range: Correct Range = " << std::to_string(targetRange->range) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetRange->header.length;
                            break;
                        }
                        case LRF_TARGET_RANGE_ERROR:
                        {
                            iswSolNetLrfTargetRangeErrorTuple* targetRangeError = (iswSolNetLrfTargetRangeErrorTuple*)commonDataHdr;
                            targetRangeLength                                   = targetRangeError->header.length;

                            //! Verify Header Length
                            if (targetRangeError->header.length != (sizeof(iswSolNetLrfTargetRangeErrorTuple) - commonDataHeaderSize))
                            {
                                std::stringstream ss1;
                                tupleVerified = false;

                                ss1 << "LRF Target Range: Incorrect Header Length = " << std::to_string(targetRangeError->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Range: Correct Header Length = " << std::to_string(targetRangeError->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetRangeErrorData = targetRangeError->rangeError;

                            //! Verify Target Range Error Input Value
                            if (((targetRangeError->rangeError >= 0) && (targetRangeError->rangeError < targetRangeData)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Range Error: Incorrect Range Error = " << std::to_string(targetRangeError->rangeError) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Range Error: Correct Range Error = " << std::to_string(targetRangeError->rangeError) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            //! Offset to another data tuple
                            offset += commonDataHeaderSize + targetRangeError->header.length;
                            break;
                        }
                        case LRF_TARGET_AZIMUTH:
                        {
                            iswSolNetLrfTargetAzimuthTuple* targetAzimuth = (iswSolNetLrfTargetAzimuthTuple*)commonDataHdr;

                            if (targetAzimuth->header.length != (sizeof(iswSolNetLrfTargetAzimuthTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Azimuth: Incorrect Header Length = " << std::to_string(targetAzimuth->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Azimuth: Correct Header Length = " << std::to_string(targetAzimuth->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetAzimuthData = targetAzimuth->azimuth;
                            if (((targetAzimuth->azimuth >= 0) && (targetAzimuth->azimuth <= 360)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Azimuth: Incorrect Azimuth = " << std::to_string(targetAzimuth->azimuth) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Azimuth: Correct Azimuth = " << std::to_string(targetAzimuth->azimuth) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            azimuthNorthReferenceData = targetAzimuth->azimuthNorthReference;
                            if ((targetAzimuth->azimuthNorthReference & 0xFE) != 0x00)
                            {
                                tupleVerified = false;

                                std::stringstream ss3;
                                ss3 << "LRF Target Azimuth: Incorrect Azimuth North Reference Data = " << std::to_string(targetAzimuth->azimuthNorthReference) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }
                            else
                            {
                                std::string str = "";
                                switch(targetAzimuth->azimuthNorthReference)
                                {
                                    case 0x00:
                                    {
                                        str = "True North";
                                        break;
                                    }
                                    case 0x01:
                                    {
                                        str = "Magnetic North";
                                        break;
                                    }
                                }

                                std::stringstream ss3;
                                ss3 << "LRF Target Azimuth: Correct Azimuth North Reference Data = " << str << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }

                            offset += commonDataHeaderSize + targetAzimuth->header.length;

                            azimuthSet = true;

                            break;
                        }
                        case LRF_TARGET_AZIMUTH_ERROR:
                        {
                            iswSolNetLrfTargetAzimuthErrorTuple* targetAzimuthError = (iswSolNetLrfTargetAzimuthErrorTuple*)commonDataHdr;

                            if (targetAzimuthError->header.length != (sizeof(iswSolNetLrfTargetAzimuthErrorTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Azimuth Error: Incorrect Header Length = " << std::to_string(targetAzimuthError->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Azimuth Error: Correct Header Length = " << std::to_string(targetAzimuthError->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetAzimuthErrorData = targetAzimuthError->azimuthError;
                            if (((targetAzimuthError->azimuthError >= 0) && (targetAzimuthError->azimuthError <= 180)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Azimuth Error: Incorrect Azimuth Error = " << std::to_string(targetAzimuthError->azimuthError) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Azimuth Error: Correct Azimuth Error = " << std::to_string(targetAzimuthError->azimuthError) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetAzimuthError->header.length;
                            break;
                        }
                        case LRF_TARGET_ELEVATION:
                        {
                            iswSolNetLrfTargetElevationTuple* targetElevation = (iswSolNetLrfTargetElevationTuple*)commonDataHdr;

                            if (targetElevation->header.length != (sizeof(iswSolNetLrfTargetElevationTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Elevation: Incorrect Header Length = " << std::to_string(targetElevation->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Elevation: Correct Header Length = " << std::to_string(targetElevation->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetElevationData = targetElevation->elevation;
                            if (((targetElevation->elevation >= -90) && (targetElevation->elevation <= 90)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Elevation: Incorrect Elevation = " << std::to_string(targetElevation->elevation) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Elevation: Correct Elevation = " << std::to_string(targetElevation->elevation) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetElevation->header.length;
                            break;
                        }
                        case LRF_TARGET_ELEVATION_ERROR:
                        {
                            iswSolNetLrfTargetElevationErrorTuple* targetElevationError = (iswSolNetLrfTargetElevationErrorTuple*)commonDataHdr;

                            if (targetElevationError->header.length != (sizeof(iswSolNetLrfTargetElevationErrorTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Elevation Error: Incorrect Header Length = " << std::to_string(targetElevationError->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Elevation Error: Correct Header Length = " << std::to_string(targetElevationError->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetElevationErrorData = targetElevationError->elevationError;
                            if (((targetElevationError->elevationError >= 0) && (targetElevationError->elevationError <= 90)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Elevation Error: Incorrect Elevation Error = " << std::to_string(targetElevationError->elevationError) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Elevation Error: Correct Elevation Error = " << std::to_string(targetElevationError->elevationError) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetElevationError->header.length;
                            break;
                        }
                        case LRF_QFACTOR:
                        {
                            iswSolNetLrfQFactorTuple* targetQFactor = (iswSolNetLrfQFactorTuple*)commonDataHdr;

                            if (targetQFactor->header.length != (sizeof(iswSolNetLrfQFactorTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target QFactor: Incorrect Header Length = " << std::to_string(targetQFactor->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target QFactor: Correct Header Length = " << std::to_string(targetQFactor->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            qFactorData = targetQFactor->QFactor;
                            if (targetQFactor->QFactor < 0)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target QFactor: Incorrect QFactor = " << std::to_string(targetQFactor->QFactor) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target QFactor: Correct QFactor = " << std::to_string(targetQFactor->QFactor) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetQFactor->header.length;
                            break;
                        }
                        case LRF_TARGET_ROLL:
                        {
                            iswSolNetLrfRollTuple* targetRoll = (iswSolNetLrfRollTuple*)commonDataHdr;

                            if (targetRoll->header.length != (sizeof(iswSolNetLrfRollTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Roll: Incorrect Header Length = " << std::to_string(targetRoll->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Roll: Correct Header Length = " << std::to_string(targetRoll->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            targetRollData = targetRoll->rollAngle;
                            if (((targetRoll->rollAngle >= -180) && (targetRoll->rollAngle <= 180)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Roll: Incorrect Roll Angle = " << std::to_string(targetRoll->rollAngle) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Roll: Correct Roll Angle = " << std::to_string(targetRoll->rollAngle) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetRoll->header.length;
                            break;
                        }
                        case LRF_BALLISTIC_ELEVATION_HOLD:
                        {
                            iswSolNetLrfBallisticElevationTuple* targetBallisticElevation = (iswSolNetLrfBallisticElevationTuple*)commonDataHdr;

                            if (targetBallisticElevation->header.length != (sizeof(iswSolNetLrfBallisticElevationTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Elevation: Incorrect Header Length = " << std::to_string(targetBallisticElevation->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Elevation: Correct Header Length = " << std::to_string(targetBallisticElevation->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            ballisticElevationHoldData = targetBallisticElevation->ballisticElevationHold;
                            if (((targetBallisticElevation->ballisticElevationHold >= -90) && (targetBallisticElevation->ballisticElevationHold <= 90)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Elevation: Incorrect Ballistic Elevation Hold = " << std::to_string(targetBallisticElevation->ballisticElevationHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Elevation: Correct Ballistic Elevation Hold = " << std::to_string(targetBallisticElevation->ballisticElevationHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetBallisticElevation->header.length;
                            break;
                        }
                        case LRF_BALLISTIC_WINDAGE_HOLD:
                        {
                            iswSolNetLrfBallisticWindageTuple* targetBallisticWindage = (iswSolNetLrfBallisticWindageTuple*)commonDataHdr;

                            if (targetBallisticWindage->header.length != (sizeof(iswSolNetLrfBallisticWindageTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Windage: Incorrect Header Length = " << std::to_string(targetBallisticWindage->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Windage: Correct Header Length = " << std::to_string(targetBallisticWindage->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            ballisticWindageHoldData = targetBallisticWindage->ballisticWindageHold;
                            if (((targetBallisticWindage->ballisticWindageHold >= -90) && (targetBallisticWindage->ballisticWindageHold <= 90)) != true)
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Windage: Incorrect Ballistic Windage Hold = " << std::to_string(targetBallisticWindage->ballisticWindageHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Windage: Correct Ballistic Windage Hold = " << std::to_string(targetBallisticWindage->ballisticWindageHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetBallisticWindage->header.length;
                            break;
                        }
                        case LRF_BALLISTIC_WINDAGE_CONFIDENCE:
                        {
                            iswSolNetLrfBallisticWindageConfidenceTuple* targetBallisticWindageConfidence = (iswSolNetLrfBallisticWindageConfidenceTuple*)commonDataHdr;

                            if (targetBallisticWindageConfidence->header.length != (sizeof(iswSolNetLrfBallisticWindageConfidenceTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Windage Confidence: Incorrect Header Length = " << std::to_string(targetBallisticWindageConfidence->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Ballistic Windage Confidence: Correct Header Length = " << std::to_string(targetBallisticWindageConfidence->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            ballisticConfidenceHoldData = targetBallisticWindageConfidence->ballisticWindageConfidenceHold;
                            if ((targetBallisticWindageConfidence->ballisticWindageConfidenceHold <= 0.00) || (targetBallisticWindageConfidence->ballisticWindageConfidenceHold >= 1.00))
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Windage Confidence: Incorrect Ballistic Windage Hold = " << std::to_string(targetBallisticWindageConfidence->ballisticWindageConfidenceHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Ballistic Windage Confidence: Correct Ballistic Windage Hold = " << std::to_string(targetBallisticWindageConfidence->ballisticWindageConfidenceHold) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + targetBallisticWindageConfidence->header.length;
                            break;
                        }
                        case LRF_TEMPERATURE:
                        {
                            iswSolNetLrfTemperatureTuple* lrfTemperature = (iswSolNetLrfTemperatureTuple*)commonDataHdr;

                            if (lrfTemperature->header.length != (sizeof(iswSolNetLrfTemperatureTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Temperature: Incorrect Header Length = " << std::to_string(lrfTemperature->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Temperature: Correct Header Length = " << std::to_string(lrfTemperature->header.length) << std::endl;
                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            //! In Celsius
                            temperatureData = lrfTemperature->temperature;
                            if ((lrfTemperature->temperature < -100.00) || (lrfTemperature->temperature > 100.00))
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Temperature: Incorrect Temperature = " << std::to_string(lrfTemperature->temperature) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Temperature: Correct Temperature = " << std::to_string(lrfTemperature->temperature) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + lrfTemperature->header.length;
                            break;
                        }
                        case LRF_HUMIDITY:
                        {
                            iswSolNetLrfHumidityTuple* lrfHumidity = (iswSolNetLrfHumidityTuple*)commonDataHdr;

                            if (lrfHumidity->header.length != (sizeof(iswSolNetLrfHumidityTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Relative Humidity: Incorrect Header Length = " << std::to_string(lrfHumidity->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Relative Humidity: Correct Header Length = " << std::to_string(lrfHumidity->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            // Relative in percent
                            humidityData = lrfHumidity->humidity;
                            if ((lrfHumidity->humidity < 0) || (lrfHumidity->humidity > 100))
                            {
                                tupleVerified = false;

                                std::stringstream ss2;
                                ss2 << "LRF Target Relative Humidity: Incorrect Relative Humidity = " << std::to_string(lrfHumidity->humidity) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }
                            else
                            {
                                std::stringstream ss2;
                                ss2 << "LRF Target Relative Humidity: Correct Relative Humidity = " << std::to_string(lrfHumidity->humidity) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss2.str(), GetIndex());
                                }
                                std::cout << ss2.str();
                            }

                            offset += commonDataHeaderSize + lrfHumidity->header.length;
                            break;
                        }
                        case LRF_PRESSURE:
                        {
                            iswSolNetLrfPressureTuple* lrfPressure = (iswSolNetLrfPressureTuple*)commonDataHdr;

                            if (lrfPressure->header.length != (sizeof(iswSolNetLrfPressureTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Pressure: Incorrect Header Length = " << std::to_string(lrfPressure->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Pressure: Correct Header Length = " << std::to_string(lrfPressure->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            std::stringstream ss2;
                            ss2 << "LRF Target Pressure: Correct Pressure = " << std::to_string(lrfPressure->pressure) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();


                            offset += commonDataHeaderSize + lrfPressure->header.length;
                            break;
                        }
                        case LRF_CLEAR_RANGE_EVENT:
                        {
                            iswSolNetLrfClearRangeEventTuple* lrfClearRangeEvent = (iswSolNetLrfClearRangeEventTuple*)commonDataHdr;

                            if (lrfClearRangeEvent->header.length != (sizeof(iswSolNetLrfClearRangeEventTuple) - commonDataHeaderSize))
                            {
                                tupleVerified = false;

                                std::stringstream ss1;
                                ss1 << "LRF Target Clear Range Event: Incorrect Header Length = " << std::to_string(lrfClearRangeEvent->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }
                            else
                            {
                                std::stringstream ss1;
                                ss1 << "LRF Target Clear Range Event: Correct Header Length = " << std::to_string(lrfClearRangeEvent->header.length) << std::endl;
                                if (theLogger->DEBUG_IswSolNet == 1)
                                {
                                    theLogger->DebugMessage(ss1.str(), GetIndex());
                                }
                                std::cout << ss1.str();
                            }

                            range_EventIdData = lrfClearRangeEvent->rangeEventId;
                            // Nothing to do here - could be any number

                            offset += commonDataHeaderSize + lrfClearRangeEvent->header.length;
                            break;
                        }
                        default:
                        {
                            std::stringstream ss1;
                            ss1 << "LRF Target - Unknown Child Tuple Id: " << std::endl;
                            ss1 << "Tuple Id     = " << std::to_string(commonDataHdr->tupleId) << std::endl;
                            ss1 << "Tuple Length = " << std::to_string(commonDataHdr->length) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();

                            tupleVerified = false;
                        }
                    }

                    if (tupleVerified)
                    {

                    }
                    else
                    {
                        tupleVerified = false;
                        break;
                    }

                    numChildTuplesInc++;
                } //! end while

                if ( tupleVerified == true )
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "Verified LRF Service Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                else
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "LRF Service Payload Not Verified" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            case LrfControlService:
            {
                iswSolNetLrfRangeControlDataDesc* rangeControlData = (iswSolNetLrfRangeControlDataDesc *)tuplePtr;
                rangeControlDataLength                             = rangeControlData->header.length;
                rangeControlDataTupleId                            = rangeControlData->header.tupleId;

                if (rangeControlData->header.length != (sizeof(iswSolNetLrfRangeControlDataDesc) - commonDataHeaderSize))
                {
                    tupleVerified = false;

                    std::stringstream ss1;
                    ss1 << "LRF Control Range: Incorrect Header Length = " << std::to_string(rangeControlData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }
                else
                {
                    std::stringstream ss1;
                    ss1 << "LRF Control Range: Correct Header Length = " << std::to_string(rangeControlData->header.length) << std::endl;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        theLogger->DebugMessage(ss1.str(), GetIndex());
                    }
                    std::cout << ss1.str();
                }

                if (tupleVerified == true)
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "Verified LRF Control Service Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                else
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "LRF Control Service Payload Not Verified" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            case Status:
            {
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Verifying Status Service Payload..." << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str() << std::endl;

                switch(tuplePtr->tupleId)
                {
                    case STATUS_BATTERY_ID:
                    {
                        iswSolNetBatteryStatusTuple* batteryStatusData = (iswSolNetBatteryStatusTuple *)tuplePtr;
                        batteryDataLength                              = batteryStatusData->header.length;
                        batteryDataTupleId                             = batteryStatusData->header.tupleId;

                        //! Verify Header Length
                        if (batteryStatusData->header.length != (sizeof(iswSolNetBatteryStatusTuple) - commonDataHeaderSize))
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Battery Status: Incorrect Header Length = " << std::to_string(batteryStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Battery Status: Correct Header Length = " << std::to_string(batteryStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        //! Verify Data
                        batteryLevel = batteryStatusData->batteryLevel;
                        if ((batteryStatusData->batteryLevel < 0) || (batteryStatusData->batteryLevel > 100))
                        {
                            std::stringstream ss2;
                            ss2 << "Battery Status: Incorrect Battery Status Level = " << std::to_string(batteryStatusData->batteryLevel) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Battery Status: Correct Battery Status Level = " << std::to_string(batteryStatusData->batteryLevel) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        batteryState = batteryStatusData->batteryState;
                        std::string batteryStateStr = "";
                        switch(batteryState)
                        {
                            case IswSolNet::BATTERY_OK:
                            {
                                 batteryStateStr = "Battery Ok";
                                 break;
                            }
                            case IswSolNet::BATTERY_LOW:
                            {
                                batteryStateStr = "Battery Low";
                                break;
                            }
                            case IswSolNet::BATTERY_CRITICAL:
                            {
                                batteryStateStr = "Battery Critical";
                                break;
                            }
                            case IswSolNet::BATTERY_CHARGING:
                            {
                                batteryStateStr = "Battery Charging";
                                break;
                            }
                            default:
                            {
                                batteryStateStr = "Unknown";
                                break;
                            }
                        }

                        std::stringstream ss3;
                        ss3 << "Battery Status: Battery State = " + batteryStateStr << std::endl;
                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss3.str(), GetIndex());
                        }
                        std::cout << ss3.str();

                        batteryCharging = batteryStatusData->batteryCharging;
                        std::string batteryChargingStr = "";

                        switch(batteryCharging)
                        {
                            case IswSolNet::BATTERY_NOT_CHARGING:
                            {
                                 batteryChargingStr = "Battery Not Charging";
                                 break;
                            }
                            case IswSolNet::BATTERY_IS_CHARGING:
                            {
                                batteryChargingStr = "Battery Is Charging";
                                break;
                            }
                            case IswSolNet::BATTERY_DOESNT_SUPPORT_CHARGING:
                            {
                                batteryChargingStr = "Battery Doesn't Support Charging";
                                break;
                            }
                            default:
                            {
                                batteryChargingStr = "Unknown";
                                break;
                            }
                        }
                        if (batteryStatusData->batteryCharging > 0x02)
                        {
                            std::stringstream ss3;
                            ss3 << "Battery Status: Incorrect Battery Charging = " + batteryChargingStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }
                        else
                        {
                            std::stringstream ss4;
                            ss4 << "Battery Status: Correct Battery Charging = " + batteryChargingStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }

                        batteryId = batteryStatusData->batteryId;
                        //! Nothing to do here - can be any value 0 - 0xFF

                        offset += commonDataHeaderSize + batteryStatusData->header.length;
                        break;
                    }
                    case STATUS_DEVICE_ID:
                    {
                        iswSolNetDeviceStatusTuple* deviceStatusData = (iswSolNetDeviceStatusTuple *)tuplePtr;
                        deviceStatusDataLength                       = deviceStatusData->header.length;
                        deviceStatusDataTupleId                      = deviceStatusData->header.tupleId;

                        //! Verify Header Length
                        if (deviceStatusData->header.length != (sizeof(iswSolNetDeviceStatusTuple) - commonDataHeaderSize))
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Device Status: Incorrect Header Length = " << std::to_string(deviceStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Device Status: Correct Header Length = " << std::to_string(deviceStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        //! Verify Data
                        deviceState = deviceStatusData->deviceState;
                        std::string deviceStateStr = "";

                        switch(deviceState)
                        {
                            case IswSolNet::DEVICE_TURNING_OFF:
                            {
                                 deviceStateStr = "Device Turning Off";
                                 break;
                            }
                            case IswSolNet::DEVICE_ON:
                            {
                                deviceStateStr = "Device On";
                                break;
                            }
                            default:
                            {
                                deviceStateStr = "Unknown";
                                break;
                            }
                        }
                        if (deviceState > 0x01)
                        {
                            std::stringstream ss2;
                            ss2 << "Device Status: Incorrect Device State Level = " + deviceStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Device Status: Correct Device State Level = " + deviceStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        displayState = deviceStatusData->displayState;
                        std::string displayStateStr = "";

                        switch(displayState)
                        {
                            case IswSolNet::DISPLAY_OFF:
                            {
                                 displayStateStr = "Display Off";
                                 break;
                            }
                            case IswSolNet::DISPLAY_ON:
                            {
                                displayStateStr = "Display On";
                                break;
                            }
                            default:
                            {
                                displayStateStr = "Unknown";
                                break;
                            }
                        }

                        if (deviceStatusData->displayState > 0x01)
                        {
                            std::stringstream ss3;
                            ss3 << "Device Status: Incorrect Display State = " + displayStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }
                        else
                        {
                            std::stringstream ss3;
                            ss3 << "Device Status: Correct Display State = " + displayStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }

                        offset += commonDataHeaderSize + deviceStatusData->header.length;
                        break;
                    }
                    case STATUS_WEAPON_ID:
                    {
                        iswSolNetWeaponStatusTuple* weaponStatusData = (iswSolNetWeaponStatusTuple *)tuplePtr;
                        weaponStatusDataLength                       = weaponStatusData->header.length;
                        weaponStatusDataTuple                        = weaponStatusData->header.tupleId;

                        //! Verify Header Length
                        if (weaponStatusData->header.length != (sizeof(iswSolNetWeaponStatusTuple) - commonDataHeaderSize))
                        {
                            tupleVerified = false;

                            std::stringstream ss1;
                            ss1 << "Weapon Status: Incorrect Header Length = " << std::to_string(weaponStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }
                        else
                        {
                            std::stringstream ss1;
                            ss1 << "Weapon Status: Correct Header Length = " << std::to_string(weaponStatusData->header.length) << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss1.str(), GetIndex());
                            }
                            std::cout << ss1.str();
                        }

                        //! Verify Data
                        weaponSightMode = weaponStatusData->weaponSightMode;
                        std::string weaponSightModeStr = "";

                        switch(weaponSightMode)
                        {
                            case IswSolNet::STANDALONE:
                            {
                                 weaponSightModeStr = "Standalone";
                                 break;
                            }
                            case IswSolNet::CCO:
                            {
                                weaponSightModeStr = "CCO";
                                break;
                            }
                            case IswSolNet::RCO:
                            {
                                weaponSightModeStr = "RCO";
                                break;
                            }
                            case IswSolNet::FULL_STANDALONE:
                            {
                                weaponSightModeStr = "Full Standalone";
                                break;
                            }
                            default:
                            {
                                weaponSightModeStr = "Unknown";
                                break;
                            }
                        }
                        if (weaponSightMode > 0x03)
                        {
                            std::stringstream ss2;
                            ss2 << "Weapon Status: Incorrect Weapon Sight Mode = " + weaponSightModeStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }
                        else
                        {
                            std::stringstream ss2;
                            ss2 << "Weapon Status: Correct Weapon Sight Mode = " + weaponSightModeStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss2.str(), GetIndex());
                            }
                            std::cout << ss2.str();
                        }

                        activeReticleId = weaponStatusData->activeReticleId;
                        std::string activeReticleIdStr = "";

                        switch(activeReticleId)
                        {
                            case IswSolNet::NONE:
                            {
                                 activeReticleIdStr = "None";
                                 break;
                            }
                            case IswSolNet::M4_M16:
                            {
                                activeReticleIdStr = "M4/M16";
                                break;
                            }
                            case IswSolNet::M249_S:
                            {
                                activeReticleIdStr = "M249 S";
                                break;
                            }
                            case IswSolNet::M249_L:
                            {
                                activeReticleIdStr = "M249 L";
                                break;
                            }
                            case IswSolNet::MIL_10:
                            {
                                 activeReticleIdStr = "10 MIL";
                                 break;
                            }
                            case IswSolNet::M136:
                            {
                                activeReticleIdStr = "M136";
                                break;
                            }
                            case IswSolNet::M141:
                            {
                                activeReticleIdStr = "M141";
                                break;
                            }
                            case IswSolNet::MIL_SCALE:
                            {
                                activeReticleIdStr = "MIL SCALE";
                                break;
                            }
                            case IswSolNet::RTA_BORESIGHT:
                            {
                                activeReticleIdStr = "RTA BORESIGHT";
                                break;
                            }
                            case IswSolNet::STINGER:
                            {
                                activeReticleIdStr = "Stinger";
                                break;
                            }
                            default:
                            {
                                activeReticleIdStr = "Unknown";
                                break;
                            }
                        }
                        if (weaponStatusData->activeReticleId > 0x10)
                        {
                            std::stringstream ss3;
                            ss3 << "Weapon Status: Incorrect Active Reticle Id = " + activeReticleIdStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }
                        else
                        {
                            std::stringstream ss3;
                            ss3 << "Weapon Status: Correct Active Reticle Id = " + activeReticleIdStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }

                        symbologyState = weaponStatusData->symbologyState;
                        std::string symbologyStateStr = "";

                        switch(symbologyState)
                        {
                            case IswSolNet::SYMBOLOGY_OFF:
                            {
                                 symbologyStateStr = "Symbology Off";
                                 break;
                            }
                            case IswSolNet::SYMBOLOGY_ON:
                            {
                                symbologyStateStr = "Symbology On";
                                break;
                            }
                            default:
                            {
                                symbologyStateStr = "Unknown";
                                break;
                            }
                        }
                        if (weaponStatusData->symbologyState > 0x01)
                        {
                            std::stringstream ss3;
                            ss3 << "Weapon Status: Incorrect Symbology State = " + symbologyStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }
                        else
                        {
                            std::stringstream ss3;
                            ss3 << "Weapon Status: Correct Symbology State = " + symbologyStateStr << std::endl;
                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss3.str(), GetIndex());
                            }
                            std::cout << ss3.str();
                        }

                        offset += commonDataHeaderSize + weaponStatusData->header.length;
                        break;
                    }
                    default:
                    {
                        std::stringstream ss1;
                        ss1 << "Status - Unknown Tuple Id:" << std::endl;
                        ss1 << "Tuple Id     = " << std::to_string(tuplePtr->tupleId) << std::endl;
                        ss1 << "Tuple Length = " << std::to_string(tuplePtr->tupleId) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss1.str(), GetIndex());
                        }
                        std::cout << ss1.str();

//                        offset += commonDataHeaderSize + commonDataHdr->length;
                        break;
                    }
                }

                if (tupleVerified == true)
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "Verified Status Payload" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                else
                {
                    std::stringstream ss_end;
                    if (theLogger->DEBUG_IswSolNet == 1)
                    {
                        ss_end << "Status Payload Not Verified" << std::endl;
                        theLogger->DebugMessage(ss_end.str(), iswInterface->GetIndex());
                    }
                    std::cout << ss_end.str();
                }
                break;
            }
            default:
            {
                std::stringstream ss;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    ss << "Device " << std::to_string(iswInterface->GetIndex()) << " Unknown Data Payload Tuple Id = " << std::to_string(tuplePtr->tupleId) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << ss.str();
                break;
            }
        }

        // Go to the next descriptor in the main dataPayload buffer
        offsetIntoPacket += tuplePtr->length + commonDataHeaderSize;
    }
    verifyDataPayloadLock.unlock();
}

void IswSolNet::ParseSolNetAdvertiseAnnounceMessage(uint8_t peerIndex, iswSolNetMessagePacket *iswSolNetMsgPkt)
{
    //! This method gets called from an IswInterface thread that
    //! processes the SolNet queues so use a lock for reentrancy issues
    parseAdvertiseMessageLock.lock();

    uint16_t offsetIntoPacket = 0;

    while ( offsetIntoPacket < iswSolNetMsgPkt->messageLength )
    {
        bool identifiedDescriptor = false;

        //! Get the beginning of the incoming Advertise Announce message
        iswSolNetDescCommonHeader *descriptor = (iswSolNetDescCommonHeader *)&iswSolNetMsgPkt->messagePayload[offsetIntoPacket];

        std::stringstream ss_main;
        ss_main << "" << std::endl;
        ss_main << "Device " << std::to_string(iswInterface->GetIndex()) << " Processing Service DescriptorId = " << std::to_string(descriptor->descriptorId) << " from peer = " << std::to_string(peerIndex) << std::endl;

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            theLogger->DebugMessage(ss_main.str(), iswInterface->GetIndex());
        }

        if ( verifyAdvertiseMessages == true )
        {
            std::cout << ss_main.str();
        }

        switch (descriptor->descriptorId)
        {
            case VideoService:
            case Audio:
            case PositionSense:
            case EnvironmentSense:
            case HealthSense:
            case SimpleUI:
            case Status:
            case RTA_SERVICE:
            case HybridSense:
            case MotionSense:
            case TemperatureSense:
            case MoistureSense:
            case VibrationSense:
            case ChemicalSense:
            case FlowSense:
            case ForceSense:
            case LrfService:
            case LrfControlService:
            case ArService:
            case Pipe:
            case PacketGenerator:
//            case Device:
            {
                // This is a service descriptor, so parse it and add to the peer's service list
                iswSolNetServiceDesc *incomingDesc = (iswSolNetServiceDesc *)descriptor;

                //! The incoming data with the peer's services has to be copied into new buffers
                //! and added to the vector of services for this peer
                iswSolNetServiceEntry serviceEntry;
                serviceEntry.serviceDesc.header.length          = incomingDesc->header.length;
                serviceEntry.serviceDesc.header.descriptorId    = incomingDesc->header.descriptorId;
                serviceEntry.serviceDesc.serviceSelector        = incomingDesc->serviceSelector;

                //! Allocate a buffer for the childblock
                uint32_t childblockLength =  serviceEntry.serviceDesc.header.length - selectorSize;

                if ( childblockLength > 0 )
                {
                    //! Parse descriptors in the childblock

                    //! Offset to where childblock pointer is in the incoming packet
                    uint16_t childblockOffset = commonHeaderSize + selectorSize;

                    //! Copy in the childblock because payload buffer belongs to IswInterface
                    uint8_t childblockBuff[childblockLength];
                    memcpy(&childblockBuff[0], &iswSolNetMsgPkt->messagePayload[offsetIntoPacket + childblockOffset], childblockLength);
                    serviceEntry.serviceDesc.childBlock = childblockBuff;

                    uint16_t offset        = 0;
                    bool foundEndpointDesc = false;

                    while ( offset < childblockLength )
                    {
                        iswSolNetDescCommonHeader *commonHdr = (iswSolNetDescCommonHeader *)&(childblockBuff[offset]);

                        std::stringstream ss;
                        ss << "  Childblock descriptorId = " << std::to_string(commonHdr->descriptorId) << " from peer = " << std::to_string(peerIndex) << " being parsed" << std::endl;

                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                        }

                        if ( verifyAdvertiseMessages == true )
                        {
                            std::cout << ss.str();
                        }

                        if ( commonHdr->descriptorId == Endpoint )
                        {
                            iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)(&serviceEntry.serviceDesc.childBlock[offset]);

                            // Some Vendor ICD's add the consumer/producer bit to the following fields, so we have to check
                            serviceEntry.dataflowId           = endpointDesc->dataflowId & iswSolNetProviderConsumerMask;
                            serviceEntry.endpointId           = endpointDesc->endpointId & iswSolNetProviderConsumerMask;
                            serviceEntry.dataPolicies         = endpointDesc->dataPolicies;
                            serviceEntry.endpointDistribution = endpointDesc->endpointDistribution;

                            //! Each service has to have an Endpoint Descriptor before
                            //! we can add it to the peer services table
                            foundEndpointDesc = true;
                            break;
                        }
                        offset += (commonHdr->length + commonHeaderSize);
                    }

                    if ( foundEndpointDesc )
                    {                            
                        //! Add this set of services to the peer's services
                        //! Data gets copied again onto the list
                        AddSolNetService(peerIndex, &serviceEntry);
                    }
                    else
                    {
                        std::stringstream ss;
                        ss << "  Service descriptorId = " << std::to_string(descriptor->descriptorId) << " from peer = " << std::to_string(peerIndex) << " not processed - no Endpoint Descriptor!!" << std::endl;

                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                        }

                        if ( verifyAdvertiseMessages == true )
                        {
                            std::cout << ss.str();
                        }
                    }
                }

                identifiedDescriptor = true;

                // Go to the next descriptor
                // incomingDesc->header.length is the length of the
                // whole descriptor including Common Header and data
                offsetIntoPacket += descriptor->length + commonHeaderSize;
                break;
            }
            case Control:
            case Reserved1:
            case ReservedMin:
            case ReservedMax:
            default:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::string msgString = "Unknown SolNet DescriptorId";
                    iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                identifiedDescriptor = false;
                break;
            }
        }

        if ( identifiedDescriptor == false )
        {
            // Look for one of the child descriptors
            // Some ICDs have child descriptors outside
            // of root descriptors - by themselves
            switch (descriptor->descriptorId)
            {
                case Label:
                case Version:
                case Endpoint:
                case MulticastGroup:
                case Encoding:
                case FlowPolicy:
                case DeviceName:
                case DeviceSerialNumber:
                case DeviceManufacturer:
                case DeviceFriendlyName:
                case AssociatedService:
                case EndpointProtocol:
                case TextLabel:
                case Device:
                case QoSProperties:
                case QoSDataRate:
                case QoSMaxLatency:
                case DeviceSoftwareDesc:
                case DeviceSoftwareNameDesc:
                case DeviceSoftwareVersionDesc:
                case VideoOutputFormatRaw:
                case VideoFormat:
                case VideoSampleFormat:
                case VideoProtocol:
                case IFOV:
                case VideoLensDistortion:
                case VideoControl:
                case ImageSensorType:
                case ImageSensorWaveband:
                case UiMotion:
                case UiButtonInput:
                case ImuOutputRate:
                case ImuSensorNoise:
                case MotionSensorRate:
                case MotionSensorAccel:
                case MotionSensorGyro:
                case MotionSensorMag:
                case MotionSensorAxes:
                case NetworkingIP:
                case NetworkingTCPPort:
                case NetworkingUDPPort:
                case StatusBattery:
                {
                    // Not sure what to do with these
                    // We are  currently only saving services
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::string msgString = "Received SolNet DescriptorId = ";
                        msgString.append(std::to_string(descriptor->descriptorId));
                        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }

                    identifiedDescriptor = true;

                    // Go to the next descriptor
                    // The descriptor->length for child descriptors does
                    // not include the length of the Common Header
                    offsetIntoPacket += descriptor->length + commonHeaderSize;
                    break;
                }
                case NetworkingReserved1:
                case NetworkingReserved2:
                case PowerReserved1:
                case PowerReserved2:
                default:
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::string msgString = "Unknown SolNet DescriptorId = ";
                        msgString.append(std::to_string(descriptor->descriptorId));
                        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }

                    identifiedDescriptor = false;
                    break;
                }
            }
        }

        if ( identifiedDescriptor == false )
        {
            break;
        }
    }

    parseAdvertiseMessageLock.unlock();
}

//!###################################################################
//! DeliverSolNetDataPacket()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         endPoint  - ISW endpoints are 1 - 3 for SolNet data,
//!                     endpoint 0 for SolNet messages
//!         iswSolNetDataPkt - pointer to the iswSolNetDataPacket
//! Description: This method handles incoming ISW SolNet data packets
//!###################################################################
void IswSolNet::DeliverSolNetDataPacket(uint8_t peerIndex, uint8_t endPoint, iswSolNetDataPacket *iswSolNetDataPkt)
{
    uint64_t now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    std::stringstream ss;
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {

        uint64_t now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        std::stringstream ss;

        ss  << "Device " << QString::number(iswInterface->GetIndex()).toStdString() << " DeliverSolNetDataPacket at " << std::to_string(now) << " With Payload Of "<< iswSolNetDataPkt->dataLength << " Bytes" << std::endl;

//        qDebug() << "delivering data packet at " << now << "with dataFlowId: " << iswSolNetDataPkt->dataflowId;

        iswInterface->theLogger->LogSimpleMessage(ss.str());
        iswInterface->throughputTestLogger->LogSimpleMessage(ss.str());
    }

    int statusMsgLen = 8;

//    qDebug() << "msg len: " << iswSolNetDataPkt->dataLength;

    if(iswSolNetDataPkt->dataflowId == 0 && iswSolNetDataPkt->dataLength == statusMsgLen)
    {

//        qDebug() << "overwriting 0 dataFlowId from 0 to 1";
        iswSolNetDataPkt->dataflowId = 1;
    }

    if(iswSolNetDataPkt->dataflowId == 4 && iswSolNetDataPkt->dataLength == statusMsgLen)
    {

//        qDebug() << "overwriting 0 dataFlowId from 4 to 5";
        iswSolNetDataPkt->dataflowId = 5;
    }

    //! Get the dataflowId without consumer bit - that's our index into tables
    uint8_t dataflowId = iswSolNetDataPkt->dataflowId & iswSolNetProviderConsumerMask;

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "Device " << std::to_string(iswInterface->GetIndex()) << " Receiving SolNet Data Pkt from Peer = " << std::to_string(peerIndex) << std::endl;
        ss << "    endpoint   = " << std::to_string(endPoint) << std::endl;
        ss << "    dataflowId = " << std::to_string(dataflowId) << std::endl;
        ss << "    seqNumber  = " << std::to_string(iswSolNetDataPkt->seqNumber) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }

    if ( (iswSolNetDataPkt->policies & POLICY_PAYLOAD_CHECKSUM) == POLICY_PAYLOAD_CHECKSUM )
    {
        //! Check payload checksum
        uint16_t checksum = 0;
        bool goodChecksum = true;
        checksum          = IswChecksum((uint32_t)iswSolNetDataPkt->dataLength, (uint16_t *)iswSolNetDataPkt->dataPayload);

        //! If payload falls on 16-bit boundary checksum is after it
        //! Else there is a pad byte
        uint16_t payloadLength = iswSolNetDataPkt->dataLength;
        if ( (checksum > 0) &&
             (iswSolNetDataPkt->dataLength < payloadDataBufferSize) )
        {
            uint16_t *packetChecksum = nullptr;
            if ( (payloadLength % 16) == 0 )
            {
                packetChecksum = (uint16_t *)&iswSolNetDataPkt->dataPayload[payloadLength];

                if ( checksum != *packetChecksum )
                {
                    goodChecksum = false;
                }
            }
            else
            {
                packetChecksum = (uint16_t *)&iswSolNetDataPkt->dataPayload[payloadLength + 1];

                if ( checksum != *packetChecksum )
                {
                    goodChecksum = false;
                }
            }
        }
        else
        {
            if ( checksum != iswSolNetDataPkt->payloadChecksum )
            {
                goodChecksum = false;
            }
        }

        if ( goodChecksum == false )
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Device " << std::to_string(iswInterface->GetIndex()) << " DeliverSolNetDataPacket from Peer = " << std::to_string(peerIndex) << std::endl;
                ss << "    payload checksum bad!" << std::endl;
                ss << "    checksum = " << std::to_string(checksum) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            return;
        }
    }

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "Device " << std::to_string(iswInterface->GetIndex()) << " DeliverSolNetDataPacket from Peer = " << std::to_string(peerIndex) << std::endl;
        ss << "    endpoint   = " << std::to_string(endPoint) << std::endl;
        ss << "    dataflowId = " << std::to_string(dataflowId) << std::endl;
        ss << "    seqNumber  = " << std::to_string(iswSolNetDataPkt->seqNumber) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }

    //! Find the service to get the callback ptr
    IswSolNet::iswSolNetPeerServiceEntry *serviceEntry =  &iswSolNetPeerServices[peerIndex].services[dataflowId];

//    qDebug() << "dataFlowId: " << dataflowId;
//    qDebug() << "serviceEntry dataFlowId: " << serviceEntry->dataflowId;

    //! Verify we are still communicating with this peer
    if ( serviceEntry->inUse )
    {
//        qDebug() << "service entry confirmed in use";
        //! Verify that we are registered to receive data
        if ( (serviceEntry->ImRegistered) && (serviceEntry->dataflowId == dataflowId) )
        {
            if ( verifyAdvertiseMessages )
            {
                qDebug() << "About to call verification";
                VerifySolNetDataPayload(serviceEntry, iswSolNetDataPkt);
            }

            //! Call the registered callback function to deliver the data to the application
            //!
            if ( serviceEntry->receiveCallbackFn != nullptr )
            {
                serviceEntry->receiveCallbackFn(iswSolNetDataPkt->dataPayload, iswSolNetDataPkt->dataLength, serviceEntry->recCallbackThisPtr);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Device " << std::to_string(iswInterface->GetIndex()) << " Deliver SolNetDataPacket to Callback for dataflowId = " << std::to_string(dataflowId) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                IswStream *iswDestStream = iswInterface->GetIswStream();
                int node_idx = iswInterface->GetIndex();
                //qDebug() << "node_idx: " << QString::number(node_idx);
                int rxMbps = iswDestStream->GetPeerRef()[node_idx].iswPeerRecord4.rxThroughput;

                // Write out RX every 100 messages for external nodes
                if(iswSolNetDataPkt->seqNumber % 100 == 0)
                {
                    std::stringstream ss;
                    ss << "Device " << std::to_string(iswInterface->GetIndex()) << " SolNetDataPacket Attempting To Deliver for dataflowId = " << std::to_string(dataflowId) << std::endl;
                    ss << "No callback function, fine if receiving from external node." <<
                    ss << "Rx: " << std::to_string(rxMbps) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }


                qDebug() << Q_FUNC_INFO << "No callback function, fine if received from external node.";

            }


            //! Send an ACK to a registered peer if the policy field indicates this
            if ( (iswSolNetDataPkt->policies & POLICY_ACK) == POLICY_ACK )
            {
                //! Allocate a new ACK packet to put on a list
                iswSolNetAckPacket *ackPkt = new iswSolNetAckPacket;
                ackPkt->endpoint           = endPoint;
                ackPkt->dataflowId         = iswSolNetDataPkt->dataflowId; //! Use the original with consumer/producer bit set
                ackPkt->seqNumber          = iswSolNetDataPkt->seqNumber;
                ackPkt->status             = 0;  //! Ack = 0, Nak = 1
                ackPkt->protocolClass      = 0;  //! data = 0, message = 1
                ackPkt->reserved           = 0;  //! Use the reserved field to pass peerIndex internally

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Device " << std::to_string(iswInterface->GetIndex()) << " Adding ackPkt for Peer = " << std::to_string(peerIndex) << std::endl;
                    ss <<  "    seqNumber = " << std::to_string(ackPkt->seqNumber) << std::endl;
                    ss <<  "    datflowId = " << std::to_string(ackPkt->dataflowId) << std::endl;
                    ss <<  "    endpoint  = " << std::to_string(ackPkt->endpoint) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }

                if ( (iswSolNetDataPkt->dataflowId & iswSolNetSetConsumerBit) == iswSolNetSetConsumerBit )
                {
                    //! We received a data packet from a peer
                    //! If the consumer bit is set, then the peer is the consumer
                    //! We are the producer so the packet goes on this device's service registered peers
                    //! ACK list
                    if ( iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList != nullptr )
                    {
                        iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
                        iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList->push_back(ackPkt);
                        iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
                    }
                }
                else
                {
                    //! The consumer bit is not set, so the Peer is the producer requesting an ACK
                    //! to this data packet.  Put the ACK on the Peer's service ACK list
                    if ( iswSolNetPeerServices[peerIndex].acksToSendList != nullptr )
                    {
                        iswSolNetPeerServices[peerIndex].peerLock.lock();
                        iswSolNetPeerServices[peerIndex].acksToSendList->push_back(ackPkt);
                        iswSolNetPeerServices[peerIndex].peerLock.unlock();
                    }
                }
            }
        }
    }
}

//!#####################################################################
//! DeliverSolNetMessagePacket()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         endPoint - ISW endpoints are 1 - 3 for SolNet data,
//!                    endpoint 0 for SolNet messages
//!         iswSolNetMsgPkt - pointer to the iswSolNetMessagePacket
//! Description: This method handles incoming ISW SolNet message packets
//!#####################################################################
void IswSolNet::DeliverSolNetMessagePacket(uint8_t peerIndex, uint8_t endPoint, iswSolNetMessagePacket *iswSolNetMsgPkt)
{
    std::string msgString = "DeliverSolNetMessagePacket ";
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "Device " << std::to_string(iswInterface->GetIndex()) << " Receiving SolNet Message Pkt from Peer = " << std::to_string(peerIndex) << std::endl;
        ss << "    peerIndex    = " << std::to_string(peerIndex) << std::endl;
        ss << "    endPoint     = " << std::to_string(endPoint) << std::endl;
        ss << "    messageClass = " << std::to_string(iswSolNetMsgPkt->messageClass) << std::endl;
        ss << "    messageId    = " << std::to_string(iswSolNetMsgPkt->messageId) << std::endl;
        ss << "    seqNumber    = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
        ss << "    policies     = " << std::to_string(iswSolNetMsgPkt->policies) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }

    if ( (iswSolNetMsgPkt->policies & POLICY_PAYLOAD_CHECKSUM) == POLICY_PAYLOAD_CHECKSUM )
    {
        //! Check payload checksum
        //! ISW payload checksum is a 16 bit value.
        uint16_t checksum = 0;
        bool goodChecksum = true;
        checksum = IswChecksum(iswSolNetMsgPkt->messageLength, (uint16_t *)iswSolNetMsgPkt->messagePayload);

        //! If payload falls on 16-bit boundary checksum is after it
        //! Else there is a pad byte
        //! Move checksum after payload is adjusted for 16-bit boundary
        uint16_t payloadLength = iswSolNetMsgPkt->messageLength;
        if ( (checksum > 0) &&
             (payloadLength < payloadMsgBufferSize) )
        {
            if ( (payloadLength % 16) == 0 )
            {
                if ( checksum != (uint16_t)iswSolNetMsgPkt->messagePayload[payloadLength] )
                {
                    goodChecksum = false;
                }
            }
            else
            {
                if ( checksum != (uint16_t)iswSolNetMsgPkt->messagePayload[payloadLength + 1] )
                {
                    goodChecksum = false;
                }
            }
        }
        else
        {
            if ( checksum != (uint16_t)iswSolNetMsgPkt->payloadChecksum )
            {
                goodChecksum = false;
            }
        }

        if ( !goodChecksum )
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Device " << std::to_string(iswInterface->GetIndex()) << " DeliverSolNetMessagePacket" << std::endl;
                ss << "    Payload Checksum Bad - Not Adding Service!" << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            return;
        }
    }

    switch ( iswSolNetMsgPkt->messageClass )
    {
        case Discovery:
        {
            switch ( iswSolNetMsgPkt->messageId )
            {
                case BrowseServices:
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Discovery - BrowseServices from Peer = " << std::to_string(peerIndex) << std::endl;
                        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }

                    if ( peerIndex != defaultPeerIndex )
                    {
                        //! Let send event thread know to send an advertise message
                        SetAdvertise(peerIndex, true);
                    }
                    break;
                }
                case ServiceAdvertiseAnnounce:
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Discovery - ServiceAdvertiseAnnounce from Peer = " << std::to_string(peerIndex) << std::endl;
                        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }

                    ParseSolNetAdvertiseAnnounceMessage(peerIndex, iswSolNetMsgPkt);
                    break;
                }
                case ServiceAdvertiseChange:
                {
                    //! Remove peer services
                    RemoveAllServicesForOnePeer(peerIndex, false);

                    //! Send Browse again to this peer - they've changed their services
                    SetBrowse(peerIndex, true);

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Discovery - ServiceAdvertiseChange" << std::endl;
                        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }
                    break;
                }
                default:
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        msgString = " Bad Discovery messageId";
                        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                    }
                    break;
                }
            }

            if ( (iswSolNetMsgPkt->messageId != Acknowledgement) && (iswSolNetMsgPkt->policies & POLICY_ACK) == POLICY_ACK )
            {
                if ( (iswSolNetPeerServices[peerIndex].inUse) && (iswSolNetPeerServices[peerIndex].acksToSendList != nullptr) )
                {
                    //! Add to ack list
                    iswSolNetAckPacket *ackPkt = new iswSolNetAckPacket;
                    ackPkt->endpoint           = 0;
                    ackPkt->dataflowId         = 0;                             //! Not used for Message Packets
                    ackPkt->seqNumber          = iswSolNetMsgPkt->seqNumber;
                    ackPkt->status             = 0;                             //! Ack = 0, Nak = 1
                    ackPkt->protocolClass      = 1;                             //! data = 0, message = 1
                    ackPkt->reserved           = 0;                             //! Use the reserved field to pass peerIndex internally

                    iswSolNetPeerServices[peerIndex].peerLock.lock();
                    iswSolNetPeerServices[peerIndex].acksToSendList->push_back(ackPkt);
                    iswSolNetPeerServices[peerIndex].peerLock.unlock();
                }
            }
            break;
    }
    case Flow:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "Flow Message";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        switch ( iswSolNetMsgPkt->messageId )
        {
            case GetStatusRequest:
            {
                uint32_t serviceSelector = 0;
                memcpy(&serviceSelector, iswSolNetMsgPkt->messagePayload, selectorSize);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - GetStatusRequest from Peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceSelector = " << std::to_string(serviceSelector) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                //! Indicate to the background thread to send a response
                SetSendGetStatusResponse(peerIndex, serviceSelector, true, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case RegisterRequest:
            {
                //! Get the RegistrationRequest message
                iswSolNetRegisterRequest *regRequest = (iswSolNetRegisterRequest *)iswSolNetMsgPkt->messagePayload;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - RegistrationRequest from Peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceSelector = " << std::to_string(regRequest->serviceSelector) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                UpdateServiceWithPeerRegisterRequest(peerIndex, regRequest, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case KeepAliveRequest:
            {
                //! Get the KeepAliveRequest Message
                iswSolNetKeepAliveRequest *keepAliveRequest = (iswSolNetKeepAliveRequest *)iswSolNetMsgPkt->messagePayload;

                if( theLogger->DEBUG_IswSolNet == 1)
                {
                    std::stringstream ss;
                    ss << "Flow - KeepAliveRequest from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceSelector = " << std::to_string(keepAliveRequest->serviceSelector) << std::endl;
                    ss << "       initiator       = " << std::to_string(keepAliveRequest->initiator) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                UpdatePeerServiceWithKeepAliveInitiator(peerIndex, keepAliveRequest, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case DeregisterRequest:
            {
                //! Get the DeregistrationRequest message
                uint32_t serviceSelector = 0;
                memcpy(&serviceSelector, &iswSolNetMsgPkt->messagePayload[0], selectorSize);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - DeregistrationRequest from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceSelector = " << std::to_string(serviceSelector) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                UpdateServiceWithPeerDeregisterRequest(peerIndex, serviceSelector, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case RevokeRegistration:
            {
                //! Get the RevokeRegistration message
                uint32_t *serviceSelector = (uint32_t *)iswSolNetMsgPkt->messagePayload;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - RevokeRegistration from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceSelector = " << std::to_string(*serviceSelector) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                UpdatePeerServiceWithRevokeRegistration(peerIndex, *serviceSelector, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case PollDataRequest:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - PollDataRequest from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                //! Get the PollDataRequest message
                uint32_t *serviceSelector = (uint32_t *)iswSolNetMsgPkt->messagePayload;

                UpdateServiceWithPeerPollDataRequest(peerIndex, *serviceSelector, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case AutonomousStartStopRequest:
            {
                //! Get the AutonomousStartStopRequest message from the payload
                iswSolNetAutonomousStartStopRequest *autonomousStartStopMsg = (iswSolNetAutonomousStartStopRequest *)iswSolNetMsgPkt->messagePayload;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - AutonomousStartStopRequest from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber        = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       selectorSelector = "  << std::to_string(autonomousStartStopMsg->serviceSelector) << std::endl;
                    ss << "       flowState        = "  << std::to_string(autonomousStartStopMsg->flowState) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                UpdateServiceWithPeerFlowState(peerIndex, autonomousStartStopMsg, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case GetStatusResponse:
            {
                iswSolNetGetStatusResponse *getStatusResponse = (iswSolNetGetStatusResponse *)iswSolNetMsgPkt->messagePayload;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Flow - GetStatusResponse from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber     = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    ss << "       serviceStatus = " << std::to_string(getStatusResponse->serviceStatus) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                //! Find the saved request message this response matches and remove it
                //! Get the serviceSelector number from the request
                uint32_t serviceSelector = 0;
                iswSolNetMsgRespListLock.lock();
                for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
                {
                    iswSolNetHeader *header = *itr;
                    if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                    {
                        //! The response is answering this request
                        memcpy(&serviceSelector, (uint32_t *)&header->iswSolNetMessagePkt.messagePayload[0], selectorSize);
                        //! Delete the message resources and remove from the list
                        delete (header);
                        itr = iswSolNetMessageResponseList.erase(itr);
                        break;
                    }
                }
                iswSolNetMsgRespListLock.unlock();

                if ( serviceSelector != 0 )
                {
                    //! Set the ImRegistered flag for this service from this peer
                    iswSolNetPeerServices[peerIndex].peerLock.lock();
                    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
                    {
                        //! Don't need to check serviceControlDesc because the serviceSelector field is in the same memory location
                        //! for both types of descriptors
                        if ( iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector )
                        {
                            //! Update status for this service from this peer
                            switch (getStatusResponse->serviceStatus)
                            {
                                case RegStatusUnavailable:
                                {
                                    iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusUnavailable;
                                    break;
                                }
                                case RegStatusAvailableToRegister:
                                {
                                    iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusAvailableToRegister;
                                    break;
                                }
                                case RegStatusAvailableForYourUseOnly:
                                {
                                    iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusAvailableForYourUseOnly;
                                    break;
                                }
                                case RegStatusUnavailableForRegistration:
                                {
                                    iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusUnavailableForRegistration;
                                    break;
                                }
                                case RegStatusAvailableForUseYouAndOthers:
                                {
                                    iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusAvailableForUseYouAndOthers;
                                break;
                            }
                            default:
                            {
                                iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusUnavailable;
                                break;
                            }
                        }
                        break;
                    }
                }
                iswSolNetPeerServices[peerIndex].peerLock.unlock();
            }
            break;
        }
        case RegisterResponse:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - RegistrationResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Find the saved request message this response matches and remove it
            //! Get the serviceSelector number from the request
            uint32_t serviceSelector = 0;
            uint8_t autonomy         = 0;

            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    iswSolNetRegisterRequest *regReqMsg = (iswSolNetRegisterRequest *)&header->iswSolNetMessagePkt.messagePayload[0];
                    memcpy(&serviceSelector, &regReqMsg->serviceSelector, selectorSize);
                    autonomy = regReqMsg->autonomy;

                    //! Delete the message resources and remove from the list
                    delete (header);
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();

            // Have to allow 0, emulator starts service selector at 0.
            if ( serviceSelector >= 0 )
            {
                //! Set the ImRegistered flag for this service from this peer
                bool foundService = false;
                iswSolNetPeerServices[peerIndex].peerLock.lock();
                for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
                {
                    if ( !iswSolNetPeerServices[peerIndex].services[dataflowId].inUse )
                    {
                        continue;
                    }

                    //! Don't need to check serviceControlDesc because the serviceSelector field is in the same memory location
                    //! for both types of descriptors
                    if ( iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector )
                    {
                        if ( iswSolNetMsgPkt->status == MsgSuccess )
                        {
                            iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered = true;
                            iswSolNetPeerServices[peerIndex].services[dataflowId].autonomy     = autonomy;
                        }
                        else
                        {
                            iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered = false;
                        }
                        foundService = true;
                        break;
                    }
                }
                iswSolNetPeerServices[peerIndex].peerLock.unlock();

                //! Debug message if we didn't find the service
                if ( (theLogger->DEBUG_IswSolNet == 1) && (!foundService) )
                {
                    std::stringstream ss;
                    ss << "    Couldn't find serviceSelector  = " << std::to_string(serviceSelector) << std::endl;
                    ss << "    for RegisterResponse seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "    Couldn't find RegisterRequest seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case DeregisterResponse:
        {
            //! Find the saved request message this response matches and remove it
            //! Get the serviceSelector number from the request
            uint32_t serviceSelector = 0;
            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    memcpy(&serviceSelector, &header->iswSolNetMessagePkt.messagePayload[0], selectorSize);
                    //! Delete the message resources and remove from the list
                    delete (header);
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - DeregistrationResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                ss << "       serviceSelector = " << std::to_string(serviceSelector) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            if ( serviceSelector >= 0 )
            {
                //! Unset the ImRegistered flag for this service from this peer
                iswSolNetPeerServices[peerIndex].peerLock.lock();
                for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
                {
                    if ( !iswSolNetPeerServices[peerIndex].services[dataflowId].inUse )
                    {
                        continue;
                    }
                    //! Don't need to check serviceControlDesc because the serviceSelector field is in the same memory location
                    //! for both types of descriptors
                    if ( iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector )
                    {
                        iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered = false;
                        break;
                    }
                }
                iswSolNetPeerServices[peerIndex].peerLock.unlock();
            }
            break;
        }
        case RevokeRegistrationResponse:
        {
            //! Find the saved request message this response matches and remove it
            uint32_t serviceSelector = 0;
            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    memcpy(&serviceSelector, &header->iswSolNetMessagePkt.messagePayload[0], selectorSize);

                    //! Delete the message resources and remove from the list
                    delete (header);
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - RevokeRegistrationResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber       = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                ss << "       serviceSelector = " << std::to_string(serviceSelector) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case PollDataResponse:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - PollDataResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber     = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                ss << "       messageLength = " << std::to_string(iswSolNetMsgPkt->messageLength) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Find the saved request message this response matches and remove it
            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();

            break;
        }
        case AutonomousStartStopResponse:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - AutonomousStartStopResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Find the saved request message this response matches and remove it
            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();
            break;
        }
        case KeepAliveResponse:
        {
            iswSolNetKeepAliveResponse *keepAliveResponse = (iswSolNetKeepAliveResponse *)iswSolNetMsgPkt->messagePayload;

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Flow - KeepAliveResponse from peer = " << std::to_string(peerIndex) << std::endl;
                ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                ss << "       initiator = " << std::to_string(keepAliveResponse->initiator) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Find the saved request message this response matches and remove it
            iswSolNetMsgRespListLock.lock();
            for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
            {
                iswSolNetHeader *header = *itr;
                if ( header->iswSolNetMessagePkt.seqNumber == iswSolNetMsgPkt->seqNumber )
                {
                    //! The response is answering this request
                    itr = iswSolNetMessageResponseList.erase(itr);
                    break;
                }
            }
            iswSolNetMsgRespListLock.unlock();
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                msgString = " Bad Flow messageId";
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        }
        break;
    }
    case Reporting:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "Reporting Message";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        switch ( iswSolNetMsgPkt->messageId )
        {
            case ReportDataflowConditionIndication:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Low Level Bridge - ReportDataflowCondition from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                break;
            }
            default:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    msgString = " Bad Reporting messageId";
                    iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                break;
            }
        }
        break;
    }
    case LowLevelBridge:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "Low Level Bridge Message";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        switch ( iswSolNetMsgPkt->messageId )
        {
            case RevokeNetworkAssociationRequest:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Low Level Bridge - RevokeNetworkAssociationRequest from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }

                //! Send RevokeNetworkAssociationResponse message
                SetSendRevokeNetworkAssociationResponse(peerIndex, iswSolNetMsgPkt->seqNumber);
                break;
            }
            case RevokeNetworkAssociationResponse:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Low Level Bridge - RevokeNetworkAssociationResponse from peer = " << std::to_string(peerIndex) << std::endl;
                    ss << "       seqNumber = " << std::to_string(iswSolNetMsgPkt->seqNumber) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                break;
            }
            default:
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    msgString = " Bad Low Level Bridge messageId";
                    iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                break;
            }
        }
        break;
    }
    case Policy:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "Policy Message";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        switch ( iswSolNetMsgPkt->messageId )
        {
        case Acknowledgement:
        {
            iswSolNetAckPacket *ackPkt = (iswSolNetAckPacket *)iswSolNetMsgPkt->messagePayload;
            uint8_t	dataflowId = ackPkt->dataflowId & iswSolNetProviderConsumerMask;

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Device " << std::to_string(iswInterface->GetIndex()) << " receiving Policy - Acknowledgement" << std::endl;
                ss << "    endpoint      = " << std::to_string(ackPkt->endpoint) << std::endl;
                ss << "    dataflowId    = " << std::to_string(ackPkt->dataflowId ) << std::endl;
                ss << "    seqNumber     = " << std::to_string(ackPkt->seqNumber) << std::endl;
                ss << "    status        = " << std::to_string(ackPkt->status) << std::endl;
                ss << "    protocolClass = " << std::to_string(ackPkt->protocolClass) << std::endl;
                ss << "    reserved      = " << std::to_string(ackPkt->reserved) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                //std::cout << ss.str();
            }

            bool ImAProducer = true;

            //! If this ACK packet is acking a packet that had the consumer bit sent
            //! then I'm the consumer - I sent a packet to the peer with the consumer bit set
            if ( (ackPkt->dataflowId & iswSolNetSetConsumerBit) == iswSolNetSetConsumerBit )
            {
                ImAProducer = false;
            }
            else
            {
                //! Im the producer
                ImAProducer = true;
            }


            // TBD - check retransmit queue for packet that is being acked
            if ( ImAProducer )
            {
                //! The original data packet was sent from us - we are the producer
                //! So check our services retransmit list
                //! Find the packet with matching seqNumber in the retransmit list
                for( auto iterator = iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList->begin();
                     iterator != iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList->end();
                     iterator ++)
                {
                    iswRetransmitItem *retransItem = *iterator;
                    if ( retransItem->iswSolNetHdr.iswSolNetDataPkt.seqNumber == ackPkt->seqNumber )
                    {
                        //! Found it!
                        delete (retransItem);
                        iterator = iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList->erase(iterator);

                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            std::stringstream ss;
                            ss << " Removed data packet from registeredPeers retransmit list" << std::endl;
                            theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                        }
                        break;
                    }
                }
            }
            else
            {
                //! For some reason (app's protocol?) we sent a data packet to the producer
                //! though we are the consumer.  The Peer is the producer, so the retransmit
                //! list is on the Peer's services
                for( auto iterator = iswSolNetPeerServices[peerIndex].retransmitList->begin();
                     iterator != iswSolNetPeerServices[peerIndex].retransmitList->end();
                     iterator ++)
                {
                    iswRetransmitItem *retransItem = *iterator;
                    if ( retransItem->iswSolNetHdr.iswSolNetDataPkt.seqNumber == ackPkt->seqNumber )
                    {
                        //! Found it!
                        delete (retransItem);
                        iterator = iswSolNetPeerServices[peerIndex].retransmitList->erase(iterator);

                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            std::stringstream ss;
                            ss << " Removed data packet from Peers retransmit list" << std::endl;
                            theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                            //std::cout << "Device " << std::to_string(GetIndex()) << ss.str();
                        }
                        break;
                    }
                }
            }

            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                msgString = " Bad Policy messageId";
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        }
        break;
    }
    case UsbHost:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "UsbHost Message";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        break;
    }
    default:
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = " Bad messageClass";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        break;
    }
    }
}

//!#############################################################
//! StopIswSolNetHandleSendEventsThread()
//! Inputs: None
//! Description: Sets a flag indicating the thread that handles
//!              SolNet send events should stop
//! #############################################################
void IswSolNet::StopIswSolNetHandleSendEventsThread(void)
{
    iswSolNetCmdLock.lock();
    checkingSolNetEvents = false;
    iswSolNetCmdLock.unlock();
}

//!#############################################################
//! StartIswSolNetHandleSendEventsThread()
//! Inputs: None
//! Description: Sets a flag indicating the thread that handles
//!              SolNet send events should start
//! #############################################################
void IswSolNet::StartIswSolNetHandleSendEventsThread(void)
{
    iswSolNetCmdLock.lock();
    checkingSolNetEvents = true;
    iswSolNetCmdLock.unlock();
}

//!#############################################################
//! StopSolNet()
//! Inputs: None
//! Description: Cleans up all before shutdown. Removes all
//!              services this device offers and also removes all
//!              peer services received from other devices.
//!              Stops all threads used to handle SolNet events.
//! #############################################################
void IswSolNet::StopSolNet()
{
    solNetStopped = true;
    sleep(1);  //!So apps can see we are shutting down

    EmptyMsgSaveList();

#ifndef TESTING  //!Don't run during unit tests
    if ( threadCheckSolNetSendEvents != 0 )
    {
        //! Stop the thread that is checking for send events
        StopIswSolNetHandleSendEventsThread();

        pthread_join(threadCheckSolNetSendEvents, nullptr);

        std::cout << "Device " << std::to_string(GetIndex()) << " Thread CheckSolNetSendEvents ended" << std::endl;
    }
#endif

    //! Remove or clear the services for this device
    RemoveAllServices(false);

#ifndef TESTING  //!Don't run during unit tests
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
            if ( iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList != nullptr )
            {
                delete(iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList);
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList = nullptr;
            }

            if ( iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList != nullptr )
            {
                delete(iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList);
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList = nullptr;
            }
        }
    }
#endif

    //! Remove or clear all services from other peers
    RemoveAllPeerServices(false);

#ifndef TESTING  //!Don't run during unit tests
    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        if ( iswSolNetPeerServices[peerIndex].acksToSendList != nullptr )
        {
            delete(iswSolNetPeerServices[peerIndex].acksToSendList);
            iswSolNetPeerServices[peerIndex].acksToSendList = nullptr;
        }

        if ( iswSolNetPeerServices[peerIndex].retransmitList != nullptr )
        {
            delete(iswSolNetPeerServices[peerIndex].retransmitList);
            iswSolNetPeerServices[peerIndex].retransmitList = nullptr;
        }
    }
#endif
}

//!#################################################################
//! SetRetransmitValues()
//! Inputs: timeout - in seconds
//!         noOfRetran - number of times to retransmit this packet
//! Description: Sets these values used when retransmitting SolNet
//!              data packets, if retransmit is set.
//! #################################################################
void IswSolNet::SetRetransmitValues(uint64_t timeout, uint8_t noOfRetrans)
{
    iswSolNetRetransTimeout = timeout;
    iswSolNetNoOfRetrans = noOfRetrans;
}

//!#####################################################################
//! RemoveOneRegisteredPeer()
//! Inputs: dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         peerIndex  - ISW representation of the peer to this node
//!                      Also thindex into the registered peers table
//!                      per service
//!         clear - true=set ack and retransmit list ptrs  to null
//!                 false=Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//!  Description: Set a peer service for a SolNet service to default
//!               values - not in use
//!#####################################################################
void IswSolNet::RemoveOneRegisteredPeer(uint8_t dataflowId, uint8_t peerIndex, bool clear)
{
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].peerRegistered                    = false;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].status                            = RegStatusAvailableToRegister;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomy                          = 0;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].flowState                         = 0;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendRegRequestResponse            = false;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].regRequestResponseSeqNo           = 0;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendDeregistrationRequestResponse = false;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].deregRequestResponseSeqNo         = 0;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendAutonomousStartStopResponse   = false;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomousStartStopSeqNo          = 0;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendGetStatusResponse             = false;
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].getStatusResponseSeqNo            = 0;

    if ( clear )
    {
       iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList = nullptr;
       iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList = nullptr;
    }
    else
    {
        if ( iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList != nullptr )
        {
            EmptyAcksToSendList(iswSolNetServices[dataflowId].registeredPeers[peerIndex].acksToSendList);
        }

        if ( iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList != nullptr )
        {
            EmptyRetransmitList(iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList);
        }
    }
    iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
}

//!#####################################################################
//! RemoveOneService()
//! Inputs: dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         clear - true=set ack and retransmit list ptrs  to null
//!                 false=Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//! Description: Set a service in the SolNet services table to
//!              default values - not in use
//!#####################################################################
void IswSolNet::RemoveOneService(uint8_t dataflowId, bool clear)
{
    iswSolNetServicesLock.lock();
    iswSolNetServiceEntry *serviceEntry = &iswSolNetServices[dataflowId];

    if ( clear )
    {
        serviceEntry->serviceDesc.header.length       = 0;
        serviceEntry->serviceDesc.header.descriptorId = 0;
        serviceEntry->serviceDesc.serviceSelector     = 0;
        serviceEntry->serviceDesc.childBlock          = nullptr;
    }
    else
    {
        if ( serviceEntry->serviceDesc.header.descriptorId == Control )
        {
            if ( serviceEntry->serviceControlDesc.childBlock != nullptr )
            {
                delete (serviceEntry->serviceControlDesc.childBlock);
                serviceEntry->serviceControlDesc.childBlock = nullptr;
            }
            serviceEntry->serviceControlDesc.header.length       = 0;
            serviceEntry->serviceControlDesc.header.descriptorId = 0;
            serviceEntry->serviceControlDesc.serviceSelector     = 0;
        }
        else
        {
            if ( serviceEntry->serviceDesc.childBlock != nullptr )
            {
                delete (serviceEntry->serviceDesc.childBlock);
                serviceEntry->serviceDesc.childBlock = nullptr;
            }
            serviceEntry->serviceDesc.header.length       = 0;
            serviceEntry->serviceDesc.header.descriptorId = 0;
            serviceEntry->serviceDesc.serviceSelector     = 0;
        }
    }
    serviceEntry->inUse                 = false;
    serviceEntry->dataflowId            = dataflowId;
    serviceEntry->endpointId            = 0;
    serviceEntry->dataPolicies          = 0;
    serviceEntry->endpointDistribution  = 0;
    serviceEntry->nextSendDataSeqNumber = 0;
    serviceEntry->receiveCallbackFn     = nullptr;
    serviceEntry->pollDataCallbackFn    = nullptr;

    //! Set the registered peers to default values
    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        RemoveOneRegisteredPeer(dataflowId, peerIndex, clear);
    }
    iswSolNetServicesLock.unlock();
}

//!#####################################################################
//! RemoveAllServices()
//! Inputs: clear - true=set ack and retransmit list ptrs  to null
//!                 false=Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//! Description: Set all services in the SolNet services table to
//!              default values - not in use
//!#####################################################################
void IswSolNet::RemoveAllServices(bool clear)
{
    //! Initialize the services for this device
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        RemoveOneService(dataflowId, clear);
    }
}

//!#####################################################################
//! RemoveOneServiceForOnePeer()
//! Inputs: peerIndex  - ISW representation of the peer to this node.
//!                      Also the index into the registered peers table
//!                      per service.
//!         dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         clear - true=set ack and retransmit list ptrs  to null
//!                 false=Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//! Description: Set one peer service in the SolNet registered peer
//!              services table to default values - not in use
//!#####################################################################
void IswSolNet::RemoveOneServiceForOnePeer(uint8_t peerIndex, uint8_t dataflowId, bool clear)
{
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServiceEntry *entry = &iswSolNetPeerServices[peerIndex].services[dataflowId];

    if ( clear )
    {
        entry->serviceDesc.header.length       = 0;
        entry->serviceDesc.header.descriptorId = 0;
        entry->serviceDesc.serviceSelector     = 0;
        entry->serviceDesc.childBlock          = nullptr;
    }
    else
    {
        //! Cleanup the childblock resources
        if ( entry->serviceDesc.header.descriptorId == Control )
        {
            if ( entry->serviceControlDesc.childBlock != nullptr )
            {
                delete (entry->serviceControlDesc.childBlock);
                entry->serviceControlDesc.childBlock = nullptr;
            }
            entry->serviceControlDesc.header.length       = 0;
            entry->serviceControlDesc.header.descriptorId = 0;
            entry->serviceControlDesc.serviceSelector     = 0;
        }
        else
        {
            if ( entry->serviceDesc.childBlock != nullptr )
            {
                delete (entry->serviceDesc.childBlock);
                entry->serviceDesc.childBlock = nullptr;
            }
            entry->serviceDesc.header.length       = 0;
            entry->serviceDesc.header.descriptorId = 0;
            entry->serviceDesc.serviceSelector     = 0;
        }
    }
    entry->inUse                 = false;
    entry->ImRegistered          = false;
    entry->dataflowId            = dataflowId;
    entry->endpointId            = 0;
    entry->dataPolicies          = 0;
    entry->endpointDistribution  = 0;
    entry->autonomy              = 0;
    entry->status                = RegStatusAvailableToRegister;
    entry->nextSendDataSeqNumber = 0;
    entry->receiveCallbackFn     = nullptr;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();
}

//!#####################################################################
//! RemoveAllServicesForOnePeer()
//! Inputs: peerIndex - ISW representation of the peer to this node.
//!                     Also the index into the registered peers table
//!                     per service.
//!         clear - true = set ack and retransmit list ptrs  to null
//!                 false = Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//! Description: Set all services for a peer in the SolNet registered
//!              peer services table to default values - not in use
//!#####################################################################
void IswSolNet::RemoveAllServicesForOnePeer(uint8_t peerIndex, bool clear)
{
    //! Run through this peers services and delete resources
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        RemoveOneServiceForOnePeer(peerIndex, dataflowId, clear);
    }
    //! Clear this peer entry
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].inUse                          = false;
    iswSolNetPeerServices[peerIndex].sendBrowse                     = false;
    iswSolNetPeerServices[peerIndex].sendAdvertise                  = false;
    iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse = false;
    iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo  = 0;

    //! Don't change these fields if we are in RevokeNetworkAssociation
    if (iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse == false )
    {
        iswSolNetPeerServices[peerIndex].revokeNetworkAssociationResponseSeqNo = 0;
    }

    if ( clear )
    {
        iswSolNetPeerServices[peerIndex].acksToSendList = nullptr;
        iswSolNetPeerServices[peerIndex].retransmitList = nullptr;
    }
    else
    {
        if ( iswSolNetPeerServices[peerIndex].acksToSendList != nullptr )
        {
            EmptyAcksToSendList(iswSolNetPeerServices[peerIndex].acksToSendList);
        }

        if ( iswSolNetPeerServices[peerIndex].retransmitList != nullptr )
        {
            EmptyRetransmitList(iswSolNetPeerServices[peerIndex].retransmitList);
        }
    }
    iswSolNetPeerServices[peerIndex].peerLock.unlock();
}

//!#####################################################################
//! RemoveAllPeerServices()
//! Inputs: peerIndex - ISW representation of the peer to this node.
//!                     Also the index into the registered peers table
//!                     per service.
//!         clear - true = set ack and retransmit list ptrs  to null
//!                 false=Empty the ack and retransmit lists first
//!                 true is used to speed up shutdown and clear is used
//!                 on startup when we know none of the pointers have
//!                 been allocated in case their value is not zero.
//!                 This fixes some crashes people saw on restart
//! Description: Set all services for all peers in the SolNet registered
//!              peer services table to default values - not in use
//!#####################################################################
void IswSolNet::RemoveAllPeerServices(bool clear)
{
    //! Initialize the peer services
    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        RemoveAllServicesForOnePeer(peerIndex, clear);
    }
}

//!#####################################################################
//! EmptyMsgSaveList()
//! Inputs: None
//! Description: Empty the list that holds messages that are waiting
//!              a SolNet response per the ISW SolNet specification
//!#####################################################################
void IswSolNet::EmptyMsgSaveList(void)
{
    if ( !iswSolNetMessageResponseList.empty() )
    {
        iswSolNetMsgRespListLock.lock();
        for (auto itr = iswSolNetMessageResponseList.begin(); itr != iswSolNetMessageResponseList.end(); itr++)
        {
            delete (*itr);
            itr = iswSolNetMessageResponseList.erase(itr);
        }
        iswSolNetMsgRespListLock.unlock();
    }
}

//!#####################################################################
//! EmptyAcksToSendList()
//! Inputs: None
//! Description: Empty the list that holds acknowledgements that need
//!              to be sent
//!#####################################################################
void IswSolNet::EmptyAcksToSendList(std::list<iswSolNetAckPacket *> *acksToSendList)
{
    //! For some reason in Qt unit tests - garbage is in list to seem like there are pointers
#ifndef TESTING
    while ( acksToSendList->empty() == false )
    {
        IswSolNet::iswSolNetAckPacket *ackPkt = acksToSendList->front();
        delete(ackPkt);
        acksToSendList->pop_front();
    }

    delete (acksToSendList);
#endif
}

//!#####################################################################
//! EmptyRetransmitList()
//! Inputs: None
//! Description: Empty the list that holds messages that need
//!              to be retransmitted
//!#####################################################################
void IswSolNet::EmptyRetransmitList(std::list<iswRetransmitItem *> *retransmitList)
{
    //! For some reason in Qt unit tests - garbage is in list to seem like there are pointers
#ifndef TESTING
    while ( retransmitList->empty() == false )
    {
        iswRetransmitItem *retransItem = retransmitList->front();
        delete(retransItem);
        retransmitList->pop_front();
    }

    delete (retransmitList);
#endif
}

//!#####################################################################
//! GetNextDataflowId()
//! Inputs: None
//! Description: This SolNet class owns and manages the dataflowId. The
//!              application uses this to get the next unique one. It
//!              is part of the SolNet service structure, but this
//!              implementation also uses it as the index into the SolNet
//!              services table because it is unique per service.
//!#####################################################################
uint8_t IswSolNet::GetNextDataflowId(void)
{
    iswSolNetServicesLock.lock();
    //! Return the global next nextDataflowId number
    iswSolNetNextDataflowId++;

    if ( iswSolNetNextDataflowId == iswSolNetProviderConsumerMask )
    {
        iswSolNetNextDataflowId = 0;
    }

    uint8_t nextDataflowId = iswSolNetNextDataflowId;
    iswSolNetServicesLock.unlock();

    return (nextDataflowId);
}

//!#####################################################################
//! DecrementDataflowId()
//! Inputs: None
//! Description: Decrement DataflowId
//! When a service is removed from the service descritor plan list,
//! make its dataflowId available again
//!#####################################################################
uint8_t IswSolNet::DecrementDataflowId(void)
{
    iswSolNetServicesLock.lock();

    //! Return the global previous nextDataflowId number
    iswSolNetNextDataflowId--;

    if ( iswSolNetNextDataflowId == iswSolNetProviderConsumerMask )
    {
        iswSolNetNextDataflowId = 0;
    }

    uint8_t nextDataflowId = iswSolNetNextDataflowId;
    iswSolNetServicesLock.unlock();

    return (nextDataflowId);
}

//!#####################################################################
//! GetNextServiceNo()
//! Inputs: None
//! Description: The combination of dataflowId, serviceSelectorNo, and
//!              peerIndex is unique.  This implementation of SolNet
//!              doesn't really use the serviceSelectorNo but we manage
//!              and track it. The user calls this method to get a
//!              unique number.
//!#####################################################################
uint32_t IswSolNet::GetNextServiceNo(void)
{
    iswSolNetServicesLock.lock();
    //! Return the global next serviceSelector number
    if ( iswSolNetNextServiceSelectorNo == 0 )
    {
        iswSolNetNextServiceSelectorNo = 1;
    }
    else
    {
        iswSolNetNextServiceSelectorNo++;
    }

    uint32_t nextServiceNo = iswSolNetNextServiceSelectorNo;
    iswSolNetServicesLock.unlock();

    return (nextServiceNo);
}

//!#####################################################################
//! GetNextMsgSeqNo()
//! Inputs: None
//! Description: Incremented when sending SolNet message packets.
//!#####################################################################
uint8_t IswSolNet::GetNextMsgSeqNo(void)
{
    iswSolNetCmdLock.lock();
    uint8_t nextMsgSeqNo = iswSolNetMessageSeqNo;
    iswSolNetMessageSeqNo++;
    iswSolNetCmdLock.unlock();

    return (nextMsgSeqNo);
}

//!#####################################################################
//! GetNextDataSeqNo()
//! Inputs: None
//! Description: Incremented when sending SolNet data packets
//!#####################################################################
uint8_t IswSolNet::GetNextDataSeqNo(uint8_t dataflowId)
{
    uint8_t nextDataSeqNo = 0;

    iswSolNetServicesLock.lock();
    nextDataSeqNo = iswSolNetServices[dataflowId].nextSendDataSeqNumber;
    iswSolNetServices[dataflowId].nextSendDataSeqNumber++;
    iswSolNetServicesLock.unlock();

    return (nextDataSeqNo);
}

//!#####################################################################
//! RegisterReceiveDataCallbackFunction()
//! Inputs: peerIndex  - ISW representation of the peer to this node
//!         dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         callbackFn - pointer to function to call when data is
//!                      received from the peer for this service
//!         thisPtr    - the thread relating to the callback function
//! Description: This function will be called if the entry in the service
//!              table is not null when data is received from a peer for
//!              this service. Used when the service is NOT in polled mode.
//!#####################################################################
void IswSolNet::RegisterReceiveDataCallbackFunction(uint8_t peerIndex, uint8_t dataflowId, iswSolNetReceiveDataCallbckFunction callbackFn, void *thisPtr)
{
    if ( callbackFn != nullptr)
    {
        if ( peerIndex == defaultPeerIndex )
        {
            for (uint8_t i = 0; i < MAX_PEERS; i++)
            {
                iswSolNetPeerServices[i].peerLock.lock();
                iswSolNetPeerServices[i].services[dataflowId].receiveCallbackFn  = callbackFn;
                iswSolNetPeerServices[i].services[dataflowId].recCallbackThisPtr = thisPtr;
                iswSolNetPeerServices[i].peerLock.unlock();
            }
        }
        else
        {
            iswSolNetPeerServices[peerIndex].peerLock.lock();
            iswSolNetPeerServices[peerIndex].services[dataflowId].receiveCallbackFn  = callbackFn;
            iswSolNetPeerServices[peerIndex].services[dataflowId].recCallbackThisPtr = thisPtr;
            iswSolNetPeerServices[peerIndex].peerLock.unlock();
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "ReceiveDataCallbackFn ptr is nullptr" << std::endl;
            theLogger->LogMessage(ss.str(), GetIndex(), std::chrono::system_clock::now());
        }
    }
}

//!#####################################################################
//! RegisterPollDataCallbackFunction()
//! Inputs: dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         callbackFn - pointer to function to call when data is
//!                      received from the peer for this service
//! Description: This function will be called if the entry in the service
//!              table is not null when data is received from a peer for
//!              this service. Used when the service is in polled mode.
//!#####################################################################
void IswSolNet::RegisterPollDataCallbackFunction(uint8_t dataflowId, iswSolNetPollDataCallbckFn callbackFn)
{
    if ( callbackFn != nullptr)
    {
        iswSolNetServicesLock.lock();
        iswSolNetServices[dataflowId].pollDataCallbackFn = callbackFn;
        iswSolNetServicesLock.unlock();
    }
    else
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "RegisterPollDataCallbackFn ptr is nullptr" << std::endl;
            theLogger->LogMessage(ss.str(), GetIndex(), std::chrono::system_clock::now());
        }
    }
}

//!###########################################################################
//! RegisterClearAssociationCallbackFunction()
//! Inputs: callbackFn - pointer to function to call when the Clear Association
//!                      is going to be issued by the application
//! Description: This function could be called when Clear Network Association
//!              is issued for a device so that applications can be
//!              handled.
//!###########################################################################
void IswSolNet::RegisterClearAssociationCallbackFunction(iswSolNetClearNetworkAssociationClbckFn callbackFn)
{
    if ( callbackFn != nullptr)
    {
        clearNetworkAssocCallbackFn = callbackFn;
    }
    else
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "ClearAssociationCallbackFn ptr is nullptr" << std::endl;
            theLogger->LogMessage(ss.str(), GetIndex(), std::chrono::system_clock::now());
        }
    }
}

//!###########################################################################
//! ClearNetworkAssociation()
//! Inputs: None
//! Description: This method handles all that must be done when a Clear Network
//!              Association is issued by the user or application.
//!###########################################################################
int IswSolNet::ClearNetworkAssociation(void)
{
    //! Call callback function so application can shutdown data streams
    if ( clearNetworkAssocCallbackFn != nullptr)
    {
        clearNetworkAssocCallbackFn(this);
    }

    //! Clear All peer services
    RemoveAllPeerServices(false);

    //! Set all peers as not registered for our services but leave our services
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
            RemoveOneRegisteredPeer(dataflowId, peerIndex, false);
        }
    }

    //! Remove all items on the message save list
    EmptyMsgSaveList();

    //! Issue ClearAssociation and RadioReset commands
    IswAssociation *iswAssociation = iswInterface->GetIswAssociation();
    IswSystem *iswSystem           = iswInterface->GetIswSystem();
    IswStream *iswStream           = iswInterface->GetIswStream();
    int status                     = 0;

    status = iswAssociation->SendClearAssociationCmd();

    //! Reset the radio to force disconnect
    status = iswSystem->SendResetRadioCmd();
    usleep(10);

    return (status);
}

std::string IswSolNet::getProviderDevName() const
{
    return providerDevName;
}

void IswSolNet::setProviderDevName(const std::string &value)
{
    providerDevName = value;
}

float_t IswSolNet::getGenerationId() const
{
    return generationId;
}

uint8_t IswSolNet::getMaxInflightSolNetMessages() const
{
    return maxInflightSolNetMessages;
}

void IswSolNet::setMaxInflightSolNetMessages(const uint8_t &value)
{
    maxInflightSolNetMessages = value;
}

uint8_t IswSolNet::getSolNetMessageRetries() const
{
    return solNetMessageRetries;
}

void IswSolNet::setSolNetMessageRetries(const uint8_t &value)
{
    solNetMessageRetries = value;
}

uint8_t IswSolNet::getRequestTimeout() const
{
    return requestTimeout;
}

void IswSolNet::setRequestTimeout(const uint8_t &value)
{
    requestTimeout = value;
}

uint8_t IswSolNet::getReportPeriod() const
{
    return reportPeriod;
}

void IswSolNet::setReportPeriod(const uint8_t &value)
{
    reportPeriod = value;
}

uint8_t IswSolNet::getKeepAlivePeriod() const
{
    return keepAlivePeriod;
}

uint16_t IswSolNet::getBatteryDataTupleId() const
{
    return batteryDataTupleId;
}

uint16_t IswSolNet::getBatteryDataLength() const
{
    return batteryDataLength;
}

uint16_t IswSolNet::getTargetDataTuple() const
{
    return targetDataTupleId;
}

uint16_t IswSolNet::getTargetDataLength() const
{
    return targetDataLength;
}

uint16_t IswSolNet::getRangeControlTupleId() const
{
    return rangeControlDataTupleId;
}

uint16_t IswSolNet::getRangeControlLength() const
{
    return rangeControlDataLength;
}

uint8_t IswSolNet::getBatteryId() const
{
    return batteryId;
}

uint8_t IswSolNet::getBatteryCharging() const
{
    return batteryCharging;
}

uint8_t IswSolNet::getBatteryState() const
{
    return batteryState;
}

uint8_t IswSolNet::getBatteryLevel() const
{
    return batteryLevel;
}

//!###########################################################################
//! HandleSolNetSendEvents()
//! Inputs: arguments - pointer to user arguments - the iswSolNet object pointer
//! Description: This method runs in a background thread and cycles through
//!              the services to see if anything needs to be done. For instance
//!              if a SolNet Browse message was received then we need to send
//!              an Advertise message with all the services to the peer.
//!###########################################################################
void *IswSolNet::HandleSolNetSendEvents(void *arguments)
{
    IswSolNet *iswSolNet = (IswSolNet *)arguments;

    int count = 0;
    while ( !iswSolNet->checkingSolNetEvents && (count < 1000) )
    {
        usleep(10);
        count++;
    }

    if ( iswSolNet->checkingSolNetEvents )
    {
        std::cout << "Device " << std::to_string(iswSolNet->GetIndex()) << " HandleSolNetSendEvents Started" << std::endl;
    }

    while ( iswSolNet->checkingSolNetEvents )
    {
        //! Check peer services for anything to do
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
            if ( !iswSolNet->iswSolNetPeerServices[peerIndex].inUse )
            {
                continue;
            }

            //! This entry is inUse

            //! Don't get lock on this message because we are resetting the device
            if ( iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse == true )
            {
                iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse = false;
                iswSolNet->SendRevokeNetworkAssociationResponseMessage(peerIndex, iswSolNet->iswSolNetPeerServices[peerIndex].revokeNetworkAssociationResponseSeqNo);

                //! We got sent a RevokeNetworkAssociation message so we have to leave the network
                //! Handles everything that needs to get done to leave the network
                iswSolNet->ClearNetworkAssociation();
            }

            //! Get lock on this peer's services
            iswSolNet->iswSolNetPeerServices[peerIndex].peerLock.lock();

            if ( iswSolNet->iswSolNetPeerServices[peerIndex].sendBrowse )
            {
                //! Reset the sendBrowse flag and send
                iswSolNet->iswSolNetPeerServices[peerIndex].sendBrowse = false;
                iswSolNet->SendBrowseMessage(peerIndex);
            }

            if ( iswSolNet->iswSolNetPeerServices[peerIndex].sendAdvertise )
            {
                //! Reset the sendAdvertise flag and send all services
                iswSolNet->iswSolNetPeerServices[peerIndex].sendAdvertise = false;

                for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
                {
                    if ( iswSolNet->iswSolNetServices[dataflowId].inUse )
                    {
                        uint8_t policies = 0;
                        iswSolNet->SendAdvertiseMessage(peerIndex, dataflowId, policies);
                    }
                }
            }

            if ( iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse )
            {
                //! Reset the sendAdvertise flag and send all services
                iswSolNet->iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse = false;
                iswSolNet->SendRevokeRegistrationResponseMessage(peerIndex, iswSolNet->iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo);
                iswSolNet->iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo = 0;
            }

            if ( iswSolNet->iswSolNetPeerServices[peerIndex].acksToSendList != nullptr )
            {
                while ( !iswSolNet->iswSolNetPeerServices[peerIndex].acksToSendList->empty() )
                {
                    IswSolNet::iswSolNetAckPacket *ackPkt = iswSolNet->iswSolNetPeerServices[peerIndex].acksToSendList->front();
                    iswSolNet->SendSolNetAckPacket(peerIndex, ackPkt);
                    iswSolNet->iswSolNetPeerServices[peerIndex].acksToSendList->pop_front();
                }
            }

            //! Now check the list of packets waiting for ACKs to retransmit
            if ( iswSolNet->iswSolNetPeerServices[peerIndex].retransmitList != nullptr )
            {
                while ( !iswSolNet->iswSolNetPeerServices[peerIndex].retransmitList->empty() )
                {
                    IswSolNet::iswRetransmitItem *retransItem = iswSolNet->iswSolNetPeerServices[peerIndex].retransmitList->front();

                    //! Based on user configuration, check for time exceeded on list the retransmit configured # of times
                    uint64_t currentTime = iswSolNet->theLogger->GetTimeStamp();
                    if ( (currentTime - retransItem->timestamp) >  iswSolNet->GetRetransTimeout() )
                    {
                        if ( retransItem->retransCount > iswSolNet->GetNoOfRetrans() )
                        {
                            //! No ACK has been received - we have reached the limit
                            //! delete the packet from the list

                            if ( iswSolNet->theLogger->DEBUG_IswSolNet == 1 )
                            {
                                std::stringstream ss;
                                ss << "Deleting a Packet from the Retransmit List for Peer = " << std::to_string(peerIndex) << std::endl;
                                iswSolNet->theLogger->LogMessage(ss.str(), iswSolNet->GetIndex(), std::chrono::system_clock::now());
                            }

                            delete(retransItem);
                            iswSolNet->iswSolNetPeerServices[peerIndex].retransmitList->pop_front();
                        }
                        else
                        {
                            //! Send the packet out
                            if ( iswSolNet->theLogger->DEBUG_IswSolNet == 1 )
                            {
                                std::stringstream ss;
                                ss << "Sending a Packet from the Retransmit List for Peer = " << std::to_string(peerIndex) << std::endl;
                                iswSolNet->theLogger->LogMessage(ss.str(), iswSolNet->GetIndex(), std::chrono::system_clock::now());
                            }

                            IswInterface::iswCommand *iswCmd = (IswInterface::iswCommand * )&retransItem->iswSolNetHdr;
                            iswSolNet->iswInterface->AddToSendQueue(iswCmd, peerIndex, retransItem->endpoint);
                            retransItem->retransCount++;
                        }
                    }
                }
            }
            //! Release the lock
            iswSolNet->iswSolNetPeerServices[peerIndex].peerLock.unlock();
        }

        //! Check services for anything to send registered peers
        for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
        {
            if ( !iswSolNet->iswSolNetServices[dataflowId].inUse )
            {
                continue;
            }

            iswSolNetServiceEntry *serviceEntry = &iswSolNet->iswSolNetServices[dataflowId];

            for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
            {
                serviceEntry->registeredPeers[peerIndex].registeredPeerLock.lock();

                if ( serviceEntry->registeredPeers[peerIndex].peerRegistered )
                {
                    //! Send a RegisterResponse message if needed
                    if ( serviceEntry->registeredPeers[peerIndex].sendRegRequestResponse )
                    {
                        //! Reset the flag
                        serviceEntry->registeredPeers[peerIndex].sendRegRequestResponse = false;
                        //! Send the messsage
                        iswSolNet->SendRegisterResponseMessage(peerIndex, serviceEntry->registeredPeers[peerIndex].regRequestResponseSeqNo, MsgSuccess);
                        serviceEntry->registeredPeers[peerIndex].regRequestResponseSeqNo = 0;
                    }

                    //! Send an AutonomousStartStopResponse message if needed
                    if ( serviceEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse )
                    {
                        //! Reset the flag
                        serviceEntry->registeredPeers[peerIndex].sendAutonomousStartStopResponse = false;

                        //! Send the messsage
                        int status = iswSolNet->SendAutonomousStartStopResponseMessage(peerIndex,
                                                                          serviceEntry->serviceDesc.serviceSelector,
                                                                          serviceEntry->registeredPeers[peerIndex].flowState,
                                                                          serviceEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo);
                        if(status == 0x80)
                        {
                            std::cout << "Autonomous Operation not Performed; Requesting Node is not Registered for This Service" << std::endl;
                        }

                        serviceEntry->registeredPeers[peerIndex].autonomousStartStopSeqNo = 0;
                    }

                    //! Send an KeepAliveResponse message if needed
                    if ( serviceEntry->registeredPeers[peerIndex].sendKeepAliveResponse)
                    {
                        //! Reset the flag
                        serviceEntry->registeredPeers[peerIndex].sendKeepAliveResponse = false;

                        //! Send the messsage
                        int status = iswSolNet->SendKeepAliveResponseMessage(peerIndex,
                                                                          serviceEntry->serviceDesc.serviceSelector,
                                                                          serviceEntry->registeredPeers[peerIndex].initiator,
                                                                          serviceEntry->registeredPeers[peerIndex].keepAliveResponseSeqNo);

                        serviceEntry->registeredPeers[peerIndex].keepAliveResponseSeqNo = 0;
                    }

                    //! Send GetStatus response if needed
                    if ( serviceEntry->registeredPeers[peerIndex].sendGetStatusResponse )
                    {
                        //! Reset the sendStatus flag and send
                        serviceEntry->registeredPeers[peerIndex].sendGetStatusResponse = false;
                        iswSolNet->SendGetStatusResponseMessage(peerIndex, serviceEntry->registeredPeers[peerIndex].status, serviceEntry->registeredPeers[peerIndex].getStatusResponseSeqNo);
                        serviceEntry->registeredPeers[peerIndex].getStatusResponseSeqNo = 0;
                    }

                    //! Send PollData Response if needed
                    if ( serviceEntry->registeredPeers[peerIndex].sendPollDataResponse )
                    {
                        serviceEntry->registeredPeers[peerIndex].sendPollDataResponse = false;
                        int status = iswSolNet->SendPollDataResponseMessage(peerIndex, serviceEntry->registeredPeers[peerIndex].pollDataResponseSeqNo);

                        if(status == 0x80)
                        {
                            std::cout << "Poll Data Not Performed; Requesting Node Is Not Registered for this Service" << std::endl;
                        }

                        //! Call Poll Data Callback function
                        //! Alert the application to send data to all registered peers
                        if ( serviceEntry->pollDataCallbackFn != nullptr )
                        {
                            serviceEntry->pollDataCallbackFn(dataflowId);
                        }
                    }

                    //! Now check the list of ACKs that need to be sent out
                    if ( serviceEntry->registeredPeers[peerIndex].acksToSendList != nullptr )
                    {
                        while ( !serviceEntry->registeredPeers[peerIndex].acksToSendList->empty() )
                        {
                            IswSolNet::iswSolNetAckPacket *ackPkt = serviceEntry->registeredPeers[peerIndex].acksToSendList->front();
                            iswSolNet->SendSolNetAckPacket(peerIndex, ackPkt);
                            serviceEntry->registeredPeers[peerIndex].acksToSendList->pop_front();
                        }
                    }

                    //! Now check the list of packets waiting for ACKs to retransmit
                    if ( serviceEntry->registeredPeers[peerIndex].retransmitList != nullptr )
                    {
                        while ( !serviceEntry->registeredPeers[peerIndex].retransmitList->empty() )
                        {
                            IswSolNet::iswRetransmitItem *retransItem = serviceEntry->registeredPeers[peerIndex].retransmitList->front();

                            //! Based on user configuration, check for time exceeded on list then retransmit configured # of times
                            uint64_t currentTime = iswSolNet->theLogger->GetTimeStamp();
                            if ( (currentTime - retransItem->timestamp) >  iswSolNet->GetRetransTimeout() )
                            {
                                if (  retransItem->retransCount > iswSolNet->GetNoOfRetrans() )
                                {
                                    //! No ACK has been received - we have reached the limit
                                    //! delete the packet from the list
                                    if ( iswSolNet->theLogger->DEBUG_IswSolNet == 1 )
                                    {
                                        std::stringstream ss;
                                        ss << "Deleting a Packet from the Retransmit List for registeredPeer = " << std::to_string(peerIndex) << std::endl;
                                        iswSolNet->theLogger->LogMessage(ss.str(), iswSolNet->GetIndex(), std::chrono::system_clock::now());
                                    }

                                    delete(retransItem);
                                    serviceEntry->registeredPeers[peerIndex].retransmitList->pop_front();
                                }
                                else
                                {
                                    //! Send the packet out
                                    if ( iswSolNet->theLogger->DEBUG_IswSolNet == 1 )
                                    {
                                        std::stringstream ss;
                                        ss << "Sending a Packet from the Retransmit List for registeredPeer = " << std::to_string(peerIndex) << std::endl;
                                        iswSolNet->theLogger->LogMessage(ss.str(), iswSolNet->GetIndex(), std::chrono::system_clock::now());
                                    }

                                    IswInterface::iswCommand *iswCmd = (IswInterface::iswCommand * )&retransItem->iswSolNetHdr;
                                    iswSolNet->iswInterface->AddToSendQueue(iswCmd, peerIndex, retransItem->endpoint);
                                    retransItem->retransCount++;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //! Send a RegisterResponse message with failure status - peer wasn't allowed to register
                    if ( serviceEntry->registeredPeers[peerIndex].sendRegRequestResponse )
                    {
                        //! Reset the flag
                        serviceEntry->registeredPeers[peerIndex].sendRegRequestResponse = false;

                        //! Send the messsage
                        uint8_t msgStatus = RequestDenied;
                        iswSolNet->SendRegisterResponseMessage(peerIndex, serviceEntry->registeredPeers[peerIndex].regRequestResponseSeqNo, msgStatus);
                        serviceEntry->registeredPeers[peerIndex].regRequestResponseSeqNo = 0;
                    }

                    //! If Peer was deregistered
                    //! Send a DeregisterResponse message
                    if ( serviceEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse )
                    {
                        //! Reset the flag
                        serviceEntry->registeredPeers[peerIndex].sendDeregistrationRequestResponse = false;
                        //! Send the messsage
                        iswSolNet->SendDeregisterResponseMessage(peerIndex, serviceEntry->registeredPeers[peerIndex].deregRequestResponseSeqNo);
                        serviceEntry->registeredPeers[peerIndex].regRequestResponseSeqNo = 0;
                    }
                }
                serviceEntry->registeredPeers[peerIndex].registeredPeerLock.unlock();
            }
        }

        sleep(2);
    }

    return (nullptr);
}

//!###########################################################################
//! StartSolNetSendThread()
//! Inputs: None
//! Description: This method starts the background thread that handles SolNet
//!              events.
//!###########################################################################
int IswSolNet::StartSolNetSendThread(void)
{
    int status = 0;

    //! Start the thread that will check for SolNet send events
    if (pthread_create(&threadCheckSolNetSendEvents, nullptr, &IswSolNet::HandleSolNetSendEvents, (void *)this) != 0)
    {
        std::cout << "HandleSolNetSendEvents Didn't Start!" << std::endl;
        status = -1;
    }
    else
    {
        StartIswSolNetHandleSendEventsThread();
    }

    return (status);
}

//!#################################################################################
//! VerifySolNetDescriptor()
//! Inputs: commonHdr - pointer to a common SolNet Descriptor header
//!         offset - pointer to the offset variable into the data buffer
//!                  This method updates offset
//! Description: This method verifies a SolNet property descriptor against
//!              the ISW SolNet specification.  If an application sets
//!              the class variable verifyAdvertiseMessages to true, then
//!              incoming Advertise messages will be checked calling this function.
//!#################################################################################
bool IswSolNet::VerifySolNetDescriptor(iswSolNetDescCommonHeader *commonHdr, uint16_t *offset, iswSolNetServiceEntry *serviceEntry)
{
    bool descriptorVerified = true;

    qDebug() << Q_FUNC_INFO << "Common Header Descriptor ID:" << hex << commonHdr->descriptorId;
    std::stringstream ssBytes;
    uint8_t* bytePtr = (uint8_t*)(commonHdr);
    int i = 0;
    for(i=0;i<commonHdr->length;i++)
    {
       ssBytes << std::to_string(*(bytePtr+i)) << " ";
    }
    ssBytes << std::endl;
    std::cout << ssBytes.str();

    switch (commonHdr->descriptorId)
    {
        case Label:
        case TextLabel:
        {
            iswSolNetPropertyTextLabelDesc *labelDesc = (iswSolNetPropertyTextLabelDesc *)commonHdr;
            if (labelDesc->header.length > SIZE_TEXT_LABEL)
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Text Label Descriptor: Incorrect Header Length = " << std::to_string(labelDesc->header.length) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Text Label Descriptor: Correct Header Length = " << std::to_string(labelDesc->header.length) << std::endl;
                ss3 << "Text Label                                   = " << labelDesc->textLabel << std::endl;
                ss3 << "Text Label Descriptor Verified" << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + labelDesc->header.length;
            break;
        }
        case Version:
        {
            iswSolNetPropertyVersionDesc *versionDesc = (iswSolNetPropertyVersionDesc *)commonHdr;
            uint8_t textLength                        = versionDesc->header.length
                                                         - sizeof(versionDesc->solNetVersion) - sizeof(versionDesc->reserved);

            if ( textLength > SIZE_VERSION_TEXT )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Version Descriptor: Incorrect Text Length = " << std::to_string(textLength) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                for (int i = 0; i < SIZE_VERSION_TEXT; i++)
                {
                    if ( !(versionDesc->versionText[i] >= 0x00) && (versionDesc->versionText[i] <= 0x7F) )
                    {
                        descriptorVerified = false;

                        std::stringstream ss3;
                        ss3 << "Version Descriptor: Incorrect Text Value" << versionDesc->versionText[i] <<  std::endl;
                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->DebugMessage(ss3.str(), GetIndex());
                        }
                        std::cout << ss3.str();
                    }
                }
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Version Descriptor: Correct Text Length = " << std::to_string(textLength) << std::endl;
                ss4 << "Version Descriptor: Text                = " << versionDesc->versionText << std::endl;
                ss4 << "Version Descriptor Verified" << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + versionDesc->header.length;
            break;
        }
        case Endpoint:
        {
            if(serviceEntry->serviceDesc.header.descriptorId == RTA_SERVICE)
            {
                //! Verify RTA Control Endpoint Descriptor
                iswSolNetPropertyRtaControlEndpointDesc *rtaControlEndpointDesc = (iswSolNetPropertyRtaControlEndpointDesc *)commonHdr;
                if ( rtaControlEndpointDesc->header.length != (sizeof(iswSolNetPropertyRtaControlEndpointDesc) - commonHeaderSize) )
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "RTA Control Endpoint Descriptor: Incorrect Header Length = " << std::to_string(rtaControlEndpointDesc->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    //! Validate dataflowId value
                    if ( ((rtaControlEndpointDesc->dataflowId & iswSolNetProviderConsumerMask) >= 0) && ((rtaControlEndpointDesc->dataflowId & iswSolNetProviderConsumerMask) <= iswSolNetProviderConsumerMask) )
                    {
                        //! Validate endpointId
                        if ( ((rtaControlEndpointDesc->endpointId  & iswSolNetProviderConsumerMask) >= 0) && ((rtaControlEndpointDesc->endpointId & iswSolNetProviderConsumerMask) < 4) )
                        {
                            //! Verify dataPolicies and endpointDistribution
                            if ( ((rtaControlEndpointDesc->dataPolicies & DATA_POLICIES_MASK) <= DATA_POLICIES_RESERVED) != true )
                            {
                                descriptorVerified = false;

                                std::stringstream ss3;
                                ss3 << "RTA Control Endpoint Descriptor: Incorrect Data Policies = " << std::to_string(rtaControlEndpointDesc->dataPolicies) << std::endl;

                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }
                        }
                        else
                        {
                            descriptorVerified = false;

                            std::stringstream ss4;
                            ss4 << "RTA Control Endpoint Descriptor: Incorrect Endpoint Id = " << std::to_string(rtaControlEndpointDesc->endpointId) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                    }
                    else
                    {
                        descriptorVerified = false;

                        std::stringstream ss5;
                        ss5 << "RTA Control Endpoint Descriptor: Incorrect Dataflow Id = " << std::to_string(rtaControlEndpointDesc->dataflowId) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss5.str(), GetIndex());
                        }
                        std::cout << ss5.str();
                    }
                }

                if (descriptorVerified == true)
                {
                    std::stringstream ss6;
                    ss6 << "RTA Control Endpoint Descriptor: Correct Header Length         = " << std::to_string(rtaControlEndpointDesc->header.length) << std::endl;
                    ss6 << "RTA Control Endpoint Descriptor: Correct Data Policies         = " << std::to_string(rtaControlEndpointDesc->dataPolicies) << std::endl;
                    ss6 << "RTA Control Endpoint Descriptor: Correct Endpoint Id           = " << std::to_string(rtaControlEndpointDesc->endpointId) << std::endl;
                    ss6 << "RTA Control Endpoint Descriptor: Correct Dataflow Id           = " << std::to_string(rtaControlEndpointDesc->dataflowId) << std::endl;
                    ss6 << "RTA Control Endpoint Descriptor: Correct Endpoint Distribution = " << std::to_string(rtaControlEndpointDesc->endpointDistribution) << std::endl;
                    ss6 << "RTA Control Endpoint Descriptor Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss6.str(), GetIndex());
                    }
                    std::cout << ss6.str();
                }

                *offset += sizeof(iswSolNetPropertyRtaControlEndpointDesc);

                //! Verify RTA Data Endpoint Descriptor
                iswSolNetPropertyRtaDataEndpointDesc *rtaDataEndpointDesc = (iswSolNetPropertyRtaDataEndpointDesc *)commonHdr;
                if ( rtaDataEndpointDesc->header.length != (sizeof(iswSolNetPropertyRtaDataEndpointDesc) - commonHeaderSize) )
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "RTA Data Endpoint Descriptor: Incorrect Header Length = " << std::to_string(rtaDataEndpointDesc->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    //! Validate dataflowId value
                    if ( ((rtaDataEndpointDesc->dataflowId & iswSolNetProviderConsumerMask) >= 0) && ((rtaDataEndpointDesc->dataflowId & iswSolNetProviderConsumerMask) <= iswSolNetProviderConsumerMask) )
                    {
                        //! Validate endpointId
                        if ( ((rtaDataEndpointDesc->endpointId  & iswSolNetProviderConsumerMask) >= 0) && ((rtaDataEndpointDesc->endpointId & iswSolNetProviderConsumerMask) < 4) )
                        {
                            //! Verify dataPolicies and endpointDistribution
                            if ( ((rtaDataEndpointDesc->dataPolicies & DATA_POLICIES_MASK) <= DATA_POLICIES_RESERVED) != true )
                            {
                                descriptorVerified = false;

                                std::stringstream ss3;
                                ss3 << "RTA Data Endpoint Descriptor: Incorrect Data Policies = " << std::to_string(rtaDataEndpointDesc->dataPolicies) << std::endl;

                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }
                        }
                        else
                        {
                            descriptorVerified = false;

                            std::stringstream ss4;
                            ss4 << "RTA Data Endpoint Descriptor: Incorrect Endpoint Id = " << std::to_string(rtaDataEndpointDesc->endpointId) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                    }
                    else
                    {
                        descriptorVerified = false;

                        std::stringstream ss5;
                        ss5 << "RTA Data Endpoint Descriptor: Incorrect Dataflow Id = " << std::to_string(rtaDataEndpointDesc->dataflowId) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss5.str(), GetIndex());
                        }
                        std::cout << ss5.str();
                    }
                }

                *offset += commonHeaderSize + sizeof(iswSolNetPropertyRtaDataEndpointDesc);

                if (descriptorVerified == true)
                {
                    std::stringstream ss6;
                    ss6 << "RTA Data Endpoint Descriptor: Correct Header Length            = " << std::to_string(rtaDataEndpointDesc->header.length) << std::endl;
                    ss6 << "RTA Data Endpoint Descriptor: Correct Data Policies            = " << std::to_string(rtaDataEndpointDesc->dataPolicies) << std::endl;
                    ss6 << "RTA Data Endpoint Descriptor: Correct Endpoint Id              = " << std::to_string(rtaDataEndpointDesc->endpointId) << std::endl;
                    ss6 << "RTA Data Endpoint Descriptor: Correct Dataflow Id              = " << std::to_string(rtaDataEndpointDesc->dataflowId) << std::endl;
                    ss6 << "RTA Data Endpoint Descriptor: Correct Endpoint Distribution    = " << std::to_string(rtaDataEndpointDesc->endpointDistribution) << std::endl;
                    ss6 << "RTA Data Endpoint Descriptor Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss6.str(), GetIndex());
                    }
                    std::cout << ss6.str();
                }

                break;
            }
            else if(VideoService == serviceEntry->serviceDesc.header.descriptorId)
            {
                iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)commonHdr;

                int i = 0;
                uint8_t *bytePtr = (uint8_t *)commonHdr;
                char hex[2];
                std::stringstream ss;
                ss << "Endpoint Descriptor Bytes: ";
                for(i = 0; i < endpointDesc->header.length; i++)
                {
                    sprintf(hex, "%x", *(bytePtr+i));
                    ss << hex << " ";
                }
                ss << std::endl;
                std::cout << ss.str();

                if (endpointDesc->header.length < ENDPOINT_DESC_CORE_BLOCK_SIZE)
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "Endpoint Descriptor: Incorrect Header Length = " << std::to_string(endpointDesc->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    //! Validate dataflowId value
                    if ( ((endpointDesc->dataflowId & iswSolNetProviderConsumerMask) >= 0) && ((endpointDesc->dataflowId & iswSolNetProviderConsumerMask) <= iswSolNetProviderConsumerMask) )
                    {
                        //! Validate endpointId
                        if ( ((endpointDesc->endpointId  & iswSolNetProviderConsumerMask) >= 0) && ((endpointDesc->endpointId & iswSolNetProviderConsumerMask) < 4) )
                        {
                            //! Verify dataPolicies and endpointDistribution
                            if ( ((endpointDesc->dataPolicies & DATA_POLICIES_MASK) <= DATA_POLICIES_RESERVED) != true )
                            {
                                descriptorVerified = false;

                                std::stringstream ss3;
                                ss3 << "Endpoint Descriptor: Incorrect Data Policies = " << std::to_string(endpointDesc->dataPolicies) << std::endl;

                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }
                        }
                        else
                        {
                            descriptorVerified = false;

                            std::stringstream ss4;
                            ss4 << "Endpoint Descriptor: Incorrect Endpoint Id = " << std::to_string(endpointDesc->endpointId) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                    }
                    else
                    {
                        descriptorVerified = false;

                        std::stringstream ss5;
                        ss5 << "Endpoint Descriptor: Incorrect Dataflow Id = " << std::to_string(endpointDesc->dataflowId) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss5.str(), GetIndex());
                        }
                        std::cout << ss5.str();
                    }
                }

                if (descriptorVerified == true)
                {
                    std::stringstream ss6;

                    ss6 << "Endpoint Descriptor: Correct Header Length = " << std::to_string(endpointDesc->header.length) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Data Policies = " << std::to_string(endpointDesc->dataPolicies) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Endpoint Id = " << std::to_string(endpointDesc->endpointId) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Dataflow Id = " << std::to_string(endpointDesc->dataflowId) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Distribution = " << std::to_string(endpointDesc->endpointDistribution) << std::endl;
                    ss6 << "Endpoint Descriptor Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss6.str(), GetIndex());
                    }
                    std::cout << ss6.str();
                }

                *offset += (ENDPOINT_DESC_CORE_BLOCK_SIZE + commonHeaderSize);

                break;
            }
            else //! Single Endpoint Descriptor
            {

                qDebug () << "Single Endpoint Descriptor";

                iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)commonHdr;

                if (endpointDesc->header.length < ENDPOINT_DESC_CORE_BLOCK_SIZE)
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "Endpoint Descriptor: Incorrect Header Length = " << std::to_string(endpointDesc->header.length) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    //! Validate dataflowId value
                    if ( ((endpointDesc->dataflowId & iswSolNetProviderConsumerMask) >= 0) && ((endpointDesc->dataflowId & iswSolNetProviderConsumerMask) <= iswSolNetProviderConsumerMask) )
                    {
                        //! Validate endpointId
                        if ( ((endpointDesc->endpointId  & iswSolNetProviderConsumerMask) >= 0) && ((endpointDesc->endpointId & iswSolNetProviderConsumerMask) < 4) )
                        {
                            //! Verify dataPolicies and endpointDistribution
                            if ( ((endpointDesc->dataPolicies & DATA_POLICIES_MASK) <= DATA_POLICIES_RESERVED) != true )
                            {
                                descriptorVerified = false;

                                std::stringstream ss3;
                                ss3 << "Endpoint Descriptor: Incorrect Data Policies = " << std::to_string(endpointDesc->dataPolicies) << std::endl;

                                if ( theLogger->DEBUG_IswSolNet == 1 )
                                {
                                    theLogger->DebugMessage(ss3.str(), GetIndex());
                                }
                                std::cout << ss3.str();
                            }
                        }
                        else
                        {
                            descriptorVerified = false;

                            std::stringstream ss4;
                            ss4 << "Endpoint Descriptor: Incorrect Endpoint Id = " << std::to_string(endpointDesc->endpointId) << std::endl;

                            if (theLogger->DEBUG_IswSolNet == 1)
                            {
                                theLogger->DebugMessage(ss4.str(), GetIndex());
                            }
                            std::cout << ss4.str();
                        }
                    }
                    else
                    {
                        descriptorVerified = false;

                        std::stringstream ss5;
                        ss5 << "Endpoint Descriptor: Incorrect Dataflow Id = " << std::to_string(endpointDesc->dataflowId) << std::endl;

                        if (theLogger->DEBUG_IswSolNet == 1)
                        {
                            theLogger->DebugMessage(ss5.str(), GetIndex());
                        }
                        std::cout << ss5.str();
                    }
                }

                if (descriptorVerified == true)
                {
                    std::stringstream ss6;

                    ss6 << "Endpoint Descriptor: Correct Header Length = " << std::to_string(endpointDesc->header.length) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Data Policies = " << std::to_string(endpointDesc->dataPolicies) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Endpoint Id   = " << std::to_string(endpointDesc->endpointId) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Dataflow Id   = " << std::to_string(endpointDesc->dataflowId) << std::endl;
                    ss6 << "Endpoint Descriptor: Correct Distribution  = " << std::to_string(endpointDesc->endpointDistribution) << std::endl;
                    ss6 << "Endpoint Descriptor Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss6.str(), GetIndex());
                    }
                    std::cout << ss6.str();
                }

                *offset += (ENDPOINT_DESC_CORE_BLOCK_SIZE + commonHeaderSize);
                break;
            }
        }
        case MulticastGroup:
        {
            iswSolNetPropertyMulticastGroupDesc *multicastDesc = (iswSolNetPropertyMulticastGroupDesc *)commonHdr;
            if ( multicastDesc->header.length != (sizeof(iswSolNetPropertyMulticastGroupDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Multicast Group Descriptor: Incorrect Header Length = " << std::to_string(multicastDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
#ifndef TESTING //! Don't run during unit tests
               IswStream *iswStream = iswInterface->GetIswStream();
               std::array<IswStream::multicastGroupRecords, MAX_MCAST_RECORDS>& mcastGroupRecords = iswStream->GetMulticastGroupRecordsRef();
               bool entryFound = false;

               //! Verify that this incoming descriptor matches a multicast group that we know about in the IswStreams class
               for (int i = 0; i < MAX_MCAST_RECORDS; i++)
               {
                   if ( mcastGroupRecords[i].iswMcastRecord.mcastAddress == multicastDesc->mcastAddress )
                   {
                       if ( !mcastGroupRecords[i].inUse )
                       {
                           //! multicastDesc->mcastGroupId won't be the same because the peer
                           //! is on a different mcastGroupId or peerIndex than the one we have
                           //! but the multicast address will be the same so this record should be inUse
                           descriptorVerified = false;
                           entryFound         = false;
                      }

                       entryFound = true;
                       break;
                   }
               }

               if ( !entryFound )
               {
                   descriptorVerified = false;

                   std::stringstream ss3;
                   ss3 << "Multicast Group Descriptor: Couldn't Find Entry" << std::endl;

                   if ( theLogger->DEBUG_IswSolNet == 1 )
                   {
                       theLogger->DebugMessage(ss3.str(), GetIndex());
                   }
                   std::cout << ss3.str();
               }
#endif
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss4;
                ss4 << "Multicast Group Descriptor: Correct Header Length = " << std::to_string(multicastDesc->header.length) << std::endl;
                ss4 << "Multicast Group Descriptor Verified" << std::endl;

                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += sizeof(iswSolNetPropertyMulticastGroupDesc);
            break;
        }
        case FlowPolicy:
        {
            iswSolNetPropertyFlowPolicyDesc *flowPolicyDesc = (iswSolNetPropertyFlowPolicyDesc *)commonHdr;
            if (flowPolicyDesc->header.length != (sizeof(iswSolNetPropertyFlowPolicyDesc) - commonHeaderSize))
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Flow Policy Descriptor: Incorrect Header Length = " << std::to_string(flowPolicyDesc->header.length) << std::endl;

                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
               if (flowPolicyDesc->autonomy > 3)
               {
                   descriptorVerified = false;

                   std::stringstream ss3;
                   ss3 << "Flow Policy Descriptor: Incorrect Autonomy Value = " << std::to_string(flowPolicyDesc->autonomy) << std::endl;

                   if (theLogger->DEBUG_IswSolNet == 1)
                   {
                       theLogger->DebugMessage(ss3.str(), GetIndex());
                   }
                   std::cout << ss3.str();
               }
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss4;

                ss4 << "Flow Policy Descriptor: Correct Header Length  = " << std::to_string(flowPolicyDesc->header.length) << std::endl;
                ss4 << "Flow Policy Descriptor: Correct Autonomy Value = " << std::to_string(flowPolicyDesc->autonomy) << std::endl;
                ss4 << "Flow Policy Descriptor Verified" << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += sizeof(iswSolNetPropertyFlowPolicyDesc);
            break;
        }
        case Encoding:
        {
            iswSolNetPropertyEncodingDesc *encodingDesc = (iswSolNetPropertyEncodingDesc *)commonHdr;
            if (encodingDesc->header.length != (sizeof(iswSolNetPropertyEncodingDesc) - commonHeaderSize))
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Encoding Descriptor: Incorrect Header Length = " << std::to_string(encodingDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
               // encodingDesc->encoding bits not defined yet
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss3;
                ss3 << "Encoding Descriptor: Correct Header Length = " << std::to_string(encodingDesc->header.length) << std::endl;
                ss3 << "Encoding Descriptor: Correct Encoding      = " << std::to_string(encodingDesc->encoding);
                ss3 << "Encoding Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += sizeof(iswSolNetPropertyEncodingDesc);
            break;
        }
        case DeviceName:
        {
            iswSolNetPropertyDeviceDesc *deviceDesc = (iswSolNetPropertyDeviceDesc *)commonHdr;
            u_int16_t deviceStrLength               = 256;

            if (deviceDesc->header.length > deviceStrLength)
            {
                descriptorVerified = false;

                std::stringstream ss2;

                ss2 << "Device Name Descriptor: Incorrect Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Name Descriptor: Correct Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;
                ss3 << "Device Name Descriptor: Device Name           = " << deviceDesc->devString << std::endl;
                ss3 << "Device Name Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + deviceDesc->header.length;
            break;
        }
        case DeviceSerialNumber:
        {
            iswSolNetPropertyDeviceDesc *deviceDesc = (iswSolNetPropertyDeviceDesc *)commonHdr;
            u_int16_t deviceStrLength               = 256;

            if ( deviceDesc->header.length > deviceStrLength )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Serial Number Descriptor: Incorrect Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Serial Number Descriptor: Correct Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;
                ss3 << "Device Serial Number Descriptor: Serial Number         = " << deviceDesc->devString << std::endl;
                ss3 << "Device Serial Number Descriptor Verified" << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + deviceDesc->header.length;
            break;
        }
        case DeviceManufacturer:
        {
            iswSolNetPropertyDeviceDesc *deviceDesc = (iswSolNetPropertyDeviceDesc *)commonHdr;
            u_int16_t deviceStrLength               = 256;

            if ( deviceDesc->header.length > deviceStrLength )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Manufacturer Descriptor: Incorrect Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Manufacturer Descriptor: Correct Header Length      = " << std::to_string(deviceDesc->header.length) << std::endl;
                ss3 << "Device Manufacturer Descriptor: Device Manufacturer        = " << deviceDesc->devString << std::endl;
                ss3 << "Device Manufacturer Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + deviceDesc->header.length;
            break;
        }
        case DeviceFriendlyName:
        {
            iswSolNetPropertyDeviceDesc *deviceDesc = (iswSolNetPropertyDeviceDesc *)commonHdr;
            u_int16_t deviceStrLength               = 256;

            if ( deviceDesc->header.length > deviceStrLength )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Friendly Name Descriptor: Incorrect Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Friendly Name Descriptor: Correct Header Length = " << std::to_string(deviceDesc->header.length) << std::endl;
                ss3 << "Device Friendly Name Descriptor: Device Friendly Name  = " << deviceDesc->devString << std::endl;
                ss3 << "Device Friendly Name Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + deviceDesc->header.length;
            break;
        }
        case EndpointProtocol: // Not Defined Yet
        {
            break;
        }
        case AssociatedService:
        {
            iswSolNetPropertyAssociatedServiceDesc *associatedDesc = (iswSolNetPropertyAssociatedServiceDesc *)commonHdr;
            if ( associatedDesc->header.length > (sizeof(iswSolNetPropertyAssociatedServiceDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss1;
                ss1 << "Associated Service Descriptor: Incorrect Header Length = " << std::to_string(associatedDesc->header.length) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }
            else
            {
                std::stringstream ss1;
                ss1 << "Associated Service Descriptor: Correct Header Length = " << std::to_string(associatedDesc->header.length) << std::endl;
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();

                if ( associatedDesc->associatedServiceId != VideoService )
                {
                    std::stringstream ss2;
                    ss2 << "Associated Service Descriptor: Incorrect Associated Service Id = " << std::to_string(associatedDesc->associatedServiceId) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                else
                {
                    std::stringstream ss2;
                    ss2 << "Associated Service Descriptor: Correct Associated Service Id = " << std::to_string(associatedDesc->associatedServiceId) << std::endl;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }

                switch(associatedDesc->associationType)
                {
                    case 0:
                    {
                        std::stringstream ss3;
                        ss3 << "Associated Service Descriptor: Correct Associated Service Type = " << std::to_string(associatedDesc->associationType) << std::endl;
                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->DebugMessage(ss3.str(), GetIndex());
                        }
                        std::cout << ss3.str();
                    }
                    case 1:
                    {
                        std::stringstream ss3;
                        ss3 << "Associated Service Descriptor: Correct Associated Service Type = " << std::to_string(associatedDesc->associationType) << std::endl;
                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->DebugMessage(ss3.str(), GetIndex());
                        }
                        std::cout << ss3.str();
                    }
                    default:
                    {
                        std::stringstream ss3;
                        ss3 << "Associated Service Descriptor: Incorrect Associated Service Type = " << std::to_string(associatedDesc->associationType) << std::endl;
                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->DebugMessage(ss3.str(), GetIndex());
                        }
                        std::cout << ss3.str();
                    }
                }
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss5;
                ss5 << "Associated Service Descriptor: Correct Header Length              = " << std::to_string(associatedDesc->header.length) << std::endl;
                ss5 << "Associated Service Descriptor: Correct Associated Service Id      = " << std::to_string(associatedDesc->associatedServiceId) << std::endl;
                ss5 << "Associated Service Descriptor: Correct Associated Selector Number = " << std::to_string(associatedDesc->associatedServiceSelector) << std::endl;
                ss5 << "Associated Service Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            *offset += commonHeaderSize + associatedDesc->header.length;
            break;
        }
        case Device: // Not Defined Yet
        {
            break;
        }
        case QoSProperties: // Needs to be tested
        {
            iswSolNetEndpointQosPropertiesDesc *qosPropertiesDesc = (iswSolNetEndpointQosPropertiesDesc *)commonHdr;

            if (QOS_PROPERTIES_DESC_CORE_BLOCK_SIZE >= qosPropertiesDesc->header.length)
            {
                descriptorVerified = false;

                std::stringstream ss1;
                ss1 << "Qos Properties Descriptor: Incorrect Header Length = " << std::to_string(qosPropertiesDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss2;
                ss2 << "Device Qos Properties Descriptor: Correct Header Length = " << std::to_string(qosPropertiesDesc->header.length) << std::endl;
                ss2 << "Device Qos Properties Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            *offset += (QOS_PROPERTIES_DESC_CORE_BLOCK_SIZE + commonHeaderSize);
            break;
        }
        case QoSDataRate: // Needs To Be Tested
        {
            std::stringstream ss1;
            if ((QOS_DATA_RATE_DESC_CORE_BLOCK_FLOAT != commonHdr->length) && (QOS_DATA_RATE_DESC_CORE_BLOCK_DOUBLE !=  commonHdr->length))
            {
                //! ***********************************************************
                //! Ref: A3309637
                //!
                //! The ISW Emulator v16.0 appears to introduce support for
                //! double-precision floating point values to represent the
                //! QoS data rate. As of the most recent ISW SolNet
                //! specification available, the data rate should be
                //! represented as single-precision floating point value.
                //! Accordingly, this function will accept both formats until a
                //! revised specification is available or the ISW Emulator is
                //! revised to only support single-precision floating point
                //! values.
                //! ***********************************************************
                descriptorVerified = false;

                std::stringstream ss1;
                ss1 << "Device Software/Firmware Version Descriptor: Incorrect Header Length = " << std::to_string(commonHdr->length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();

            }
            else
            {
                ss1 << "Device Qos Data Rate Descriptor: Valid Header Length = " << std::to_string(commonHdr->length) << std::endl;
            }

            // Double check valid data rate values for check
            if(QOS_DATA_RATE_DESC_CORE_BLOCK_FLOAT == commonHdr->length)
            {
                iswSolNetEndpointQosDataRateDesc *qosDataRateDesc = (iswSolNetEndpointQosDataRateDesc *)commonHdr;
                uint32_t tmpDataRate                              = qosDataRateDesc->dataRate;
                qDebug() << Q_FUNC_INFO << "Hex data rate representation:" << hex << tmpDataRate;
                float dataRate = *((float *)&(tmpDataRate));
                if(dataRate < 0)
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "Device Qos Data Rate Descriptor: Invalid Data Rate Value = " << std::to_string(dataRate) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                ss1 << "Device Qos Data Rate Descriptor: Valid Data Rate Value: " << std::to_string(dataRate) << std::endl;
                *offset += (QOS_DATA_RATE_DESC_CORE_BLOCK_FLOAT + commonHeaderSize);
            }
            else
            {
                iswSolNetEndpointQosDataRateDescDouble *qosDataRateDesc = (iswSolNetEndpointQosDataRateDescDouble *)commonHdr;
                uint64_t tmpDataRate                                    = qosDataRateDesc->dataRate;
                qDebug() << Q_FUNC_INFO << "Hex data rate representation:" << hex << tmpDataRate;
                double dataRate = *((double *)&(tmpDataRate));

                if(dataRate < 0)
                {
                    descriptorVerified = false;

                    std::stringstream ss2;
                    ss2 << "Device Qos Data Rate Descriptor: Invalid Data Rate Value = " << std::to_string(dataRate) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss2.str(), GetIndex());
                    }
                    std::cout << ss2.str();
                }
                ss1 << "Device Qos Data Rate Descriptor: Valid Data Rate Value: " << std::to_string(dataRate) << std::endl;
                *offset += (QOS_DATA_RATE_DESC_CORE_BLOCK_DOUBLE + commonHeaderSize);
            }

            if (descriptorVerified == true)
            {
                ss1 << "Device Qos Data Rate Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }
            break;
        }
        case QoSMaxLatency: // Not Defined Yet
        {
            iswSolNetQoSMaxLatencyDesc *qosMaxLatencyDesc = (iswSolNetQoSMaxLatencyDesc *)commonHdr;

//            if (qosMaxLatencyDesc->header.length != (sizeof(iswSolNetEndpointQosPropertiesDesc) - commonHeaderSize))
            if (qosMaxLatencyDesc->header.length != (sizeof(iswSolNetQoSMaxLatencyDesc) - commonHeaderSize))
            {
                descriptorVerified = false;

                std::stringstream ss1;
                ss1 << "Device QoS Max Latency Descriptor: Incorrect Header Length = " << std::to_string(qosMaxLatencyDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }

            // Need to find what the max should be, not in ICD
            uint16_t maxLatency = qosMaxLatencyDesc->maxLatency;
            if (maxLatency > 0xFFFF)
            {
                std::stringstream ss2;
                ss2 << "QoS Max Latency: Incorrect Max Latency = " << std::to_string(frameNumber) << std::endl;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "QoS Max Latency: Correct Max Latency = " << std::to_string(maxLatency) << std::endl;
                if (theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss3;
                ss3 << "Device Qos Max Latency Descriptor: Correct Header Length = " << std::to_string(qosMaxLatencyDesc->header.length) << std::endl;
                ss3 << "Device Qos Max Latency Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            // Check if need to include common header as well or not
            *offset += commonHeaderSize + sizeof(qosMaxLatencyDesc);
            break;
        }
        case DeviceSoftwareDesc:
        {
            iswSolNetDeviceSoftwareFirmwareVersionDesc *devSoftwareFirmwareDesc = (iswSolNetDeviceSoftwareFirmwareVersionDesc *)commonHdr;
            if (devSoftwareFirmwareDesc->header.length != (sizeof(iswSolNetDeviceSoftwareFirmwareVersionDesc) - commonHeaderSize))
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Software/Firmware Version Descriptor: Incorrect Header Length = " << std::to_string(devSoftwareFirmwareDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if (descriptorVerified == true)
            {
                std::stringstream ss3;
                ss3 << "Device Software/Firmware Version Descriptor: Correct Header Length = " << std::to_string(devSoftwareFirmwareDesc->header.length) << std::endl;
                ss3 << "Device Software/Firmware Version Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += sizeof(iswSolNetDeviceSoftwareFirmwareVersionDesc);
            break;
        }
        case DeviceSoftwareNameDesc:
        {
            iswSolNetSoftwareFirmwareNameDesc *devSoftwareFirmwareNameDesc = (iswSolNetSoftwareFirmwareNameDesc *)commonHdr;
            u_int16_t deviceStrLength                                      = 256;

            if ( devSoftwareFirmwareNameDesc->header.length > deviceStrLength )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Software/Firmware Name Descriptor: Incorrect Header Length = " << std::to_string(devSoftwareFirmwareNameDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Software/Firmware Name Descriptor: Correct Header Length = " << std::to_string(devSoftwareFirmwareNameDesc->header.length) << std::endl;
                ss3 << "Device Software/Firmware Name Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + devSoftwareFirmwareNameDesc->header.length;
            break;
        }
        case DeviceSoftwareVersionDesc:
        {
            iswSolNetSoftwareFirmwareVersionDesc *devSoftwareFirmwareVersionDesc = (iswSolNetSoftwareFirmwareVersionDesc *)commonHdr;
            u_int16_t deviceStrLength = 256;

            if ( devSoftwareFirmwareVersionDesc->header.length > deviceStrLength )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Device Software/Firmware Version Descriptor: Incorrect Header Length = " << std::to_string(devSoftwareFirmwareVersionDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Device Software/Firmware Version Descriptor: Correct Header Length = " << std::to_string(devSoftwareFirmwareVersionDesc->header.length) << std::endl;
                ss3 << "Device Software/Firmware Version Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + devSoftwareFirmwareVersionDesc->header.length;
            break;
        }
        case VideoSampleFormat:
        {
            iswSolNetPropertyVideoSampleFormatDesc *videoSampleDesc = (iswSolNetPropertyVideoSampleFormatDesc *)commonHdr;

            if ( videoSampleDesc->header.length != VIDEO_SAMPLE_FORMAT_DESC_CORE_BLOCK_SIZE )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Sample Format Descriptor: Incorrect Header Length = " << std::to_string(videoSampleDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoSampleDesc->bitDepth != 0x0018 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Sample Descriptor: Incorrect Bit Depth Value = " << std::to_string(videoSampleDesc->bitDepth) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            switch(videoSampleDesc->pixelFormat)
            {
                case PIXEL_FORMAT_Y:
                case PIXEL_FORMAT_NV21:
                case PIXEL_FORMAT_YUY2:
                case PIXEL_FORMAT_NV16:
                case PIXEL_FORMAT_YUV422:
                case PIXEL_FORMAT_YUV420:
                case PIXEL_FORMAT_RGBA_4444:
                case PIXEL_FORMAT_RGBA_8888:
                case PIXEL_FORMAT_RGBA_5551:
                case PIXEL_FORMAT_RGBX_8888:
                case PIXEL_FORMAT_RGB_332:
                case PIXEL_FORMAT_RGB_565:
                case PIXEL_FORMAT_RGB_888:
                    //! ###########################################################
                    //! These are all valid values for the Pixel Format field so
                    //! no action needs to be taken
                    //! ###########################################################
                    break;
                default:
                {
                    //! ###########################################################
                    //! Invalid Pixel Format value
                    //! ###########################################################
                    descriptorVerified = false;

                    std::stringstream ss3;
                    ss3 << "Video Sample Descriptor: Invalid Pixel Format Value = " << std::to_string(videoSampleDesc->pixelFormat) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss3.str(), GetIndex());
                    }
                    std::cout << ss3.str();
                    break;
                }
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Video Sample Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + videoSampleDesc->header.length;
            break;
        }
        case VideoOutputFormatRaw:
        {
            iswSolNetPropertyVideoOutputFormatRawDesc *videoFormatRawDesc = (iswSolNetPropertyVideoOutputFormatRawDesc *)commonHdr;

            printDescriptor(commonHdr);

            if ( videoFormatRawDesc->header.length > (sizeof(iswSolNetPropertyVideoOutputFormatRawDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Format Raw Descriptor: Incorrect Header Length = " << std::to_string(videoFormatRawDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Format Raw Descriptor: Correct Header Length = " << std::to_string(videoFormatRawDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoFormatRawDesc->resX <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Format Raw Descriptor: Incorrect ResX = " << std::to_string(videoFormatRawDesc->resX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video Format Raw Descriptor: Correct ResX = " << std::to_string(videoFormatRawDesc->resX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( videoFormatRawDesc->resY <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss4;
                ss4 << "Video Format Raw Descriptor: Incorrect ResY = " << std::to_string(videoFormatRawDesc->resY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "Video Format Raw Descriptor: Correct ResY = " << std::to_string(videoFormatRawDesc->resY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( videoFormatRawDesc->frameRate <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss5;
                ss5 << "Video Format Raw Descriptor: Incorrect Frame Rate = " << std::to_string(videoFormatRawDesc->frameRate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }
            else
            {
                std::stringstream ss5;
                ss5 << "Video Format Raw Descriptor: Correct Frame Rate = " << std::to_string(videoFormatRawDesc->frameRate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            if ( videoFormatRawDesc->bitDepth <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss6;
                ss6 << "Video Format Raw Descriptor: Incorrect Bit Depth = " << std::to_string(videoFormatRawDesc->bitDepth) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }
            else
            {
                std::stringstream ss6;
                ss6 << "Video Format Raw Descriptor: Correct Bit Depth = " << std::to_string(videoFormatRawDesc->bitDepth) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }

            if ( videoFormatRawDesc->pixelformat <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss7;
                ss7 << "Video Format Raw Descriptor: Incorrect Pixel Format = " << std::to_string(videoFormatRawDesc->pixelformat) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss7.str(), GetIndex());
                }
                std::cout << ss7.str();
            }
            else
            {
                std::stringstream ss7;
                ss7 << "Video Format Raw Descriptor: Correct Pixel Format = " << std::to_string(videoFormatRawDesc->pixelformat) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss7.str(), GetIndex());
                }
                std::cout << ss7.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss8;
                ss8 << "Video Format Raw Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss8.str(), GetIndex());
                }
                std::cout << ss8.str();
            }

            *offset += commonHeaderSize + videoFormatRawDesc->header.length;
            break;
        }
        case VideoFormat:
        {
            iswSolNetPropertyVideoFormatDesc *videoFormatDesc = (iswSolNetPropertyVideoFormatDesc *)commonHdr;

            //! Verify Length
            if (VIDEO_FORMAT_PROPERTY_DESC_CORE_BLOCK_SIZE > videoFormatDesc->header.length)
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Format Descriptor: Incorrect Header Length = " << std::to_string(videoFormatDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Format Descriptor: Correct Header Length = " << std::to_string(videoFormatDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoFormatDesc->videoFormatX <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Format Descriptor: Incorrect Format_X = " << std::to_string(videoFormatDesc->videoFormatX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video Format Descriptor: Correct Format_X = " << std::to_string(videoFormatDesc->videoFormatX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( videoFormatDesc->videoFormatY <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss4;
                ss4 << "Video Format Descriptor: Incorrect Format_Y = " << std::to_string(videoFormatDesc->videoFormatY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "Video Format Descriptor: Correct Format_Y = " << std::to_string(videoFormatDesc->videoFormatY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( videoFormatDesc->frameRate <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss5;
                ss5 << "Video Format Descriptor: Incorrect Frame Rate = " << std::to_string(videoFormatDesc->frameRate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }
            else
            {
                std::stringstream ss5;
                ss5 << "Video Format Descriptor: Correct Frame Rate = " << std::to_string(videoFormatDesc->frameRate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            if ( videoFormatDesc->formatOptions < 0 )
            {
                descriptorVerified = false;

                std::stringstream ss6;
                ss6 << "Video Format Descriptor: Incorrect Format Options = " << std::to_string(videoFormatDesc->formatOptions) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }
            else
            {
                std::stringstream ss6;
                ss6 << "Video Format Descriptor: Correct Format Options = " << std::to_string(videoFormatDesc->formatOptions) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }

            if ( videoFormatDesc->encodingFormat < 0 )
            {
                descriptorVerified = false;

                std::stringstream ss7;
                ss7 << "Video Format Descriptor: Incorrect Encoding Format = " << std::to_string(videoFormatDesc->encodingFormat) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss7.str(), GetIndex());
                }
                std::cout << ss7.str();
            }
            else
            {
                std::stringstream ss7;
                ss7 << "Video Format Descriptor: Correct Encoding Format = " << std::to_string(videoFormatDesc->encodingFormat) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss7.str(), GetIndex());
                }
                std::cout << ss7.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss11;
                ss11 << "Video Format Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss11.str(), GetIndex());
                }
                std::cout << ss11.str();
            }

            *offset += commonHeaderSize + videoFormatDesc->header.length;
            break;
        }
        case MaximumFrameSize:
        {
            iswSolNetPropertyMaximumFrameSizeDesc *frameSizeDesc = (iswSolNetPropertyMaximumFrameSizeDesc *)commonHdr;

            std::stringstream ss1;
            if(frameSizeDesc->header.length != MAXIMUM_FRAME_SIZE_DESC_CORE_BLOCK_SIZE)
            {
                descriptorVerified = false;

                ss1 << "Maximum Frame Size Descriptor: Invalid header length = " << std::to_string(frameSizeDesc->header.length) << std::endl;
                if(theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }
            else
            {
                ss1 << "Maximum Frame Size Descriptor: Valid header length = " << std::to_string(frameSizeDesc->header.length) << std::endl;
                if(theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss1.str(), GetIndex());
                }
                std::cout << ss1.str();
            }

            std::stringstream ss2;
            if(frameSizeDesc->frameSize <= 0)
            {
                descriptorVerified = false;

                ss2 << "Maximum Frame Size Descriptor: Invalid maximum frame size = " << std::to_string(frameSizeDesc->frameSize) << std::endl;
                if(theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                ss2 << "Maximum Frame Size Descriptor: Valid maximum frame size = " << std::to_string(frameSizeDesc->frameSize) << std::endl;
                if(theLogger->DEBUG_IswSolNet == 1)
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Maximum Frame Size Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + frameSizeDesc->header.length;
            break;
        }
        case RleFormat: // Not Defined Yet
        {
            break;
        }
        case VideoProtocol:
        {
            iswSolNetPropertyVideoProtocolDesc *videoProtocolDesc = (iswSolNetPropertyVideoProtocolDesc *)commonHdr;

            if ( videoProtocolDesc->header.length > (sizeof(iswSolNetPropertyVideoProtocolDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Protocol Descriptor: Incorrect Header Length = " << std::to_string(videoProtocolDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Protocol Descriptor: Correct Header Length = " << std::to_string(videoProtocolDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoProtocolDesc->protocol <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Protocol Descriptor: Incorrect Protocol = " << std::to_string(videoProtocolDesc->protocol) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video Protocol Descriptor: Correct Protocol = " << std::to_string(videoProtocolDesc->protocol) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Video Protocol Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + videoProtocolDesc->header.length;
            break;
        }
        case IFOV:
        {
            iswSolNetPropertyVideoIfovDesc *videoIFOVDesc = (iswSolNetPropertyVideoIfovDesc *)commonHdr;

            if ( videoIFOVDesc->header.length > (sizeof(iswSolNetPropertyVideoIfovDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video IFOV Descriptor: Incorrect Header Length = " << std::to_string(videoIFOVDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video IFOV Descriptor: Correct Header Length = " << std::to_string(videoIFOVDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoIFOVDesc->IFoV <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video IFOV Descriptor: Incorrect Instant Field Of View = " << std::to_string(videoIFOVDesc->IFoV) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video IFOV Descriptor: Correct Instant Field Of View = " << std::to_string(videoIFOVDesc->IFoV) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Video IFOV Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + videoIFOVDesc->header.length;
            break;
        }
        case PrincipalPoint:
        {
            iswSolNetPropertyVideoPrincipalPointDesc *videoPrincipalPointDesc = (iswSolNetPropertyVideoPrincipalPointDesc *)commonHdr;

            if ( videoPrincipalPointDesc->header.length > (sizeof(iswSolNetPropertyVideoPrincipalPointDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Principal Point Descriptor: Incorrect Header Length = " << std::to_string(videoPrincipalPointDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->LogMessage(ss2.str(), GetIndex(), std::chrono::system_clock::now());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Principal Point Descriptor: Correct Header Length = " << std::to_string(videoPrincipalPointDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->LogMessage(ss2.str(), GetIndex(), std::chrono::system_clock::now());
                }
                std::cout << ss2.str();
            }

            if ( videoPrincipalPointDesc->principalPointX <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Principal Point Descriptor: Incorrect PointX = " << std::to_string(videoPrincipalPointDesc->principalPointX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video Principal Point Descriptor: Correct PointX = " << std::to_string(videoPrincipalPointDesc->principalPointX) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( videoPrincipalPointDesc->principalPointY <= 0 )
            {
                descriptorVerified = false;

                std::stringstream ss4;
                ss4 << "Video Principal Point Descriptor: Incorrect PointY = " << std::to_string(videoPrincipalPointDesc->principalPointY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "Video Principal Point Descriptor: Correct PointY = " << std::to_string(videoPrincipalPointDesc->principalPointY) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss5;
                ss5 << "Video Principal Point Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            *offset += commonHeaderSize + videoPrincipalPointDesc->header.length;
            break;
        }
        case VideoLensDistortion:
        {
            iswSolNetPropertyVideoLensDistortionDesc *videoLensDistortionDesc = (iswSolNetPropertyVideoLensDistortionDesc *)commonHdr;

            if ( videoLensDistortionDesc->header.length > (sizeof(iswSolNetPropertyVideoLensDistortionDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Lens Distortion Descriptor: Incorrect Header Length = " << std::to_string(videoLensDistortionDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Lens Distortion Descriptor: Correct Header Length = " << std::to_string(videoLensDistortionDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "Video Lens Distortion Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + videoLensDistortionDesc->header.length;
            break;
        }
        case VideoControl:
        {
            iswSolNetPropertyVideoControlDesc *videoControlDesc = (iswSolNetPropertyVideoControlDesc *)commonHdr;

            if ( videoControlDesc->header.length > (sizeof(iswSolNetPropertyVideoControlDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Video Control Descriptor: Incorrect Header Length = " << std::to_string(videoControlDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Video Control Descriptor: Correct Header Length = " << std::to_string(videoControlDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( videoControlDesc->controlBitmap > 1 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Video Control Descriptor: Incorrect Control Bitmap = " << std::to_string(videoControlDesc->controlBitmap) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Video Control Descriptor: Correct Control Bitmap = " << std::to_string(videoControlDesc->controlBitmap) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Video Control Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + videoControlDesc->header.length;
            break;
        }
        case ImageSensorType:
        {
            iswSolNetPropertyVideoImageSensorTypeDesc *imageSensorTypeDesc = (iswSolNetPropertyVideoImageSensorTypeDesc *)commonHdr;

            if ( imageSensorTypeDesc->header.length > (sizeof(iswSolNetPropertyVideoImageSensorTypeDesc) - commonHeaderSize) )
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Image Sensor Type Descriptor: Incorrect Header Length = " << std::to_string(imageSensorTypeDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Image Sensor Type Descriptor: Correct Header Length = " << std::to_string(imageSensorTypeDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( imageSensorTypeDesc->type > 0x000C )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Image Sensor Type Descriptor: Incorrect Image Sensor Type = " << std::to_string(imageSensorTypeDesc->type) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Image Sensor Type Descriptor: Correct Image Sensor Type = " << std::to_string(imageSensorTypeDesc->type) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            // TODO -- ADD DESCRIPTION VALUE

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Image Sensor Type Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + imageSensorTypeDesc->header.length;
            break;
        }
        case ImageSensorWaveband:
        {
            iswSolNetPropertyVideoImageSensorWavebandDesc *imageSensorWavebandDesc = (iswSolNetPropertyVideoImageSensorWavebandDesc *)commonHdr;

            if (imageSensorWavebandDesc->header.length < IMAGE_SENSOR_WAVEBAND_DESC_CORE_BLOCK_SIZE)
            {
                descriptorVerified = false;

                std::stringstream ss2;
                ss2 << "Image Sensor Waveband Descriptor: Incorrect Header Length = " << std::to_string(imageSensorWavebandDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Image Sensor Waveband Descriptor: Correct Header Length = " << std::to_string(imageSensorWavebandDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( imageSensorWavebandDesc->waveband > 0x0100 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Image Sensor Waveband Descriptor: Incorrect Waveband = " << std::to_string(imageSensorWavebandDesc->waveband) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Image Sensor Waveband Descriptor: Correct Waveband = " << std::to_string(imageSensorWavebandDesc->waveband) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            // TODO -- ADD DESCRIPTION VALUE

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Image Sensor Waveband Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + imageSensorWavebandDesc->header.length;
            break;
        }
        case VideoIntrinsicParameters: //! Not Defined Yet
        {
            break;
        }
        case VideoFrameRate: //! Not Defined Yet
        {
            break;
        }
        case UiMotion:
        {
            iswSolNetPropertySimpleUiMotionDesc *UiMotionDesc = (iswSolNetPropertySimpleUiMotionDesc *)commonHdr;

            if ( UiMotionDesc->header.length > (sizeof(iswSolNetPropertySimpleUiMotionDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "UI Motion Descriptor: Incorrect Header Length = " << std::to_string(UiMotionDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "UI Motion Descriptor: Correct Header Length = " << std::to_string(UiMotionDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if  ( UiMotionDesc->type > 1 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "UI Motion Descriptor: Incorrect Type = " << std::to_string(UiMotionDesc->type) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "UI Motion Descriptor: Correct Type = " << std::to_string(UiMotionDesc->type) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( UiMotionDesc->flags > 2 )
            {
                 descriptorVerified = false;

                 std::stringstream ss4;
                 ss4 << "UI Motion Descriptor: Incorrect Flags = " << std::to_string(UiMotionDesc->flags) << std::endl;

                 if ( theLogger->DEBUG_IswSolNet == 1 )
                 {
                     theLogger->DebugMessage(ss4.str(), GetIndex());
                 }
                 std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "UI Motion Descriptor: Correct Flags = " << std::to_string(UiMotionDesc->flags) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss5;
                ss5 << "UI Motion Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            *offset += commonHeaderSize + UiMotionDesc->header.length;
            break;
        }
        case UiButtonInput:
        {
            iswSolNetPropertySimpleUiButtonInputDesc *UiButtonInputDesc = (iswSolNetPropertySimpleUiButtonInputDesc *)commonHdr;

            if ( UiButtonInputDesc->header.length > (sizeof(iswSolNetPropertySimpleUiButtonInputDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "UI Button Input Descriptor: Incorrect Header Length = " << std::to_string(UiButtonInputDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss3;
                ss3 << "UI Button Input Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            *offset += commonHeaderSize + UiButtonInputDesc->header.length;
            break;
        }
        case ImuOutputRate:
        {
            iswSolNetPropertyImuOutputRateDesc *imuRateDesc = (iswSolNetPropertyImuOutputRateDesc *)commonHdr;

            if ( imuRateDesc->header.length > (sizeof(iswSolNetPropertyImuOutputRateDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "IMU Output Rate Descriptor: Incorrect Header Length = " << std::to_string(imuRateDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "IMU Output Rate Descriptor: Correct Header Length = " << std::to_string(imuRateDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( imuRateDesc->rate <= 1 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "IMU Output Rate Descriptor: Incorrect Rate = " << std::to_string(imuRateDesc->rate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "IMU Output Rate Descriptor: Correct Rate = " << std::to_string(imuRateDesc->rate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "IMU Output Rate Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + imuRateDesc->header.length;
            break;
        }
        case ImuFloatUpdateRate:
        {
            iswSolNetPropertyImuInputRateDesc *floatUpdateRateDesc = (iswSolNetPropertyImuInputRateDesc *)commonHdr;

            if ( floatUpdateRateDesc->header.length > (sizeof(iswSolNetPropertyImuInputRateDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "IMU Float Update Rate Descriptor: Incorrect Header Length = " << std::to_string(floatUpdateRateDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "IMU Float Update Rate Descriptor: Correct Header Length = " << std::to_string(floatUpdateRateDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( floatUpdateRateDesc->rateMinimum <= 1 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "IMU Float Update Rate Descriptor: Incorrect Float Update Rate = " << std::to_string(floatUpdateRateDesc->rateMinimum) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "IMU Float Update Rate Descriptor: Correct Float Update Rate = " << std::to_string(floatUpdateRateDesc->rateMinimum) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "RTA IMU Rate Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            *offset += commonHeaderSize + floatUpdateRateDesc->header.length;
            break;
        }
        case ImuSensorNoise:
        {
            iswSolNetPropertImuSensorNoiseDesc *ImuSensorNoiseDesc = (iswSolNetPropertImuSensorNoiseDesc *)commonHdr;

            if ( ImuSensorNoiseDesc->header.length > (sizeof(iswSolNetPropertImuSensorNoiseDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "IMU Sensor Noise Descriptor: Incorrect Header Length = " << std::to_string(ImuSensorNoiseDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "IMU Sensor Noise Descriptor: Correct Header Length = " << std::to_string(ImuSensorNoiseDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( ImuSensorNoiseDesc->accelerometerXNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "IMU Sensor Noise Descriptor: Incorrect Accelerometer X Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerXNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "IMU Sensor Noise Descriptor: Correct Accelerometer X Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerXNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( ImuSensorNoiseDesc->accelerometerYNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss4;
               ss4 << "IMU Sensor Noise Descriptor: Incorrect Accelerometer Y Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerYNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss4.str(), GetIndex());
               }
               std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "IMU Sensor Noise Descriptor: Correct Accelerometer Y Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerYNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( ImuSensorNoiseDesc->accelerometerZNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss5;
               ss5 << "IMU Sensor Noise Descriptor: Incorrect Accelerometer Z Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerZNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss5.str(), GetIndex());
               }
               std::cout << ss5.str();
            }
            else
            {
                std::stringstream ss5;
                ss5 << "IMU Sensor Noise Descriptor: Correct Accelerometer Z Noise = " << std::to_string(ImuSensorNoiseDesc->accelerometerZNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            if ( ImuSensorNoiseDesc->gyroscopeXNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss6;
               ss6 << "IMU Sensor Noise Descriptor: Incorrect Gyroscope X Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeXNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss6.str(), GetIndex());
               }
               std::cout << ss6.str();
            }
            else
            {
                std::stringstream ss6;
                ss6 << "IMU Sensor Noise Descriptor: Correct Gyroscope X Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeXNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }

            if ( ImuSensorNoiseDesc->gyroscopeYNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss7;
               ss7 << "IMU Sensor Noise Descriptor: Incorrect Gyroscope Y Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeYNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss7.str(), GetIndex());
               }
               std::cout << ss7.str();
            }
            else
            {
                std::stringstream ss7;
                ss7 << "IMU Sensor Noise Descriptor: Correct Gyroscope Y Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeYNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss7.str(), GetIndex());
                }
                std::cout << ss7.str();
            }

            if ( ImuSensorNoiseDesc->gyroscopeZNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss8;
               ss8 << "IMU Sensor Noise Descriptor: Incorrect Gyroscope Z Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeZNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss8.str(), GetIndex());
               }
               std::cout << ss8.str();
            }
            else
            {
                std::stringstream ss8;
                ss8 << "IMU Sensor Noise Descriptor: Correct Gyroscope Z Noise = " << std::to_string(ImuSensorNoiseDesc->gyroscopeZNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss8.str(), GetIndex());
                }
                std::cout << ss8.str();
            }

            if ( ImuSensorNoiseDesc->magnetometerXNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss9;
               ss9 << "IMU Sensor Noise Descriptor: Incorrect Magnetometer X Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerXNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss9.str(), GetIndex());
               }
               std::cout << ss9.str();
            }
            else
            {
                std::stringstream ss9;
                ss9 << "IMU Sensor Noise Descriptor: Correct Magnetometer X Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerXNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss9.str(), GetIndex());
                }
                std::cout << ss9.str();
            }

            if ( ImuSensorNoiseDesc->magnetometerYNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss10;
               ss10 << "IMU Sensor Noise Descriptor: Incorrect Magnetometer Y Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerYNoise)<< std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss10.str(), GetIndex());
               }
               std::cout << ss10.str();
            }
            else
            {
                std::stringstream ss10;
                ss10 << "IMU Sensor Noise Descriptor: Correct Magnetometer Y Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerYNoise)<< std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss10.str(), GetIndex());
                }
                std::cout << ss10.str();
            }

            if ( ImuSensorNoiseDesc->magnetometerZNoise <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss11;
               ss11 << "IMU Sensor Noise Descriptor: Incorrect Magnetometer Z Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerZNoise) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss11.str(), GetIndex());
               }
               std::cout << ss11.str();
            }
            else
            {
                std::stringstream ss11;
                ss11 << "IMU Sensor Noise Descriptor: Correct Magnetometer Z Noise = " << std::to_string(ImuSensorNoiseDesc->magnetometerZNoise) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss11.str(), GetIndex());
                }
                std::cout << ss11.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss12;
                ss12 << "IMU Sensor Noise Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss12.str(), GetIndex());
                }
                std::cout << ss12.str();
            }

            *offset += commonHeaderSize + ImuSensorNoiseDesc->header.length;
            break;
        }
        case MotionSensorRate:
        {
            iswSolNetPropertyMotionSensorRateDesc *motionSensorRateDesc = (iswSolNetPropertyMotionSensorRateDesc *)commonHdr;

            if ( motionSensorRateDesc->header.length > (sizeof(iswSolNetPropertyMotionSensorRateDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Motion Sensor Rate Descriptor: Incorrect Header Length = " << std::to_string(motionSensorRateDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Motion Sensor Rate Descriptor: Correct Header Length = " << std::to_string(motionSensorRateDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( motionSensorRateDesc->rate < 60 )
            {
                descriptorVerified = false;

                std::stringstream ss3;
                ss3 << "Motion Sensor Rate Descriptor: Incorrect Rate = " << std::to_string(motionSensorRateDesc->rate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Motion Sensor Rate Descriptor: Correct Rate = " << std::to_string(motionSensorRateDesc->rate) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "Motion Sensor Rate Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + motionSensorRateDesc->header.length;
            break;
        }
        case MotionSensorAccel:
        {
            iswSolNetPropertyMotionSensorAccel *motionSensorAccelDesc = (iswSolNetPropertyMotionSensorAccel *)commonHdr;

            if ( motionSensorAccelDesc->header.length > (sizeof(iswSolNetPropertyMotionSensorAccel) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Motion Sensor Accelometer Descriptor: Incorrect Header Length = " << std::to_string(motionSensorAccelDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Motion Sensor Accelometer Descriptor: Correct Header Length = " << std::to_string(motionSensorAccelDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            // Detect Presence of Child Block
            if((sizeof(iswSolNetPropertyMotionSensorAccel) - commonHeaderSize) > 0)
            {
                std::stringstream ss;
                ss << "Motion Sensor Accelerometer Descriptor: Child Descriptors Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }
            else
            {
                std::stringstream ss;
                ss << "Motion Sensor Accelerometer Descriptor: Child Descriptors Not Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "Motion Sensor Accelerometer Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + motionSensorAccelDesc->header.length;
            break;
        }
        case MotionSensorGyro:
        {
            iswSolNetPropertyMotionSensorGyro *motionSensorGyroDesc = (iswSolNetPropertyMotionSensorGyro *)commonHdr;

            if ( motionSensorGyroDesc->header.length > (sizeof(iswSolNetPropertyMotionSensorAccel) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Motion Sensor Gyroscope Descriptor: Incorrect Header Length = " << std::to_string(motionSensorGyroDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Motion Sensor Gyroscope Descriptor: Correct Header Length = " << std::to_string(motionSensorGyroDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            // Detect Presence of Child Block
            if((sizeof(iswSolNetPropertyMotionSensorGyro) - commonHeaderSize) > 0)
            {
                std::stringstream ss;
                ss << "Motion Sensor Gyroscope Descriptor: Child Descriptors Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }
            else
            {
                std::stringstream ss;
                ss << "Motion Sensor Gyroscope Descriptor: Child Descriptors Not Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "Motion Sensor Gyroscope Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + motionSensorGyroDesc->header.length;
            break;
        }
        case MotionSensorMag:
        {
            iswSolNetPropertyMotionSensorMag *motionSensorMagDesc = (iswSolNetPropertyMotionSensorMag *)commonHdr;
            if ( motionSensorMagDesc->header.length > (sizeof(iswSolNetPropertyMotionSensorAccel) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Motion Sensor Magnetometer Descriptor: Incorrect Header Length = " << std::to_string(motionSensorMagDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Motion Sensor Magnetometer Descriptor: Correct Header Length = " << std::to_string(motionSensorMagDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            // Detect Presence of Child Block
            if((sizeof(iswSolNetPropertyMotionSensorMag) - commonHeaderSize) > 0)
            {
                std::stringstream ss;
                ss << "Motion Sensor Magnetometer Descriptor: Child Descriptors Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }
            else
            {
                std::stringstream ss;
                ss << "Motion Sensor Magnetometer Descriptor: Child Descriptors Not Present" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "Motion Sensor Magnetometer Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + motionSensorMagDesc->header.length;
            break;
        }
        case NetworkingIP:
        {
            iswSolNetPropertyIpServiceDesc *ipDesc = (iswSolNetPropertyIpServiceDesc *)commonHdr;

            if ( ipDesc->header.length > (sizeof(iswSolNetPropertyIpServiceDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Networking IP Descriptor: Incorrect Header Length = " << std::to_string(ipDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Networking IP Descriptor: Correct Header Length = " << std::to_string(ipDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( ipDesc->ipV4Address <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "Networking IP Descriptor: Incorrect IP v4 Address = " << std::to_string(ipDesc->ipV4Address) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Networking IP Descriptor: Correct IP v4 Address = " << std::to_string(ipDesc->ipV4Address) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Networking IP Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            break;
        }
        case NetworkingTCPPort:
        {
            iswSolNetPropertyTcpPortDesc *tcpPortDesc = (iswSolNetPropertyTcpPortDesc *)commonHdr;

            if ( tcpPortDesc->header.length > (sizeof(iswSolNetPropertyTcpPortDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Networking TCP Port Descriptor: Incorrect Header Length = " << std::to_string(tcpPortDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {

                std::stringstream ss2;
                ss2 << "Networking TCP Port Descriptor: Correct Header Length = " << std::to_string(tcpPortDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( tcpPortDesc->tcpPort <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "Networking TCP Port Descriptor: Incorrect TCP Port = " << std::to_string(tcpPortDesc->tcpPort) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Networking TCP Port Descriptor: Correct TCP Port = " << std::to_string(tcpPortDesc->tcpPort) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss4;
                ss4 << "Networking TCP Port Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            break;
        }
        case NetworkingUDPPort:
        {
            iswSolNetPropertyUdpPortDescriptor *udpPortDesc = (iswSolNetPropertyUdpPortDescriptor *)commonHdr;

            if ( udpPortDesc->header.length > (sizeof(iswSolNetPropertyUdpPortDescriptor) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Networking UDP Port Descriptor: Incorrect Header Length = " << std::to_string(udpPortDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Networking UDP Port Descriptor: Correct Header Length = " << std::to_string(udpPortDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( udpPortDesc->udpPort <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "Networking UDP Port Descriptor: Incorrect UDP Port = " << std::to_string(udpPortDesc->udpPort) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Networking UDP Port Descriptor: Correct UDP Port = " << std::to_string(udpPortDesc->udpPort) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( udpPortDesc->direction <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss4;
               ss4 << "Networking UDP Port Descriptor: Incorrect Direction = " << std::to_string(udpPortDesc->direction) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss4.str(), GetIndex());
               }
               std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "Networking UDP Port Descriptor: Correct Direction = " << std::to_string(udpPortDesc->direction) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( udpPortDesc->mcastIpAddress <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss5;
               ss5 << "Networking UDP Port Descriptor: Incorrect Multicast IP Address = " << std::to_string(udpPortDesc->mcastIpAddress) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss5.str(), GetIndex());
               }
               std::cout << ss5.str();
            }
            else
            {
                std::stringstream ss5;
                ss5 << "Networking UDP Port Descriptor: Correct Multicast IP Address = " << std::to_string(udpPortDesc->mcastIpAddress) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss6;
                ss6 << "Networking UDP Port Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }

            *offset += commonHeaderSize + udpPortDesc->header.length;
            break;
        }
        case ArService:
        {
            iswSolNetPropertyArType2OutputDesc *arType2OutputDesc = (iswSolNetPropertyArType2OutputDesc *)commonHdr;

            if ( arType2OutputDesc->header.length > (sizeof(iswSolNetPropertyArType2OutputDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "AR Service Descriptor: Incorrect Header Length = " << std::to_string(arType2OutputDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "AR Service Descriptor: Correct Header Length = " << std::to_string(arType2OutputDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( arType2OutputDesc->minHeight <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "AR Service Descriptor: Incorrect Minimum Height = " << std::to_string(arType2OutputDesc->minHeight) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "AR Service Descriptor: Correct Minimum Height = " << std::to_string(arType2OutputDesc->minHeight) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( arType2OutputDesc->minWidth <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss4;
               ss4 << "AR Service Descriptor: Incorrect Minimum Width = " << std::to_string(arType2OutputDesc->minWidth) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss4.str(), GetIndex());
               }
               std::cout << ss4.str();
            }
            else
            {
                std::stringstream ss4;
                ss4 << "AR Service Descriptor: Correct Minimum Width = " << std::to_string(arType2OutputDesc->minWidth) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }

            if ( arType2OutputDesc->supportedRleVersions <= 0 )
            {
               descriptorVerified = false;

               std::stringstream ss5;
               ss5 << "AR Service Descriptor: Incorrect Supported RLE Versions = " << std::to_string(arType2OutputDesc->supportedRleVersions) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss5.str(), GetIndex());
               }
               std::cout << ss5.str();
            }
            else
            {
                std::stringstream ss5;
                ss5 << "AR Service Descriptor: Correct Supported RLE Versions = " << std::to_string(arType2OutputDesc->supportedRleVersions) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss5.str(), GetIndex());
                }
                std::cout << ss5.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss6;
                ss6 << "AR Service Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss6.str(), GetIndex());
                }
                std::cout << ss6.str();
            }

            *offset += commonHeaderSize + arType2OutputDesc->header.length;
            break;
        }
        case StatusBattery:
        {
            iswSolNetPropertyStatusBatteryDesc *statusBatteryDesc = (iswSolNetPropertyStatusBatteryDesc *)commonHdr;

            if ( statusBatteryDesc->header.length < (sizeof(statusBatteryDesc->batteryId) + sizeof(statusBatteryDesc->reserved)) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "Status Battery Descriptor: Incorrect Header Length = " << std::to_string(statusBatteryDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "Status Battery Descriptor: Correct Header Length = " << std::to_string(statusBatteryDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( statusBatteryDesc->batteryId < 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "Status Battery Descriptor: Incorrect Battery Id = " << std::to_string(statusBatteryDesc->batteryId) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "Status Battery  Descriptor: Correct Battery Id = " << std::to_string(statusBatteryDesc->batteryId) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "Status Battery Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + statusBatteryDesc->header.length;
            break;
        }
        case LrfCapabilities:
        {
            iswSolNetLrfCapabilitiesDesc *lrfCapabilitiesDesc = (iswSolNetLrfCapabilitiesDesc *)commonHdr;

            if ( lrfCapabilitiesDesc->header.length < (sizeof(iswSolNetLrfCapabilitiesDesc) - commonHeaderSize) )
            {
               descriptorVerified = false;

               std::stringstream ss2;
               ss2 << "LRF Capabilities Descriptor: Incorrect Header Length = " << std::to_string(lrfCapabilitiesDesc->header.length) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss2.str(), GetIndex());
               }
               std::cout << ss2.str();
            }
            else
            {
                std::stringstream ss2;
                ss2 << "LRF Capabilities Descriptor: Correct Header Length = " << std::to_string(lrfCapabilitiesDesc->header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();
            }

            if ( lrfCapabilitiesDesc->capabilities < 0 )
            {
               descriptorVerified = false;

               std::stringstream ss3;
               ss3 << "LRF Capabilities Descriptor: Incorrect Capabilities = " << std::to_string(lrfCapabilitiesDesc->capabilities) << std::endl;

               if ( theLogger->DEBUG_IswSolNet == 1 )
               {
                   theLogger->DebugMessage(ss3.str(), GetIndex());
               }
               std::cout << ss3.str();
            }
            else
            {
                std::stringstream ss3;
                ss3 << "LRF Capabilities Descriptor: Correct Capabilities = " << std::to_string(lrfCapabilitiesDesc->capabilities) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss3.str(), GetIndex());
                }
                std::cout << ss3.str();
            }

            if ( descriptorVerified == true )
            {
                std::stringstream ss;
                ss << "LRF Capabilities Descriptor Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }

            *offset += commonHeaderSize + lrfCapabilitiesDesc->header.length;
            break;
        }
        case VideoReserved2:
        case VideoReserved3:
        case VideoReserved4:
        case VideoReserved5:
        case VideoReserved6:
        case VideoReserved7:
        case VideoReserved8:
        case VideoReserved9:
        case VideoReserved10:
        case VideoReserved12:
        case UiReservedMin:
        case UiReservedMax:
        case MotionReserved1:
        case MotionReserved2:
        case NetworkingReserved1:
        case NetworkingReserved2:
        case ArReserved1:
        case ArReserved2:
        case PowerReserved1:
        case PowerReserved2:
        default:
        {
            descriptorVerified = false;

            std::stringstream main_ss2;
            main_ss2 << "Incorrect Property Descriptor ID = " << std::to_string(commonHdr->descriptorId) << std::endl;

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(main_ss2.str(), GetIndex());
            }
            std::cout << main_ss2.str();
            break;
        }
    }

    return descriptorVerified;
}

//!#################################################################################
//! VerifySolNetServiceEntry()
//! Inputs: serviceEntry - pointer to iswSolNetServiceEntry with the serviceDescriptor
//!                        and any child descriptors
//! Description: This method verifies a SolNet service descriptor against
//!              the ISW SolNet specification.  If an application sets
//!              the class variable verifyAdvertiseMessages to true, then
//!              incoming Advertise messages will be checked calling this function.
//!#################################################################################
bool IswSolNet::VerifySolNetServiceEntry(iswSolNetServiceEntry *serviceEntry)
{
    bool serviceEntryVerified = false;
    bool headerVerified       = false;
    bool descriptorIdVerified = false;
    bool childBlockVerified   = false;
    bool checkChildBlock      = true;
    bool serviceVerified      = false;

    std::cout << " " << std::endl;
    
    std::stringstream ss_main;
    ss_main << "Verifying SolNet Service Entry..." << std::endl;

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        theLogger->LogMessage(ss_main.str(), GetIndex(), std::chrono::system_clock::now());
    }
    std::cout << ss_main.str();

    //! Validate dataflowId Value
    if ( (serviceEntry->dataflowId >= 0) && (serviceEntry->dataflowId <= iswSolNetProviderConsumerMask) )
    {
        std::stringstream ss;
        ss << "Service Entry Dataflow Id Verified" << std::endl;

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            theLogger->DebugMessage(ss.str(), GetIndex());
        }
        std::cout << ss.str();

        //! Validate endpointId
        if ( (serviceEntry->endpointId >= 0) && (serviceEntry->endpointId < 4) )
        {

            std::stringstream ss1;
            ss1 << "Service Entry Endpoint Id Verified" << std::endl;

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(ss1.str(), GetIndex());
            }
            std::cout << ss1.str();

            //! Verify dataPolicies
            if ( (serviceEntry->dataPolicies & DATA_POLICIES_MASK) <= DATA_POLICIES_RESERVED )
            {
                std::stringstream ss2;
                ss2 << "Service Entry Data Policies Verified" << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss2.str(), GetIndex());
                }
                std::cout << ss2.str();

                std::stringstream ss3;

                //! Verify endpointDistribution
                //! Data target (0 = unicast, 0xFF = broadcast, 0x01-0xFE = multicast group ID)
                if ( (serviceEntry->endpointDistribution >= 0x0) &&
                     (serviceEntry->endpointDistribution <= 0xFF) )
                {
                    serviceEntryVerified = true;

                    ss3 << "Service Entry Endpoint Distribution Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                         theLogger->DebugMessage(ss3.str(), GetIndex());
                    }
                    std::cout << ss3.str();
                }
                else
                {
                    ss3 << "Service Entry: Incorrect Endpoint Distribution = " << std::to_string(serviceEntry->endpointDistribution ) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss3.str(), GetIndex());
                    }
                    std::cout << ss3.str();
                }
            }
            else
            {
                std::stringstream ss4;
                ss4 << "Service Entry: Incorrect Data Policies = " << std::to_string(serviceEntry->dataPolicies) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss4.str(), GetIndex());
                }
                std::cout << ss4.str();
            }
        }
    }

    if ( serviceEntryVerified == true )
    {
        if ( serviceEntry->serviceDesc.serviceSelector <= 0xFFFFFFFF )
        {
            //! Verify a valid descriptorId
            switch (serviceEntry->serviceDesc.header.descriptorId)
            {
                case Audio:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Audio Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case PositionSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Position Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case EnvironmentSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Environment Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case HealthSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Health Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case HybridSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Hybrid Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case MotionSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Motion Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case TemperatureSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Temperature Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case MoistureSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Moisture Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case VibrationSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Vibration Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case ChemicalSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Chemical Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case FlowSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Flow Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case ForceSense:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Force Sense Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case RTA_SERVICE:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet RTA Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case LrfService:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet LRF Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case LrfControlService:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet LRF Control Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case ArService:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet AR Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();


                    break;
                }
                case VideoService:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Video Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case Status:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Status Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case SimpleUI:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet SimpleUI Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case PacketGenerator:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Packet Generator Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    break;
                }
                case Control:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Control Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    //! Has more fields than than just service selector in the header
                    if ( serviceEntry->serviceControlDesc.protocolId < 0 )
                    {
                        descriptorIdVerified = false;

                        std::stringstream ss;
                        ss << "Incorrect Control Service Descriptor protocolId Value = " << std::to_string(serviceEntry->serviceControlDesc.protocolId) << std::endl;

                        if ( theLogger->DEBUG_IswSolNet == 1 )
                        {
                            theLogger->DebugMessage(ss.str(), GetIndex());
                        }
                        std::cout << ss.str();
                    }
                    checkChildBlock = false;
                    break;
                }
                case Pipe:
                {
                    descriptorIdVerified = true;

                    std::stringstream ss;
                    ss << "Verifying SolNet Pipe Service Descriptor..." << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();

                    //! Has more fields than than just service selector in the header
                    // TBD
                    checkChildBlock = false;
                    break;
                }
                case Reserved1:
                case ReservedMin:
                case ReservedMax:
                default:
                {
                    descriptorIdVerified = false;
                    checkChildBlock      = false;

                    std::stringstream ss;
                    ss << "Incorrect SolNet Header Descriptor Id = " << std::to_string(serviceEntry->serviceDesc.header.descriptorId) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();
                    break;
                }
            }
        }

        if ( descriptorIdVerified == true )
        {
            std::stringstream ss;
            ss << "Service Entry Header Descriptor ID Verified" << std::endl;
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(ss.str(), GetIndex());
            }
            std::cout << ss.str();

            //! Verify header values
            if ( serviceEntry->serviceDesc.header.length > 0 )
            {
                if ( (serviceEntry->serviceDesc.serviceSelector >= 0) &&
                     (serviceEntry->serviceDesc.serviceSelector < 0xFFFFFFFF) )
                {
                    headerVerified = true;
                }
                else
                {
                    std::stringstream ss;
                    ss << "Service Entry: Incorrect Service Selector = " << std::to_string(serviceEntry->serviceDesc.serviceSelector) << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();
                }
            }
            else
            {
                std::stringstream ss;
                ss << "Service Entry: Incorrect Header Length = " << std::to_string(serviceEntry->serviceDesc.header.length) << std::endl;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), GetIndex());
                }
                std::cout << ss.str();
            }
        }

        if ( headerVerified == true )
        {
            std::stringstream ss;
            ss << "Service Entry Header Length Verified" << std::endl;

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(ss.str(), GetIndex());
            }
            std::cout << ss.str();

            //! Verify the childblock
            size_t childBlockLength = 0;
            if ( serviceEntry->serviceDesc.header.descriptorId == Control )
            {
                childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize - sizeof(serviceEntry->serviceControlDesc.protocolId);
            }
            else if ( serviceEntry->serviceDesc.header.descriptorId == Pipe )
            {
                // TBD
            }
            else
            {
                childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize;
                size_t i = 0;
                uint8_t *bytePtr = (uint8_t *)(serviceEntry->serviceDesc.childBlock);
                char hex[2];
                std::stringstream ss;
                ss << "Video Service Class Descriptor Bytes: ";
                for(i = 0; i < childBlockLength; i++)
                {
                    sprintf(hex, "%x", *(bytePtr+i));
                    ss << hex << " ";
                }
                ss << std::endl;
                std::cout << ss.str();
            }

            if ( (childBlockLength == 0) ||
                 (checkChildBlock == false) )
            {
                childBlockVerified = true;
            }
            else
            {
                bool hasUnicastEndpointDesc   = false;
                bool hasBroadcastEndpointDesc = false;
                bool hasMulticastDesc         = false;

                std::stringstream ss;

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    ss << "Verifying SolNet Property Descriptors..." << std::endl;
                    theLogger->LogMessage(ss.str(), GetIndex(), std::chrono::system_clock::now());
                }
                std::cout << ss.str();

                uint16_t offset = 0;
                while ( offset < childBlockLength )
                {
                    iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)(serviceEntry->serviceDesc.childBlock + offset);

                    //! If this childblock has an Endpoint Desc with distribution set to Multicast, check for a Multicast Group Property Desc
                    if ( commonHeader->descriptorId == Endpoint )
                    {
                        iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)commonHeader;
                        if (endpointDesc->endpointDistribution == 0xFF)
                        {
                            hasBroadcastEndpointDesc = true;
                        }
                        else
                        {
                            hasUnicastEndpointDesc = true;
                        }
                    }

                    if ( commonHeader->descriptorId == MulticastGroup )
                    {
                        hasMulticastDesc = true;
                    }

                    childBlockVerified = VerifySolNetDescriptor(commonHeader, &offset, serviceEntry);

                    if ( (childBlockVerified == false) ||
                         (offset == 0) )
                    {
                        break;
                    }
                }

                //! We assume there is always a Unicast Endpoint descriptor
                //! Then there could be a Broadcast Endpoint descriptor or a Multicast Group descriptor
                //! ICD says if sending a Broadcast Endpoint descriptor they "may" also send Unicast
                if ( !(hasUnicastEndpointDesc || hasBroadcastEndpointDesc) )
                {
                    childBlockVerified = false;
                }

                if ( childBlockVerified == true )
                {
                    std::stringstream ss;
                    ss << "Service Entry: Property Descriptors Verified" << std::endl;

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                       theLogger->DebugMessage(ss.str(), GetIndex());
                    }
                    std::cout << ss.str();
                }
            }
        }
    }

    std::stringstream ss_main2;

    //! If all of these are true, the service has been verified
    if ( (serviceEntryVerified == true) &&
         (headerVerified == true) &&
         (childBlockVerified == true) )
    {
        serviceVerified = true;

        ss_main2 << "SolNet Root Service Descriptor Verified" << std::endl;

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            theLogger->DebugMessage(ss_main2.str(), GetIndex());
        }
        std::cout << ss_main2.str();
    }
    else
    {
        ss_main2 << "Incorrect SolNet Root Service Descriptor" << std::endl;

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            theLogger->DebugMessage(ss_main2.str(), GetIndex());
        }
        std::cout << ss_main2.str();
    }

    std::cout << " " << std::endl;

    return serviceVerified;
}

//!#################################################################################
//! AddSolNetService()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!                     0xFF = Add service for this device; any other valid peerIndex
//!                     indicates the peer that is sending the service announcement
//!         serviceEntry - pointer to iswSolNetServiceEntry with the serviceDescriptor
//!                        and any child descriptors
//! Description: This method adds a new service to the SolNet services table
//!              If peerIndex is defaultPeerIndex => add services to this device
//!              Else add to a peer
//!#################################################################################
void IswSolNet::AddSolNetService(uint8_t peerIndex, iswSolNetServiceEntry *serviceEntry)
{
    std::string msgString = "AddSolNetService";
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    if ( serviceEntry->dataflowId > iswSolNetProviderConsumerMask )
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "dataflowId Is Out of Range!";
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        return;
    }

    switch (serviceEntry->serviceDesc.header.descriptorId)
    {
        case VideoService:
        {
            iswSolNetServiceEntry *newEntry = nullptr;
            //! If it's defaultPeerIndex, then its a service for this device
            //! Add the new serviceEntry to the iswSolNet vector of services
            //! The dataflowId is the index into the table
            if ( peerIndex == defaultPeerIndex )
            {
                iswSolNetServicesLock.lock();
                newEntry = &iswSolNetServices[serviceEntry->dataflowId];

                //! This entry is in use
                newEntry->inUse = true;

                //! Setup the common header
                newEntry->serviceDesc.header.length       = serviceEntry->serviceDesc.header.length;
                newEntry->serviceDesc.header.descriptorId = serviceEntry->serviceDesc.header.descriptorId;

                //! The serviceSelector number will be set - using this as index into the array
                newEntry->serviceDesc.serviceSelector = serviceEntry->serviceDesc.serviceSelector;

                //! Copy the other fields
                newEntry->dataflowId           = serviceEntry->dataflowId;
                newEntry->endpointId           = serviceEntry->endpointId;
                newEntry->dataPolicies         = serviceEntry->dataPolicies;
                newEntry->endpointDistribution = serviceEntry->endpointDistribution;

                // TBD - add these when you add the service
                newEntry->receiveCallbackFn  = nullptr;
                newEntry->pollDataCallbackFn = nullptr;

                //! Initialize all registered peers to empty
                for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
                {
                    RemoveOneRegisteredPeer(serviceEntry->dataflowId, peerIndex, false);

                    //! Allow peers to register for this service by default
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.lock();
                    serviceEntry->registeredPeers[peerIndex].status = RegStatusAvailableToRegister;
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.unlock();
                }

                //! Total length of data in childblock area
                size_t childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize;

                if ( childBlockLength == 0 )
                {
                    //! There should only be one newEntry in this case
                    newEntry->serviceDesc.childBlock = nullptr;
                }
                else
                {
                    //! Allocate the childblock buffer
                    newEntry->serviceDesc.childBlock = new uint8_t[childBlockLength];
                    memcpy(newEntry->serviceDesc.childBlock, serviceEntry->serviceDesc.childBlock, childBlockLength);

                    //! Restore nested QoS data rate descriptors, if applicable
//                    restoreQosDescriptors(&newEntry->serviceDesc);
//                    restoreVideoFormatDescriptors(&newEntry->serviceDesc);
                }
                iswSolNetServicesLock.unlock();

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Adding Local ServiceDesc:" << std::endl;
                    ss << "dataflowId          = " << std::to_string(newEntry->dataflowId) << std::endl;
                    ss << "header.length       = " << std::to_string(newEntry->serviceDesc.header.length) << std::endl;
                    ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceDesc.header.descriptorId) << std::endl;
                    ss << "selectorId          = " << std::to_string(newEntry->serviceDesc.serviceSelector) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                //! This is an incoming Advertisement from another peer
                //! Check whether the user wants us to verify this Advertise Message
                bool serviceVerified = false;
                bool addService = true;
                if ( verifyAdvertiseMessages == true )
                {
                    serviceVerified = VerifySolNetServiceEntry(serviceEntry);

                    if ( serviceVerified == false )
                    {
                        addService = false;
                    }
                }

                if ( addService == true )
                {
                    //! Add this service sent from a peer to their list of services
                    //! Assign a new serviceEntry
                    iswSolNetPeerServiceEntry *newEntry = nullptr;

                    //! Get the lock for this peer
                    iswSolNetPeerServices[peerIndex].peerLock.lock();
                    newEntry = &iswSolNetPeerServices[peerIndex].services[serviceEntry->dataflowId];

                    //! This entry is in use
                    newEntry->inUse        = true;
                    newEntry->ImRegistered = false;

                    //! Setup the common header
                    newEntry->serviceDesc.header.length       = serviceEntry->serviceDesc.header.length;
                    newEntry->serviceDesc.header.descriptorId = serviceEntry->serviceDesc.header.descriptorId;

                    //! The serviceSelector number will be set - using this as index into the array
                    newEntry->serviceDesc.serviceSelector = serviceEntry->serviceDesc.serviceSelector;

                    //! Copy the other fields
                    newEntry->dataflowId           = serviceEntry->dataflowId;

                    newEntry->dataPolicies         = serviceEntry->dataPolicies;

                    newEntry->endpointId           = serviceEntry->endpointId;
                    newEntry->endpointDistribution = serviceEntry->endpointDistribution;

                    newEntry->autonomy             = 0;
                    newEntry->status               = RegStatusAvailableToRegister;
                    newEntry->receiveCallbackFn    = nullptr;

                    //! Total length of data in childblock area
                    size_t childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize;

                    if ( childBlockLength == 0 )
                    {
                        //! There should only be one newEntry in this case
                        newEntry->serviceDesc.childBlock = nullptr;
                    }
                    else
                    {
                        //! Allocate the childblock buffer
                        newEntry->serviceDesc.childBlock = new uint8_t[childBlockLength];
                        memcpy(newEntry->serviceDesc.childBlock, serviceEntry->serviceDesc.childBlock, childBlockLength);

                        //! Restore nested QoS data rate descriptors, if applicable
//                        restoreQosDescriptors(&newEntry->serviceDesc);
                    }
                    iswSolNetPeerServices[peerIndex].inUse = true;
                    iswSolNetPeerServices[peerIndex].peerLock.unlock();

                    std::stringstream ss;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        ss << "Adding Peer ServiceDesc:" << std::endl;
                        ss << "dataflowId          = " << std::to_string(newEntry->dataflowId) << std::endl;
                        ss << "header.length       = " << std::to_string(newEntry->serviceDesc.header.length) << std::endl;
                        ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceDesc.header.descriptorId) << std::endl;
                        ss << "selectorId          = " << std::to_string(newEntry->serviceDesc.serviceSelector) << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }

                    if ( verifyAdvertiseMessages == true )
                    {
                        std::cout << ss.str();
                    }
                }
                else
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Invalid ServiceDesc" << std::endl;
                        ss << "Not Added to Services Table" << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
            }
            break;
        }
        case Audio:
        case PositionSense:
        case EnvironmentSense:
        case HealthSense:
        case SimpleUI:
        case Status:
        case RTA_SERVICE:
        case HybridSense:
        case MotionSense:
        case TemperatureSense:
        case MoistureSense:
        case VibrationSense:
        case ChemicalSense:
        case FlowSense:
        case ForceSense:
        case LrfService:
        case LrfControlService:
        case ArService:
        case Pipe:
        case PacketGenerator:
        {
            iswSolNetServiceEntry *newEntry = nullptr;
            //! If it's defaultPeerIndex, then its a service for this device
            //! Add the new serviceEntry to the iswSolNet vector of services
            //! The dataflowId is the index into the table
            if ( peerIndex == defaultPeerIndex )
            {
                iswSolNetServicesLock.lock();
                newEntry = &iswSolNetServices[serviceEntry->dataflowId];

                //! This entry is in use
                newEntry->inUse = true;

                //! Setup the common header
                newEntry->serviceDesc.header.length       = serviceEntry->serviceDesc.header.length;
                newEntry->serviceDesc.header.descriptorId = serviceEntry->serviceDesc.header.descriptorId;

                //! The serviceSelector number will be set - using this as index into the array
                newEntry->serviceDesc.serviceSelector = serviceEntry->serviceDesc.serviceSelector;

                //! Copy the other fields
                newEntry->dataflowId           = serviceEntry->dataflowId;
                newEntry->endpointId           = serviceEntry->endpointId;
                newEntry->dataPolicies         = serviceEntry->dataPolicies;
                newEntry->endpointDistribution = serviceEntry->endpointDistribution;

                // TBD - add these when you add the service
                newEntry->receiveCallbackFn  = nullptr;
                newEntry->pollDataCallbackFn = nullptr;

                //! Initialize all registered peers to empty
                for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
                {
                    RemoveOneRegisteredPeer(serviceEntry->dataflowId, peerIndex, false);

                    //! Allow peers to register for this service by default
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.lock();
                    serviceEntry->registeredPeers[peerIndex].status = RegStatusAvailableToRegister;
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.unlock();
                }

                //! Total length of data in childblock area
                size_t childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize;

                if ( childBlockLength == 0 )
                {
                    //! There should only be one newEntry in this case
                    newEntry->serviceDesc.childBlock = nullptr;
                }
                else
                {
                    //! Allocate the childblock buffer
                    newEntry->serviceDesc.childBlock = new uint8_t[childBlockLength];
                    memcpy(newEntry->serviceDesc.childBlock, serviceEntry->serviceDesc.childBlock, childBlockLength);
                }
                iswSolNetServicesLock.unlock();

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Adding Local ServiceDesc:" << std::endl;
                    ss << "dataflowId          = " << std::to_string(newEntry->dataflowId) << std::endl;
                    ss << "header.length       = " << std::to_string(newEntry->serviceDesc.header.length) << std::endl;
                    ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceDesc.header.descriptorId) << std::endl;
                    ss << "selectorId          = " << std::to_string(newEntry->serviceDesc.serviceSelector) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                //! This is an incoming Advertisement from another peer
                //! Check whether the user wants us to verify this Advertise Message
                bool serviceVerified = false;
                bool addService = true;
                if ( verifyAdvertiseMessages == true )
                {
                    serviceVerified = VerifySolNetServiceEntry(serviceEntry);

                    if ( serviceVerified == false )
                    {
                        addService = false;
                    }
                }

                if ( addService == true )
                {
                    //! Add this service sent from a peer to their list of services
                    //! Assign a new serviceEntry
                    iswSolNetPeerServiceEntry *newEntry = nullptr;

                    //! Get the lock for this peer
                    iswSolNetPeerServices[peerIndex].peerLock.lock();
                    newEntry = &iswSolNetPeerServices[peerIndex].services[serviceEntry->dataflowId];

                    //! This entry is in use
                    newEntry->inUse        = true;
                    newEntry->ImRegistered = false;

                    //! Setup the common header
                    newEntry->serviceDesc.header.length       = serviceEntry->serviceDesc.header.length;
                    newEntry->serviceDesc.header.descriptorId = serviceEntry->serviceDesc.header.descriptorId;

                    //! The serviceSelector number will be set - using this as index into the array
                    newEntry->serviceDesc.serviceSelector = serviceEntry->serviceDesc.serviceSelector;

                    //! Copy the other fields
                    newEntry->dataflowId           = serviceEntry->dataflowId;

                    newEntry->dataPolicies         = serviceEntry->dataPolicies;

                    newEntry->endpointId           = serviceEntry->endpointId;
                    newEntry->endpointDistribution = serviceEntry->endpointDistribution;

                    newEntry->autonomy             = 0;
                    newEntry->status               = RegStatusAvailableToRegister;
                    newEntry->receiveCallbackFn    = nullptr;

                    //! Total length of data in childblock area
                    size_t childBlockLength = serviceEntry->serviceDesc.header.length - selectorSize;

                    if ( childBlockLength == 0 )
                    {
                        //! There should only be one newEntry in this case
                        newEntry->serviceDesc.childBlock = nullptr;
                    }
                    else
                    {
                        //! Allocate the childblock buffer
                        newEntry->serviceDesc.childBlock = new uint8_t[childBlockLength];
                        memcpy(newEntry->serviceDesc.childBlock, serviceEntry->serviceDesc.childBlock, childBlockLength);
                    }
                    iswSolNetPeerServices[peerIndex].inUse = true;
                    iswSolNetPeerServices[peerIndex].peerLock.unlock();

                    std::stringstream ss;
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        ss << "Adding Peer ServiceDesc:" << std::endl;
                        ss << "dataflowId          = " << std::to_string(newEntry->dataflowId) << std::endl;
                        ss << "header.length       = " << std::to_string(newEntry->serviceDesc.header.length) << std::endl;
                        ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceDesc.header.descriptorId) << std::endl;
                        ss << "selectorId          = " << std::to_string(newEntry->serviceDesc.serviceSelector) << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }

                    if ( verifyAdvertiseMessages == true )
                    {
                        std::cout << ss.str();
                    }
                }
                else
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Invalid ServiceDesc" << std::endl;
                        ss << "Not Added to Services Table" << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
            }
            break;
        }
        case Control:
        {
            //! Assign a new serviceEntry - the index into the array is the serviceSelector number
            iswSolNetServiceEntry *newEntry = nullptr;

            //! If this is for us - this device
            if ( peerIndex == defaultPeerIndex )
            {
                //! Add the new serviceEntry to the iswSolNet vector of services
                iswSolNetServicesLock.lock();
                newEntry = &iswSolNetServices[serviceEntry->dataflowId];

                //! This entry is in use
                newEntry->inUse = true;

                //! Setup the common header
                newEntry->serviceControlDesc.header.length       = serviceEntry->serviceControlDesc.header.length;
                newEntry->serviceControlDesc.header.descriptorId = serviceEntry->serviceControlDesc.header.descriptorId;
                newEntry->serviceControlDesc.protocolId          = serviceEntry->serviceControlDesc.protocolId;
                newEntry->serviceControlDesc.reserved            = 0x0000;

                //! Setup serviceSelector
                newEntry->serviceControlDesc.serviceSelector = serviceEntry->serviceControlDesc.serviceSelector;

                //! Copy the other fields
                newEntry->dataflowId           = serviceEntry->dataflowId;
                newEntry->endpointId           = serviceEntry->endpointId;
                newEntry->dataPolicies         = serviceEntry->dataPolicies;
                newEntry->endpointDistribution = serviceEntry->endpointDistribution;

                // TBD - add these when you add the service
                newEntry->receiveCallbackFn  = nullptr;
                newEntry->pollDataCallbackFn = nullptr;

                //! Initialize all registered peers to empty and available
                for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
                {
                    RemoveOneRegisteredPeer(serviceEntry->dataflowId, peerIndex, false);

                    //! Allow peers to register for this service by default
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.lock();

                    //! This type of descriptor is for information only - peers don't register for it
                    serviceEntry->registeredPeers[peerIndex].status = RegStatusUnavailableForRegistration;
                    serviceEntry->registeredPeers[peerIndex].registeredPeerLock.unlock();
                }

                //! Total length of data in childblock area
                size_t childBlockLength = serviceEntry->serviceControlDesc.header.length - (selectorSize + sizeof(serviceEntry->serviceControlDesc.protocolId) + sizeof(serviceEntry->serviceControlDesc.reserved));

                if ( childBlockLength == 0 )
                {
                    //! There should only be one newEntry in this case
                    newEntry->serviceControlDesc.childBlock = nullptr;
                }
                else
                {
                    //! Allocate the childblock buffer
                    newEntry->serviceControlDesc.childBlock = new uint8_t[childBlockLength];
                    memcpy(newEntry->serviceControlDesc.childBlock, serviceEntry->serviceControlDesc.childBlock, childBlockLength);
                }
                iswSolNetServicesLock.unlock();

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "ServiceControlDesc:" << std::endl;
                    ss << "header.length       = " << std::to_string(newEntry->serviceControlDesc.header.length) << std::endl;
                    ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceControlDesc.header.descriptorId) << std::endl;
                    ss << "selectorId          = " << std::to_string(newEntry->serviceControlDesc.serviceSelector) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                //! This is an incoming Advertisement from another peer
                //! Check whether the user wants us to validate this
                bool serviceVerified = false;
                bool addService = true;
                if ( verifyAdvertiseMessages == true )
                {
                    serviceVerified = VerifySolNetServiceEntry(serviceEntry);

                    if ( !serviceVerified )
                    {
                        addService = false;
                    }
                }

                if ( addService == true )
                {
                    //! Add this service sent from a peer to their list of services
                    //! Assign a new serviceEntry - the index into the array is the serviceSelector number
                    iswSolNetPeerServiceEntry *newEntry = nullptr;

                    //! Get the lock for this peer
                    iswSolNetPeerServices[peerIndex].peerLock.lock();
                    newEntry = &iswSolNetPeerServices[peerIndex].services[serviceEntry->dataflowId];

                    //! This entry is in use
                    newEntry->inUse        = true;
                    newEntry->ImRegistered = false;

                    //! Setup the common header
                    newEntry->serviceControlDesc.header.length       = serviceEntry->serviceControlDesc.header.length;
                    newEntry->serviceControlDesc.header.descriptorId = serviceEntry->serviceControlDesc.header.descriptorId;
                    newEntry->serviceControlDesc.protocolId          = serviceEntry->serviceControlDesc.protocolId;
                    newEntry->serviceControlDesc.reserved            = 0x0000;

                    //! Setup serviceSelector
                    newEntry->serviceControlDesc.serviceSelector = serviceEntry->serviceControlDesc.serviceSelector;

                    //! Copy the other fields
                    newEntry->dataflowId           = serviceEntry->dataflowId;
                    newEntry->endpointId           = serviceEntry->endpointId;
                    newEntry->dataPolicies         = serviceEntry->dataPolicies;
                    newEntry->endpointDistribution = serviceEntry->endpointDistribution;
                    newEntry->autonomy             = 0;

                    //! This type of descriptor is for informational purposes and not available to register
                    newEntry->status            = RegStatusUnavailableForRegistration;
                    newEntry->receiveCallbackFn = nullptr;

                    //! Total length of data in childblock area
                    size_t childBlockLength = serviceEntry->serviceControlDesc.header.length - (selectorSize + sizeof(serviceEntry->serviceControlDesc.protocolId) + sizeof(serviceEntry->serviceControlDesc.reserved));

                    if ( childBlockLength == 0 )
                    {
                        //! There should only be one newEntry in this case
                        newEntry->serviceControlDesc.childBlock = nullptr;
                    }
                    else
                    {
                        //! Allocate the childblock buffer
                        newEntry->serviceControlDesc.childBlock = new uint8_t[childBlockLength];
                        memcpy(newEntry->serviceControlDesc.childBlock, serviceEntry->serviceControlDesc.childBlock, childBlockLength);
                    }
                    iswSolNetPeerServices[peerIndex].inUse = true;
                    iswSolNetPeerServices[peerIndex].peerLock.unlock();

                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "ServiceControlDesc:" << std::endl;
                        ss << "header.length       = " << std::to_string(newEntry->serviceControlDesc.header.length) << std::endl;
                        ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceControlDesc.header.descriptorId) << std::endl;
                        ss << "selectorId          = " << std::to_string(newEntry->serviceControlDesc.serviceSelector) << std::endl;

                        if ( newEntry->serviceControlDesc.childBlock != nullptr )
                        {
                            ss << "childblock Length=" << std::to_string(childBlockLength) << std::endl;

                            uint16_t offset = 0;
                            while ( offset < childBlockLength )
                            {
                                //! Print childblock descriptors
                                iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)&newEntry->serviceControlDesc.childBlock[offset];
                                if ( (commonHeader != nullptr) && (commonHeader->length > 0) )
                                {
                                    uint8_t *data = &newEntry->serviceControlDesc.childBlock[offset];
                                    ss << "header.length       = " << std::to_string(commonHeader->length) << std::endl;
                                    ss << "header.descriptorId = ";
                                    PrintDescriptorIdToLogfile(&ss, commonHeader->descriptorId, data);
                                    offset += commonHeader->length + commonHeaderSize;
                                }
                            }
                        }
                        else
                        {
                            ss << "childblock = nullptr" << std::endl;
                        }
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
                else
                {
                    if ( theLogger->DEBUG_IswSolNet == 1 )
                    {
                        std::stringstream ss;
                        ss << "Invalid ServiceControlDesc:" << std::endl;
                        ss << "header.length       = " << std::to_string(newEntry->serviceControlDesc.header.length) << std::endl;
                        ss << "header.descriptorId = " << GetServiceIdString(newEntry->serviceControlDesc.header.descriptorId) << std::endl;
                        ss << "selectorId          = " << std::to_string(newEntry->serviceControlDesc.serviceSelector) << std::endl;
                        ss << "Not Added to services table:" << std::endl;
                        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                    }
                }
            }
            break;
        }
        case Reserved1:
//        case Reserved2:
        case ReservedMin:
        case ReservedMax:
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::string msgString = "Unused SolNet DescriptorId";
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!#################################################################################
//! SetBrowse()
//! Inputs: browse - boolean true/false
//! Description: This method sets the peerServices sendBrowse value used by
//!              HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::SetBrowse(uint8_t peerIndex, bool browse)
{
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].inUse = true;
    iswSolNetPeerServices[peerIndex].sendBrowse = browse;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SetBrowse to  " << std::to_string(browse) << " for peer = " << std::to_string(peerIndex) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! SetAdvertise()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         advertise - boolean true/false
//! Description: This method sets the peerServices sendAdvertise value used by
//!              HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::SetAdvertise(uint8_t peerIndex, bool advertise)
{
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].inUse = true;
    iswSolNetPeerServices[peerIndex].sendAdvertise = advertise;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SetAdvertise to  " << std::to_string(advertise) << " for peer = " << std::to_string(peerIndex) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! SetImRegistered()
//! Inputs: peerIndex  - ISW representation of the peer to this node
//!         dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         registered - boolean true/false
//! Description: This method sets the peerServices ImRegistered value used by
//!              HandleSolNetSendEvents() and other methods. SolNet only performs
//!              actions if this device is registered for the peer services.
//!#################################################################################
void IswSolNet::SetImRegistered(uint8_t peerIndex, uint8_t dataflowId, bool registered)
{
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].inUse = true;
    iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered = registered;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SetImRegistered to  " << std::to_string(registered) << " for peer = " << std::to_string(peerIndex) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! SetSendGetStatusResponse()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         sendStatus - boolean true/false
//!         seqNumber - sequence number of the Get Status Request packet
//! Description: This method sets the peerServices sendStatus value used by
//!              HandleSolNetSendEvents() indicating to send the Get Status Response
//!#################################################################################
void IswSolNet::SetSendGetStatusResponse(uint8_t peerIndex, uint32_t serviceSelector, bool sendStatus, uint8_t seqNumber)
{
    for (uint8_t i = 0; i < MAX_NUMBER_APPS; i++)
    {
        if ( (iswSolNetServices[i].inUse) && (iswSolNetServices[i].serviceDesc.serviceSelector == serviceSelector) )
        {
            iswSolNetServices[i].registeredPeers[peerIndex].registeredPeerLock.lock();
            iswSolNetServices[i].registeredPeers[peerIndex].sendGetStatusResponse  = sendStatus;
            iswSolNetServices[i].registeredPeers[peerIndex].getStatusResponseSeqNo = seqNumber;
            iswSolNetServices[i].registeredPeers[peerIndex].registeredPeerLock.unlock();
            break;
        }
    }

    if (theLogger->DEBUG_IswSolNet == 1)
    {
        std::stringstream ss;
        ss << "SetSendStatus to  " << std::to_string(sendStatus) << " for peer = " << std::to_string(peerIndex) << " for serviceSelector=" << std::to_string(serviceSelector) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! SetRegistrationStatus()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         registrationStatus - 0 - 4
//! Description: This method sets the peerServices registration status value used by
//!              HandleSolNetSendEvents(). See iswSolNetRegistrationStatus enum
//!#################################################################################
void IswSolNet::SetRegistrationStatus(uint8_t peerIndex, uint32_t serviceSelector, uint8_t registrationStatus)
{
    iswSolNetServicesLock.lock();
    for (uint8_t i = 0; i < MAX_NUMBER_APPS; i++)
    {
        if ( (iswSolNetServices[i].inUse) && (iswSolNetServices[i].serviceDesc.serviceSelector == serviceSelector) )
        {
            iswSolNetServices[i].registeredPeers[peerIndex].registeredPeerLock.lock();
            iswSolNetServices[i].registeredPeers[peerIndex].status = registrationStatus;
            iswSolNetServices[i].registeredPeers[peerIndex].registeredPeerLock.unlock();
            break;
        }
    }
    iswSolNetServicesLock.unlock();

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SetRegistrationStatus to  " << std::to_string(registrationStatus) << " for peer = " << std::to_string(peerIndex) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! SetSendRevokeNetworkAssociationResponse()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the RevokeNetworkAssociationRequest pkt
//! Description: This method sets the peerServices sendRevokeNetworkAssociationResponse
//!              value to true - used by HandleSolNetSendEvents() - indicating to
//!              send the Revoke Network Response message
//!#################################################################################
void IswSolNet::SetSendRevokeNetworkAssociationResponse(uint8_t peerIndex, uint8_t seqNumber)
{
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse  = true;
    iswSolNetPeerServices[peerIndex].revokeNetworkAssociationResponseSeqNo = seqNumber;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss << "SetSendRevokeNetworkAssociationResponse for peer = " << std::to_string(peerIndex) << std::endl;
        theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
}

//!#################################################################################
//! UpdateServiceWithPeerRegisterRequest()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         regRequest - pointer to SolNet Register Request message
//!         seqNumber - sequence number of the RevokeNetworkAssociationRequest pkt
//! Description: This method sets the peerServices with the message data
//!              used by HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::UpdateServiceWithPeerRegisterRequest(uint8_t peerIndex, iswSolNetRegisterRequest *regRequest, uint8_t seqNumber)
{
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( iswSolNetServices[dataflowId].inUse &&
             (iswSolNetServices[dataflowId].serviceDesc.serviceSelector == regRequest->serviceSelector) )
        {
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();

            if ( (iswSolNetServices[dataflowId].registeredPeers[peerIndex].status != RegStatusUnavailable) &&
                 (iswSolNetServices[dataflowId].registeredPeers[peerIndex].status != RegStatusUnavailableForRegistration) )
            {
                //! Register this peer to receive service data
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].peerRegistered = true;
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomy       = regRequest->autonomy;
            }
            else
            {
                //! This peer can't register to receive service data
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].peerRegistered = false;
            }

            //! Indicate to the background thread to send a response
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendRegRequestResponse  = true;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].regRequestResponseSeqNo = seqNumber;

            //! Wait for Autonomous Start/Stop Message to set this
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].flowState = 0;

            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
            break;
        }
    }
}

//!#################################################################################
//! UpdateServiceWithPeerDeregisterRequest()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         seqNumber - sequence number of the RevokeNetworkAssociationRequest pkt
//! Description: This method sets the peerServices flag sendDeregistrationRequestResponse
//!              to true - used by HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::UpdateServiceWithPeerDeregisterRequest(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber)
{
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( iswSolNetServices[dataflowId].inUse )
        {
            //! If deregistering all services or just this one
            if ( (serviceSelector == 0xFFFFFFFF) || (serviceSelector == iswSolNetServices[dataflowId].serviceDesc.serviceSelector) )
            {
                //! Indicate to send a response
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].peerRegistered                    = false;
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendDeregistrationRequestResponse = true;
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].deregRequestResponseSeqNo         = seqNumber;
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();

                if (serviceSelector == iswSolNetServices[dataflowId].serviceDesc.serviceSelector)
                {
                    break;
                }
            }
        }
    }
}

//!#################################################################################
//! UpdatePeerServiceWithRevokeRegistration()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         seqNumber - sequence number of the RevokeNetworkAssociationRequest pkt
//! Description: This method sets the peerServices status to unavailable -
//!              used by HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::UpdatePeerServiceWithRevokeRegistration(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber)
{
    if ( iswSolNetPeerServices[peerIndex].inUse )
    {
        //! If all services were revoked - remove all services for this peer
        if ( serviceSelector == 0xFFFFFFFF )
        {
            RemoveAllServicesForOnePeer(peerIndex, false);
        }
        else
        {
            for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
            {
                if ( iswSolNetPeerServices[peerIndex].services[dataflowId].inUse )
                {
                    if ( serviceSelector == 0xFFFFFFFF )
                    {
                        iswSolNetPeerServices[peerIndex].peerLock.lock();
                        iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusUnavailableForRegistration;
                        iswSolNetPeerServices[peerIndex].peerLock.unlock();
                    }
                    else
                    {
                        if ( iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector )
                        {
                            //! If this is the service that was revoked - remove just this service for this peer
                            RemoveOneServiceForOnePeer(peerIndex, dataflowId, false);
                            iswSolNetPeerServices[peerIndex].peerLock.lock();
                            iswSolNetPeerServices[peerIndex].services[dataflowId].status = RegStatusUnavailableForRegistration;
                            iswSolNetPeerServices[peerIndex].peerLock.unlock();
                            break;
                        }
                    }
                }
            }
        }
    }
    //! Set these for HandleSolNetSendEvents thread
    iswSolNetPeerServices[peerIndex].peerLock.lock();
    iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo  = seqNumber;
    iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse = true;
    iswSolNetPeerServices[peerIndex].peerLock.unlock();
}

//!#################################################################################
//! UpdateServiceWithPeerPollDataRequest()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         seqNumber - sequence number of the SolNet PollDataRequest pkt
//! Description: This method sets the peerServices status to unavailable -
//!              used by HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::UpdateServiceWithPeerPollDataRequest(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber)
{
    //! Set these for HandleSolNetSendEvents thread
    iswSolNetServicesLock.lock();
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( iswSolNetServices[dataflowId].inUse && (iswSolNetServices[dataflowId].serviceDesc.serviceSelector == serviceSelector) )
        {
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].pollDataResponseSeqNo = seqNumber;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendPollDataResponse  = true;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
            break;
        }
    }
    iswSolNetServicesLock.unlock();
}

//!#################################################################################
//! UpdateServiceWithPeerFlowState()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         autonomousStartStopMsg - pointer to iswSolNetAutonomousStartStopRequest
//!         seqNumber - sequence number of the SolNet PollDataRequest pkt
//! Description: This method sets the peerServices flow state and autonomouse start
//!              stop data - used by HandleSolNetSendEvents()
//!#################################################################################
void IswSolNet::UpdateServiceWithPeerFlowState(uint8_t peerIndex, iswSolNetAutonomousStartStopRequest *autonomousStartStopMsg, uint8_t seqNumber)
{
    iswSolNetServicesLock.lock();
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( !iswSolNetServices[dataflowId].inUse )
        {
            continue;
        }

//        std::cout << "iswSolNetServices[dataflowId].serviceDesc.serviceSelector = " << std::to_string(iswSolNetServices[dataflowId].serviceDesc.serviceSelector) << std::endl;
//        std::cout << "autonomousStartStopMsg->serviceSelector                   = " << std::to_string(autonomousStartStopMsg->serviceSelector) << std::endl;

        if ( iswSolNetServices[dataflowId].serviceDesc.serviceSelector == autonomousStartStopMsg->serviceSelector )
        {
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].flowState                       = autonomousStartStopMsg->flowState;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendAutonomousStartStopResponse = true;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].autonomousStartStopSeqNo        = seqNumber;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
            break;
        }
    }
    iswSolNetServicesLock.unlock();
}

//!#################################################################################
//! UpdateServiceWithKeepAliveInitiator()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         keepAliveRequestMsg - pointer to iswSolNetKeepAliveRequest
//!         seqNumber - sequence number of the SolNet KeepAliverRequest pkt
//! Description: This method sets the peerServices keep alive initiator
//!#################################################################################
void IswSolNet::UpdatePeerServiceWithKeepAliveInitiator(uint8_t peerIndex, iswSolNetKeepAliveRequest *keepAliveRequest, uint8_t seqNumber)
{
    iswSolNetServicesLock.lock();
    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( !iswSolNetServices[dataflowId].inUse )
        {
            continue;
        }

        if ( iswSolNetServices[dataflowId].serviceDesc.serviceSelector == keepAliveRequest->serviceSelector )
        {
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].initiator              = keepAliveRequest->initiator;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendKeepAliveResponse  = true;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].keepAliveResponseSeqNo = seqNumber;
            iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();

            std::cout << "Initiator             = " << std::to_string(iswSolNetServices[dataflowId].registeredPeers[peerIndex].initiator) << std::endl;
            std::cout << "SendKeepAliveResponse = " << std::to_string(iswSolNetServices[dataflowId].registeredPeers[peerIndex].sendKeepAliveResponse) << std::endl;
            std::cout << "Sequence Number       = " << std::to_string(iswSolNetServices[dataflowId].registeredPeers[peerIndex].keepAliveResponseSeqNo) << std::endl;
            break;
        }
    }
    iswSolNetServicesLock.unlock();
}

//!#################################################################################
//! IswChecksum()
//! Inputs: count - size of bytes of data
//!         addr - pointer to data buffer to run checksum on
//! Description: This method sets the peerServices flow state and autonomouse start
//!              stop data - used by HandleSolNetSendEvents()
//!#################################################################################
uint16_t IswSolNet::IswChecksum(uint32_t count, uint16_t *addr)
{
    if ( addr == nullptr )
    {
        return -1;
    }

    //! From the ISW Embedment Guide Section 15.2.4
    uint32_t sum = 0;

    while( count > 1 )
    {
        /* Sum 16 bits at a time */
        sum += (uint16_t)(*addr);
        addr++;
        count -= 2;
    }

    /* Add left-over byte, if any */
    if( count > 0 )
        sum += *((uint8_t *)addr);

    /* Fold 32-bit sum to 16 bits */
    while (sum >> 16)
    {
        sum = (sum & 0xFFFF) + (sum >> 16);
    }

    /* Compute one’s compliment */
    sum = (uint16_t)(~sum);

    return (sum != 0 ? sum : 0xFFFF);
}

//!#################################################################################
//! CalculateHeaderChecksum()
//! Inputs: iswSolNetHdr - pointer to the SolNet header
//! Description: This method calculates the SolNet header checksum for an outgoing
//!              packet
//!#################################################################################
void IswSolNet::CalculateHeaderChecksum(iswSolNetHeader *iswSolNetHdr)
{
    uint16_t *pktPtr = nullptr;

    switch ( iswSolNetHdr->protocolClass )
    {
        case DataPacket:
        {
            //! Set header checksum field to zero first
            iswSolNetHdr->iswSolNetDataPkt.headerChecksum = 0;

            // The checksum includes SolNet Data Header + Protocol Version + Protocol Class
            uint32_t count = dataPacketHdrSize + 2;
            pktPtr         = (uint16_t *)(iswSolNetHdr);

            iswSolNetHdr->iswSolNetDataPkt.headerChecksum = IswChecksum(count, pktPtr);
            break;
        }
        case MessagePacket:
        {
            //! Set header checksum field to zero first
            iswSolNetHdr->iswSolNetMessagePkt.headerChecksum = 0;

            // The checksum includes SolNet Message Header + Protocol Version + Protocol Class
            uint32_t count = messagePacketHdrSize + 2;
            pktPtr         = (uint16_t *)(iswSolNetHdr);

            iswSolNetHdr->iswSolNetMessagePkt.headerChecksum = IswChecksum(count, pktPtr);
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::string msgString = "CalculateHeaderChecksum Error - Bad SolNet Packet Type";
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
 }

//!#################################################################################
//!
//! SolNet Class is a little different than the other ISW friend classes in that
//! it sends either a SolNet Message packet or a SolNet Data Packet, so there is
//! one "setup" function for each.  The "send" functions below all call
//! SendSolNetMessagePacket()
//!
//!#################################################################################
//!#################################################################################
//!
//! Discovery Protocol Messages
//!
//!#################################################################################

//!###################################################################
//! SendBrowseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendBrowseMessage(uint8_t peerIndex)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendBrowsePacket";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the SolNet Browse Message to this peer
    uint8_t messageClass   = Discovery;
    uint8_t messageId      = BrowseServices;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, 0, MsgSuccess);

    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::stringstream ss;
        ss <<  "Sending BrowseServices Message to Peer = " << std::to_string(peerIndex) << std::endl;
        iswInterface->theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    return ( status );
}

//!###################################################################
//! SendAdvertiseMessage()
//! Inputs: peerIndex  - ISW representation of the peer to this node
//!         dataflowId - Bits 0 – 6: Dataflow Number (0 – 127)
//!                      Bit 7: Role (0 = producer, 1 = consumer)
//!                      Also the index into the services table
//!         policies   - POLICY_PAYLOAD_CHECKSUM = 0x01;
//!                      POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
//!                      POLICY_NO_ACK = 0x00;
//!                      POLICY_ACK = 0x02;
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendAdvertiseMessage(uint8_t peerIndex, uint8_t dataflowId, uint8_t policies)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendAdvertiseMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the SolNet Advertise Message to this peer
    uint8_t messageClass = Discovery;
    uint8_t messageId    = ServiceAdvertiseAnnounce;

    int status = 0;
    //! Get the service and send
    iswSolNetServiceEntry *serviceEntry = &iswSolNetServices[dataflowId];

    if ( !serviceEntry->inUse )
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::string msgString = "service Not inUse for Index = ";
            msgString.append(std::to_string(dataflowId));
            iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        status = -1;
        return (status);
    }

    switch ( serviceEntry->serviceDesc.header.descriptorId )
    {
        case VideoService:
        case Audio:
        case PositionSense:
        case EnvironmentSense:
        case HealthSense:
        case SimpleUI:
        case Status:
        case RTA_SERVICE:
        case HybridSense:
        case MotionSense:
        case TemperatureSense:
        case MoistureSense:
        case VibrationSense:
        case ChemicalSense:
        case FlowSense:
        case ForceSense:
        case LrfService:
        case LrfControlService:
        case ArService:
        case Pipe:
        case PacketGenerator:
        {
            //! Initialize
            uint8_t payload[payloadMsgBufferSize];
            memset(payload, 0, payloadMsgBufferSize);

            if ( serviceEntry->serviceDesc.header.length <= payloadMsgBufferSize )
            {
                //! Copy the entry header into the message payload
                memcpy(payload, &serviceEntry->serviceDesc, sizeof(iswSolNetServiceDesc));

                if ( serviceEntry->serviceDesc.childBlock != nullptr )
                {
                    //! Offset to where childblock pointer is
                    uint16_t offset = commonHeaderSize + selectorSize;
                    //! How much data we can add past the offset based on Message Packet headers in a USB frame
                    uint16_t childblockLength = serviceEntry->serviceDesc.header.length - selectorSize;

                    //! Copy any child descriptors into the message payload just past the headers
                    memcpy(&payload[offset], serviceEntry->serviceDesc.childBlock, childblockLength);
                }

                //! Send the Advertise Message
                status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, (serviceEntry->serviceDesc.header.length + commonHeaderSize), false, 0, MsgSuccess);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss <<  "sending AdvertiseMessage for dataflowId = " << std::to_string(serviceEntry->dataflowId) << " to peer " << std::to_string(peerIndex) << std::endl;
                    iswInterface->theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                //! We shouldn't hit this case!  It was handled in AddSolNetService() so that each entry is the right size.
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss <<  "Advertise service too big for Message Packet" << std::endl;
                    iswInterface->theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case Control:
        {
            //! Initialize
            uint8_t payload[payloadMsgBufferSize];
            memset(payload, 0, payloadMsgBufferSize);

            if ( serviceEntry->serviceControlDesc.header.length <= payloadMsgBufferSize )
            {
                //! Copy the entry header into the message payload
                memcpy(payload, &serviceEntry->serviceControlDesc, sizeof(iswSolNetServiceControlDesc));

                if ( serviceEntry->serviceControlDesc.childBlock != nullptr )
                {
                    //! Offset to where childblock pointer is
                    uint8_t offset = commonHeaderSize + selectorSize + sizeof(serviceEntry->serviceControlDesc.protocolId) + sizeof(serviceEntry->serviceControlDesc.reserved);

                    //! How much data we can add past the offset based on Message Packet headers in a USB frame
                    uint16_t childblockLength = serviceEntry->serviceControlDesc.header.length - (selectorSize + sizeof(serviceEntry->serviceControlDesc.protocolId) + sizeof(serviceEntry->serviceControlDesc.reserved));

                    //! Copy any child descriptors into the message payload just past the headers
                    memcpy(&payload[offset], serviceEntry->serviceControlDesc.childBlock, childblockLength);
                }

                //! Send the Advertise Message
                status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, (serviceEntry->serviceControlDesc.header.length + commonHeaderSize), false, 0, MsgSuccess);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss <<  "Sending Advertise Message to Peer " << std::to_string(peerIndex) << std::endl;
                    iswInterface->theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                //! We shouldn't hit this case!  It was handled in AddSolNetService() so that each entry is the right size.
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss <<  "Advertise service too big for Message Packet" << std::endl;
                    iswInterface->theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            break;
        }
        case Reserved1:
//        case Reserved2:
        case ReservedMin:
        case ReservedMax:
        default:
        {
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::string msgString = "Unused SolNet DescriptorId";
                iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }

    return ( status );
}

//!###################################################################
//! SendAdvertiseChangeMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendAdvertiseChangeMessage(uint8_t peerIndex)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendAdvertiseChangeMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the SolNet Advertise Change Message to this peer
    uint8_t messageClass   = Discovery;
    uint8_t messageId      = ServiceAdvertiseChange;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, 0, MsgSuccess);

    return ( status );
}

//!###################################################################
//!
//! Flow Protocol Messages
//!
//!###################################################################

//!###################################################################
//! SendRegisterRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         autonomy - 0 = wait to be polled; 1 = send automatically to
//!                    registered nodes
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendRegisterRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t autonomy)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRegisterRequestMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    bool foundService = false;
    int status = 0;

    for (uint8_t dataflowId = 0; dataflowId < MAX_PEERS; dataflowId++)
    {
        if ( (iswSolNetPeerServices[peerIndex].inUse) &&
             (iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector) )
        {
            foundService = true;
            //! Check that we have not been revoked by the peer for this service
            if ( (iswSolNetPeerServices[peerIndex].services[dataflowId].status != RegStatusUnavailable) &&
                 (iswSolNetPeerServices[peerIndex].services[dataflowId].status != RegStatusUnavailableForRegistration) )
            {
                //! Send the RegisterRequestMessage to this peer
                uint8_t messageClass   = Flow;
                uint8_t messageId      = RegisterRequest;
                uint8_t policies       = 0;
                uint16_t payloadLength = sizeof(iswSolNetRegisterRequest);
                uint8_t payload[payloadMsgBufferSize];
                memset(payload, 0, payloadMsgBufferSize);

                //! Fill in the payload with the Register Request message
                iswSolNetRegisterRequest *registerRequest = (iswSolNetRegisterRequest *)payload;
                registerRequest->serviceSelector          = serviceSelector;
                registerRequest->autonomy                 = autonomy;

                bool save = true;
                status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Device " << std::to_string(GetIndex()) << " Sending RegisterRequest to Peer = " << std::to_string(peerIndex) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
            }
            else
            {
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    std::stringstream ss;
                    ss << "Device " << std::to_string(GetIndex()) << " Not Sending RegisterRequest to Peer = " << std::to_string(peerIndex) << std::endl;
                    theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                status = -1;
            }
            break;
        }
    }

    if ( !foundService )
    {
        status = -1;
    }

    return ( status );
}

//!###################################################################
//! SendRegisterResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Register Request
//!                     Message packet
//!         msgStatus - see iswSolNetMsgStatus
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendRegisterResponseMessage(uint8_t peerIndex, uint8_t seqNumber, uint8_t msgStatus)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRegisterResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the RegisterResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = RegisterResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, msgStatus);

    return ( status );
}

//!###################################################################
//! SendReportDataflowConditionIndication()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - Service for which the condition has occurred
//!         endpointNumber - Endpoint for which the condition has occurred
//!         dataflowId - Dataflow ID for which the condition has occurred
//!         condition - Condition that has occurred
//!         msgStatus - see iswSolNetMsgStatus
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendReportDataflowConditionIndication(uint8_t peerIndex, uint32_t serviceSelector, uint8_t endpointNumber, uint8_t dataflowId, uint8_t seqNumber, uint8_t condition)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendReportDataflowConditionIndication";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the ReportDataflowCondition to this peer
    uint8_t messageClass   = Reporting;
    uint8_t messageId      = ReportDataflowConditionIndication;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    iswSolNetReportDataflowConditionIndication *reportDataflowConditionIndication = (iswSolNetReportDataflowConditionIndication *)payload;
    reportDataflowConditionIndication->serviceSelector                            = serviceSelector;
    reportDataflowConditionIndication->endpointNumber                             = endpointNumber;
    reportDataflowConditionIndication->dataflowId                                 = dataflowId;
    reportDataflowConditionIndication->condition                                  = condition;

    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!###################################################################
//! SendAutonomousStartStopRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         flowState - 0 = stop dataflow, 1 = start dataflow
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendAutonomousStartStopRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t flowState)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendAutonomousStartStopMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the AutonomousStartStopRequestMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = AutonomousStartStopRequest;
    uint8_t policies       = 0;
    uint16_t payloadLength = sizeof(iswSolNetAutonomousStartStopRequest);
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    iswSolNetAutonomousStartStopRequest *autonomousStartStopMsg = (iswSolNetAutonomousStartStopRequest *)payload;
    autonomousStartStopMsg->serviceSelector = serviceSelector;
    autonomousStartStopMsg->flowState       = flowState; //0 = stop dataflow; 1 = start dataflow

    int status = 0;

    bool save = true;
    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    return ( status );
}

//!###################################################################
//! SendAutonomousStartStopResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//!         flowState - 0=stop dataflow, 1=start dataflow
//!         seqNumber - sequence number of the SolNet Autonomous Start
//!                     Stop Request Message
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendAutonomousStartStopResponseMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t flowState, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendAutonomousStartStopResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the AutonomousStartStopResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = AutonomousStartStopResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    iswSolNetAutonomousStartStopRequest autonomousStartStopMsg;
    autonomousStartStopMsg.serviceSelector = serviceSelector;
    autonomousStartStopMsg.flowState       = flowState;
    autonomousStartStopMsg.reserved[0]     = 0;
    autonomousStartStopMsg.reserved[1]     = 0;
    autonomousStartStopMsg.reserved[2]     = 0;
    memcpy(payload, &autonomousStartStopMsg, sizeof(iswSolNetAutonomousStartStopRequest));
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    //std::cout << "Device " << std::to_string(GetIndex()) << " Sending AutonomousStartStopResponse to peer=" << std::to_string(peerIndex) << std::endl;

    return ( status );
}

//!###################################################################
//! SendDeregisterRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendDeregisterRequestMessage(uint8_t peerIndex, uint32_t serviceSelector)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendDeregisterRequestMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    bool foundService = false;
    int status = 0;

    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( (iswSolNetPeerServices[peerIndex].services[dataflowId].inUse) &&
             (iswSolNetPeerServices[peerIndex].services[dataflowId].serviceDesc.serviceSelector == serviceSelector) )
        {
            foundService = true;
            //! Don't bother sending if we are already not registered or revoked
            if ( iswSolNetPeerServices[peerIndex].services[dataflowId].ImRegistered )
            {
                if ( (iswSolNetPeerServices[peerIndex].services[dataflowId].status != RegStatusUnavailableForRegistration) &&
                     (iswSolNetPeerServices[peerIndex].services[dataflowId].status != RegStatusUnavailable) )
                {
                    //! Send the DeregisterRequestMessage to this peer
                    uint8_t messageClass   = Flow;
                    uint8_t messageId      = DeregisterRequest;
                    uint8_t policies       = 0;
                    uint16_t payloadLength = selectorSize;
                    uint8_t payload[payloadMsgBufferSize];
                    memset(payload, 0, payloadMsgBufferSize);

                    //! Fill in the payload with the DeRegister Request serviceSelector
                    memcpy(payload, &serviceSelector, selectorSize);

                    bool save = true;
                    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);
                }
                else
                {
                    //! Then you are revoked - nothing to do
                    //! status is 0
                }
            }
            else
            {
                //! Then you are deregistered - nothing to do
                //! status is 0
            }
            break;
        }
    }

    if ( !foundService )
    {
        //! Then you are deregistered - nothing to do
        //! status is 0
    }

    return ( status );
}

//!###################################################################
//! SendDeregisterResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Deregister
//!                     Request Message
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswSolNet::SendDeregisterResponseMessage(uint8_t peerIndex, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendDeregisterResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the DeregisterResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = DeregisterResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendRevokeRegistrationMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendRevokeRegistrationMessage(uint8_t peerIndex, uint32_t serviceSelector)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRevokeRegistrationMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the RevokeRegistrationMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = RevokeRegistration;
    uint8_t policies       = 0;
    uint16_t payloadLength = selectorSize;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    //! Fill in the payload with the RevokeRegistration serviceSelector
    memcpy(payload, &serviceSelector, selectorSize);

    bool save = true;
    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        if ( iswSolNetServices[dataflowId].inUse )
        {
            //! If all services are revoked for this peer or just the matching service
            if ( (serviceSelector == 0xFFFFFFFF) || (iswSolNetServices[dataflowId].serviceDesc.serviceSelector == serviceSelector) )
            {
                RemoveOneRegisteredPeer(dataflowId, peerIndex, false);
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].status = RegStatusUnavailableForRegistration;
                iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();

                if (iswSolNetServices[dataflowId].serviceDesc.serviceSelector == serviceSelector)
                {
                    break;
                }
            }
        }
    }

    return ( status );
}

//!#######################################################################
//! SendRevokeRegistrationResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Revoke Registration
//!                     Request Message
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendRevokeRegistrationResponseMessage(uint8_t peerIndex, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRevokeRegistrationResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the RevokeRegistrationResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = RevokeRegistrationResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendGetStatusRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendGetStatusRequestMessage(uint8_t peerIndex, uint32_t serviceSelector)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendGetStatusRequestMessage";
        iswInterface->theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the GetStatusRequestMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = GetStatusRequest;
    uint8_t policies       = 0;
    uint16_t payloadLength = selectorSize;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    //! Fill in the payload with the GetStatusRequest serviceSelector
    memcpy(payload, &serviceSelector, selectorSize);

    bool save = true;
    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    return (status);
}

//!#######################################################################
//! SendGetStatusResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceStatus - see iswSolNetRegistrationStatus
//!         seqNumber - sequence number of the SolNet Revoke Registration
//!                     Request Message
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendGetStatusResponseMessage(uint8_t peerIndex, uint8_t serviceStatus, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendGetStatusResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send GetStatusResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = GetStatusResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = sizeof(iswSolNetGetStatusResponse);
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    iswSolNetGetStatusResponse statusResponse;
    statusResponse.serviceStatus = serviceStatus;
    memcpy(payload, &statusResponse, sizeof(iswSolNetGetStatusResponse));

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendRevokeNetworkAssociationRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendRevokeNetworkAssociationRequestMessage(uint8_t peerIndex)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRevokeNetworkAssociationRequestMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send theRevokeNetworkAssociationRequestMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = RevokeNetworkAssociationRequest;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    int status = 0;
    bool save  = true;
    status     = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendRevokeNetworkAssociationResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Network Association
//!                     Request Message packet
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendRevokeNetworkAssociationResponseMessage(uint8_t peerIndex, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendRevokeRegistrationResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the RevokeNetworkAssociationResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = RevokeNetworkAssociationResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendPollDataRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendPollDataRequestMessage(uint8_t peerIndex, uint32_t serviceSelector)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendPollDataRequestMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the PollDataRequestMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = PollDataRequest;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    //! Fill in the payload with the serviceSelector
    memcpy(payload, &serviceSelector, selectorSize);

    int status = 0;
    bool save  = true;
    status     = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendPollDataResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Poll Data Request
//!                     Message packet
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendPollDataResponseMessage(uint8_t peerIndex, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendPollDataResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the PollDataResponseMessage Message to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = PollDataResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = 0;
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);
    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//! SendKeepAliveRequestMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         serviceSelector - SolNet serviceSelector for the service
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendKeepAliveRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t initiator)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendKeepAliveRequestMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the SendKeepAliveRequestMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = KeepAliveRequest;
    uint8_t policies       = 0;
    uint16_t payloadLength = sizeof(iswSolNetKeepAliveRequest);
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    // ! Fill in the payload with the Keep Alive Request serviceSelector
    iswSolNetKeepAliveRequest *keepAliveRequestMsg = (iswSolNetKeepAliveRequest *)payload;
    keepAliveRequestMsg->serviceSelector           = serviceSelector;
    keepAliveRequestMsg->initiator                 = initiator; // 0 = Service Provider; 1 = Service Subscriber

    int status = 0;
    bool save = true;
    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, save, 0, MsgSuccess);

    std::cout << "Device " << std::to_string(GetIndex()) << " Sending KeepAliveRequest to Peer = " << std::to_string(peerIndex) << std::endl;

    return ( status );
}

//!#######################################################################
//! SendKeepAliveResponseMessage()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         seqNumber - sequence number of the SolNet Poll Data Request
//!                     Message packet
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendKeepAliveResponseMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t initiator, uint8_t seqNumber)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString = "SendKeepAliveResponseMessage";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Send the SendKeepAliveResponseMessage to this peer
    uint8_t messageClass   = Flow;
    uint8_t messageId      = KeepAliveResponse;
    uint8_t policies       = 0;
    uint16_t payloadLength = sizeof(iswSolNetKeepAliveResponse);
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    iswSolNetKeepAliveResponse *keepAliveResponseMsg = (iswSolNetKeepAliveResponse *)payload;
    keepAliveResponseMsg->serviceSelector            = serviceSelector;
    keepAliveResponseMsg->initiator                  = initiator;
    keepAliveResponseMsg->reserved[0]                = 0;
    keepAliveResponseMsg->reserved[1]                = 0;
    keepAliveResponseMsg->reserved[2]                = 0;

    int status = 0;

    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, seqNumber, MsgSuccess);

    return ( status );
}

//!#######################################################################
//!
//! General SolNet Message Packet
//!
//!#######################################################################

//!###################################################################
//! SetupSolNetMessagePacket()
//! Inputs: iswSolNetHdr  - pointer to the command structure
//!         seqNumber     - sequence number for this packet
//!         msgStatus     - 0=requests, variable for response
//!                         see iswSolNetMsgStatus
//!         messageClass  - see iswsolNetMsgClassTypes
//!         messageId     - type of message with class - see MsgId structs
//!         policies      - POLICY_PAYLOAD_CHECKSUM = 0x01;
//!                         POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
//!                         POLICY_NO_ACK = 0x00;
//!                         POLICY_ACK = 0x02;
//!         payload       - pointer to buffer holding payload data
//!         payloadLength - length of payload buffer
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
int IswSolNet::SetupSolNetMessagePacket(iswSolNetHeader *iswSolNetHdr,
                                        uint8_t seqNumber,
                                        uint8_t msgStatus,
                                        uint8_t messageClass,
                                        uint8_t messageId,
                                        uint8_t policies,
                                        uint8_t *payload,
                                        uint16_t payloadLength)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        msgString = "SetupSolNetMessagePacket";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    int status = 0;

    //! SolNet header
    iswSolNetHdr->version       = 0x00;
    iswSolNetHdr->protocolClass = 0x01;

    //! Fill in the Message Packet data
    if ( (((messageClass > 0x00) && (messageClass < 0x40)) ||
          (messageClass > 0x5F)) &&
         (messageId > 0x00) )
    {
        //! Message Packet
        iswSolNetHdr->iswSolNetMessagePkt.messageClass = messageClass;
        iswSolNetHdr->iswSolNetMessagePkt.messageId    = messageId;


        //! These response messages have to have the same seqNumber as the request message
        if ( (messageClass == Flow) &&
             ((messageId == GetStatusResponse) ||
              (messageId == RegisterResponse) ||
              (messageId == DeregisterResponse) ||
              (messageId == RevokeRegistrationResponse) ||
              (messageId == PollDataResponse) ||
              (messageId == AutonomousStartStopResponse) ||
              (messageId == RevokeNetworkAssociationResponse) ||
              (messageId == KeepAliveResponse)))
        {
            iswSolNetHdr->iswSolNetMessagePkt.seqNumber = seqNumber;
        }
        else
        {
            iswSolNetHdr->iswSolNetMessagePkt.seqNumber = GetNextMsgSeqNo();
        }

        iswSolNetHdr->iswSolNetMessagePkt.policies       = policies;
        iswSolNetHdr->iswSolNetMessagePkt.status         = msgStatus;
        iswSolNetHdr->iswSolNetMessagePkt.reserved1      = 0;
        iswSolNetHdr->iswSolNetMessagePkt.headerChecksum = 0;
        iswSolNetHdr->iswSolNetMessagePkt.messageLength  = payloadLength;

        //! Initialize buffer to zeros
        memset(&iswSolNetHdr->iswSolNetMessagePkt.messagePayload[0], 0, payloadMsgBufferSize);
        iswSolNetHdr->iswSolNetMessagePkt.payloadChecksum = 0;

        //! Get the header checksum
        CalculateHeaderChecksum(iswSolNetHdr);

        if ( payloadLength != 0 )
        {
            //! Copy in the message to the buffer sent ISW
            memcpy(iswSolNetHdr->iswSolNetMessagePkt.messagePayload, payload, payloadLength);

            //! ISW specs say to ignore acknowledgement policy in bits 1 and 2
            if ( (policies & POLICY_PAYLOAD_CHECKSUM) == POLICY_PAYLOAD_CHECKSUM )
            {
                //! ISW payload checksum is a 16 bit value.
                uint16_t checksum = 0;
                checksum          = IswChecksum(payloadLength, (uint16_t *)&iswSolNetHdr->iswSolNetMessagePkt.messagePayload);

                //! Move checksum after payload is adjusted for 16-bit boundary
                if ( payloadLength < payloadMsgBufferSize )
                {
                    //! If payload falls on 16-bit boundary just copy checksum after it
                    if ( (payloadLength % 16) == 0 )
                    {
                        memcpy(&iswSolNetHdr->iswSolNetMessagePkt.messagePayload[payloadLength], (uint8_t *)&checksum, 2);
                    }
                    else
                    {
                        //! add pad byte to 16-bit boundary and copy checksum
                        memcpy(&iswSolNetHdr->iswSolNetMessagePkt.messagePayload[payloadLength + 1], (uint8_t *)&checksum, 2);
                    }

                    //! This is for unit testing - doesn't get copied into the buffer sent to hw
#ifdef TESTING
                    iswSolNetHdr->iswSolNetMessagePkt.payloadChecksum = checksum;
#endif
                }
                else
                {
                    iswSolNetHdr->iswSolNetMessagePkt.payloadChecksum = checksum;
                }
            }
        }

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "messageClass = " << std::to_string(iswSolNetHdr->iswSolNetMessagePkt.messageClass) << std::endl;
            ss << "messageId    = " << std::to_string(iswSolNetHdr->iswSolNetMessagePkt.messageId) << std::endl;
            ss << "seqNumber    = " << std::to_string(iswSolNetHdr->iswSolNetMessagePkt.seqNumber) << std::endl;
            ss << "status       = " << std::to_string(iswSolNetHdr->iswSolNetMessagePkt.status) << std::endl;
            ss << "policies     = " << std::to_string(iswSolNetHdr->iswSolNetMessagePkt.policies) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "Message class or id not supported";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        status = -1;
    }

    return (status);

}

//!#######################################################################
//! SendSolNetMessagePacket()
//! Inputs: messageClass  - see iswsolNetMsgClassTypes
//!         messageId     - type of message with class - see MsgId structs
//!         peerIndex     - ISW representation of the peer to this node
//!         policies      - POLICY_PAYLOAD_CHECKSUM = 0x01;
//!                         POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
//!                         POLICY_NO_ACK = 0x00;
//!                         POLICY_ACK = 0x02;
//!         payload       - pointer to buffer holding payload data
//!         payloadLength - length of payload buffer
//!         save          - true=this is a request, save it on list to
//!                         match response; false=do nothing
//!         seqNumber     - sequence number for this packet
//!         msgStatus     - 0=requests, variable for response
//!                         see iswSolNetMsgStatus
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendSolNetMessagePacket(uint8_t messageClass,
                                       uint8_t messageId,
                                       uint8_t peerIndex,
                                       uint8_t policies,
                                       uint8_t *payload,
                                       uint16_t payloadLength,
                                       bool save,
                                       uint8_t seqNumber,
                                       uint8_t msgStatus)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        msgString = "SendSolNetMessagePacket";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    int status = 0;

    IswInterface::iswCommand iswCmd;

    status = SetupSolNetMessagePacket(&iswCmd.iswSolNetHdr,
                                      seqNumber,
                                      msgStatus,
                                      messageClass,
                                      messageId,
                                      policies,
                                      payload,
                                      payloadLength);

    if ( status == 0 )
    {
        //! All SolNet Message Packets use endpoint = 0
        uint8_t endpoint = 0;
        status           = iswInterface->SyncSendSolNetMessagePacket(&iswCmd, peerIndex, endpoint);

        if ( save )
        {
            //! Add to MessageResponse list
            iswSolNetHeader *saveSolNetMsg = new iswSolNetHeader;
            memcpy(saveSolNetMsg, &iswCmd.iswSolNetHdr, sizeof(iswSolNetHeader));
            iswSolNetMsgRespListLock.lock();
            iswSolNetMessageResponseList.push_back(saveSolNetMsg);
            iswSolNetMsgRespListLock.unlock();
        }

        if ( (policies & POLICY_ACK) == POLICY_ACK )
        {
            //! Add this packet to the retransmit list
        }
    }

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "SendSolNetMessagePacket failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//!
//! General SolNet Data Packet
//!
//!###################################################################

//!###################################################################
//! SetupSolNetDataPacket()
//! Inputs: iswSolNetHdr  - pointer to the command structure
//!         dataflowId    - Bits 0 – 6: Dataflow Number (0 – 127)
//!                         Bit 7: Role (0 = producer, 1 = consumer)
//!                         Also the index into the services table
//!         policies      - POLICY_PAYLOAD_CHECKSUM = 0x01;
//!                         POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
//!                         POLICY_NO_ACK = 0x00;
//!                         POLICY_ACK = 0x02;
//!         payload       - pointer to buffer holding payload data
//!         payloadLength - length of payload buffer
//!         retransItem   - pointer to a pointer to a iswRetransmitItem
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
int IswSolNet::SetupSolNetDataPacket(iswSolNetHeader *iswSolNetHdr, uint8_t dataflowId, uint8_t policies,
                                     uint8_t *payload, uint16_t payloadLength, iswRetransmitItem **retransItem)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        std::string msgString;
        msgString = "SetupSolNetDataPacket";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }


    if ( (policies & POLICY_ACK) == POLICY_ACK )
    {
        //! This packet must be added to a retransmit queue
        *retransItem = new iswRetransmitItem;
    }

    int status = 0;

    //! SolNet header
    iswSolNetHdr->version       = 0x00;
    iswSolNetHdr->protocolClass = 0x00;

    //! Fill in the Data Packet data
    //! dataflowId is our Id plus set the bit that we are a provider per spec
    iswSolNetHdr->iswSolNetDataPkt.dataflowId     = dataflowId;  //! Bit 7 = 0 for producer
    iswSolNetHdr->iswSolNetDataPkt.seqNumber      = GetNextDataSeqNo(dataflowId);
    iswSolNetHdr->iswSolNetDataPkt.policies       = policies;
    iswSolNetHdr->iswSolNetDataPkt.reserved1      = 0;
    iswSolNetHdr->iswSolNetDataPkt.reserved2      = 0;
    iswSolNetHdr->iswSolNetDataPkt.headerChecksum = 0;
    iswSolNetHdr->iswSolNetDataPkt.dataLength     = payloadLength;

    //! Initialize buffer to zeros
    memset(&iswSolNetHdr->iswSolNetDataPkt.dataPayload[0], 0, payloadDataBufferSize);
    iswSolNetHdr->iswSolNetDataPkt.payloadChecksum = 0;

    //! Calculate the header checksum
    CalculateHeaderChecksum(iswSolNetHdr);

    if ( payloadLength != 0 )
    {
        //! Copy in the message to the buffer sent ISW
        memcpy(iswSolNetHdr->iswSolNetDataPkt.dataPayload, payload, payloadLength);

        //! ISW specs say to ignore acknowledgement policy in bits 1 and 2
        if ( (policies & POLICY_PAYLOAD_CHECKSUM) == POLICY_PAYLOAD_CHECKSUM )
        {
            //! ISW payload checksum is a 16 bit value.
            uint16_t checksum = 0;
            checksum = IswChecksum(payloadLength, (uint16_t *)&iswSolNetHdr->iswSolNetDataPkt.dataPayload);

            //! Move checksum after payload is adjusted for 16-bit boundary
            if ( payloadLength < payloadDataBufferSize )
            {
                uint16_t *packetChecksum = nullptr;

                //! If payload falls on 16-bit boundary just copy checksum after it
                if ( (payloadLength % 16) == 0 )
                {
                    packetChecksum = (uint16_t *)&iswSolNetHdr->iswSolNetDataPkt.dataPayload[payloadLength];
                    *packetChecksum = checksum;
                }
                else
                {
                    //! add pad byte to 16-bit boundary and copy checksum
                    packetChecksum = (uint16_t *)&iswSolNetHdr->iswSolNetDataPkt.dataPayload[payloadLength + 1];
                    *packetChecksum = checksum;
                }

#ifdef TESTING
                // This is for unit testing - doesn't get copied into the buffer sent to hw
                iswSolNetHdr->iswSolNetDataPkt.payloadChecksum = checksum;
#endif
            }
            else
            {
                iswSolNetHdr->iswSolNetDataPkt.payloadChecksum = checksum;
            }

            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                std::stringstream ss;
                ss << "Device " << std::to_string(iswInterface->GetIndex()) << " SetupSolNetDataPacket" << std::endl;
                ss << "payload checksum = " << std::to_string(checksum) << std::endl;
                theLogger->LogMessage(ss.str(), iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }
    }

    //! Setup retransmit packet
    if ( (policies & POLICY_ACK) == POLICY_ACK )
    {
        //! SolNet header
        (*retransItem)->iswSolNetHdr.version       = iswSolNetHdr->version;
        (*retransItem)->iswSolNetHdr.protocolClass = iswSolNetHdr->protocolClass;

        //! Fill in the Data Packet data
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataflowId     = iswSolNetHdr->iswSolNetDataPkt.dataflowId;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.seqNumber      = iswSolNetHdr->iswSolNetDataPkt.seqNumber;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.policies       = iswSolNetHdr->iswSolNetDataPkt.policies;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.reserved1      = iswSolNetHdr->iswSolNetDataPkt.reserved1;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.reserved2      = iswSolNetHdr->iswSolNetDataPkt.reserved2;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.headerChecksum = iswSolNetHdr->iswSolNetDataPkt.headerChecksum;
        (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataLength     = iswSolNetHdr->iswSolNetDataPkt.dataLength;

        //! Initialize buffer to zeros
        memset(&(*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataPayload[0], 0, payloadDataBufferSize);
        memcpy((*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataPayload, iswSolNetHdr->iswSolNetDataPkt.dataPayload, iswSolNetHdr->iswSolNetDataPkt.dataLength);

        //! Move checksum after payload is adjusted for 16-bit boundary
        if ( payloadLength < payloadDataBufferSize )
        {
            //! If payload falls on 16-bit boundary just copy checksum after it
            if ( (payloadLength % 16) == 0 )
            {
                memcpy(&(*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataPayload[payloadLength], &iswSolNetHdr->iswSolNetDataPkt.payloadChecksum, 2);
            }
            else
            {
                //! add pad byte to 16-bit boundary and copy checksum
                memcpy(&(*retransItem)->iswSolNetHdr.iswSolNetDataPkt.dataPayload[payloadLength + 1], &iswSolNetHdr->iswSolNetDataPkt.payloadChecksum, 2);
            }
        }
        else
        {
            (*retransItem)->iswSolNetHdr.iswSolNetDataPkt.payloadChecksum = iswSolNetHdr->iswSolNetDataPkt.payloadChecksum;
        }
    }

    return (status);
}

//!#######################################################################
//! SendSolNetDataPacket()
//! Inputs: peerIndex     - ISW representation of the peer to this node
//!         endpoint      - ISW endpoints are 1 - 3 for SolNet data,
//!                         endpoint 0 for SolNet messages
//!         dataflowId    - Bits 0 – 6: Dataflow Number (0 – 127)
//!                         Bit 7: Role (0 = producer, 1 = consumer)
//!                         Also the index into the services table
//!         policies      - POLICY_PAYLOAD_CHECKSUM    = 0x01;
//!                         POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
//!                         POLICY_NO_ACK              = 0x00;
//!                         POLICY_ACK                 = 0x02;
//!         payload       - pointer to buffer holding payload data
//!         payloadLength - length of payload buffer
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendSolNetDataPacket(uint8_t peerIndex,
                                    uint8_t endpoint,
                                    uint8_t dataflowId,
                                    uint8_t policies,
                                    uint8_t *payload,
                                    uint16_t payloadLength)
{
    uint64_t now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    //qDebug() << "sendingSolNetDataPacket: " << now;
    std::string msgString;
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        msgString = "SendSolNetDataPacket";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    bool ImAProducer = true;

    if ( (dataflowId & iswSolNetSetConsumerBit) == 0x80)
    {
        ImAProducer = false;
    }

    IswInterface::iswCommand *iswCmd = new IswInterface::iswCommand;
    iswRetransmitItem *retransItem   = nullptr;
    status                          = SetupSolNetDataPacket(&iswCmd->iswSolNetHdr, dataflowId,
                                                            policies, payload, payloadLength, &retransItem);

    if ( status == 0 )
    {
        //! Add copy to retransmit queue if an ACK is requested
        if ( ((policies & POLICY_ACK) == POLICY_ACK) && (retransItem != nullptr) )
        {
            //! If we are the producer, requesting an ACK from the peer
            //! put this packet on this device's services retransmit queue
            //! for the registered peer
            if ( ImAProducer )
            {
                if ((peerIndex < 0x3F) || (peerIndex == 0xFF) || (peerIndex >= 0x40 && peerIndex <= 0x5E))
                {
                    // No retransmit on broadcast or multicast
                    retransItem = nullptr;
                }
                else
                {
                    iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.lock();

                    //! Add a timestamp to this retransmit packet
                    retransItem->endpoint     = endpoint;
                    retransItem->retransCount = 0;
                    retransItem->timestamp    = theLogger->GetTimeStamp();
                    iswSolNetServices[dataflowId].registeredPeers[peerIndex].retransmitList->push_back(retransItem);
                    iswSolNetServices[dataflowId].registeredPeers[peerIndex].registeredPeerLock.unlock();
                }
            }
            else
            {
                if (retransItem != nullptr)
                {
                    //! Else we are a consumer of a Peer's services and requested an ACK on a packet that
                    //! we sent the Peer, so put the packet on the Peer's services retransmit queue
                    iswSolNetPeerServices[peerIndex].peerLock.lock();

                    //! Add a timestamp to this retransmit packet
                    retransItem->endpoint     = endpoint;
                    retransItem->retransCount = 0;
                    retransItem->timestamp    = theLogger->GetTimeStamp();
                    iswSolNetPeerServices[peerIndex].retransmitList->push_back(retransItem);
                    iswSolNetPeerServices[peerIndex].peerLock.unlock();
                }
            }
        }

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "AddToSendQueue:" << std::endl;
            ss << "peerIndex = " << std::to_string(peerIndex) << std::endl;
            ss << "endpoint  = " << std::to_string(endpoint) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }

        //! Send the packet out
        if ( solNetStopped == false )
        {
            iswInterface->AddToSendQueue(iswCmd, peerIndex, endpoint);
        }
        else
        {
            status = -1;
        }
    }

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "SendSolNetDataPacket Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!#######################################################################
//! SendSolNetAckPacket()
//! Inputs: peerIndex - ISW representation of the peer to this node
//!         ackPkt - pointer to iswSolNetAckPacket
//! Description: Send the ISW command to the firmware
//!#######################################################################
int IswSolNet::SendSolNetAckPacket(uint8_t peerIndex, iswSolNetAckPacket *ackPkt)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        msgString = "SendSolNetAckPacket";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    //! Send the SolNet Ackowledgement Message to this peer
    uint8_t messageClass = Policy;
    uint8_t messageId    = Acknowledgement;
    int8_t policies      = 0;

    //! Initialize
    uint8_t payload[payloadMsgBufferSize];
    memset(payload, 0, payloadMsgBufferSize);

    //! Copy the entry header into the message payload
    uint16_t payloadLength = sizeof(iswSolNetAckPacket);
    memcpy(payload, ackPkt, payloadLength);

    //! Send the Acknowledgement Packet
    status = SendSolNetMessagePacket(messageClass, messageId, peerIndex, policies, payload, payloadLength, false, 0, MsgSuccess);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            msgString = "SendSolNetAckPacket failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream ss;
            ss << "Sending ACK Packet:" << std::endl;
            ss << "endpoint      = " << std::to_string(ackPkt->endpoint) << std::endl;
            ss << "dataflowId    = " << std::to_string(ackPkt->dataflowId) << std::endl;
            ss << "seqNumber     = " << std::to_string(ackPkt->seqNumber) << std::endl;
            ss << "protocolClass = " << std::to_string(ackPkt->protocolClass) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
    }

    return ( status );
}

//!#######################################################################
//! GetServiceIdString()
//! Inputs: serviceId - SolNet Service ID number
//! Description: Return the string name associated with the service id
//!              number
//!#######################################################################
std::string IswSolNet::GetServiceIdString(uint16_t serviceId)
{
    switch (serviceId)
    {
        case Reserved1: return ( "Reserved 1" );
        case VideoService: return ( "Video Service" );
        case Audio: return ( "Audio Service" );
        case PositionSense: return ( "Position Sense" );
        case EnvironmentSense: return ( "Environment Sense" );
        case HealthSense: return ( "Health Sense" );
        case SimpleUI: return ( "Simple UI" );
        case Control: return ( "Control" ); //! overlaps: LRF Control, AR Control
        case Status: return ( "Status" );
        case RTA_SERVICE: return ( "RTA" );
        case AR_OVERLAY: return ( "AR Overlay" );
        case HybridSense: return ( "Hybrid Sense" );
        case MotionSense: return ( "Motion Sense" );
        case TemperatureSense: return ( "Temperature Sense" );
        case MoistureSense: return ( "Moisture Sense" );
        case VibrationSense: return ( "Vibration Sense" );
        case ChemicalSense: return ( "Chemical Sense" );
        case FlowSense: return ( "Flow Sense" );
        case ForceSense: return ( "Force Sense" );
        case LrfService: return ( "LRF Service" ); //! overlaps: LRF Control
        case LrfControlService: return ( "LRF Control" );
        case ArType1: return ( "AR Type 1" );
        case ArService: return ( "AR Service" );
        case Pipe: return ( "Pipe" );
        case ReservedMin: return ( "Reserved Min" );
        case ReservedMax: return ( "Reserved Max" );
        case PacketGenerator: return ( "Packet Generator" );
        default: return ( "Unknown_serviceId");
    }
}

//!#######################################################################
//! GetPropertyIdString()
//! Inputs: propertyId - SolNet property descriptor ID number
//! Description: Return the string name associated with the property id
//!              number
//!#######################################################################
std::string IswSolNet::GetPropertyIdString(uint16_t propertyId)
{
    switch (propertyId)
    {
        case Label: return ( "Label" );
        case TextLabel: return ( "Text Label");
        case Version: return ( "Version" );
        case Endpoint: return ( "Endpoint" );
        case MulticastGroup: return ( "Multicast Group" );
        case Encoding: return ( "Encoding" );
        case FlowPolicy: return ( "Flow Policy" );
        case DeviceName: return ( "Device Name" );
        case DeviceSerialNumber: return ( "Device Serial Number" );
        case DeviceManufacturer: return ( "Device Manufacturer" );
        case DeviceFriendlyName: return ( "Device Friendly Name" );
        case AssociatedService: return ( "Associated Service" );
        case QoSProperties: return ("QoS Properties");
        case QoSDataRate: return ("QoS Data Rate");
        case QoSMaxLatency: return ("QoS Max Latency");
//        case VideoFormatRaw: return ( "Video Format Raw" );
        case VideoFormat: return ( "Video Format" ); //! overlaps: Video Reserved 10, Video Reserved 11, Video Reserved 12
        case VideoSampleFormat: return ("Video Sample Format");
        case MaximumFrameSize: return ("Maximum Frame Size");
        case RleFormat: return ("RLE Format");
        case VideoReserved2: return ( "Video Reserved 2" );
        case VideoProtocol: return ( "Video Protocol Raw" );
        case VideoReserved3: return ( "Video Reserved 3" );
        case VideoReserved4: return ( "Video Reserved 4" );
        case IFOV: return ( "Video IFOV" );
        case VideoReserved5: return ( "Video Reserved 5" );
        case VideoReserved6: return ( "Video Reserved 6" );
        case PrincipalPoint: return ( "Video Principal Point" );
        case VideoReserved7: return ( "Video Reserved 7" );
        case VideoReserved8: return ( "Video Reserved 8" );
        case VideoLensDistortion: return ( "Video Lens Distortion" );
        case VideoReserved9: return ( "Video Reserved 9" );
        case VideoReserved10: return ( "Video Reserved 10" );
        case VideoControl: return ( "Video Control" );
        case ImageSensorType: return ( "Image Sensor Type" );
        case ImageSensorWaveband: return (" Image Sensor Waveband");
        case VideoFrameRate: return ("Video Frame Rate");
        case VideoIntrinsicParameters: return ("Video Intrinsic Parameters");
        case VideoReserved12: return ( "Video Reserved 12" );
        case PositionDevice: return ("Position Device");
        case UiMotion: return ( "UI Motion" );
        case UiButtonInput: return ( "UI Button Input" );
        case UiReservedMin: return ( "UI Reserved Min" );
        case UiReservedMax: return ( "UI Reserved Max" );
        case ImuOutputRate : return ( "IMU Output Rate" );
        case ImuFloatUpdateRate: return ( "IMU Float Update Rate" );
        case ImuSensorNoise: return ( "IMU Sensor Noise" );
        case ArImuInputProperty: return ("AR IMU Input");
        case MotionSensorRate: return ( "Motion Sensor Rate" );
        case MotionSensorAccel: return ( "Motion Sensor Accelerometer" );
        case MotionSensorGyro: return ( "Motion Sensor Gyroscope" );
        case MotionSensorMag: return ( "Motion Sensor Magnetometer" );
        case MotionSensorAxes: return ( "Motion Sensor Axes" );
        case MotionReserved1: return ( "Motion Reserved 1" );
        case MotionReserved2: return ( "Motion Reserved 2" );
        case NetworkingIP: return ( "Networking IP" );
        case NetworkingTCPPort: return ( "Networking TCP Port" );
        case NetworkingUDPPort: return ( "Networking UDP Port" );
        case NetworkingReserved1: return ( "Networking Reserved 1" );
        case NetworkingReserved2: return ( "Networking Reserved 2" );
        case ArType2OverlayOutput: return ( "AR Type 2 Overlay Output" );
        case ArVideoInput: return ("AR Video Input");
        case ArReserved1: return ( "AR Reserved 1" );
        case ArReserved2: return ( "AR Reserved 2" );
        case GssipFeatureSupport: return ("GSSIP Feature Support");
        case StatusBattery: return ( "Power Battery" );
        case PowerReserved1: return ( "Power Reserved 1" );
        case PowerReserved2: return ( "Power Reserved 2" );
        case LrfCapabilities: return ("LRF Capabilities");
        default: return ( "Unknown_propertyId");
    }
}

//!#######################################################################
//! PrintDescriptorIdToLogfile()
//! Inputs: ss - pointer to stringstream
//!         descriptorId - SolNet number for the descriptor type
//!         data - pointer to a generic data buffer to type cast
//!                based on descriptorId
//! Description: Print the SolNet Property Descriptor Id
//!#######################################################################

void IswSolNet::PrintDescriptorIdToLogfile(std::stringstream *ss, uint16_t descriptorId, uint8_t *data)
{
    switch (descriptorId)
    {
        case Label:
        {
            *ss << "Label " << std::endl;
            iswSolNetPropertyTextLabelDesc *labelDesc = (iswSolNetPropertyTextLabelDesc *)data;
            *ss << "label = " << labelDesc->textLabel << std::endl;
            break;
        }
        case TextLabel:
        {
            *ss << "Text Label " << std::endl;
            iswSolNetPropertyTextLabelDesc *labelDesc = (iswSolNetPropertyTextLabelDesc *)data;
            *ss << "text label = " << labelDesc->textLabel << std::endl;
            break;
        }
        case Version:
        {
            *ss << "Version " << std::endl;
            iswSolNetPropertyVersionDesc *versionDesc = (iswSolNetPropertyVersionDesc *)data;
            *ss <<  "version       = " << std::to_string(versionDesc->solNetVersion) << std::endl;
            *ss <<  "versionString = " << versionDesc->versionText << std::endl;
            break;
        }
        case Endpoint:
        {
            *ss << "Endpoint " << std::endl;
            iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)data;
            *ss <<  "endpointId           = " << std::to_string(endpointDesc->endpointId) << std::endl;
            *ss <<  "dataflowId           = " << std::to_string(endpointDesc->dataflowId) << std::endl;
            *ss <<  "endpointDistribution = " << std::to_string(endpointDesc->endpointDistribution) << std::endl;
            *ss <<  "dataPolicies         = " << std::to_string(endpointDesc->dataPolicies) << std::endl;
            break;
        }
        case MulticastGroup:
        {
            *ss << "McastGroup " << std::endl;
            iswSolNetPropertyMulticastGroupDesc *mcastDesc = (iswSolNetPropertyMulticastGroupDesc *)data;
            *ss <<  "mcastGroupId  = " << std::to_string(mcastDesc->mcastGroupId) << std::endl;
            *ss <<  "reserved1     = " << std::to_string(mcastDesc->reserved1) << std::endl;
            *ss <<  " mcastAddress = " << std::to_string(mcastDesc->mcastAddress) << std::endl;

            std::stringstream macAddress;
            for (int i = 0; i < 6; i++)
            {
                macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)mcastDesc->mcastMacAddress[i];
                if ( i < 5 )
                {
                    macAddress << ":";
                }
            }
            *ss <<  "mcastMacAddress = " << macAddress << std::endl;
            *ss <<  "reserved2       = " << std::to_string(mcastDesc->reserved2) << std::endl;
            break;
        }
        case Encoding:
        {
            *ss << "Encoding " << std::endl;
            iswSolNetPropertyEncodingDesc *encodingDesc = (iswSolNetPropertyEncodingDesc *)data;
            *ss <<  "encoding = " << std::to_string(encodingDesc->encoding) << std::endl;
            break;
        }
        case FlowPolicy:
        {
            *ss << "FlowPolicy " << std::endl;
            break;
        }
        case DeviceName:
        {
            *ss << "DeviceName " << std::endl;
            iswSolNetPropertyDeviceDesc *devNameDesc = (iswSolNetPropertyDeviceDesc *)data;
            *ss <<  "deviceNameString = " << devNameDesc->devString << std::endl;
            break;
        }
        case DeviceSerialNumber:
        {
            *ss << "DeviceSerialNumber " << std::endl;
            iswSolNetPropertyDeviceDesc *devSerialNoDesc = (iswSolNetPropertyDeviceDesc *)data;
            *ss <<  "deviceSerialNoString = " << devSerialNoDesc->devString << std::endl;
            break;
        }
        case DeviceManufacturer:
        {
            *ss << "DeviceManufacturer " << std::endl;
            iswSolNetPropertyDeviceDesc *devManufacturerDesc = (iswSolNetPropertyDeviceDesc *)data;
            *ss <<  "deviceManufacturerString = " << devManufacturerDesc->devString << std::endl;
            break;
        }
        case DeviceFriendlyName:
        {
            *ss << "DeviceFriendlyName " << std::endl;
            iswSolNetPropertyDeviceDesc *devFriendlyNameDesc = (iswSolNetPropertyDeviceDesc *)data;
            *ss <<  "deviceFriendlyNameString = " << devFriendlyNameDesc->devString << std::endl;
            break;
        }
        case AssociatedService:
        {
            *ss << "AssociatedService " << std::endl;
            iswSolNetPropertyAssociatedServiceDesc *associatedServiceDesc = (iswSolNetPropertyAssociatedServiceDesc *)data;
            *ss <<  "associatedServiceId       = " << associatedServiceDesc->associatedServiceId << std::endl;
            *ss <<  "associatedServiceType     = " << associatedServiceDesc->associationType << std::endl;
            *ss <<  "reserved                  = " << associatedServiceDesc->reserved << std::endl;
            *ss <<  "associatedServiceSelector = " << associatedServiceDesc->associatedServiceSelector << std::endl;
            break;
        }
        case DeviceSoftwareDesc:
        {
            *ss << "DeviceSoftwareDesc " << std::endl;
            break;
        }
        case DeviceSoftwareNameDesc:
        {
            *ss << "DeviceSoftwareNameDesc " << std::endl;
            break;
        }
        case DeviceSoftwareVersionDesc:
        {
            *ss << "DeviceSoftwareVersionDesc " << std::endl;
            break;
        }
        case VideoOutputFormatRaw:
        {
            *ss << "VideoOutputFormatRaw " << std::endl;
            iswSolNetPropertyVideoOutputFormatRawDesc *videoFormatRawDesc = (iswSolNetPropertyVideoOutputFormatRawDesc *)data;
            *ss <<  "resX        = " << std::to_string(videoFormatRawDesc->resX) << std::endl;
            *ss <<  "resY        = " << std::to_string(videoFormatRawDesc->resY) << std::endl;
            *ss <<  "frameRate   = " << std::to_string(videoFormatRawDesc->frameRate) << std::endl;
            *ss <<  "bitDepth    = " << std::to_string(videoFormatRawDesc->bitDepth) << std::endl;
            *ss <<  "pixelformat = " << std::to_string(videoFormatRawDesc->pixelformat) << std::endl;
            break;
        }
        case VideoFormat:
        {
            *ss << "VideoFormat " << std::endl;
            iswSolNetPropertyVideoFormatDesc *videoFormatDesc = (iswSolNetPropertyVideoFormatDesc *)data;
            *ss <<  "formatX        = " << std::to_string(videoFormatDesc->videoFormatX) << std::endl;
            *ss <<  "formatY        = " << std::to_string(videoFormatDesc->videoFormatY) << std::endl;
            *ss <<  "frameRate      = " << std::to_string(videoFormatDesc->frameRate) << std::endl;
            *ss <<  "formatOptions  = " << std::to_string(videoFormatDesc->formatOptions) << std::endl;
            *ss <<  "reserved       = " << std::to_string(videoFormatDesc->reserved) << std::endl;
            *ss <<  "encodingFormat = " << std::to_string(videoFormatDesc->encodingFormat) << std::endl;
            break;
        }
        case VideoSampleFormat:
        {
            *ss << "VideoSampleFormat " << std::endl;
            iswSolNetPropertyVideoSampleFormatDesc *videoSampleDesc = (iswSolNetPropertyVideoSampleFormatDesc *)data;
            *ss <<  "bitDepth    = " << std::to_string(videoSampleDesc->bitDepth) << std::endl;
            *ss <<  "pixelFormat = " << std::to_string(videoSampleDesc->pixelFormat) << std::endl;
            break;
        }
        case VideoReserved2:
        {
            *ss << "VideoReserved2 " << std::endl;
            break;
        }
        case VideoProtocol:
        {
            *ss << "VideoProtocolRaw " << std::endl;
            iswSolNetPropertyVideoProtocolDesc *videoProtocolRawDesc = (iswSolNetPropertyVideoProtocolDesc *)data;
            *ss <<  "protocol = " << std::to_string(videoProtocolRawDesc->protocol) << std::endl;
            break;
        }
        case VideoReserved3:
        {
            *ss << "VideoReserved3 " << std::endl;
            break;
        }
        case VideoReserved4:
        {
            *ss << "VideoReserved4 " << std::endl;
            break;
        }
        case IFOV:
        {
            *ss << "VideoIFOV " << std::endl;
            iswSolNetPropertyVideoIfovDesc *ifovDesc = (iswSolNetPropertyVideoIfovDesc *)data;

            //! Format for float
            std::stringstream stream;
            stream << std::fixed << std::setprecision(2) << (float)ifovDesc->IFoV;
            *ss <<  "instantFieldOfView = " << stream.str() << std::endl;
            break;
        }
        case VideoReserved5:
        {
            *ss << "VideoReserved5 " << std::endl;
            break;
        }
        case VideoReserved6:
        {
            *ss << "VideoReserved6 " << std::endl;
            break;
        }
        case PrincipalPoint:
        {
            *ss << "VideoPrincipalPoint " << std::endl;
            iswSolNetPropertyVideoPrincipalPointDesc *videoPPDesc = (iswSolNetPropertyVideoPrincipalPointDesc *)data;
            *ss <<  "videoPointX = " << std::to_string(videoPPDesc->principalPointX) << std::endl;
            *ss <<  "videoPointY = " << std::to_string(videoPPDesc->principalPointY) << std::endl;
            break;
        }
        case VideoReserved7:
        {
            *ss << "VideoReserved7 " << std::endl;
            break;
        }
        case VideoReserved8:
        {
            *ss << "VideoReserved8 " << std::endl;
            break;
        }
        case VideoLensDistortion:
        {
            *ss << "VideoLensDistortion " << std::endl;
            iswSolNetPropertyVideoLensDistortionDesc *videoLensDistortionDesc = (iswSolNetPropertyVideoLensDistortionDesc *)data;

//            for(int i = 0; i < 6; i++)
//            {
//               *ss <<  "        distortionCoefficients=" << std::to_string(videoLensDistortionDesc->distortionCoefficients[i]) << std::endl;
//            }
            break;
        }
        case VideoReserved9:
        {
            *ss << "VideoReserved9 " << std::endl;
            break;
        }
        case VideoReserved10:
        {
            *ss << "VideoReserved10 " << std::endl;
            break;
        }
        case VideoControl:
        {
            *ss << "VideoControl " << std::endl;
            iswSolNetPropertyVideoControlDesc *videoControlDesc = (iswSolNetPropertyVideoControlDesc *)data;
            *ss <<  "controlBitMap = " << std::to_string(videoControlDesc->controlBitmap) << std::endl;
            break;
        }
        case ImageSensorType:
        {
            *ss << "ImageSensorType " << std::endl;
            iswSolNetPropertyVideoImageSensorTypeDesc *imageSensorTypeDesc = (iswSolNetPropertyVideoImageSensorTypeDesc *)data;
            *ss <<  "type     = " << std::to_string(imageSensorTypeDesc->type) << std::endl;
            *ss <<  "reserved = " << std::to_string(imageSensorTypeDesc->reserved1) << std::endl;
//            *ss <<  "        description=" << std::to_string(imageSensorTypeDesc->description) << std::endl;
            break;
        }
        case ImageSensorWaveband:
        {
            *ss << "ImageSensorWaveband " << std::endl;
            iswSolNetPropertyVideoImageSensorWavebandDesc *imageSensorWavebandDesc = (iswSolNetPropertyVideoImageSensorWavebandDesc *)data;
            *ss <<  "waveband = " << std::to_string(imageSensorWavebandDesc->waveband) << std::endl;
            *ss <<  "reserved = " << std::to_string(imageSensorWavebandDesc->reserved) << std::endl;
//            *ss <<  "        description=" << std::to_string(imageSensorWavebandDesc->description) << std::endl;
            break;
        }
        case UiMotion:
        {
            *ss << "UiMotion " << std::endl;
            iswSolNetPropertySimpleUiMotionDesc *UiMotionDesc = (iswSolNetPropertySimpleUiMotionDesc *)data;
            *ss <<  "terminalId = " << std::to_string(UiMotionDesc->terminalId) << std::endl;
            *ss <<  "type       = " << std::to_string(UiMotionDesc->type) << std::endl;
            *ss <<  "flags      = " << std::to_string(UiMotionDesc->flags) << std::endl;
            *ss <<  "xMin       = " << std::to_string(UiMotionDesc->xMin) << std::endl;
            *ss <<  "xMax       = " << std::to_string(UiMotionDesc->xMax) << std::endl;
            *ss <<  "yMin       = " << std::to_string(UiMotionDesc->yMin) << std::endl;
            *ss <<  "yMax       = " << std::to_string(UiMotionDesc->yMax) << std::endl;
            *ss <<  "zMin       = " << std::to_string(UiMotionDesc->zMin) << std::endl;
            *ss <<  "zMax       = " << std::to_string(UiMotionDesc->zMax) << std::endl;
            break;
        }
        case UiButtonInput:
        {
            *ss << "UiButtons " << std::endl;
            iswSolNetPropertySimpleUiButtonInputDesc *uiButtonsDesc = (iswSolNetPropertySimpleUiButtonInputDesc *)data;
            *ss <<  "terminalId   = " << std::to_string(uiButtonsDesc->terminalId) << std::endl;
            *ss <<  "capabilities = " << std::to_string(uiButtonsDesc->capabilities) << std::endl;
            *ss <<  "buttonCount  = " << std::to_string(uiButtonsDesc->buttonCount) << std::endl;
            *ss <<  "stateCount   = " << std::to_string(uiButtonsDesc->stateCount) << std::endl;
            break;
        }
        case UiReservedMin:
        {
            *ss << "UiReservedMin " << std::endl;
            break;
        }
        case UiReservedMax:
        {
            *ss << "UiReservedMax " << std::endl;
            break;
        }
        case ImuOutputRate:
        {
            *ss << "RtaImuRateProperty " << std::endl;
            iswSolNetPropertyImuOutputRateDesc *rtaImuRateDesc = (iswSolNetPropertyImuOutputRateDesc *)data;
            *ss <<  "rate = " << std::to_string(rtaImuRateDesc->rate) << std::endl;
            break;
        }
        case ImuSensorNoise:
        {
            *ss << "ImuSensorNoise " << std::endl;
            iswSolNetPropertImuSensorNoiseDesc *imuSensorNoiseDesc = (iswSolNetPropertImuSensorNoiseDesc *)data;
            *ss <<  "accelerometerXNoise = " << std::to_string(imuSensorNoiseDesc->accelerometerXNoise) << std::endl;
            *ss <<  "accelerometerYNoise = " << std::to_string(imuSensorNoiseDesc->accelerometerYNoise) << std::endl;
            *ss <<  "accelerometerZNoise = " << std::to_string(imuSensorNoiseDesc->accelerometerZNoise) << std::endl;
            *ss <<  "gyroscopeXNoise     = " << std::to_string(imuSensorNoiseDesc->gyroscopeXNoise) << std::endl;
            *ss <<  "gyroscopeYNoise     = " << std::to_string(imuSensorNoiseDesc->gyroscopeYNoise) << std::endl;
            *ss <<  "gyroscopeZNoise     = " << std::to_string(imuSensorNoiseDesc->gyroscopeZNoise) << std::endl;
            *ss <<  "magnetometerXNoise  = " << std::to_string(imuSensorNoiseDesc->magnetometerXNoise) << std::endl;
            *ss <<  "magnetometerYNoise  = " << std::to_string(imuSensorNoiseDesc->magnetometerYNoise) << std::endl;
            *ss <<  "magnetometerZNoise  = " << std::to_string(imuSensorNoiseDesc->magnetometerZNoise) << std::endl;
            break;
        }
        case MotionSensorRate:
        {
            *ss << "MotionSensorRate " << std::endl;
            iswSolNetPropertyMotionSensorRateDesc *motionSensorRateDesc = (iswSolNetPropertyMotionSensorRateDesc *)data;
            *ss <<  "rate = " << std::to_string(motionSensorRateDesc->rate) << std::endl;
            break;
        }
        case MotionSensorAccel:
        {
            *ss << "MotionSensorAccel " << std::endl;
            iswSolNetPropertyMotionSensorAccel *motionSensorAccel = (iswSolNetPropertyMotionSensorAccel *)data;
            break;
        }
        case MotionSensorGyro:
        {
            *ss << "MotionSensorGyro " << std::endl;
            iswSolNetPropertyMotionSensorGyro *motionSensorGyro = (iswSolNetPropertyMotionSensorGyro *)data;
            break;
        }
        case MotionSensorMag:
        {
            *ss << "MotionSensorMag " << std::endl;
            iswSolNetPropertyMotionSensorMag *motionSensorGyro = (iswSolNetPropertyMotionSensorMag *)data;
            break;
        }
        case StatusBattery:
        {
            *ss << "StatusBattery " << std::endl;
            iswSolNetPropertyStatusBatteryDesc *statusBatteryDesc = (iswSolNetPropertyStatusBatteryDesc *)data;
            *ss <<  "batteryID   = " << std::to_string(statusBatteryDesc->batteryId) << std::endl;
            *ss <<  "reserved[0] = " << std::to_string(statusBatteryDesc->reserved[0]) << std::endl;
            *ss <<  "reserved[1] = " << std::to_string(statusBatteryDesc->reserved[1]) << std::endl;
            *ss <<  "reserved[2] = " << std::to_string(statusBatteryDesc->reserved[2]) << std::endl;
            *ss <<  "description = " << std::to_string(*statusBatteryDesc->description) << std::endl;
            break;
        }
        default:
        {
            *ss << "Unknown " << std::endl;
            break;
        }
    }
}

//!#######################################################################
//! PrintDescriptorIdToDatabase()
//! Inputs: cdss - pointer to stringstream
//!         pss - pointer to stringstream
//!         vss - pointer to stringstream
//!         descriptorId - SolNet number for the descriptor type
//!         data - pointer to a generic data buffer to type cast
//!                based on descriptorId
//! Description: Print the SolNet Property Descriptor Id
//!#######################################################################
void IswSolNet::PrintDescriptorIdToDatabase(std::stringstream *cdss, std::stringstream *pss, std::stringstream *vss, uint16_t descriptorId, uint8_t *data) //#WIP
{
    std::stringstream ss;

    switch (descriptorId)
    {
        case Label:
        {
            iswSolNetPropertyTextLabelDesc *textLabelDesc = (iswSolNetPropertyTextLabelDesc *)data; // #WIP

            *cdss << "Label";
            *pss << DBLogRecord::iaquoted("{label}");
            ss << "{" << textLabelDesc->textLabel << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case Version:
        {
            iswSolNetPropertyVersionDesc *versionDesc = (iswSolNetPropertyVersionDesc *)data;
            *cdss << "Version";
            *pss << DBLogRecord::iaquoted("{version,reserved,versionString}");
            ss << "{" << std::to_string(versionDesc->solNetVersion)
               << "," ;
               for (int i = 0; i < 3; i++)
               {
                   ss << std::to_string(versionDesc->reserved[i]);
               }
            ss << "," << versionDesc->versionText << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case Endpoint:
        {
            iswSolNetPropertyEndpointDesc *endpointDesc = (iswSolNetPropertyEndpointDesc *)data;
            *cdss << "Endpoint";
            *pss << DBLogRecord::iaquoted("{endpointId,dataflowId,endpointDistribution,dataPolicies}");
            ss << "{" << std::to_string(endpointDesc->endpointId)
               << "," << std::to_string(endpointDesc->dataflowId)
               << "," << std::to_string(endpointDesc->endpointDistribution)
               << "," << std::to_string(endpointDesc->dataPolicies) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case MulticastGroup:
        {
            iswSolNetPropertyMulticastGroupDesc *mcastDesc = (iswSolNetPropertyMulticastGroupDesc *)data;
            std::stringstream macAddress;
            for (int i = 0; i < 6; i++)
            {
                macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)mcastDesc->mcastMacAddress[i];
                if ( i < 5 )
                {
                    macAddress << ":";
                }
            }
            *cdss << "McastGroup";
            *pss << DBLogRecord::iaquoted("{mcastGroupId,reserved1,mcastAddress,mcastMacAddress,reserved2}");
            ss << "{" << std::to_string(mcastDesc->mcastGroupId)
               << "," << std::to_string(mcastDesc->reserved1)
               << "," << std::to_string(mcastDesc->mcastAddress)
               << "," << macAddress
               << "," << std::to_string(mcastDesc->reserved2) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case Encoding:
        {
            iswSolNetPropertyEncodingDesc *encodingDesc = (iswSolNetPropertyEncodingDesc *)data;
            *cdss << "Encoding";
            *pss << DBLogRecord::iaquoted("{encoding}");
            ss << "{" << std::to_string(encodingDesc->encoding) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case DeviceName:
        {
            iswSolNetPropertyDeviceDesc *devNameDesc = (iswSolNetPropertyDeviceDesc *)data;
            *cdss << "DeviceName";
            *pss << DBLogRecord::iaquoted("{deviceNameString}");
            ss << "{" << devNameDesc->devString << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case DeviceSerialNumber:
        {
            iswSolNetPropertyDeviceDesc *devSerialNoDesc = (iswSolNetPropertyDeviceDesc *)data;
            *cdss << "DeviceSerialNumber";
            *pss << DBLogRecord::iaquoted("{deviceSerialNoString}");
            ss << "{" << devSerialNoDesc->devString << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case DeviceManufacturer:
        {
            iswSolNetPropertyDeviceDesc *devManufacturerDesc = (iswSolNetPropertyDeviceDesc *)data;
            *cdss << "DeviceManufacturer";
            *pss << DBLogRecord::iaquoted("{deviceManufacturerString}");
            ss << "{" << devManufacturerDesc->devString << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case DeviceFriendlyName:
        {
            iswSolNetPropertyDeviceDesc *devFriendlyNameDesc = (iswSolNetPropertyDeviceDesc *)data;
            *cdss << "DeviceFriendlyName";
            *pss << DBLogRecord::iaquoted("{deviceFriendlyNameString}");
            ss << "{" << devFriendlyNameDesc->devString << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoOutputFormatRaw:
        {
            iswSolNetPropertyVideoOutputFormatRawDesc *videoFormatRawDesc = (iswSolNetPropertyVideoOutputFormatRawDesc *)data;
            *cdss << "VideoOutputFormatRaw";
            *pss << DBLogRecord::iaquoted("{resX,resY,frameRate,bitDepth,pixelformat}");
            ss << "{" << std::to_string(videoFormatRawDesc->resX)
               << "," << std::to_string(videoFormatRawDesc->resY)
               << "," << std::to_string(videoFormatRawDesc->frameRate)
               << "," << std::to_string(videoFormatRawDesc->bitDepth)
               << "," << std::to_string(videoFormatRawDesc->pixelformat) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoFormat:
        {
            *cdss << "VideoFormat";
            *pss << DBLogRecord::iaquoted("{}");
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoReserved2:
        {
            *cdss << "VideoReserved2";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoProtocol:
        {
            iswSolNetPropertyVideoProtocolDesc *videoProtocolRawDesc = (iswSolNetPropertyVideoProtocolDesc *)data;
            *cdss << "VideoProtocolRaw";
            *pss << DBLogRecord::iaquoted("{protocol}");
            ss << "{" << std::to_string(videoProtocolRawDesc->protocol) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoReserved3:
        {
            *cdss << "VideoReserved3";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoReserved4:
        {
            *cdss << "VideoReserved4";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case IFOV:
        {
            iswSolNetPropertyVideoIfovDesc *ifovDesc = (iswSolNetPropertyVideoIfovDesc *)data;

            //! Format for float
            std::stringstream stream;
            stream << std::fixed << std::setprecision(2) << (float)ifovDesc->IFoV;
            *cdss << "VideoIFOV";
            *pss << DBLogRecord::iaquoted("{instantFieldOfView}");
            ss << "{" << stream.rdbuf() << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoReserved5:
        {
            *cdss << "VideoReserved5";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoReserved6:
        {
            *cdss << "VideoReserved6";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case PrincipalPoint:
        {
            iswSolNetPropertyVideoPrincipalPointDesc *videoPPDesc = (iswSolNetPropertyVideoPrincipalPointDesc *)data;
            *cdss << "VideoPrincipalPoint";
            *pss << DBLogRecord::iaquoted("{videoPointX,videoPointY}");
            ss << "{" << std::to_string(videoPPDesc->principalPointX)
               << "," << std::to_string(videoPPDesc->principalPointY) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoReserved7:
        {
            *cdss << "VideoReserved7";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoReserved8:
        {
            *cdss << "VideoReserved8";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoLensDistortion:
        {
            iswSolNetPropertyVideoLensDistortionDesc *videoLensDistortionDesc = (iswSolNetPropertyVideoLensDistortionDesc *)data;
            //! Format for float
            std::stringstream stream;
    //        for (int i; i<6; i++)
    //        {
    //            stream << "[";
    //            stream << std::fixed << std::setprecision(2) << (float)videoLensDistortionDesc->distortionCoefficients[i];
    //            stream << "] ";
    //        }
            *cdss << "VideoIFOV";
            *pss << DBLogRecord::iaquoted("{distortionCoefficients}");
    //        ss << "{" << stream.str() << "}"; // OC
            ss << "{" << stream.rdbuf() << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case VideoReserved9:
        {
            *cdss << "VideoReserved9";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoReserved10:
        {
            *cdss << "VideoReserved10";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoControl:
        {
            iswSolNetPropertyVideoControlDesc *videoControlDesc = (iswSolNetPropertyVideoControlDesc *)data;
            *cdss << "VideoControl";
            *pss << DBLogRecord::iaquoted("{controlBitmap,reserved}");
            ss << "{" << std::to_string(videoControlDesc->controlBitmap)
               << "," << std::to_string(videoControlDesc->reserved) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case ImageSensorType:
        {
            *cdss << "ImageSensorType";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case VideoReserved12:
        {
            *cdss << "VideoReserved12";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case UiMotion:
        {
            iswSolNetPropertySimpleUiMotionDesc *UiMotionDesc = (iswSolNetPropertySimpleUiMotionDesc *)data;
            *cdss << "UiMotion";
            *pss << DBLogRecord::iaquoted("{terminalId,type,flags,xMin,xMax,yMin,yMax,zMin,zMax}");
            ss << "{" <<std::to_string(UiMotionDesc->terminalId)
               << "," << std::to_string(UiMotionDesc->type)
               << "," << std::to_string(UiMotionDesc->flags)
               << "," << std::to_string(UiMotionDesc->xMin)
               << "," << std::to_string(UiMotionDesc->xMax)
               << "," << std::to_string(UiMotionDesc->yMin)
               << "," << std::to_string(UiMotionDesc->yMax)
               << "," << std::to_string(UiMotionDesc->zMin)
               << "," << std::to_string(UiMotionDesc->zMax) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case UiButtonInput:
        {
            iswSolNetPropertySimpleUiButtonInputDesc *uiButtonsDesc = (iswSolNetPropertySimpleUiButtonInputDesc *)data;
            *cdss << "UiButtons";
            *pss << DBLogRecord::iaquoted("{terminalId,capabilities,buttonCount,stateCount}");
            ss << "{" << std::to_string(uiButtonsDesc->terminalId)
               << "," << std::to_string(uiButtonsDesc->capabilities)
               << "," << std::to_string(uiButtonsDesc->buttonCount)
               << "," << std::to_string(uiButtonsDesc->stateCount) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case UiReservedMin:
        {
            *cdss << "UiReservedMin";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case UiReservedMax:
        {
            *cdss << "UiReservedMax";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case ImuOutputRate:
        {
            iswSolNetPropertyImuOutputRateDesc *rtaImuRateDesc = (iswSolNetPropertyImuOutputRateDesc *)data;
            *cdss << "RtaImuOutputRateProperty";
            *pss << DBLogRecord::iaquoted("{rate}");
            ss << "{" << std::to_string(rtaImuRateDesc->rate) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case ImuFloatUpdateRate:
        {
            *cdss << "ImuFloatUpdateRate";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case ImuSensorNoise:
        {
            *cdss << "ImuSensorNoise";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case MotionReserved1:
        {
            *cdss << "MotionReserved1";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case MotionReserved2:
        {
            *cdss << "MotionReserved2";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case NetworkingIP:
        {
            *cdss << "NetworkingIp";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case NetworkingTCPPort:
        {
            *cdss << "NetworkingTcpPort";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case NetworkingUDPPort:
        {
            *cdss << "NetworkingUdpPort";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case NetworkingReserved1:
        {
            *cdss << "NetworkingReserved1";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case NetworkingReserved2:
        {
            *cdss << "NetworkingReserved2";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case ArType2OverlayOutput:
        {
            iswSolNetPropertyArType2OutputDesc *arOverlayDesc = (iswSolNetPropertyArType2OutputDesc *)data;
            *cdss << "ArType2OverlayOutput";
            *pss << DBLogRecord::iaquoted("{minWidth,minHeight,supportedRleVersions}");
            ss << "{" << std::to_string(arOverlayDesc->minWidth)
               << "," << std::to_string(arOverlayDesc->minHeight)
               << "," << std::to_string(arOverlayDesc->supportedRleVersions) << "}";
            *vss << DBLogRecord::ssiaquoted(&ss);
            break;
        }
        case ArReserved1:
        {
            *cdss << "ArReserved1";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case ArReserved2:
        {
            *cdss << "ArReserved2";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case StatusBattery:
        {
            *cdss << "PowerBattery";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case PowerReserved1:
        {
            *cdss << "PowerReserved1";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        case PowerReserved2:
        {
            *cdss << "PowerReserved2";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
        default:
        {
            *cdss << "UNKNOWN";
            *pss << DBLogRecord::iaquoted("{}");
            *vss << DBLogRecord::iaquoted("{}");
            break;
        }
    }
}

//!#######################################################################
//! PrintPeerServicesToLogfile()
//! Inputs: None
//! Description: Print the SolNet peer services to the logfile
//!#######################################################################
void IswSolNet::PrintPeerServicesToLogfile(void)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        //! Print Peer Services
        std::string msgString = "PrintPeerServices for this device:";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
    {
        if ( !iswSolNetPeerServices[peerIndex].inUse )
        {
            continue;
        }

        if ( theLogger->DEBUG_IswSolNet == 1 )
        {
            std::stringstream stringStrm;
            stringStrm << "peerServiceEntry[" << std::to_string(peerIndex) << "]:" << std::endl;
            stringStrm << "    inUse                                 = " << std::to_string(iswSolNetPeerServices[peerIndex].inUse) << std::endl;
            stringStrm << "    sendBrowse                            = " << std::to_string(iswSolNetPeerServices[peerIndex].sendBrowse) << std::endl;
            stringStrm << "    sendAdvertise                         = " << std::to_string(iswSolNetPeerServices[peerIndex].sendAdvertise) << std::endl;
            stringStrm << "    sendRevokeRegistrationResponse        = " << std::to_string(iswSolNetPeerServices[peerIndex].sendRevokeRegistrationResponse) << std::endl;
            stringStrm << "    revokeRegRequestResponseSeqNo         = " << std::to_string(iswSolNetPeerServices[peerIndex].revokeRegRequestResponseSeqNo) << std::endl;
            stringStrm << "    sendRevokeNetworkAssociationResponse  = " << std::to_string(iswSolNetPeerServices[peerIndex].sendRevokeNetworkAssociationResponse) << std::endl;
            stringStrm << "    revokeNetworkAssociationResponseSeqNo = " << std::to_string(iswSolNetPeerServices[peerIndex].revokeNetworkAssociationResponseSeqNo) << std::endl;
            theLogger->DebugMessage(stringStrm.str(), iswInterface->GetIndex());
        }

        for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
        {
            std::stringstream ss;
            iswSolNetPeerServiceEntry *serviceEntry = &iswSolNetPeerServices[peerIndex].services[dataflowId];

            if ( !serviceEntry->inUse )
            {
                continue;
            }

            ss << "    services[" << std::to_string(dataflowId) << "]:" << std::endl;
            ss << "      ImRegistered = " << std::to_string(serviceEntry->ImRegistered) << std::endl;
            ss << "      dataflowId   = " << std::to_string(serviceEntry->dataflowId) << std::endl;
            ss << "      endpointId   = " << std::to_string(serviceEntry->endpointId) << std::endl;
            ss << "      dataPolicies = " << std::to_string(serviceEntry->dataPolicies) << std::endl;
            ss << "      endpointDist = " << std::to_string(serviceEntry->endpointDistribution) << std::endl;
            ss << "      autonomy     = " << std::to_string(serviceEntry->autonomy) << std::endl;

            switch (serviceEntry->status)
            {
                case RegStatusUnavailable:
                {
                    ss << "      status = RegStatusUnavailable" << std::endl;
                    break;
                }
                case RegStatusAvailableToRegister:
                {
                    ss << "      status = RegStatusAvailableToRegister" << std::endl;
                    break;
                }
                case RegStatusAvailableForYourUseOnly:
                {
                    ss << "      status = RegStatusAvailableForYourUseOnly" << std::endl;
                    break;
                }
                case RegStatusUnavailableForRegistration:
                {
                    ss << "      status = RegStatusUnavailableForRegistration" << std::endl;
                    break;
                }
                case RegStatusAvailableForUseYouAndOthers:
                {
                    ss << "      status = RegStatusAvailableForUseYouAndOthers" << std::endl;
                    break;
                }
                default:
                {
                    ss << "      status=Unknown" << std::endl;
                    break;
                }
            }

            if ( serviceEntry->serviceDesc.header.descriptorId == Control )
            {
                ss << "      ServiceControlDesc:" << std::endl;
                ss << "        header.length       = " << std::to_string(serviceEntry->serviceControlDesc.header.length) << std::endl;
                ss << "        header.descriptorId = " << GetServiceIdString(serviceEntry->serviceControlDesc.header.descriptorId) << std::endl;
                ss << "        selectorId          = " << std::to_string(serviceEntry->serviceControlDesc.serviceSelector) << std::endl;
                ss << "        protocolId          = " << std::to_string(serviceEntry->serviceControlDesc.protocolId) << std::endl;
                ss << "        reserved            = " << std::to_string(serviceEntry->serviceControlDesc.reserved) << std::endl;

                if ( serviceEntry->serviceControlDesc.childBlock != nullptr )
                {
                    uint16_t childblockLength = serviceEntry->serviceControlDesc.header.length  - (selectorSize + sizeof(serviceEntry->serviceControlDesc.protocolId) + sizeof(serviceEntry->serviceControlDesc.reserved));
                    ss << "        childblock Length = " << std::to_string(childblockLength) << std::endl;

                    uint16_t offset = 0;
                    while ( offset < childblockLength )
                    {
                        //! Print childblock descriptors
                        iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)&serviceEntry->serviceControlDesc.childBlock[offset];
                        if ( (commonHeader != nullptr) && (commonHeader->length > 0) )
                        {
                            uint8_t *data = &serviceEntry->serviceControlDesc.childBlock[offset];
                            ss << "        Child Descriptor:"<< std::endl;
                            ss << "          header.length       = " << std::to_string(commonHeader->length) << std::endl;
                            ss << "          header.descriptorId = ";
                            PrintDescriptorIdToLogfile(&ss, commonHeader->descriptorId, data);
                            offset += commonHeader->length + commonHeaderSize;
                        }
                    }
                }
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
            else
            {
                ss << "      header.length       = " << std::to_string(serviceEntry->serviceDesc.header.length) << std::endl;
                ss << "      header.descriptorId = " << GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId) << std::endl;
                ss << "      selectorId          = " << std::to_string(serviceEntry->serviceDesc.serviceSelector) << std::endl;

                if ( serviceEntry->serviceDesc.childBlock != nullptr )
                {
                    uint16_t childblockLength = serviceEntry->serviceDesc.header.length - selectorSize;
                    ss << "      childblock Length = " << std::to_string(childblockLength) << std::endl;

                    uint16_t offset = 0;
                    while ( offset < childblockLength )
                    {
                        //! Print childblock descriptors
                        iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)&serviceEntry->serviceDesc.childBlock[offset];
                        if ( (commonHeader != nullptr) && (commonHeader->length > 0) )
                        {
                            uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
                            ss << "      Child Descriptor:" << std::endl;
                            ss << "        header.length       = " << std::to_string(commonHeader->length) << std::endl;
                            ss << "        header.descriptorId = ";
                            PrintDescriptorIdToLogfile(&ss, commonHeader->descriptorId, data);
                            offset += commonHeader->length + commonHeaderSize;
                        }
                    }
                }
                if ( theLogger->DEBUG_IswSolNet == 1 )
                {
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
            }
        }
    }
}

//!#######################################################################
//! PrintServices()
//! Inputs: None
//! Description: Print all the device services to the logfile
//!#######################################################################
void IswSolNet::PrintServicesToLogfile(void)
{
    if ( theLogger->DEBUG_IswSolNet == 1 )
    {
        //! Print this devices services
        std::string msgString = "PrintServices for this device";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    for (uint8_t dataflowId = 0; dataflowId < MAX_NUMBER_APPS; dataflowId++)
    {
        std::stringstream ss;
        iswSolNetServiceEntry *serviceEntry = &iswSolNetServices[dataflowId];

        if ( !serviceEntry->inUse )
        {
            continue;
        }

        ss << "serviceEntry[" << std::to_string(dataflowId) << "]:" << std::endl;
        ss << "    inUse        = " << std::to_string(serviceEntry->inUse) << std::endl;
        ss << "    dataflowId   = " << std::to_string(serviceEntry->dataflowId) << std::endl;
        ss << "    endpointId   = " << std::to_string(serviceEntry->endpointId) << std::endl;
        ss << "    dataPolicies = " << std::to_string(serviceEntry->dataPolicies) << std::endl;
        ss << "    endpointDist = " << std::to_string(serviceEntry->endpointDistribution) << std::endl;

        //! Print the registered peers for this service
        for (uint8_t peerIndex = 0; peerIndex < MAX_PEERS; peerIndex++)
        {
            if ( serviceEntry->registeredPeers[peerIndex].peerRegistered )
            {
                ss << "    RegisteredPeers[" << std::to_string(peerIndex) << "]"  << std::endl;
                ss << "        peerRegistered = " << std::to_string(serviceEntry->registeredPeers[peerIndex].peerRegistered) << std::endl;
                ss << "        flowState      = " << std::to_string(serviceEntry->registeredPeers[peerIndex].flowState) << std::endl;
                ss << "        autonomy       = " << std::to_string(serviceEntry->registeredPeers[peerIndex].autonomy) << std::endl;
                ss << "        status         = " << std::to_string(serviceEntry->registeredPeers[peerIndex].status) << std::endl;
            }
        }

        if ( serviceEntry->serviceDesc.header.descriptorId == Control )
        {
            ss << "  ServiceControlDesc:" << std::endl;
            ss << "      header.length       = " << std::to_string(serviceEntry->serviceControlDesc.header.length) << std::endl;
            ss << "      header.descriptorId = " << GetServiceIdString(serviceEntry->serviceControlDesc.header.descriptorId) << std::endl;
            ss << "      selectorId          = " << std::to_string(serviceEntry->serviceControlDesc.serviceSelector) << std::endl;
            ss << "      protocolId          = " << std::to_string(serviceEntry->serviceControlDesc.protocolId) << std::endl;
            ss << "      reserved            = " << std::to_string(serviceEntry->serviceControlDesc.reserved) << std::endl;

            if ( serviceEntry->serviceControlDesc.childBlock != nullptr )
            {
                uint16_t childblockLength = serviceEntry->serviceControlDesc.header.length - (IswSolNet::selectorSize +
                                                                                              sizeof(serviceEntry->serviceControlDesc.protocolId) +
                                                                                              sizeof(serviceEntry->serviceControlDesc.reserved));

                ss << "      childblock Length = " << std::to_string(childblockLength) << std::endl;

                uint16_t offset = 0;
                while ( offset < childblockLength )
                {
                    //! Print childblock descriptors
                    iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)&serviceEntry->serviceControlDesc.childBlock[offset];
                    if ( commonHeader->descriptorId > 0 )
                    {
                        uint8_t *data = &serviceEntry->serviceControlDesc.childBlock[offset];
                        ss << "Child Descriptor:" << std::endl;
                        ss << "      header.descriptorId = ";
                        PrintDescriptorIdToLogfile(&ss, commonHeader->descriptorId, data);
                        ss << "      header.length = " << std::to_string(commonHeader->length) << std::endl;
                        ss << "      selectorId    = " << std::to_string(serviceEntry->serviceControlDesc.serviceSelector) << std::endl;
                        offset += commonHeader->length + commonHeaderSize;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
        }
        else
        {
            ss << "    ServiceDesc:" << std::endl;
            ss << "        header.descriptorId = " << GetServiceIdString(serviceEntry->serviceDesc.header.descriptorId) << std::endl;
            ss << "        header.length       = " << std::to_string(serviceEntry->serviceDesc.header.length) << std::endl;
            ss << "        selectorId          = " << std::to_string(serviceEntry->serviceDesc.serviceSelector) << std::endl;

            if ( serviceEntry->serviceDesc.childBlock != nullptr )
            {
                uint16_t childblockLength = serviceEntry->serviceDesc.header.length - sizeof(serviceEntry->serviceDesc.serviceSelector);
                ss << "        childblock Length = " << std::to_string(childblockLength) << std::endl;

                uint16_t offset = 0;
                while ( offset < childblockLength )
                {
                    //! Print childblock descriptors
                    iswSolNetDescCommonHeader *commonHeader = (iswSolNetDescCommonHeader *)&serviceEntry->serviceDesc.childBlock[offset];
                    if ( commonHeader->descriptorId > 0 )
                    {
                        uint8_t *data = &serviceEntry->serviceDesc.childBlock[offset];
                        ss << "Child Descriptor:" << std::endl;
                        ss << "        header.length       = " << std::to_string(commonHeader->length) << std::endl;
                        ss << "        header.descriptorId = ";
                        PrintDescriptorIdToLogfile(&ss, commonHeader->descriptorId, data);
                        offset += commonHeader->length + commonHeaderSize;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            if ( theLogger->DEBUG_IswSolNet == 1 )
            {
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
        }
    }

    PrintPeerServicesToLogfile();
}

/******************************************************************************
 * restoreQosDescriptors
 *
 * Description: The IswSolNet::AddSolNetService() function copies a buffer
 * received from iswTestTool. The VideoService might include a QoS data rate
 * descriptor, embedded within multiple layers of childblocks. During the copy
 * process in AddSolNetService(), the childblock pointer references are lost
 * and must be restored, specifically for the QoS data rate descriptor. This
 * function restores the references which are contained by the instance of
 * IswSolNet.
 *
 * Input: iswSolNetServiceDesc *serviceDesc: A reference (i.e. a pointer) to a
 * service descriptor, which contains a childblock. The childblock contains
 * the data elements that form QoS Properties and Qos Data Rate descriptors.
 *
 * Output: This function points the service descriptor childblock to the QoS
 * Properties descriptor, and points the QoS Properties descriptor childblock
 * to the QoS Data Rate descriptor, in accordance with the ISW SolNet
 * specification document for TWS.
 *****************************************************************************/
void IswSolNet::restoreQosDescriptors(iswSolNetServiceDesc *serviceDesc)
{
    int childBlockSize = serviceDesc->header.length - selectorSize;
    int offset = 0;

    uint8_t *buffer = serviceDesc->childBlock;

    iswSolNetEndpointQosPropertiesDesc *propertiesDesc = nullptr;
    iswSolNetEndpointQosDataRateDesc *dataRateDesc = nullptr;
    iswSolNetPropertyEndpointDesc *endpointDesc = nullptr;
    iswSolNetDescCommonHeader *commonHdr = nullptr;

    //! Iterate over entire childblock buffer, saving references to QOS data
    //! rate, QoS properties, and endpoint descriptors
    qDebug() << Q_FUNC_INFO << "Child block size:" << childBlockSize;
    while(offset < childBlockSize)
    {
        commonHdr = (IswSolNet::iswSolNetDescCommonHeader *)(buffer+offset);
        qDebug() << Q_FUNC_INFO << "Common header descriptor ID:" << hex << commonHdr->descriptorId;
        qDebug() << Q_FUNC_INFO << "Offset:" << offset;
        switch(commonHdr->descriptorId)
        {
            case IswSolNet::iswSolNetPropertyDescIds::QoSDataRate:
            {
                dataRateDesc = (IswSolNet::iswSolNetEndpointQosDataRateDesc *)(commonHdr);
                offset += iswSolNetEndpointQosDataRateDescSize;
                break;
            }
            //! ###############################################################
            //! The Endpoint, QoS Properties, Image Sensor Waveband, and Video
            //! Format descriptors are defined with structs containing uint8_t
            //! pointers, used to represent optional or variable components
            //! contained within the descriptor (e.g. the QoS Properties
            //! descriptor contains a QoS Data Rate descriptor, which is
            //! referenced by a pointer).
            //!
            //! Since these pointers are not explicitly written into the ISW
            //! SolNet specification, they cannot be counted as part of the
            //! core block size of the descriptor (i.e. the size requirements
            //! of a pointer is not counted in the lenght field of the common
            //! header descriptor); however, the pointer memory size
            //! needs to be accounted for when traversing the Service Class
            //! Descriptor child block in order to avoid typecasting to invalid
            //! points in memory. The following case statements provide the
            //! appropriate offsets to avoid invalid references.
            //! ###############################################################
            case IswSolNet::iswSolNetPropertyDescIds::QoSProperties:
            {
                propertiesDesc = (IswSolNet::iswSolNetEndpointQosPropertiesDesc *)(commonHdr);
                offset += iswSolNetEndpointQosPropertiesDescSize;
                break;
            }
            case IswSolNet::iswSolNetPropertyDescIds::Endpoint:
            {
                printDescriptor(commonHdr);
                endpointDesc = (IswSolNet::iswSolNetPropertyEndpointDesc *)(commonHdr);
                offset += iswSolNetPropertyEndpointDescSize;
                break;
            }
            case IswSolNet::iswSolNetPropertyDescIds::VideoFormat:
            {
                offset += iswSolNetPropertyVideoFormatDescSize;
                break;
            }
            case IswSolNet::iswSolNetPropertyDescIds::ImageSensorWaveband:
            {
                offset += iswSolNetPropertyVideoImageSensorWavebandDescSize;
                break;
            }
            default:
            {
                offset += commonHdr->length + commonHeaderSize;
                break;
            }
        }
    }
    //! If QoS data rate, QoS properties, and endpoint descriptors are valid
    //! (i.e. not nullptr) references, restore childblock pointers accordingly
    if(nullptr == propertiesDesc)
    {
        qDebug() << Q_FUNC_INFO << "Unable to find QoS properties descriptor";
    }
    else if(nullptr == dataRateDesc)
    {
        qDebug() << Q_FUNC_INFO << "Unable to find QoS data rate descriptor";
    }
    else if(nullptr == endpointDesc)
    {
        qDebug() << Q_FUNC_INFO << "Unable to find endpoint descriptor";
    }
    else
    {
//        propertiesDesc->childBlock = (uint8_t *)(dataRateDesc);
//        endpointDesc->childBlock = (uint8_t *)(propertiesDesc);
    }
}

//!############################################################################
//! Name: restoreVideoFormatDescriptors
//! Description: The IswSolNet::AddSolNetService() function copies a buffer
//! received from iswTestTool. The Video Service class descriptor will include
//! a Video Sample Format Descriptor and might include a Maximum Frame Size
//! Descriptor, both of which must be embedded into the child block of a Video
//! Format Property Descriptor. This function parses the child block of the
//! Video Service class descriptor, allocates a buffer
//!
//!
//! Input: commondHdr: iswSolNetDescCommonHeader pointer to the common
//! header corresponding with an ISW SolNet descriptor that needs to be
//! printed for debugging purposes.
//!
//! Output: A single space delimited hexadecimal representation of the
//! bytes that form an ISW SolNet descriptor, printed to stdout.
//!############################################################################
void IswSolNet::restoreVideoFormatDescriptors(iswSolNetServiceDesc *serviceDesc)
{
    qDebug() << Q_FUNC_INFO << "start.";
    int childBlockSize = serviceDesc->header.length - selectorSize;
    int offset = 0;

    uint8_t *buffer = serviceDesc->childBlock;

    iswSolNetPropertyVideoFormatDesc *videoFormatDesc = nullptr;
    iswSolNetPropertyVideoSampleFormatDesc *sampleFormatDesc = nullptr;
    iswSolNetDescCommonHeader *commonHdr = nullptr;

    while(offset < childBlockSize)
    {
        commonHdr = (iswSolNetDescCommonHeader *)(buffer+offset);
        qDebug() << Q_FUNC_INFO << "Common header descriptor ID:" << hex << commonHdr->descriptorId;
        printDescriptor(commonHdr);
        switch(commonHdr->descriptorId)
        {
            case IswSolNet::iswSolNetPropertyDescIds::VideoFormat:
            {
                qDebug() << Q_FUNC_INFO << "found video format descriptor";
                printDescriptor(commonHdr);
                videoFormatDesc = (iswSolNetPropertyVideoFormatDesc *)(commonHdr);
                offset += sizeof(iswSolNetPropertyVideoFormatDesc);
                break;
            }
            case IswSolNet::iswSolNetPropertyDescIds::VideoSampleFormat:
            {
                qDebug() << Q_FUNC_INFO << "found video sample format descriptor";
                sampleFormatDesc = (iswSolNetPropertyVideoSampleFormatDesc *)(commonHdr);
                offset += sizeof(iswSolNetPropertyVideoSampleFormatDesc);
                break;
            }
            //! ###############################################################
            //! The Endpoint, QoS Properties, and Image Sensor Waveband
            //! descriptors are defined with structs containing uint8_t
            //! pointers, used to represent optional or variable components
            //! contained within the descriptor (e.g. the QoS Properties
            //! descriptor contains a QoS Data Rate descriptor, which is
            //! referenced by a pointer).
            //!
            //! Since these pointers are not explicitly written into the ISW
            //! SolNet specification, they cannot be counted as part of the
            //! core block size of the descriptor (i.e. the size requirements
            //! of a pointer is not counted in the lenght field of the common
            //! header descriptor); however, the pointer memory size
            //! needs to be accounted for when traversing the Service Class
            //! Descriptor child block in order to avoid typecasting to invalid
            //! points in memory. The following case statements provide the
            //! appropriate offsets to avoid invalid references.
            //! ###############################################################
            default:
            {
                offset += commonHdr->length + commonHeaderSize;
                break;
            }
        }
    }
//    videoFormatChildBlock = videoFormatDesc->childBlock;
    if(nullptr != sampleFormatDesc)
    {
//        videoFormatDesc->childBlock = (uint8_t *)sampleFormatDesc;
        qDebug() << Q_FUNC_INFO << "Video Format Descriptor Length:" << videoFormatDesc->header.length;
    }
    else
    {
        qDebug() << Q_FUNC_INFO << "Unable to locate Video Format Descriptor child block";
    }


    qDebug() << Q_FUNC_INFO << "end.";
}

//!############################################################################
//! Name: printDescriptor
//! Description: Given an ISW SolNet descriptor common header, print a
//! space delimited hexadecimal representation of the bytes that
//! form an ISW SolNet descriptor.
//!
//! Input: commondHdr: iswSolNetDescCommonHeader pointer to the common
//! header corresponding with an ISW SolNet descriptor that needs to be
//! printed for debugging purposes.
//!
//! Output: A single space delimited hexadecimal representation of the
//! bytes that form an ISW SolNet descriptor, printed to stdout.
//!############################################################################
void IswSolNet::printDescriptor(iswSolNetDescCommonHeader *commonHdr)
{
    int i = 0;
    int length = commonHdr->length + commonHeaderSize;

    uint8_t *bytePtr = (uint8_t *)commonHdr;
    char hex[3];
    std::stringstream ss;
    ss << Q_FUNC_INFO << "Descriptor bytes: ";
    for(i = 0; i < length; i++)
    {
        sprintf(hex, "%02x", *(bytePtr+i));
        ss << hex << " ";
    }
    ss << std::endl;
    std::cout << ss.str();
}

//!#############################################################################
//! Name: serviceAdded
//!
//! Description: Check the SolNet services array for the specified Service
//! Descriptor ID
//!
//! Input: Service Descriptor ID from ISW SolNet Specification
//!
//! Output: Returns true if the corresponding service is found
//! (i.e. the service has been added to the device) or false if the
//! corresponding service is not found
//!#############################################################################
bool IswSolNet::serviceAdded(uint16_t serviceId)
{
    bool found = false;
    for(auto iter = iswSolNetServices.begin(); iter != iswSolNetServices.end(); ++iter)
    {
        if(serviceId == iter->serviceControlDesc.header.descriptorId)
        {
            found = true;
        }
    }
    return found;
}

//!#############################################################################
//! Name: getServiceSelector
//!
//! Description: Check the SolNet services array for the specified Service
//! Descriptor ID, and return the corresponding Service Selector (if found)
//!
//! Input: Service Descriptor ID from ISW SolNet Specification, pointer to a
//! Service Selector
//!
//! Output: Returns true if the corresponding service is found
//! (i.e. the service has been added to the device) or false if the
//! corresponding service is not found. Sets the Service Selector pointer, if
//! the corresponding service is found
//!#############################################################################
bool IswSolNet::getServiceSelector(uint16_t serviceId, uint32_t* serviceSelector)
{
    bool found = false;
    for(auto iter = iswSolNetServices.begin(); iter != iswSolNetServices.end(); ++iter)
    {
        if(serviceId == iter->serviceControlDesc.header.descriptorId)
        {
            *serviceSelector = iter->serviceControlDesc.serviceSelector;
            qDebug() << Q_FUNC_INFO << "Service Selector:" << *serviceSelector;
            found = true;
        }
    }
    return found;
}
