//!###############################################
//! Filename: IswSolNet.h
//! Description: Class that provides an interface
//!              to ISW SolNet commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWSOLNET_H
#define ISWSOLNET_H

#include <mutex>
#include <pthread.h>
#include <string.h>
#include <list>
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswSolNet
{

public:

    //! IswSolNet Public Constructor
    IswSolNet(IswInterface *isw_interface, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass);
    ~IswSolNet() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;
    Logger *throughputTestLogger   = nullptr;
    Logger *throughputResultsLogger = nullptr;

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! Methods to control the SolNet class object
    //! Stop this object and stop all send/receive threads
    void StopSolNet(void);

    //! Check whether SolNet is stopped
    bool IsSolNetStopped(void) { return solNetStopped; }

    //! Allow the user to set a flag to indicate that incoming
    //! Advertise messages should be verified against the ISW SolNet spec
    void setVerifyAdvertiseMsgs(bool verify) { verifyAdvertiseMessages = verify; }

    //! Allow the user to set a flag to indicate whether or not
    //! to verify the checksum fields
    void setVerifyChecksum(bool verify) { verifyChecksum = verify; }

    //! Get or Set the retransmit timeout and
    //! number of times to retransmit
    uint64_t GetRetransTimeout(void) { return (iswSolNetRetransTimeout); }
    uint8_t GetNoOfRetrans(void) { return (iswSolNetNoOfRetrans); }
    void SetRetransmitValues(uint64_t timeout, uint8_t noOfRetrans);

    //!#############################################################################
    //! Name: serviceAdded
    //!
    //! Description: Check the SolNet services array for the specified Service
    //! Descriptor ID
    //!
    //! Input: Service Descriptor ID from ISW SolNet Specification
    //!
    //! Output: Returns true if the corresponding service is found
    //! (i.e. the service has been added to the device) or false if the
    //! corresponding service is not found
    //!#############################################################################
    bool serviceAdded(uint16_t serviceId);

    //!#############################################################################
    //! Name: getServiceSelector
    //!
    //! Description: Check the SolNet services array for the specified Service
    //! Descriptor ID, and return the corresponding Service Selector (if found)
    //!
    //! Input: Service Descriptor ID from ISW SolNet Specification, pointer to a
    //! Service Selector
    //!
    //! Output: Returns true if the corresponding service is found
    //! (i.e. the service has been added to the device) or false if the
    //! corresponding service is not found. Sets the Service Selector pointer, if
    //! the corresponding service is found
    //!#############################################################################
    bool getServiceSelector(uint16_t serviceId, uint32_t* serviceSelector);

    //***************************************************************
    //***************************************************************
    // *  Don't change these two structs:
    // *    iswSolNetMessagePacket
    // *    iswSolNetDataPacket
    // *  The "pad" field is included in the payloadMsgBufferSize.  The 
    // *  code inserts each message, which is smaller than the buffer, 
    // *  then backs up the checksum field to the end of the data - 
    // *  accounting for the "pad" byte. The code is written, has been 
    // *  working for a long time, and has unit tests that verify it.
    //***************************************************************

    //! ISW SolNet command structures
    //!
    //! Payload size is calculated based on USB MAX packet size of 512
    //! SWIM_Header + SolNet Common Header + SolNetMessagePktHdr + payload + checksum = 512
    //!      8      +         2            +         12          +   488   +   2      = 512
    static const uint16_t payloadMsgBufferSize = 488;
    static const uint16_t messagePacketHdrSize = 10; //! Size up to and including payloadLength for checksum calculation

    //! SolNet Message Packet Structure
    struct iswSolNetMessagePacket
    {
        uint8_t messageClass;                          //! Class of messages
        uint8_t messageId;                             //! Identifier of the message within the message class
        uint8_t seqNumber;                             //! Incrementing count
        uint8_t policies;                              //! Packet policies
        uint8_t status;                                //! Message status (0 for requests; variable for responses)
        uint8_t reserved1;                             //! Reserved for future
        uint16_t headerChecksum;                       //! Checksum for header only
        uint16_t messageLength;                        //! Length of message payload, in bytes
        uint8_t	messagePayload[payloadMsgBufferSize];  //! Block containing the message payload; size is payload[488]
        uint16_t payloadChecksum;                      //! Checksum for payload; optional, specified by policies field
    }__attribute__((packed));

    //! Payload size is calculated based on USB MAX packet size of 4064
    //! SWIM_Header + SolNet Common Header + SolNetDataPktHdr + payload + checksum = 4064
    //!      8      +         2            +         12          +   4040   +   2      = 4064
    static const uint16_t payloadDataBufferSize   = 4040;
    const static uint8_t iswSolNetMessageResponse = 0x80;
    const static uint8_t iswSolNetMessageIdMask   = 0x7F;
    static const uint16_t dataPacketHdrSize       = 10;  //! Size up to and including payloadLength for checksum calculation

    //! SolNet Data Packet Structure
    struct iswSolNetDataPacket
    {
        uint8_t dataflowId;                         //! Logical flows over a single stream
        uint8_t reserved1;                          //! Reserved for future specification
        uint16_t seqNumber;                         //! Incrementing count
        uint8_t policies;                           //! Packet policies
        uint8_t reserved2;                          //! Reserved for future specification
        uint16_t headerChecksum;                    //! Checksum for header only
        uint16_t dataLength;                        //! Length of data payload, in bytes
        uint8_t dataPayload[payloadDataBufferSize]; //! Block containing the data payload; size is payload[4040]
        uint16_t payloadChecksum;                   //! Checksum for payload; optional, specified by policies field
    }__attribute__((packed));

   //***************************************************************
   //***************************************************************

    //! SolNet Protocol
    enum iswSolNetProtocol: uint8_t
    {
        DataPacket      = 0x00,
        MessagePacket   = 0x01
        //! 0x02 – 0x7F	Reserved for future specification
        //! 0x80 – 0xEF	Reserved for vendor use
    };

    //! ISW SolNet Header Structure
    struct iswSolNetHeader
    {
        uint8_t version;             //! SolNet version
        uint8_t protocolClass;       //! Data = 0x00, Message = 0x01
        union
        {
            iswSolNetMessagePacket iswSolNetMessagePkt;
            iswSolNetDataPacket iswSolNetDataPkt;
        };
    }__attribute__((packed));

    //! Ack Packet Structure
    struct iswSolNetAckPacket
    {
        uint8_t endpoint;           //! Endpoint number of acknowledged packet
        uint8_t	dataflowId;         //! number of acknowledged packet
        uint16_t seqNumber;         //! sequence number of packet being acked
        uint8_t	status;             //! ACK (0), NAK (1)
        uint8_t	protocolClass;      //! 0 = data packet, 1 = message packet
        uint16_t reserved;          //! holds peerIndex internally
    }__attribute__((packed));

    //! Window Ack Packet Structure
    struct iswSolNetWindowAckPacket
    {
        uint8_t endpoint;           //! Endpoint number of acknowledged packet
        uint8_t	dataflowId;         //! number of acknowledged packet
        uint16_t seqNumber;         //! sequence number of packet being acked
        uint8_t windowBitmap[3];    //! Bitmap where the lsb of the lowest byte is the starting sequence number and each ascending bit represents the next sequence number; Bit value = ACK(0) or NAK(1)
        uint8_t	status;             //! ACK (0), NAK (1)
        uint8_t	protocolClass;      //! 0 = data packet, 1 = message packet
        uint8_t reserved[2] ;       //! holds peerIndex internally
    }__attribute__((packed));

    static const uint8_t Size_SolNet_Header_Only = 2;
    static const uint8_t Size_SolNet_MsgPktHdr   = (sizeof(iswSolNetMessagePacket) - payloadMsgBufferSize);
    static const uint8_t Size_SolNet_DataPktHdr  = (sizeof(iswSolNetDataPacket) - payloadDataBufferSize);

    //! Flow Protocol payloads
    //! Register Request Command
    struct iswSolNetRegisterRequest
    {
        uint32_t serviceSelector;   //! The selector of the service with which to register
        uint8_t autonomy;           //! 0 = wait to be polled; 1 = send data automatically to registered node(s)
        uint8_t reserved[3];        //! Reserved for future specification
    }__attribute__((packed));

    uint16_t iswSolNetRegisterRequestSize = sizeof (IswSolNet::iswSolNetRegisterRequest);

    //! Autonomous Start/Stop Request Command
    struct iswSolNetAutonomousStartStopRequest
    {
        uint32_t serviceSelector;      //! The service with which to register
        uint8_t	 flowState;            //! 0 = stop dataflow; 1 = start dataflow
        uint8_t	 reserved[3];          //! for future specification
    }__attribute__((packed));

    uint16_t iswSolNetAutonomousStartStopRequestSize = sizeof (IswSolNet::iswSolNetAutonomousStartStopRequest);

    enum iswSolNetAutonomousFlowMessageStatus: uint8_t
    {
        AUTONOMOUS_OPERATION_NOT_PERFORMED = 0x80
    };
 
    //! Get Status Response Command
    struct iswSolNetGetStatusResponse
    {
        uint8_t serviceStatus;         //! Status of the service, relative to the requesting node.
        uint8_t	reserved[3];           //! for future specification
    }__attribute__((packed));

    uint16_t iswSolNetGetStatusResponseSize = sizeof (IswSolNet::GetStatusResponse);

    struct iswSolNetKeepAliveRequest
    {
        uint32_t serviceSelector;      //! The service with which to register
        uint8_t	 initiator;            //! 0 = service provider; 1 = service subscriber
        uint8_t	 reserved[3];          //! for future specification
    }__attribute__((packed));

    uint16_t iswSolNetKeepAliveRequestSize = sizeof (IswSolNet::iswSolNetKeepAliveRequest);

    struct iswSolNetKeepAliveResponse
    {
        uint32_t serviceSelector;       //! The service with which to register
        uint8_t	 initiator;             //! 0 = service provider; 1 = service subscriber
        uint8_t	 reserved[3];           //! for future specification
    }__attribute__((packed));

    uint16_t iswSolNetKeepAliveResponseSize = sizeof (IswSolNet::iswSolNetKeepAliveResponse);

    enum iswSolNetKeepAliveStatus: uint8_t
    {
        NoLongerAvailable   = 0x80,
        SenderNotRegister   = 0x81,
        ReceiverNotRegister = 0x82
    };

    enum iswSolNetAutonomy:uint8_t
    {
        WAIT_TO_BE_POLLED                       = 0x00,
        SEND_AUTOMATIC_AFTER_AUTONOMOUS_MESSAGE = 0x02,
        SEND_AUTOMATIC_AFTER_REGISTRATION       = 0x03
    };

    enum iswSolNetFlowRequest: uint8_t
    {
        STOP_DATAFLOW  = 0x00,
        START_DATAFLOW = 0x01
    };

    enum iswSolNetFlowResult: uint8_t
    {
        DATAFLOW_STOPPED  = 0x00,
        DATAFLOW_STARTED  = 0x01
    };

    struct iswSolNetReportDataflowConditionIndication
    {
        float_t serviceSelector;      //! The service with which to register
        uint8_t endpointNumber;
        uint8_t dataflowId;
        uint8_t condition;            //! Based on conditions under iswSolNetDataflowCondition enumeration
        uint8_t reserved1;
    }__attribute__((packed));

    uint16_t iswSolNetReportDataflowConditionIndicationSize = sizeof (IswSolNet::iswSolNetReportDataflowConditionIndication);

    enum iswSolNetDataflowCondition: uint8_t
    {
        UNKNOWN_ENDPOINT_DATAFLOW        = 0x00,
        UNREGISTERED_ENDPOINT_DATAFLOW   = 0x01,
        CONGESTED_ENDPOINT_DATAFLOW      = 0x02
    };

    enum iswSolNetKeepAliveInitiator: uint8_t
    {
        SERVICE_PROVIDER   = 0x00,
        SERVICE_SUBSCRIBER = 0x01
    };

    //! *********************************
    //! SolNet Service Descriptor definitions
    //! References:
    //! ISW SolNet Protocol Specification -- Section 6.7.3 (Table 6.4)
    //! These are root descriptor Ids
    //! *********************************
    enum iswSolNetServiceIds: uint16_t
    {
        Reserved1               = 0x0000,	//! RESERVED	Reserved for future specification
        VideoService            = 0x0001,	//! VIDEO	Root service descriptor representing a video provider or consumer
        Audio                   = 0x0002,	//! AUDIO	Root service descriptor representing an audio provider or consumer
        PositionSense           = 0x0003,	//! POSITION SENSE	Root service descriptor representing a provider or consumer of position, presence, and proximity information (magnetometer, geomagnetic sensor, proximity sensor)
        EnvironmentSense        = 0x0004,	//! ENVIRONMENT SENSE	Root service descriptor representing a provider or consumer of environmental information (e.g. temperature)
        HealthSense             = 0x0005,	//! HEALTH SENSE	Root service descriptor representing a provider or consumer of health information (e.g. heart rate) for one or more individuals
        SimpleUI                = 0x0006,	//! SIMPLE_UI	Root service descriptor representing a provider or consumer of simple user interface commands or operations.
        Control                 = 0x0007,	//! CONTROL	Root service descriptor representing provider or consumer of a control protocol.
        Status                  = 0x0008,	//! STATUS	Root service descriptor representing status information shared with other nodes.
        RTA_SERVICE             = 0x0009,	//! RTA	Root service descriptor representing a rapid target acquisition (RTA) provider or consumer
        ArType1                 = 0x000A,	//! AR Type 1 Base Service Descriptor representing provider or consumer of augmented reality (AR) for ENVG-B DR AR ICD
        HybridSense             = 0x000B,       //! HYBRID SENSE Base Service Descriptor representing a provider or consumer of a sensor service that combines other sensor information
        MotionSense             = 0x000C,       //! MOTION SENSE Base Service Descriptor representing a provider or consumer of motion, velocity, and displacement information (accelerometer, gyroscope)
        TemperatureSense        = 0x000D,	//! TEMPERATURE SENSE	Root service descriptor representing a provider or consumer of temperature information (thermometer, thermal/infrared, pulse)
        MoistureSense           = 0x000E,	//! MOISTURE SENSE	Root service descriptor representing a provider or consumer of moisture information (humidity, moisture)
        VibrationSense          = 0x000F,	//! VIBRATION SENSE	Root service descriptor representing a provider or consumer of vibration information (microphone, hydrophone, seismometer/geophone, musical pickup)
        ChemicalSense           = 0x0010,	//! CHEMICAL SENSE	Root service descriptor representing a provider or consumer of chemical information (gas)
        FlowSense               = 0x0011,	//! FLOW SENSE	Root service descriptor representing a provider or consumer of flow information (flow meter)
        ForceSense              = 0x0012,	//! FORCE SENSE	Root service descriptor representing a provider or consumer of force, torque, load, strain, or pressure information (barometer, surface acoustic wave)
        LrfService              = 0x0013,       //! LRF  Root service descriptor representing laser range finder (LRF) range data.
        LrfControlService       = 0x0014,       //! LRF Control  Root service descriptor representing laser range finder (LRF) control.
        AR_OVERLAY              = 0x0015,       //! RESERVED Reserved for future specification
        ArService               = 0x0016,       //! AR Service Composite service descriptor representing augmented reality (AR) provider or consumer
        Pipe                    = 0x0017,       //! PIPE  Base service descriptor representing a pipe for tunneling a protocol between devices
        ReservedMin             = 0x0018,       //! RESERVED	Reserved for future specification
        ReservedMax             = 0x007F,
        PacketGenerator         = 0x008F        //! Packet Generator Service
    };

    //! Service Protocol Ids
    enum iswSolNetServiceProtocolIdRanges: uint16_t
    {
        Unspecified             = 0x0000,
        ProtocolReservedMin     = 0x0001,   //! Reserved for future specification
        ProtocolReservedMax     = 0x7FFF,
        ProtocolVendorMin       = 0x8000,   //! Values in this range are permitted for vendor use
        ProtocolVendorMax       = 0xFFFF
    };

    //! *********************************
    //! SolNet Property Descriptor Definitions
    //! References:
    //! ISW SolNet Protocol Specification -- Section 6.7.3 (Table 6.7)
    //! These are property descriptor Ids
    //! *********************************
    enum iswSolNetPropertyDescIds: uint16_t
    {
        Label                       = 0x0100,   //! DEPRECATED Text string that provides a label for the service or descriptor of which it is a child
        Version                     = 0x0101,   //! Descriptor defining highest SolNet version supported by this node
        Endpoint                    = 0x0102,   //! Descriptor containing information about an endpoint and its operational parameters.
        MulticastGroup              = 0x0103,   //! Descriptor that represents a multicast group.
        Encoding                    = 0x0104,   //! Descriptor that provides dataflow hints
        FlowPolicy                  = 0x0105,   //! Descriptor that defines flow policies, including support for polling/autonomous data
        DeviceName                  = 0x0106,   //! Descriptor containing text string describing the device name.
        DeviceSerialNumber          = 0x0107,   //! Descriptor containing text string describing the device serial number.
        DeviceManufacturer          = 0x0108,   //! Descriptor containing text string describing the device manufacturer.
        DeviceFriendlyName          = 0x0109,   //! Descriptor containing text string describing the device friendly name.
        AssociatedService           = 0x010A,   //! Identifies a service associated with this service
        EndpointProtocol            = 0x010B,   //! Identifies protocol or formatting of commands/data flowing over an endpoint
        TextLabel                   = 0x010C,   //! Defines parent descriptor. Human readable UTF text string (Supersedes LABEL)
        Device                      = 0x010D,   //! Container for all device specific descriptors
        QoSProperties               = 0x010E,   //! Defines QoS related properties for endpoints
        QoSDataRate                 = 0x010F,   //! Defines the data rate for the service data flow
        QoSMaxLatency               = 0x0110,   //! Defines the maximum latency need by the endpoint/dataflows and set per peer.
        DeviceSoftwareDesc          = 0x0111,   //! Descriptor containing text string describing the device software.
        DeviceSoftwareNameDesc      = 0x0112,   //! Descriptor containing text string describing the device software name.
        DeviceSoftwareVersionDesc   = 0x0113,   //! Descriptor containing text string describing the device software version.
        VideoOutputFormatRaw        = 0x0800,   //! Raw video format
        VideoFormat                 = 0x0801,   //! Video Format
        VideoSampleFormat           = 0x0802,   //! Pixel Format of the Video Format
        MaximumFrameSize            = 0x0803,   //! Maximum JPEG Frame Size
        RleFormat                   = 0x0804,   //! Run Length Encoding (RLE) Format
        ArVideoInput                = 0x0880,   //! AR Video Input Format
        VideoReserved2              = 0x08FF,	//! Reserved
        VideoProtocol               = 0x0900,   //! Video Packetization Protocol
        VideoReserved3              = 0x0901,   //! Reserved for future specification
        VideoReserved4              = 0x097F,   //! Reserved
        IFOV                        = 0x0980,   //! Instantaneous Field of View
        VideoReserved5              = 0x0981,   //! Reserved for future specification
        VideoReserved6              = 0x09FF,   //! Reserved
        PrincipalPoint              = 0x0A00,   //! Principal Point
        VideoReserved7              = 0x0A01,   //! Reserved for future specification
        VideoReserved8              = 0x0A7F,   //! Reserved
        VideoLensDistortion         = 0x0A80,   //! Video lens distortion parameters
        VideoReserved9              = 0x0A81,   //! Reserved for future specification
        VideoControl                = 0x0B00,   //! Various video controls
        VideoReserved10             = 0x0AFF,   //! Reserved
        ImageSensorType             = 0x0B01,   //! Video Image Sensor Type
        ImageSensorWaveband         = 0x0B02,   //! Video image sensor waveband
        VideoIntrinsicParameters    = 0x0B80,   //! Video intrinsic camera parameters
        VideoFrameRate              = 0x0B90,   //! Video output frame rate
        VideoReserved12             = 0x0BFF,   //! Reserved
        PositionDevice              = 0x1000,   //! Position Device
        UiMotion                    = 0x1C00,	//! Descriptor representing a user interface motion input (e.g. mouse, trackball, pointer, etc.)
        UiButtonInput               = 0x1C01,	//! Descriptor representing a set of user interface buttons (e.g. keyboard, keypad, button bar, etc.)
        UiReservedMin               = 0x1C02,	//! Values reserved for future specification.
        UiReservedMax               = 0x1FFF,	//! Reserved
        ImuOutputRate               = 0x2400,   //! IMU raw output rate, represented as an unsigned integer
        ImuFloatUpdateRate          = 0x2401,   //! IMU float update rate, as represented as a floating number
        ImuSensorNoise              = 0x2402,
        ArImuInputProperty          = 0x2403,   //! AR IMU Input Property
        MotionSensorRate            = 0x2500,   //! Motion Sensor Rate
        MotionSensorAxes            = 0x2502,   //! Motion Sensor Axes
        MotionSensorAccel           = 0x2600,   //! Motion Sensor Acceleration
        MotionSensorGyro            = 0x2601,   //! Motion Sensor Gyro
        MotionSensorMag             = 0x2602,   //! Motion Sensor Mag
        MotionReserved1             = 0x2700,
        MotionReserved2             = 0x27FF,
        NetworkingIP                = 0x2800,   //! Descriptor that describes basic IP parameters of a service
        NetworkingTCPPort           = 0x2801,   //! Descriptor representing a TCP port supported by an IP service
        NetworkingUDPPort           = 0x2802,   //! Descriptor representing a UDP port supported by an IP service
        NetworkingReserved1         = 0x2803,   //! Reserved for future specification
        NetworkingReserved2         = 0x2BFF,
        ArType2OverlayOutput        = 0x2E00,   //! Descriptor that identifies an AR overlay output
        ArReserved1                 = 0x2D01,   //! Reserved for future specification
        ArReserved2                 = 0x2DFF,
        GssipFeatureSupport         = 0x3000,   //! Identifies messages of GSSIP supported or not supported by a GSSIP-compliant device
        StatusBattery               = 0x3400,   //! Descriptor that identifies a battery
        PowerReserved1              = 0x3401,   //! Reserved for future specification
        PowerReserved2              = 0x34FF,
        LrfCapabilities             = 0x3800
    };

    //! Service Descriptor Header -- Used for all ISW descriptors
    struct iswSolNetDescCommonHeader
    {
        uint16_t length;
        uint16_t descriptorId;
    }__attribute__((packed));

    //! Data Tuple Header -- Used for all ISW SolNet Data Payloads
    struct iswSolNetDataTupleCommonHeader
    {
        uint16_t length;
        uint16_t tupleId;
    }__attribute__((packed));

    static const uint16_t dataPayloadCommonHeaderSize = sizeof(IswSolNet::iswSolNetDataTupleCommonHeader);

    //! Service Descriptor -- Used for the root service descriptors
    struct iswSolNetServiceDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t serviceSelector;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetServiceDescSize = sizeof (IswSolNet::iswSolNetServiceDesc);

    //! The SolNet Control descriptor has a different
    //! size than the standard service descriptor so
    //! has to be handled separately throughout the code
    struct iswSolNetServiceControlDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t serviceSelector;
        uint16_t protocolId;
        uint16_t reserved;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetServiceControlDescSize = sizeof (IswSolNet::iswSolNetServiceControlDesc);

    static const uint16_t commonHeaderSize     = sizeof(IswSolNet::iswSolNetDescCommonHeader);
    static const uint16_t selectorSize         = sizeof(uint32_t);
    static const uint16_t commonDataHeaderSize = sizeof(IswSolNet::iswSolNetDataTupleCommonHeader);

    //! *********************************
    //! Property Descriptors - these are
    //! child descriptors and reside in
    //! the child blocks
    //! *********************************

    //! General Property Descriptors
    static const uint8_t SIZE_VERSION_TEXT = 16;
    static const uint8_t SIZE_TEXT_LABEL   = 16;
    static const uint8_t AXES              = 3;

    //! Version Property Descriptor
    struct iswSolNetPropertyVersionDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t solNetVersion;
        uint8_t reserved[3];
        uint8_t versionText[SIZE_VERSION_TEXT];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVersionDescSize = sizeof(IswSolNet::iswSolNetPropertyVersionDesc);

    //! Text Label Property Descriptor
    struct iswSolNetPropertyTextLabelDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t textLabel[SIZE_TEXT_LABEL];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyTextLabelDescSize = sizeof (IswSolNet::iswSolNetPropertyTextLabelDesc);

    //!Endpoint QoS Data Rate Descriptor
    struct iswSolNetEndpointQosDataRateDescDouble
    {
        iswSolNetDescCommonHeader header;
        uint64_t dataRate;
    }__attribute__((packed));

    //!Endpoint QoS Data Rate Descriptor
    struct iswSolNetEndpointQosDataRateDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t dataRate;
    }__attribute__((packed));

    uint16_t iswSolNetEndpointQosDataRateDescSize = sizeof(IswSolNet::iswSolNetEndpointQosDataRateDesc);
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constants to maintain the minimum size (in bytes) for QoS
    //! data rate descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                dataRate                      |          4 or 8
    //!
    //! Total: 4 bytes (float) or 8 bytes (double)
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!************************************************************************
    static const uint16_t QOS_DATA_RATE_DESC_CORE_BLOCK_FLOAT = 4;
    static const uint16_t QOS_DATA_RATE_DESC_CORE_BLOCK_DOUBLE = 8;

    struct iswSolNetEndpointQosPropertiesDesc
    {
        iswSolNetDescCommonHeader header;
    }__attribute__((packed));

    uint16_t iswSolNetEndpointQosPropertiesDescSize = sizeof(IswSolNet::iswSolNetEndpointQosPropertiesDesc);
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for QoS
    //! properties descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                                              |
    //!
    //! Total: 0 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!
    //! Since the childblock pointer is a mechanism implemented to embed
    //! other descriptors within an endpoint descriptor, and ISW SolNet
    //! compliant devices do not implement pointers in the endpoint descriptors
    //! sent to the ISW toolkit, the childBlock pointer size is not counted.
    //!************************************************************************
    static const uint16_t QOS_PROPERTIES_DESC_CORE_BLOCK_SIZE = 0;

    //! Endpoint Property Descriptor
    struct iswSolNetPropertyEndpointDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t endpointId;
        uint8_t dataflowId;
        uint8_t endpointDistribution;
        uint8_t dataPolicies;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyEndpointDescSize = sizeof (IswSolNet::iswSolNetPropertyEndpointDesc);

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for an endpoint
    //! descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!              endpointId                      |       1
    //!              dataflowId                      |       1
    //!              endpointDistribution            |       1
    //!              dataPolicies                    |       1
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!
    //! Since the childblock pointer is a mechanism implemented to embed
    //! other descriptors within an endpoint descriptor, and ISW SolNet
    //! compliant devices do not implement pointers in the endpoint descriptors
    //! sent to the ISW toolkit, the childBlock pointer size is not counted.
    //!************************************************************************
    static const uint16_t ENDPOINT_DESC_CORE_BLOCK_SIZE = 4;

    //! RTA Control Endpoint Property Descriptor
    struct iswSolNetPropertyRtaControlEndpointDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t endpointId;
        uint8_t dataflowId;
        uint8_t endpointDistribution;
        uint8_t dataPolicies;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyRtaControlEndpointDescSize = sizeof (IswSolNet::iswSolNetPropertyRtaControlEndpointDesc);

    //! RTA Data Endpoint Property Descriptor
    struct iswSolNetPropertyRtaDataEndpointDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t endpointId;
        uint8_t dataflowId;
        uint8_t endpointDistribution;
        uint8_t dataPolicies;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyRtaDataEndpointDescSize = sizeof (IswSolNet::iswSolNetPropertyRtaDataEndpointDesc);

    //! Multicast Group Property Descriptor
    struct iswSolNetPropertyMulticastGroupDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t mcastGroupId;
        uint8_t reserved1;
        uint16_t mcastAddress;
        uint8_t mcastMacAddress[6];
        uint16_t reserved2;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMulticastGroupDescSize = sizeof (IswSolNet::iswSolNetPropertyMulticastGroupDesc);

    //! Device Name Descriptor
    struct iswSolNetDeviceNameDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t name;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceNameDescSize = sizeof (IswSolNet::iswSolNetDeviceNameDesc);

    //! Device Serial Number Descriptor
    struct iswSolNetDeviceSerialNumberDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t serialNumber;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceSerialNumberDescSize = sizeof (IswSolNet::iswSolNetDeviceSerialNumberDesc);

    //! Device Manufacturer Descriptor
    struct iswSolNetDeviceManufacturerDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t manufacturer;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceManufacturerDescSize = sizeof (IswSolNet::iswSolNetDeviceManufacturerDesc);

    //! Device Friendly Name Descriptor
    struct iswSolNetDeviceFriendlyNameDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t friendlyName;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceFriendlyNameDescSize = sizeof(IswSolNet::iswSolNetDeviceFriendlyNameDesc);

    //! Encoding Property Descriptor
    struct iswSolNetPropertyEncodingDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t encoding;
        uint8_t reserved[3];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyEncodingDescSize = sizeof(IswSolNet::iswSolNetPropertyEncodingDesc);

    //! Flow Policy Property Descriptor
    struct iswSolNetPropertyFlowPolicyDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t autonomy;
        uint8_t reserved[3];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyFlowPolicyDescSize = sizeof(IswSolNet::iswSolNetPropertyFlowPolicyDesc);

    //! This struct can be used for properties:
    //! Device Descriptor
    //! Device Name Descriptor
    //! Device Serial Number Descriptor
    //! Device Manufacturer Descriptor
    //! Device Friendly Name Descriptor
    //! They are all the same

    //! Device Friendly Name
    struct iswSolNetPropertyDeviceDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t devString[256];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyDeviceDescSize = sizeof(IswSolNet::iswSolNetPropertyDeviceDesc);

    //! Associated Service Property Descriptor
    struct iswSolNetPropertyAssociatedServiceDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t associatedServiceId;
        uint8_t associationType;
        uint8_t reserved;
        uint32_t associatedServiceSelector;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyAssociatedServiceDescSize = sizeof(IswSolNet::iswSolNetPropertyAssociatedServiceDesc);

    //! ISW SolNet Association Type
    enum iswSolNetAssociationType: uint8_t
    {
        DEPENDENT_ON = 0x00,
        SIBLING_TO   = 0x01
    };

    //! ISW SolNet Property Endpoint Protocol Descriptor
    struct iswSolNetPropertyEndpointProtocolDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t endpointProtocol;
        uint16_t reserved1;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyEndpointProtocolDescSize = sizeof (IswSolNet::iswSolNetPropertyEndpointProtocolDesc);

    //! ISW SolNet Endpoint Protocol Ids
    enum iswSolNetEndpointProtocolIds: uint16_t
    {
        Tuple = 0x0001,
        GSSIP = 0x0002
    };

    //! ISW SolNet Device Descriptor
    struct iswSolNetDeviceDesc
    {
        iswSolNetDescCommonHeader header;
        float_t generationId;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceDescSize = sizeof (IswSolNet::iswSolNetDeviceDesc);

    struct iswSolNetQoSMaxLatencyDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t maxLatency;
        uint16_t reserved1;
    }__attribute__((packed));

    uint16_t iswSolNetQoSMaxLatencyDescSize = sizeof(IswSolNet::iswSolNetQoSMaxLatencyDesc);

    //! Device Software/Firmware Version Descriptor
    struct iswSolNetDeviceSoftwareFirmwareVersionDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t *childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetDeviceSoftwareFirmwareVersionDescSize = sizeof (IswSolNet::iswSolNetDeviceSoftwareFirmwareVersionDesc);

    struct iswSolNetSoftwareFirmwareNameDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t componentName[256];
    }__attribute__((packed));

    uint16_t iswSolNetSoftwareFirmwareNameDescSize = sizeof (IswSolNet::iswSolNetSoftwareFirmwareNameDescSize);

    struct iswSolNetSoftwareFirmwareVersionDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t componentVersion[256];
    }__attribute__((packed));

    uint16_t iswSolNetSoftwareFirmwareVersionDescSize = sizeof (IswSolNet::iswSolNetSoftwareFirmwareNameDesc);

    //! Video Property Descriptors
    struct iswSolNetPropertyVideoOutputFormatRawDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t resX;
        uint16_t resY;
        uint32_t frameRate;
        uint16_t bitDepth;
        uint16_t pixelformat;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoOutputFormatRawDescSize =  sizeof(IswSolNet::iswSolNetPropertyVideoOutputFormatRawDesc);

    //! Enumerated Raw Video Pixel Format
    enum iswSolNetRawVideoPixelFormat: uint16_t
    {
        PIXEL_FORMAT_Y               = 0x0001,
        PIXEL_FORMAT_NV21            = 0x0002,
        PIXEL_FORMAT_YUY2            = 0x0003,
        PIXEL_FORMAT_NV16            = 0x0004,
        PIXEL_FORMAT_YUV422          = 0x0005,
        PIXEL_FORMAT_YUV420          = 0x0006,
        PIXEL_FORMAT_RGBA_4444       = 0x0020,
        PIXEL_FORMAT_RGBA_8888       = 0x0021,
        PIXEL_FORMAT_RGBA_5551       = 0x0022,
        PIXEL_FORMAT_RGBX_8888       = 0x0023,
        PIXEL_FORMAT_RGB_332         = 0x0030,
        PIXEL_FORMAT_RGB_565         = 0x0031,
        PIXEL_FORMAT_RGB_888         = 0x0032
    };

    struct iswSolNetPropertyVideoSampleFormatDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t bitDepth;
        uint16_t pixelFormat;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoSampleFormatDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoSampleFormatDesc);

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for video
    //! sample format descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!              bitDepth                        |       2
    //!              pixelFormat                     |       2
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!************************************************************************
    static const uint16_t VIDEO_SAMPLE_FORMAT_DESC_CORE_BLOCK_SIZE = 4;

    struct iswSolNetPropertyMaximumFrameSizeDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t frameSize = 0x258; // Max is set to 600 kB, typical frames shall be less than 330 Kb (Per FWS-CS ICD)
    }__attribute__((packed));

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for video
    //! sample format descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!              frameSize                       |       4
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!************************************************************************
    static const uint16_t MAXIMUM_FRAME_SIZE_DESC_CORE_BLOCK_SIZE = 4;

    struct iswSolNetPropertyVideoFormatDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t videoFormatX;
        uint16_t videoFormatY;
        float frameRate;
        uint8_t formatOptions;
        uint8_t reserved;
        uint16_t encodingFormat;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoFormatDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoFormatDesc);

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for video
    //! format property descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!              videoFormatX                    |       2
    //!              videoFormatY                    |       2
    //!              frameRate                       |       4
    //!              formatOptions                   |       1
    //!              reserved                        |       1
    //!              encodingFormat                  |       2
    //!
    //! Total: 12 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!
    //! Since the childblock pointer is a mechanism implemented to embed
    //! other descriptors within an endpoint descriptor, and ISW SolNet
    //! compliant devices do not implement pointers in the endpoint descriptors
    //! sent to the ISW toolkit, the childBlock pointer size is not counted.
    //!************************************************************************
    static const uint16_t VIDEO_FORMAT_PROPERTY_DESC_CORE_BLOCK_SIZE = 12;

    enum iswSolNetVideoPixelFormat: uint16_t
    {
        YOnly    = 0x0001,
        NV21     = 0x0002,
        YUY2     = 0x0003,
        NV16     = 0x0004,
        YUV422   = 0x0005,
        YUV420   = 0x0006,
        RGBA4444 = 0x0020,
        RGBA8888 = 0x0021,
        RGBA5551 = 0x0022,
        RGBX8888 = 0x0023,
        RGB332   = 0x0030,
        RGB555   = 0x0031,
        RGB888   = 0x0032,
    };

    struct iswSolNetPropertyRleFormatDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t reserved1;
        uint16_t rleVersion;
    }__attribute__((packed));

    enum iswSolNetRleVersionNumbers: uint16_t
    {
        RLEVersion1 = 0x0001,
        RLEVersion5 = 0x0010
    };

    struct iswSolNetPropertyVideoInputFormatDesc
    {
        iswSolNetDescCommonHeader header;
        float_t minimumRate;
        float_t maximumRate;
        uint16_t supportedResolutions;
        uint16_t supportedFormats;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoInputFormatDescSize = sizeof (IswSolNet::iswSolNetPropertyVideoInputFormatDesc);

    enum iswSolNetSupportedVideoInputResolution: uint16_t
    {
        RES_640x480 = 0x0001
    };

    enum iswSolNetSupportedVideoInputsFormat: uint16_t
    {
        FormatY = 0x0001,
        RGB_332 = 0x0002,
        RGB_888 = 0x0008
    };

    //! Enumerated Video Encoding Format
    enum iswSolNetEncodingFormat: uint16_t
    {
        RAW           = 0x0000,
        BASELINE_JPEG = 0x0001,
        RLE           = 0x0002
    };

    struct iswSolNetPropertyVideoProtocolDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t protocol;
        uint16_t reserved;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoProtocolDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoProtocolDesc);

    struct iswSolNetPropertyVideoIfovDesc
    {
        iswSolNetDescCommonHeader header;
        float_t IFoV;
    }__attribute__((packed));;

    uint16_t iswSolNetPropertyVideoIfovDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoIfovDesc);

    //! Enumerated Video Protocol Types
    enum iswSolNetRawVideoProtocolTypes: uint16_t
    {
        FVTS = 0x0001
    };

    //! Video Principal Point Property Descriptor
    struct iswSolNetPropertyVideoPrincipalPointDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t principalPointX;
        uint16_t principalPointY;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoPrincipalPointDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoPrincipalPointDesc);

    //! Video Lens Distortion Property Descriptor
    struct iswSolNetPropertyVideoLensDistortionDesc
    {
        iswSolNetDescCommonHeader header;
        float_t distortionCoefficientK1;
        float_t distortionCoefficientK2;
        float_t distortionCoefficientP1;
        float_t distortionCoefficientP2;
        float_t distortionCoefficientK3;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoLensDistortionDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoLensDistortionDesc);

    //! Video Control Property Descriptor
    struct iswSolNetPropertyVideoControlDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t controlBitmap;
        uint16_t reserved;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoControlDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoControlDesc);

    //! Video Image Sensor Type Property Descriptor
    struct iswSolNetPropertyVideoImageSensorTypeDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t type;
        uint16_t reserved1;
        uint8_t typeDescription;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoImageSensorTypeDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorTypeDesc);

    //! Enumerated Video Image Sensor Type
    enum iswSolNetVideoImageSensorTypes: uint16_t
    {
        IMAGE_SENSOR_TYPE_UNKNOWN  = 0x0000,
        IMAGE_SENSOR_TYPE_OTHER    = 0x0001,
        IMAGE_SENSOR_TYPE_COMPUTER = 0x0002,
        IMAGE_SENSOR_TYPE_CCD      = 0x0003,
        IMAGE_SENSOR_TYPE_CMOS     = 0x0004,
        IMAGE_SENSOR_TYPE_I2CMOS   = 0x0005,
        IMAGE_SENSOR_TYPE_EBAPS    = 0x0006,
        IMAGE_SENSOR_TYPE_INGAAS   = 0x0007,
        IMAGE_SENSOR_TYPE_INSB     = 0x0008,
        IMAGE_SENSOR_TYPE_VOX      = 0x0009,
        IMAGE_SENSOR_TYPE_ALPD     = 0x000A,
        IMAGE_SENSOR_TYPE_TOF      = 0x000B,
        IMAGE_SENSOR_TYPE_SLM      = 0x000C
    };

    //! Video Image Sensor Waveband Property Descriptor
    struct iswSolNetPropertyVideoImageSensorWavebandDesc
    {
       iswSolNetDescCommonHeader header;
       uint16_t waveband;
       uint16_t reserved;
//       uint8_t* description;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyVideoImageSensorWavebandDescSize = sizeof(IswSolNet::iswSolNetPropertyVideoImageSensorWavebandDesc);
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the minimum size (in bytes) for QoS
    //! properties descriptor that could be received by the ISW toolkit.
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                waveband                      |          2
    //!                reserved                      |          2
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the endpoint descriptor.
    //!
    //! The description pointer is a variable-length, null-terminated,
    //! optional, human readable string of the sensor type. ISW SolNet
    //! compliant devices do not implement pointers in the endpoint descriptors
    //! sent to the ISW toolkit, and simply send a buffer of UTF-8 encoded
    //! characters representing the string. Thus, the description size is not
    //! counted.
    //!************************************************************************
    static const uint16_t IMAGE_SENSOR_WAVEBAND_DESC_CORE_BLOCK_SIZE = 4;

    //! Enumerated Video Image Sensor Waveband
    enum iswSolNetVideoImageSensorWaveband: uint16_t
    {
        IMAGE_SENSOR_WAVEBAND_OTHER    = 0x0001,
        IMAGE_SENSOR_WAVEBAND_XRAY     = 0x0002,
        IMAGE_SENSOR_WAVEBAND_UV       = 0x0003,
        IMAGE_SENSOR_WAVEBAND_VIS      = 0x0004,
        IMAGE_SENSOR_WAVEBAND_NIR      = 0x0005,
        IMAGE_SENSOR_WAVEBAND_SWIR     = 0x0006,
        IMAGE_SENSOR_WAVEBAND_MWIR     = 0x0007,
        IMAGE_SENSOR_WAVEBAND_LWIR     = 0x0008,
        IMAGE_SENSOR_WAVEBAND_COMPUTER = 0x0100
    };

    struct iswSolNetPropertyVideoIntrinsicParametersDesc
    {
       iswSolNetDescCommonHeader header;
       float_t focalLengthX;
       float_t focalLengthY;
       float_t principalPointX;
       float_t principalPointY;
       float_t skew;
    }__attribute__((packed));

    //! Association Property Descriptor
    struct iswSolNetPropertyAssociationDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t associatedServiceId;
        uint8_t associationType;
        uint8_t reserved1;
        uint32_t associatedServiceSelector;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyAssociationDescSize = sizeof(IswSolNet::iswSolNetPropertyAssociationDesc);

    //! Motion Sensor Accelerometer Property Descriptor
    struct iswSolNetMotionSensorAccelerometerDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t numberOfAxes;
        uint8_t reserved1[3];
    }__attribute__((packed));

    uint16_t iswSolNetMotionSensorAccelerometerDescSize = sizeof(IswSolNet::iswSolNetMotionSensorAccelerometerDesc);

    //! Audio Property Descriptors
    struct iswSolNetPropertyAudioDesc
    {
        iswSolNetDescCommonHeader header;
        //Not defined yet
    }__attribute__((packed));

    //! Position Property Descriptors
    struct iswSolNetPropertyPositionDesc
    {
        iswSolNetDescCommonHeader header;
        //Not defined yet
    }__attribute__((packed));

    struct iswSolNetPropertyPositionDeviceClassDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t Class;
        uint16_t reserved1;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyPositionDeviceClassDescSize = sizeof(IswSolNet::iswSolNetPropertyPositionDeviceClassDesc);

    enum iswSolNetPositionDeviceClass: uint16_t
    {
        Unknown_Class = 0x0000, //! Unknown Position
        GSSIP_RCVR    = 0x0001, //! GSSIP Receiver
        GSSIP_INTER   = 0x0002  //! GSSIP Intelligent Terminal
    };

    struct iswSolNetPropertyGssipFeatureSupportDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t gssipRevision;
        uint8_t filter;
        uint8_t count;
        uint8_t reserved1;
        uint16_t encodingFormat;
        uint16_t messageId[3];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyGssipFeatureSupportDescSize = sizeof(IswSolNet::iswSolNetPropertyGssipFeatureSupportDesc);

    enum iswSolNetGSSIPRevision: uint8_t
    {
        RevA = 0x00, //! Rev A
        RevB = 0x01, //! Rev B
        RevC = 0x02, //! Rev C
        RevD = 0x03  //! Rev D
    };

    enum iswSolNetGSSIPFilter: uint8_t
    {
        Include = 0x00, //! Include
        Exclude = 0x01  //! Exclude
    };

    //! Environment Property Descriptors
    struct iswSolNetPropertyEnvDesc
    {
        iswSolNetDescCommonHeader header;
        // Not defined yet
    }__attribute__((packed));

    //! Health Property Descriptors
    struct iswSolNetPropertyHealthDesc
    {
        iswSolNetDescCommonHeader header;
        // Not defined yet
    }__attribute__((packed));

    //! SimpleUI Property Descriptors
    struct iswSolNetPropertySimpleUiMotionDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t terminalId;
        uint8_t type;
        uint16_t flags;
        uint16_t xMin;
        uint16_t xMax;
        uint16_t yMin;
        uint16_t yMax;
        uint16_t zMin;
        uint16_t zMax;
    }__attribute__((packed));

    uint16_t iswSolNetPropertySimpleUiMotionDescSize = sizeof(IswSolNet::iswSolNetPropertySimpleUiMotionDesc);

    //! SimpleUI Standard Format
    struct iswSolNetPropertySimpleUiStandardFormat
    {
        uint8_t terminalId;
        uint8_t mode;
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
    }__attribute__((packed));

    uint16_t iswSolNetPropertySimpleUiStandardFormatSize = sizeof(IswSolNet::iswSolNetPropertySimpleUiStandardFormat);

    //! Enumerated SimpleUI Standard Format Mode
    enum iswSolNetSimpleUiStandardFormatMode: uint8_t
    {
        NEW_VALUE     = 0x00,
        CHANGE_VALUE  = 0x01,
        NEW_SPEED     = 0x02
    };

    //! SimpleUI Button Input Property Descriptor
    struct iswSolNetPropertySimpleUiButtonInputDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t terminalId;
        uint8_t capabilities;
        uint8_t buttonCount;
        uint8_t stateCount;
    }__attribute__((packed));

    uint16_t iswSolNetPropertySimpleUiButtonInputDescSize = sizeof(IswSolNet::iswSolNetPropertySimpleUiButtonInputDesc);

    //! Enumerated SimpleUI Button States
    enum iswSolNetSimpleUiButtonStates: uint8_t
    {
        TERMINAL_SUPPORTS_STANDARD_FORMAT           = 0x01,
        TERMINAL_SUPPORTS_PRESS_EVENT               = 0x02,
        TERMINAL_SUPPORTS_RELEASE_EVENT             = 0x04,
        TERMINAL_SUPPORTS_HOLD_EVENT                = 0x08,
        TERMINAL_SUPPORTS_DOUBLE_PRESS              = 0x10,
        TERMINAL_SUPPORTS_STATE_TRANSITION_EVENTS   = 0x20,
        TERMINAL_SUPPORTS_BITMAP_EVENT              = 0x80
    };

    enum iswSolNetSimpleUiButtonEvents: uint8_t
    {
        PRESS_EVENT        = 0x00,
        RELEASE_EVENT      = 0x01,
        HOLD_EVENT         = 0x02,
        DOUBLE_PRESS_EVENT = 0x03,
        SHORT_TAP_EVENT    = 0x04,
        UPDATE_TYPE        = 0x05,
        BITMAP_EVENT       = 0x10
    };

    struct iswSolNetPropertySimpleUiShortBitMapEventUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventId;
        uint8_t buttonData;
    }__attribute__((packed));

    uint16_t iswSolNetPropertySimpleUiShortBitMapEventUpdateSize = sizeof(IswSolNet::iswSolNetPropertySimpleUiShortBitMapEventUpdate);

    struct iswSolNetPropertySimpleUiLongBitMapEventUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventId;
        uint8_t blockCount;
        uint32_t bitmapBlock;
    }__attribute__((packed));

    struct iswSolNetPropertySimpleUiShortButtonEventUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventId;
        uint8_t buttonData;
    }__attribute__((packed));

    struct iswSolNetPropertySimpleUiLongButtonEventUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventId;
        uint8_t blockCount;
        uint32_t bitmapBlock;
    }__attribute__((packed));

    struct iswSolNetPropertySimpleUiExtendedEventMapUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventCount;
        uint8_t reserved1;
        uint8_t eventId1;
        uint8_t eventId2;
        uint8_t eventId3;
    };

    struct iswSolNetPropertySimpleUiShortTimestampedBitMapEventUpdate
    {
        uint8_t updateType;
        uint8_t terminalId;
        uint8_t eventId;
        uint8_t buttonData;
        uint32_t timestamp;
    }__attribute__((packed));

    //! Control Property Descriptor
    struct iswSolNetPropertyControlDesc
    {
        iswSolNetDescCommonHeader header;
        // Not defined yet
    }__attribute__((packed));

    //! RTA Property Descriptors
    struct iswSolNetPropertyImuOutputRateDesc
    {
        iswSolNetDescCommonHeader header;
        float_t rate;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyImuOutputRateDescSize = sizeof(IswSolNet::iswSolNetPropertyImuOutputRateDesc);

    struct iswSolNetPropertyImuInputRateDesc
    {
        iswSolNetDescCommonHeader header;
        float rateMinimum;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyImuInputRateDescSize = sizeof(IswSolNet::iswSolNetPropertyImuInputRateDesc);

    struct iswSolNetPropertyMotionSensorRateDesc
    {
        iswSolNetDescCommonHeader header;
        float_t rate;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorRateDescSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorRateDesc);

    struct iswSolNetPropertyMotionSensorAccelerometerDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t* childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorAccelerometerDescSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAccelerometerDesc);

    struct iswSolNetPropertyMotionSensorMagnetometerDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t* childBlock;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorMagnetometerDescSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorMagnetometerDesc);

    struct iswSolNetPropertImuSensorNoiseDesc
    {
        iswSolNetDescCommonHeader header;
        uint64_t accelerometerXNoise;
        uint64_t accelerometerYNoise;
        uint64_t accelerometerZNoise;
        uint64_t gyroscopeXNoise;
        uint64_t gyroscopeYNoise;
        uint64_t gyroscopeZNoise;
        uint64_t magnetometerXNoise;
        uint64_t magnetometerYNoise;
        uint64_t magnetometerZNoise;
    }__attribute__((packed));

    uint16_t iswSolNetPropertImuSensorNoiseDescSize = sizeof(IswSolNet::iswSolNetPropertImuSensorNoiseDesc);

    //! Networking Property Descriptors
    struct iswSolNetPropertyIpServiceDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t ipV4Address;
    }__attribute__((packed));

    //! TCP Port Property Descriptor
    struct iswSolNetPropertyTcpPortDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t tcpPort;
        uint8_t direction;
        uint8_t reserved;
    }__attribute__((packed));

    //! Enumerated IP Data Flow Direction
    enum iswSolNetIpDataFlowDirection: uint8_t
    {
        CONSUME         = 0x01,
        PROVIDE         = 0x02,
        BIDIRECTIONAL   = 0x03
    };

    //! UDP Port Property Descriptor
    struct iswSolNetPropertyUdpPortDescriptor
    {
        iswSolNetDescCommonHeader header;
        uint16_t udpPort;
        uint8_t direction;
        uint8_t reserved;
        uint32_t mcastIpAddress;
    }__attribute__((packed));

    //! Range Property Descriptor
    struct iswSolNetPropertyRangeDesc
    {
        iswSolNetDescCommonHeader header;
        // Not defined yet
    }__attribute__((packed));

    //! AR Type 1 Property Descriptor
    struct iswSolNetPropertyArType1Desc
    {
        iswSolNetDescCommonHeader header;
        // Not defined yet
    }__attribute__((packed));

    //! AR Property -- Overlay Output Descriptor
    struct iswSolNetPropertyArType2OutputDesc
    {
        iswSolNetDescCommonHeader header;
        uint16_t minWidth;
        uint16_t minHeight;
        uint16_t supportedRleVersions;
        uint8_t reserved[3];
    }__attribute__((packed));

    uint16_t iswSolNetPropertyArType2OutputDescSize = sizeof(IswSolNet::iswSolNetPropertyArType2OutputDesc);

    enum iswSolNetSupportedRleVersions: uint8_t
    {
        RLE_V05 = 0x01
    };

    //! Status Battery Description Descriptor
    //! description field is a NULL terminated
    //! string that is padded with zeros so that
    //! it's length is a multiple of 4 bytes
    //! Made it a pointer here because you have
    //! to check the length and allocate a buffer
    //! big enough to hold the string
    struct iswSolNetPropertyStatusBatteryDesc
    {
        iswSolNetDescCommonHeader header;
        uint8_t batteryId;
        uint8_t reserved[3];
        uint8_t *description;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyStatusBatteryDescSize = sizeof (IswSolNet::iswSolNetPropertyStatusBatteryDesc);

    //! Motion Sensor Axes Property Descriptor
    struct iswSolNetPropertyMotionSensorAxes
    {
        iswSolNetDescCommonHeader header;
        uint8_t numberOfAxes;
        uint8_t reserved[3];
    }__attribute__((packed));

    //! These next three descriptors contain
    //! iswSolNetPropertyMotionSensorAxes as
    //! sub descriptors

    //! Motion Sensor Accelerometer Property Descriptor
    struct iswSolNetPropertyMotionSensorAccel
    {
        iswSolNetDescCommonHeader header;
        //uint8_t *childBlock;
        uint8_t acceleromometerAxesNum;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorAccelSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorAccel);

    //! Motion Sensor Gyro Property Descriptor
    struct iswSolNetPropertyMotionSensorGyro
    {
        iswSolNetDescCommonHeader header;
        //uint8_t *childBlock;
        uint8_t gyroAxesNum;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorGyroSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorGyro);

    //! Motion Sensor Magnetometer Property Descriptor
    struct iswSolNetPropertyMotionSensorMag
    {
        iswSolNetDescCommonHeader header;
        //uint8_t *childBlock;
        uint8_t magAxesNum;
    }__attribute__((packed));

    uint16_t iswSolNetPropertyMotionSensorMagSize = sizeof(IswSolNet::iswSolNetPropertyMotionSensorMag);

    //! The following are used in data payloads
    //! Some of the Ids conflict with message Ids
    //! But it's ok - they are only in data payloads
    enum iswSolNetVideoDataDescId: uint16_t
    {
        VIDEO_FRAME_RAW_METADATA     = 0x0100,
        VIDEO_RTA_METADATA_ID        = 0x0101, // Deprecated
        VIDEO_DATA                   = 0x0102,
        VIDEO_FRAME_JPEG_METADATA    = 0x0103,
        VIDEO_PROPERTIES             = 0x0104,
        VIDEO_POLARITY               = 0x0105,
        VIDEO_ZOOM                   = 0x0106,
        VIDEO_RESOLUTION             = 0x0107,
        VIDEO_WAVEBAND               = 0x0108,
        VIDEO_CALIBRATION            = 0x0109,
        VIDEO_AGC                    = 0x010A,
        VIDEO_BAE_VENDOR_ID          = 0xFE00,
        VIDEO_DRS_VENDOR_ID          = 0xFF00
    };

    //! Enumerated RTA Data Tuple IDs
    enum iswSolNetRtaDataDescId: uint16_t
    {
        RTA_STATUS        = 0x0200,
        RTA_RETICLE       = 0x0201,
        RTA_IMU_DATA      = 0x0202,
        RTA_BAE_VENDOR_ID = 0xFE01,
        RTA_DRS_VENDOR_ID = 0xFF01
    };

    //! Enumerated Motion Sense Data Tuple IDs
    enum iswSolNetMotionSenseDataTupleId: uint16_t
    {
        MOTION_SENSE_SAMPLE = 0x0001,
        MOTION_SENSE_ACCEL  = 0x1000,
        MOTION_SENSE_MAG    = 0x1001,
        MOTION_SENSE_GYRO   = 0x1002
    };

    //! Enumerated Status Data Tuple IDs
    enum iswSolNetStatusDataTupleId: uint16_t
    {
        STATUS_BATTERY_ID    = 0x0001,
        STATUS_DEVICE_ID     = 0x0002,
        STATUS_WEAPON_ID     = 0x0003,
        STATUS_BAE_VENDOR_ID = 0xFE02,
        STATUS_DRS_VENDOR_ID = 0xFF02
    };

    //! Video Frame Metadata Descriptor
    struct iswSolNetVideoFrameRawMetadataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint16_t frameNumber;
        uint16_t packetCount;
        uint16_t frameRowNumber;
        uint16_t frameColumnNumber;
    }__attribute__((packed));

    //! Video Properties Tuple
    struct iswSolNetVideoPropertiesDataTuple
    {
       iswSolNetDataTupleCommonHeader header;
       uint8_t videoPropertiesTuples;
    }__attribute__((packed));

    //! Video Waveband Tuple
    struct iswSolNetVideoWavebandTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t videoPolarity;
        uint8_t reserved[3];
    }__attribute__((packed));
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Waveband Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 videoPolarity                |          1
    //!                 reserved                     |          3
    //!
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_WAVEBAND_TUPLE_LENGTH = 4;


    //! Video Resolution Tuple
    struct iswSolNetVideoResolutionTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint16_t videoFormatX;
        uint16_t videoFormatY;
    }__attribute__((packed));
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Resolution Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 videoFormatX                 |          1
    //!                 videoFormatY                 |          1
    //!                 reserved                     |          3
    //!
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_RESOLUTION_TUPLE_LENGTH = 5;

    //! Video Polarity Tuple
    struct iswSolNetVideoPolarityTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t videoPolarity;
        uint8_t reserved[3];
    }__attribute__((packed));
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Polarity Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 videoPolarity                |          1
    //!                 reserved                     |          3
    //!
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_POLARITY_TUPLE_LENGTH = 4;


    //! Video Image Calibration Tuple
    struct iswSolNetVideoImageCalibrationDataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint16_t imageCalibration;
        uint16_t reserved;
    }__attribute__((packed));

    //! Video Data Tuple
    struct iswSolNetVideoDataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t data;
    }__attribute__((packed));

    //! Video Automatic Gain Control (AGC) Tuple
    struct iswSolNetVideoAGCTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t videoFormatX;
        uint8_t reserved[3];
    };
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Automatic Gain Control (AGC) Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 videoFormatX                 |          1
    //!                 reserved                     |          3
    //!
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_AGC_TUPLE_LENGTH = 4;

    //! Video Frame JPEG Metadata Tuple
    struct iswSolNetVideoFrameJpegMetadataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint16_t frameNumber;
        uint16_t packetCount;
        uint8_t jpegProperties;
        uint8_t reserved[3];
    }__attribute__((packed));

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Frame JPEG Metadata Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 frameNumber                  |          2
    //!                 packetCount                  |          2
    //!                 jpegProperties               |          1
    //!                 reserved                     |          3
    //!
    //!
    //! Total: 8 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_FRAME_JPEG_METADATA_TUPLE_LENGTH = 8;

    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant mask used to verify the received Video Frame JPEG
    //! Metadata Tuple JPEG properties field.
    //!
    //!     Bits |      Field Name                |     Value
    //! _________|________________________________|__________________________
    //!     3-7  |      Reserved                  |     0b00000
    //!     1-2  |      JPEG Abbreviated Format   |     VAR
    //!     0    |      Last Packet in Frame      |     VAR
    //!
    //!
    //! A valid JPEG properties field would therefore have the value 0b00000XXX
    //! so if a bitwise AND operation using the mask 0b11111000 (hex 0xF8)
    //! yielded a nonzero result, the JPEG properties field would necessarily
    //! be invalid.
    //!************************************************************************
    static const uint8_t VIDEO_FRAME_JPEG_METADATA_TUPLE_JPEG_PROPERTIES_MASK = 0xF8;

    //! Video Zoom Tuple
    struct iswSolNetVideoZoomTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t zoomMagnification;
    }__attribute__((packed));
    //!************************************************************************
    //! Ref: A3309637
    //!
    //! Global constant to maintain the exact size (in bytes) for SolNet Video
    //! Zoom Tuple
    //!
    //!              Field Name                      |   Size (bytes)
    //! _____________________________________________|__________________________
    //!                 zoomMagnification            |          4
    //!
    //!
    //! Total: 4 bytes
    //!
    //! The ISW SolNet specification explicitly excludes the common header
    //! from the total size of the tuple.
    //!************************************************************************
    static const uint16_t VIDEO_ZOOM_TUPLE_LENGTH = 4;

    //! Video RTA Metadata
    struct iswSolNetVideoRtaMetadataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint16_t videoPolarity;
        uint16_t imageCalibration;
    }__attribute__((packed));

    //! Enumerated ISW SolNet Video Polarity
    enum iswSolNetVideoPolarity: uint16_t
    {
        WHITE_HOT   = 0x0000,
        BLACK_HOT   = 0x0001,
        OUTLINE     = 0x0002,
    };

    //! Enumerated ISW SolNet Image Calibration
    enum iswSolNetImageCalibration: uint16_t
    {
        NO_IMAGE_CALIBRATION                  = 0x0000,
        IMAGE_CALIBRATION_PROCESS_OCCURRING   = 0x0001,
        IMAGE_CALIBRATION_NEEDED_FROM_FWS_I   = 0x0002
    };

    //! RTA Status Data Tuple
    struct iswSolNetRtaStatusDataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t rtaEnabled;
        uint8_t rtaMode;
        uint8_t rtaPolarity;
        uint8_t rtaZoom;
        uint8_t rtaBubble;
        uint8_t rtaManualAlignment;
        uint8_t reticleInVideo;
        uint8_t reserved;
    }__attribute__((packed));

    //! Enumerate RTA Enabled
    enum iswSolNetRtaEnabled: uint8_t
    {
        RTA_OPERATION_DISABLED = 0x00,
        RTA_OPERATION_ENABLED  = 0x01
    };

    //! Enumerate RTA Mode
    enum iswSolNetRtaMode: uint8_t
    {
        SPATIALLY_ALIGNED       = 0x00,
        GOGGLE_VIDE0_FWS_I_PIP  = 0x01,
        FWS_I_VIDEO_GOGGLE_PIP  = 0x02,
        FULL_SCREEN_FWS_I_VIDEO = 0x03,
        FULL_SCREEN_SYMBOLOGY   = 0x04
    };

    //! Enumerated RTA Polarity
    enum iswSolNetRtaPolarity: uint8_t
    {
        RTA_POLARITY_WHITE_HOT   = 0x00,
        RTA_POLARITY_BLACK_HOT   = 0x01
    };

    //! Enumerated RTA Zoom
    enum iswSolNetRtaZoom: uint8_t
    {
        RTA_BUBBLE_ZOOM_DISABLED = 0x00,
        RTA_BUBBLE_ZOOM_ENABLED  = 0x01
    };

    //! Enumerated RTA Bubble On/Off
    enum iswSolNetRtaBubble: uint8_t
    {
        RTA_BUBBLE_DISABLED = 0x00,
        RTA_BUBBLE_ENABLED  = 0x01
    };

    //! Enumerated RTA Manual Alignment
    enum iswSolNetRtaManualAlignment: uint8_t
    {
        MANUAL_ALIGNMENT_NOT_REQUESTED     = 0x00,
        MANUAL_ALIGNMENT_REQUESTED_BY_USER = 0x01
    };

    //! Enumerated RTA Reticle In Video
    enum iswSolNetRtaReticleInVideo: uint8_t
    {
        NO_RETICLE_FWS_I_VIDEO          = 0x00,
        RETICLE_INCLUDED_FWS_I_VIDEO    = 0x01
    };

    //! RTA Reticle Tuple
    struct iswSolNetRtaReticleDataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t rtaReticleType;
        uint8_t rtaReticleColorRed;
        uint8_t rtaReticleColorGreen;
        uint8_t rtaReticleColorBlue;
        int16_t rtaReticleXOffset;
        int16_t rtaReticleYOffset;
    }__attribute__((packed));

    //! Enumerated RTA Reticle Type
    enum iswSolNetRtaReticleType: uint8_t
    {
        CROSS_HAIR = 0x00,
        CIRCULAR   = 0x01
    };

    //! Enumerated RTA Reticle Color
    enum iswSolNetRtaReticleColor: uint8_t
    {
        RTA_RETICLE_COLOR_RED    = 0xFF,
        RTA_RETICLE_COLOR_GREEN  = 0x00,
        RTA_RETICLE_COLOR_BLUE   = 0x00
    };

    //! Enumerated RTA Reticle X Offset
    enum iswSolNetRtaReticleXOffset: uint16_t
    {
        X_OFFSET_NEGATIVE_1_1_3 = 0xFFFC,
        X_OFFSET_NEGATIVE_1     = 0xFFFD,
        X_OFFSET_NEGATIVE_2_3   = 0xFFFE,
        X_OFFSET_NEGATIVE_1_3   = 0xFFFF,
        X_OFFSET_ZERO           = 0x0000,
        X_OFFSET_POSITIVE_1_3   = 0x0001,
        X_OFFSET_POSITIVE_2_3   = 0x0002,
        X_OFFSET_POSITIVE_1     = 0x0003
    };

    //! Enumerated RTA Reticle Y Offset
    enum iswSolNetRtaReticleYOffset: uint16_t
    {
        Y_OFFSET_NEGATIVE_1_1_3 = 0xFFFC,
        Y_OFFSET_NEGATIVE_1     = 0xFFFD,
        Y_OFFSET_NEGATIVE_2_3   = 0xFFFE,
        Y_OFFSET_NEGATIVE_1_3   = 0xFFFF,
        Y_OFFSET_ZERO           = 0x0000,
        Y_OFFSET_POSITIVE_1_3   = 0x0001,
        Y_OFFSET_POSITIVE_2_3   = 0x0002,
        Y_OFFSET_POSITIVE_1     = 0x0003
    };

    //! RTA IMU Data Tuple
    struct iswSolNetRtaImuDataTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float quaternionReal;
        float quaternionI;
        float quaternionJ;
        float quaternionK;
    }__attribute__((packed));

    //! Motion Sense Data Tuple
    //! Sample Data Tuple
    struct iswSolNetSampleDataTuple
    {
       iswSolNetDataTupleCommonHeader header;
       uint32_t sampleTimestamp;
       uint8_t sensorTypeDataTuples;
    }__attribute__((packed));

    //! Accelerometer Data Tuple
    struct iswSolNetAccelerometerDataTuple
    {
       iswSolNetDataTupleCommonHeader header;
       float_t accelerationData[2];
    }__attribute__((packed));

    //! Gyro Data Tuple
    struct iswSolNetGyroDataTuple
    {
       iswSolNetDataTupleCommonHeader header;
       float_t gyroData[2];
    }__attribute__((packed));

    //! Magnetometer Data Tuple
    struct iswSolNetMagnetometerDataTuple
    {
       iswSolNetDataTupleCommonHeader header;
       float_t magnetometerData[2];
    }__attribute__((packed));

    //! Battery Status Data Tuple
    struct iswSolNetBatteryStatusTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t batteryLevel;
        uint8_t batteryState;
        uint8_t batteryCharging;
        uint8_t batteryId;
    }__attribute__((packed));

    //! Enumerated Battery State
    enum iswSolNetBatteryState: uint8_t
    {
        BATTERY_OK       = 0x00,
        BATTERY_LOW      = 0x01,
        BATTERY_CRITICAL = 0x02,
        BATTERY_CHARGING = 0x03
    };

    //! Enumerated Battery Charging
    enum iswSolNetBatteryCharging: uint8_t
    {
        BATTERY_NOT_CHARGING            = 0x00,
        BATTERY_IS_CHARGING             = 0x01,
        BATTERY_DOESNT_SUPPORT_CHARGING = 0x02
    };

    //! Device Status Data Tuple
    struct iswSolNetDeviceStatusTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t deviceState;
        uint8_t displayState;
        uint16_t reserved;
    }__attribute__((packed));

    //! Enumerated Device State
    enum iswSolNetDeviceState: uint8_t
    {
        DEVICE_TURNING_OFF = 0x00,
        DEVICE_ON          = 0x01
    };

    //! Enumerated Display State
    enum iswSolNetDisplayState: uint8_t
    {
        DISPLAY_OFF = 0x00,
        DISPLAY_ON  = 0x01
    };

    //! Weapon Status Data Tuple
    struct iswSolNetWeaponStatusTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t weaponSightMode;
        uint8_t activeReticleId;
        uint8_t symbologyState;
        uint8_t reserved;
    }__attribute__((packed));

    //! Enumerated Weapon Sight Mode
    enum iswSolNetWeaponSightMode: uint8_t
    {
        STANDALONE      = 0x00,
        CCO             = 0x01,
        RCO             = 0x02,
        FULL_STANDALONE = 0x03
    };

    //! Enumerated Active Reticle ID
    enum iswSolNetActiveReticleId: uint8_t
    {
        NONE          = 0x00,
        M4_M16        = 0x01,
        M249_S        = 0x02,
        M249_L        = 0x03,
        MIL_10        = 0x04,
        M136          = 0x05,
        M141          = 0x06,
        MIL_SCALE     = 0x07,
        STINGER       = 0x08,
        RTA_BORESIGHT = 0x10
    };

    //! Weapon Status Descriptor
    struct iswSolNetLrfControlData
    {
        iswSolNetDescCommonHeader header;
        uint8_t weaponSightMode;
        uint8_t activeReticleId;
        uint8_t symbologyState;
        uint16_t reserved;
    }__attribute__((packed));

    //! Enumerated Symbology State
    enum iswSolNetSymbologyState: uint8_t
    {
        SYMBOLOGY_OFF = 0x00,
        SYMBOLOGY_ON  = 0x01
    };

    // LRF, LRF Control Property Descriptors

    //! Enumerated LRF Service Property Descriptor IDs
    enum iswSolNetLrfServiceDescIds: uint16_t
    {
        LRF_CAPABILITIES_DESC  = 0x3800
    };

    //! Enumerated Status Service Property Descriptor Ids
    enum iswSolNetStatusPropertyDescIds: uint16_t
    {
        STATUS_BATTERY_DESC = 0x3400
    };

    //! Enumerated LRF Data Descriptor IDs
    enum iswSolNetLrfDataDescTupleIds: uint16_t
    {
        LRF_TARGET                          = 0x0000,  // Main Target Tuple
        LRF_TARGET_RANGE                    = 0x0001,
        LRF_TARGET_RANGE_ERROR              = 0x0002,
        LRF_TARGET_AZIMUTH                  = 0x0003,
        LRF_TARGET_AZIMUTH_ERROR            = 0x0004,
        LRF_TARGET_ELEVATION                = 0x0005,
        LRF_TARGET_ELEVATION_ERROR          = 0x0006,
        LRF_QFACTOR                         = 0x0007,
        LRF_TARGET_ROLL                     = 0x0008,
        LRF_BALLISTIC_ELEVATION_HOLD        = 0x0020,
        LRF_BALLISTIC_WINDAGE_HOLD          = 0x0021,
        LRF_BALLISTIC_WINDAGE_CONFIDENCE    = 0x0022,
        LRF_TEMPERATURE                     = 0x0040,
        LRF_HUMIDITY                        = 0x0041,
        LRF_PRESSURE                        = 0x0042,
        LRF_CLEAR_RANGE_EVENT               = 0x0080,
        LRF_MANUFACTURING_TUPLES_MIN        = 0x8000,
        LRF_MANUFACTURING_TUPLES_MAX        = 0x8100
    };

    //! Enumerated LRF Control Data Descriptor IDs
    enum iswSolNetLrfControlDataDescIds: uint16_t
    {
        LRF_CTRL_RANGE = 0x0000
    };

    //! Enumerated LRF Status Data Descriptor IDs
    enum iswSolNetLrfStatusDataDescIDs: uint16_t
    {
        STATUS_BATTERY = 0x0001,
        STATUS_DEVICE  = 0x0002
    };

    //! LRF Range Data Descriptor
    struct iswSolNetLrfRangeControlDataDesc
    {
       iswSolNetDataTupleCommonHeader header;
    }__attribute__((packed));

    //! LRF Target Descriptor
    struct iswSolNetLrfTargetTuple
    {
        iswSolNetDataTupleCommonHeader header;
        uint8_t targetFlags;
        uint8_t rangeEventId;
        uint8_t targetIndex;
        uint8_t totalNumberOfTargets;
        uint8_t targetChildTuples;  // This is a buffer of size (uint8_t * totalNumberOfTargets)
                                    // it will follow totalNumberOfTargets in the dataPayload
    }__attribute__((packed));

    //! LRF Target Range Descriptor
    struct iswSolNetLrfTargetRangeTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t range;
    }__attribute__((packed));

    //! LRF Target Range Error Descriptor
    struct iswSolNetLrfTargetRangeErrorTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t rangeError;
    }__attribute__((packed));

    //! LRF Target Azimuth Descriptor
    struct iswSolNetLrfTargetAzimuthTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t azimuth;
        uint8_t azimuthNorthReference;
        uint8_t reserved[3];
    }__attribute__((packed));

    //! Enumerated Azimuth North Reference
    enum iswSolNetLrfAzimuthNorthReference: uint8_t
    {
        TRUE_NORTH     = 0x00,
        MAGNETIC_NORTH = 0x01,
    };

    //! LRF Target Azimuth Error Descriptor
    struct iswSolNetLrfTargetAzimuthErrorTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t azimuthError;
    }__attribute__((packed));

    //! LRF Target Elevation Descriptor
    struct iswSolNetLrfTargetElevationTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t elevation;
    }__attribute__((packed));

    //! LRF Target Elevation Error Descriptor
    struct iswSolNetLrfTargetElevationErrorTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t elevationError;
    }__attribute__((packed));

    //! LRF Ballistic Elevation Descriptor
    struct iswSolNetLrfBallisticElevationTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t ballisticElevationHold;
    }__attribute__((packed));

    //! LRF Ballistic Windage Descriptor
    struct iswSolNetLrfBallisticWindageTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t ballisticWindageHold;
    }__attribute__((packed));

    //! LRF Ballistic Windage Confidence Descriptor
    struct iswSolNetLrfBallisticWindageConfidenceTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t ballisticWindageConfidenceHold;
    }__attribute__((packed));

    //! LRF Temperature Descriptor
    struct iswSolNetLrfTemperatureTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t temperature;
    }__attribute__((packed));

    //! LRF Humidity Descriptor
    struct iswSolNetLrfHumidityTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t humidity;
    }__attribute__((packed));

    //! LRF Pressure Descriptor
    struct iswSolNetLrfPressureTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t pressure;
    }__attribute__((packed));

    //! LRF Q-Factor Descriptor
    struct iswSolNetLrfQFactorTuple
    {
        iswSolNetDataTupleCommonHeader header;
        float_t QFactor;
    }__attribute__((packed));

    //! LRF Target Roll Descriptor
    struct iswSolNetLrfRollTuple
    {
       iswSolNetDataTupleCommonHeader header;
       float_t rollAngle;
    }__attribute__((packed));

    struct iswSolNetLrfClearRangeEventTuple
    {
       iswSolNetDataTupleCommonHeader header;
       uint8_t rangeEventId;
       uint8_t reserved[3];
    }__attribute__((packed));

    struct iswSolNetLrfCapabilitiesDesc
    {
        iswSolNetDescCommonHeader header;
        uint32_t capabilities;
    }__attribute__((packed));

    struct iswSolNetTextLabelDesc
    {
       iswSolNetDescCommonHeader header;
       uint8_t textLabel;
    }__attribute__((packed));

    //! Enumerated AR Data Payload IDs
    enum iswSolNetArDataDescTupleIds: uint16_t
    {
        AR_GET_PARAM               = 0x2E01,
        AR_GET_PARAM_RESP          = 0x2E02,
        AR_SET_PARAM               = 0x2E03,
        AR_SET_PARAM_RESP          = 0x2E04,
        AR_PARAM_ERROR             = 0x2E05,
        AR_OVERLAY_DISTORT         = 0x2E06,
        AR_OVERLAY_FOV             = 0x2E09,
        AR_FLOAT_IMU_RATE          = 0x2401,
        AR_IMU_NOISE               = 0x2402,
        VIDEO_INTRINSICS           = 0x0B80,
        VIDEO_FRAME_RATE           = 0x0B90,
        VIDEO_CONTROL              = 0x0B00,
        AR_VIDEO_DISTORT           = 0x0A80,
        VIDEO_FRAME_RLE_METADATA   = 0x010B,
        AR_IMU_DATA                = 0x900F,
        AR_IMU_ROUNDTRIP_TIME      = 0x2E07,
        AR_MAG_CAL_CHANGED         = 0x2E08,
        VIDEO_FRAME_METADATA       = 0x0100,
        AR_VIDEO_IMU_SYNC_METADATA = 0x9010
    };

    //! ***********************************
    //! End SolNet Descriptor declarations
    //! ***********************************

    //! ***************************************
    //! End SolNet Application Services structs
    //! ***************************************
    //!
    //! MASK to check Data Policies field
    static const uint8_t DATA_POLICIES_MASK          = 0xFF;
    static const uint8_t DATA_POLICIES_PRIORITY_MASK = 0x01;
    static const uint8_t DATA_POLICIES_DISCARD_MASK  = 0x02;
    static const uint8_t DATA_POLICIES_FLOW_MASK     = 0x0E;

    enum iswSolNetQoS: uint8_t
    {
        HiNoDiscard     = 0x01,
        HiDiscard       = 0x11,
        LowNoDiscard    = 0x00,
        LowDiscard      = 0x10
    };

    //! Data Policies Priority values
    static const uint8_t DATA_POLICIES_HI_PRIORITY  = 0x01;
    static const uint8_t DATA_POLICIES_LOW_PRIORITY = 0x00;

    //! Data Policies Discard values
    static const uint8_t DATA_POLICIES_DISCARD    = 0x02;
    static const uint8_t DATA_POLICIES_NO_DISCARD = 0x00;

    //! Data Policies Flow values
    static const uint8_t DATA_POLICIES_POLL_FLOW       = 0x00;
    static const uint8_t DATA_POLICIES_AUTONOMOUS_FLOW = 0x04;
    static const uint8_t DATA_POLICIES_NO_FLOW         = 0x08;
    static const uint8_t DATA_POLICIES_ALWAYS_FLOW     = 0x0C;

    //! Data Policies Reserved bits
    static const uint8_t DATA_POLICIES_RESERVED = 0xE0;

    //! Sol Net Data Packet Support
    //! Register a callback function to the application
    //! for data packets received
    typedef void (* iswSolNetReceiveDataCallbckFunction)(uint8_t *data, uint16_t transferSize, void *thisPtr);
    void RegisterReceiveDataCallbackFunction(uint8_t peerIndex, uint8_t dataflowId, iswSolNetReceiveDataCallbckFunction callbackFn, void *thisPtr);

    //! Register a callback function to the application
    //! for a poll data request from another peer
    //! The application will use this function to send
    //! all data for a service to all registered peers
    typedef void (* iswSolNetPollDataCallbckFn)(uint8_t dataflowId);
    void RegisterPollDataCallbackFunction(uint8_t dataflowId, iswSolNetPollDataCallbckFn callbackFn);

    //! Add these to dataflowId to set provider or consumer
    const static uint8_t iswSolNetSetConsumerBit       = 0x80;
    const static uint8_t iswSolNetProviderConsumerMask = 0x7F;

    //! Current max number of apps for this device - can increase
    static const uint8_t MAX_NUMBER_APPS = iswSolNetProviderConsumerMask;

    //! Use this peerIndex when adding a service to this device
    static const uint8_t defaultPeerIndex = 0xFF;

    //! MAX ISW peers per ISW specification
#define MAX_PEERS 14

    //! SolNet Endpoint distribution settings - used in Property Descriptors
    enum iswSolNetEndpointDistribution: uint8_t
    {
        Unicast     = 0x00,
        Broadcast   = 0xFF
        //Multicast = multicast group Id or peerIndex see IswStream class
    };

    //! ISW SolNet Retransmit
    struct iswRetransmitItem
    {
        iswSolNetHeader iswSolNetHdr;
        uint64_t timestamp;
        uint8_t retransCount;
        uint8_t endpoint;
    }; // Doesn't need to be packed - it's only used locally within our system

    //! Info kept for each peer that sends this device a RegisterRequest message
    struct iswSolNetRegisteredPeer
    {
        std::mutex registeredPeerLock;
        bool peerRegistered;
        uint32_t generationId;
        uint8_t flowState ;      //! 0 = stop dataflow; 1 = start dataflow
        uint8_t autonomy;        //! 0 = wait to be polled; 1 = send data automatically - how the peer registered with us
        uint8_t status;          //! status of this service in regards to this peer see iswSolNetRegistrationStatus
        uint8_t initiator;       //! initiator of the message relative to the service -- Subscriber or Provider
        bool sendRegRequestResponse;
        uint8_t regRequestResponseSeqNo;
        bool sendDeregistrationRequestResponse;
        uint8_t deregRequestResponseSeqNo;
        bool sendAutonomousStartStopResponse;
        uint8_t autonomousStartStopSeqNo;
        bool sendGetStatusResponse;
        uint8_t getStatusResponseSeqNo;
        bool sendPollDataResponse;
        uint8_t pollDataResponseSeqNo;
        bool sendKeepAliveResponse;
        uint8_t keepAliveResponseSeqNo;
        //! We are the producer, but the peer may have a case to send
        //! data packets to us and request an ACK. Those ACKs are held
        //! on this list per peer.
        std::list<iswSolNetAckPacket *> *acksToSendList;
        //! Retransmit queue in case we requests ACKs on data packets we send the peer
        std::list<iswRetransmitItem *> *retransmitList;
    }; // Doesn't need to be packed - it's only used locally within our system

    //! ServiceEntry struct used in adding services for this device
    struct iswSolNetServiceEntry
    {
        bool inUse;
        uint32_t generationId;          //! Generation ID
        uint8_t dataflowId;             //! Bits 0 – 6: Dataflow Number (0 – 127) and Bit 7: Role (0 = producer, 1 = consumer)
        uint8_t endpointId;             //! 0 -3
        uint8_t endpointDistribution;   //! Data target (0 = unicast, 0xFF = broadcast, 0x01-0xFE = multicast group ID)
        uint8_t dataPolicies;           //! See iswSolNetQoS
        uint8_t nextSendDataSeqNumber;  //! 0 - 255 - wraps
        iswSolNetReceiveDataCallbckFunction receiveCallbackFn;  //! Set by application
        void *recCallbackThisPtr;                               //! Set by application
        iswSolNetPollDataCallbckFn pollDataCallbackFn;          //! Set by application
        std::array<iswSolNetRegisteredPeer, MAX_PEERS> registeredPeers;  //! which peers have registered for this service
        union
        {
            iswSolNetServiceDesc serviceDesc;
            iswSolNetServiceControlDesc serviceControlDesc;
        };
    }; // Doesn't need to be packed - it's only used locally within our system

    struct iswSolNetPeerServiceEntry
    {
        bool inUse;
        bool ImRegistered;              //! Used in peer services to see if this device is registered to receive peer data
        uint32_t generationId;          //! Generation ID
        uint8_t dataflowId;             //! Bits 0 – 6: Dataflow Number (0 – 127) and Bit 7: Role (0 = producer, 1 = consumer)
        uint8_t endpointId;             //! 0 -3
        uint8_t endpointDistribution;   //! Data target (0 = unicast, 0xFF = broadcast, 0x01-0xFE = multicast group ID)
        uint8_t dataPolicies;           //! See iswSolNetQoS
        uint8_t autonomy;               //! default = 0 = wait to be polled; 1 = send data automatically - how we registered with this peer for this service
        uint8_t nextSendDataSeqNumber;  //! 0 - 255 - wraps
        uint8_t status;                 //! See iswSolNetRegistrationStatus
        iswSolNetReceiveDataCallbckFunction receiveCallbackFn;  // Set by application
        void *recCallbackThisPtr;                               // Set by application
        union
        {
            iswSolNetServiceDesc serviceDesc;
            iswSolNetServiceControlDesc serviceControlDesc;
        };
    }; // Doesn't need to be packed - it's only used locally within our system

    //! peerServiceEntry struct includes an array of services for the peer
    //! with struct above. Indicates the Peer is the producer and we are the
    //! consumer.
    struct iswSolNetPeerServicesEntry
    {
        bool inUse;
        std::mutex peerLock;
        bool sendBrowse;
        bool sendAdvertise;
        bool sendRevokeRegistrationResponse;
        uint8_t revokeRegRequestResponseSeqNo;
        bool sendRevokeNetworkAssociationResponse;
        uint8_t revokeNetworkAssociationResponseSeqNo;
        std::array<iswSolNetPeerServiceEntry, MAX_NUMBER_APPS> services;

        //! The peer is the producer and sent a data packet requesting an ACK
        //! This list holds ACKs for that case
        std::list<iswSolNetAckPacket *> *acksToSendList;
        //! We are the consumer, but there may be a case where we send
        //! data to the producer and request an ACK.  In that case we
        //! hold the packet on a retransmit queue waiting for the ACK
        std::list<iswRetransmitItem *> *retransmitList;
    }; // Doesn't need to be packed - it's only used locally within our system
    std::array<iswSolNetPeerServicesEntry, MAX_PEERS> *GetPeerServices(void) { return(&iswSolNetPeerServices); }

    enum iswSolNetRegistrationStatus: uint8_t
    {
        RegStatusUnavailable                    = 0x00,     //Unavailable for registrations
        RegStatusAvailableToRegister            = 0x01,     //No current registrations; available for registration
        RegStatusAvailableForYourUseOnly        = 0x02,     //Registered to requesting node only; available for use
        RegStatusUnavailableForRegistration     = 0x03,     //Registered to non-requesting node; unavailable for registration
        RegStatusAvailableForUseYouAndOthers    = 0x04      //Registered to requesting and non—requesting nodes; available for use
    };

    enum iswSolNetSimpleUiButtonCount: uint8_t
    {
        OneButton    = 0x01,
        TwoButtons   = 0x02,
        ThreeButtons = 0x03
    };

    enum iswSolNetSimpleUiStateCount: uint8_t
    {
        OneState = 0x01,
        TwoState = 0x02,
    };

    enum iswSolNetCapabilities: uint8_t
    {
        STANDARD_FORMAT = 0x01,
        PRESS           = 0x02,
        RELEASE         = 0x04,
        HOLD            = 0x08,
        DOUBLE_PRESS    = 0x10,
        HIGH_STATE      = 0x20,
        BITMAP          = 0x80
    };

    //! Methods to manage service arrays for the device and
    //! for the peers of the device
    //!
    //! Initialze Service arrays to default values
    void RemoveOneRegisteredPeer(uint8_t dataflowId, uint8_t peerIndex, bool clear);
    void RemoveOneService(uint8_t dataflowId, bool clear);
    void RemoveAllServices(bool clear);

    //! Initialze Peer Service arrays to default values
    void RemoveOneServiceForOnePeer(uint8_t peerIndex, uint8_t dataflowId, bool clear);
    void RemoveAllServicesForOnePeer(uint8_t peerIndex, bool clear);
    void RemoveAllPeerServices(bool clear);

    //! Application calls this - it's the index into the services table
    uint8_t GetNextDataflowId(void);

    //! Used to decrement the dataflow id count when a service is removed from the service desc list.
    //! Prevents a gap from being left in the dataflowId sequence.
    uint8_t DecrementDataflowId(void);

    //! Application calls this first before passing in the serviceEntry to AddSolNetService
    uint32_t GetNextServiceNo(void);

    //! Used to verify incoming Advertise messages - childblock descriptors
    bool VerifySolNetDescriptor(iswSolNetDescCommonHeader *descriptor, uint16_t *offset, iswSolNetServiceEntry *serviceEntry);

    //! Used to verify incoming Advertise messages containing services
    bool VerifySolNetServiceEntry(iswSolNetServiceEntry *serviceEntry);

    //! Can be used to add a service to this device or to a peer's device array
    void AddSolNetService(uint8_t peerIndex, iswSolNetServiceEntry *serviceEntry);

    //! dataflowId is used as the index into the service array
    std::array<iswSolNetServiceEntry, MAX_NUMBER_APPS> *GetServices(void) { return(&iswSolNetServices); }

    static const uint8_t POLICY_PAYLOAD_CHECKSUM    = 0x01;
    static const uint8_t POLICY_PAYLOAD_NO_CHECKSUM = 0x00;
    static const uint8_t POLICY_NO_ACK              = 0x00;
    static const uint8_t POLICY_ACK                 = 0x02;

    //! Methods to manage the registered services table
    void SetBrowse(uint8_t peerIndex, bool browse);
    void SetAdvertise(uint8_t peerIndex, bool advertise);
    void SetImRegistered(uint8_t peerIndex, uint8_t dataflowId, bool registered);
    void SetSendGetStatusResponse(uint8_t peerIndex, uint32_t serviceSelector, bool sendStatus, uint8_t seqNumber);
    void SetSendRevokeNetworkAssociationResponse(uint8_t peerIndex, uint8_t seqNumber);
    void SetRegistrationStatus(uint8_t peerIndex, uint32_t serviceSelector, uint8_t registrationStatus);
    void UpdateServiceWithPeerRegisterRequest(uint8_t peerIndex, iswSolNetRegisterRequest *regRequest, uint8_t seqNumber);
    void UpdateServiceWithPeerDeregisterRequest(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber);
    void UpdateServiceWithPeerFlowState(uint8_t peerIndex, iswSolNetAutonomousStartStopRequest *autonomousStartStopMsg, uint8_t seqNumber);
    void UpdateServiceWithPeerPollDataRequest(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber);
    void UpdatePeerServiceWithRevokeRegistration(uint8_t peerIndex, uint32_t serviceSelector, uint8_t seqNumber);
    void UpdatePeerServiceWithKeepAliveInitiator(uint8_t peerIndex, iswSolNetKeepAliveRequest *keepAliveRequest, uint8_t seqNumber);

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverSolNetPacket(uint8_t peerIndex, uint8_t endPoint, uint8_t *data);

    //! Send ISW commands to the firmware
    //!
    //! SolNet Message Packets
    int SendSolNetMessagePacket(uint8_t messageClass,
                                uint8_t messageId,
                                uint8_t peerIndex,
                                uint8_t policies,
                                uint8_t *payload,
                                uint16_t payloadLength,
                                bool save,
                                uint8_t seqNumber,
                                uint8_t msgStatus);

    //! SolNet Discovery Messages
    int SendBrowseMessage(uint8_t peerIndex);
    int SendAdvertiseMessage(uint8_t peerIndex, uint8_t dataflowId, uint8_t policies);
    int SendAdvertiseChangeMessage(uint8_t peerIndex);

    //! SolNet Flow Messages
    int SendRegisterRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t autonomy);
    int SendRegisterResponseMessage(uint8_t peerIndex, uint8_t seqNumber, uint8_t msgStatus);
    int SendAutonomousStartStopRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t flowState);
    int SendAutonomousStartStopResponseMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t flowState, uint8_t seqNumber);
    int SendDeregisterRequestMessage(uint8_t peerIndex, uint32_t serviceSelector);
    int SendDeregisterResponseMessage(uint8_t peerIndex, uint8_t seqNumber);
    int SendRevokeRegistrationMessage(uint8_t peerIndex, uint32_t serviceSelector);
    int SendRevokeRegistrationResponseMessage(uint8_t peerIndex, uint8_t seqNumber);
    int SendGetStatusRequestMessage(uint8_t peerIndex, uint32_t serviceSelector);
    int SendGetStatusResponseMessage(uint8_t peerIndex, uint8_t serviceStatus, uint8_t seqNumber);
    int SendRevokeNetworkAssociationRequestMessage(uint8_t peerIndex);
    int SendRevokeNetworkAssociationResponseMessage(uint8_t peerIndex, uint8_t seqNumber);
    int SendPollDataRequestMessage(uint8_t peerIndex, uint32_t serviceSelector);
    int SendPollDataResponseMessage(uint8_t peerIndex, uint8_t seqNumber);
    int SendKeepAliveRequestMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t initiator);
    int SendKeepAliveResponseMessage(uint8_t peerIndex, uint32_t serviceSelector, uint8_t initiator, uint8_t seqNumber);

    //! SolNet Reporting Messages
    int SendReportDataflowConditionIndication(uint8_t peerIndex, uint32_t serviceSelector, uint8_t endpointNumber, uint8_t dataflowId, uint8_t seqNumber, uint8_t condition);

    //! mutex lock synchronizes send from two
    //! threads - IswSolNet and IswInterface
    //! when sending packets
    std::mutex iswSolNetCmdLock;

    //! Send SolNet data packet
    int SendSolNetDataPacket(uint8_t peerIndex,
                             uint8_t endpoint,
                             uint8_t dataflowId,
                             uint8_t policies,
                             uint8_t *payload,
                             uint16_t payloadLength);

    //! When this device receives the SolNet RevokeNetworkAssociation message
    //! it will call this function to alert the application or GUI to shutdown
    //! all data streams.
    typedef void (* iswSolNetClearNetworkAssociationClbckFn)(IswSolNet *thisPtr);
    void RegisterClearAssociationCallbackFunction(iswSolNetClearNetworkAssociationClbckFn callbackFn);

    //! Send an acknowledgement packet to the firmware
    int SendSolNetAckPacket(uint8_t peerIndex, iswSolNetAckPacket *ackPkt);

    //! For Debugging
    static std::string GetServiceIdString(uint16_t serviceId);
    static std::string GetPropertyIdString(uint16_t propertyId);
    void PrintDescriptorIdToLogfile(std::stringstream *ss, uint16_t descriptorId, uint8_t *data);
    static void PrintDescriptorIdToDatabase(std::stringstream *cdss, std::stringstream *pss, std::stringstream *vss, uint16_t descriptorId, uint8_t *data);
    void PrintPeerServicesToLogfile(void);
    void PrintServicesToLogfile(void);

    std::string getProviderDevName() const;
    void setProviderDevName(const std::string &value);

    //!############################################################################
    //! Name: printDescriptor
    //! Description: Given an ISW SolNet descriptor common header, print a
    //! single space delimited hexadecimal representation of the bytes that
    //! form an ISW SolNet descriptor.
    //!
    //! Input: commondHdr: iswSolNetDescCommonHeader pointer to the common
    //! header corresponding with an ISW SolNet descriptor that needs to be
    //! printed for debugging purposes.
    //!
    //! Output: A single space delimited hexadecimal representation of the
    //! bytes that form an ISW SolNet descriptor, printed to stdout.
    //!############################################################################
    void printDescriptor(iswSolNetDescCommonHeader *commonHdr);

    ACCESS:
    float_t getGenerationId() const;

    uint8_t getMaxInflightSolNetMessages() const;
    void setMaxInflightSolNetMessages(const uint8_t &value);

    uint8_t getSolNetMessageRetries() const;
    void setSolNetMessageRetries(const uint8_t &value);

    uint8_t getRequestTimeout() const;
    void setRequestTimeout(const uint8_t &value);

    uint8_t getReportPeriod() const;
    void setReportPeriod(const uint8_t &value);

    uint8_t getKeepAlivePeriod() const;

    uint16_t getBatteryDataTupleId() const;
    uint16_t getBatteryDataLength() const;
    uint16_t getTargetDataTuple() const;
    uint16_t getTargetDataLength() const;
    uint16_t getRangeControlTupleId() const;
    uint16_t getRangeControlLength() const;
    uint8_t getBatteryId() const;
    uint8_t getBatteryCharging() const;
    uint8_t getBatteryState() const;
    uint8_t getBatteryLevel() const;

    void restoreQosDescriptors(iswSolNetServiceDesc *);
    void restoreVideoFormatDescriptors(iswSolNetServiceDesc *);

    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! Class variables
    uint8_t iswSolNetMessageSeqNo = 0;
    bool solNetStopped            = false;

    //! Thread to check for:
    //! 1. Send Browse
    //! 2. Send Advertise
    //! 3. Send Ack
    pthread_t threadCheckSolNetSendEvents = 0;

    //! While this module is running to check events
    bool checkingSolNetEvents = false;

    //! Used in handling retransmit of packets waiting for ACKs
    uint64_t iswSolNetRetransTimeout; // Linux timestamp in microseconds 1000000 = 1 second
    uint8_t iswSolNetNoOfRetrans;

    //! Set by application in register call to handle the RevokeNetworkAssociation message
    iswSolNetClearNetworkAssociationClbckFn clearNetworkAssocCallbackFn = nullptr;

    //! ISW SolNet commands and events
    enum iswSolNetMsgClassCategories: uint8_t
    {
        ReservedMsgClass1   = 0x00,     //! Reserved for future specification
        GlobalAppLow        = 0x01,     //! range 0x01 to 0x1F	Message classes that apply to all devices.
        GlobalAppHi         = 0x1F,
        GenFunctLow         = 0x20,     //! range 0x02 to 0x3F	Message classes that apply to particular device functions or conditions but are not specific to individual devices.
        GenFunctHi          = 0x3F,
        ReservedMsgClassLow = 0x40,     //! range 0x40 to 0x5F	Reserved for future specification
        ReservedMsgClassHi  = 0x5F,     //! range 0x40 to 0x5F	Reserved for future specification
        TargetDevLow        = 0x60,     //! range 0x60 to 0x7F	Message classes that apply to specific devices or device applications.
        TargetDevHi         = 0x7F,
        VendorLow           = 0x80,     //! range 0x80 to 0xFE	Vendor-defined classes. Messages within these classes are left to vendor definition and use.
        VendorHi            = 0xFE
    };

    enum iswsolNetMsgClassTypes: uint8_t
    {
        Discovery      = 0x01,     //! Messages for service discovery and notification
        Flow           = 0x02,     //! Messages for flow establishment and control
        Reporting      = 0x03,     //! Messages for reporting SolNet Conditions
        LowLevelBridge = 0x04,     //! Messages for performing operations
        Policy         = 0x1F,
        UsbHost        = 0x20
    };

    //! ISW SolNet Message Status
    enum iswSolNetMsgStatus: uint8_t
    {
        MsgSuccess                          = 0x00,
        UndefError                          = 0x01,     //! used when no other status is appropriate
        TransientFailure                    = 0x02,     //! retry
        OutOfMemory                         = 0x03,     //! retry
        InvalidParameter                    = 0x04,
        RequestDenied                       = 0x05,
        ResourceBusy                        = 0x06,     //! retry
        OpTimedOut                          = 0x07,
        OpInProgress                        = 0x08,
        PolicyViolation                     = 0x09,     //! range 0x09 to 0x7F Reserved for future specification
        ResourceNotReady                    = 0x0A,
        UnrecognizedSolNetVersion           = 0x0B,
        ReservedFutureHi                    = 0x7F,
        ReservedVendorLow                   = 0x80,     //! range 0x80 to 0xFE reserved for vendor use
        ReservedVendorHi                    = 0xFE,
        NotSupported                        = 0xFF      //! SolNet protocol not supported
    };

    //! ISW SolNet Discovery Message IDs
    enum iswSolnetDiscoveryMsgIds: uint8_t
    {
        BrowseServices                      = 0x01,     //! Browse Service: Search for available nodes and services
        ServiceAdvertiseAnnounce            = 0x81,     //! Service Advertisement: Announce supported/expected services
        ServiceAdvertiseChange              = 0x02,
        ServiceAdvertiseChangeConfirmation  = 0x82      //! Announce change of advertisement content
    };

    //! ISW SolNet Flow Message IDs
    enum iswSolnetFlowMsgIds: uint8_t
    {
        GetStatusRequest                    = 0x01,
        RegisterRequest                     = 0x02,
        DeregisterRequest                   = 0x03,
        RevokeRegistration                  = 0x04,
        PollDataRequest                     = 0x05,
        AutonomousStartStopRequest          = 0x06,
        KeepAliveRequest                    = 0x07,
        GetStatusResponse                   = 0x81,
        RegisterResponse                    = 0x82,
        DeregisterResponse                  = 0x83,
        RevokeRegistrationResponse          = 0x84,
        PollDataResponse                    = 0x85,
        AutonomousStartStopResponse         = 0x86,
        KeepAliveResponse                   = 0x87
    };

    enum iswSolnetReportingMsgId: uint8_t
    {
        ReportDataflowConditionIndication   = 0x81
    };

    enum iswSolnetLowLevelBridgeMsgId: uint8_t
    {
        RevokeNetworkAssociationRequest  = 0x01,
        RevokeNetworkAssociationResponse = 0x81
    };

    enum iswSolnetPolicyMsgIds: uint8_t
    {
        Acknowledgement = 0x81	// Acknowledgement packet (ACK)
    };

    enum iswSolNetPropertyDescriptorRanges: uint16_t
    {
        GeneralMinPropId        = 0x0100,	//! GENERAL	Property descriptors describing properties of an ISW node or general properties of various classes
        GeneralMaxPropId        = 0x07FF,
        VideoMinPropId          = 0x0800,	//! VIDEO	Property descriptors describing properties of a video service or video element
        VideoMaxPropId          = 0x0BFF,
        AudioMinPropId          = 0x0C00,	//! AUDIO	Property descriptors describing properties of an audio service or audio element
        AudioMaxPropId          = 0x0FFF,
        PositionMinPropId       = 0x1000,	//! POSITION		Property descriptors describing properties of a service or element that provides position or vectors
        PositionMaxPropId       = 0x13FF,
        EnvMinPropId            = 0x1400,	//! ENVIRONMENT	Property descriptors describing properties of a service or element that provides environmental information
        EnvMaxPropId            = 0x17FF,
        HealthMinPropId         = 0x1800,	//! HEALTH	Property descriptors describing properties of a service or element that provides human health information
        HealthMaxPropId         = 0x1BFF,
        UiMinPropId             = 0x1C00,       //! UI	Property descriptors describing properties of a UI service or element
        UiMaxPropId             = 0x1FFF,
        ControlMinPropId        = 0x2000,	//! CONTROL	Property descriptors describing properties of a service or element consisting solely of control (protocol) exchange
        ControlMaxPropId        = 0x23FF,
        RtaMinPropId            = 0x2400,	//! RTA	PropId descriptors describing properties of a service or element that performs Rapid Target Acquisition
        RtaMaxPropId            = 0x27FF,
        NetworkingMinPropId     = 0x2800,	//! NETWORKING	Property descriptors describing properties of a service or element that provides networking function (e.g. IP protocol).
        NetworkingMaxPropId     = 0x2BFF,
        RangeMinPropId          = 0x2C00,       //! RANGE  Property descriptors describing properties of a service or element that provides range data
        RangeMaxPropId          = 0x2CFF,
        ArMinPropId             = 0x2D00,       //! AR    Property descriptors describing properties of a service or element that provides augmented reality (AR) information
        ArMaxPropId             = 0x2DFF,
        Reserved1MinPropId      = 0x2E00,       //! RESERVED	Reserved for future specification
        Reserved1MaxPropId      = 0x33FF,
        PowerMinPropId          = 0x3400,
        PowerMaxPropId          = 0x34FF,
        Reserved2MinPropId      = 0x3500,	//! RESERVED	Reserved for future specification
        Reserved2MaxPropId      = 0x7FFF
    };

    //! Used for Services for this device
    //!
    //! dataflowId is used as the index into the service array
    //! the user calls GetNextDataflowId()
    uint8_t iswSolNetNextDataflowId = 0;

    //! serviceSelector is unique per service
    //! the user calls GetNextServiceNo()
    uint32_t iswSolNetNextServiceSelectorNo = 0;

    //! Used to lock the services table for multi-threaded access
    std::mutex iswSolNetServicesLock;

    //! This is the main array or table of SolNet services for this device
    std::array<iswSolNetServiceEntry, MAX_NUMBER_APPS> iswSolNetServices;

    //! Peer services
    //!
    //! This is the peer servie array or table of peer services
    //! for this device
    std::array<iswSolNetPeerServicesEntry, MAX_PEERS> iswSolNetPeerServices;

    //! Save messages that have a response that must be matched per service
    //! in the iswSolNetMessageResponseList
    //! Lock the list for multi-threaded access
    std::mutex iswSolNetMsgRespListLock;
    std::list<iswSolNetHeader *> iswSolNetMessageResponseList;

    //! Set to validate incoming Advertise Messages
    bool verifyAdvertiseMessages = false;
    bool verifyChecksum          = true;

    //! Start/Stop the HandleSolNetSendEvents thread
    void StartIswSolNetHandleSendEventsThread(void);
    void StopIswSolNetHandleSendEventsThread(void);

    std::mutex parseAdvertiseMessageLock;
    std::mutex verifyDataPayloadLock;
    void ParseSolNetAdvertiseAnnounceMessage(uint8_t peerIndex, iswSolNetMessagePacket *iswSolNetMsgPkt);
    void VerifySolNetDataPayload(IswSolNet::iswSolNetPeerServiceEntry *serviceEntry, iswSolNetDataPacket *iswSolNetDataPkt);

    //! The main DeliverSolNetPacket determines which of these should be
    //! called based on whether it is a SolNet Message or Data Packet
    void DeliverSolNetDataPacket(uint8_t peerIndex, uint8_t endPoint, iswSolNetDataPacket *iswSolNetDataPkt);
    void DeliverSolNetMessagePacket(uint8_t peerIndex, uint8_t endPoint, iswSolNetMessagePacket *iswSolNetMsgHdr);

    //! Used in sending SolNet messages
    uint8_t GetNextMsgSeqNo(void);

    //! Sequence Numbers for data are per Application
    uint8_t GetNextDataSeqNo(uint8_t dataflowId);

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    int SetupSolNetMessagePacket(iswSolNetHeader *iswSolNetHdr,
                                 uint8_t seqNumber,
                                 uint8_t msgStatus,
                                 uint8_t messageClass,
                                 uint8_t messageId,
                                 uint8_t policies,
                                 uint8_t *payload,
                                 uint16_t payloadLengthr);

    int SetupSolNetDataPacket(iswSolNetHeader *iswSolNetHdr,
                              uint8_t dataflowId,
                              uint8_t policies,
                              uint8_t *payload,
                              uint16_t payloadLength,
                              iswRetransmitItem **retransItem);

    int SetupSolNetAckPacket(iswSolNetHeader *iswSolNetHdr, iswSolNetAckPacket *ackPkt);

    //! Methods to manage SolNet class object
    //!
    //! Used in calculating SolNet checksums
    uint16_t IswChecksum(uint32_t count, uint16_t *addr);
    void CalculateHeaderChecksum(iswSolNetHeader *iswSolNetHdr);

    //! Empty the list of saved messages to match requests with responses
    void EmptyMsgSaveList(void);
    //! Empty the list of Acknowledgement packets to send out
    void EmptyAcksToSendList(std::list<iswSolNetAckPacket *> *acksToSendList);
    //! Empty the list of packets waiting for ACKs
    void EmptyRetransmitList(std::list<iswRetransmitItem *> *retransmitList);

    //! For handling SolNet send events
    static void *HandleSolNetSendEvents(void *arguments);
    int StartSolNetSendThread(void);

    //! Handle cleanup of resources and clear network association for this device
    int ClearNetworkAssociation(void);

    //! Set the generationId to zero by default -- increase prior to service advertise change is sent
    uint8_t generationId = 0;

    uint8_t batteryLevel    = 0x00;
    uint8_t batteryState    = 0x00;
    uint8_t batteryCharging = 0x00;
    uint8_t batteryId       = 0x00;

    uint16_t rangeControlDataLength  = 0x0000;
    uint16_t rangeControlDataTupleId = 0x0000;

    //! Video Data Tuple Fields
    uint16_t videoFrameLength             = 0x0000;
    uint16_t videoFrameDataTupleId        = 0x0000;
    uint16_t videoPropertiesLength        = 0x0000;
    uint16_t videoPropertiesTupleId       = 0x0000;
    uint16_t videoPolarityLength          = 0x0000;
    uint16_t videoPolarityTupleId         = 0x0000;
    uint16_t videoImageCalibrationLength  = 0x0000;
    uint16_t videoImageCalibrationTupleId = 0x0000;
    uint16_t videoDataLength              = 0x0000;
    uint16_t videoDataTupleId             = 0x0000;
    uint16_t frameNumber                  = 0x0000;
    uint16_t packetCount                  = 0x0000;
    uint16_t rowNumber                    = 0x0000;
    uint16_t colNumber                    = 0x0000;
    uint8_t *videoPropertyTuples          = nullptr;
    uint8_t videoPolarity                 = 0x00;
    uint16_t videoImageCalibration        = 0x0000;
    uint8_t *videoData                    = nullptr;

    //! RTA Data Tuple Fields
    uint16_t rtaStatusDataLength   = 0x0000;
    uint16_t rtaStatusDataTupleId  = 0x0000;
    uint16_t rtaReticleDataLength  = 0x0000;
    uint16_t rtaReticleDataTupleId = 0x0000;
    uint16_t rtaImuDataLength      = 0x0000;
    uint16_t rtaImuDataTupleId     = 0x0000;
    uint8_t rtaEnabled             = 0x00;
    uint8_t rtaMode                = 0x00;
    uint8_t rtaPolarity            = 0x00;
    uint8_t rtaZoom                = 0x00;
    uint8_t rtaBubble              = 0x00;
    uint8_t rtaManualAlignment     = 0x00;
    uint8_t reticleInVideo         = 0x00;

    uint8_t rtaReticleType         = 0x00;
    uint8_t rtaReticleColorRed     = 0x00;
    uint8_t rtaReticleColorGreen   = 0x00;
    uint8_t rtaReticleColorBlue    = 0x00;
    int16_t rtaReticleXOffset      = 0x0000;
    int16_t rtaReticleYOffset      = 0x0000;

    float quaternionReal           = 0;
    float quaternionI              = 0;
    float quaternionJ              = 0;
    float quaternionK              = 0;

    //! Motion Sense Data Tuple Fields
    uint16_t sampleDataLength        = 0x0000;
    uint16_t sampleDataTupleId       = 0x0000;
    uint16_t accelerationDataLength  = 0x0000;
    uint16_t accelerationDataTupleId = 0x0000;
    uint16_t gyroDataLength          = 0x0000;
    uint16_t gyroDataTupleId         = 0x0000;
    uint16_t magDataLength           = 0x0000;
    uint16_t magDataTupleId          = 0x0000;
    uint32_t sampleTimestamp         = 0x0000;
    uint8_t *sensorTypeDataTuples    = nullptr;
    float_t accelerationData[2]      = {};
    float_t gyroData[2]              = {};
    float_t magData[2]               = {};

    //! LRF Data Tuple Fields
    uint16_t targetDataLength    = 0x0000;
    uint16_t targetDataTupleId   = 0x0000;
    uint8_t targetFlags          = 0x00;
    uint8_t rangeEventId         = 0x00;
    uint8_t targetIndex          = 0x00;
    uint8_t totalNumberOfTargets = 0x00;
    uint8_t *targetChildTuple    = nullptr;

    int64_t targetRangeData;
    uint16_t targetRangeLength;
    uint16_t targetRangeTupleId;
    int64_t targetRangeErrorData;
    int64_t targetAzimuthData;
    uint8_t azimuthNorthReferenceData;
    int64_t targetAzimuthErrorData;
    int64_t targetElevationData;
    int64_t targetElevationErrorData;
    int64_t qFactorData;
    int64_t targetRollData;
    int64_t ballisticElevationHoldData;
    int64_t ballisticWindageHoldData;
    int64_t ballisticConfidenceHoldData;
    int64_t temperatureData;
    int64_t humidityData;
    int64_t pressureData;
    uint8_t range_EventIdData;

    //! Status Data Tuple Fields
    uint16_t batteryDataLength       = 0x0000;
    uint16_t batteryDataTupleId      = 0x0000;
    uint16_t deviceStatusDataLength  = 0x0000;
    uint16_t deviceStatusDataTupleId = 0x0000;
    uint16_t weaponStatusDataLength  = 0x0000;
    uint16_t weaponStatusDataTuple   = 0x0000;

    uint8_t keepAlivePeriod           = 1; // Minimum
    uint8_t reportPeriod              = 1; // Default
    uint8_t requestTimeout            = 1; // Minimum
    uint8_t solNetMessageRetries      = 3; // Default
    uint8_t maxInflightSolNetMessages = 5; // Default

    uint8_t deviceState     = 0x00;
    uint8_t displayState    = 0x00;

    uint8_t weaponSightMode = 0x00;
    uint8_t activeReticleId = 0x00;
    uint8_t symbologyState  = 0x00;

private:

    std::string providerDevName   = "";
    std::string subscriberDevName = "";

};

#endif //! ISWSOLNET_H
