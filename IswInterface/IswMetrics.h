//!###############################################
//! Filename: IswMetrics.h
//! Description: Class that provides an interface
//!              to ISW Metrics commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWMETRICS_H
#define ISWMETRICS_H

#include <mutex>
#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswMetrics
{
public:
    //! IswMetrics Public Constructor
    IswMetrics(IswInterface *isw_interface, Logger *logger, DBLogger *dblogger);
    ~IswMetrics() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;
    //! DBLogger object pointer passed into constructor
    DBLogger *dbLogger = nullptr;

    //! ISW Metrics command structures
    struct iswMetricsCommand //!Get Wired and Wireless Metrics Command Block
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved;
    }__attribute__((packed));

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    uint16_t GetWirelessThroughput(void) { return (iswWirelessThroughput); }
    void SetPhyRateForExternal(uint8_t phy_rate) { iswPhyRate = phy_rate; }
    float GetPhyRate(void);  //! Returns Mpbs
    uint8_t GetRssi(void) {return iswRssi;}
    void SetRssi(uint8_t rssi) {iswRssi = rssi;}

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    ///! Send ISW commands to the firmware
    int SendGetWirelessMetrics(void);


ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

    //! For multi-threaded access
    std::mutex iswMetricCmdLock;

    //! Class variables
    uint16_t iswWirelessThroughput = 0;
    uint8_t iswPhyRate             = 0;
    uint8_t iswRssi                = 0;
    uint8_t iswLatency                =0;

    //! ISW Metric commands and events
    enum iswMetricsCmdEventTypes: uint8_t
    {
        Reserved1               = 0,
        Reserved2               = 1,
        GetWirelessMetrics      = 2,
        ResrvdFuture            = 3
    };

    struct iswMetricsGetWirelessMetrics
    {
        uint16_t throughput;        //Throughput (in Mbps)
        uint8_t phyRate;            //Most frequent overall PHY Rate used to transmit data (calculated roughly once per second).  Can be 0xFF (invalid) if the stream is idle.
        uint8_t numPeers;           //Number of peers in the list; all fields starting from “Peer Index” are repeated for each peer
    }__attribute__((packed));;

    struct iswPeerMetrics
    {
        uint8_t peerIndex;
        uint8_t phyRate;
        uint8_t rssi;
        uint8_t reserved;
        uint16_t transmitThroughput;
        uint16_t receiveThroughput;
    }__attribute__((packed));

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupGetWirelessMetrics(iswMetricsCommand *iswMetricsCmd, void *routingHdr);

};

#endif //! ISWMETRICS_H
