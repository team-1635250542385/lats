//!###############################################
//! Filename: IswFirmware.cpp
//! Description: Class that provides an interface
//!              to ISW Firmware commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswFirmware.h"
#include "IswInterface.h"
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <zlib.h>
#include <QDebug>

//!###############################################################
//! Constructor: IswFirmware()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswFirmware::IswFirmware(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswFirmware::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswFirmware::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;
    msgString.clear();

    switch (cmd)
    {
        case GetImageVersion:
        {
            std::string msgString;

            //! Get the event data returned from firmware
            iswGetImageVersionEvent *versionEvent = (iswGetImageVersionEvent *)data;

            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                std::stringstream ss;
                ss << "    versionString = " << versionEvent->versionString << std::endl;
                ss << "    apiVersion    = " << std::to_string(versionEvent->apiVersion) << std::endl;
                ss << "    reserved      = ";

                for (int i = 0; i < 3; i++)
                {
                    ss << std::to_string(versionEvent->reserved[i]);
                }

                ss << std::endl;
                ss << "    versionNumber = "  << std::to_string(versionEvent->versionNumber) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }

            iswFwCmdLock.lock();

            if ( waitingActiveSelector )
            {
                std::stringstream ss;
                ss << versionEvent->versionString;
                iswActiveFirmwareVersion.clear();
                iswActiveFirmwareVersion.append(ss.str());
                msgString = "Active FirmwareVersion = ";
                msgString.append(iswActiveFirmwareVersion.c_str());
                waitingActiveSelector = false;
            }
            else
            {
                std::stringstream ss;
                ss << versionEvent->versionString;
                iswStandbyFirmwareVersion.clear();
                iswStandbyFirmwareVersion.append(ss.str());
                msgString = "Standby FirmwareVersion = ";
                msgString.append(iswStandbyFirmwareVersion.c_str());
            }

            iswApiVersion = versionEvent->apiVersion;
            iswVersionNumber = versionEvent->versionNumber;

            iswFwCmdLock.unlock();

            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case LoadImage:
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "LoadImage event";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            receivedImageLoadConfirmation = true;

            if ( ImageLoadLastBlock )
            {
                qDebug() << "Received Last Load Image Confirmation";
                imageLoaded = true;
            }

            break;
        }
        case RunImage:
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "RunImage Event";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            runCmdConfirmed = true;
            break;
        }
        case LoadPublicKeyCert:
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "LoadPublicKeyCert Event";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            publicCertLoaded = true;
            break;
        }
        case LoadRootPublicKey:
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "LoadRootPublicKey Event";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            rootCertLoaded = true;
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "Bad Firmware Event Type: " + std::to_string(cmd);
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! SetupGetFirmwareVersionCmd()
//! Inputs: iswFirmwareCmd - Pointer to the command structure
//!         selector - type - 0 = standby firmware, 1 = active firmware
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswFirmware::SetupGetFirmwareVersionCmd(iswFirmwareCommand *iswFirmwareCmd, uint8_t selector, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupFirmwareVersion";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswFirmwareCmd->cmdType    = IswInterface::Firmware;
    iswFirmwareCmd->command    = GetImageVersion;
    iswFirmwareCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswFirmwareCmd->reserved1  = 0x00;
    iswFirmwareCmd->selector   = 1;   //! 0 = standby, 1 = active
    *iswFirmwareCmd->reserved2 = {0};

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswFirmwareCommand);

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswFirmwareCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswFirmwareCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswFirmwareCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswFirmwareCmd->reserved1) << std::endl;
        ss << "    selector   = " << std::to_string(iswFirmwareCmd->selector) << std::endl;
        ss << "    reserved2  = ";

        for (int i = 0; i < 3; i++)
        {
            ss << std::to_string(iswFirmwareCmd->reserved2[i]);
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetFirmwareVersionCmd()
//! Inputs: selector - 0 = standby firmware, 1 = active firmware
//! Description: Send the Start Association command to the firmware
//!###################################################################
int IswFirmware::SendGetFirmwareVersionCmd(uint8_t selector)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SendGetFirmwareVersion";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    if ( selector == 1 )
    {
        waitingActiveSelector = true;
    }
    else
    {
        waitingActiveSelector = false;
    }

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetFirmwareVersionCmd(&iswCmd.iswFirmwareCmd, selector, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            msgString = "SendGetFirmwareVersion failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupRunImageCmd()
//! Inputs: iswFirmwareCmd - Pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswFirmware::SetupRunImageCmd(iswFirmwareCommand *iswFirmwareCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupRunImageCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswFirmwareCmd->cmdType    = IswInterface::Firmware;
    iswFirmwareCmd->command    = RunImage;
    iswFirmwareCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswFirmwareCmd->reserved1  = 0x00;
    iswFirmwareCmd->selector   = 0;   //! 0 = standby => it's always zero
    *iswFirmwareCmd->reserved2 = {0};

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswFirmwareCommand);

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswFirmwareCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswFirmwareCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswFirmwareCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswFirmwareCmd->reserved1) << std::endl;
        ss << "    selector   = " << std::to_string(iswFirmwareCmd->selector) << std::endl;
        ss << "    reserved2  = ";

        for (int i = 0; i < 3; i++)
        {
            ss << std::to_string(iswFirmwareCmd->reserved2[i]);
        }
        ss << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendRunImageCmd()
//! Inputs: None
//! Description: Send the Run Image command to the firmware
//!###################################################################
int IswFirmware::SendRunImageCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SendRunImageCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupRunImageCmd(&iswCmd.iswFirmwareCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            msgString = "SendRunImageCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! GetFirmwareFileToLoad()
//! Inputs: firmwareFilename - string containing the full path file name
//!         memblock - pointer to a char * buffer
//!         crc - pointer to the the crc
//! Description: Read the file into memory and perform a CRC check on 
//!		 the contents
//!###################################################################
int IswFirmware::GetFirmwareFileToLoad(std::string firmwareFilename, char **memblock, unsigned long *crc)
{
    int length = 0;

    std::ifstream file (firmwareFilename.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (file.is_open())
    {
        //! get length of file:
        file.seekg (0, file.end);
        length = file.tellg();
        file.seekg (0, file.beg);

        *memblock = new char [length];
        file.read (*memblock, length);
        file.close();

        //! Get CRC of data in memory
        *crc = crc32(*crc, (const unsigned char *)*memblock, length);

        std::cout << "Read " << std::to_string(length) << " Bytes From " << firmwareFilename.c_str() << " Into Memory" << std::endl;
    }
    else
    {
      std::cout << "Unable To Open File" << firmwareFilename.c_str() << std::endl;
    }

    return (length);
}

//!###################################################################
//! GetRootPublicKeyToLoad()
//! Inputs: publicRootKeyFilename - string containing the full path file name
//!         memblock - pointer to a char * buffer
//!         crc - pointer to the the crc
//! Description: Read the file into memory and perform a CRC check on
//!		 the contents
//!###################################################################
int IswFirmware::GetRootPublicKeyToLoad(std::string publicRootKeyFilename, char **memblock, unsigned long *crc)
{
    int length = 0;

    std::ifstream file (publicRootKeyFilename.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (file.is_open())
    {
        //! get length of file:
        file.seekg (0, file.end);
        length = file.tellg();
        file.seekg (0, file.beg);

        *memblock = new char [length];
        file.read (*memblock, length);
        file.close();

        //! Get CRC of data in memory
        *crc = crc32(*crc, (const unsigned char *)*memblock, length);

        std::cout << "Read " << std::to_string(length) << " Bytes From " << publicRootKeyFilename.c_str() << " Into Memory" << std::endl;
    }
    else
    {
      std::cout << "Unable to open file" << publicRootKeyFilename.c_str() << std::endl;
    }

    return (length);
}

//!###################################################################
//! GetPublicKeyCertificateToLoad()
//! Inputs: publicKeyCertificateFilename - string containing the full path file name
//!         memblock - pointer to a char * buffer
//!         crc - pointer to the the crc
//! Description: Read the file into memory and perform a CRC check on
//!		 the contents
//!###################################################################
int IswFirmware::GetPublicKeyCertificateToLoad(std::string publicKeyCertificateFilename, char **memblock, unsigned long *crc)
{
    int length = 0;

    std::ifstream file (publicKeyCertificateFilename.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (file.is_open())
    {
        //! get length of file:
        file.seekg (0, file.end);
        length = file.tellg();
        file.seekg (0, file.beg);

        *memblock = new char [length];
        file.read (*memblock, length);
        file.close();

        //! Get CRC of data in memory
        *crc = crc32(*crc, (const unsigned char *)*memblock, length);

        std::cout << "Read " << std::to_string(length) << " Bytes from " << publicKeyCertificateFilename.c_str() << " Into Memory" << std::endl;
    }
    else
    {
      std::cout << "Unable To Open File" << publicKeyCertificateFilename.c_str() << std::endl;
    }

    return (length);
}

//!###################################################################
//! SetupLoadImageCmd()
//! Inputs: iswLoadImageCmd - Pointer to the command structure
//!         lastBlock - 0 = not last, 1 = last, 2 = label
//!         seqNumber - Incrementing block sequence number starting at 0
//!         blockSize - Size in bytes of block sent 256 - 3840
//!                     Set to actual size for last block
//!         dataPtr - pointer to data buffer that gets copied into
//!                   the contiguous memory holding this message
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswFirmware::SetupLoadImageCmd(iswLoadImageCommand *iswLoadImageCmd, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupLoadImageCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Clear data buffer
    memset(iswLoadImageCmd->data, 0, 256); // ToDo Ben: Reduced from 512

    iswLoadImageCmd->cmdType       = IswInterface::Firmware;
    iswLoadImageCmd->command       = LoadImage;
    iswLoadImageCmd->cmdContext    = iswInterface->GetNextCmdCount();
    iswLoadImageCmd->reserved1     = 0x00;
    iswLoadImageCmd->selector      = 0;
    iswLoadImageCmd->lastBlock     = lastBlock;
    iswLoadImageCmd->blockSequence = seqNumber;
    iswLoadImageCmd->reserved2     = 0x0000;
    iswLoadImageCmd->blockSize     = blockSize;
    memcpy(iswLoadImageCmd->data, dataPtr, blockSize);

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;

    //! 12 bytes in message before data
    routingHdrPtr->dataLength = sizeof(iswLoadImageCommand) - sizeof(iswLoadImageCmd->data) + blockSize;

    if ( lastBlock == 1 )
    {
        //! dataLength must be a multiple of 4 bytes
        //! data buffer here is 512 zeroed out above, so just
        //! adjust the dataLength.  ISW Firmware ignores any padded
        //! bytes past blockSize
        if ( routingHdrPtr->dataLength%4 > 0 )
        {
            uint16_t padLength = (4 - (routingHdrPtr->dataLength % 4)) % 4;
            routingHdrPtr->dataLength += padLength;
        }

        std::cout << "sizeof(iswLoadImageCommand)   = " << std::to_string(sizeof(iswLoadImageCommand)) << std::endl;
        std::cout << "sizeof(iswLoadImageCmd->data) = " << std::to_string(sizeof(iswLoadImageCmd->data)) << std::endl;
        std::cout << "blockSize                     = " << std::to_string(blockSize) << std::endl;
        std::cout << "routingHdrPtr->dataLength     = " << std::to_string(routingHdrPtr->dataLength) << std::endl;
    }

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType       = " << std::to_string(iswLoadImageCmd->cmdType) << std::endl;
        ss << "    command       = " << std::to_string(iswLoadImageCmd->command) << std::endl;
        ss << "    cmdContext    = " << std::to_string(iswLoadImageCmd->cmdContext) << std::endl;
        ss << "    reserved1     = " << std::to_string(iswLoadImageCmd->reserved1) << std::endl;
        ss << "    selector      = " << std::to_string(iswLoadImageCmd->selector) << std::endl;
        ss << "    lastBlock     = " << std::to_string(iswLoadImageCmd->lastBlock) << std::endl;
        ss << "    blockSequence = " << std::to_string(iswLoadImageCmd->blockSequence) << std::endl;
        ss << "    reserved2     = " << std::to_string(iswLoadImageCmd->reserved2) << std::endl;
        ss << "    blockSize     = " << std::to_string(iswLoadImageCmd->blockSize) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendLoadImageCmd()
//! Inputs: firmwareFilename - string holding full path file name
//!         label - to apply to this firmware version
//! Description: Send the Run Image command to the firmware
//!###################################################################
int IswFirmware::SendLoadImageCmd(std::string firmwareFilename, std::string signatureFilename, std::string label, uint8_t updateType)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SendLoadImageCmd for ";
        msgString.append(firmwareFilename.c_str());
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;

    imageLoaded = false;

    uint8_t lastBlock  = 0;
    uint16_t seqNumber = 0;
    //!int maxPacketSize = iswInterface->GetUsbInterface()->GetMaxPacketSizeOut();
    //!maxPacketSize - (sizeof(iswLoadImageCommand) + sizeof(IswInterface::iswRoutingHdr) + sizeof(IswInterface::SWIM_Header)) = 488
    //!but blockSize must be a multiple of 256 and less than maxPacketSize (512)
    uint16_t blockSize = 256;

    //! Read firmware file into memory
    char *memblock;
    unsigned long crc = crc32(0L, nullptr, 0);

    int sizeInBytes = GetFirmwareFileToLoad(firmwareFilename, &memblock, &crc);

    if ( sizeInBytes <= 0 )
    {
        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            msgString = "SendLoadImageCmd Failed ";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        if ( memblock != nullptr )
            delete (memblock);

        status = -1;
    }
    else
    {
        int numBlocks = sizeInBytes/blockSize;
        if ( sizeInBytes%blockSize > 0 )
        {
            numBlocks++;
        }

        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            std::stringstream ss;
            ss << "    sizeInBytes = " << std::to_string(sizeInBytes) << std::endl;
            ss << "    numBlocks   = " << std::to_string(numBlocks) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
        std::cout << "  sizeInBytes = " << std::to_string(sizeInBytes) << std::endl;
        std::cout << "  numBlocks   = " << std::to_string(numBlocks) << std::endl;
        std::cout << "  Downloading image now ..." << std::endl;

        uint8_t *dataPtr = (uint8_t *)memblock;
        while ( seqNumber < numBlocks )
        {
            if ( seqNumber == (numBlocks - 1) )
            {
                lastBlock = 1;
                blockSize = sizeInBytes - ((numBlocks-1) * blockSize);

                qDebug() << "Loading last block of file, lastBlock = " << QString::number(lastBlock);

                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    std::stringstream ss;
                    ss << "LastBlock = 1:" << std::endl;
                    ss << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
                    ss << "    blockSize = " << std::to_string(blockSize) << std::endl;
                    ss << "    lastBlock = " << std::to_string(lastBlock) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << "LastBlock = 1:" << std::endl;
                std::cout << "  seqNumber = " << std::to_string(seqNumber) << std::endl;
                std::cout << "  blockSize = " << std::to_string(blockSize) << std::endl;
                std::cout << "  lastBlock = " << std::to_string(lastBlock) << std::endl;

            }

            SetupLoadImageCmd(&iswCmd.iswLoadImageCmd, lastBlock, seqNumber, blockSize, dataPtr, &routingHdr);
            receivedImageLoadConfirmation = false;
            status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

            if ( status < 0 )
            {
                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    msgString = "SendLoadImageCmd Failed";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                return ( status );
            }


            int count = 0;
            while ( !receivedImageLoadConfirmation && count< 1000 )
            {
                usleep(100);
                count++;
            }

            if ( !receivedImageLoadConfirmation )
            {
                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    std::stringstream ss;
                    ss << "Block Not Confirmed, seqNumber = " << std::to_string(seqNumber) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << "Block Not Confirmed, seqNumber = " << std::to_string(seqNumber) << std::endl;

                status = -1;
                return ( status );
            }

            //! Update and loop again
            dataPtr += blockSize;
            seqNumber++;
        }

        //! Format CRC and label command
        lastBlock = 2;

        // Legacy Update
        if (updateType == 0)
        {
            blockSize = sizeof(IswFirmware::iswImageCrcAndLabel);

            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                std::stringstream ss;
                ss << "CRC and Label: " << std::endl;
                ss << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
                ss << "    blockSize = " << std::to_string(blockSize) << std::endl;
                ss << "    lastBlock = " << std::to_string(lastBlock) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }

            std::cout << "CRC and Label: " << std::endl;
            std::cout << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
            std::cout << "    blockSize = " << std::to_string(blockSize) << std::endl;
            std::cout << "    lastBlock = " << std::to_string(lastBlock) << std::endl;

            qDebug() << "crc size:" << sizeof(crc);
            qDebug() << "crc:" << crc;

            IswFirmware::iswImageCrcAndLabel crc_label;
            crc_label.crc[0] = (uint8_t)((crc >> 0) & 0xFF);
            crc_label.crc[1] = (uint8_t)((crc >> 8) & 0xFF);
            crc_label.crc[2] = (uint8_t)((crc >> 16) & 0xFF);
            crc_label.crc[3] = (uint8_t)((crc >> 24) & 0xFF);


            sprintf((char *)&crc_label.label, "%s", label.c_str());

            //! Pad label with zeros to 32 bytes
            if ( label.size() < 32 )
            {
                for (int i = label.size(); i < 32; i++)
                {
                    crc_label.label[i] = 0;
                }
             }

            dataPtr = (uint8_t *)&crc_label;
            status = SendLastLoadImageCmd(lastBlock, seqNumber, blockSize, dataPtr);

        } // ECDSA Update = 1
        else if (updateType == 1)
        {
            blockSize = sizeof(IswFirmware::iswImageCrcAndLabelAndSignature);

            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                std::stringstream ss;
                ss << "CRC and Label and Signature: " << std::endl;
                ss << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
                ss << "    blockSize = " << std::to_string(blockSize) << std::endl;
                ss << "    lastBlock = " << std::to_string(lastBlock) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }

            std::cout << "CRC and Label and Signature: " << std::endl;
            std::cout << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
            std::cout << "    blockSize = " << std::to_string(blockSize) << std::endl;
            std::cout << "    lastBlock = " << std::to_string(lastBlock) << std::endl;

            IswFirmware::iswImageCrcAndLabelAndSignature crc_label_signature;
            crc_label_signature.crc[0] = (uint8_t)((crc >> 0) & 0xFF);
            crc_label_signature.crc[1] = (uint8_t)((crc >> 8) & 0xFF);
            crc_label_signature.crc[2] = (uint8_t)((crc >> 16) & 0xFF);
            crc_label_signature.crc[3] = (uint8_t)((crc >> 24) & 0xFF);

            sprintf((char *)&crc_label_signature.label, "%s", label.c_str());

            //! Pad label with zeros to 32 bytes
            if ( label.size() < 32 )
            {
                for (int i = label.size(); i < 32; i++)
                {
                    crc_label_signature.label[i] = 0;
                }
             }

            //! Read firmware file into memory
            uint8_t sig_buffer[64] = {0};
            std::ifstream file (signatureFilename.c_str(), std::ios::binary);
            if (file.is_open())
            {
                file.read(reinterpret_cast<char *>(sig_buffer), 64);
                std::cout << "Read " << 64 << " Bytes From " << signatureFilename.c_str() << " Into Memory" << std::endl;
            }
            else
            {
                qDebug() << "Can't open .sig file";
            }

            file.close();

            //std::cout << "Read bytes:" << std::endl;
                for (size_t i = 0; i < sizeof(sig_buffer); ++i) {
                    //printf("%02X ", sig_buffer[i]);
                    //if ((i + 1) % 16 == 0) std::cout << std::endl;
                    crc_label_signature.signature[i] = sig_buffer[i];
                }

            // All the data for the last load image command, 4 bytes crc, 32 bytes label, 64 bytes ecdsa signature.
            uint8_t* structBytesPtr = (uint8_t *)&crc_label_signature;

            status = SendLastLoadImageCmd(lastBlock, seqNumber, blockSize, structBytesPtr);

        }

        if ( status < 0 )
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "SendLoadImageCmd Failed";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }

        if ( memblock != nullptr )
            delete (memblock);
    }

    return ( status );
}

void IswFirmware::SetupLastLoadImageCmd(iswLastLoadImageCommand *iswLastLoadImageCmd, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupLastLoadImageCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Clear data buffer
    memset(iswLastLoadImageCmd->data, 0, 100);

    iswLastLoadImageCmd->cmdType       = IswInterface::Firmware;
    iswLastLoadImageCmd->command       = LoadImage;
    iswLastLoadImageCmd->cmdContext    = iswInterface->GetNextCmdCount();
    iswLastLoadImageCmd->reserved1     = 0x00;
    iswLastLoadImageCmd->selector      = 0;
    iswLastLoadImageCmd->lastBlock     = lastBlock;
    iswLastLoadImageCmd->blockSequence = seqNumber;
    iswLastLoadImageCmd->reserved2     = 0x0000;
    iswLastLoadImageCmd->blockSize     = blockSize; // Need to set this, 4 bytes crc, 32 bytes label, 64 bytes ecdsa if not legacy, total 36 or 100
    memcpy(iswLastLoadImageCmd->data, dataPtr, blockSize);

    // Need ECSDA signature for ECDSA update, not for legacy.

    QString dataStr = "";
    for (int i = 0; i < 100; i++)
    {
        dataStr += QString::number(iswLastLoadImageCmd->data[i]) + " ";
    }

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;

    //! 12 bytes in message before data
    routingHdrPtr->dataLength = sizeof(iswLastLoadImageCommand) - sizeof(iswLastLoadImageCmd->data) + blockSize;

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType       = " << std::to_string(iswLastLoadImageCmd->cmdType) << std::endl;
        ss << "    command       = " << std::to_string(iswLastLoadImageCmd->command) << std::endl;
        ss << "    cmdContext    = " << std::to_string(iswLastLoadImageCmd->cmdContext) << std::endl;
        ss << "    reserved1     = " << std::to_string(iswLastLoadImageCmd->reserved1) << std::endl;
        ss << "    selector      = " << std::to_string(iswLastLoadImageCmd->selector) << std::endl;
        ss << "    lastBlock     = " << std::to_string(iswLastLoadImageCmd->lastBlock) << std::endl;
        ss << "    blockSequence = " << std::to_string(iswLastLoadImageCmd->blockSequence) << std::endl;
        ss << "    reserved2     = " << std::to_string(iswLastLoadImageCmd->reserved2) << std::endl;
        ss << "    blockSize     = " << std::to_string(iswLastLoadImageCmd->blockSize) << std::endl;
        //ss << "    data           = " << std::to_string(iswLastLoadImageCmd->data) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

int IswFirmware::SendLastLoadImageCmd(uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr)
{
    int status = -1;

    if (lastBlock == 2)
    {
        ImageLoadLastBlock = true;
        std::string msgString;
        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            msgString = "SendLastLoadImageCmd";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            msgString.clear();
        }
        status = 0;
        imageLoaded = false;

        IswInterface::iswCommand iswCmd;
        IswInterface::iswRoutingHdr routingHdr;

        SetupLastLoadImageCmd(&iswCmd.iswLoadLastImageCmd, lastBlock, seqNumber, blockSize, dataPtr, &routingHdr);

        receivedImageLoadConfirmation = false;
        status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

        if ( status < 0 )
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "SendLastLoadImageCmd failed";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            qDebug() << "LastLoadImageCmd Failed";
            return ( status );
        }

        int count = 0;
        while (!receivedImageLoadConfirmation && count < 25 )
        {
            sleep(5);
            count++;
        }

        if ( !receivedImageLoadConfirmation )
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                std::stringstream ss;
                ss << "Last Load Image Failed, seqNumber = " << std::to_string(seqNumber) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }

            qDebug() << "Last Load Image Failed";

            status = -1;
            return ( status );
        }
        else
        {
            qDebug() << "got load image confirmation in send last load image.";
        }

    }
    else
    {
        qDebug() << "Incorrect Place In Sequence to execute Last Load Image command. lastBlock should be 2, it is: " << QString::number(lastBlock);
    }

    return ( status );
}

void IswFirmware::SetupLoadPublicKeyCertificateCmd(iswLoadPublicKeyCertCommand *iswLoadPublicKeyCertCmd, uint8_t certificate, uint8_t lastBlock, uint16_t seqNumber, uint16_t blockSize, uint8_t *dataPtr, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupLoadPublicKeyCertificateCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    //! Clear data buffer
    memset(iswLoadPublicKeyCertCmd->data, 0, 256); // ToDo Ben: Originally 512, account for other bytes below, and swim and routing header.

    iswLoadPublicKeyCertCmd->cmdType         = IswInterface::Firmware;
    iswLoadPublicKeyCertCmd->command         = LoadPublicKeyCert;
    iswLoadPublicKeyCertCmd->cmdContext      = iswInterface->GetNextCmdCount();
    iswLoadPublicKeyCertCmd->reserved1       = 0x00;
    iswLoadPublicKeyCertCmd->certificateType = certificate;
    iswLoadPublicKeyCertCmd->lastBlock       = lastBlock;
    iswLoadPublicKeyCertCmd->blockSequence   = seqNumber;
    iswLoadPublicKeyCertCmd->reserved2       = 0x00;
    iswLoadPublicKeyCertCmd->blockSize       = blockSize;
    memcpy(iswLoadPublicKeyCertCmd->data, dataPtr, blockSize);

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswLoadPublicKeyCertCommand);

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType         = " << std::to_string(iswLoadPublicKeyCertCmd->cmdType) << std::endl;
        ss << "    command         = " << std::to_string(iswLoadPublicKeyCertCmd->command) << std::endl;
        ss << "    cmdContext      = " << std::to_string(iswLoadPublicKeyCertCmd->cmdContext) << std::endl;
        ss << "    reserved1       = " << std::to_string(iswLoadPublicKeyCertCmd->reserved1) << std::endl;
        ss << "    certificateType = " << std::to_string(iswLoadPublicKeyCertCmd->certificateType) << std::endl;
        ss << "    lastBlock       = " << std::to_string(iswLoadPublicKeyCertCmd->lastBlock) << std::endl;
        ss << "    blockSequence   = " << std::to_string(iswLoadPublicKeyCertCmd->blockSequence) << std::endl;
        ss << "    reserved2       = " << std::to_string(iswLoadPublicKeyCertCmd->reserved2) << std::endl;
        ss << "    blockSize       = " << std::to_string(iswLoadPublicKeyCertCmd->blockSize) << std::endl;
//        ss << "    data= " << std::to_string(iswLoadPublicKeyCertCmd->data) << std::endl;

        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

int IswFirmware::SendLoadPublicKeyCertificateCmd(std::string publicKeyCertificateFilename, uint8_t certificate, std::string label)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SendLoadPublicKeyCertificateCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;

    uint8_t lastBlock  = 0;
    uint16_t seqNumber = 0;

    //!int maxPacketSize = iswInterface->GetUsbInterface()->GetMaxPacketSizeOut();
    //!maxPacketSize - (sizeof(iswLoadImageCommand) + sizeof(IswInterface::iswRoutingHdr) + sizeof(IswInterface::SWIM_Header)) = 488
    //!but blockSize must be a multiple of 256 and less than maxPacketSize (512)
    uint16_t blockSize = 256;

    //! Read firmware file into memory
    char *memblock;
    unsigned long crc = crc32(0L, nullptr, 0);

    int sizeInBytes = GetFirmwareFileToLoad(publicKeyCertificateFilename, &memblock, &crc);

    if ( sizeInBytes <= 0 )
    {
        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            msgString = "SendLoadPublicKeyCertificateCmd Failed ";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }

        if ( memblock != nullptr )
            delete (memblock);

        status = -1;
    }
    else
    {
        int numBlocks = sizeInBytes/blockSize;
        if ( sizeInBytes%blockSize > 0 )
        {
            numBlocks++;
        }

        if ( theLogger->DEBUG_IswFirmware == 1 )
        {
            std::stringstream ss;
            ss << "    sizeInBytes = " << std::to_string(sizeInBytes) << std::endl;
            ss << "    numBlocks   = " << std::to_string(numBlocks) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
        std::cout << "  sizeInBytes = " << std::to_string(sizeInBytes) << std::endl;
        std::cout << "  numBlocks   = " << std::to_string(numBlocks) << std::endl;
        std::cout << "  Downloading image now ..." << std::endl;

        uint8_t *dataPtr = (uint8_t *)memblock;
        while ( seqNumber < numBlocks )
        {
            if ( seqNumber == (numBlocks - 1) )
            {
                lastBlock = 1;
                blockSize = sizeInBytes - ((numBlocks-1) * blockSize);

                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    std::stringstream ss;
                    ss << "LastBlock = 1:" << std::endl;
                    ss << "    seqNumber = " << std::to_string(seqNumber) << std::endl;
                    ss << "    blockSize = " << std::to_string(blockSize) << std::endl;
                    ss << "    lastBlock = " << std::to_string(lastBlock) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << "LastBlock = 1:" << std::endl;
                std::cout << "  seqNumber = " << std::to_string(seqNumber) << std::endl;
                std::cout << "  blockSize = " << std::to_string(blockSize) << std::endl;
                std::cout << "  lastBlock = " << std::to_string(lastBlock) << std::endl;

            }
            SetupLoadPublicKeyCertificateCmd(&iswCmd.iswLoadPublicKeyCertCmd, certificate, lastBlock, seqNumber, blockSize, dataPtr, &routingHdr);
            std::cout << "calling sync send for load public key command" << std::endl;
            publicCertLoaded = false;
            status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);
            std::cout << "sync send status: " << status;
            sleep(1);

            int count = 0;
            while ( !publicCertLoaded && count< 1000 )
            {
                usleep(100);
                count++;
            }

            if ( status < 0 )
            {
                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    msgString = "SendLoadPublicKeyCertificateCmd failed";
                    theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                }
                return ( status );
            }

            if ( !publicCertLoaded )
            {
                if ( theLogger->DEBUG_IswFirmware == 1 )
                {
                    std::stringstream ss;
                    ss << "Block not confirmed, seqNumber = " << std::to_string(seqNumber) << std::endl;
                    theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                }
                std::cout << "Block not confirmed, seqNumber = " << std::to_string(seqNumber) << std::endl;

                status = -1;
                return ( status );
            }

            //! Update and loop again
            dataPtr += blockSize;
            seqNumber++;
        }

        if ( status < 0 )
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "SendLoadPublicKeyCertificateCmd Failed";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }
        else
        {
            std::cout << "Sent Last Block" << std::endl;
            usleep(100);

        }

        if ( memblock != nullptr )
            delete (memblock);
    }

    return ( status );
}

void IswFirmware::SetupLoadRootPublicKeyCmd(iswLoadRootPublicKeyCommand *iswLoadRootPublicKeyCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SetupLoadRootPublicKeyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswLoadRootPublicKeyCmd->cmdType    = IswInterface::Firmware;
    iswLoadRootPublicKeyCmd->command    = LoadRootPublicKey;
    iswLoadRootPublicKeyCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswLoadRootPublicKeyCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswLoadRootPublicKeyCommand);

    qDebug() << "size of load root public key: " << routingHdrPtr->dataLength;

    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswLoadRootPublicKeyCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswLoadRootPublicKeyCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswLoadRootPublicKeyCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswLoadRootPublicKeyCmd->reserved1) << std::endl;

        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

int IswFirmware::SendLoadRootPublicKeyCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswFirmware == 1 )
    {
        msgString = "SendLoadRootPublicKeyCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupLoadRootPublicKeyCmd(&iswCmd.iswLoadRootKeyCmd, &routingHdr);
    qDebug() << "setup load root public key";
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);
    qDebug() << "sync send status: " << status;

        if ( status < 0 )
        {
            if ( theLogger->DEBUG_IswFirmware == 1 )
            {
                msgString = "SendLoadPublicKeyCertificateCmd Failed";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
        }

    return ( status );
}
