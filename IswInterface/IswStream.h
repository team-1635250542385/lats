//!###############################################
//! Filename: IswStream.h
//! Description: Class that provides an interface
//!              to ISW Stream commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWSTREAM_H
#define ISWSTREAM_H

#include <mutex>
#include "../Logger/Logger.h"

#ifdef TESTING
//! For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterface;

class IswStream
{

public:
    IswStream(IswInterface *isw_interface, Logger *logger);
    ~IswStream() {}

    //! Logger object pointer passed into constructor
    Logger *theLogger = nullptr;

    //! ISW Stream command structures
    struct iswStreamCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
    }__attribute__((packed));

    struct iswExtendedStreamCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t version;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswSetStreamSpecCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t peerIndex;
        uint8_t subStreamTxPriority;
        uint8_t subStreamTxDiscardable;
        uint8_t reserved2;
        uint32_t maxTxLatency;
    }__attribute__((packed));

    struct iswGetStreamSpecCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t peerIndex;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswGetPeerFirmwareVersionCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t peerIndex;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswStartMulticastGroupCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t phyRate;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswJoinMulticastGroupCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint16_t mcastAddress;
        uint8_t mcastPhyRate;
        uint8_t reserved2;
    }__attribute__((packed));

    struct iswLeaveMulticastGroupCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t mcastPeerIndex;
        uint8_t reserved2[3];
    }__attribute__((packed));

    struct iswModifyMulticastGroupCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved1;
        uint8_t mcastPeerIndex;
        uint8_t mcastPhyRate;
        uint16_t reserved2;
    }__attribute__((packed));

    //! Returns the index into the iswInterfaceArray for the current
    //! ISW device
    uint8_t GetIndex(void);

    //! These methods return class variables to the application
    //! The values of each is filled when an ISW command gets a
    //! return from the firmware
    uint8_t GetIswChannel(void)  { return (iswChannel); }
    uint8_t GetIswRole(void)  { return (iswRole); }
    uint8_t GetIswLinkQuality(void) { return (iswLinkQuality); }
    uint8_t GetIswPeerIndex(void) { return (iswPeerIndex); }
    uint8_t GetIswHibernating(void) { return (iswHibernating); }
    uint16_t GetIswDuration(void) { return (iswDuration); }

    //! Each ISW class has this method.  When IswInterface receives
    //! data it passes it off to the correct friend class by calling
    //! DeliverIswMessage() for that object
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Send ISW commands to the firmware
    int SendGetStreamsStatusCmd(void);
    int SendGetExtendedStreamsStatusCmd(uint8_t version);
    int SendSetStreamsSpecCmd(uint8_t peerIndex, uint8_t txPriority, uint8_t txDiscard, uint32_t maxTxLatency);
    int SendGetStreamSpecCmd(uint8_t peerIndex);
    int SendGetMulticastGroupsStatusCmd(void);
    int SendGetPeerFirmwareVersionCmd(uint8_t peerIndex);
    int SendStartMulticastGroupCmd(uint8_t phyRate);
    int SendJoinMulticastGroupCmd(uint16_t mcastAddress, uint8_t phyRate);
    int SendLeaveMulticastGroupCmd(uint8_t mcastPeerIndex);
    int SendModifyMulticastGroupCmd(uint8_t mcastPeerIndex, uint8_t phyRate);

    //! For debugging
    void PrintPeerRecords(void);

ACCESS:
    //! ISW Stream command structures
    struct iswPeerRecord_Version1
    {
        uint8_t peerIndex;
        uint8_t peerState;
        uint16_t deviceType;
        uint8_t linkStatus;
        uint8_t linkQuality;
        uint16_t txQueueDepth;
    }__attribute__((packed));

    struct iswPeerRecord_Version2
    {
        uint8_t peerIndex;
        uint8_t peerState;
        uint16_t deviceType;
        uint8_t linkStatus;
        uint8_t linkQuality;
        uint16_t txQueueDepth;
        uint8_t macAddress[6];
        uint16_t reserved2;
    }__attribute__((packed));

    struct iswPeerRecord_Version3
    {
        uint8_t peerIndex;
        uint8_t peerState;
        uint16_t deviceType;
        uint8_t linkStatus;
        uint8_t linkQuality;
        uint16_t txQueueDepth;
        uint8_t macAddress[6];
        uint16_t throughput;
        uint16_t transferSize;
        uint8_t running;
        uint16_t reserved2;
    }__attribute__((packed));

    struct iswPeerRecord_Version4
    {
        uint8_t peerIndex;
        uint8_t peerState;
        uint16_t deviceType;
        uint8_t linkStatus;
        uint8_t linkQuality;
        uint16_t txQueueDepth;
        uint8_t macAddress[6];
        uint16_t txThroughput;
        uint16_t rxThroughput;
        uint8_t modePhyRate;
        uint8_t averageRSSI;
    }__attribute__((packed));

#define MAX_PEERS 14
    std::mutex iswPeerRecordLock;
    void ParsePeerRecords(uint8_t peerVersion, uint8_t *dataPtr);

    struct iswEventStreamsConnectionInd // FW 30810 or Later
    {
        uint8_t peerIndex;
        uint8_t linkStatus;
        uint16_t deviceType;
        uint8_t macAddress[6];
        uint16_t reserved;
    }__attribute__((packed));

    struct iswEventStreamsHibernationInd // FW 30810 or Later
    {
        uint8_t peerIndex;
        uint8_t hibernating;
        uint16_t duration;
    }__attribute__((packed));

    void ParseConnectionIndication(iswEventStreamsConnectionInd *streamsConnectionInd);
    void ParseHibernationIndication(iswEventStreamsHibernationInd *streamsHibernationInd); // FW 30810 or Later

    struct peerMetrics
    {
        uint8_t phyRate;
        uint8_t rssi;
        uint16_t transmitThroughput;
        uint16_t receiveThroughput;
    }; // Doesn't need to be packed - it's only used locally within our system

public:

    struct iswPeerRecord
    {
        uint8_t recordVersion                 = 0;
        bool inUse                            = false;         //! Peer Record is being actively used
        bool isConnected                      = false;
        uint64_t connectionTimestampMicrosecs = 0;
        uint8_t securityKey[16];        
        union
        {
            iswPeerRecord_Version1 iswPeerRecord1;
            iswPeerRecord_Version2 iswPeerRecord2;
            iswPeerRecord_Version3 iswPeerRecord3;
            iswPeerRecord_Version4 iswPeerRecord4;
            peerMetrics metrics;
        };
    };

    //! User methods
    void PeerRecordLock(void) { iswPeerRecordLock.lock(); }
    void PeerRecordUnlock(void) { iswPeerRecordLock.unlock(); }

    //! This one returns a copy of the peer records - read only
    void GetPeers(std::array<iswPeerRecord, MAX_PEERS> *peers);

    static const uint8_t ROLE_COORDINATOR = 0x00;
    static const uint8_t ROLE_PEER        = 0x01;
    static const uint8_t CONNECTED        = 0x01;
    static const uint8_t DISCONNECTED     = 0x00;

ACCESS:
    std::array<iswPeerRecord, MAX_PEERS> iswPeers;

public:
    //! This one returns a reference to the peer records - can update values
    std::array<iswPeerRecord, MAX_PEERS>& GetPeerRef(void) { return (iswPeers); }
    void InitIswPeerRecords(void);

ACCESS:
    //! Pointer to the IswInterface object for this device
    IswInterface *iswInterface = nullptr;

//#define HIBERNATING_MASK 0x04  //! Hibernating Mask
//#define ROLE_MASK 0x01         //! Role Mask

#define PEER_STATE_MASK 0x05

    //! For multi-threaded access
    std::mutex iswStreamCmdLock;

    //! Class variables
    uint8_t iswPeerRecordVersion = 0;
    uint8_t iswChannel           = 0;
    uint8_t iswRole              = 0; //! 0 = Coordinator, 1 = Non-Coordinator (peer)
    uint8_t iswLinkQuality       = 0;
    uint8_t iswPeerIndex         = 0;
    uint8_t iswHibernating       = 0x00;
    uint16_t iswDuration         = 0;

    //! ISW Stream commands and events
    enum iswStreamCmdEventTypes: uint8_t
    {
        ReservedStreamCmd                   = 0x0,
        SetStreamsSpec                      = 0x01,
        GetStreamsSpec                      = 0x02,
        GetStreamsStatus                    = 0x03,
        GetExtendedStreamsStatus            = 0x04,
        StartMulticastGroup                 = 0x05,
        JoinMulticastGroup                  = 0x06,
        LeaveMulticastGroup                 = 0x07,
        ModifyMulticastGroup                = 0x08,
        GetMulticastGroupStatus             = 0x09,
        //! Reserved Gap
        GetPeerFirmwareVersion              = 0x21,
        //! Reserved Gap
        ConnectionIndication                = 0x81,
        StreamsStatusIndication             = 0x82,
        MulticastActivityIndication         = 0x83,
        MulticastGroupsStatusIndication     = 0x84,
        HibernationIndication               = 0x85
    };

    //! ISW Stream command structures
    struct iswEventStreamsStatus
    {
        uint8_t numPeers;
        uint8_t peerVersion;
        uint8_t channel;
        uint8_t role;
    }__attribute__((packed));

    struct iswEventGetStreamSpec
    {
        uint8_t peerIndex;
        uint8_t subStreamTxPriority;
        uint8_t subStreamTxDiscardable;
        uint8_t reserved2;
        uint32_t maxTxLatency;
    }__attribute__((packed));

    struct iswMulticastGroupRecord
    {
        uint8_t mcastPeerIndex;
        uint8_t mcastStatus;
        uint16_t mcastAddress;
        uint8_t ownerPeerIndex;
        uint8_t mcastPhyRate;
        uint16_t txQueueDepth;
    }__attribute__((packed));

    struct iswEventMulticastGroupStatus
    {
        uint8_t numMulticastRecords;
        uint8_t reserved[3];
    }__attribute__((packed));

    struct iswEventMulticastActivity
    {
        uint8_t mcastPeerIndex;
        uint8_t activityType;
        uint16_t mcastAddress;
    }__attribute__((packed));

    struct iswEventStartMulticastGroup
    {
        uint8_t mcastPeerIndex;
        uint8_t mcastPhyRate;
        uint16_t mcastAddress;
    }__attribute__((packed));

    struct iswEventJoinMulticastGroup
    {
        uint8_t mcastPeerIndex;
        uint8_t mcastPhyRate;
        uint16_t mcastAddress;
    }__attribute__((packed));

    struct iswEventLeaveMulticastGroup
    {
        uint8_t mcastPeerIndex;
        uint8_t reserved[3];
    }__attribute__((packed));

    struct iswEventModifyMulticastGroup
    {
        uint8_t mcastPeerIndex;
        uint8_t mcastPhyRate;
        uint8_t reserved;
    }__attribute__((packed));

    //! Methods to parse peer and multicast records that
    //! are retrieved from ISW Stream messages that come
    //! periodically - see ISW Embedment Guide
#define MAX_MCAST_RECORDS 64  //! This is the number of multicast peer index's
    std::mutex iswMcastRecordLock;
    void McastRecordLock(void) { iswMcastRecordLock.lock(); }
    void McastRecordUnlock(void) { iswMcastRecordLock.unlock(); }

    void ParseMulticastGroupRecord(iswMulticastGroupRecord *dataPtr);
    void UpdateMulticastGroupRecord(iswEventMulticastActivity *mcastActivity);

    //! These functions fill the ISW Header for each send command
    //! to the firmware
    void SetupGetMulticastGroupsStatusCmd(iswStreamCommand *iswStreamCmd, void *routingHdr);

    void SetupGetStreamsStatusCmd(iswStreamCommand *iswStreamCmd, void *routingHdr);
    void SetupGetExtendedStreamsStatusCmd(iswExtendedStreamCommand *iswExtendedStreamCmd, uint8_t version, void *routingHdr);
    void SetupSetStreamsSpecCmd(iswSetStreamSpecCommand *iswSetStreamSpecCmd, uint8_t peerIndex, uint8_t txPriority,
                                uint8_t txDiscard, uint32_t maxTxLatency, void *routingHdr);
    void SetupGetStreamsSpecCmd(iswGetStreamSpecCommand *iswGetStreamSpecCmd, uint8_t peerIndex, void *routingHdr);
    void SetupGetPeerFirmwareVersionCmd(iswGetPeerFirmwareVersionCommand *getPeerFirmwareVersionCmd, uint8_t peerIndex, void *routingHdr);
    void SetupStartMulticastGroupCmd(iswStartMulticastGroupCommand *iswStartMulticastGroupCmd, uint8_t phyRate, void *routingHdr);
    void SetupJoinMulticastGroupCmd(iswJoinMulticastGroupCommand *joinMulticastGroupCmd, uint16_t mcastAddress, uint8_t phyRate, void *routingHdr);
    void SetupLeaveMulticastGroupCmd(iswLeaveMulticastGroupCommand *leaveMulticastGroupCmd, uint8_t mcastPeerIndex, void *routingHdr);
    void SetupModifyMulticastGroupCmd(iswModifyMulticastGroupCommand *modifyMulticastGroupCmd, uint8_t mcastPeerIndex, uint8_t phyRate, void *routingHdr);

public:
    //! Used to retrieve multicast group data
#define MCAST_MY_OWNER_PEER_INDEX  0x00

    struct multicastGroupRecords
    {
        bool inUse = false;
        iswMulticastGroupRecord iswMcastRecord;
    }; // Doesn't need to be packed - it's only used locally within our system

    enum iswMulticastStatusTypes: uint8_t
    {
        McastGroupOriginatedAndPaused     = 0,
        McastGroupOriginatedAndActive     = 1,
        McastGroupAvailableToJoin         = 2,
        McastGroupJoined                  = 3,
        McastGroupLeft                    = 4
    };

ACCESS:
    std::array<multicastGroupRecords, MAX_MCAST_RECORDS> iswMulticastGroupRecords;

public:
    //! Return a refernce to the Isw library table - for internal ISW lib use
    std::array<multicastGroupRecords, MAX_MCAST_RECORDS>& GetMulticastGroupRecordsRef(void) { return (iswMulticastGroupRecords); }
    //! Return a copy of the internal ISW lib table to the caller's passed in array
    void GetMulticastGroupRecords(std::array<multicastGroupRecords, MAX_MCAST_RECORDS> *mcastGroupRecords);
    void InitIswMulticastRecords(void);

};

#endif //! ISWSTREAM_H
