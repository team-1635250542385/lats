//!###############################################
//! Filename: IswIdentity.cpp
//! Description: Class that provides an interface
//!              to ISW Identity commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswIdentity.h"
#include "IswInterface.h"
#include <sstream>
#include <iomanip>

#include <QDebug>

//!###############################################################
//! Constructor: IswIdentity()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswIdentity::IswIdentity(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }

}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswIdentity::GetIndex(void)
{
    return (iswInterface->GetIndex());
}


//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswIdentity::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;


    switch (cmd)
    {
        case SetDeviceType:
        {

            qDebug() << "delivering set dev type message";
            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString = "Received SetDeviceType Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            break;
        }
        case GetDeviceType:
        {
            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString = "Received GetDeviceType ";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }


            //qDebug() << "\ndata: " << data;
            iswDeviceRole = *data;

            //qDebug() << "untouched data: " << data;
            //qDebug() << "iswDeviceRole: " << iswDeviceRole;

            uint16_t devType = 0x00FF & data[3];


            //qDebug() << "get dev type before parse: " << devType << ", " << iswDeviceType;


            devType          = devType << 8;
            //qDebug() << "after first parse: " << devType;


//            for(int i = 0; i < 47; i++)
//            {
//                qDebug() << "data char" << data[i];
//            }

            //qDebug() << "data piece: " << data[2]; // This piece maxes at 21, needs to go up to 24

            // manually overwrite data[2] for testing
            //data[2] = 22;
            //qDebug() << "overwritten data[2]" << data[2];
            devType         += data[2];

            //qDebug() << "\nget dev type: " << devType << ", " << iswDeviceType;


            iswDeviceType    = devType;

            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                std::stringstream ss;
                msgString.clear();
                ss << std::setfill('0');
                ss << "  deviceRole = " << std::to_string(iswDeviceRole) << std::endl;
                ss << "  deviceType = ";

                for (int i = 0; i < 2; i++)
                {
                    ss <<  std::hex << std::setw(2) << (int)data[3-i];
                }
                msgString.append(ss.str());
                theLogger->DebugMessage(msgString, iswInterface->GetIndex());
            }

            break;
        }
        case GetMACAddress:
        {
            std::stringstream macAddress;
            msgString.append("Received MacAddress = ");

            for (int i = 0; i < 6; i++)
            {
                //Get it as a string
                macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)data[i];
                if ( i < 5 )
                {
                    macAddress << ":";
                }
                //Get the data
                iswMacAddress[i] = data[i];
            }

            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString.append(macAddress.str());
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            iswMacAddressStr.clear();
            iswMacAddressStr.append(macAddress.str());
            break;
        }
        case SetCoordinator:
        {
            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString = "Received SetCoordinator Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case GetCoordinator:
        {
            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString = "Received GetCoordinator = ";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            //! Update class params
            IswIdentityCmdLock.lock();
            iswCoordinatorCapable    = data[0];
            iswCoordinatorPrecedence = data[1];
            IswIdentityCmdLock.unlock();

            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                std::stringstream ss;
                ss << "  Coordinator Capable    = " << std::to_string(iswCoordinatorCapable) << std::endl;
                ss << "  Coordinator Precedence = " << std::to_string(iswCoordinatorPrecedence) << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswIdentity == 1 )
            {
                msgString = "Bad Identity Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###############################################################
//! GetDeviceTypeDefaults()
//! Inputs: devType - type of ISW device
//!         coordinatorCapable - pointer to coordinatorCabable
//!                              variableto update
//!         coordinatorPrecedence - pointer to coordinatorPrecedence
//!                                 variable to update
//! Description: ISW devices have default settings per the ISW
//!              Embedment Guide. This returns  the defaults per
//!              device type.
//!###############################################################
void IswIdentity::GetDeviceTypeDefaults(uint16_t devType, uint8_t *coordinatorCapable, uint8_t *coordinatorPrecedence)
{
        std::string msgString;
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "GetDeviceTypeDefaults";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
        std::stringstream ss;
        ss << "  For Device Type = ";

        //! Log device type
        //! Set coordinator capable defaults base on ISW Embedment Guide Table 14-2
        switch (devType)
        {
            case iswDeviceTypes::TWS:
            {
                ss << " Thermal Weapon Sight" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::HMD:
            {
                ss << " Helmet Mounted Display" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::PHN:
            {
                ss << " Smartphone/computer" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::LRF:
            {
                ss << " Laser Range Finder" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::GPS:
            {
                ss << " GPS Device" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::RLH:
            {
                ss << " ISW-Mac Relay, USB Host Connected" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::RLD:
            {
                ss << " ISW-Mac Relay, USB Device Connected" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::DPT:
            {
                ss << " Depot Device" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::NET:
            {
                ss << " ISW-Mac Network Device" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::ENC:
            {
                ss << " Video Encoder (camera)" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::KPD:
            {
                ss << " Keypad" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::NES:
            {
               ss << " ISW-Mac Network Device – Slave Only" << std::endl;

               *coordinatorCapable    = 0;
               *coordinatorPrecedence = 0; // 5
               break;
            }
            case iswDeviceTypes::HTR:
            {
                ss << " Handheld Tactical Radio" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::CHD:
            {
                ss << " Command Mode Head Mounted Device" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::GNM:
            {
                ss << " Generic Coordinator-Capable Device" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::GNS:
            {
                ss << " Generic Peer Device (Non-Coordinator)" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::NET1:
            {
                ss << " General Network Node Priority 1" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 1;
                break;
            }
            case iswDeviceTypes::NET2:
            {
                ss << " General Network Node Priority 2" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 2;
                break;
            }
            case iswDeviceTypes::NET3:
            {
                ss << " General Network Node Priority 3" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 3;
                break;
            }
            case iswDeviceTypes::NET4:
            {
                ss << " General Network Node Priority 4" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 4;
                break;
            }
            case iswDeviceTypes::NET5:
            {
                ss << " General Network Node Priority 5" << std::endl;

                *coordinatorCapable    = 1;
                *coordinatorPrecedence = 5;
                break;
            }
            case iswDeviceTypes::Generic_Biometric_Sensor:
            {
                ss << " Generic Biometric Sensor" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::Generic_Environmental_Sensor:
            {
                ss << " Generic Environmental Sensor" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::Generic_Positioning_Sensor:
            {
                ss << " Generic Positioning Sensor - GPS" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 0; // 5
                break;
            }
            case iswDeviceTypes::ReservedLegacy:
            case iswDeviceTypes::ReservedDeviceTypes:
            default:
            {
                ss << " Undefined Device Type" << std::endl;

                *coordinatorCapable    = 0;
                *coordinatorPrecedence = 5;
                break;
            }
        }

        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            ss << "  Returned Coordinator Capable    = " << std::to_string(*coordinatorCapable) << std::endl;
            ss << "  Returned Coordinator Precedence = " << std::to_string(*coordinatorPrecedence) << std::endl;
            theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
        }
}

//!###############################################################
//! GetDeviceTypeString()
//! Inputs: devType - type of ISW device
//! Description: Returns  the friendly string name for the device
//!              device type.
//!###############################################################
std::string IswIdentity::GetDeviceTypeString(uint16_t devType)
{
    switch (devType)
    {
        case iswDeviceTypes::ReservedDeviceTypes: return ( "Reserved Device Types" );
        case iswDeviceTypes::TWS: return ( "TWS" );
        case iswDeviceTypes::HMD: return ( "HMD" );
        case iswDeviceTypes::PHN: return ( "PHN" );
        case iswDeviceTypes::LRF: return ( "LRF" );
        case iswDeviceTypes::GPS: return ( "GPS" );
        case iswDeviceTypes::RLH: return ( "RLH" );
        case iswDeviceTypes::RLD: return ( "RLD" );
        case iswDeviceTypes::DPT: return ( "DPT" );
        case iswDeviceTypes::NET: return ( "NET" );
        case iswDeviceTypes::ENC: return ( "ENC" );
        case iswDeviceTypes::KPD: return ( "KPD" );
        case iswDeviceTypes::NES: return ( "NES" );
        case iswDeviceTypes::HTR: return ( "HTR" );
        case iswDeviceTypes::CHD: return ( "CHD" );
        case iswDeviceTypes::GNM: return ( "GNM" );
        case iswDeviceTypes::GNS: return ( "GNS" );
        case iswDeviceTypes::ReservedLegacy: return ( "Reserved Legacy" );
        case iswDeviceTypes::NET1: return ( "NET1" );
        case iswDeviceTypes::NET2: return ( "NET2" );
        case iswDeviceTypes::NET3: return ( "NET3" );
        case iswDeviceTypes::NET4: return ( "NET4" );
        case iswDeviceTypes::NET5: return ( "NET5" );
        case iswDeviceTypes::Generic_Biometric_Sensor: return ( "Generic_Biometric_Sensor" );
        case iswDeviceTypes::Generic_Environmental_Sensor: return ( "Generic_Environmental_Sensor" );
        case iswDeviceTypes::Generic_Positioning_Sensor: return ( "Generic_Positioning_Sensor" );
        default: return ( "Unknown_devType");
    }
}


//!###################################################################
//! SetupGetMacAddressCmd()
//! Inputs: iswIdentityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswIdentity::SetupGetMacAddressCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SetupGetMacAddressCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswIdentityCmd->cmdType    = IswInterface::Identity;
    iswIdentityCmd->command    = GetMACAddress;
    iswIdentityCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswIdentityCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswIdentityCommand);

    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswIdentityCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswIdentityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswIdentityCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswIdentityCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetMacAddressCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswIdentity::SendGetMacAddressCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SendGetMacAddressCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetMacAddressCmd(&iswCmd.iswIdentityCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "SendGetMacAddressCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetDeviceTypeCmd()
//! Inputs: iswSetDevTypeCmd - pointer to the command structure
//!         deviceType - number representing the ISW device type
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswIdentity::SetupSetDeviceTypeCmd(iswSetDeviceTypeCommand *iswSetDevTypeCmd, uint16_t deviceType, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SetupSetDeviceTypeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    qDebug() << "device type message being set up: " << deviceType;

   iswSetDevTypeCmd->cmdType    = IswInterface::Identity;
   iswSetDevTypeCmd->command    = SetDeviceType;
   iswSetDevTypeCmd->cmdContext = iswInterface->GetNextCmdCount();
   iswSetDevTypeCmd->reserved1  = 0x00;
   iswSetDevTypeCmd->deviceType = deviceType;
   // I added this, shouldnt be needed, trying to fix Generic cases bug
   //iswDeviceType = deviceType;
   iswSetDevTypeCmd->reserved2  = 0x00;

   qDebug() << "setup send set dev type after set var: " << iswSetDevTypeCmd->deviceType;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSetCoordinatorCommand);

    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswSetDevTypeCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswSetDevTypeCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswSetDevTypeCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswSetDevTypeCmd->reserved1) << std::endl;
        ss << "    deviceType = " << std::to_string(iswSetDevTypeCmd->deviceType) << std::endl;
        ss << "    reserved2  = " << std::to_string(iswSetDevTypeCmd->reserved2) << std::endl;

        qDebug() << QString::fromStdString(ss.str());
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetDeviceTypeCmd()
//! Inputs: deviceType - ISW device type number
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswIdentity::SendSetDeviceTypeCmd(uint16_t deviceType)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SendSetDeviceTypeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetDeviceTypeCmd(&iswCmd.iswSetDevTypeCmd, deviceType, &routingHdr);

    qDebug() << "dev type after setup before send" << iswCmd.iswSetDevTypeCmd.deviceType;

    qDebug() << "got passed setup";


    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    qDebug() << "identity dev type after syncSend:" << iswDeviceType;

    qDebug() << "dev type after SyncSend" << iswCmd.iswSetDevTypeCmd.deviceType;

    qDebug() << "send status: " << status;

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "SendSetDeviceTypeCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetDeviceTypeCmd()
//! Inputs: iswIdentityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswIdentity::SetupGetDeviceTypeCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SetupGetDeviceTypeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswIdentityCmd->cmdType    = IswInterface::Identity;
    iswIdentityCmd->command    = GetDeviceType;
    iswIdentityCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswIdentityCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswIdentityCommand);

    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswIdentityCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswIdentityCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswIdentityCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswIdentityCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendGetDeviceTypeCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswIdentity::SendGetDeviceTypeCmd()
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SendGetDeviceTypeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetDeviceTypeCmd(&iswCmd.iswIdentityCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    //qDebug() << "dev type in sendGetDev: " << iswCmd.iswSetDevTypeCmd.deviceType;

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "SendGetDeviceTypeCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupSetCoordinatorCmd()
//! Inputs: iswSetCoordCmd - pointer to the command structure
//!         coordinatorCapable - 0 = no, 1 = yes
//!         coordinatorPrecedence - 0 to 5
//!         persist - 0 = do nothing, 1 = store values in NVRAM
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswIdentity::SetupSetCoordinatorCmd(iswSetCoordinatorCommand *iswSetCoordCmd, uint8_t coordinatorCapable,
                                         uint8_t coordinatorPrecedence, uint8_t persist, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SetupSetCoordinatorCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswSetCoordCmd->cmdType               = IswInterface::Identity;
    iswSetCoordCmd->command               = SetCoordinator;
    iswSetCoordCmd->cmdContext            = iswInterface->GetNextCmdCount();
    iswSetCoordCmd->reserved1             = 0x00;
    iswSetCoordCmd->coordinatorCapable    = coordinatorCapable;  //! Can act as coordinator; 1=yes; 0=no
    iswSetCoordCmd->coordinatorPrecedence = coordinatorPrecedence; //! 0 to 5
    iswSetCoordCmd->reserved2             = 0x00;
    iswSetCoordCmd->persist               = persist; //! Set to 1 = save to NVRAM

    //! Update class params
    IswIdentityCmdLock.lock();
    iswCoordinatorCapable = coordinatorCapable;
    iswCoordinatorPrecedence = coordinatorPrecedence;
    iswPersist = persist;
    IswIdentityCmdLock.unlock();

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswSetCoordinatorCommand);

    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType               = " << std::to_string(iswSetCoordCmd->cmdType) << std::endl;
        ss << "    command               = " << std::to_string(iswSetCoordCmd->command) << std::endl;
        ss << "    cmdContext            = " << std::to_string(iswSetCoordCmd->cmdContext) << std::endl;
        ss << "    reserved1             = " << std::to_string(iswSetCoordCmd->reserved1) << std::endl;
        ss << "    coordinatorCapable    = " << std::to_string(iswSetCoordCmd->coordinatorCapable) << std::endl;
        ss << "    coordinatorPrecedence = " << std::to_string(iswSetCoordCmd->coordinatorPrecedence) << std::endl;
        ss << "    reserved2             = " << std::to_string(iswSetCoordCmd->reserved2) << std::endl;
        ss << "    persist               = " << std::to_string(iswSetCoordCmd->persist) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetCoordinatorCmd()
//! Inputs: coordinatorCapable - 0 = no, 1 = yes
//!         coordinatorPrecedence - 0 to 5
//!         persist - 0 = do nothing, 1 = store values in NVRAM
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswIdentity::SendSetCoordinatorCmd(uint8_t coordinatorCapable, uint8_t coordinatorPrecedence, uint8_t persist)
{
    std::string msgString;

    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SendSetCoordinatorCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetCoordinatorCmd(&iswCmd.iswSetCoordinatorCmd, coordinatorCapable, coordinatorPrecedence, persist, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "SendSetCoordinatorCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupGetCoordinatorCmd()
//! Inputs: iswIdentityCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswIdentity::SetupGetCoordinatorCmd(iswIdentityCommand *iswIdentityCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SetupSetCoordinatorCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswIdentityCmd->cmdType    = IswInterface::Identity;
    iswIdentityCmd->command    = GetCoordinator;
    iswIdentityCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswIdentityCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswIdentityCommand);
}

//!###################################################################
//! SendGetCoordinatorCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswIdentity::SendGetCoordinatorCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswIdentity == 1 )
    {
        msgString = "SendGetCoordinatorCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetCoordinatorCmd(&iswCmd.iswIdentityCmd, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswIdentity == 1 )
        {
            msgString = "SendGetCoordinatorCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}
