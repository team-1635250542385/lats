//!###############################################
//! Filename: IswAssociation.cpp
//! Description: Class that provides an interface
//!              to ISW Association commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswAssociation.h"
#include "IswStream.h"
#include "IswInterface.h"
#include <sstream>
#include <iomanip>

//!###############################################################
//! Constructor: IswAssociation()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswAssociation::IswAssociation(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswAssociation::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################

void IswAssociation::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;
    msgString.clear();

    switch (cmd)
    {
        case StartAssociation:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = "Received StartAssociation Confirmation With Timeout = ";
                uint8_t timeout = data[0];
                msgString.append(std::to_string(timeout));
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                msgString.clear();
            }
            break;
        }
        case StopAssociation:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = "Received StopAssociation Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                msgString.clear();
            }
            break;
        }
        case ClearAssociation:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = "Received ClearAssociation Confirmation";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                msgString.clear();
            }
            break;
        }
        case AssociationIndication:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = "Received AssociationIndication";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                msgString.clear();
            }

            ParseAssociationIndication((iswEventAssociationInd *)data);
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = "Bad Association Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! SetupStartAssociationCmd()
//! Inputs: iswStartAssociateCmd - pointer to the command structure
//!         type - type - 1 = invite, 2 = search
//!         timeout - timeout to wait on Association completed
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswAssociation::SetupStartAssociationCmd(iswStartAssociationCommand *iswStartAssociateCmd, uint8_t type, uint8_t timeout, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SetupStartAssociation";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswStartAssociateCmd->cmdType    = IswInterface::Association;
    iswStartAssociateCmd->command    = StartAssociation;
    iswStartAssociateCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswStartAssociateCmd->reserved1  = 0x00;
    iswStartAssociateCmd->type       = type;          //! cmdType = 1 invite, cmdType = 2 search
    iswStartAssociateCmd->timeout    = timeout;    //! in seconds
    iswStartAssociateCmd->reserved2  = 0x0000;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStartAssociationCommand);

    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswStartAssociateCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswStartAssociateCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswStartAssociateCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswStartAssociateCmd->reserved1) << std::endl;
        ss << "    type       = " << std::to_string(iswStartAssociateCmd->type) << std::endl;
        ss << "    timeout    = " << std::to_string(iswStartAssociateCmd->timeout) << std::endl;
        ss << "    reserved2  = " << std::to_string(iswStartAssociateCmd->reserved2) << std::endl;;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendStartAssociationCmd()
//! Inputs: type - 1 = invite, 2 = search
//!         timeout - timeout to wait on Association completed
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswAssociation::SendStartAssociationCmd(uint8_t type, uint8_t timeout)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SendStartAssociationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupStartAssociationCmd(&iswCmd.iswStartAssociationCmd, type, timeout, (void *)&routingHdr);

    // Capture the timestamp
    startAssociationTimestamp = theLogger->GetTimeStamp();

    // Send packet to firmware
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswAssociation == 1 )
        {
            msgString = "SendStartAssociationCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    else
    {
        msgString = "startAssociationTimestamp = ";
        msgString.append(std::to_string((uint64_t)startAssociationTimestamp));
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    return ( status );
}

//!###################################################################
//! SetupStopAssociationCmd()
//! Inputs: iswClearAssociateCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswAssociation::SetupStopAssociationCmd(iswStopAssociationCommand *iswStopAssociateCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SetupStopAssociation";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswStopAssociateCmd->cmdType    = IswInterface::Association;
    iswStopAssociateCmd->command    = StopAssociation;
    iswStopAssociateCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswStopAssociateCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStopAssociationCommand);

    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswStopAssociateCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswStopAssociateCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswStopAssociateCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswStopAssociateCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendStopAssociationCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswAssociation::SendStopAssociationCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SendStopAssociationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupStopAssociationCmd(&iswCmd.iswStopAssociationCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswAssociation == 1 )
        {
            msgString = "SendStopAssociationCmd Failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! SetupClearAssociationCmd()
//! Inputs: iswClearAssociateCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswAssociation::SetupClearAssociationCmd(iswClearAssociationCommand *iswClearAssociateCmd, void *routingHdr)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SetupClearAssociation";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswClearAssociateCmd->cmdType    = IswInterface::Association;
    iswClearAssociateCmd->command    = ClearAssociation;
    iswClearAssociateCmd->cmdContext = iswInterface->GetNextCmdCount();
    iswClearAssociateCmd->reserved1  = 0x00;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswClearAssociationCommand);

    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswClearAssociateCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswClearAssociateCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswClearAssociateCmd->cmdContext) << std::endl;
        ss << "    reserved1  = " << std::to_string(iswClearAssociateCmd->reserved1) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendClearAssociationCmd()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswAssociation::SendClearAssociationCmd(void)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "SendClearAssociationCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupClearAssociationCmd(&iswCmd.iswClearAssociationCmd, (void *)&routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswAssociation == 1 )
        {
            msgString = "SendClearAssociationCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! ParseAssociationIndication()
//! Inputs: iswAssociationInd - pointer to the ISW Association
//!         Indication command
//! Description: Walks through the Peer Records sent in the messages
//!              and updates the internal peer record table for the
//!              device
//!###################################################################
void IswAssociation::ParseAssociationIndication(iswEventAssociationInd *iswAssociationInd)
{
    std::string msgString;

    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "ParseAssociationIndication";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        msgString.clear();
    }

    uint8_t peerIndex    = iswAssociationInd->peerIndex;

    IswStream *iswStream = iswInterface->GetIswStream();
    std::array<IswStream::iswPeerRecord, MAX_PEERS>& iswPeers = iswStream->GetPeerRef();

    //! Update the IswStreams peer record
    iswStream->PeerRecordLock();
    iswPeers[peerIndex].inUse = true;

    //! security key
    for (int i = 8; i < 24; i++)
    {
        iswPeers[peerIndex].securityKey[i] = iswAssociationInd->securityKey[i];
    }

    //! Hasn't been set yet, so default to 2 so we
    //! save the macAddress
    if ( iswPeers[peerIndex].recordVersion < 1 )
    {
        iswPeers[peerIndex].recordVersion = 2;
    }

    if ( theLogger->DEBUG_IswAssociation == 1 )
    {
        msgString = "    Peer index = ";
        msgString.append(std::to_string(peerIndex));
        msgString.append("\n");
        msgString.append("    inUse = ");
        msgString.append(std::to_string(iswPeers[peerIndex].inUse));
        msgString.append("\n");
        msgString.append("    recordVersion = ");
        msgString.append(std::to_string(iswPeers[peerIndex].recordVersion));
        msgString.append("\n");
        msgString.append("    Security key = ");
        for (int i = 0; i < 16; i++)
        {
            msgString.append(std::to_string(iswPeers[peerIndex].securityKey[i]));
        }
        msgString.append("\n");
    }

    switch ( iswPeers[peerIndex].recordVersion )
    {
        case 1:
        {
            iswPeers[peerIndex].iswPeerRecord1.deviceType = iswAssociationInd->deviceType;
            break;
        }
        case 2:
        {
            std::stringstream ss;
            std::stringstream macAddress;

            for (int i = 0; i < 6; i++)
            {
                iswPeers[peerIndex].iswPeerRecord2.macAddress[i] = iswAssociationInd->macAddress[i];
            }
            iswPeers[peerIndex].iswPeerRecord2.deviceType = iswAssociationInd->deviceType;

            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord2.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record2.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord2.deviceType << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case 3:
        {
            for (int i = 0; i < 6; i++)
            {
                iswPeers[peerIndex].iswPeerRecord3.macAddress[i] = iswAssociationInd->macAddress[i];
            }
            iswPeers[peerIndex].iswPeerRecord3.deviceType = iswAssociationInd->deviceType;

            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord3.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record3.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord3.deviceType << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        case 4:
        {
            for (int i = 0; i < 6; i++)
            {
                iswPeers[peerIndex].iswPeerRecord4.macAddress[i] = iswAssociationInd->macAddress[i];
            }
            iswPeers[peerIndex].iswPeerRecord4.deviceType = iswAssociationInd->deviceType;

            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                std::stringstream ss;
                std::stringstream macAddress;
                for (int i = 0; i < 6; i++)
                {
                    macAddress <<  std::hex << std::setfill('0') << std::setw(2) << (int)iswPeers[peerIndex].iswPeerRecord4.macAddress[i];
                    if ( i < 5 )
                    {
                        macAddress << ":";
                    }
                }
                ss << "    record4.macAddress = " << macAddress.str() << std::endl <<
                ss << "    deviceType         = " <<  std::hex << (int)iswPeers[peerIndex].iswPeerRecord4.deviceType << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_IswAssociation == 1 )
            {
                msgString = " - Bad Peer Record Version";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
    iswStream->PeerRecordUnlock();
}
