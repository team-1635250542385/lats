//!###################################################
//! Filename: IswInterfaceInit.h
//! Description: Class that initalizes each device
//!              Under test with an IswInterface
//!              and a UsbInterface class.  It returns
//!              a list of IswInterface pointers
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ISWINTERFACEINIT_H
#define ISWINTERFACEINIT_H

#include "../Logger/Logger.h"
#include "../Logger/DBLogger.h"
#include "../UsbInterface/UsbInterfaceInit.h"
#include "../IswInterface/IswInterface.h"

#ifdef TESTING
// For running unit tests
#define ACCESS public
#else
#define ACCESS protected
#endif

class IswInterfaceInit
{

public:
    IswInterfaceInit(uint8_t devType, Logger *logger, Logger *throughputTestLoggerPass, Logger *throughputResultsLoggerPass, DBLogger *dblogger, Logger *latencyLogger);
    ~IswInterfaceInit();

    //! iswInterfaceArray holds the ISW devices attached via USB. Each entry
    //! is a pointer to an IswInterface object for the device. The index into
    //! this array matches the index in the usbInterfaceArray for the same device.
#define MAX_ISW_NODES 21
    std::array<IswInterface *, MAX_ISW_NODES> *InitIswInterfaceArray(void);
    std::array<IswInterface *, MAX_ISW_NODES> *GetIswInterfaceArray(void) { return (&iswInterfaceArray); }

    //! Clean up the entries in the array before exiting
    void ExitIswInterfaceArray(void);

    //! Used in USB Hotplug handling to add or remove entries
    void AddIswInterfaceToArray(uint8_t index, IswInterface *iswInterfacePtr);
    void RemoveIswInterfaceFromArray(uint8_t index);

    struct ExternalDeviceEntry
    {
        std::string macAddr;
        uint16_t devType;
    };

    //! Used in Hotplug handling to receive events: add or remove
    struct HotplugEvent
    {
        bool external              = false;
        struct libusb_context *ctx = nullptr;
        struct libusb_device *dev  = nullptr;
        libusb_hotplug_event event;
        ExternalDeviceEntry externalEntry;
    };

    //! Callback passed to libusb.  If there is a USB hotplug event this
    //! gets called to handle it putting the event on a hotplug handling queue
    static int HandleUsbHotplugEvent(struct libusb_context *ctx, struct libusb_device *dev,
                                    libusb_hotplug_event event, void *user_data);
    //! Add an ISW device from the hotplug event
    uint8_t AddHotplugDevice(HotplugEvent *hotplugEvent);
    //! Remove an ISW device from the hotplug event
    uint8_t RemoveHotplugDevice(HotplugEvent *hotplugEvent);
    //! Run periodically in a background thread to get events and handle them
    uint8_t CheckHotplugQueue(void);

    UsbInterfaceInit *GetUsbInterfaceInit(void) { return usbInterfaceInit; }

    bool CheckEntryExists(std::string macAddr);

ACCESS:
    uint8_t deviceType;
    UsbInterfaceInit *usbInterfaceInit = nullptr;

#define ISW_INTERFACE_LOG_INDEX 0xFFFF
    Logger *theLogger  = nullptr;
    Logger *throughputTestLogger   = nullptr;
    Logger *throughputResultsLogger = nullptr;
    DBLogger *dbLogger = nullptr;
    Logger *latencyLogger = nullptr;

    //! ISW network allows up to 21 devices
    std::array<IswInterface *, MAX_ISW_NODES> iswInterfaceArray;

    //! Hotplug handling
    std::mutex iswInitArrayCmdLock;
    std::mutex hotplugQueueMutex;
    std::queue<HotplugEvent *> hotplugQueue;
public:
    void AddToHotplugQueue(HotplugEvent *hotplugEvent);
ACCESS:
    HotplugEvent *RemoveFromHotplugQueue(void);
    void EmptyHotplugQueue(void);
};

#endif // ISWINTERFACEINIT_H
