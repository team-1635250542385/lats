//!###############################################
//! Filename: IswMetrics.cpp
//! Description: Class that provides an interface
//!              to ISW Metrics commands
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "IswTest.h"
#include "IswInterface.h"
#include <sstream>

#include "QDebug"

//!###############################################################
//! Constructor: IswTest()
//! Inputs: iswInterface - a pointer to the IswInterface object
//!         logger - a pointer to the Logger object
//!###############################################################
IswTest::IswTest(IswInterface *isw_interface, Logger *logger, DBLogger *dblogger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
        dbLogger = dblogger;
    }
}

//!#############################################################
//! GetIndex()
//! Inputs: None
//! Description: Returns the index into the iswInterfaceArray
//!              for this device
//! #############################################################
uint8_t IswTest::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!###############################################################
//! DeliverIswMessage()
//! Inputs: cmd - ISW API command number
//!         data - pointer to the data in a receive buffer
//! Description: This method handles incoming ISW API messages for
//!              this friend class
//!###############################################################
void IswTest::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;

    switch (cmd)
    {
        case RapCommand:
        {
            msgString = "Received RapCommand confirmation";
            qDebug() << QString::fromStdString(msgString);
            if ( theLogger->DEBUG_IswTest == 1 )
            {
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
        case RapIndication:
        {
            if ( theLogger->DEBUG_IswTest == 1 )
            {
                msgString = "Received GetRapStatus confirm";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }

            iswTestRapInd *rapInd = (iswTestRapInd *)data;
            iswSleepPercentage    = rapInd->sleepPercentage;
            iswScanPercentage     = rapInd->scanPercentage;

            qDebug() << "RAP Sleep Percentage: " << iswSleepPercentage;
            qDebug() << "RAP Scan Percentage: " << iswScanPercentage;

            if ( theLogger->DEBUG_IswTest == 1 )
            {
                std::stringstream ss;
                msgString.clear();
                ss << " Isw Test RAP Sleep Percentage = " << std::to_string(iswSleepPercentage) << std::endl;
                ss << " Isw Test RAP Scan Perecentage = " << std::to_string(iswScanPercentage) << std::endl;
                msgString.append(ss.str());
                theLogger->DebugMessage(msgString, iswInterface->GetIndex());
            }
            break;
        }
        default:
        {
            qDebug() << "isw test deliver bad event type: " << cmd;
            if ( theLogger->DEBUG_IswTest == 1 )
            {
                msgString = "Bad System Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!###################################################################
//! SetupSetRapStatus()
//! Inputs: iswTestCmd - pointer to the command structure
//!         routingHdr - pointer to the ISW Router Header
//! Description: Fills in the ISW Header for the send command
//!              to the firmware
//!###################################################################
void IswTest::SetupRapCommand(iswTestCommand *iswTestCmd, void *routingHdr, uint8_t rate)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswMetrics == 1 )
    {
        msgString = "SetupGetWirelessMetrics";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    iswTestCmd->cmdType    = IswInterface::Test;
    iswTestCmd->command    = RapCommand;
    iswTestCmd->contextId  = iswInterface->GetNextCmdCount();
    iswTestCmd->reserved   = 0x00;
    iswTestCmd->rate       = rate;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswTestCommand);

    if ( theLogger->DEBUG_IswTest == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(iswTestCmd->cmdType) << std::endl;
        ss << "    command    = " << std::to_string(iswTestCmd->command) << std::endl;
        ss << "    cmdContext = " << std::to_string(iswTestCmd->contextId) << std::endl;
        ss << "    reserved   = " << std::to_string(iswTestCmd->reserved) << std::endl;
        ss << "    rate       = " << std::to_string(iswTestCmd->rate) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!###################################################################
//! SendSetRapStatus()
//! Inputs: None
//! Description: Send the ISW command to the firmware
//!###################################################################
int IswTest::SendRapCommand(uint8_t rate)
{
    std::string msgString;
    if ( theLogger->DEBUG_IswTest == 1 )
    {
        msgString = "SendSetRapStatus";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupRapCommand(&iswCmd.iswTestCmd, (void *)&routingHdr, rate);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);
    //qDebug() << "completed SyncSend with status: " << status;
    if ( status < 0 )
    {
        if ( theLogger->DEBUG_IswTest == 1 )
        {
            msgString = "SendSetRapStatus failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }
    else
    {
        if ( theLogger->DEBUG_IswTest == 1 )
        {
            msgString = "RAP Rate Set to " + std::to_string(rate);
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    return ( status );
}

//!###################################################################
//! ParseRapIndication()
//! Inputs: iswTesRapInd - pointer to the ISW Test
//!         Indication command
//! Description:
//!###################################################################
void IswTest::ParseRapIndication(iswTestRapInd *iswTestRapInd)
{

}

