var class_set_background_scan_parameters_dialog =
[
    [ "SetBackgroundScanParametersDialog", "class_set_background_scan_parameters_dialog.html#ab9d400e76595839b15327b137269ea5a", null ],
    [ "~SetBackgroundScanParametersDialog", "class_set_background_scan_parameters_dialog.html#ae875c1569afbf8ffabe70aed58a7f183", null ],
    [ "getFluxScanDuration", "class_set_background_scan_parameters_dialog.html#ab954ac047e4418a745000a3d7e428652", null ],
    [ "getFluxScanFrequency", "class_set_background_scan_parameters_dialog.html#a2a2d37e7dec22e1cba72453e8221c24e", null ],
    [ "getStableScanFrequency", "class_set_background_scan_parameters_dialog.html#afa3c564d17f0a405ccdef52ccea4c42f", null ],
    [ "getVersion", "class_set_background_scan_parameters_dialog.html#ac18d7948be63e5810688f5257f599ae2", null ]
];