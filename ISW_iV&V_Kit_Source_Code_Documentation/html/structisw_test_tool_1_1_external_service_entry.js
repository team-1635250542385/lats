var structisw_test_tool_1_1_external_service_entry =
[
    [ "dataflowId", "structisw_test_tool_1_1_external_service_entry.html#a4f677a0cfb0c4af93850faa0e7a30e73", null ],
    [ "dynArrayIndex", "structisw_test_tool_1_1_external_service_entry.html#a118595b3804531d105faa97c10e9478c", null ],
    [ "endpointDistribution", "structisw_test_tool_1_1_external_service_entry.html#a7d513a138cbcbe89fa437694eecf11fb", null ],
    [ "linkQuality", "structisw_test_tool_1_1_external_service_entry.html#aa55c0196ed65486f3ccd1b86edd781cf", null ],
    [ "multicastGroupId", "structisw_test_tool_1_1_external_service_entry.html#a01994aa3d149433407eae8f5f962120d", null ],
    [ "peerIndex", "structisw_test_tool_1_1_external_service_entry.html#afa7c763f829c341764ee3791efc0efa0", null ],
    [ "selected", "structisw_test_tool_1_1_external_service_entry.html#a826188e854f7752356e7a8f4955fee28", null ],
    [ "serviceName", "structisw_test_tool_1_1_external_service_entry.html#a0675e24667aa5664f0ad4913308c98b5", null ]
];