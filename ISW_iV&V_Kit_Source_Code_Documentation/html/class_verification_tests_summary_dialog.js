var class_verification_tests_summary_dialog =
[
    [ "VerificationTestsSummaryDialog", "class_verification_tests_summary_dialog.html#adcb256e864d494930da80f3da269b152", null ],
    [ "~VerificationTestsSummaryDialog", "class_verification_tests_summary_dialog.html#a651e9a02a5d2c0e45aa2778a75e09e81", null ],
    [ "getNumberOfCommandsFail", "class_verification_tests_summary_dialog.html#a22426edb072f20ce5b67b46b5c35254b", null ],
    [ "getNumberOfCommandsPass", "class_verification_tests_summary_dialog.html#a379b6ed00d13bf64a0504a66d70071b7", null ],
    [ "getPercentageOfRequiredCommandsPass", "class_verification_tests_summary_dialog.html#a96e922b4f1d7ace45a41563719cd489f", null ],
    [ "setCommand", "class_verification_tests_summary_dialog.html#ae831f7aea2947406486116588a0f710f", null ],
    [ "setNumberOfCommandsFail", "class_verification_tests_summary_dialog.html#a2a48c0d7d41b0c8365ebd4893a7a177b", null ],
    [ "setNumberOfCommandsPass", "class_verification_tests_summary_dialog.html#a2cbbce78bebf01a483ce13ed68d47a50", null ],
    [ "setPercentageOfRequiredCommandsPass", "class_verification_tests_summary_dialog.html#ae5a4e2c1fec6e658da5adb09b9dcaebd", null ],
    [ "setResult", "class_verification_tests_summary_dialog.html#a635a87852ac16ea63404c5b8863f6246", null ]
];