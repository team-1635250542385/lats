var structisw_test_tool_1_1_sol_net_peer_list_entry =
[
    [ "averageRSSI", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#adde0ef0c0463f84e3e73f55dd04bd053", null ],
    [ "connectionTimestampMicrosecs", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a4dd5e83ae5c2466b50fcdb3a3a5f4f21", null ],
    [ "devName", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a7b244baed377f9c57059ab51be9b8a14", null ],
    [ "devType", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a13aa0b52a6c812f6782dcf4feb208043", null ],
    [ "isConnected", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a76758b655991f4294505ae78f7296556", null ],
    [ "macAddr", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a97911e1b4727a425ea40969c5746537c", null ],
    [ "modePhyRate", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a3e9168aee8496263450ddd5a6ef2b1a0", null ],
    [ "peerIndex", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a488a7a8b196fb530cff89a0523adfeb7", null ],
    [ "recvThroughput", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#a1f1a6d555d0dcb7f859be1ce9dec014c", null ],
    [ "xmtThroughput", "structisw_test_tool_1_1_sol_net_peer_list_entry.html#af85a167ee937b83641b791f116f9441e", null ]
];