var class_gst_player =
[
    [ "GstPlayer", "class_gst_player.html#a44c0cf6a97e09b686923ab380dbc8f13", null ],
    [ "~GstPlayer", "class_gst_player.html#ac9d3b7ec13f7a1eaa60c1a17c61756cb", null ],
    [ "isPlayerRunning", "class_gst_player.html#ac0726b02b171b2f2d3db0dbe22a8b3b7", null ],
    [ "receiveVideoDataInPlayer", "class_gst_player.html#ae232ed0a61824bee3e1dc3d2898b0d7f", null ],
    [ "startReceiving", "class_gst_player.html#a694d4ab46dac1825344393625011e458", null ],
    [ "stopReceiving", "class_gst_player.html#a3303bc64cfae282614fd067a389e8504", null ]
];