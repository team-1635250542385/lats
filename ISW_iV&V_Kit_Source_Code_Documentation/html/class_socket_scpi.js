var class_socket_scpi =
[
    [ "SocketScpi", "class_socket_scpi.html#aea6881fd0c24921711edb651c92d7cc9", null ],
    [ "SocketScpi", "class_socket_scpi.html#a638cf879dc13fb9eb87d5b8949a4092d", null ],
    [ "~SocketScpi", "class_socket_scpi.html#aae04f6750c82bb6fdc4f34620c712c9d", null ],
    [ "checkError", "class_socket_scpi.html#a6a19f0804a9a9ecdacf1a2b032256f12", null ],
    [ "connect", "class_socket_scpi.html#ab2c6807f3e4874a7014a34769cace0b1", null ],
    [ "disconnect", "class_socket_scpi.html#a700734357daa3ea3cde9e486506f9311", null ],
    [ "recv", "class_socket_scpi.html#adbf7af0359a06b4c9f9c34d6e0fba1f1", null ],
    [ "send", "class_socket_scpi.html#aa0d81aa2f65e9fc09a1f6379fe2ac762", null ]
];