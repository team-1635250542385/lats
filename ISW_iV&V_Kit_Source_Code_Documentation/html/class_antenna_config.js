var class_antenna_config =
[
    [ "AntennaConfig", "class_antenna_config.html#a5884da8af9a0dcdff3fc1a15304bbf9d", null ],
    [ "~AntennaConfig", "class_antenna_config.html#a2e82376138d2b6a85c54e767f907ac9e", null ],
    [ "getEnable", "class_antenna_config.html#af78c22e7697163d76ee38e7e17e33205", null ],
    [ "getSwitchThresholdAdder", "class_antenna_config.html#ae0982e3adff9cfc9e157c7648cdb0542", null ],
    [ "getTriggerAlgorithm", "class_antenna_config.html#aa747f8bbd3a1711992bdbed24533963d", null ],
    [ "getTriggerPhyRateThreshold", "class_antenna_config.html#a49c1caa7d2f6534702b34376c58bc208", null ],
    [ "getTriggerRssiThreshold", "class_antenna_config.html#a5c2ac15070d3e58a1ab976508fad1737", null ]
];