var structisw_test_tool_1_1_multicast_group_record =
[
    [ "mcastAddress", "structisw_test_tool_1_1_multicast_group_record.html#af9c68be46aa9e890d5d049f0ed2a3127", null ],
    [ "mcastPeerIndex", "structisw_test_tool_1_1_multicast_group_record.html#a08da464e6ac4f2fdb18d2e05b58f6ec1", null ],
    [ "mcastPhyRate", "structisw_test_tool_1_1_multicast_group_record.html#a2c153bdbe7a5c149e17f5f0ad2d7597f", null ],
    [ "mcastStatus", "structisw_test_tool_1_1_multicast_group_record.html#a8bb3bc4df079c8900534dd38970529bc", null ],
    [ "txQueueDepth", "structisw_test_tool_1_1_multicast_group_record.html#ac3517f74901634318f7c5083c02410a4", null ]
];