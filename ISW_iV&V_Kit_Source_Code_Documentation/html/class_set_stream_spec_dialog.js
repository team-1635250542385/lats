var class_set_stream_spec_dialog =
[
    [ "SetStreamSpecDialog", "class_set_stream_spec_dialog.html#a2a603ad2c270e852c30537c7dd5fce44", null ],
    [ "~SetStreamSpecDialog", "class_set_stream_spec_dialog.html#a6f8a314f2c78ed3ce62e141a266418c1", null ],
    [ "getEndpointTxDiscardable", "class_set_stream_spec_dialog.html#a406936799d683d6535f5f4cfea60a5ca", null ],
    [ "getEndpointTxPriority", "class_set_stream_spec_dialog.html#a55a92415602d13b62c45f99f1573b704", null ],
    [ "getMaxTxLatency", "class_set_stream_spec_dialog.html#a36e7d1418b9f0bf3aff9be08d09d1fbe", null ],
    [ "getPeerCount", "class_set_stream_spec_dialog.html#ae75fd239c4cb9660d4d6a7abe36f07f0", null ],
    [ "getPeerIndex", "class_set_stream_spec_dialog.html#addfd7ba96f58b07f26cc3a7dad0b893d", null ],
    [ "setEndpointTxDiscardable", "class_set_stream_spec_dialog.html#aa5689208881b20a4e1b79f30140589ea", null ],
    [ "setEndpointTxPriority", "class_set_stream_spec_dialog.html#af408ed2e991b7b0fb108abaab6489122", null ],
    [ "setMaxTxLatency", "class_set_stream_spec_dialog.html#a2e829ddcfab3396bba5a185415c871d4", null ],
    [ "setPeerCount", "class_set_stream_spec_dialog.html#aa5bc8ce393d955debe6a11f812d99c7f", null ]
];