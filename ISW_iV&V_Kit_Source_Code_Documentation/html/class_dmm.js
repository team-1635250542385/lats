var class_dmm =
[
    [ "Dmm", "class_dmm.html#af2280c692fe98e894f7168d77d314f6e", null ],
    [ "Dmm", "class_dmm.html#a2db358071a5375ab6185f1270c76e30f", null ],
    [ "~Dmm", "class_dmm.html#a12fb0ae64751f08edee98f3647c05e6b", null ],
    [ "configureTemperature", "class_dmm.html#adcd50628fcf44b91f84c09e2f4f70816", null ],
    [ "connect", "class_dmm.html#a518be607053006454ac3503a3e4f9e0c", null ],
    [ "disconnect", "class_dmm.html#ab8a2f871ce8f855e3f1917d11d280475", null ],
    [ "getCurrent", "class_dmm.html#a8cd7359975e5db5fb894ead0cfc8a94b", null ],
    [ "getTemp", "class_dmm.html#ad1ce142fc4dea62b9eb4307670d5601f", null ],
    [ "getVolt", "class_dmm.html#a4a299a2b9396f040bf9396946bfec2f1", null ]
];