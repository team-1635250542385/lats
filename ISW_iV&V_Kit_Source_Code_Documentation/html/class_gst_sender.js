var class_gst_sender =
[
    [ "GstSender", "class_gst_sender.html#a0d7967e3d8d112ddf7cee3ae86e10b85", null ],
    [ "~GstSender", "class_gst_sender.html#ac89414991aa1019c702161469d7469c8", null ],
    [ "isSenderRunning", "class_gst_sender.html#a3d01a9eb3774910d7583fb7f666984c3", null ],
    [ "startSending", "class_gst_sender.html#aa03ee0eee20a46551b396d7a90eca147", null ],
    [ "stopSending", "class_gst_sender.html#a924cf4e3d299e461a0145c088660eaf7", null ],
    [ "videoFromSourceAvailable", "class_gst_sender.html#ab45c3c74c2aefe6dcb73a3d8d98e8f10", null ]
];