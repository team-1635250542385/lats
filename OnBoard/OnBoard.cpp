//!###################################################
//! Filename: OnBoard.cpp
//! Description: Class that provides an interface
//!              to the on board tests that run
//!              on some test boards from the vendor.
//! These tests are not inniated on the host, like
//! the PacketGenerator or VideoTest classes
//! but run from board to board. The host application
//! sends special commands in the form of ISW commands
//! to the firmware to configure and start/stop these
//! tests.  This is the traditional ISW command format.
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################

#include "OnBoard.h"
#include "../IswInterface/IswInterface.h"

//!########################################################
//! Constructor: OnBoard class
//! Inputs: - pointer to the iswTestTool object
//!         - pointer to the Logger class
//!########################################################
OnBoard::OnBoard(IswInterface *isw_interface, Logger *logger)
{
    if (isw_interface == nullptr)
    {
        std::cout << "OnBoard: isw_interface pointer!" << std::endl;
        exit(-1);
    }
    else
    {
        iswInterface = isw_interface;
        theLogger = logger;
    }
}

OnBoard::~OnBoard()
{

}

//!##########################################################
//! GetIndex()
//! Returns the index into the array of IswInterface pointers
//!##########################################################
uint8_t OnBoard::GetIndex(void)
{
    return (iswInterface->GetIndex());
}

//!##########################################################
//! DeliverIswMessage()
//! Used like all ISW API classes to deliver an incoming
//! message from the firmware.
//!##########################################################
void OnBoard::DeliverIswMessage(uint8_t cmd, uint8_t *data)
{
    std::string msgString;
    msgString.clear();

    switch (cmd)
    {
        case SetTestMode:
        {
            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                msgString = "SetTestModeResponse";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                std::cout << msgString.c_str() << std::endl;
            }
            testModeSet = true;
            break;
        }
        case GetTestMode:
        {
            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                msgString = "GetTestModeResponse";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            iswGetTestModeCmdResponse *getTestResp = (iswGetTestModeCmdResponse *)data;
            testIsRunning = getTestResp->running;

            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                std::stringstream ss;
                ss << "    throughput   = " << std::to_string(getTestResp->throughput) << std::endl;
                ss << "    transferSize = " << std::to_string(getTestResp->transferSize) << std::endl;
                ss << "    running      = " << std::to_string(getTestResp->running) << std::endl;
                ss << "    reserved     = 0" << std::endl;
                theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
                std::cout << msgString.c_str() << std::endl;
            }

            break;
        }
        case StartTest:
        {
            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                msgString = "StartTestResponse";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                std::cout << msgString.c_str() << std::endl;
            }
            break;
        }
        case StopTest:
        {
            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                msgString = "StopTestResponse";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
                std::cout << msgString.c_str() << std::endl;
            }
            break;
        }
        default:
        {
            if ( theLogger->DEBUG_OnBoard == 1 )
            {
                msgString = "Bad System Event Type";
                theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
            }
            break;
        }
    }
}

//!##########################################################
//! SetupSetTestModeCmd()
//! Setup the ISW headers for the SendSetTestModeCmd
//!##########################################################
void OnBoard::SetupSetTestModeCmd(iswSetTestModeCommand *setTestModeCmd, uint8_t peerIndex, uint16_t pktSize, uint16_t throughput, void *routingHdr)
{
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::string msgString;
        msgString = "SetupSetTestModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    setTestModeCmd->iswCmd.cmdType    = IswInterface::OnBoardTests;
    setTestModeCmd->iswCmd.command    = SetTestMode;
    setTestModeCmd->iswCmd.cmdContext = iswInterface->GetNextCmdCount();
    setTestModeCmd->iswCmd.reserved   = 0;
    setTestModeCmd->peerIndex         = peerIndex;
    setTestModeCmd->throughput        = throughput;
    setTestModeCmd->transferSize      = pktSize;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr = 1;
    routingHdrPtr->destAddr = 1;
    routingHdrPtr->dataLength = sizeof(iswSetTestModeCommand);

    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(setTestModeCmd->iswCmd.cmdType) << std::endl;
        ss << "    command    = " << std::to_string(setTestModeCmd->iswCmd.command) << std::endl;
        ss << "    cmdContext = " << std::to_string(setTestModeCmd->iswCmd.cmdContext) << std::endl;
        ss << "    reserved   = " << std::to_string(setTestModeCmd->iswCmd.reserved) << std::endl;
        ss << "    peerIndex  = " << std::to_string(setTestModeCmd->peerIndex) << std::endl;
        ss << "    throughput = " << std::to_string(setTestModeCmd->throughput) << std::endl;
        ss << "    pktSize    = " << std::to_string(setTestModeCmd->transferSize) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!##########################################################
//! SendSetTestModeCmd()
//! Send the ISW command SendSetTestModeCmd to the firmware
//!##########################################################
int OnBoard::SendSetTestModeCmd(uint8_t peerIndex, uint16_t pktSize, uint16_t throughput)
{
    iswOnBoardCmdLock.lock();

    std::string msgString;
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        msgString = "SendSetTestModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupSetTestModeCmd(&iswCmd.setOnBoardTestModeCmd, peerIndex, pktSize, throughput, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_OnBoard == 1 )
        {
            msgString = "SendSetTestModeCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    iswOnBoardCmdLock.unlock();

    return ( status );
}

//!##########################################################
//! SetupGetTestModeCmd()
//! Setup the ISW headers for the SendGetTestModeCmd
//!##########################################################
void OnBoard::SetupGetTestModeCmd(iswGetTestModeCommand *getTestModeCmd, uint8_t peerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::string msgString;
        msgString = "SetupGetTestModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    getTestModeCmd->iswCmd.cmdType    = IswInterface::OnBoardTests;
    getTestModeCmd->iswCmd.command    = GetTestMode;
    getTestModeCmd->iswCmd.cmdContext = iswInterface->GetNextCmdCount();
    getTestModeCmd->iswCmd.reserved   = 0;
    getTestModeCmd->peerIndex         = peerIndex;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswGetTestModeCommand);

    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(getTestModeCmd->iswCmd.cmdType) << std::endl;
        ss << "    command    = " << std::to_string(getTestModeCmd->iswCmd.command) << std::endl;
        ss << "    cmdContext = " << std::to_string(getTestModeCmd->iswCmd.cmdContext) << std::endl;
        ss << "    reserved   = " << std::to_string(getTestModeCmd->iswCmd.reserved) << std::endl;
        ss << "    peerIndex  = " << std::to_string(getTestModeCmd->peerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!##########################################################
//! SendGetTestModeCmd()
//! Send the ISW command SendGetTestModeCmd to the firmware
//!##########################################################
int OnBoard::SendGetTestModeCmd(uint8_t peerIndex)
{
    iswOnBoardCmdLock.lock();

    std::string msgString;
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        msgString = "SendGetTestModeCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupGetTestModeCmd(&iswCmd.getOnBoardTestModeCmd, peerIndex, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_OnBoard == 1 )
        {
            msgString = "SendGetTestModeCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    iswOnBoardCmdLock.unlock();

    return ( status );
}

//!##########################################################
//! SetupStartTestCmd()
//! Setup the ISW headers for the SendStartTestCmd
//!##########################################################
void OnBoard::SetupStartTestCmd(iswStartTestCommand *startTestCmd, uint8_t peerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::string msgString;
        msgString = "SetupStartTestCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    startTestCmd->iswCmd.cmdType    = IswInterface::OnBoardTests;
    startTestCmd->iswCmd.command    = StartTest;
    startTestCmd->iswCmd.cmdContext = iswInterface->GetNextCmdCount();
    startTestCmd->iswCmd.reserved   = 0;
    startTestCmd->peerIndex         = peerIndex;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStartTestCommand);

    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(startTestCmd->iswCmd.cmdType) << std::endl;
        ss << "    command    = " << std::to_string(startTestCmd->iswCmd.command) << std::endl;
        ss << "    cmdContext = " << std::to_string(startTestCmd->iswCmd.cmdContext) << std::endl;
        ss << "    reserved   = " << std::to_string(startTestCmd->iswCmd.reserved) << std::endl;
        ss << "    peerIndex  = " << std::to_string(startTestCmd->peerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!##########################################################
//! SendStartTestCmd()
//! Send the ISW command SendStartTestCmd to the firmware
//!##########################################################
int OnBoard::SendStartTestCmd(uint8_t peerIndex)
{
    iswOnBoardCmdLock.lock();

    std::string msgString;
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        msgString = "SendStartTestCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupStartTestCmd(&iswCmd.startTestCommand, peerIndex, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_OnBoard == 1 )
        {
            msgString = "SendStartTestCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    iswOnBoardCmdLock.unlock();

    return ( status );
}

//!##########################################################
//! SetupStopTestCmd()
//! Setup the ISW headers for the SendStopTestCmd
//!##########################################################
void OnBoard::SetupStopTestCmd(iswStopTestCommand *stopTestCmd, uint8_t peerIndex, void *routingHdr)
{
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::string msgString;
        msgString = "SetupStopTestCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }

    stopTestCmd->iswCmd.cmdType    = IswInterface::OnBoardTests;
    stopTestCmd->iswCmd.command    = StopTest;
    stopTestCmd->iswCmd.cmdContext = iswInterface->GetNextCmdCount();
    stopTestCmd->iswCmd.reserved   = 0;
    stopTestCmd->peerIndex         = peerIndex;

    //! Add the ISW Routing Header to the buffer
    IswInterface::iswRoutingHdr *routingHdrPtr = (IswInterface::iswRoutingHdr *)routingHdr;
    routingHdrPtr->srcAddr                     = 1;
    routingHdrPtr->destAddr                    = 1;
    routingHdrPtr->dataLength                  = sizeof(iswStopTestCommand);

    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        std::stringstream ss;
        ss << "    cmdType    = " << std::to_string(stopTestCmd->iswCmd.cmdType) << std::endl;
        ss << "    command    = " << std::to_string(stopTestCmd->iswCmd.command) << std::endl;
        ss << "    cmdContext = " << std::to_string(stopTestCmd->iswCmd.cmdContext) << std::endl;
        ss << "    reserved   = " << std::to_string(stopTestCmd->iswCmd.reserved) << std::endl;
        ss << "    peerIndex  = " << std::to_string(stopTestCmd->peerIndex) << std::endl;
        theLogger->DebugMessage(ss.str(), iswInterface->GetIndex());
    }
}

//!##########################################################
//! SendStopTestCmd()
//! Send the ISW command SendStopTestCmd to the firmware
//!##########################################################
int OnBoard::SendStopTestCmd(uint8_t peerIndex)
{
    iswOnBoardCmdLock.lock();

    std::string msgString;
    if ( theLogger->DEBUG_OnBoard == 1 )
    {
        msgString = "SendStopTestCmd";
        theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
    }
    int status = 0;

    IswInterface::iswCommand iswCmd;
    IswInterface::iswRoutingHdr routingHdr;
    SetupStopTestCmd(&iswCmd.stopTestCommand, peerIndex, &routingHdr);
    status = iswInterface->SyncSendIswCmd(&iswCmd, &routingHdr);

    if ( status < 0 )
    {
        if ( theLogger->DEBUG_OnBoard == 1 )
        {
            msgString = "SendStopTestCmd failed";
            theLogger->LogMessage(msgString, iswInterface->GetIndex(), std::chrono::system_clock::now());
        }
    }

    iswOnBoardCmdLock.unlock();

    return ( status );
}
