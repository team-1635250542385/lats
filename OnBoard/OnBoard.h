//!#################################################
//! Filename: OnBoard.h
//! Description: Class that provides an interface
//!              to the on board tests that run
//!              on some test boards from the vendor
//!
//! Controlled by: PEO Soldier
//! Controlled By: PdM-SMS
//! CUI Category: CTI, EXPT
//! Distribution Statement: D
//! POC: Matthew G. Hunter, 703-704-1914
//! Export Notice: This document contains technical data that is subject to either the International Traffic in Arms Regulations (22 CFR
//! 120-130) or the Export Administration Regulations (15 CFR 730-774). U.S. government authorization may be required to transfer this data to foreign persons.
//! DISTRIBUTION STATEMENT D. Distribution authorized to the Department of Defense and U.S. DoD contractors only for administration and
//! operational use of technical data. Date of determination is 9/27/18. Other requests for this document shall be referred to Product Manager - Soldier Maneuver Sensors, 10125 Kingman Road, Building 317, ATTN: SFAE-SDR-SMPT-SMS, Fort Belvoir, VA 22606-5800.
//!################################################
#ifndef ONBOARDINTERFACE_H
#define ONBOARDINTERFACE_H

#include <stdint.h>
#include <mutex>
#include "../Logger/Logger.h"

class IswInterface;

class OnBoard
{
public:
    OnBoard(IswInterface *isw_interface, Logger *logger);
    ~OnBoard();

    Logger *theLogger = nullptr;

    //! OnBoard tests run on several test boards
    //! supplied by the vendor. These tests are not
    //! inniated on the host, like PacketGenerator
    //! but run from board to board. The host application
    //! sends special commands in the form of ISW commands
    //! to the firmware to configure and start/stop these
    //! tests.  This is the traditional ISW command format.
    struct iswGeneralCommand
    {
        uint8_t cmdType;
        uint8_t command;
        uint8_t cmdContext;
        uint8_t reserved;
    }__attribute__((packed));

    //! Set Test Mode Command
    //! Header Command Type=0x01, Command ID=0x07
    //! To configure the test  with user input for:
    //! - desired throughput
    //! - transfer or packet size
    //! - what peer to send to
    //! - reserved for future
    struct iswSetTestModeCommand
    {
        iswGeneralCommand iswCmd;
        uint16_t throughput;       // Throughput in Mbps you want to send
        uint16_t transferSize;     // Size of packet. Commander hard-codes this to 4064
        uint8_t peerIndex;         // Index of peer you want to send data to
        uint8_t reserved1[3];
    }__attribute__((packed));

    //! The user application can call these to
    //! get the status of the OnBoard test setup.
    //! textModeSet is true when the firmware returns
    //! successful statue from the SetTestModeCommand
    bool isTestModeSet(void) { return (testModeSet); }

    //! testIsRunning is true while the device is sending data
    bool isTestRunning(void) { return (testIsRunning); }

    //! Test Mode Get Command
    //! Header Command Type=0x01, Command ID=0x08
    struct iswGetTestModeCommand
    {
        iswGeneralCommand iswCmd;
        uint8_t peerIndex;  // Index of peer you want to get test info for
        uint8_t reserved1[3];
    };

    //! Test Start Command
    //! Header Command Type=0x01, Command ID=0x09
    struct iswStartTestCommand
    {
        iswGeneralCommand iswCmd;
        uint8_t peerIndex;  //! peer index you want to start traffic for
        uint8_t reserved1[3];
    };

    //! Test Stop Command
    //! Header Command Type=0x01, Command ID=0x0A
    struct iswStopTestCommand
    {
        iswGeneralCommand iswCmd;
        uint8_t peerIndex;  //! peer index for which you want to stop traffic
        uint8_t reserved1[3];
    };

    uint8_t GetIndex(void);

    //! Used like all ISW API classes to deliver an incoming
    //! message from the firmware.
    void DeliverIswMessage(uint8_t cmd, uint8_t *data);

    //! Used like all ISW API classes to send commands to the firmware
    int SendSetTestModeCmd(uint8_t peerIndex, uint16_t pktSize, uint16_t throughput);
    int SendGetTestModeCmd(uint8_t peerIndex);
    int SendStartTestCmd(uint8_t peerIndex);
    int SendStopTestCmd(uint8_t peerIndex);

private:
    IswInterface *iswInterface = nullptr;
    std::mutex iswOnBoardCmdLock;
    bool testModeSet = false;
    bool testIsRunning = false;

    //! OnBoard Test commands
    enum iswOnBoardCmdEventTypes: uint8_t
    {
        SetTestMode             = 0x07,
        GetTestMode             = 0x08,
        StartTest               = 0x09,
        StopTest                = 0x0A
    };

    //! Test Mode Get Response
    //! Header Command Type=0x01, Command ID=0x08
    struct iswGetTestModeCmdResponse
    {
        uint16_t throughput;    //! Throughput in Mbps that will be sent
        uint16_t transferSize;  //! Size of packets that will be sent (4064)
        uint8_t running;        //! Whether packets are being generated currently
        uint8_t reserved[3];
    };

    void SetupSetTestModeCmd(iswSetTestModeCommand *setTestModeCmd, uint8_t peerIndex, uint16_t pktSize, uint16_t  throughput, void *routingHdr);
    void SetupGetTestModeCmd(iswGetTestModeCommand *getTestModeCmd, uint8_t peerIndex, void *routingHdr);
    void SetupStartTestCmd(iswStartTestCommand *startTestCmd, uint8_t peerIndex, void *routingHdr);
    void SetupStopTestCmd(iswStopTestCommand *stopTestCmd, uint8_t peerIndex, void *routingHdr);
};

#endif // ONBOARDINTERFACE_H
