/********************************************************************************
** Form generated from reading UI file 'iswtesttool.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ISWTESTTOOL_H
#define UI_ISWTESTTOOL_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_iswTestTool
{
public:
    QAction *actionExit;
    QAction *actionUpdate_Firmware;
    QAction *actionSave;
    QAction *actionSave_2;
    QAction *actionLoad_Scenario;
    QAction *actionAttenuator_Control;
    QAction *actionOpen;
    QAction *actionClose_Database;
    QAction *actionService_Descriptor_Configuration;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_21;
    QVBoxLayout *verticalLayout_20;
    QTabWidget *tabWidget;
    QWidget *tab;
    QListWidget *netDeviceList;
    QGroupBox *groupBox;
    QLabel *macAddressLabel;
    QLabel *coordPriorityLabel;
    QLabel *devTypeLabel;
    QLabel *coordPeerLabel;
    QLineEdit *macAddressEdit;
    QLineEdit *coordPriorityEdit;
    QLineEdit *devTypeEdit;
    QLineEdit *coordPeerEdit;
    QLineEdit *firmwareEdit;
    QLabel *firmwareLabel;
    QLineEdit *channelEdit;
    QLabel *channelLabel;
    QLabel *peersLabel;
    QLineEdit *peerEdit;
    QLabel *phyRateLabel;
    QLineEdit *phyRateEdit;
    QLabel *netIdLabel;
    QLineEdit *netIdEdit;
    QGraphicsView *netTopologyGraphicsView;
    QLabel *netDevLabel;
    QLabel *netTopologyLabel;
    QPushButton *scanButton;
    QListWidget *peerListWidget;
    QLabel *label;
    QPushButton *updateNetTopologyButton;
    QLabel *mcastAddrLabel;
    QLineEdit *mcastAddrLineEdit;
    QLabel *connectionTimeLabel;
    QLineEdit *connectionTimeEdit;
    QWidget *tab_4;
    QWidget *verticalLayoutWidget_22;
    QVBoxLayout *verticalLayout_22;
    QLabel *label_4;
    QWidget *verticalLayoutWidget_23;
    QVBoxLayout *verticalLayout_23;
    QComboBox *deviceComboBox;
    QWidget *verticalLayoutWidget_24;
    QVBoxLayout *verticalLayout_24;
    QPushButton *printServicesPushButton;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QTreeWidget *serviceDescTreeWidget;
    QWidget *verticalLayoutWidget_26;
    QVBoxLayout *verticalLayout_26;
    QLabel *label_7;
    QWidget *verticalLayoutWidget_27;
    QVBoxLayout *verticalLayout_27;
    QPlainTextEdit *serviceDescriptorPlainTextEdit;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *clearServiceDescriptorTextPushButton;
    QPushButton *saveServiceDescriptorTextPushButton;
    QWidget *tab_5_registerServices;
    QWidget *verticalLayoutWidget_20;
    QVBoxLayout *verticalLayout_regServices;
    QTableWidget *registerServicesTable;
    QWidget *tab_7;
    QLabel *activeLinksLabel;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QTableWidget *availableLinksTable;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QGraphicsView *dataPlotGraphicsView_2;
    QLabel *activeLinksLabel_2;
    QWidget *tab_3;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_3;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *rssiCheckBox;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_6;
    QCheckBox *perCheckBox;
    QWidget *verticalLayoutWidget_7;
    QVBoxLayout *verticalLayout_7;
    QCheckBox *transmitCheckBox;
    QWidget *verticalLayoutWidget_8;
    QVBoxLayout *verticalLayout_8;
    QCheckBox *receiveCheckBox;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QGraphicsView *dataPlotGraphicsView;
    QLabel *dataPlotLabel;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_5;
    QLCDNumber *dataThroughputLcdNumber;
    QWidget *verticalLayoutWidget_9;
    QVBoxLayout *verticalLayout_9;
    QComboBox *activeLinksComboBox;
    QLabel *label_6;
    QPushButton *savePlotPushButton;
    QWidget *tab_8;
    QWidget *verticalLayoutWidget_10;
    QVBoxLayout *verticalLayout_10;
    QGroupBox *groupBox_2;
    QWidget *verticalLayoutWidget_11;
    QVBoxLayout *verticalLayout_11;
    QCheckBox *voltageCheckBox;
    QWidget *verticalLayoutWidget_12;
    QVBoxLayout *verticalLayout_12;
    QCheckBox *currentCheckBox;
    QWidget *verticalLayoutWidget_14;
    QVBoxLayout *verticalLayout_14;
    QCheckBox *powerCheckBox;
    QGraphicsView *powerMeasurementsGraphicsView;
    QWidget *verticalLayoutWidget_13;
    QVBoxLayout *verticalLayout_13;
    QGroupBox *groupBox_6;
    QWidget *verticalLayoutWidget_15;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_9;
    QWidget *verticalLayoutWidget_16;
    QVBoxLayout *verticalLayout_16;
    QLCDNumber *voltageLcdNumber;
    QWidget *verticalLayoutWidget_17;
    QVBoxLayout *verticalLayout_17;
    QLCDNumber *currentLcdNumber;
    QWidget *verticalLayoutWidget_18;
    QVBoxLayout *verticalLayout_18;
    QLCDNumber *powerLcdNumber;
    QWidget *verticalLayoutWidget_19;
    QVBoxLayout *verticalLayout_19;
    QLabel *label_10;
    QWidget *verticalLayoutWidget_25;
    QVBoxLayout *verticalLayout_25;
    QLabel *label_11;
    QPushButton *savePowerPlotPushButton;
    QPushButton *pwrMeasurementStartBtn;
    QScrollBar *powerMeasurementScrollBar;
    QWidget *tab_2;
    QPushButton *loadScenarioButton;
    QPushButton *saveScenarioButton;
    QPushButton *startScenarioButton;
    QPushButton *endScenarioButton;
    QPushButton *editScenarioButton;
    QPlainTextEdit *testAutomationPlainTextEdit;
    QPushButton *solnetValidationButton;
    QPushButton *clearTestAutomationTextPushButton;
    QPushButton *saveTestAutomationTextPushButton;
    QPushButton *apiComplianceButton;
    QWidget *tab_6;
    QLineEdit *firmwareDeviceEdit;
    QGroupBox *groupBox_4;
    QLabel *label_2;
    QLineEdit *firmwareUpdateEdit;
    QPushButton *updateFirmwareButton;
    QToolButton *firmwareDialogToolButton;
    QGroupBox *groupBox_5;
    QLabel *label_3;
    QLineEdit *activeFirmwareEdit;
    QLabel *firmwareDeviceLabel;
    QPushButton *cancelPushButton;
    QTextEdit *firmwareUpdateTextEdit;
    QPushButton *saveUpdateFirmwareTextPushButton;
    QPushButton *clearUpdateFirmwareTextPushButton;
    QLabel *label_8;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuOptions;
    QMenu *menuDatabase_Connection;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *iswTestTool)
    {
        if (iswTestTool->objectName().isEmpty())
            iswTestTool->setObjectName(QString::fromUtf8("iswTestTool"));
        iswTestTool->resize(1432, 688);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(iswTestTool->sizePolicy().hasHeightForWidth());
        iswTestTool->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../../../Desktop/C5ISR_Image.png"), QSize(), QIcon::Normal, QIcon::Off);
        iswTestTool->setWindowIcon(icon);
        actionExit = new QAction(iswTestTool);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionUpdate_Firmware = new QAction(iswTestTool);
        actionUpdate_Firmware->setObjectName(QString::fromUtf8("actionUpdate_Firmware"));
        actionSave = new QAction(iswTestTool);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave_2 = new QAction(iswTestTool);
        actionSave_2->setObjectName(QString::fromUtf8("actionSave_2"));
        actionLoad_Scenario = new QAction(iswTestTool);
        actionLoad_Scenario->setObjectName(QString::fromUtf8("actionLoad_Scenario"));
        actionAttenuator_Control = new QAction(iswTestTool);
        actionAttenuator_Control->setObjectName(QString::fromUtf8("actionAttenuator_Control"));
        actionOpen = new QAction(iswTestTool);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionClose_Database = new QAction(iswTestTool);
        actionClose_Database->setObjectName(QString::fromUtf8("actionClose_Database"));
        actionService_Descriptor_Configuration = new QAction(iswTestTool);
        actionService_Descriptor_Configuration->setObjectName(QString::fromUtf8("actionService_Descriptor_Configuration"));
        centralWidget = new QWidget(iswTestTool);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayout_21 = new QVBoxLayout(centralWidget);
        verticalLayout_21->setSpacing(6);
        verticalLayout_21->setContentsMargins(11, 11, 11, 11);
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        verticalLayout_20 = new QVBoxLayout();
        verticalLayout_20->setSpacing(6);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        tabWidget->setFont(font);
        tabWidget->setMovable(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tab->sizePolicy().hasHeightForWidth());
        tab->setSizePolicy(sizePolicy1);
        netDeviceList = new QListWidget(tab);
        netDeviceList->setObjectName(QString::fromUtf8("netDeviceList"));
        netDeviceList->setEnabled(true);
        netDeviceList->setGeometry(QRect(23, 40, 201, 221));
        netDeviceList->setFont(font);
        netDeviceList->viewport()->setProperty("cursor", QVariant(QCursor(Qt::ArrowCursor)));
        netDeviceList->setContextMenuPolicy(Qt::CustomContextMenu);
        netDeviceList->setAcceptDrops(false);
        netDeviceList->setDragEnabled(false);
        netDeviceList->setDragDropMode(QAbstractItemView::NoDragDrop);
        netDeviceList->setDefaultDropAction(Qt::IgnoreAction);
        netDeviceList->setAlternatingRowColors(true);
        netDeviceList->setSortingEnabled(false);
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(23, 310, 489, 260));
        groupBox->setFont(font);
        macAddressLabel = new QLabel(groupBox);
        macAddressLabel->setObjectName(QString::fromUtf8("macAddressLabel"));
        macAddressLabel->setGeometry(QRect(16, 30, 91, 16));
        coordPriorityLabel = new QLabel(groupBox);
        coordPriorityLabel->setObjectName(QString::fromUtf8("coordPriorityLabel"));
        coordPriorityLabel->setGeometry(QRect(16, 60, 141, 16));
        devTypeLabel = new QLabel(groupBox);
        devTypeLabel->setObjectName(QString::fromUtf8("devTypeLabel"));
        devTypeLabel->setGeometry(QRect(16, 90, 91, 16));
        coordPeerLabel = new QLabel(groupBox);
        coordPeerLabel->setObjectName(QString::fromUtf8("coordPeerLabel"));
        coordPeerLabel->setGeometry(QRect(18, 120, 291, 16));
        macAddressEdit = new QLineEdit(groupBox);
        macAddressEdit->setObjectName(QString::fromUtf8("macAddressEdit"));
        macAddressEdit->setEnabled(true);
        macAddressEdit->setGeometry(QRect(117, 29, 151, 20));
        macAddressEdit->setStyleSheet(QString::fromUtf8("border-color: rgb(0, 0, 0);"));
        macAddressEdit->setReadOnly(true);
        macAddressEdit->setClearButtonEnabled(false);
        coordPriorityEdit = new QLineEdit(groupBox);
        coordPriorityEdit->setObjectName(QString::fromUtf8("coordPriorityEdit"));
        coordPriorityEdit->setGeometry(QRect(157, 59, 101, 20));
        coordPriorityEdit->setAcceptDrops(false);
        coordPriorityEdit->setReadOnly(true);
        devTypeEdit = new QLineEdit(groupBox);
        devTypeEdit->setObjectName(QString::fromUtf8("devTypeEdit"));
        devTypeEdit->setGeometry(QRect(107, 89, 281, 20));
        devTypeEdit->setReadOnly(true);
        coordPeerEdit = new QLineEdit(groupBox);
        coordPeerEdit->setObjectName(QString::fromUtf8("coordPeerEdit"));
        coordPeerEdit->setEnabled(true);
        coordPeerEdit->setGeometry(QRect(58, 118, 113, 20));
        coordPeerEdit->setReadOnly(true);
        firmwareEdit = new QLineEdit(groupBox);
        firmwareEdit->setObjectName(QString::fromUtf8("firmwareEdit"));
        firmwareEdit->setEnabled(true);
        firmwareEdit->setGeometry(QRect(140, 148, 151, 20));
        firmwareEdit->setReadOnly(true);
        firmwareEdit->setClearButtonEnabled(false);
        firmwareLabel = new QLabel(groupBox);
        firmwareLabel->setObjectName(QString::fromUtf8("firmwareLabel"));
        firmwareLabel->setGeometry(QRect(16, 150, 121, 16));
        channelEdit = new QLineEdit(groupBox);
        channelEdit->setObjectName(QString::fromUtf8("channelEdit"));
        channelEdit->setGeometry(QRect(150, 177, 251, 21));
        channelEdit->setReadOnly(true);
        channelLabel = new QLabel(groupBox);
        channelLabel->setObjectName(QString::fromUtf8("channelLabel"));
        channelLabel->setGeometry(QRect(17, 180, 121, 16));
        peersLabel = new QLabel(groupBox);
        peersLabel->setObjectName(QString::fromUtf8("peersLabel"));
        peersLabel->setGeometry(QRect(300, 29, 71, 20));
        peerEdit = new QLineEdit(groupBox);
        peerEdit->setObjectName(QString::fromUtf8("peerEdit"));
        peerEdit->setGeometry(QRect(382, 30, 91, 21));
        peerEdit->setReadOnly(true);
        phyRateLabel = new QLabel(groupBox);
        phyRateLabel->setObjectName(QString::fromUtf8("phyRateLabel"));
        phyRateLabel->setGeometry(QRect(300, 57, 71, 20));
        phyRateEdit = new QLineEdit(groupBox);
        phyRateEdit->setObjectName(QString::fromUtf8("phyRateEdit"));
        phyRateEdit->setGeometry(QRect(370, 56, 101, 21));
        phyRateEdit->setReadOnly(true);
        netIdLabel = new QLabel(groupBox);
        netIdLabel->setObjectName(QString::fromUtf8("netIdLabel"));
        netIdLabel->setGeometry(QRect(17, 210, 91, 20));
        netIdEdit = new QLineEdit(groupBox);
        netIdEdit->setObjectName(QString::fromUtf8("netIdEdit"));
        netIdEdit->setGeometry(QRect(110, 210, 91, 21));
        netIdEdit->setReadOnly(true);
        netTopologyGraphicsView = new QGraphicsView(tab);
        netTopologyGraphicsView->setObjectName(QString::fromUtf8("netTopologyGraphicsView"));
        netTopologyGraphicsView->setGeometry(QRect(510, 40, 881, 491));
        netTopologyGraphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
        netDevLabel = new QLabel(tab);
        netDevLabel->setObjectName(QString::fromUtf8("netDevLabel"));
        netDevLabel->setGeometry(QRect(24, 23, 130, 16));
        netDevLabel->setFont(font);
        netTopologyLabel = new QLabel(tab);
        netTopologyLabel->setObjectName(QString::fromUtf8("netTopologyLabel"));
        netTopologyLabel->setGeometry(QRect(510, 18, 131, 21));
        netTopologyLabel->setFont(font);
        scanButton = new QPushButton(tab);
        scanButton->setObjectName(QString::fromUtf8("scanButton"));
        scanButton->setGeometry(QRect(51, 270, 141, 28));
        peerListWidget = new QListWidget(tab);
        peerListWidget->setObjectName(QString::fromUtf8("peerListWidget"));
        peerListWidget->setGeometry(QRect(240, 40, 261, 221));
        peerListWidget->setFont(font);
        peerListWidget->setDefaultDropAction(Qt::IgnoreAction);
        peerListWidget->setAlternatingRowColors(true);
        peerListWidget->setSelectionMode(QAbstractItemView::NoSelection);
        peerListWidget->setSortingEnabled(true);
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(240, 20, 63, 20));
        label->setFont(font);
        updateNetTopologyButton = new QPushButton(tab);
        updateNetTopologyButton->setObjectName(QString::fromUtf8("updateNetTopologyButton"));
        updateNetTopologyButton->setGeometry(QRect(1160, 540, 201, 28));
        updateNetTopologyButton->setFont(font);
        mcastAddrLabel = new QLabel(tab);
        mcastAddrLabel->setObjectName(QString::fromUtf8("mcastAddrLabel"));
        mcastAddrLabel->setGeometry(QRect(521, 544, 131, 20));
        mcastAddrLabel->setFont(font);
        mcastAddrLineEdit = new QLineEdit(tab);
        mcastAddrLineEdit->setObjectName(QString::fromUtf8("mcastAddrLineEdit"));
        mcastAddrLineEdit->setGeometry(QRect(650, 543, 101, 20));
        mcastAddrLineEdit->setReadOnly(true);
        connectionTimeLabel = new QLabel(tab);
        connectionTimeLabel->setObjectName(QString::fromUtf8("connectionTimeLabel"));
        connectionTimeLabel->setGeometry(QRect(790, 544, 181, 20));
        connectionTimeEdit = new QLineEdit(tab);
        connectionTimeEdit->setObjectName(QString::fromUtf8("connectionTimeEdit"));
        connectionTimeEdit->setEnabled(true);
        connectionTimeEdit->setGeometry(QRect(977, 542, 91, 20));
        connectionTimeEdit->setStyleSheet(QString::fromUtf8("border-color: rgb(0, 0, 0);"));
        connectionTimeEdit->setReadOnly(true);
        connectionTimeEdit->setClearButtonEnabled(false);
        tabWidget->addTab(tab, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        verticalLayoutWidget_22 = new QWidget(tab_4);
        verticalLayoutWidget_22->setObjectName(QString::fromUtf8("verticalLayoutWidget_22"));
        verticalLayoutWidget_22->setGeometry(QRect(10, 10, 251, 31));
        verticalLayout_22 = new QVBoxLayout(verticalLayoutWidget_22);
        verticalLayout_22->setSpacing(6);
        verticalLayout_22->setContentsMargins(11, 11, 11, 11);
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        verticalLayout_22->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(verticalLayoutWidget_22);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font);

        verticalLayout_22->addWidget(label_4);

        verticalLayoutWidget_23 = new QWidget(tab_4);
        verticalLayoutWidget_23->setObjectName(QString::fromUtf8("verticalLayoutWidget_23"));
        verticalLayoutWidget_23->setGeometry(QRect(10, 40, 251, 41));
        verticalLayout_23 = new QVBoxLayout(verticalLayoutWidget_23);
        verticalLayout_23->setSpacing(6);
        verticalLayout_23->setContentsMargins(11, 11, 11, 11);
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        verticalLayout_23->setContentsMargins(0, 0, 0, 0);
        deviceComboBox = new QComboBox(verticalLayoutWidget_23);
        deviceComboBox->setObjectName(QString::fromUtf8("deviceComboBox"));
        deviceComboBox->setFont(font);

        verticalLayout_23->addWidget(deviceComboBox);

        verticalLayoutWidget_24 = new QWidget(tab_4);
        verticalLayoutWidget_24->setObjectName(QString::fromUtf8("verticalLayoutWidget_24"));
        verticalLayoutWidget_24->setGeometry(QRect(50, 80, 160, 51));
        verticalLayout_24 = new QVBoxLayout(verticalLayoutWidget_24);
        verticalLayout_24->setSpacing(6);
        verticalLayout_24->setContentsMargins(11, 11, 11, 11);
        verticalLayout_24->setObjectName(QString::fromUtf8("verticalLayout_24"));
        verticalLayout_24->setContentsMargins(0, 0, 0, 0);
        printServicesPushButton = new QPushButton(verticalLayoutWidget_24);
        printServicesPushButton->setObjectName(QString::fromUtf8("printServicesPushButton"));

        verticalLayout_24->addWidget(printServicesPushButton);

        horizontalLayoutWidget_2 = new QWidget(tab_4);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(279, 9, 301, 541));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        serviceDescTreeWidget = new QTreeWidget(horizontalLayoutWidget_2);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        serviceDescTreeWidget->setHeaderItem(__qtreewidgetitem);
        serviceDescTreeWidget->setObjectName(QString::fromUtf8("serviceDescTreeWidget"));
        serviceDescTreeWidget->setFont(font);
        serviceDescTreeWidget->setAlternatingRowColors(true);

        horizontalLayout_2->addWidget(serviceDescTreeWidget);

        verticalLayoutWidget_26 = new QWidget(tab_4);
        verticalLayoutWidget_26->setObjectName(QString::fromUtf8("verticalLayoutWidget_26"));
        verticalLayoutWidget_26->setGeometry(QRect(610, 10, 160, 22));
        verticalLayout_26 = new QVBoxLayout(verticalLayoutWidget_26);
        verticalLayout_26->setSpacing(6);
        verticalLayout_26->setContentsMargins(11, 11, 11, 11);
        verticalLayout_26->setObjectName(QString::fromUtf8("verticalLayout_26"));
        verticalLayout_26->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(verticalLayoutWidget_26);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        verticalLayout_26->addWidget(label_7);

        verticalLayoutWidget_27 = new QWidget(tab_4);
        verticalLayoutWidget_27->setObjectName(QString::fromUtf8("verticalLayoutWidget_27"));
        verticalLayoutWidget_27->setGeometry(QRect(609, 39, 781, 461));
        verticalLayout_27 = new QVBoxLayout(verticalLayoutWidget_27);
        verticalLayout_27->setSpacing(6);
        verticalLayout_27->setContentsMargins(11, 11, 11, 11);
        verticalLayout_27->setObjectName(QString::fromUtf8("verticalLayout_27"));
        verticalLayout_27->setContentsMargins(0, 0, 0, 0);
        serviceDescriptorPlainTextEdit = new QPlainTextEdit(verticalLayoutWidget_27);
        serviceDescriptorPlainTextEdit->setObjectName(QString::fromUtf8("serviceDescriptorPlainTextEdit"));
        serviceDescriptorPlainTextEdit->setFont(font);
        serviceDescriptorPlainTextEdit->setReadOnly(true);

        verticalLayout_27->addWidget(serviceDescriptorPlainTextEdit);

        horizontalLayoutWidget_3 = new QWidget(tab_4);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(610, 500, 168, 61));
        horizontalLayout_4 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        clearServiceDescriptorTextPushButton = new QPushButton(horizontalLayoutWidget_3);
        clearServiceDescriptorTextPushButton->setObjectName(QString::fromUtf8("clearServiceDescriptorTextPushButton"));
        clearServiceDescriptorTextPushButton->setFont(font);

        horizontalLayout_4->addWidget(clearServiceDescriptorTextPushButton);

        saveServiceDescriptorTextPushButton = new QPushButton(horizontalLayoutWidget_3);
        saveServiceDescriptorTextPushButton->setObjectName(QString::fromUtf8("saveServiceDescriptorTextPushButton"));
        saveServiceDescriptorTextPushButton->setFont(font);

        horizontalLayout_4->addWidget(saveServiceDescriptorTextPushButton);

        tabWidget->addTab(tab_4, QString());
        tab_5_registerServices = new QWidget();
        tab_5_registerServices->setObjectName(QString::fromUtf8("tab_5_registerServices"));
        verticalLayoutWidget_20 = new QWidget(tab_5_registerServices);
        verticalLayoutWidget_20->setObjectName(QString::fromUtf8("verticalLayoutWidget_20"));
        verticalLayoutWidget_20->setGeometry(QRect(10, 10, 1391, 551));
        verticalLayout_regServices = new QVBoxLayout(verticalLayoutWidget_20);
        verticalLayout_regServices->setSpacing(6);
        verticalLayout_regServices->setContentsMargins(11, 11, 11, 11);
        verticalLayout_regServices->setObjectName(QString::fromUtf8("verticalLayout_regServices"));
        verticalLayout_regServices->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_regServices->setContentsMargins(0, 0, 0, 0);
        registerServicesTable = new QTableWidget(verticalLayoutWidget_20);
        registerServicesTable->setObjectName(QString::fromUtf8("registerServicesTable"));
        sizePolicy.setHeightForWidth(registerServicesTable->sizePolicy().hasHeightForWidth());
        registerServicesTable->setSizePolicy(sizePolicy);
        registerServicesTable->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        registerServicesTable->setAlternatingRowColors(false);
        registerServicesTable->setIconSize(QSize(0, 0));
        registerServicesTable->setCornerButtonEnabled(false);
        registerServicesTable->setRowCount(0);
        registerServicesTable->setColumnCount(0);
        registerServicesTable->horizontalHeader()->setVisible(false);
        registerServicesTable->horizontalHeader()->setCascadingSectionResizes(true);
        registerServicesTable->horizontalHeader()->setMinimumSectionSize(80);
        registerServicesTable->horizontalHeader()->setDefaultSectionSize(182);
        registerServicesTable->verticalHeader()->setVisible(false);
        registerServicesTable->verticalHeader()->setCascadingSectionResizes(true);
        registerServicesTable->verticalHeader()->setMinimumSectionSize(80);
        registerServicesTable->verticalHeader()->setDefaultSectionSize(160);

        verticalLayout_regServices->addWidget(registerServicesTable);

        tabWidget->addTab(tab_5_registerServices, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        activeLinksLabel = new QLabel(tab_7);
        activeLinksLabel->setObjectName(QString::fromUtf8("activeLinksLabel"));
        activeLinksLabel->setGeometry(QRect(23, 10, 181, 20));
        activeLinksLabel->setFont(font);
        verticalLayoutWidget_2 = new QWidget(tab_7);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(20, 30, 911, 531));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        availableLinksTable = new QTableWidget(verticalLayoutWidget_2);
        availableLinksTable->setObjectName(QString::fromUtf8("availableLinksTable"));
        availableLinksTable->setFont(font);
        availableLinksTable->setAlternatingRowColors(true);
        availableLinksTable->setGridStyle(Qt::DashLine);
        availableLinksTable->setRowCount(0);

        verticalLayout_2->addWidget(availableLinksTable);

        horizontalLayoutWidget = new QWidget(tab_7);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(949, 30, 441, 531));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        dataPlotGraphicsView_2 = new QGraphicsView(horizontalLayoutWidget);
        dataPlotGraphicsView_2->setObjectName(QString::fromUtf8("dataPlotGraphicsView_2"));

        horizontalLayout->addWidget(dataPlotGraphicsView_2);

        activeLinksLabel_2 = new QLabel(tab_7);
        activeLinksLabel_2->setObjectName(QString::fromUtf8("activeLinksLabel_2"));
        activeLinksLabel_2->setGeometry(QRect(950, 10, 151, 20));
        activeLinksLabel_2->setFont(font);
        tabWidget->addTab(tab_7, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayoutWidget_3 = new QWidget(tab_3);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(30, 110, 251, 191));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox_3 = new QGroupBox(verticalLayoutWidget_3);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setFont(font);
        verticalLayoutWidget_5 = new QWidget(groupBox_3);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(20, 40, 201, 31));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        rssiCheckBox = new QCheckBox(verticalLayoutWidget_5);
        rssiCheckBox->setObjectName(QString::fromUtf8("rssiCheckBox"));
        rssiCheckBox->setFont(font);

        verticalLayout_5->addWidget(rssiCheckBox);

        verticalLayoutWidget_6 = new QWidget(groupBox_3);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(20, 70, 201, 31));
        verticalLayout_6 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        perCheckBox = new QCheckBox(verticalLayoutWidget_6);
        perCheckBox->setObjectName(QString::fromUtf8("perCheckBox"));
        perCheckBox->setFont(font);

        verticalLayout_6->addWidget(perCheckBox);

        verticalLayoutWidget_7 = new QWidget(groupBox_3);
        verticalLayoutWidget_7->setObjectName(QString::fromUtf8("verticalLayoutWidget_7"));
        verticalLayoutWidget_7->setGeometry(QRect(20, 100, 201, 31));
        verticalLayout_7 = new QVBoxLayout(verticalLayoutWidget_7);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        transmitCheckBox = new QCheckBox(verticalLayoutWidget_7);
        transmitCheckBox->setObjectName(QString::fromUtf8("transmitCheckBox"));
        transmitCheckBox->setFont(font);

        verticalLayout_7->addWidget(transmitCheckBox);

        verticalLayoutWidget_8 = new QWidget(groupBox_3);
        verticalLayoutWidget_8->setObjectName(QString::fromUtf8("verticalLayoutWidget_8"));
        verticalLayoutWidget_8->setGeometry(QRect(20, 130, 201, 31));
        verticalLayout_8 = new QVBoxLayout(verticalLayoutWidget_8);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        receiveCheckBox = new QCheckBox(verticalLayoutWidget_8);
        receiveCheckBox->setObjectName(QString::fromUtf8("receiveCheckBox"));
        receiveCheckBox->setFont(font);

        verticalLayout_8->addWidget(receiveCheckBox);


        verticalLayout_3->addWidget(groupBox_3);

        verticalLayoutWidget_4 = new QWidget(tab_3);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(290, 40, 1101, 481));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        dataPlotGraphicsView = new QGraphicsView(verticalLayoutWidget_4);
        dataPlotGraphicsView->setObjectName(QString::fromUtf8("dataPlotGraphicsView"));
        dataPlotGraphicsView->setEnabled(true);

        verticalLayout_4->addWidget(dataPlotGraphicsView);

        dataPlotLabel = new QLabel(tab_3);
        dataPlotLabel->setObjectName(QString::fromUtf8("dataPlotLabel"));
        dataPlotLabel->setGeometry(QRect(290, 17, 81, 20));
        dataPlotLabel->setFont(font);
        verticalLayoutWidget = new QWidget(tab_3);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(29, 19, 191, 22));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);

        verticalLayout->addWidget(label_5);

        dataThroughputLcdNumber = new QLCDNumber(tab_3);
        dataThroughputLcdNumber->setObjectName(QString::fromUtf8("dataThroughputLcdNumber"));
        dataThroughputLcdNumber->setGeometry(QRect(220, 313, 61, 20));
        dataThroughputLcdNumber->setFont(font);
        dataThroughputLcdNumber->setSmallDecimalPoint(true);
        verticalLayoutWidget_9 = new QWidget(tab_3);
        verticalLayoutWidget_9->setObjectName(QString::fromUtf8("verticalLayoutWidget_9"));
        verticalLayoutWidget_9->setGeometry(QRect(29, 40, 251, 31));
        verticalLayout_9 = new QVBoxLayout(verticalLayoutWidget_9);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        activeLinksComboBox = new QComboBox(verticalLayoutWidget_9);
        activeLinksComboBox->setObjectName(QString::fromUtf8("activeLinksComboBox"));
        activeLinksComboBox->setFont(font);

        verticalLayout_9->addWidget(activeLinksComboBox);

        label_6 = new QLabel(tab_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(30, 313, 181, 20));
        label_6->setFont(font);
        savePlotPushButton = new QPushButton(tab_3);
        savePlotPushButton->setObjectName(QString::fromUtf8("savePlotPushButton"));
        savePlotPushButton->setGeometry(QRect(750, 530, 84, 28));
        savePlotPushButton->setFont(font);
        tabWidget->addTab(tab_3, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QString::fromUtf8("tab_8"));
        verticalLayoutWidget_10 = new QWidget(tab_8);
        verticalLayoutWidget_10->setObjectName(QString::fromUtf8("verticalLayoutWidget_10"));
        verticalLayoutWidget_10->setGeometry(QRect(19, 29, 215, 131));
        verticalLayout_10 = new QVBoxLayout(verticalLayoutWidget_10);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(verticalLayoutWidget_10);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayoutWidget_11 = new QWidget(groupBox_2);
        verticalLayoutWidget_11->setObjectName(QString::fromUtf8("verticalLayoutWidget_11"));
        verticalLayoutWidget_11->setGeometry(QRect(0, 30, 301, 31));
        verticalLayout_11 = new QVBoxLayout(verticalLayoutWidget_11);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        voltageCheckBox = new QCheckBox(verticalLayoutWidget_11);
        voltageCheckBox->setObjectName(QString::fromUtf8("voltageCheckBox"));
        voltageCheckBox->setFont(font);

        verticalLayout_11->addWidget(voltageCheckBox);

        verticalLayoutWidget_12 = new QWidget(groupBox_2);
        verticalLayoutWidget_12->setObjectName(QString::fromUtf8("verticalLayoutWidget_12"));
        verticalLayoutWidget_12->setGeometry(QRect(0, 60, 301, 31));
        verticalLayout_12 = new QVBoxLayout(verticalLayoutWidget_12);
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setContentsMargins(11, 11, 11, 11);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(0, 0, 0, 0);
        currentCheckBox = new QCheckBox(verticalLayoutWidget_12);
        currentCheckBox->setObjectName(QString::fromUtf8("currentCheckBox"));
        currentCheckBox->setFont(font);

        verticalLayout_12->addWidget(currentCheckBox);

        verticalLayoutWidget_14 = new QWidget(groupBox_2);
        verticalLayoutWidget_14->setObjectName(QString::fromUtf8("verticalLayoutWidget_14"));
        verticalLayoutWidget_14->setGeometry(QRect(0, 90, 301, 31));
        verticalLayout_14 = new QVBoxLayout(verticalLayoutWidget_14);
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setContentsMargins(11, 11, 11, 11);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        powerCheckBox = new QCheckBox(verticalLayoutWidget_14);
        powerCheckBox->setObjectName(QString::fromUtf8("powerCheckBox"));
        powerCheckBox->setFont(font);

        verticalLayout_14->addWidget(powerCheckBox);


        verticalLayout_10->addWidget(groupBox_2);

        powerMeasurementsGraphicsView = new QGraphicsView(tab_8);
        powerMeasurementsGraphicsView->setObjectName(QString::fromUtf8("powerMeasurementsGraphicsView"));
        powerMeasurementsGraphicsView->setGeometry(QRect(261, 30, 1131, 481));
        verticalLayoutWidget_13 = new QWidget(tab_8);
        verticalLayoutWidget_13->setObjectName(QString::fromUtf8("verticalLayoutWidget_13"));
        verticalLayoutWidget_13->setGeometry(QRect(20, 220, 247, 201));
        verticalLayout_13 = new QVBoxLayout(verticalLayoutWidget_13);
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setContentsMargins(11, 11, 11, 11);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        groupBox_6 = new QGroupBox(verticalLayoutWidget_13);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setFont(font);
        verticalLayoutWidget_15 = new QWidget(groupBox_6);
        verticalLayoutWidget_15->setObjectName(QString::fromUtf8("verticalLayoutWidget_15"));
        verticalLayoutWidget_15->setGeometry(QRect(0, 50, 91, 31));
        verticalLayout_15 = new QVBoxLayout(verticalLayoutWidget_15);
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setContentsMargins(11, 11, 11, 11);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        verticalLayout_15->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(verticalLayoutWidget_15);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font);

        verticalLayout_15->addWidget(label_9);

        verticalLayoutWidget_16 = new QWidget(groupBox_6);
        verticalLayoutWidget_16->setObjectName(QString::fromUtf8("verticalLayoutWidget_16"));
        verticalLayoutWidget_16->setGeometry(QRect(90, 50, 120, 31));
        verticalLayout_16 = new QVBoxLayout(verticalLayoutWidget_16);
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        verticalLayout_16->setContentsMargins(0, 0, 0, 0);
        voltageLcdNumber = new QLCDNumber(verticalLayoutWidget_16);
        voltageLcdNumber->setObjectName(QString::fromUtf8("voltageLcdNumber"));
        voltageLcdNumber->setFont(font);
        voltageLcdNumber->setSmallDecimalPoint(true);

        verticalLayout_16->addWidget(voltageLcdNumber);

        verticalLayoutWidget_17 = new QWidget(groupBox_6);
        verticalLayoutWidget_17->setObjectName(QString::fromUtf8("verticalLayoutWidget_17"));
        verticalLayoutWidget_17->setGeometry(QRect(91, 80, 120, 31));
        verticalLayout_17 = new QVBoxLayout(verticalLayoutWidget_17);
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setContentsMargins(11, 11, 11, 11);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        verticalLayout_17->setContentsMargins(0, 0, 0, 0);
        currentLcdNumber = new QLCDNumber(verticalLayoutWidget_17);
        currentLcdNumber->setObjectName(QString::fromUtf8("currentLcdNumber"));
        currentLcdNumber->setFont(font);
        currentLcdNumber->setSmallDecimalPoint(true);

        verticalLayout_17->addWidget(currentLcdNumber);

        verticalLayoutWidget_18 = new QWidget(groupBox_6);
        verticalLayoutWidget_18->setObjectName(QString::fromUtf8("verticalLayoutWidget_18"));
        verticalLayoutWidget_18->setGeometry(QRect(90, 110, 120, 31));
        verticalLayout_18 = new QVBoxLayout(verticalLayoutWidget_18);
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setContentsMargins(11, 11, 11, 11);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        verticalLayout_18->setContentsMargins(0, 0, 0, 0);
        powerLcdNumber = new QLCDNumber(verticalLayoutWidget_18);
        powerLcdNumber->setObjectName(QString::fromUtf8("powerLcdNumber"));
        powerLcdNumber->setFont(font);
        powerLcdNumber->setSmallDecimalPoint(true);

        verticalLayout_18->addWidget(powerLcdNumber);

        verticalLayoutWidget_19 = new QWidget(groupBox_6);
        verticalLayoutWidget_19->setObjectName(QString::fromUtf8("verticalLayoutWidget_19"));
        verticalLayoutWidget_19->setGeometry(QRect(0, 80, 93, 31));
        verticalLayout_19 = new QVBoxLayout(verticalLayoutWidget_19);
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setContentsMargins(11, 11, 11, 11);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        verticalLayout_19->setContentsMargins(0, 0, 0, 0);
        label_10 = new QLabel(verticalLayoutWidget_19);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font);

        verticalLayout_19->addWidget(label_10);

        verticalLayoutWidget_25 = new QWidget(groupBox_6);
        verticalLayoutWidget_25->setObjectName(QString::fromUtf8("verticalLayoutWidget_25"));
        verticalLayoutWidget_25->setGeometry(QRect(0, 110, 91, 31));
        verticalLayout_25 = new QVBoxLayout(verticalLayoutWidget_25);
        verticalLayout_25->setSpacing(6);
        verticalLayout_25->setContentsMargins(11, 11, 11, 11);
        verticalLayout_25->setObjectName(QString::fromUtf8("verticalLayout_25"));
        verticalLayout_25->setContentsMargins(0, 0, 0, 0);
        label_11 = new QLabel(verticalLayoutWidget_25);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font);

        verticalLayout_25->addWidget(label_11);


        verticalLayout_13->addWidget(groupBox_6);

        savePowerPlotPushButton = new QPushButton(tab_8);
        savePowerPlotPushButton->setObjectName(QString::fromUtf8("savePowerPlotPushButton"));
        savePowerPlotPushButton->setGeometry(QRect(680, 530, 84, 28));
        savePowerPlotPushButton->setFont(font);
        pwrMeasurementStartBtn = new QPushButton(tab_8);
        pwrMeasurementStartBtn->setObjectName(QString::fromUtf8("pwrMeasurementStartBtn"));
        pwrMeasurementStartBtn->setGeometry(QRect(40, 450, 191, 28));
        powerMeasurementScrollBar = new QScrollBar(tab_8);
        powerMeasurementScrollBar->setObjectName(QString::fromUtf8("powerMeasurementScrollBar"));
        powerMeasurementScrollBar->setGeometry(QRect(400, 520, 661, 20));
        powerMeasurementScrollBar->setOrientation(Qt::Horizontal);
        tabWidget->addTab(tab_8, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        loadScenarioButton = new QPushButton(tab_2);
        loadScenarioButton->setObjectName(QString::fromUtf8("loadScenarioButton"));
        loadScenarioButton->setEnabled(true);
        loadScenarioButton->setGeometry(QRect(700, 520, 111, 41));
        loadScenarioButton->setFont(font);
        saveScenarioButton = new QPushButton(tab_2);
        saveScenarioButton->setObjectName(QString::fromUtf8("saveScenarioButton"));
        saveScenarioButton->setEnabled(false);
        saveScenarioButton->setGeometry(QRect(820, 521, 111, 41));
        saveScenarioButton->setFont(font);
        startScenarioButton = new QPushButton(tab_2);
        startScenarioButton->setObjectName(QString::fromUtf8("startScenarioButton"));
        startScenarioButton->setEnabled(false);
        startScenarioButton->setGeometry(QRect(940, 521, 111, 41));
        startScenarioButton->setFont(font);
        endScenarioButton = new QPushButton(tab_2);
        endScenarioButton->setObjectName(QString::fromUtf8("endScenarioButton"));
        endScenarioButton->setEnabled(false);
        endScenarioButton->setGeometry(QRect(1060, 520, 111, 41));
        endScenarioButton->setFont(font);
        editScenarioButton = new QPushButton(tab_2);
        editScenarioButton->setObjectName(QString::fromUtf8("editScenarioButton"));
        editScenarioButton->setEnabled(true);
        editScenarioButton->setGeometry(QRect(1180, 520, 111, 41));
        editScenarioButton->setFont(font);
        testAutomationPlainTextEdit = new QPlainTextEdit(tab_2);
        testAutomationPlainTextEdit->setObjectName(QString::fromUtf8("testAutomationPlainTextEdit"));
        testAutomationPlainTextEdit->setGeometry(QRect(23, 20, 1371, 491));
        testAutomationPlainTextEdit->setFont(font);
        solnetValidationButton = new QPushButton(tab_2);
        solnetValidationButton->setObjectName(QString::fromUtf8("solnetValidationButton"));
        solnetValidationButton->setEnabled(true);
        solnetValidationButton->setGeometry(QRect(240, 527, 161, 31));
        solnetValidationButton->setFont(font);
        clearTestAutomationTextPushButton = new QPushButton(tab_2);
        clearTestAutomationTextPushButton->setObjectName(QString::fromUtf8("clearTestAutomationTextPushButton"));
        clearTestAutomationTextPushButton->setGeometry(QRect(410, 529, 80, 28));
        clearTestAutomationTextPushButton->setFont(font);
        saveTestAutomationTextPushButton = new QPushButton(tab_2);
        saveTestAutomationTextPushButton->setObjectName(QString::fromUtf8("saveTestAutomationTextPushButton"));
        saveTestAutomationTextPushButton->setGeometry(QRect(500, 529, 80, 28));
        saveTestAutomationTextPushButton->setFont(font);
        apiComplianceButton = new QPushButton(tab_2);
        apiComplianceButton->setObjectName(QString::fromUtf8("apiComplianceButton"));
        apiComplianceButton->setEnabled(true);
        apiComplianceButton->setGeometry(QRect(48, 527, 161, 31));
        apiComplianceButton->setFont(font);
        tabWidget->addTab(tab_2, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        firmwareDeviceEdit = new QLineEdit(tab_6);
        firmwareDeviceEdit->setObjectName(QString::fromUtf8("firmwareDeviceEdit"));
        firmwareDeviceEdit->setGeometry(QRect(120, 20, 161, 20));
        firmwareDeviceEdit->setFont(font);
        groupBox_4 = new QGroupBox(tab_6);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(20, 98, 421, 91));
        groupBox_4->setFont(font);
        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 30, 71, 16));
        label_2->setFont(font);
        firmwareUpdateEdit = new QLineEdit(groupBox_4);
        firmwareUpdateEdit->setObjectName(QString::fromUtf8("firmwareUpdateEdit"));
        firmwareUpdateEdit->setGeometry(QRect(80, 30, 291, 20));
        firmwareUpdateEdit->setFont(font);
        updateFirmwareButton = new QPushButton(groupBox_4);
        updateFirmwareButton->setObjectName(QString::fromUtf8("updateFirmwareButton"));
        updateFirmwareButton->setGeometry(QRect(120, 60, 171, 23));
        updateFirmwareButton->setFont(font);
        firmwareDialogToolButton = new QToolButton(groupBox_4);
        firmwareDialogToolButton->setObjectName(QString::fromUtf8("firmwareDialogToolButton"));
        firmwareDialogToolButton->setGeometry(QRect(380, 30, 25, 19));
        groupBox_5 = new QGroupBox(tab_6);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(20, 227, 421, 71));
        groupBox_5->setFont(font);
        label_3 = new QLabel(groupBox_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 30, 47, 13));
        label_3->setFont(font);
        activeFirmwareEdit = new QLineEdit(groupBox_5);
        activeFirmwareEdit->setObjectName(QString::fromUtf8("activeFirmwareEdit"));
        activeFirmwareEdit->setGeometry(QRect(70, 30, 331, 20));
        firmwareDeviceLabel = new QLabel(tab_6);
        firmwareDeviceLabel->setObjectName(QString::fromUtf8("firmwareDeviceLabel"));
        firmwareDeviceLabel->setGeometry(QRect(22, 20, 101, 20));
        firmwareDeviceLabel->setFont(font);
        cancelPushButton = new QPushButton(tab_6);
        cancelPushButton->setObjectName(QString::fromUtf8("cancelPushButton"));
        cancelPushButton->setGeometry(QRect(20, 50, 221, 28));
        cancelPushButton->setFont(font);
        firmwareUpdateTextEdit = new QTextEdit(tab_6);
        firmwareUpdateTextEdit->setObjectName(QString::fromUtf8("firmwareUpdateTextEdit"));
        firmwareUpdateTextEdit->setGeometry(QRect(463, 39, 921, 481));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        firmwareUpdateTextEdit->setFont(font1);
        firmwareUpdateTextEdit->setReadOnly(true);
        saveUpdateFirmwareTextPushButton = new QPushButton(tab_6);
        saveUpdateFirmwareTextPushButton->setObjectName(QString::fromUtf8("saveUpdateFirmwareTextPushButton"));
        saveUpdateFirmwareTextPushButton->setGeometry(QRect(466, 535, 84, 28));
        saveUpdateFirmwareTextPushButton->setFont(font);
        clearUpdateFirmwareTextPushButton = new QPushButton(tab_6);
        clearUpdateFirmwareTextPushButton->setObjectName(QString::fromUtf8("clearUpdateFirmwareTextPushButton"));
        clearUpdateFirmwareTextPushButton->setGeometry(QRect(566, 535, 84, 28));
        clearUpdateFirmwareTextPushButton->setFont(font);
        label_8 = new QLabel(tab_6);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(464, 13, 81, 20));
        label_8->setFont(font);
        tabWidget->addTab(tab_6, QString());

        verticalLayout_20->addWidget(tabWidget);


        verticalLayout_21->addLayout(verticalLayout_20);

        iswTestTool->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(iswTestTool);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1432, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuOptions = new QMenu(menuBar);
        menuOptions->setObjectName(QString::fromUtf8("menuOptions"));
        menuDatabase_Connection = new QMenu(menuOptions);
        menuDatabase_Connection->setObjectName(QString::fromUtf8("menuDatabase_Connection"));
        iswTestTool->setMenuBar(menuBar);
        mainToolBar = new QToolBar(iswTestTool);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        iswTestTool->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(iswTestTool);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        iswTestTool->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuOptions->menuAction());
        menuFile->addAction(actionExit);
        menuOptions->addAction(actionAttenuator_Control);
        menuOptions->addAction(menuDatabase_Connection->menuAction());
        menuDatabase_Connection->addAction(actionOpen);
        menuDatabase_Connection->addAction(actionClose_Database);

        retranslateUi(iswTestTool);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(iswTestTool);
    } // setupUi

    void retranslateUi(QMainWindow *iswTestTool)
    {
        iswTestTool->setWindowTitle(QCoreApplication::translate("iswTestTool", "Intra-Soldier Wireless (ISW) V&V Kit - v0.7", nullptr));
        actionExit->setText(QCoreApplication::translate("iswTestTool", "Exit", nullptr));
        actionUpdate_Firmware->setText(QCoreApplication::translate("iswTestTool", "Update Firmware", nullptr));
        actionSave->setText(QCoreApplication::translate("iswTestTool", "Open", nullptr));
        actionSave_2->setText(QCoreApplication::translate("iswTestTool", "Save", nullptr));
        actionLoad_Scenario->setText(QCoreApplication::translate("iswTestTool", "Load Scenario", nullptr));
        actionAttenuator_Control->setText(QCoreApplication::translate("iswTestTool", "Attenuator Control", nullptr));
        actionOpen->setText(QCoreApplication::translate("iswTestTool", "Open Database", nullptr));
        actionClose_Database->setText(QCoreApplication::translate("iswTestTool", "Close Database", nullptr));
        actionService_Descriptor_Configuration->setText(QCoreApplication::translate("iswTestTool", "Service Descriptor Configuration", nullptr));
        groupBox->setTitle(QCoreApplication::translate("iswTestTool", "Device Attributes:", nullptr));
        macAddressLabel->setText(QCoreApplication::translate("iswTestTool", "MAC Address:", nullptr));
        coordPriorityLabel->setText(QCoreApplication::translate("iswTestTool", "Coordinator Priority:", nullptr));
        devTypeLabel->setText(QCoreApplication::translate("iswTestTool", "Device Type:", nullptr));
        coordPeerLabel->setText(QCoreApplication::translate("iswTestTool", "Role:", nullptr));
#if QT_CONFIG(tooltip)
        macAddressEdit->setToolTip(QCoreApplication::translate("iswTestTool", "Test String", nullptr));
#endif // QT_CONFIG(tooltip)
        firmwareLabel->setText(QCoreApplication::translate("iswTestTool", "Firmware Version:", nullptr));
        channelLabel->setText(QCoreApplication::translate("iswTestTool", "Network Channel:", nullptr));
        peersLabel->setText(QCoreApplication::translate("iswTestTool", "# of Peers:", nullptr));
        phyRateLabel->setText(QCoreApplication::translate("iswTestTool", "PHY Rate:", nullptr));
        netIdLabel->setText(QCoreApplication::translate("iswTestTool", "Network ID:", nullptr));
        netDevLabel->setText(QCoreApplication::translate("iswTestTool", "Device List:", nullptr));
        netTopologyLabel->setText(QCoreApplication::translate("iswTestTool", "Network Topology:", nullptr));
        scanButton->setText(QCoreApplication::translate("iswTestTool", "Update Device List", nullptr));
        label->setText(QCoreApplication::translate("iswTestTool", "Peer List:", nullptr));
        updateNetTopologyButton->setText(QCoreApplication::translate("iswTestTool", "Update Network Topology", nullptr));
        mcastAddrLabel->setText(QCoreApplication::translate("iswTestTool", "Multicast Address:", nullptr));
        connectionTimeLabel->setText(QCoreApplication::translate("iswTestTool", "Time to Join Network (sec):", nullptr));
#if QT_CONFIG(tooltip)
        connectionTimeEdit->setToolTip(QCoreApplication::translate("iswTestTool", "Test String", nullptr));
#endif // QT_CONFIG(tooltip)
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("iswTestTool", "Network Configuration", nullptr));
        label_4->setText(QCoreApplication::translate("iswTestTool", "Device List:", nullptr));
        printServicesPushButton->setText(QCoreApplication::translate("iswTestTool", "Print Services", nullptr));
        label_7->setText(QCoreApplication::translate("iswTestTool", "Log:", nullptr));
        clearServiceDescriptorTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Clear Text", nullptr));
        saveServiceDescriptorTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Save Text", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("iswTestTool", "Service Descriptor Configuration", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5_registerServices), QCoreApplication::translate("iswTestTool", "Register Services", nullptr));
        activeLinksLabel->setText(QCoreApplication::translate("iswTestTool", "Data Traffic Configuration:", nullptr));
        activeLinksLabel_2->setText(QCoreApplication::translate("iswTestTool", "Data Throughput Plot:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QCoreApplication::translate("iswTestTool", "Data Traffic Configuration", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("iswTestTool", "Performance Statistics Filter:", nullptr));
        rssiCheckBox->setText(QCoreApplication::translate("iswTestTool", "RSSI", nullptr));
        perCheckBox->setText(QCoreApplication::translate("iswTestTool", "PER", nullptr));
        transmitCheckBox->setText(QCoreApplication::translate("iswTestTool", "Transmit (Mbps)", nullptr));
        receiveCheckBox->setText(QCoreApplication::translate("iswTestTool", "Receive (Mbps)", nullptr));
        dataPlotLabel->setText(QCoreApplication::translate("iswTestTool", "Data Plot:", nullptr));
        label_5->setText(QCoreApplication::translate("iswTestTool", "Available Links:", nullptr));
        label_6->setText(QCoreApplication::translate("iswTestTool", "Data Thoughputput (Mbps):", nullptr));
        savePlotPushButton->setText(QCoreApplication::translate("iswTestTool", "Save Plot", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("iswTestTool", "Network Performance Statistics", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("iswTestTool", "Power Measurements Filter:", nullptr));
        voltageCheckBox->setText(QCoreApplication::translate("iswTestTool", "Voltage (V)", nullptr));
        currentCheckBox->setText(QCoreApplication::translate("iswTestTool", "Current (mA)", nullptr));
        powerCheckBox->setText(QCoreApplication::translate("iswTestTool", "Power (mW)", nullptr));
        groupBox_6->setTitle(QCoreApplication::translate("iswTestTool", "Real-Time Power Measurements:", nullptr));
        label_9->setText(QCoreApplication::translate("iswTestTool", "Voltage (V)", nullptr));
        label_10->setText(QCoreApplication::translate("iswTestTool", "Current (mA)", nullptr));
        label_11->setText(QCoreApplication::translate("iswTestTool", "Power (mW)", nullptr));
        savePowerPlotPushButton->setText(QCoreApplication::translate("iswTestTool", "Save Plot", nullptr));
        pwrMeasurementStartBtn->setText(QCoreApplication::translate("iswTestTool", "Start Power measurement", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_8), QCoreApplication::translate("iswTestTool", "Power Measurements", nullptr));
        loadScenarioButton->setText(QCoreApplication::translate("iswTestTool", "Load Scenario", nullptr));
        saveScenarioButton->setText(QCoreApplication::translate("iswTestTool", "Save Scenario", nullptr));
        startScenarioButton->setText(QCoreApplication::translate("iswTestTool", "Start Scenario", nullptr));
        endScenarioButton->setText(QCoreApplication::translate("iswTestTool", "End Scenario", nullptr));
        editScenarioButton->setText(QCoreApplication::translate("iswTestTool", "Edit Scenario", nullptr));
        solnetValidationButton->setText(QCoreApplication::translate("iswTestTool", "ISW SolNet Validation", nullptr));
        clearTestAutomationTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Clear Text", nullptr));
        saveTestAutomationTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Save Text", nullptr));
        apiComplianceButton->setText(QCoreApplication::translate("iswTestTool", "ISW API Compliance", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("iswTestTool", "Test Automation", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("iswTestTool", "Update Firmware Image:", nullptr));
        label_2->setText(QCoreApplication::translate("iswTestTool", "Firmware:", nullptr));
        updateFirmwareButton->setText(QCoreApplication::translate("iswTestTool", "Update Firmware Image", nullptr));
        firmwareDialogToolButton->setText(QCoreApplication::translate("iswTestTool", "...", nullptr));
        groupBox_5->setTitle(QCoreApplication::translate("iswTestTool", "Firmware:", nullptr));
        label_3->setText(QCoreApplication::translate("iswTestTool", "Active:", nullptr));
        firmwareDeviceLabel->setText(QCoreApplication::translate("iswTestTool", "Device Name:", nullptr));
        cancelPushButton->setText(QCoreApplication::translate("iswTestTool", "Cancel/Exit Firmware Update", nullptr));
        saveUpdateFirmwareTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Save Text", nullptr));
        clearUpdateFirmwareTextPushButton->setText(QCoreApplication::translate("iswTestTool", "Clear Text", nullptr));
        label_8->setText(QCoreApplication::translate("iswTestTool", "Log:", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QCoreApplication::translate("iswTestTool", "Update Firmware", nullptr));
        menuFile->setTitle(QCoreApplication::translate("iswTestTool", "File", nullptr));
        menuOptions->setTitle(QCoreApplication::translate("iswTestTool", "Options", nullptr));
        menuDatabase_Connection->setTitle(QCoreApplication::translate("iswTestTool", "Database Connection", nullptr));
    } // retranslateUi

};

namespace Ui {
    class iswTestTool: public Ui_iswTestTool {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ISWTESTTOOL_H
